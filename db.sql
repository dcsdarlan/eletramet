--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.8
-- Dumped by pg_dump version 9.6.0

-- Started on 2018-06-14 18:57:31

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 49379)
-- Name: mecsw; Type: SCHEMA; Schema: -; Owner: mecsw
--

CREATE SCHEMA mecsw;


ALTER SCHEMA mecsw OWNER TO mecsw;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2330 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = mecsw, pg_catalog;

--
-- TOC entry 188 (class 1259 OID 49380)
-- Name: sq_review; Type: SEQUENCE; Schema: mecsw; Owner: mecsw
--

CREATE SEQUENCE sq_review
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_review OWNER TO mecsw;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 208 (class 1259 OID 57556)
-- Name: tb_calibration_laboratory; Type: TABLE; Schema: mecsw; Owner: postgres
--

CREATE TABLE tb_calibration_laboratory (
    code integer NOT NULL,
    name character varying NOT NULL,
    cnpj character varying(14) NOT NULL,
    email character varying,
    contact character varying NOT NULL,
    place character varying NOT NULL,
    number character varying(50) NOT NULL,
    complement character varying,
    neighborhood character varying NOT NULL,
    city character varying NOT NULL,
    country character varying(2) NOT NULL,
    cep character varying(8) NOT NULL,
    tel1 character varying,
    tel2 character varying,
    tel3 character varying
);


ALTER TABLE tb_calibration_laboratory OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 57569)
-- Name: tb_calibration_laboratory_aud; Type: TABLE; Schema: mecsw; Owner: postgres
--

CREATE TABLE tb_calibration_laboratory_aud (
    code bigint NOT NULL,
    cep character varying NOT NULL,
    city character varying,
    code_review integer NOT NULL,
    type_review smallint,
    cnpj character varying,
    complement character varying,
    contact character varying,
    country character varying(2),
    email character varying,
    name character varying,
    neighborhood character varying,
    number character varying,
    place character varying,
    tel1 character varying,
    tel2 character varying,
    tel3 character varying
);


ALTER TABLE tb_calibration_laboratory_aud OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 57564)
-- Name: tb_calibration_laboratory_telphone; Type: TABLE; Schema: mecsw; Owner: postgres
--

CREATE TABLE tb_calibration_laboratory_telphone (
    code integer NOT NULL,
    calibration_laboratory_code integer NOT NULL,
    ddd character varying(2) NOT NULL,
    number character varying(9) NOT NULL
);


ALTER TABLE tb_calibration_laboratory_telphone OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 49382)
-- Name: tb_functionality; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_functionality (
    code bigint NOT NULL,
    description character varying(255),
    mnemonic character varying(255),
    order_function integer
);


ALTER TABLE tb_functionality OWNER TO mecsw;

--
-- TOC entry 190 (class 1259 OID 49388)
-- Name: tb_measuring_instrument; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_measuring_instrument (
    code integer NOT NULL,
    specification_identifier character varying(30) NOT NULL,
    measuring_instrument_type_code integer NOT NULL,
    measuring_instrument_model_code integer NOT NULL,
    measuring_instrument_place_use_code integer NOT NULL,
    calibration_frequency_weeks integer NOT NULL,
    last_calibration_date timestamp without time zone NOT NULL,
    next_calibration_date timestamp without time zone NOT NULL,
    obsolete_identifier character varying(3) NOT NULL,
    damaged_identifier character varying(3) NOT NULL,
    state_identifier character varying(30) NOT NULL,
    state_timestamp timestamp without time zone NOT NULL,
    description text,
    version bigint NOT NULL,
    calibration_frequency_days integer,
    measuring_instrument_department_code integer,
    calibration_laboratory_code integer
);


ALTER TABLE tb_measuring_instrument OWNER TO mecsw;

--
-- TOC entry 191 (class 1259 OID 49394)
-- Name: tb_measuring_instrument_aud; Type: TABLE; Schema: mecsw; Owner: postgres
--

CREATE TABLE tb_measuring_instrument_aud (
    code integer NOT NULL,
    specification_identifier character varying(30) NOT NULL,
    measuring_instrument_type_code integer NOT NULL,
    measuring_instrument_model_code integer NOT NULL,
    measuring_instrument_place_use_code integer NOT NULL,
    calibration_frequency_weeks integer NOT NULL,
    last_calibration_date timestamp without time zone NOT NULL,
    next_calibration_date timestamp without time zone NOT NULL,
    obsolete_identifier character varying(3) NOT NULL,
    damaged_identifier character varying(3) NOT NULL,
    state_identifier character varying(30) NOT NULL,
    state_timestamp timestamp without time zone NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint,
    measuring_instrument_department_code integer,
    calibration_laboratory_code integer
);


ALTER TABLE tb_measuring_instrument_aud OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 57352)
-- Name: tb_measuring_instrument_department; Type: TABLE; Schema: mecsw; Owner: postgres
--

CREATE TABLE tb_measuring_instrument_department (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    version bigint,
    user_code integer
);


ALTER TABLE tb_measuring_instrument_department OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 57362)
-- Name: tb_measuring_instrument_department_aud; Type: TABLE; Schema: mecsw; Owner: postgres
--

CREATE TABLE tb_measuring_instrument_department_aud (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint,
    user_code integer
);


ALTER TABLE tb_measuring_instrument_department_aud OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 49400)
-- Name: tb_measuring_instrument_manufacturer; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_measuring_instrument_manufacturer (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    version bigint
);


ALTER TABLE tb_measuring_instrument_manufacturer OWNER TO mecsw;

--
-- TOC entry 193 (class 1259 OID 49406)
-- Name: tb_measuring_instrument_manufacturer_aud; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_measuring_instrument_manufacturer_aud (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint
);


ALTER TABLE tb_measuring_instrument_manufacturer_aud OWNER TO mecsw;

--
-- TOC entry 194 (class 1259 OID 49412)
-- Name: tb_measuring_instrument_measuring_instrument_standard; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_measuring_instrument_measuring_instrument_standard (
    measuring_instrument_code bigint NOT NULL,
    measuring_instrument_standard_code bigint NOT NULL
);


ALTER TABLE tb_measuring_instrument_measuring_instrument_standard OWNER TO mecsw;

--
-- TOC entry 195 (class 1259 OID 49415)
-- Name: tb_measuring_instrument_model; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_measuring_instrument_model (
    code integer NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    measuring_instrument_manufacturer_code integer NOT NULL,
    version bigint NOT NULL
);


ALTER TABLE tb_measuring_instrument_model OWNER TO mecsw;

--
-- TOC entry 196 (class 1259 OID 49421)
-- Name: tb_measuring_instrument_model_aud; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_measuring_instrument_model_aud (
    code integer NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    measuring_instrument_manufacturer_code integer NOT NULL,
    code_review integer NOT NULL,
    type_review smallint
);


ALTER TABLE tb_measuring_instrument_model_aud OWNER TO mecsw;

--
-- TOC entry 197 (class 1259 OID 49427)
-- Name: tb_measuring_instrument_place_use; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_measuring_instrument_place_use (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    version bigint
);


ALTER TABLE tb_measuring_instrument_place_use OWNER TO mecsw;

--
-- TOC entry 198 (class 1259 OID 49433)
-- Name: tb_measuring_instrument_place_use_aud; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_measuring_instrument_place_use_aud (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint
);


ALTER TABLE tb_measuring_instrument_place_use_aud OWNER TO mecsw;

--
-- TOC entry 199 (class 1259 OID 49439)
-- Name: tb_measuring_instrument_standard; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_measuring_instrument_standard (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    version bigint
);


ALTER TABLE tb_measuring_instrument_standard OWNER TO mecsw;

--
-- TOC entry 200 (class 1259 OID 49445)
-- Name: tb_measuring_instrument_standard_aud; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_measuring_instrument_standard_aud (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint
);


ALTER TABLE tb_measuring_instrument_standard_aud OWNER TO mecsw;

--
-- TOC entry 201 (class 1259 OID 49451)
-- Name: tb_measuring_instrument_type; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_measuring_instrument_type (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    version bigint
);


ALTER TABLE tb_measuring_instrument_type OWNER TO mecsw;

--
-- TOC entry 202 (class 1259 OID 49457)
-- Name: tb_measuring_instrument_type_aud; Type: TABLE; Schema: mecsw; Owner: postgres
--

CREATE TABLE tb_measuring_instrument_type_aud (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint
);


ALTER TABLE tb_measuring_instrument_type_aud OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 49463)
-- Name: tb_review; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_review (
    code bigint NOT NULL,
    audit_date timestamp without time zone,
    user_code bigint,
    transaction_code integer,
    transaction_title character varying(100)
);


ALTER TABLE tb_review OWNER TO mecsw;

--
-- TOC entry 204 (class 1259 OID 49466)
-- Name: tb_user; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_user (
    code integer NOT NULL,
    ldap_login character varying(60) NOT NULL,
    user_name character varying(120) NOT NULL,
    active character varying(1) NOT NULL,
    last_access timestamp without time zone,
    version bigint,
    email character varying
);


ALTER TABLE tb_user OWNER TO mecsw;

--
-- TOC entry 205 (class 1259 OID 49469)
-- Name: tb_user_functionality; Type: TABLE; Schema: mecsw; Owner: mecsw
--

CREATE TABLE tb_user_functionality (
    user_code bigint NOT NULL,
    functionality_code bigint NOT NULL
);


ALTER TABLE tb_user_functionality OWNER TO mecsw;

--
-- TOC entry 2331 (class 0 OID 0)
-- Dependencies: 188
-- Name: sq_review; Type: SEQUENCE SET; Schema: mecsw; Owner: mecsw
--

SELECT pg_catalog.setval('sq_review', 1394, true);


--
-- TOC entry 2321 (class 0 OID 57556)
-- Dependencies: 208
-- Data for Name: tb_calibration_laboratory; Type: TABLE DATA; Schema: mecsw; Owner: postgres
--

COPY tb_calibration_laboratory (code, name, cnpj, email, contact, place, number, complement, neighborhood, city, country, cep, tel1, tel2, tel3) FROM stdin;
1	TESTE 01	11111111111111	TESTE 01	TESTE 01	TESTE 01	TESTE 01	TESTE 01	TESTE 01	TESTE 01	TE	11111111	(11)1111-1111	(11)1111-1111	(11)1111-1111
2	TESTE 02	22222222222222	TESTE 02	TESTE 02	TESTE 02	TESTE 02	TESTE 02	TESTE 02	TESTE 02	TE	22222222	(22)2222-2222	(22)2222-2222	(22)2222-2222
3	TESTE 03 	11111111111111	TESTE 03 	TESTE 03 	TESTE 03 	TESTE 03 	TESTE 03 	TESTE 03 	TESTE 03 	TE	11111111	(11)1111-1111	(11)1111-1111	(03)1111-1111
4	TESTE 04 44444444444444	44444444444444	TESTE 04 44444444444444	TESTE 04 44444444444444	TESTE 04 44444444444444	TESTE 04 44444444444444	TESTE 04 44444444444444	TESTE 04 44444444444444	TESTE 04 44444444444444	44	44444444	(44)4444-4444	(44)4444-4444	(44)4444-4444
5	5	55555555555555	5	5	5	5	5	5	5	55	55555555	(55)5555-5555	(55)5555-5555	(55)5555-5555
6	6	66666666666666	6	6	6	6	6	6	6	6	66666666	(66)6666-6666	(66)6666-6666	(66)6666-6666
7	7	77777777777777	7	7	7	7	7	7	7	77	77777777	(77)7777-7777	(77)7777-7777	(77)7777-7777
\.


--
-- TOC entry 2323 (class 0 OID 57569)
-- Dependencies: 210
-- Data for Name: tb_calibration_laboratory_aud; Type: TABLE DATA; Schema: mecsw; Owner: postgres
--

COPY tb_calibration_laboratory_aud (code, cep, city, code_review, type_review, cnpj, complement, contact, country, email, name, neighborhood, number, place, tel1, tel2, tel3) FROM stdin;
2	60000	Fortaleza	1326	0	77777777777777	1	Cont	BR	email@em.com	Teste	Centro	1	rua 1	\N	\N	\N
3	78666445	Fortaleza	1327	0	33333333333333		João	BR	e@e.com	Lab	Sul	100	Rua 100	\N	\N	\N
4	60777777	FORTALEZA	1329	0	88855774777777		João	Br	lb@em.com	LAb	Centro	10	Rua N	\N	\N	\N
5	60763000	Fortaleza	1330	0	77777777777777		Roberto	BR	ebteste@email.com	Laboratorio de Teste	Centro	789	Rua N	\N	\N	\N
5	60763000	Fortaleza	1331	2	77777777777777		Roberto	BR	ebteste@email.com	Laboratorio de Teste	Centro	789	Rua N	\N	\N	\N
4	60777777	FORTALEZA	1332	2	88855774777777		João	Br	lb@em.com	LAb	Centro	10	Rua N	\N	\N	\N
3	78666445	Fortaleza	1333	2	33333333333333		João	BR	e@e.com	Lab	Sul	100	Rua 100	\N	\N	\N
1	60000000	Fortaleza	1334	1	11111111111111	ap 10	Contato do Laboratorio	BR	laboratorio@email.com	Laboratorio 13	Centro	100	Rua A	\N	\N	\N
2	60850000	Fortaleza	1335	1	77777777777777	1	Cont	BR	email@em.com	Teste 1	Centro	1	rua 1	\N	\N	\N
3	78552244	Fortaleza	1336	0	89654114444185	12	João	CE	em.2@email.com	Simulação	Centro	85	Av. A	\N	\N	\N
3	78552244	Fortaleza	1337	2	89654114444185	12	João	CE	em.2@email.com	Simulação	Centro	85	Av. A	\N	\N	\N
3	60888000	Fortaleza	1338	0	18745587445885		robson	CE	email1@mail.com	Ultimo teste	Messejana	10	Rua A	(85)55555-5555	(85)99999-9999	(85)88888-8888
4	99999999	Teste	1340	0	99999999999999	Teste	Teste	CE	Teste	Teste	Teste	Teste	Teste	(85)99999-9999	(99)99999-9999	(99)99999-9999
5	99999999	Teste	1341	0	99999999999999	Teste	Teste	CE	Teste	Teste	Teste	Teste	Teste	(85)99999-9999	(99)99999-9999	(99)99999-9999
5	99999999	Teste	1342	2	99999999999999	Teste	Teste	CE	Teste	Teste	Teste	Teste	Teste	(85)99999-9999	(99)99999-9999	(99)99999-9999
5	11111111	TESTE 2	1343	0	12345678912322	TESTE 2	TESTE 2	TE	TESTE 2	TESTE 2	TESTE 2	TESTE 2	TESTE 2	(11)11111-1111		
6	12345678	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	1345	0	33333333333333	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TE	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	(33)33333-3333	(33)33333-3333	(33)33333-3333
7	60420314	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	1346	0	11111111111111	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	TESTE 4 	12	TESTE 4 	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	TESTE 4 	TESTE 4 	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	(85)12345-6789	(99)99999-9999	(99)99999-9999
8	60420314	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	1347	0	11111111111111	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	TESTE 4 	12	TESTE 4 	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	TESTE 4 	TESTE 4 	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	(85)12345-6789	(99)99999-9999	(99)99999-9999
8	60420314	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	1348	2	11111111111111	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	TESTE 4 	12	TESTE 4 	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	TESTE 4 	TESTE 4 	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	(85)12345-6789	(99)99999-9999	(99)99999-9999
7	60420314	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	1349	1	11111111111111	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	TESTE 4 	12	TESTE 4 	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	TESTE 4 	TESTE 4 	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	(99)99999-9999	(99)99999-9999	(99)99999-9999
8	11111111	sssssssssssssss	1350	0	11111111111111	sssssssssssssss	adassss	ss	sssssssssssssssssss	ssssssssssss	ssssssssssss	sssssssssss	ssssssssssssssssssss	(55)55555-5555	(55)55555-5555	(55)55555-5555
9	11111111	sssssssssssssss	1351	0	11111111111111	sssssssssssssss	adassss	ss	sssssssssssssssssss	ssssssssssss	ssssssssssss	sssssssssss	ssssssssssssssssssss	(55)55555-5555	(55)55555-5555	(55)55555-5555
10	11111111	sssssssssssssss	1352	0	11111111111111	sssssssssssssss	adassss	ss	sssssssssssssssssss	ssssssssssss	ssssssssssss	sssssssssss	ssssssssssssssssssss	(55)55555-5555	(55)55555-5555	(55)55555-5555
10	11111111	sssssssssssssss	1353	2	11111111111111	sssssssssssssss	adassss	ss	sssssssssssssssssss	ssssssssssss	ssssssssssss	sssssssssss	ssssssssssssssssssss	(55)55555-5555	(55)55555-5555	(55)55555-5555
9	11111111	sssssssssssssss	1354	2	11111111111111	sssssssssssssss	adassss	ss	sssssssssssssssssss	ssssssssssss	ssssssssssss	sssssssssss	ssssssssssssssssssss	(55)55555-5555	(55)55555-5555	(55)55555-5555
9	60123123	TESTE 5	1357	0	12345612000112		TESTE 5	ce	TESTE@TESTE 5	TESTE 5	TESTE 5	123456789	TESTE 5	(88)88888-8888		
9	60123123	TESTE 5	1359	2	12345612000112		TESTE 5	ce	TESTE@TESTE 5	TESTE 5	TESTE 5	123456789	TESTE 5	(88)88888-8888		
8	11111111	sssssssssssssss	1360	2	11111111111111	sssssssssssssss	adassss	ss	sssssssssssssssssss	ssssssssssss	ssssssssssss	sssssssssss	ssssssssssssssssssss	(55)55555-5555	(55)55555-5555	(55)55555-5555
7	60420314	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	1361	2	11111111111111	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	TESTE 4 	12	TESTE 4 	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	TESTE 4 	TESTE 4 	TESTE 4 TESTE 4 TESTE 4 TESTE 4 	(99)99999-9999	(99)99999-9999	(99)99999-9999
6	12345678	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	1362	2	33333333333333	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TE	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	(33)33333-3333	(33)33333-3333	(33)33333-3333
5	11111111	TESTE 2	1363	2	12345678912322	TESTE 2	TESTE 2	TE	TESTE 2	TESTE 2	TESTE 2	TESTE 2	TESTE 2	(11)11111-1111		
4	99999999	Teste	1364	2	99999999999999	Teste	Teste	CE	Teste	Teste	Teste	Teste	Teste	(85)99999-9999	(99)99999-9999	(99)99999-9999
4	11111111	1111111111111111111111111111	1365	0	11111111111111	111111111111111111111111	1111111111111111111111111111	11	TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 TESTE 3 	11111111111111	11111111111111111111	111111111111111111111111	1111111111111111111111	(11)11111-1111	(11)11111-1111	(11)11111-1111
5	22222222	22222222222222222222	1366	0	22222222222222	222222	22222222222222222222	22	22222222222222222222222	1233333212332113	222222222222222222222222	22222222222222222222	22222222222222222222	(88)1234-5678		
6	99999999	99999999999	1367	0	99999999999999		9999999999999999999999	99	99999999999999999999	9999999999999999999999999999999	99999999999999999999999	999999999999999999999	999999999999999999999	(99)9999-9999	(88)8888-8888	
7	12345678	TESTE 10	1368	0	98765432100012	TESTE 10	TESTE 10	TE	TESTE 10	TESTE 10	TESTE 10	TESTE 10	TESTE 10	(39)2512-3456	(99)9999-9999	(99)9999-9999
1	11111111	TESTE 01	1373	0	11111111111111	TESTE 01	TESTE 01	TE	TESTE 01	TESTE 01	TESTE 01	TESTE 01	TESTE 01	(11)1111-1111	(11)1111-1111	(11)1111-1111
2	22222222	TESTE 02	1376	0	22222222222222	TESTE 02	TESTE 02	TE	TESTE 02	TESTE 02	TESTE 02	TESTE 02	TESTE 02	(22)2222-2222	(22)2222-2222	(22)2222-2222
3	11111111	TESTE 03 	1380	0	11111111111111	TESTE 03 	TESTE 03 	TE	TESTE 03 	TESTE 03 	TESTE 03 	TESTE 03 	TESTE 03 	(11)1111-1111	(11)1111-1111	(03)1111-1111
4	44444444	TESTE 04 44444444444444	1381	0	44444444444444	TESTE 04 44444444444444	TESTE 04 44444444444444	44	TESTE 04 44444444444444	TESTE 04 44444444444444	TESTE 04 44444444444444	TESTE 04 44444444444444	TESTE 04 44444444444444	(44)4444-4444	(44)4444-4444	(44)4444-4444
5	55555555	5	1382	0	55555555555555	5	5	55	5	5	5	5	5	(55)5555-5555	(55)5555-5555	(55)5555-5555
6	66666666	6	1385	0	66666666666666	6	6	6	6	6	6	6	6	(66)6666-6666	(66)6666-6666	(66)6666-6666
7	77777777	7	1391	0	77777777777777	7	7	77	7	7	7	7	7	(77)7777-7777	(77)7777-7777	(77)7777-7777
\.


--
-- TOC entry 2322 (class 0 OID 57564)
-- Dependencies: 209
-- Data for Name: tb_calibration_laboratory_telphone; Type: TABLE DATA; Schema: mecsw; Owner: postgres
--

COPY tb_calibration_laboratory_telphone (code, calibration_laboratory_code, ddd, number) FROM stdin;
\.


--
-- TOC entry 2302 (class 0 OID 49382)
-- Dependencies: 189
-- Data for Name: tb_functionality; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_functionality (code, description, mnemonic, order_function) FROM stdin;
1	Administration Manager	ROLE_ADM	1
8		ROLE_REG_CALIBRATION_LABORATORY	8
9		ROLE_REG_NOTIFICATION_USER	9
10		ROLE_REG_NOTIFICATION_RULES	10
11		ROLE_MOV_ME	11
12		ROLE_RPT_ME	12
2		ROLE_REG_MEASURING_INSTRUMENT_TYPE	2
3		ROLE_REG_MEASURING_INSTRUMENT_MANUFACTURER	3
4		ROLE_REG_MEASURING_INSTRUMENT_MODEL	4
5		ROLE_REG_MEASURING_INSTRUMENT_PLACE_USE	5
6		ROLE_REG_MEASURING_INSTRUMENT_STANDARD	6
7		ROLE_REG_MEASURING_INSTRUMENT	7
13	\N	ROLE_REG_MEASURING_INSTRUMENT_DEPARTMENT	7
\.


--
-- TOC entry 2303 (class 0 OID 49388)
-- Dependencies: 190
-- Data for Name: tb_measuring_instrument; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_measuring_instrument (code, specification_identifier, measuring_instrument_type_code, measuring_instrument_model_code, measuring_instrument_place_use_code, calibration_frequency_weeks, last_calibration_date, next_calibration_date, obsolete_identifier, damaged_identifier, state_identifier, state_timestamp, description, version, calibration_frequency_days, measuring_instrument_department_code, calibration_laboratory_code) FROM stdin;
7	BD-06	43	91	31	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	inUse	2017-09-22 00:00:00	\N	0	\N	\N	\N
504	BD-08 10124614	43	88	32	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	inUse	2017-09-22 00:00:00	\N	0	\N	\N	\N
505	BD-08 11284331	43	88	33	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2017-11-21 00:00:00	\N	0	\N	\N	\N
16	MR-02.	46	99	12	52	2017-08-24 00:00:00	2018-08-23 00:00:00	no	no	inUse	2017-08-24 00:00:00	\N	0	\N	\N	\N
134	PP-19 / K100918	1	1	49	52	2017-10-20 00:00:00	2018-10-19 00:00:00	no	no	inUse	2017-10-20 00:00:00	\N	0	\N	\N	\N
6	11191504 / BD-05	43	90	30	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	inUse	2017-09-22 00:00:00		1	\N	\N	\N
232	PP-32 / k091111	1	2	2	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	inUse	2017-12-20 00:00:00		1	\N	\N	\N
230	PP-30 / K100616	1	2	50	52	2017-12-27 00:00:00	2018-12-26 00:00:00	no	no	inUse	2018-03-24 11:52:12.363		3	\N	\N	\N
54	PP-12 / K091116	1	1	7	52	2017-12-08 00:00:00	2018-12-07 00:00:00	no	no	inUse	2017-12-08 00:00:00	THS	2	\N	\N	\N
704	HPT-05 / 3201091	5	24	40	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2018-02-02 14:11:46.187		3	\N	\N	\N
231	PP-31 / K110208	1	2	51	52	2017-12-11 00:00:00	2018-12-10 00:00:00	no	no	collected	2018-05-23 09:18:57.182		2	\N	\N	\N
449	SEFAR	48	102	12	52	2016-12-19 00:00:00	2017-12-18 00:00:00	no	no	collected	2018-01-10 16:57:41.218		1	\N	\N	\N
559	MULT-02 / 88450278	11	97	12	52	2018-03-07 00:00:00	2019-03-06 00:00:00	no	no	inUse	2018-03-12 17:24:14.927		7	\N	\N	\N
26	PM-31 / D110123	1	3	60	52	2017-08-07 00:00:00	2018-08-06 00:00:00	no	no	collected	2018-05-15 15:37:09.19		1	\N	\N	\N
426	HPT-03 / 3201192	5	24	37	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-23 00:00:00		1	\N	\N	\N
62	PM-36 / D101047	1	3	58	52	2017-08-04 00:00:00	2018-08-03 00:00:00	no	no	collected	2018-05-23 09:27:42.123	DANIFICADO	3	\N	\N	\N
123	HTP-15 / 109611067G	5	105	71	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	inUse	2018-03-13 16:24:42.968		4	\N	\N	\N
101	PP-18 / K100403	1	1	107	52	2017-12-18 00:00:00	2018-12-17 00:00:00	no	no	inUse	2017-12-18 00:00:00		2	\N	\N	\N
705	HPT-22/3202232	5	24	38	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-23 00:00:00		1	\N	\N	\N
255	MULT-14	11	94	94	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	inUse	2018-02-19 11:26:10.935		3	\N	\N	\N
696	MULT 06 / 11470123	11	95	1	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-23 00:00:00		3	\N	\N	\N
192	PP-25 / K100918	1	1	1	52	2018-04-27 00:00:00	2019-04-26 00:00:00	no	no	collected	2018-05-07 11:08:30.504	Ths\r\n	1	\N	\N	\N
708	PP-04 / K091118	1	2	41	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2017-11-21 00:00:00		1	\N	\N	\N
712	PM-30 / D091001	1	3	65	52	2017-12-28 00:00:00	2018-12-27 00:00:00	no	no	inUse	2018-03-19 10:23:45.885		4	\N	\N	\N
30	PP-10 / K101214	1	2	53	52	2017-12-18 00:00:00	2018-12-17 00:00:00	no	no	inUse	2018-01-15 12:10:19.299		5	\N	\N	\N
298	HPT-25/3420393	5	163	1	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2017-08-16 00:00:00		3	\N	\N	\N
102	PM-42 / D101138	1	3	10	52	2017-11-20 00:00:00	2018-11-19 00:00:00	no	no	inUse	2017-11-20 00:00:00		1	\N	\N	\N
1050	PP-1000	66	145	75	52	1999-11-20 00:00:00	2000-11-18 00:00:00	yes	no	registered	2018-02-15 18:43:35.32		2	\N	\N	\N
423	MULT-05 / ET405001046	44	93	12	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	inUse	2018-02-16 18:46:14.543		1	\N	\N	\N
226	PP-27 / K130913	1	2	52	52	2017-12-07 00:00:00	2018-12-06 00:00:00	no	no	inUse	2017-12-07 00:00:00		1	\N	\N	\N
27	PM-32 / D110127	1	3	11	52	2017-12-14 00:00:00	2018-12-13 00:00:00	no	no	inUse	2018-02-23 09:41:34.946		2	\N	\N	\N
227	PP-28 / K140206	1	2	56	52	2017-12-19 00:00:00	2018-12-18 00:00:00	no	no	inUse	2017-12-19 00:00:00		3	\N	\N	\N
24	PP-09 / K101218	1	21	43	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	collected	2018-01-15 12:13:00.392		4	\N	\N	\N
158	PAT-01	47	101	12	52	2018-03-26 00:00:00	2019-03-25 00:00:00	no	no	inUse	2018-04-18 17:59:30.369		2	\N	\N	\N
91	PP-14 / K110502	1	1	45	52	2017-12-19 00:00:00	2018-12-18 00:00:00	no	no	inUse	2018-01-09 14:08:59.864		3	\N	\N	\N
684	TH-21 / 1380000794	4	104	12	52	2017-11-22 00:00:00	2018-11-21 00:00:00	no	no	inUse	2018-01-12 14:56:30.142		2	\N	\N	\N
124	PM-46 / D101039	1	3	2	52	2017-12-06 00:00:00	2018-12-05 00:00:00	no	no	inUse	2018-03-22 08:36:12.226		2	\N	\N	\N
25	TH-02 / Q116844	4	103	12	52	2018-03-09 00:00:00	2019-03-08 00:00:00	no	no	inUse	2018-04-03 12:45:28.796		5	\N	\N	\N
28	PM-33 / D110128	1	3	43	52	2018-04-19 00:00:00	2019-04-18 00:00:00	no	no	collected	2018-04-09 12:34:37.798		3	\N	\N	\N
116	PM-45 / D101037	1	3	60	52	2018-04-14 00:00:00	2019-04-13 00:00:00	no	no	inUse	2018-05-15 15:37:24.54		3	\N	\N	\N
140	PP-23 / K100618	1	1	46	52	2017-10-23 00:00:00	2018-10-22 00:00:00	no	no	inUse	2018-05-16 08:56:24.191		3	\N	\N	\N
282	AMP-03	6	11	2	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2017-11-21 00:00:00	\N	0	\N	\N	\N
69	VOLT-10	7	12	5	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2017-11-23 00:00:00	\N	0	\N	\N	\N
126	PM-48 / D101033	1	3	59	52	2017-11-16 00:00:00	2018-11-15 00:00:00	no	yes	inUse	2017-11-16 00:00:00		1	\N	2	\N
2	BD-01	43	86	27	70	2019-09-18 00:00:00	2021-01-20 00:00:00	no	no	inUse	2017-09-22 00:00:00		1	\N	2	\N
139	PP-22 / K100614	1	1	47	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	inUse	2018-02-01 12:35:26.209		9	\N	3	\N
4	BD-03	43	88	29	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	dropOff	2018-06-11 12:55:51.846		2	\N	3	\N
228	PP-29 / K090801	1	1	44	52	2017-12-21 00:00:00	2018-12-20 00:00:00	no	no	inUse	2016-12-28 00:00:00		5	\N	2	\N
683	MR-01	45	100	12	2	2017-08-24 00:00:00	2017-09-07 00:00:00	no	no	inUse	2017-08-24 00:00:00		1	\N	1	\N
114	PM-43 / D101034	1	3	61	52	2017-10-18 00:00:00	2018-10-17 00:00:00	no	no	inUse	2017-10-18 00:00:00		1	\N	2	\N
92	PP-15 / K110401	1	1	54	52	2018-01-18 00:00:00	2019-01-17 00:00:00	no	no	collected	2018-05-23 09:18:12.418		6	\N	2	\N
189	ANE-01	45	173	12	52	2018-01-30 00:00:00	2019-01-29 00:00:00	no	no	collected	2018-03-20 11:02:25.021		4	\N	\N	\N
157	MULT-12 / EB294000257	11	96	12	52	2018-03-26 00:00:00	2019-03-25 00:00:00	no	no	inUse	2018-04-18 17:59:08.495		4	\N	\N	\N
5	BD-04	43	89	29	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	inUse	2017-09-22 00:00:00		1	\N	2	\N
11	HPT - 21 / 3202231	5	24	36	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-23 00:00:00		2	\N	\N	2
63	PM-37 / D101046	10	3	57	52	2017-11-05 00:00:00	2018-11-04 00:00:00	no	no	collected	2018-05-23 09:27:14.388		3	\N	2	2
193	PP-26 / K130202	1	26	55	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2018-02-02 21:17:57.276		8	\N	\N	5
121	HTP-13 / 109608214G	5	105	97	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	collected	2018-02-22 13:34:59.968		4	\N	\N	6
22	PP-07 / K101216	1	21	55	52	2017-09-06 00:00:00	2018-09-05 00:00:00	no	no	inUse	2017-09-06 00:00:00		2	\N	\N	6
93	PP-16 / K110505	1	1	48	52	2017-08-29 00:00:00	2018-08-28 00:00:00	no	no	inUse	2017-08-29 00:00:00		1	\N	\N	1
53	PM-39 / D101046	1	3	8	52	2017-12-12 00:00:00	2018-12-11 00:00:00	no	no	inUse	2017-12-12 00:00:00		1	\N	\N	7
8	BD-07	43	92	30	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	inUse	2017-09-22 00:00:00		1	\N	\N	1
594	PP-02 / 15844	15	22	9	52	2017-08-18 00:00:00	2018-08-17 00:00:00	no	no	inUse	2017-08-18 00:00:00	\N	0	\N	\N	\N
48	TH-29 / 7DB600084	3	6	10	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2017-11-23 00:00:00	\N	0	\N	\N	\N
183	TH-39 / 7DBC00320	3	6	8	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2017-11-23 00:00:00	\N	0	\N	\N	\N
260	VOLT-20	7	26	15	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2017-11-23 00:00:00	\N	0	\N	\N	\N
184	TH-40 / 7DBC00316	3	6	20	52	2017-08-15 00:00:00	2018-08-14 00:00:00	no	no	inUse	2017-08-15 00:00:00	\N	0	\N	\N	\N
181	TH-37 / 7DBC00312	3	6	20	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2017-08-16 00:00:00	\N	0	\N	\N	\N
153	TH-34 / 7DBC00315	3	6	20	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2017-08-16 00:00:00	\N	0	\N	\N	\N
14	CRA / 03.11PNP	16	28	21	75	2017-03-01 00:00:00	2018-08-08 00:00:00	no	no	inUse	2017-03-01 00:00:00	\N	0	\N	\N	\N
178	CRA / 07.12 PNP	16	30	21	75	2017-03-01 00:00:00	2018-08-08 00:00:00	no	no	inUse	2017-03-01 00:00:00	\N	0	\N	\N	\N
389	CRA / 6556/17	16	32	21	75	2017-05-30 00:00:00	2018-11-06 00:00:00	no	no	inUse	2017-05-30 00:00:00	\N	0	\N	\N	\N
390	CRA / 6557/17	16	32	21	75	2017-05-30 00:00:00	2018-11-06 00:00:00	no	no	inUse	2017-05-30 00:00:00	\N	0	\N	\N	\N
391	CRA / 6558/17	16	32	21	75	2017-05-30 00:00:00	2018-11-06 00:00:00	no	no	inUse	2017-05-30 00:00:00	\N	0	\N	\N	\N
394	CRA / 6555/17	16	32	21	75	2017-05-30 00:00:00	2018-11-06 00:00:00	no	no	inUse	2017-05-30 00:00:00	\N	0	\N	\N	\N
15	CRT / 03.11 P.N.P	17	33	21	75	2017-03-02 00:00:00	2018-08-09 00:00:00	no	no	inUse	2017-03-02 00:00:00	\N	0	\N	\N	\N
170	CRT / 06.12	17	36	21	75	2017-08-23 00:00:00	2019-01-30 00:00:00	no	no	inUse	2017-08-23 00:00:00	\N	0	\N	\N	\N
392	CRT / 6559/17	17	38	21	75	2017-05-30 00:00:00	2018-11-06 00:00:00	no	no	inUse	2017-05-30 00:00:00	\N	0	\N	\N	\N
393	CRT / 6560/17	17	38	21	75	2017-05-30 00:00:00	2018-11-06 00:00:00	no	no	inUse	2017-05-30 00:00:00	\N	0	\N	\N	\N
283	AMP-04	6	11	2	52	2017-12-01 00:00:00	2018-11-30 00:00:00	no	no	inUse	2017-11-30 00:00:00		1	\N	\N	\N
249	MULT-13 / 29990136	11	94	94	52	2016-10-07 00:00:00	2017-10-06 00:00:00	no	yes	inRepair	2018-02-19 11:25:03.348		3	\N	\N	\N
136	PP-20 / K091005	1	21	6	52	2017-09-04 00:00:00	2018-09-03 00:00:00	no	no	collected	2018-05-15 15:22:54.316	THS	2	\N	\N	\N
87	PM-40 / D101125	1	3	43	52	2018-03-13 00:00:00	2019-03-12 00:00:00	no	no	collected	2018-03-19 10:23:31.253		4	\N	\N	\N
261	PP-34/Z1507024	65	157	1	52	2018-01-10 00:00:00	2019-01-09 00:00:00	no	no	registered	1900-01-01 00:00:00		1	\N	\N	\N
241	TH-47 / 7DE902156	3	6	6	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inUse	2018-04-17 18:20:13.273		4	\N	\N	\N
52	VOLT-02	7	27	43	52	2018-01-11 00:00:00	2019-01-10 00:00:00	no	no	collected	2018-03-13 15:30:19.845		4	\N	\N	\N
351	CC-1	9	15	2	52	2016-12-19 00:00:00	2017-12-18 00:00:00	yes	no	registered	2018-01-24 19:37:23.197		3	\N	\N	\N
271	CRT / 1205157248	17	164	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 09:59:18.327		2	\N	\N	\N
44	CRA / 006PNP	16	28	21	75	2018-04-02 00:00:00	2019-09-09 00:00:00	no	no	inUse	2018-05-08 09:46:09.539		2	\N	\N	\N
154	TH-35 / 7DBC00318	3	6	106	52	2017-08-15 00:00:00	2018-08-14 00:00:00	no	no	inUse	2017-08-15 00:00:00		1	\N	\N	\N
700	TH-22 / 7DE902160	3	6	20	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2017-08-15 00:00:00		1	\N	\N	\N
272	CRT / 1205157269	17	164	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 09:59:56.221		2	\N	\N	\N
587	HTP-24	5	8	1	52	2017-02-03 00:00:00	2018-02-02 00:00:00	no	no	collected	2018-02-26 10:56:20.414		1	\N	\N	\N
13	CRA / 02.11 PNP	16	29	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 10:01:18.711		4	\N	\N	\N
60	PM-35 / D101138	1	3	66	52	2017-12-06 00:00:00	2018-12-05 00:00:00	no	no	inUse	2017-11-17 00:00:00		2	\N	\N	\N
95	CRT / 04.11	17	152	21	75	2018-03-05 00:00:00	2019-08-12 00:00:00	no	no	inUse	2018-04-04 10:36:38.453		5	\N	\N	\N
222	VOLT-23	7	26	1	52	2018-01-11 00:00:00	2019-01-10 00:00:00	no	no	inUse	2018-03-07 15:48:43.07		5	\N	\N	\N
100	CRT / S.008/15	17	34	21	75	2018-03-05 00:00:00	2019-08-12 00:00:00	no	no	inUse	2018-04-04 10:36:15.303		5	\N	\N	\N
295	PAQ-05 / 06911085	14	20	4	75	2016-11-28 00:00:00	2018-05-07 00:00:00	no	no	inUse	2016-11-28 00:00:00		1	\N	\N	\N
254	AMP-02/1203372401	6	10	5	52	2017-12-04 00:00:00	2018-12-03 00:00:00	no	no	inUse	2017-12-04 00:00:00		1	\N	\N	\N
278	TH-58 \\ 7E0800217	3	23	43	52	2018-04-27 00:00:00	2019-04-26 00:00:00	yes	no	collected	2018-04-04 13:16:06.098		7	\N	\N	\N
107	CRA / 011.00PNP	16	29	21	75	2018-04-02 00:00:00	2019-09-09 00:00:00	no	no	inUse	2018-05-02 14:01:57.984		2	\N	\N	\N
253	AMP-01	6	11	5	52	2017-12-01 00:00:00	2018-11-30 00:00:00	no	no	inUse	2017-11-23 00:00:00		5	\N	\N	\N
292	MULT-18/94810005	10	16	1	52	2017-01-24 00:00:00	2018-01-23 00:00:00	no	no	collected	2018-02-20 17:13:44.009		1	\N	\N	\N
98	VOLT-03	7	25	43	52	2017-05-11 00:00:00	2018-05-10 00:00:00	no	no	collected	2018-04-30 11:38:52.742		1	\N	\N	\N
274	CRA/1810140045	16	31	3	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inCalibration	2017-11-28 00:00:00		2	\N	\N	\N
285	VOLT-06	7	162	2	52	2017-12-01 00:00:00	2018-11-30 00:00:00	no	no	inUse	2017-11-30 00:00:00		1	\N	\N	\N
284	VOLT-05	7	162	2	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2017-11-21 00:00:00		2	\N	\N	\N
42	CRT / 5.008/14	17	34	21	75	2017-03-02 00:00:00	2018-08-09 00:00:00	no	no	inUse	2017-03-02 00:00:00		1	\N	\N	\N
277	T.H - 02 / M1830011	4	7	1	52	2018-01-22 00:00:00	2019-01-21 00:00:00	no	no	inUse	2018-02-23 08:55:17.688		4	\N	\N	\N
609	FT-01	12	18	1	52	2017-02-09 00:00:00	2018-02-08 00:00:00	no	no	collected	2018-02-26 10:58:15.222		1	\N	\N	\N
236	TH-42 / 7DF20024	3	23	41	52	2018-02-20 00:00:00	2019-02-19 00:00:00	no	no	inUse	2018-02-28 17:34:21.504		6	\N	\N	\N
137	PP-21 / K100310	1	1	6	52	2018-02-19 00:00:00	2019-02-18 00:00:00	no	no	inUse	2018-05-16 09:08:34.133		7	\N	\N	\N
288	MULT-17	13	19	1	52	2017-01-27 00:00:00	2018-01-26 00:00:00	no	no	registered	2018-03-05 11:03:36.625		1	\N	\N	\N
350	ETF-03/3233	8	14	1	52	2017-03-09 00:00:00	2018-03-08 00:00:00	no	no	registered	2018-03-05 11:04:04.07		2	\N	\N	\N
146	VOLT-04	7	25	16	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2017-11-23 00:00:00		2	\N	\N	\N
233	VOLT-22	7	26	43	52	2018-03-15 00:00:00	2019-03-14 00:00:00	no	no	collected	2018-03-07 15:48:06.778		4	\N	\N	\N
296	PAQ-06 / BG003241	14	20	4	75	2017-01-20 00:00:00	2018-06-29 00:00:00	no	no	inUse	2017-01-20 00:00:00		1	\N	\N	\N
242	TH-48 / 7DE902157	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:31:04.718		3	\N	\N	\N
588	HPT-09 / 3201093	5	24	13	52	2018-05-04 00:00:00	2019-05-03 00:00:00	no	no	inCalibration	2018-05-08 13:49:38.255		3	\N	\N	\N
57	VOLT-01	7	25	17	52	2018-03-15 00:00:00	2019-03-14 00:00:00	no	no	inUse	2018-04-30 11:34:27.697		6	\N	\N	\N
693	P. TEMP. - 01	2	174	1	52	2017-01-26 00:00:00	2018-01-25 00:00:00	no	no	collected	2018-02-20 17:13:19.429		2	\N	\N	\N
194	VOLT-19/1203372403	7	13	43	52	2017-05-11 00:00:00	2018-05-10 00:00:00	no	no	collected	2018-04-30 11:40:02.294		5	\N	\N	\N
125	PM-47 / D101124	1	3	67	52	2017-10-18 00:00:00	2018-10-17 00:00:00	no	no	inUse	2017-10-18 00:00:00	\N	0	\N	\N	\N
155	TH-36 / 7DBC00321	3	6	59	52	2017-05-15 00:00:00	2018-05-14 00:00:00	no	no	inUse	2017-05-15 00:00:00	\N	0	\N	\N	\N
152	TH-33 / 7DBC00313	3	6	65	52	2017-05-15 00:00:00	2018-05-14 00:00:00	no	no	inUse	2017-05-15 00:00:00	\N	0	\N	\N	\N
172	101231 - BTA 1	63	122	73	27	2017-01-31 00:00:00	2017-08-08 00:00:00	no	no		2017-01-31 00:00:00	\N	0	\N	\N	\N
173	101232 - BTA 2	63	122	73	27	2017-01-31 00:00:00	2017-08-08 00:00:00	no	no		2017-01-31 00:00:00	\N	0	\N	\N	\N
175	101228 - BTA 4	63	123	73	27	2017-01-31 00:00:00	2017-08-08 00:00:00	no	no		2017-01-31 00:00:00	\N	0	\N	\N	\N
177	101227 - BTA 6	63	123	73	27	2017-01-31 00:00:00	2017-08-08 00:00:00	no	no		2017-01-31 00:00:00	\N	0	\N	\N	\N
604	8406 / PM 23	64	124	13	52	2013-07-17 00:00:00	2014-07-16 00:00:00	yes	no	collected	2013-07-17 00:00:00	\N	0	\N	\N	\N
605	8407 / PM 24	64	124	13	52	2013-07-17 00:00:00	2014-07-16 00:00:00	yes	no	collected	2013-07-17 00:00:00	\N	0	\N	\N	\N
606	8427 / PM 25	64	124	13	52	2013-07-17 00:00:00	2014-07-16 00:00:00	yes	no	collected	2013-07-17 00:00:00	\N	0	\N	\N	\N
617	500000/PM-17	65	125	43	52	2010-10-20 00:00:00	2011-10-19 00:00:00	yes	no	collected	2010-10-20 00:00:00	\N	0	\N	\N	\N
310	503491/PM-19	65	125	43	52	2010-10-20 00:00:00	2011-10-19 00:00:00	yes	no	collected	2010-10-20 00:00:00	\N	0	\N	\N	\N
202	HTP-02	54	24	71	27	2017-01-07 00:00:00	2017-07-15 00:00:00	no	no	dropOff	2018-03-01 09:51:30.867		1	\N	\N	\N
212	202694/TERM-45	58	119	71	27	2018-01-09 00:00:00	2018-07-17 00:00:00	no	no	inUse	2017-07-05 00:00:00		2	\N	\N	\N
33	BI-02	52	114	71	52	2017-03-21 00:00:00	2018-03-20 00:00:00	no	no	registered	2018-01-22 20:23:26.508		3	\N	\N	\N
213	202692/TERM-47	58	119	71	27	2018-01-09 00:00:00	2018-07-17 00:00:00	no	no	inUse	2017-07-05 00:00:00		2	\N	\N	\N
43	TH-25 / 7DB600086	3	6	45	52	2018-02-20 00:00:00	2019-02-19 00:00:00	no	no	inUse	2018-02-28 17:37:46.687		6	\N	\N	\N
204	202691/TERM-50	58	119	71	27	2018-01-09 00:00:00	2018-07-17 00:00:00	no	no	inUse	2017-07-05 00:00:00		2	\N	\N	\N
259	TERMI-01 \\ 60307101	51	112	70	52	2016-10-06 00:00:00	2017-10-05 00:00:00	no	no	inCalibration	2018-01-10 13:35:31.838		1	\N	\N	\N
256	TH-53 \\ 7DE901121	3	6	43	52	2017-05-15 00:00:00	2018-05-14 00:00:00	no	no	collected	2018-04-17 17:09:11.319		1	\N	\N	\N
701	TH-23 / 09C0181	3	6	9	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inUse	2018-04-06 11:10:02.112		4	\N	\N	\N
279	TH-54 \\ 7E0800228	3	23	67	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inUse	2018-04-09 14:01:59.948		7	\N	\N	\N
246	PIN-01/140027839	50	110	12	52	2017-05-19 00:00:00	2018-05-18 00:00:00	no	no	inUse	2017-05-19 00:00:00		1	\N	\N	\N
217	BI-03	59	116	71	52	2018-03-22 00:00:00	2019-03-21 00:00:00	no	no	inUse	2017-01-04 00:00:00		5	\N	\N	\N
49	TH-30 / 7DB600083	3	6	52	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2018-01-24 18:50:49.513		2	\N	\N	\N
218	BI-04	59	116	71	52	2018-03-23 00:00:00	2019-03-22 00:00:00	no	no	inUse	2017-01-06 00:00:00		5	\N	\N	\N
45	TH-26 / 7DB600098	3	6	7	52	2017-11-29 00:00:00	2018-11-28 00:00:00	no	no	inUse	2017-11-29 00:00:00		2	\N	\N	\N
31	BI-01	52	113	71	52	2017-03-20 00:00:00	2018-03-19 00:00:00	no	no	registered	2018-01-22 20:20:08.619		4	\N	\N	\N
214	BI-01	59	113	71	52	2018-03-20 00:00:00	2019-03-19 00:00:00	no	no	inUse	2016-12-30 00:00:00		13	\N	\N	\N
234	BP-06	62	114	72	27	2018-03-29 00:00:00	2018-10-04 00:00:00	no	no	inUse	2017-09-04 00:00:00		2	\N	\N	\N
219	BI-05	59	116	71	52	2018-03-24 00:00:00	2019-03-23 00:00:00	no	no	inUse	2017-01-15 00:00:00		5	\N	\N	\N
32	BI-03	53	116	71	52	2017-03-23 00:00:00	2018-03-22 00:00:00	no	no	registered	2018-01-22 20:19:07.595		2	\N	\N	\N
34	BI-04	53	116	71	54	2017-03-23 00:00:00	2018-04-05 00:00:00	no	no	registered	2018-01-22 20:24:57.383		2	\N	\N	\N
235	BP-07	62	121	72	27	2018-03-28 00:00:00	2018-10-03 00:00:00	no	no	inUse	2017-09-04 00:00:00		2	\N	\N	\N
35	BI-05	53	116	71	27	2016-04-27 00:00:00	2016-11-02 00:00:00	no	no	registered	2018-03-06 13:15:38.811		2	\N	\N	\N
1003	ION 8650 / 1309A694-01	60	120	71	52	2017-10-21 00:00:00	2018-10-20 00:00:00	no	no	registered	2018-01-10 13:09:18.976		2	\N	\N	\N
293	CES-03/0031984	49	108	69	52	2018-04-16 00:00:00	2019-04-15 00:00:00	no	no	inUse	2018-04-19 15:49:45.723		6	\N	\N	\N
569	ED 8273	52	115	12	27	2016-02-16 00:00:00	2016-08-23 00:00:00	no	no	registered	2018-03-06 13:15:22.702		3	\N	\N	\N
244	ESP-01 / AF1412	51	111	33	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2018-02-19 11:28:03.784		3	\N	\N	\N
252	TH-50 / 7DE902109	3	6	51	52	2018-01-19 00:00:00	2019-01-18 00:00:00	no	no	inUse	2018-02-20 14:03:29.486		5	\N	\N	\N
251	CES-02/0046051	49	109	21	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2018-02-16 10:57:03.683		4	\N	\N	\N
215	BI-02	59	114	71	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inUse	2017-01-02 00:00:00		6	\N	\N	\N
224	TH-41 / 7DBC00319	3	23	43	52	2018-04-27 00:00:00	2019-04-26 00:00:00	no	no	collected	2018-04-05 09:09:31.556	*UMIDADE	6	\N	\N	\N
46	TH-27 / 7DB600088	3	6	11	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:29:21.175		4	\N	\N	\N
258	TH-52 \\ 7DE901217	3	6	64	52	2017-05-15 00:00:00	2018-05-14 00:00:00	no	no	inUse	2017-05-15 00:00:00		1	\N	\N	\N
50	TH-31 / 7DB600099	3	6	44	52	2018-02-20 00:00:00	2019-02-19 00:00:00	no	no	inUse	2018-02-28 17:21:00.512		6	\N	\N	\N
207	BI-02	55	117	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		1	\N	\N	\N
208	BI-03	56	116	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		1	\N	\N	\N
210	BI-05	56	116	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		1	\N	\N	\N
209	BI-04	56	116	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		1	\N	\N	\N
206	BI-01	55	117	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		1	\N	\N	\N
40	BM-01	61	116	72	27	2017-12-26 00:00:00	2018-07-03 00:00:00	no	no	inUse	2017-06-30 00:00:00		1	\N	\N	\N
211	ZERA	57	118	71	26	2018-03-02 00:00:00	2018-08-31 00:00:00	no	no	collected	2018-03-07 11:13:51.917		4	\N	\N	\N
238	TH-44 / 7DE902112	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:28:01.647		3	\N	\N	\N
243	TH-49 7DE902097	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:33:51.374		3	\N	\N	\N
39	BP-05	62	114	72	27	2017-12-16 00:00:00	2018-06-23 00:00:00	no	no	inUse	2017-07-14 00:00:00		1	\N	\N	\N
41	BM-02	61	116	72	27	2017-12-27 00:00:00	2018-07-04 00:00:00	no	no	inUse	2017-07-31 00:00:00		1	\N	\N	\N
132	BM-04	61	116	72	27	2017-12-28 00:00:00	2018-07-05 00:00:00	no	no	inUse	2017-07-31 00:00:00		1	\N	\N	\N
133	BM-05	61	116	72	27	2018-02-02 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-15 00:00:00		1	\N	\N	\N
38	BP-01	62	114	72	27	2017-12-15 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-17 00:00:00		1	\N	\N	\N
37	BP-02	62	114	72	27	2017-12-22 00:00:00	2018-06-29 00:00:00	no	no	inUse	2017-06-30 00:00:00		1	\N	\N	\N
130	BP-04	62	114	72	27	2017-11-25 00:00:00	2018-06-02 00:00:00	no	no	inUse	2017-06-27 00:00:00		1	\N	\N	\N
280	TH-55 \\ 7E0800225	3	6	43	52	2018-02-20 00:00:00	2019-02-19 00:00:00	no	no	collected	2018-04-17 18:20:01.398		7	\N	\N	\N
328	500003/PM-18	64	125	43	52	2010-08-31 00:00:00	2011-08-30 00:00:00	yes	no	collected	2010-08-31 00:00:00	\N	0	\N	\N	\N
438	503788/PM-10	65	125	43	52	2010-08-19 00:00:00	2011-08-18 00:00:00	yes	no	collected	2010-08-19 00:00:00	\N	0	\N	\N	\N
615	503790/PM-16	65	125	43	52	2010-08-19 00:00:00	2011-08-18 00:00:00	yes	no	collected	2010-08-19 00:00:00	\N	0	\N	\N	\N
432	503489/PM-05	65	125	43	52	2010-06-07 00:00:00	2011-06-06 00:00:00	yes	no	collected	2010-06-07 00:00:00	\N	0	\N	\N	\N
165	505130 / PM 49	64	124	43	52	2012-05-30 00:00:00	2013-05-29 00:00:00	yes	no	collected	2012-05-30 00:00:00	\N	0	\N	\N	\N
167	505131 / PM 50	64	124	43	52	2012-05-30 00:00:00	2013-05-29 00:00:00	yes	no	collected	2012-05-30 00:00:00	\N	0	\N	\N	\N
168	505132 / PM 51	64	124	43	52	2012-05-30 00:00:00	2013-05-29 00:00:00	yes	no	collected	2012-05-30 00:00:00	\N	0	\N	\N	\N
229	505515/PM-01	65	125	43	52	2010-06-07 00:00:00	2011-06-06 00:00:00	yes	no	collected	2010-06-07 00:00:00	\N	0	\N	\N	\N
433	506431/PM-06	65	125	13	52	2009-06-30 00:00:00	2010-06-29 00:00:00	yes	no	collected	2009-06-30 00:00:00	\N	0	\N	\N	\N
434	506433/PM-07	65	125	13	52	2009-06-30 00:00:00	2010-06-29 00:00:00	yes	no	collected	2009-06-30 00:00:00	\N	0	\N	\N	\N
385	503490/PM 02	65	125	13	52	2009-10-01 00:00:00	2010-09-30 00:00:00	yes	no	collected	2009-10-01 00:00:00	\N	0	\N	\N	\N
436	500001/PM-08	65	125	13	52	2009-06-30 00:00:00	2010-06-29 00:00:00	yes	no	collected	2009-06-30 00:00:00	\N	0	\N	\N	\N
430	503488/PM-03	65	125	13	52	2009-05-13 00:00:00	2010-05-12 00:00:00	yes	no	collected	2009-05-13 00:00:00	\N	0	\N	\N	\N
431	506428/PM-04	65	125	13	52	2009-05-13 00:00:00	2010-05-12 00:00:00	yes	no	collected	2009-05-13 00:00:00	\N	0	\N	\N	\N
563	505516/PM-14	65	125	13	52	2009-10-01 00:00:00	2010-09-30 00:00:00	yes	no	collected	2009-10-01 00:00:00	\N	0	\N	\N	\N
596	505130/PM-20	67	124	75	52	2009-07-06 00:00:00	2010-07-05 00:00:00	yes	no	collected	2009-07-06 00:00:00	\N	0	\N	\N	\N
597	505131/PM-21	67	124	75	52	2009-07-06 00:00:00	2010-07-05 00:00:00	yes	no	collected	2009-07-06 00:00:00	\N	0	\N	\N	\N
598	505132/PM-22	67	124	75	52	2009-07-06 00:00:00	2010-07-05 00:00:00	yes	no	collected	2009-07-06 00:00:00	\N	0	\N	\N	\N
687	202694/TERM-45	68	119	13	52	2013-07-18 00:00:00	2014-07-17 00:00:00	yes	no	collected	2013-07-18 00:00:00	\N	0	\N	\N	\N
689	202692/TERM-47	68	119	13	52	2013-07-18 00:00:00	2014-07-17 00:00:00	yes	no	collected	2013-07-18 00:00:00	\N	0	\N	\N	\N
372	TERM-01	68	126	76	52	2012-04-19 00:00:00	2013-04-18 00:00:00	yes	no	collected	2012-04-19 00:00:00	\N	0	\N	\N	\N
383	TERM-02	68	126	77	52	2012-04-19 00:00:00	2013-04-18 00:00:00	yes	no	collected	2012-04-19 00:00:00	\N	0	\N	\N	\N
380	TERM-05	68	126	78	52	2012-04-18 00:00:00	2013-04-17 00:00:00	yes	no	collected	2012-04-18 00:00:00	\N	0	\N	\N	\N
378	TERM-07	68	126	43	52	2011-04-05 00:00:00	2012-04-03 00:00:00	yes	no	collected	2011-04-05 00:00:00	\N	0	\N	\N	\N
376	TERM-08	68	126	43	52	2011-04-05 00:00:00	2012-04-03 00:00:00	yes	no	collected	2011-04-05 00:00:00	\N	0	\N	\N	\N
382	TERM-04	68	126	74	52	2011-02-25 00:00:00	2012-02-24 00:00:00	yes	no	collected	2011-02-25 00:00:00	\N	0	\N	\N	\N
655	TERM-44	68	126	9	52	2013-04-13 00:00:00	2014-04-12 00:00:00	yes	no	collected	2013-04-13 00:00:00	\N	0	\N	\N	\N
377	TERM-09	68	126	43	52	2010-02-23 00:00:00	2011-02-22 00:00:00	yes	no	collected	2010-02-23 00:00:00	\N	0	\N	\N	\N
1051	TERM-43	68	126	43	52	2010-02-23 00:00:00	2011-02-22 00:00:00	yes	no	collected	2010-02-23 00:00:00	\N	0	\N	\N	\N
373	TERM-12	68	126	43	52	2010-02-23 00:00:00	2011-02-22 00:00:00	yes	no	collected	2010-02-23 00:00:00	\N	0	\N	\N	\N
375	TERM-10	68	126	43	52	2010-02-23 00:00:00	2011-02-22 00:00:00	yes	no	collected	2010-02-23 00:00:00	\N	0	\N	\N	\N
706	16112742 - Multimed 1	69	127	43	52	2010-09-28 00:00:00	2011-09-27 00:00:00	yes	no	collected	2010-09-28 00:00:00	\N	0	\N	\N	\N
613	16072335 - Multimed 2	70	127	74	52	2011-02-10 00:00:00	2012-02-09 00:00:00	yes	no	collected	2011-02-10 00:00:00	\N	0	\N	\N	\N
1004	16072335 - Multimed 2	69	127	74	52	2010-10-20 00:00:00	2011-10-19 00:00:00	yes	no	collected	2010-10-20 00:00:00	\N	0	\N	\N	\N
1005	26066401 - Multimed 3	69	127	43	52	2011-06-08 00:00:00	2012-06-06 00:00:00	yes	no	collected	2011-06-08 00:00:00	\N	0	\N	\N	\N
707	26066401 - Multimed 3	70	127	43	52	2011-04-15 00:00:00	2012-04-13 00:00:00	yes	no	collected	2011-04-15 00:00:00	\N	0	\N	\N	\N
610	AJT086 / FT - 01	12	18	80	52	2011-11-18 00:00:00	2012-11-16 00:00:00	yes	no	collected	2011-11-18 00:00:00	\N	0	\N	\N	\N
163	VOLT-19	7	130	43	52	2013-01-23 00:00:00	2014-01-22 00:00:00	yes	no	collected	2013-01-23 00:00:00	\N	0	\N	\N	\N
74	VOLT-14	7	12	81	52	2012-11-08 00:00:00	2013-11-07 00:00:00	yes	no	collected	2012-11-08 00:00:00	\N	0	\N	\N	\N
64	VOLT-05	7	12	3	52	2014-01-20 00:00:00	2015-01-19 00:00:00	yes	no	collected	2014-01-20 00:00:00	\N	0	\N	\N	\N
65	VOLT-06	7	12	3	52	2014-01-20 00:00:00	2015-01-19 00:00:00	yes	no	collected	2014-01-20 00:00:00	\N	0	\N	\N	\N
67	VOLT-08	7	12	13	52	2013-01-23 00:00:00	2014-01-22 00:00:00	yes	no	collected	2013-01-23 00:00:00	\N	0	\N	\N	\N
68	VOLT-09	7	12	18	52	2012-10-31 00:00:00	2013-10-30 00:00:00	yes	no	collected	2012-10-31 00:00:00	\N	0	\N	\N	\N
71	VOLT-11	7	12	82	52	2013-01-16 00:00:00	2014-01-15 00:00:00	yes	no	collected	2013-01-16 00:00:00	\N	0	\N	\N	\N
73	VOLT-13	7	12	83	52	2013-01-16 00:00:00	2014-01-15 00:00:00	yes	no	collected	2013-01-16 00:00:00	\N	0	\N	\N	\N
99	VOLT-17	7	12	13	52	2012-11-08 00:00:00	2013-11-07 00:00:00	yes	yes	collected	2012-11-08 00:00:00	\N	0	\N	\N	\N
77	VOLT-15	7	12	84	52	2013-07-22 00:00:00	2014-07-21 00:00:00	yes	no	collected	2013-07-22 00:00:00	\N	0	\N	\N	\N
78	VOLT-16	7	12	81	52	2013-07-22 00:00:00	2014-07-21 00:00:00	yes	no	collected	2013-07-22 00:00:00	\N	0	\N	\N	\N
72	VOLT-12	7	12	85	52	2013-07-22 00:00:00	2014-07-21 00:00:00	yes	no	collected	2013-07-22 00:00:00	\N	0	\N	\N	\N
205	6002350-LOG 01	71	131	43	52	2013-01-22 00:00:00	2014-01-21 00:00:00	yes	no	collected	2013-01-22 00:00:00	\N	0	\N	\N	\N
678	TH - 18 / 38770	4	132	13	52	2011-12-20 00:00:00	2012-12-18 00:00:00	yes	no	collected	2011-12-20 00:00:00	\N	0	\N	\N	\N
676	TH - 16 / 38763	4	132	13	52	2011-12-20 00:00:00	2012-12-18 00:00:00	yes	no	collected	2011-12-20 00:00:00	\N	0	\N	\N	\N
679	TH - 19 / 38772	4	132	13	52	2013-07-24 00:00:00	2014-07-23 00:00:00	yes	no	collected	2013-07-24 00:00:00	\N	0	\N	\N	\N
680	TH - 20 / 38774	4	132	13	52	2013-07-24 00:00:00	2014-07-23 00:00:00	yes	no	collected	2013-07-24 00:00:00	\N	0	\N	\N	\N
671	TH - 11 / 38777	4	132	13	52	2013-04-13 00:00:00	2014-04-12 00:00:00	yes	no	collected	2013-04-13 00:00:00	\N	0	\N	\N	\N
542	506430/PM-13	66	125	13	52	2009-10-01 00:00:00	2010-09-30 00:00:00	yes	no	dropOff	2018-01-10 17:50:48.52		1	\N	\N	\N
564	503737/PM-15	66	125	13	52	2009-12-22 00:00:00	2010-12-21 00:00:00	yes	no	dropOff	2018-01-10 17:51:11.744		1	\N	\N	\N
592	BM 06	61	128	79	27	2012-10-15 00:00:00	2013-04-22 00:00:00	yes	no	registered	2018-02-05 11:58:29.775		1	\N	\N	\N
593	BP 10	62	129	79	27	2012-05-31 00:00:00	2012-12-06 00:00:00	yes	no	collected	2012-05-31 00:00:00		1	\N	\N	\N
670	TH - 10 / 38771	4	132	13	52	2012-02-01 00:00:00	2013-01-30 00:00:00	yes	no	collected	2012-02-01 00:00:00		1	\N	\N	\N
66	VOLT-07	7	12	12	52	2014-01-20 00:00:00	2015-01-19 00:00:00	no	no	collected	2014-01-20 00:00:00		2	\N	\N	\N
675	TH-15 / 38767	4	132	43	52	2018-04-16 00:00:00	2019-04-15 00:00:00	no	no	collected	2012-03-15 00:00:00		2	\N	\N	\N
677	TH - 17 / 38775	4	132	13	52	2014-04-29 00:00:00	2015-04-28 00:00:00	yes	no	collected	2014-04-29 00:00:00	\N	0	\N	\N	\N
685	C TEMP - 01	36	133	43	52	2010-09-23 00:00:00	2011-09-22 00:00:00	yes	no	collected	2010-09-23 00:00:00	\N	0	\N	\N	\N
200	C TEMP - 09	36	134	3	52	1900-01-01 00:00:00	1900-01-01 00:00:00	yes	no	collected	1900-01-01 00:00:00	\N	0	\N	\N	\N
81	C TEMP - 03	36	134	89	52	2013-03-25 00:00:00	2014-03-24 00:00:00	yes	no	collected	2013-03-25 00:00:00	\N	0	\N	\N	\N
82	C TEMP - 04	36	134	90	52	2013-01-23 00:00:00	2014-01-22 00:00:00	yes	no	collected	2013-01-23 00:00:00	\N	0	\N	\N	\N
84	C TEMP - 06	36	134	91	52	2013-10-25 00:00:00	2014-10-24 00:00:00	yes	no	collected	2013-10-25 00:00:00	\N	0	\N	\N	\N
607	G4F006	72	135	43	52	2009-09-15 00:00:00	2010-09-14 00:00:00	yes	no	collected	2009-09-15 00:00:00	\N	0	\N	\N	\N
595	15976	73	136	79	52	2010-10-21 00:00:00	2011-10-20 00:00:00	yes	no	collected	2010-10-21 00:00:00	\N	0	\N	\N	\N
109	1166-TORQUE.06	27	54	73	52	2011-11-04 00:00:00	2012-11-02 00:00:00	yes	no	dropOff	2011-11-04 00:00:00	\N	0	\N	\N	\N
106	1165-TORQUE.04	27	54	43	52	2013-01-15 00:00:00	2014-01-14 00:00:00	yes	no	collected	2013-01-15 00:00:00	\N	0	\N	\N	\N
108	1165-TORQUE.05	27	54	43	52	2013-01-15 00:00:00	2014-01-14 00:00:00	yes	no	collected	2013-01-15 00:00:00	\N	0	\N	\N	\N
681	/ HPT-19	5	138	13	52	2009-11-13 00:00:00	2010-11-12 00:00:00	yes	no	collected	2009-11-13 00:00:00	\N	0	\N	\N	\N
174	101229 - BTA 3	63	123	73	27	2012-10-16 00:00:00	2013-04-23 00:00:00	yes	no	collected	2012-10-16 00:00:00	\N	0	\N	\N	\N
176	101230 - BTA 5	63	123	73	27	2012-10-16 00:00:00	2013-04-23 00:00:00	yes	no	collected	2012-10-16 00:00:00	\N	0	\N	\N	\N
118	HTP-11 / 109611060G	5	105	92	52	2014-02-11 00:00:00	2015-02-10 00:00:00	yes	no	collected	2014-02-11 00:00:00	\N	0	\N	\N	\N
122	109611068G-HTP-14	5	105	13	52	2013-07-20 00:00:00	2014-07-19 00:00:00	yes	no	collected	2013-07-20 00:00:00	\N	0	\N	\N	\N
612	850 - MULT-04	11	139	93	52	2011-09-20 00:00:00	2012-09-18 00:00:00	yes	no	collected	2011-09-20 00:00:00	\N	0	\N	\N	\N
17	14850129- MULT-07	11	94	95	52	2012-05-22 00:00:00	2013-05-21 00:00:00	yes	no	collected	2012-05-22 00:00:00	\N	0	\N	\N	\N
18	14850127-MULT-08	11	94	95	52	2012-11-05 00:00:00	2013-11-04 00:00:00	yes	no	collected	2012-11-05 00:00:00	\N	0	\N	\N	\N
190	24060467 Mult-13	11	94	13	52	2013-07-22 00:00:00	2014-07-21 00:00:00	yes	no	collected	2013-07-22 00:00:00	\N	0	\N	\N	\N
590	H23024 - PM 29	75	140	79	52	2011-12-07 00:00:00	2012-12-05 00:00:00	yes	no	collected	2011-12-07 00:00:00	\N	0	\N	\N	\N
699	K29022 - PP 06	75	141	79	52	2012-05-07 00:00:00	2013-05-06 00:00:00	yes	no	collected	2012-05-07 00:00:00	\N	0	\N	\N	\N
379	TERM-06	68	126	43	52	2014-05-02 00:00:00	2015-05-01 00:00:00	yes	no	collected	2014-05-02 00:00:00	\N	0	\N	\N	\N
381	TERM-03	68	126	43	52	2014-05-02 00:00:00	2015-05-01 00:00:00	yes	no	collected	2014-05-02 00:00:00	\N	0	\N	\N	\N
384	TERM-13	68	126	43	52	2014-05-02 00:00:00	2015-05-01 00:00:00	yes	no	collected	2014-05-02 00:00:00	\N	0	\N	\N	\N
374	TERM-11	68	126	43	52	2014-05-02 00:00:00	2015-05-01 00:00:00	yes	no	collected	2014-05-02 00:00:00	\N	0	\N	\N	\N
437	505514/PM-09	65	125	13	52	2013-02-21 00:00:00	2014-02-20 00:00:00	yes	no	collected	2013-02-21 00:00:00	\N	0	\N	\N	\N
541	503765/PM-12	65	125	13	52	2013-02-21 00:00:00	2014-02-20 00:00:00	yes	no	collected	2013-02-21 00:00:00	\N	0	\N	\N	\N
187	1210960051-HTP-17	5	105	98	52	2014-07-07 00:00:00	2015-07-06 00:00:00	yes	no	collected	2014-07-07 00:00:00	\N	0	\N	\N	\N
424	88360208/ MULT-03	11	97	13	52	2014-07-18 00:00:00	2015-07-17 00:00:00	yes	no	collected	2014-07-18 00:00:00	\N	0	\N	\N	\N
19	TORQ.02	25	142	21	75	2014-08-21 00:00:00	2016-01-28 00:00:00	yes	no	collected	2014-08-21 00:00:00	\N	0	\N	\N	\N
142	CRT / 04.12	17	143	21	75	2015-09-24 00:00:00	2017-03-02 00:00:00	yes	no	collected	2015-09-24 00:00:00	\N	0	\N	\N	\N
240	6039434	71	131	43	52	2006-09-11 00:00:00	2007-09-10 00:00:00	yes	no	collected	2006-09-11 00:00:00	\N	0	\N	\N	\N
51	TH-32 / 7DB600089	3	6	43	52	2014-07-16 00:00:00	2015-07-15 00:00:00	yes	yes	collected	2014-07-16 00:00:00	\N	0	\N	\N	\N
591	H29008 - PP 05	75	141	79	52	2011-12-07 00:00:00	2012-12-05 00:00:00	yes	yes	collected	2011-12-07 00:00:00	\N	0	\N	\N	\N
698	J23020 - PM 28	75	140	79	52	2011-05-04 00:00:00	2012-05-02 00:00:00	yes	yes	collected	2011-05-04 00:00:00	\N	0	\N	\N	\N
429	500002	65	125	43	52	2008-07-25 00:00:00	2009-07-24 00:00:00	yes	no	collected	2008-07-25 00:00:00	\N	0	\N	\N	\N
1054	CRA .016.01LP	16	146	21	52	2010-10-18 00:00:00	2011-10-17 00:00:00	yes	no	collected	2010-10-18 00:00:00	\N	0	\N	\N	\N
1008	CRA .016.01LNP	16	146	21	52	2010-10-18 00:00:00	2011-10-17 00:00:00	yes	no	collected	2010-10-18 00:00:00	\N	0	\N	\N	\N
1055	CRT 013.01	17	147	21	52	2010-10-18 00:00:00	2011-10-17 00:00:00	yes	no	collected	2010-10-18 00:00:00	\N	0	\N	\N	\N
1053	505516	79	145	99	51	2004-09-24 00:00:00	2005-09-16 00:00:00	yes	no	dropOff	2017-12-29 15:42:14.451		1	\N	\N	\N
117	109611057G-HTP-10	5	105	43	52	2014-07-07 00:00:00	2015-07-06 00:00:00	yes	no	collected	2014-07-07 00:00:00		1	\N	\N	\N
608	H5C125	74	137	43	52	2008-06-26 00:00:00	2009-06-25 00:00:00	yes	no	collected	2008-06-26 00:00:00		1	\N	\N	\N
427	HPT-01 / 3201093	5	24	43	52	2012-04-24 00:00:00	2013-04-23 00:00:00	yes	yes	collected	2012-04-24 00:00:00		3	\N	\N	\N
160	ALT-02 / 1000624	78	44	12	75	2017-08-23 00:00:00	2019-01-30 00:00:00	yes	yes	collected	2017-08-23 00:00:00		1	\N	\N	\N
1052	TH-01 / Q102265	4	103	43	52	2008-06-18 00:00:00	2009-06-17 00:00:00	yes	no	collected	2008-06-18 00:00:00		1	\N	\N	\N
237	TH-43 / 7DF200025	3	23	76	52	2016-03-14 00:00:00	2017-03-13 00:00:00	yes	no	collected	2016-03-14 00:00:00		1	\N	\N	\N
695	K090908- PP 03	1	1	3	52	2011-01-10 00:00:00	2012-01-09 00:00:00	yes	no	collected	2011-01-10 00:00:00		1	\N	\N	\N
216	ZERA	59	118	71	52	2017-03-25 00:00:00	2018-03-24 00:00:00	yes	no	collected	2014-11-15 00:00:00		3	\N	\N	\N
83	C TEMP - 05	36	169	25	52	2018-01-25 00:00:00	2019-01-24 00:00:00	no	no	inUse	2018-03-08 12:04:00.524		2	\N	\N	\N
703	HPT-20 / 3202406	5	24	13	52	2018-05-04 00:00:00	2019-05-03 00:00:00	yes	no	inCalibration	2018-05-08 13:05:08.269		2	\N	\N	\N
711	RA / LYE567	21	45	21	75	2015-02-18 00:00:00	2016-07-27 00:00:00	yes	no	collected	2015-02-18 00:00:00		1	\N	\N	\N
1007	PP-1001	66	145	75	52	1990-03-28 00:00:00	1991-03-27 00:00:00	yes	no	registered	2018-02-15 18:55:57.937		2	\N	\N	\N
673	TH-13 / 38765	4	132	43	52	2018-04-16 00:00:00	2019-04-15 00:00:00	no	no	collected	2014-04-29 00:00:00		2	\N	\N	\N
672	TH - 12 / 38770	4	132	13	52	2018-02-21 00:00:00	2019-02-20 00:00:00	no	no	collected	2014-04-29 00:00:00		2	\N	\N	\N
201	CT-10	36	169	27	52	2017-08-28 00:00:00	2018-08-27 00:00:00	no	no	inUse	2018-03-08 11:57:26.101		3	\N	\N	\N
674	TH - 14 / 38769	4	132	4	52	2018-02-21 00:00:00	2019-02-20 00:00:00	no	no	inUse	2018-03-08 12:53:26.106		3	\N	\N	\N
86	CT-08	36	171	27	52	2017-08-29 00:00:00	2018-08-28 00:00:00	no	no	inUse	2018-03-08 12:00:38.449		3	\N	\N	\N
85	CT-07	36	169	25	52	2018-01-25 00:00:00	2019-01-24 00:00:00	no	no	inUse	2018-03-08 12:57:56.788		7	\N	\N	\N
686	C TEMP - 02	36	169	25	52	2018-01-03 00:00:00	2019-01-02 00:00:00	yes	no	registered	2018-03-08 13:18:00.8		2	\N	\N	\N
197	24280339-MULT-13	11	94	94	52	1900-01-01 00:00:00	1900-12-31 00:00:00	yes	no	registered	2018-04-09 16:13:58.054		1	\N	\N	\N
710	08.50/30	80	48	21	75	2010-11-05 00:00:00	2012-04-13 00:00:00	yes	yes	collected	2010-11-05 00:00:00	\N	0	\N	\N	\N
36	CRA 017.01LP	16	148	43	75	2010-11-05 00:00:00	2012-04-13 00:00:00	yes	no	collected	2010-11-05 00:00:00	\N	0	\N	\N	\N
1009	CRA 017.01 LNP	16	148	43	75	2010-11-05 00:00:00	2012-04-13 00:00:00	yes	no	collected	2010-11-05 00:00:00	\N	0	\N	\N	\N
196	08.76/06	21	45	21	75	2009-09-28 00:00:00	2011-03-07 00:00:00	yes	no	collected	2009-09-28 00:00:00	\N	0	\N	\N	\N
265	08.76/20	21	45	43	75	2011-02-18 00:00:00	2012-07-27 00:00:00	yes	no	collected	2011-02-18 00:00:00	\N	0	\N	\N	\N
79	08.76/22	21	45	21	75	2009-05-25 00:00:00	2010-11-01 00:00:00	yes	no	collected	2009-05-25 00:00:00	\N	0	\N	\N	\N
179	ETB110003010- MULT-09	11	149	93	52	2013-01-23 00:00:00	2014-01-22 00:00:00	yes	no	collected	2013-01-23 00:00:00	\N	0	\N	\N	\N
185	85570119/MULT-1	11	97	13	52	2013-07-22 00:00:00	2014-07-21 00:00:00	yes	no	collected	2013-07-22 00:00:00	\N	0	\N	\N	\N
180	ETB110003906- MULT-10	11	149	93	52	2013-01-23 00:00:00	2014-01-22 00:00:00	yes	no	collected	2013-01-23 00:00:00	\N	0	\N	\N	\N
691	202693/TERM-49	68	119	102	52	2014-04-30 00:00:00	2015-04-29 00:00:00	yes	no	collected	2014-04-30 00:00:00	\N	0	\N	\N	\N
692	202691/TERM-50	68	119	102	52	2014-04-30 00:00:00	2015-04-29 00:00:00	yes	no	collected	2014-04-30 00:00:00	\N	0	\N	\N	\N
688	202695/TERM-46	68	119	102	52	2014-11-13 00:00:00	2015-11-12 00:00:00	yes	no	collected	2014-11-13 00:00:00	\N	0	\N	\N	\N
690	202696/TERM-48	68	119	102	52	2014-11-13 00:00:00	2015-11-12 00:00:00	yes	no	collected	2014-11-13 00:00:00	\N	0	\N	\N	\N
148	VOLT-18	7	150	18	52	2014-11-03 00:00:00	2015-11-02 00:00:00	yes	yes	collected	2014-11-03 00:00:00	\N	0	\N	\N	\N
645	PP-01 / 01-249-5	15	22	12	52	2014-11-10 00:00:00	2015-11-09 00:00:00	yes	no	collected	2014-11-10 00:00:00	\N	0	\N	\N	\N
96	CRT / 05.11	17	152	12	75	2016-02-17 00:00:00	2017-07-26 00:00:00	yes	no	collected	2016-02-17 00:00:00	\N	0	\N	\N	\N
364	BPE-02	81	153	104	52	1900-01-01 00:00:00	1900-01-01 00:00:00	yes	no	collected	1900-01-01 00:00:00	\N	0	\N	\N	\N
395	CRT / 11961/17	18	39	21	75	2017-09-28 00:00:00	2019-03-07 00:00:00	no	no	inUse	2017-09-28 00:00:00	\N	0	\N	\N	\N
396	CRT / 11962/17	18	39	21	75	2017-09-28 00:00:00	2019-03-07 00:00:00	no	no	inUse	2017-09-28 00:00:00	\N	0	\N	\N	\N
397	CRT / 11963/17	18	40	21	75	2017-09-28 00:00:00	2019-03-07 00:00:00	no	no	inUse	2017-09-28 00:00:00	\N	0	\N	\N	\N
398	CRT / 11964/17	18	40	21	75	2017-09-28 00:00:00	2019-03-07 00:00:00	no	no	inUse	2017-09-28 00:00:00	\N	0	\N	\N	\N
156	CTL / 07.12	19	41	21	75	2017-08-23 00:00:00	2019-01-30 00:00:00	no	no	inUse	2017-08-23 00:00:00	\N	0	\N	\N	\N
149	PAQ / 08.39/73	20	20	21	75	2017-08-23 00:00:00	2019-01-30 00:00:00	no	no	inUse	2017-08-23 00:00:00	\N	0	\N	\N	\N
250	PAQ-04/ 0075795	14	44	21	75	2016-11-28 00:00:00	2018-05-07 00:00:00	no	no	inUse	2016-11-28 00:00:00	\N	0	\N	\N	\N
144	RC-06.12	23	47	12	75	2016-08-12 00:00:00	2018-01-19 00:00:00	yes	no	collected	2018-01-10 10:15:58.296		2	\N	\N	\N
12	RC 01.11	23	47	21	75	2017-03-02 00:00:00	2018-08-09 00:00:00	yes	no	collected	2018-01-10 10:16:35.556		2	\N	\N	\N
162	RC-07.12	23	47	22	75	2016-08-05 00:00:00	2018-01-12 00:00:00	yes	no	collected	2018-01-10 10:15:15.453		2	\N	\N	\N
1056	TH-46 7DE902119	3	6	101	52	2015-03-26 00:00:00	2016-03-24 00:00:00	yes	yes	collected	2015-03-26 00:00:00		1	\N	\N	\N
159	PP-24 / H090303	1	1	101	0	2016-07-04 00:00:00	2016-07-04 00:00:00	yes	no	inRepair	2018-03-21 11:05:38.405		1	\N	\N	\N
303	TORQ - 03 / 800428	28	58	69	75	2017-11-24 00:00:00	2019-05-03 00:00:00	no	no	inUse	2018-01-12 16:26:22.668		5	\N	\N	\N
709	MCR / 08.50/64	24	48	21	75	2016-10-20 00:00:00	2018-03-29 00:00:00	yes	no	collected	2018-01-10 10:11:35.5		2	\N	\N	\N
363	BPE-01 / 101221	81	113	1	52	2018-01-10 00:00:00	2019-01-09 00:00:00	no	no	registered	2018-01-10 21:09:00.039		3	\N	\N	\N
145	PAQ-02	20	167	21	75	2018-03-14 00:00:00	2019-08-21 00:00:00	no	no	inUse	2018-03-20 12:45:18.838		6	\N	\N	\N
365	BPE-03	81	166	1	52	2016-10-27 00:00:00	2017-10-26 00:00:00	yes	no	collected	2016-10-27 00:00:00		1	\N	\N	\N
23	PP-08 / K101217	1	21	108	52	2017-12-27 00:00:00	2018-12-26 00:00:00	no	no	collected	2018-05-23 09:13:05.095		11	\N	\N	\N
550	PAQ-13 / 06904252	14	20	21	75	2018-04-27 00:00:00	2019-10-04 00:00:00	no	no	inUse	2018-05-04 16:07:39.688		5	\N	\N	\N
110	TORQ - 07	27	54	21	75	2017-11-23 00:00:00	2019-05-02 00:00:00	yes	no	collected	2018-01-12 18:40:57.29		4	\N	\N	\N
359	TORQ -15 / 0CR006601	28	57	21	75	2017-12-21 00:00:00	2019-05-30 00:00:00	no	no	inUse	2017-07-04 00:00:00		3	\N	\N	\N
702	TH-24 /  09C0183	3	6	43	75	2016-06-09 00:00:00	2017-11-16 00:00:00	yes	yes	collected	2016-06-09 00:00:00		5	\N	\N	\N
269	LUX-01/29354	31	62	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2017-01-24 00:00:00		2	\N	\N	\N
195	08.39/55	14	20	21	75	2012-02-07 00:00:00	2013-07-16 00:00:00	yes	no	registered	2018-03-20 12:46:54.006		2	\N	\N	\N
267	DOS-02	29	60	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2017-01-24 00:00:00		8	\N	\N	\N
182	TH-38 / 7DBC00314	3	6	76	52	2016-10-21 00:00:00	2017-10-20 00:00:00	yes	no	collected	2016-10-21 00:00:00		1	\N	\N	\N
131	BM-03	61	151	72	27	2017-12-27 00:00:00	2018-07-04 00:00:00	yes	no	registered	2018-03-07 22:07:46.424		3	\N	\N	\N
304	TH-59 / 09C0180	3	6	13	52	1900-01-01 00:00:00	1900-12-31 00:00:00	yes	yes	collected	1900-01-01 00:00:00		2	\N	\N	\N
225	08.39/45	14	20	21	75	2009-06-30 00:00:00	2010-12-07 00:00:00	yes	no	registered	2018-03-20 12:47:27.93		1	\N	\N	\N
111	TORQ - 08 / 1169	26	54	43	75	2017-01-19 00:00:00	2018-06-28 00:00:00	yes	no	collected	2018-01-09 16:28:50.313		3	\N	\N	\N
353	TORQ - 14 / 0DP013938	28	57	21	75	2017-12-21 00:00:00	2019-05-30 00:00:00	no	no	inUse	2017-07-04 00:00:00		3	\N	\N	\N
281	TORQ-13/309524G	28	56	21	75	2017-08-23 00:00:00	2019-01-30 00:00:00	no	no	inUse	2017-08-23 00:00:00		1	\N	\N	\N
112	TORQ - 09	26	53	21	75	2017-08-23 00:00:00	2019-01-30 00:00:00	no	no	inUse	2017-08-23 00:00:00		1	\N	\N	\N
262	TORQ-11/ 1604620052	25	52	21	75	2018-03-07 00:00:00	2019-08-14 00:00:00	no	no	inUse	2018-04-04 10:35:58.354		4	\N	\N	\N
266	DOS-01/140201184	29	59	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2017-01-24 00:00:00		6	\N	\N	\N
191	PAQ-03	14	43	21	75	2016-09-12 00:00:00	2018-02-19 00:00:00	no	yes	collected	2018-02-20 16:30:29.989		1	\N	\N	\N
268	MST-01	30	61	23	104	2017-03-20 00:00:00	2019-03-18 00:00:00	no	no	inUse	2017-03-20 00:00:00		1	\N	\N	\N
387	ALT-01/03007181	78	44	12	75	2016-05-02 00:00:00	2017-10-09 00:00:00	yes	yes	collected	2016-05-02 00:00:00		2	\N	\N	\N
141	TORQ-10 /6JK100045	25	51	21	75	2017-11-24 00:00:00	2019-05-03 00:00:00	no	no	inUse	2018-01-19 10:01:56.773		3	\N	\N	\N
143	RA / 05.12	21	45	21	75	2016-08-12 00:00:00	2018-01-19 00:00:00	no	no	dropOff	2018-03-01 11:24:29.381		1	\N	\N	\N
90	PAQ-01 / 9819738	20	42	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 10:03:20.905		4	\N	\N	\N
151	08.39/49	14	20	21	75	2012-02-07 00:00:00	2013-07-16 00:00:00	yes	no	registered	2018-03-20 12:47:41.183		1	\N	\N	\N
1	CTL / 14.407	19	41	21	75	2018-04-02 00:00:00	2019-09-09 00:00:00	no	no	inUse	2018-05-08 10:18:09.987		2	\N	\N	\N
161	MCR-01	24	50	21	75	2016-10-20 00:00:00	2018-03-29 00:00:00	no	no	collected	2018-03-28 19:47:27.809		1	\N	\N	\N
248	MCR-02/120880831	24	49	21	75	2018-04-09 00:00:00	2019-09-16 00:00:00	no	no	inUse	2018-04-10 15:26:11.164		5	\N	\N	\N
331	Injetora2	35	67	25	52	2017-08-21 00:00:00	2018-08-20 00:00:00	no	no	inUse	2017-08-21 00:00:00	\N	0	\N	\N	\N
332	Injetora3	35	68	25	52	2017-08-21 00:00:00	2018-08-20 00:00:00	no	no	inUse	2017-08-21 00:00:00	\N	0	\N	\N	\N
333	Injetora4	35	68	25	52	2017-08-28 00:00:00	2018-08-27 00:00:00	no	no	inUse	2017-08-28 00:00:00	\N	0	\N	\N	\N
334	Injetora5	35	69	25	52	2017-08-04 00:00:00	2018-08-03 00:00:00	no	no	inUse	2017-08-04 00:00:00	\N	0	\N	\N	\N
335	Injetora6	35	70	25	52	2017-08-09 00:00:00	2018-08-08 00:00:00	no	no	inUse	2017-08-09 00:00:00	\N	0	\N	\N	\N
336	Injetora7	35	71	25	52	2017-08-17 00:00:00	2018-08-16 00:00:00	no	no	inUse	2017-08-17 00:00:00	\N	0	\N	\N	\N
337	Injetora8	35	72	25	52	2017-08-03 00:00:00	2018-08-02 00:00:00	no	no	inUse	2017-08-03 00:00:00	\N	0	\N	\N	\N
338	Injetora9	35	73	25	52	2017-08-24 00:00:00	2018-08-23 00:00:00	no	no	inUse	2017-08-24 00:00:00	\N	0	\N	\N	\N
339	Injetora10	35	73	25	52	2017-08-25 00:00:00	2018-08-24 00:00:00	no	no	inUse	2017-08-25 00:00:00	\N	0	\N	\N	\N
340	Injetora11	35	74	25	52	2017-08-08 00:00:00	2018-08-07 00:00:00	no	no	inUse	2017-08-08 00:00:00	\N	0	\N	\N	\N
341	Injetora12	35	75	25	52	2017-08-22 00:00:00	2018-08-21 00:00:00	no	no	inUse	2017-08-22 00:00:00	\N	0	\N	\N	\N
342	Injetora13	35	76	25	52	2017-08-23 00:00:00	2018-08-22 00:00:00	no	no	inUse	2017-08-23 00:00:00	\N	0	\N	\N	\N
1006	G4F006	74	135	43	52	2009-11-17 00:00:00	2010-11-16 00:00:00	yes	no	dropOff	2017-12-29 15:40:11.977		1	\N	\N	\N
1059	PP-35 / C140409	65	155	106	52	2017-12-22 00:00:00	2018-12-21 00:00:00	no	no	inUse	2017-12-29 21:01:22.44	THS	4	\N	\N	\N
203	202693/TERM-49	58	119	71	27	2018-01-09 00:00:00	2018-07-17 00:00:00	no	no	inUse	2017-07-05 00:00:00		2	\N	\N	\N
264	MULT.P-02 /4015909	33	64	24	52	2000-01-01 00:00:00	2000-12-30 00:00:00	no	no	registered	2018-01-10 13:15:15.246		2	\N	\N	\N
540	503492/PM-11	66	125	74	52	2010-11-05 00:00:00	2011-11-04 00:00:00	yes	no	dropOff	2018-01-10 17:49:56.041		1	\N	\N	\N
1060	BPE-04/1012442	81	153	1	52	2018-01-10 00:00:00	2019-01-09 00:00:00	no	no	registered	2018-01-10 21:10:16.04		1	\N	\N	\N
1073	MULT-16	39	94	21	52	2018-03-14 00:00:00	2019-03-13 00:00:00	no	no	inUse	2018-03-26 10:17:22.028		4	\N	\N	\N
1030	T-01	41	84	21	72	2017-12-04 00:00:00	2019-04-22 00:00:00	no	no	inUse	2017-03-07 00:00:00		1	\N	\N	\N
347	CT-04	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		2	\N	\N	\N
301	Tridimensional/00221402	42	85	21	52	2018-03-16 00:00:00	2019-03-15 00:00:00	no	no	inUse	2018-04-03 15:40:28.496		3	\N	\N	\N
352	Testador2	37	79	76	52	2018-04-06 00:00:00	2019-04-05 00:00:00	no	no	inUse	2018-04-18 18:00:17.493		2	\N	\N	\N
357	TH-56	38	81	21	52	2017-01-20 00:00:00	2018-01-19 00:00:00	no	no	dropOff	2018-02-28 08:19:53.906		1	\N	\N	\N
275	TORQ-12/T045360	28	55	21	75	2015-06-01 00:00:00	2016-11-07 00:00:00	no	no	registered	2018-03-07 17:46:36.498		4	\N	\N	\N
330	Injetora1	35	66	25	52	2017-08-01 00:00:00	2018-07-31 00:00:00	no	no	inUse	2017-08-01 00:00:00		2	\N	\N	\N
1062	TH-61 / HT208-6048	82	158	68	75	2018-02-21 00:00:00	2019-07-31 00:00:00	no	no	inUse	2018-02-27 10:11:01.828		5	\N	\N	\N
354	Perfil-1/S2K-01125	40	168	69	52	2018-04-04 00:00:00	2019-04-03 00:00:00	no	no	inUse	2018-05-08 09:26:26.644		5	\N	\N	\N
305	RA -121.342/G303168	22	46	21	75	2017-01-23 00:00:00	2018-07-02 00:00:00	yes	no	collected	2018-01-10 10:14:35.654		3	\N	\N	\N
120	HTP-12 / 109611058G	5	105	39	52	2016-11-30 00:00:00	2017-11-29 00:00:00	yes	no	collected	2018-01-06 16:08:33.691		2	\N	\N	\N
29	PM-34 / D110124	1	3	62	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	inUse	2018-04-04 08:35:14.449	Sala instrumentação	10	\N	\N	\N
257	TH-51 \\ 7DE902118	3	6	54	52	2018-01-19 00:00:00	2019-01-18 00:00:00	no	no	inUse	2018-02-20 14:02:12.8		4	\N	\N	\N
245	CES-01/0032144	49	108	69	52	2018-04-13 00:00:00	2019-04-12 00:00:00	no	no	inUse	2018-04-19 15:49:22.708		3	\N	\N	\N
1071	TH-62 / 604468	82	132	31	75	2018-02-23 00:00:00	2019-08-02 00:00:00	no	no	inUse	2018-04-03 10:35:51.613		3	\N	\N	\N
425	HPT-02 / 3201194	5	24	71	52	2017-03-14 00:00:00	2018-03-13 00:00:00	no	no	inUse	2018-05-16 14:07:36.017		3	\N	\N	\N
1065	PAQ-12 / 71130329308	14	154	21	75	2017-02-05 00:00:00	2018-07-15 00:00:00	no	no	registered	2018-04-03 13:08:04.969		4	\N	\N	\N
1063	PAQ-09 / D12841	20	42	66	75	2018-01-08 00:00:00	2019-06-17 00:00:00	no	no	inUse	2018-02-19 12:03:03.883		5	\N	\N	\N
358	TH-57	38	108	21	52	2017-01-20 00:00:00	2018-01-19 00:00:00	no	no	dropOff	2018-02-28 08:20:15.659		2	\N	\N	\N
300	DH48S-S	34	65	12	52	2016-10-27 00:00:00	2017-10-26 00:00:00	no	no	registered	2018-03-02 17:57:41.005		1	\N	\N	\N
263	MULT.P-01 /1273581	33	64	24	52	2016-05-13 00:00:00	2017-05-12 00:00:00	no	no	registered	2018-03-05 10:59:44.925		1	\N	\N	\N
270	CAC-01/655473	32	63	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2018-05-14 09:55:58.517		4	\N	\N	\N
1072	TH-63/604421	4	132	31	75	2018-02-23 00:00:00	2019-08-02 00:00:00	no	no	inUse	2018-04-03 15:42:09.34		3	\N	\N	\N
1058	PAQ-07 / 71130329308	14	154	21	75	2018-03-01 00:00:00	2019-08-08 00:00:00	no	no	inCalibration	2018-03-08 15:28:44.49		5	\N	\N	\N
1061	TH-60 / HT208-6044	82	158	26	75	2018-02-21 00:00:00	2019-07-31 00:00:00	no	no	inUse	2018-03-07 17:12:49.747		6	\N	\N	\N
61	PM-38 / D1011032	1	3	63	52	2017-12-11 00:00:00	2018-12-10 00:00:00	no	no	inUse	2018-04-09 12:37:29.3		2	\N	\N	\N
345	CT-02	36	169	25	52	2017-08-25 00:00:00	2018-08-24 00:00:00	no	no	inUse	2017-08-11 00:00:00		4	\N	\N	\N
1001	Injetora15	35	77	25	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	inUse	2017-08-02 00:00:00		3	\N	\N	\N
348	CT-05	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		2	\N	\N	\N
1064	PAQ-11 / 14824436	20	42	66	75	2018-01-08 00:00:00	2019-06-17 00:00:00	no	no	inUse	2018-02-19 11:57:05.444		5	\N	\N	\N
344	CT-01	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		2	\N	\N	\N
349	CT-06	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		2	\N	\N	\N
343	Injetora14	35	75	25	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	inUse	2017-08-02 00:00:00		1	\N	\N	\N
346	CT-03	36	169	25	52	2017-08-23 00:00:00	2018-08-22 00:00:00	no	no	inUse	2017-08-11 00:00:00		3	\N	\N	\N
1069	MULT-23 / 22030278	11	94	66	75	2018-01-17 00:00:00	2019-06-26 00:00:00	no	no	inUse	2018-03-08 11:09:16.495		3	\N	\N	\N
1070	BI-06	59	155	71	52	2018-03-26 00:00:00	2019-03-25 00:00:00	no	no	inUse	2018-03-07 11:11:15.644		4	\N	\N	\N
88	PM-41 / D100946	1	3	1	52	2017-11-17 00:00:00	2018-11-16 00:00:00	no	no	inUse	2017-12-06 00:00:00		2	\N	\N	\N
239	TH-45 / 7DE902117	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:27:37.932		4	\N	\N	\N
360	MULT-15 / 40920080	39	94	70	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	registered	2018-03-05 11:01:33.81		4	\N	\N	\N
47	TH-28 / 7DB600099	3	6	43	52	2018-02-20 00:00:00	2019-02-19 00:00:00	yes	yes	collected	2018-04-09 14:01:16.949		8	\N	\N	\N
1075	ESP-02 / BAG-27	51	111	33	52	2018-03-09 00:00:00	2019-03-08 00:00:00	no	no	collected	2018-03-21 12:40:29.412		2	\N	\N	\N
1074	MULT-24 / 30780021	39	172	1	52	2018-03-26 00:00:00	2019-03-25 00:00:00	no	no	inUse	2018-04-11 12:45:16.501		4	\N	\N	\N
1076	TH-64 / 2016380128	4	175	31	52	2018-04-18 00:00:00	2019-04-17 00:00:00	no	no	collected	2018-04-18 19:20:59.869		3	\N	\N	\N
1077	ESF-01	84	176	111	104	2018-05-11 00:00:00	2020-05-08 00:00:00	no	no	registered	2018-05-15 09:31:43.232		1	\N	\N	\N
1078	ESF-02	84	177	111	104	2017-05-08 00:00:00	2019-05-06 00:00:00	no	no	registered	2018-05-15 09:34:36.097		1	\N	\N	\N
1079	ESF-03	84	176	111	104	2017-05-08 00:00:00	2019-05-06 00:00:00	no	no	registered	2018-05-15 09:35:49.479		1	\N	\N	\N
1081	TEMP-2	85	178	111	104	2018-05-07 00:00:00	2020-05-04 00:00:00	no	no	registered	2018-05-15 09:41:53.926		2	\N	\N	\N
1080	TEMP-1	85	178	111	104	2017-05-05 00:00:00	2019-05-03 00:00:00	no	no	registered	2018-05-15 09:40:55.275		2	\N	\N	\N
299	HPT-15 / 3201433	5	24	110	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2018-02-16 11:16:51.765		3	\N	\N	\N
1082	Ab	10	131	110	10	2018-06-11 00:00:00	2018-08-20 00:00:00	no	no	registered	2018-06-11 10:52:23.693	útima Tentiva 2	3	\N	2	\N
1083	ABCDSDAFSDFASDFFF	6	10	31	123	2022-06-30 00:00:00	2024-11-07 00:00:00	yes	yes	registered	2018-06-11 11:08:01.363		1	\N	1	\N
1068	VOLT-26	7	68	19	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inUse	2018-04-30 11:35:40.434		6	\N	2	\N
355	Testador1	37	79	76	52	2018-03-06 00:00:00	2019-03-05 00:00:00	no	no	inUse	2018-03-13 12:19:14.446		3	\N	3	\N
1066	VOLT-24	7	97	14	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inUse	2018-03-13 15:29:33.592		5	\N	2	\N
287	MP-01	77	144	112	52	2016-10-27 00:00:00	2017-10-26 00:00:00	yes	yes	dropOff	2018-06-11 12:50:03.967		2	\N	2	\N
1084	zTest z	87	179	112	200	2027-06-01 00:00:00	2031-04-01 00:00:00	no	no	dropOff	2018-06-11 12:53:17.858	zTest zzTest zzTest z	6	\N	3	\N
1067	VOLT-25 	7	106	18	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inUse	2018-03-13 09:11:49.716		5	\N	1	\N
1085	TESTE	87	165	48	999	2028-01-01 00:00:00	2047-02-23 00:00:00	no	yes	dropOff	2018-06-11 12:55:40.961	fffffffffffffffffffffffffffffffffffffffffffff	3	\N	3	\N
94	PP-17 / K110428	1	1	48	52	2018-04-20 00:00:00	2019-04-19 00:00:00	no	no	inUse	2018-05-16 08:49:27.269		6	\N	3	\N
1086	1 Test 1	10	105	31	101	2018-06-01 00:00:00	2020-05-08 00:00:00	no	no	registered	2018-06-11 13:20:46.272	1 Test 1	4	\N	2	\N
1087	126	44	86	111	999	2018-06-14 00:00:00	2037-08-06 00:00:00	no	no	registered	2018-06-14 15:06:16.091		1	\N	1	\N
1088	ddddddddddddddddddd	44	86	111	108	2018-06-20 00:00:00	2020-07-15 00:00:00	yes	yes	registered	2018-06-14 15:19:46.591	sadf	1	\N	1	\N
1089	123456789	45	10	93	99	2019-06-21 00:00:00	2021-05-14 00:00:00	no	no	registered	2018-06-14 15:21:37.975		1	\N	2	\N
1090	123456	19	119	42	70	2018-06-30 00:00:00	2019-11-02 00:00:00	no	yes	registered	2018-06-14 15:30:44.975		1	\N	2	\N
115	PM-44 / D100602	1	3	64	52	2017-10-19 00:00:00	2018-10-18 00:00:00	no	no	inUse	2017-10-19 00:00:00		1	\N	\N	1
1091	11111111111111	76	26	6	111	2018-06-21 00:00:00	2020-08-06 00:00:00	no	no	registered	2018-06-14 17:09:38.979	111111111111	1	\N	1	2
1092	5	10	12	31	5	2018-06-28 00:00:00	2018-08-02 00:00:00	no	no	registered	2018-06-14 17:37:23.89		1	\N	2	\N
1093	6	87	86	111	666	2018-06-13 00:00:00	2031-03-19 00:00:00	no	no	registered	2018-06-14 17:39:16.459		1	\N	1	6
3	BD-02	43	87	28	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	inUse	2017-09-22 00:00:00		1	\N	\N	1
1094	126	56	107	3	111	2018-06-06 00:00:00	2020-07-22 00:00:00	no	no	registered	2018-06-14 18:44:49.247		1	\N	2	7
\.


--
-- TOC entry 2304 (class 0 OID 49394)
-- Dependencies: 191
-- Data for Name: tb_measuring_instrument_aud; Type: TABLE DATA; Schema: mecsw; Owner: postgres
--

COPY tb_measuring_instrument_aud (code, specification_identifier, measuring_instrument_type_code, measuring_instrument_model_code, measuring_instrument_place_use_code, calibration_frequency_weeks, last_calibration_date, next_calibration_date, obsolete_identifier, damaged_identifier, state_identifier, state_timestamp, description, code_review, type_review, measuring_instrument_department_code, calibration_laboratory_code) FROM stdin;
193	PP-26 / K130202	1	1	53	52	2017-11-23 00:00:00	2018-11-22 00:00:00	yes	yes	inUse	2017-11-23 00:00:00		23	1	\N	\N
193	PP-26 / K130202	1	1	53	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2017-11-23 00:00:00		24	1	\N	\N
305	RA -121.342	22	46	21	75	2017-01-23 00:00:00	2018-07-02 00:00:00	yes	no	inUse	2017-01-23 00:00:00		88	1	\N	\N
120	HTP-12 / 109611058G	5	105	39	52	2016-11-30 00:00:00	2017-11-29 00:00:00	yes	no	inUse	2016-11-30 00:00:00		89	1	\N	\N
162	RC-07.12	23	47	22	75	2016-08-05 00:00:00	2018-01-12 00:00:00	yes	no	inUse	2016-08-05 00:00:00		90	1	\N	\N
144	RC-06.12	23	47	12	75	2016-08-12 00:00:00	2018-01-19 00:00:00	yes	no	inUse	2016-08-12 00:00:00		91	1	\N	\N
709	MCR / 08.50/64	24	48	21	75	2016-10-20 00:00:00	2018-03-29 00:00:00	yes	no	inUse	2016-10-20 00:00:00		92	1	\N	\N
12	RC 01.11	23	47	21	75	2017-03-02 00:00:00	2018-08-09 00:00:00	yes	no	inUse	2017-03-02 00:00:00		93	1	\N	\N
6	11191504 / BD-05	43	90	30	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	inUse	2017-09-22 00:00:00		94	1	\N	\N
1006	G4F006	74	135	43	52	2009-11-17 00:00:00	2010-11-16 00:00:00	yes	no	dropOff	2017-12-29 15:40:11.977		95	1	\N	\N
1053	505516	79	145	99	51	2004-09-24 00:00:00	2005-09-16 00:00:00	yes	no	dropOff	2017-12-29 15:42:14.451		96	1	\N	\N
712	PM-30 / D091001	1	3	43	52	2016-11-04 00:00:00	2017-11-03 00:00:00	no	no	collected	2017-12-29 15:47:30.9		97	1	\N	\N
712	PM-30 / D091001	1	3	43	52	2016-11-04 00:00:00	2017-11-03 00:00:00	no	no	inCalibration	2017-12-29 15:48:16.652		98	1	\N	\N
712	PM-30 / D091001	1	3	43	52	2017-12-28 00:00:00	2018-12-27 00:00:00	no	no	collected	2017-12-29 15:49:46.564		99	1	\N	\N
1058	PAQ-07 / 71130329308	14	154	21	52	2017-12-28 00:00:00	2018-12-27 00:00:00	no	no	registered	2017-12-29 19:30:37.477		178	0	\N	\N
1058	PAQ-07 / 71130329308	14	154	21	52	2017-12-28 00:00:00	2018-12-27 00:00:00	no	no	collected	2017-12-29 19:33:06.726		179	1	\N	\N
569	ED 8273	52	115	12	27	2016-02-16 00:00:00	2016-08-23 00:00:00	no	no	inUse	2017-12-29 19:36:49.79		180	1	\N	\N
189	DM 9070334	45	98	12	52	2016-12-12 00:00:00	2017-12-11 00:00:00	no	no	inCalibration	2016-12-12 00:00:00		181	1	\N	\N
1059	PP-35/C140409	65	155	12	52	2017-12-22 00:00:00	2018-12-21 00:00:00	no	no	registered	2017-12-29 20:59:48.356	Padrão da bancada de inspeção BI-06	259	0	\N	\N
1059	PP-35/C140409	65	155	12	52	2017-12-22 00:00:00	2018-12-21 00:00:00	no	no	inUse	2017-12-29 21:01:22.44	Padrão da bancada de inspeção BI-06	267	1	\N	\N
29	PM-34 / D110124	1	3	2	52	2016-12-14 00:00:00	2017-12-13 00:00:00	no	no	collected	2018-01-03 21:30:54.983		319	1	\N	\N
23	PP-08 / K101217	1	1	43	52	2016-12-15 00:00:00	2017-12-14 00:00:00	no	no	collected	2018-01-03 21:31:39.004		320	1	\N	\N
124	PM-46 / D101039	1	3	43	52	2017-12-06 00:00:00	2018-12-05 00:00:00	no	no	collected	2018-01-03 21:32:27.398		321	1	\N	\N
30	PP-10 / K101214	1	1	43	52	2017-12-18 00:00:00	2018-12-17 00:00:00	no	no	collected	2018-01-03 21:32:59.579		322	1	\N	\N
230	PP-30 / K100616	1	2	43	52	2016-12-01 00:00:00	2017-11-30 00:00:00	no	no	collected	2018-01-03 21:37:26.285		323	1	\N	\N
27	PM-32 / D110127	1	3	11	52	2017-12-14 00:00:00	2018-12-13 00:00:00	no	no	collected	2018-01-03 21:38:09.995		324	1	\N	\N
91	PP-14 / K110502	1	1	43	52	2017-12-19 00:00:00	2018-12-18 00:00:00	no	no	collected	2018-01-03 21:38:38.12		325	1	\N	\N
61	PM-38 / D1011032	1	3	43	52	2017-12-11 00:00:00	2018-12-10 00:00:00	no	no	collected	2018-01-03 21:39:15.125		326	1	\N	\N
29	PM-34 / D110124	1	3	2	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	collected	2018-01-03 21:30:54.983		327	1	\N	\N
23	PP-08 / K101217	1	1	43	52	2017-12-27 00:00:00	2018-12-26 00:00:00	no	no	collected	2018-01-03 21:31:39.004		328	1	\N	\N
230	PP-30 / K100616	1	2	43	52	2017-12-27 00:00:00	2018-12-26 00:00:00	no	no	collected	2018-01-03 21:37:26.285		329	1	\N	\N
345	CT-2	36	78	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		331	1	\N	\N
29	PM-34 / D110124	1	3	2	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	inUse	2018-01-06 16:01:50.729		332	1	\N	\N
29	PM-34 / D110124	1	3	2	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	collected	2018-01-06 16:04:05.088		333	1	\N	\N
29	PM-34 / D110124	1	3	2	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	collected	2018-01-06 16:04:05.088	Sala instrumentação	334	1	\N	\N
120	HTP-12 / 109611058G	5	105	39	52	2016-11-30 00:00:00	2017-11-29 00:00:00	yes	no	dropOff	2018-01-06 16:08:33.691		335	1	\N	\N
121	HTP-13 / 109608214G	5	105	97	52	2016-11-30 00:00:00	2017-11-29 00:00:00	no	no	inCalibration	2016-11-30 00:00:00		336	1	\N	\N
117	109611057G-HTP-10	5	105	43	52	2014-07-07 00:00:00	2015-07-06 00:00:00	yes	no	collected	2014-07-07 00:00:00		337	1	\N	\N
228	PP-29 / K090801	1	2	2	52	2016-12-21 00:00:00	2017-12-20 00:00:00	no	no	inUse	2016-12-28 00:00:00		338	1	\N	\N
228	PP-29 / K090801	1	2	2	52	2017-12-21 00:00:00	2018-12-20 00:00:00	no	no	inUse	2016-12-28 00:00:00		339	1	\N	\N
253	AMP-01	6	9	5	52	2017-11-01 00:00:00	2018-10-31 00:00:00	no	no	inUse	2017-11-23 00:00:00		340	1	\N	\N
253	AMP-01	6	9	5	52	2017-12-01 00:00:00	2018-11-30 00:00:00	no	no	inUse	2017-11-23 00:00:00		341	1	\N	\N
283	AMP-04	6	11	2	52	2017-12-01 00:00:00	2018-11-30 00:00:00	no	no	inUse	2017-11-30 00:00:00		342	1	\N	\N
214	BI-01	59	113	71	52	2016-12-17 00:00:00	2017-12-16 00:00:00	no	no	inUse	2016-12-30 00:00:00		343	1	\N	\N
214	BI-01	59	113	71	52	2017-10-17 00:00:00	2018-10-16 00:00:00	no	no	inUse	2016-12-30 00:00:00		344	1	\N	\N
203	202693/TERM-49	58	119	71	27	2017-12-05 00:00:00	2018-06-12 00:00:00	no	no	inUse	2017-07-05 00:00:00		345	1	\N	\N
204	202691/TERM-50	58	119	71	27	2017-12-05 00:00:00	2018-06-12 00:00:00	no	no	inUse	2017-07-05 00:00:00		346	1	\N	\N
212	202694/TERM-45	58	119	71	27	2017-12-05 00:00:00	2018-06-12 00:00:00	no	no	inUse	2017-07-05 00:00:00		347	1	\N	\N
212	202694/TERM-45	58	119	71	27	2018-01-09 00:00:00	2018-07-17 00:00:00	no	no	inUse	2017-07-05 00:00:00		350	1	\N	\N
213	202692/TERM-47	58	119	71	27	2018-01-09 00:00:00	2018-07-17 00:00:00	no	no	inUse	2017-07-05 00:00:00		351	1	\N	\N
213	202692/TERM-47	58	119	71	27	2017-12-05 00:00:00	2018-06-12 00:00:00	no	no	inUse	2017-07-05 00:00:00		348	1	\N	\N
204	202691/TERM-50	58	119	71	27	2018-01-09 00:00:00	2018-07-17 00:00:00	no	no	inUse	2017-07-05 00:00:00		349	1	\N	\N
203	202693/TERM-49	58	119	71	27	2018-01-09 00:00:00	2018-07-17 00:00:00	no	no	inUse	2017-07-05 00:00:00		352	1	\N	\N
139	PP-22 / K100614	1	1	46	52	2017-01-19 00:00:00	2018-01-18 00:00:00	no	no	collected	2018-01-09 14:01:46.032		353	1	\N	\N
23	PP-08 / K101217	1	1	46	52	2017-12-27 00:00:00	2018-12-26 00:00:00	no	no	inUse	2018-01-09 14:03:55.226		354	1	\N	\N
139	PP-22 / K100614	1	1	13	52	2017-01-19 00:00:00	2018-01-18 00:00:00	no	no	collected	2018-01-09 14:01:46.032		355	1	\N	\N
92	PP-15 / K110401	1	1	43	52	2017-01-23 00:00:00	2018-01-22 00:00:00	no	no	collected	2018-01-09 14:07:31.049		356	1	\N	\N
91	PP-14 / K110502	1	1	51	52	2017-12-19 00:00:00	2018-12-18 00:00:00	no	no	inUse	2018-01-09 14:08:59.864		357	1	\N	\N
111	TORQUE -08	26	54	43	75	2017-01-19 00:00:00	2018-06-28 00:00:00	no	no	collected	2018-01-09 16:28:50.313		379	1	\N	\N
709	MCR / 08.50/64	24	48	21	75	2016-10-20 00:00:00	2018-03-29 00:00:00	yes	no	collected	2018-01-10 10:11:35.5		380	1	\N	\N
305	RA -121.342	22	46	21	75	2017-01-23 00:00:00	2018-07-02 00:00:00	yes	no	collected	2018-01-10 10:14:35.654		381	1	\N	\N
162	RC-07.12	23	47	22	75	2016-08-05 00:00:00	2018-01-12 00:00:00	yes	no	collected	2018-01-10 10:15:15.453		382	1	\N	\N
144	RC-06.12	23	47	12	75	2016-08-12 00:00:00	2018-01-19 00:00:00	yes	no	collected	2018-01-10 10:15:58.296		383	1	\N	\N
12	RC 01.11	23	47	21	75	2017-03-02 00:00:00	2018-08-09 00:00:00	yes	no	collected	2018-01-10 10:16:35.556		384	1	\N	\N
1003	S/N 3376419	60	120	71	52	2017-01-15 00:00:00	2018-01-14 00:00:00	no	no	registered	2018-01-10 13:09:18.976		385	1	\N	\N
264	MULT.P-02 /4015909	33	64	24	52	1900-01-01 00:00:00	1900-12-31 00:00:00	no	no	registered	2018-01-10 13:15:15.246		386	1	\N	\N
264	MULT.P-02 /4015909	33	64	24	52	2000-01-01 00:00:00	2000-12-30 00:00:00	no	no	registered	2018-01-10 13:15:15.246		387	1	\N	\N
277	T.H - 02	4	7	1	52	2016-08-09 00:00:00	2017-08-08 00:00:00	no	no	inCalibration	2016-08-09 00:00:00		388	1	\N	\N
259	TERMI-01 \\ 60307101	51	112	70	52	2016-10-06 00:00:00	2017-10-05 00:00:00	no	no	inCalibration	2018-01-10 13:35:31.838		389	1	\N	\N
449	SEFAR	48	102	12	52	2016-12-19 00:00:00	2017-12-18 00:00:00	no	no	collected	2018-01-10 16:57:41.218		390	1	\N	\N
1002	HPT-02	5	106	76	0	1900-01-01 00:00:00	1900-01-01 00:00:00	no	no	inCalibration	2018-01-10 17:28:02.748		391	1	\N	\N
1002	HPT-02	5	106	76	0	1900-01-01 00:00:00	1900-01-01 00:00:00	no	no	collected	2018-01-10 17:36:40.14		392	1	\N	\N
540	503492/PM-11	66	125	74	52	2010-11-05 00:00:00	2011-11-04 00:00:00	yes	no	dropOff	2018-01-10 17:49:56.041		393	1	\N	\N
542	506430/PM-13	66	125	13	52	2009-10-01 00:00:00	2010-09-30 00:00:00	yes	no	dropOff	2018-01-10 17:50:48.52		394	1	\N	\N
564	503737/PM-15	66	125	13	52	2009-12-22 00:00:00	2010-12-21 00:00:00	yes	no	dropOff	2018-01-10 17:51:11.744		395	1	\N	\N
608	H5C125	74	137	43	52	2008-06-26 00:00:00	2009-06-25 00:00:00	yes	no	collected	2008-06-26 00:00:00		396	1	\N	\N
303	TORQ -03 / 800428	28	58	69	75	2017-11-24 00:00:00	2019-05-03 00:00:00	no	no	collected	2018-01-10 18:13:49.976		397	1	\N	\N
303	TORQ - 03 / 800428	28	58	69	75	2017-11-24 00:00:00	2019-05-03 00:00:00	no	no	collected	2018-01-10 18:13:49.976		398	1	\N	\N
303	TORQ - 03 / 800428	28	58	69	52	2012-06-22 00:00:00	2013-06-21 00:00:00	no	no	collected	2018-01-10 18:13:49.976		399	1	\N	\N
427	3201093/ HPT-01	5	24	76	52	2012-04-24 00:00:00	2013-04-23 00:00:00	yes	yes	collected	2012-04-24 00:00:00		402	1	\N	\N
704	HPT-05 / 3201091	5	24	12	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	collected	2018-01-10 19:33:28.735		403	1	\N	\N
11	HPT - 21 / 3202231	5	24	36	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-23 00:00:00		404	1	\N	\N
588	HPT-09 / 3201093	5	24	12	52	2017-02-03 00:00:00	2018-02-02 00:00:00	no	no	inUse	2017-02-03 00:00:00		405	1	\N	\N
101	PP-18 / K100403	1	1	61	52	2017-12-18 00:00:00	2018-12-17 00:00:00	no	no	inUse	2017-12-18 00:00:00		408	1	\N	\N
101	PP-18 / K100403	1	1	107	52	2017-12-18 00:00:00	2018-12-17 00:00:00	no	no	inUse	2017-12-18 00:00:00		409	1	\N	\N
228	PP-29 / K090801	1	2	44	52	2017-12-21 00:00:00	2018-12-20 00:00:00	no	no	inUse	2016-12-28 00:00:00		410	1	\N	\N
1002	HPT-02	5	106	40	0	2017-03-14 00:00:00	2017-03-14 00:00:00	no	no	inUse	2018-01-10 20:16:13.06		413	1	\N	\N
1002	HPT-02	5	106	40	52	2017-03-14 00:00:00	2018-03-13 00:00:00	no	no	inUse	2018-01-10 20:16:13.06		414	1	\N	\N
426	HPT-03 / 3201192	5	24	37	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-23 00:00:00		415	1	\N	\N
705	HPT-22/3202232	5	24	38	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-23 00:00:00		416	1	\N	\N
287	MP-01	77	144	1	52	2016-10-27 00:00:00	2017-10-26 00:00:00	yes	yes	inRepair	2016-10-27 00:00:00		418	1	\N	\N
363	BPE-01	81	153	1	52	1900-01-01 00:00:00	1900-12-31 00:00:00	yes	yes	collected	1900-01-01 00:00:00		419	1	\N	\N
363	BPE-01 / 101221	81	113	1	52	1900-01-01 00:00:00	1900-12-31 00:00:00	no	no	registered	2018-01-10 21:09:00.039		420	1	\N	\N
1060	BPE-04/1012442	81	153	1	52	2018-01-10 00:00:00	2019-01-09 00:00:00	no	no	registered	2018-01-10 21:10:16.04		421	0	\N	\N
363	BPE-01 / 101221	81	113	1	52	2018-01-10 00:00:00	2019-01-09 00:00:00	no	no	registered	2018-01-10 21:09:00.039		422	1	\N	\N
261	PP-34/Z1507024	65	157	1	52	2018-01-10 00:00:00	2019-01-09 00:00:00	no	no	registered	1900-01-01 00:00:00		425	1	\N	\N
60	PM-41 / D100946	1	3	109	52	2017-11-17 00:00:00	2018-11-16 00:00:00	no	no	inUse	2017-11-17 00:00:00		427	1	\N	\N
350	ETF-03/3233	8	14	1	52	2017-03-09 00:00:00	2018-03-08 00:00:00	no	no	inUse	2017-03-09 00:00:00		428	1	\N	\N
1002	HPT-02 / 3201194	5	106	40	52	2017-03-14 00:00:00	2018-03-13 00:00:00	no	no	inUse	2018-01-10 20:16:13.06		429	1	\N	\N
1059	PP-35/C140409	65	155	106	52	2017-12-22 00:00:00	2018-12-21 00:00:00	no	no	inUse	2017-12-29 21:01:22.44	Padrão da bancada de inspeção BI-06	430	1	\N	\N
141	TORQ-10 /6jk100045	25	51	21	75	2017-11-24 00:00:00	2019-05-03 00:00:00	no	no	inCalibration	2017-11-24 00:00:00		431	1	\N	\N
13	CRA / 02.11PNP	16	29	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inCalibration	2017-11-22 00:00:00		432	1	\N	\N
684	TH-21 / 1380000794	4	104	12	52	2017-11-22 00:00:00	2018-11-21 00:00:00	no	no	inUse	2018-01-12 14:56:30.142		433	1	\N	\N
157	MULT-12 / EB294000257	11	96	12	52	2017-11-22 00:00:00	2018-11-21 00:00:00	no	no	inUse	2018-01-12 14:59:15.967		434	1	\N	\N
303	TORQ - 03 / 800428	28	58	69	52	2017-11-24 00:00:00	2018-11-23 00:00:00	no	no	inUse	2018-01-12 16:26:22.668		435	1	\N	\N
45	TH-26 / 7DB600098	3	6	11	52	2017-11-29 00:00:00	2018-11-28 00:00:00	no	no	inUse	2017-11-29 00:00:00		436	1	\N	\N
236	TH-42 / 7DF2024	3	23	43	52	2016-12-01 00:00:00	2017-11-30 00:00:00	no	no	collected	2018-01-12 16:35:20.634		437	1	\N	\N
280	TH-55 \\ 7E0800225	3	6	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	inUse	2016-11-28 00:00:00		438	1	\N	\N
702	TH-24 /  09C0183	3	6	43	52	2016-06-09 00:00:00	2017-06-08 00:00:00	yes	no	collected	2016-06-09 00:00:00		439	1	\N	\N
304	TH-59 / 09C0180	3	6	13	52	1900-01-01 00:00:00	1900-12-31 00:00:00	yes	no	collected	1900-01-01 00:00:00		440	1	\N	\N
158	PAT-01	47	101	12	52	2016-12-19 00:00:00	2017-12-18 00:00:00	no	no	collected	2018-01-12 17:08:05.25		441	1	\N	\N
110	TORQUE-07	27	54	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	collected	2018-01-12 18:40:57.29		442	1	\N	\N
110	TORQUE-07	27	54	21	75	2017-11-23 00:00:00	2019-05-02 00:00:00	no	no	collected	2018-01-12 18:40:57.29		443	1	\N	\N
90	PAQ-01 / 9819738	14	42	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	collected	2018-01-12 18:48:05.776		444	1	\N	\N
251	CES-02	49	109	33	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2017-11-21 00:00:00		446	1	\N	\N
90	PAQ-01 / 9819738	20	42	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	collected	2018-01-12 18:48:05.776		445	1	\N	\N
251	CES-02	49	109	33	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	collected	2018-01-12 18:57:08.046		447	1	\N	\N
30	PP-10 / K101214	1	1	56	52	2017-12-18 00:00:00	2018-12-17 00:00:00	no	no	inUse	2018-01-15 12:10:19.299		448	1	\N	\N
24	PP-09 / K101218	1	1	43	52	2017-01-19 00:00:00	2018-01-18 00:00:00	no	no	collected	2018-01-15 12:13:00.392		449	1	\N	\N
50	TH-31 / 7DB600095	3	6	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	collected	2018-01-15 13:01:43.22		450	1	\N	\N
49	TH-30 / 7DB600083	3	6	43	52	2016-10-21 00:00:00	2017-10-20 00:00:00	no	no	collected	2018-01-15 13:09:00.698		451	1	\N	\N
1030	T-01	41	84	21	72	2017-12-04 00:00:00	2019-04-22 00:00:00	no	no	inUse	2017-03-07 00:00:00		452	1	\N	\N
160	ALT-02 / 1000624	78	44	12	75	2017-08-23 00:00:00	2019-01-30 00:00:00	yes	yes	collected	2017-08-23 00:00:00		453	1	\N	\N
387	ALT-01 / 08.14/09	78	44	12	75	2016-05-02 00:00:00	2017-10-09 00:00:00	yes	yes	collected	2016-05-02 00:00:00		454	1	\N	\N
280	TH-55 \\ 7E0800225	3	6	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	yes	no	inUse	2016-11-28 00:00:00		455	1	\N	\N
280	TH-55 \\ 7E0800225	3	6	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	collected	2018-01-16 18:32:51.372		456	1	\N	\N
52	VOLT-02	7	27	87	52	2016-11-21 00:00:00	2017-11-20 00:00:00	no	no	collected	2018-01-16 18:44:50.119		457	1	\N	\N
154	TH-35 / 7DBC00318	3	6	106	52	2017-08-15 00:00:00	2018-08-14 00:00:00	no	no	inUse	2017-08-15 00:00:00		458	1	\N	\N
236	TH-42 / 7DF2024	3	23	87	52	2016-12-01 00:00:00	2017-11-30 00:00:00	no	no	collected	2018-01-12 16:35:20.634		459	1	\N	\N
47	TH-28 / 7DB600099	3	6	12	52	2017-01-26 00:00:00	2018-01-25 00:00:00	yes	yes	collected	2017-01-26 00:00:00		460	1	\N	\N
47	TH-28 / 7DB600099	3	6	12	52	2017-01-26 00:00:00	2018-01-25 00:00:00	yes	yes	inRepair	2018-01-17 18:18:24.482		461	1	\N	\N
47	TH-28 / 7DB600099	3	6	70	52	2017-01-26 00:00:00	2018-01-25 00:00:00	yes	yes	inRepair	2018-01-17 18:18:24.482		462	1	\N	\N
278	TH-58 \\ 7E080217	3	6	66	52	2016-11-28 00:00:00	2017-11-27 00:00:00	yes	no	inRepair	2018-01-19 09:22:36.701		463	1	\N	\N
25	TH-02	4	103	12	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	inCalibration	2018-01-19 09:28:28.802		464	1	\N	\N
271	CRT / 1205157248	17	37	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 09:59:18.327		465	1	\N	\N
272	CRT / 1205157269	17	37	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 09:59:56.221		466	1	\N	\N
13	CRA / 02.11PNP	16	29	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 10:01:18.711		467	1	\N	\N
141	TORQ-10 /6jk100045	25	51	21	75	2017-11-24 00:00:00	2019-05-03 00:00:00	no	no	inUse	2018-01-19 10:01:56.773		468	1	\N	\N
90	PAQ-01 / 9819738	20	42	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 10:03:20.905		469	1	\N	\N
110	TORQUE-07	27	54	21	75	2017-11-23 00:00:00	2019-05-02 00:00:00	yes	no	collected	2018-01-12 18:40:57.29		470	1	\N	\N
111	TORQUE -08	26	54	43	75	2017-01-19 00:00:00	2018-06-28 00:00:00	yes	no	collected	2018-01-09 16:28:50.313		471	1	\N	\N
222	VOLT-23	7	13	87	52	2017-11-27 00:00:00	2018-11-26 00:00:00	no	no	collected	2018-01-19 17:18:24.257		472	1	\N	\N
262	TORQ-11/ 1604620052	25	52	21	75	2016-06-01 00:00:00	2017-11-08 00:00:00	no	no	collected	2018-01-19 17:27:31.666		473	1	\N	\N
100	CRT / S.008/15	17	34	34	75	2016-08-09 00:00:00	2018-01-16 00:00:00	no	no	collected	2018-01-19 17:28:52.603		474	1	\N	\N
95	CRT / 04.11	17	152	87	75	2016-08-09 00:00:00	2018-01-16 00:00:00	no	no	collected	2018-01-19 17:30:40.102		475	1	\N	\N
31	BI-01	52	113	71	27	2017-03-20 00:00:00	2017-09-25 00:00:00	no	no	inUse	2016-04-22 00:00:00		476	1	\N	\N
31	BI-01	52	113	71	54	2017-03-20 00:00:00	2018-04-02 00:00:00	no	no	inUse	2016-04-22 00:00:00		477	1	\N	\N
34	BI-04	53	116	71	54	2017-03-23 00:00:00	2018-04-05 00:00:00	no	no	inUse	2016-04-26 00:00:00		478	1	\N	\N
33	BI-02	52	114	71	54	2017-03-21 00:00:00	2018-04-03 00:00:00	no	no	inUse	2016-04-23 00:00:00		479	1	\N	\N
1052	TH-01 / Q102265	4	103	43	52	2008-06-18 00:00:00	2009-06-17 00:00:00	yes	no	collected	2008-06-18 00:00:00		480	1	\N	\N
32	BI-03	53	116	71	52	2017-03-23 00:00:00	2018-03-22 00:00:00	no	no	inUse	2016-04-25 00:00:00		481	1	\N	\N
33	BI-02	52	114	71	52	2017-03-21 00:00:00	2018-03-20 00:00:00	no	no	inUse	2016-04-23 00:00:00		482	1	\N	\N
31	BI-01	52	113	71	52	2017-03-20 00:00:00	2018-03-19 00:00:00	no	no	inUse	2016-04-22 00:00:00		483	1	\N	\N
139	PP-22 / K100614	1	1	13	52	2017-01-16 00:00:00	2018-01-15 00:00:00	no	no	collected	2018-01-09 14:01:46.032		484	1	\N	\N
92	PP-15 / K110401	1	1	43	52	2017-01-17 00:00:00	2018-01-16 00:00:00	no	no	collected	2018-01-09 14:07:31.049		485	1	\N	\N
24	PP-09 / K101218	1	1	43	52	2017-01-17 00:00:00	2018-01-16 00:00:00	no	no	collected	2018-01-15 12:13:00.392		486	1	\N	\N
24	PP-09 / K101218	1	1	43	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	collected	2018-01-15 12:13:00.392		487	1	\N	\N
92	PP-15 / K110401	1	1	43	52	2018-01-18 00:00:00	2019-01-17 00:00:00	no	no	collected	2018-01-09 14:07:31.049		488	1	\N	\N
139	PP-22 / K100614	1	1	13	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	collected	2018-01-09 14:01:46.032		489	1	\N	\N
219	BI-05	59	116	71	52	2017-03-24 00:00:00	2018-03-23 00:00:00	no	no	inUse	2017-01-15 00:00:00		490	1	\N	\N
218	BI-04	59	116	71	52	2017-03-23 00:00:00	2018-03-22 00:00:00	no	no	inUse	2017-01-06 00:00:00		491	1	\N	\N
215	BI-02	59	114	71	52	2017-03-21 00:00:00	2018-03-20 00:00:00	no	no	inUse	2017-01-02 00:00:00		492	1	\N	\N
32	BI-03	53	116	71	52	2017-03-23 00:00:00	2018-03-22 00:00:00	no	no	registered	2018-01-22 20:19:07.595		493	1	\N	\N
31	BI-01	52	113	71	52	2017-03-20 00:00:00	2018-03-19 00:00:00	no	no	registered	2018-01-22 20:20:08.619		494	1	\N	\N
214	BI-01	59	113	71	52	2017-03-20 00:00:00	2018-03-19 00:00:00	no	no	inUse	2016-12-30 00:00:00		495	1	\N	\N
33	BI-02	52	114	71	52	2017-03-21 00:00:00	2018-03-20 00:00:00	no	no	registered	2018-01-22 20:23:26.508		496	1	\N	\N
217	BI-03	59	116	71	52	2017-03-23 00:00:00	2018-03-22 00:00:00	no	no	inUse	2017-01-04 00:00:00		497	1	\N	\N
34	BI-04	53	116	71	54	2017-03-23 00:00:00	2018-04-05 00:00:00	no	no	registered	2018-01-22 20:24:57.383		498	1	\N	\N
1061	TH-60	82	158	70	75	2018-01-23 00:00:00	2019-07-02 00:00:00	no	no	registered	2018-01-23 12:07:38.98		511	0	\N	\N
1061	TH-60 / HT208-6044	82	158	70	75	2018-01-23 00:00:00	2019-07-02 00:00:00	no	no	registered	2018-01-23 12:07:38.98		512	1	\N	\N
1062	TH-61 / HT208-6048	82	158	21	75	2018-01-23 00:00:00	2019-07-02 00:00:00	no	no	registered	2018-01-23 12:26:53.974		534	0	\N	\N
266	DOS-01	29	59	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2017-01-24 00:00:00		535	1	\N	\N
266	DOS-01	29	59	23	103	2017-01-24 00:00:00	2019-01-15 00:00:00	no	no	inUse	2017-01-24 00:00:00		536	1	\N	\N
267	DOS-02	29	60	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2017-01-24 00:00:00		537	1	\N	\N
267	DOS-02	29	60	23	105	2017-01-24 00:00:00	2019-01-29 00:00:00	no	no	inUse	2017-01-24 00:00:00		538	1	\N	\N
266	DOS-01	29	59	23	80	2017-01-24 00:00:00	2018-08-07 00:00:00	no	no	inUse	2017-01-24 00:00:00		539	1	\N	\N
266	DOS-01	29	59	23	100	2017-01-24 00:00:00	2018-12-25 00:00:00	no	no	inUse	2017-01-24 00:00:00		540	1	\N	\N
267	DOS-02	29	60	23	101	2017-01-24 00:00:00	2019-01-01 00:00:00	no	no	inUse	2017-01-24 00:00:00		541	1	\N	\N
267	DOS-02	29	60	23	102	2017-01-24 00:00:00	2019-01-08 00:00:00	no	no	inUse	2017-01-24 00:00:00		542	1	\N	\N
267	DOS-02	29	60	23	103	2017-01-24 00:00:00	2019-01-15 00:00:00	no	no	inUse	2017-01-24 00:00:00		543	1	\N	\N
267	DOS-02	29	60	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2017-01-24 00:00:00		544	1	\N	\N
267	DOS-02	29	60	23	105	2017-01-24 00:00:00	2019-01-29 00:00:00	no	no	inUse	2017-01-24 00:00:00		545	1	\N	\N
267	DOS-02	29	60	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2017-01-24 00:00:00		546	1	\N	\N
266	DOS-01	29	59	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2017-01-24 00:00:00		547	1	\N	\N
269	LUX-01	31	62	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2017-01-24 00:00:00		548	1	\N	\N
52	VOLT-02	7	27	12	52	2018-01-11 00:00:00	2019-01-10 00:00:00	no	no	collected	2018-01-16 18:44:50.119		549	1	\N	\N
222	VOLT-23	7	13	12	52	2018-01-11 00:00:00	2019-01-10 00:00:00	no	no	collected	2018-01-19 17:18:24.257		550	1	\N	\N
257	TH-51 \\ 7DE902118	3	6	76	52	2016-09-26 00:00:00	2017-09-25 00:00:00	no	no	inCalibration	2018-01-24 18:00:02.92		551	1	\N	\N
252	TH-50 / 7DE902109	3	6	34	52	2016-08-19 00:00:00	2017-08-18 00:00:00	no	no	inCalibration	2018-01-24 18:09:21.922		552	1	\N	\N
182	TH-38 / 7DBC00314	3	6	76	52	2016-10-21 00:00:00	2017-10-20 00:00:00	yes	no	collected	2016-10-21 00:00:00		553	1	\N	\N
252	TH-50 / 7DE902109	3	6	76	52	2016-08-19 00:00:00	2017-08-18 00:00:00	no	no	inCalibration	2018-01-24 18:09:21.922		554	1	\N	\N
237	TH-43 / 7DF200025	3	23	76	52	2016-03-14 00:00:00	2017-03-13 00:00:00	yes	no	collected	2016-03-14 00:00:00		555	1	\N	\N
49	TH-30 / 7DB600083	3	6	52	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2018-01-24 18:50:49.513		556	1	\N	\N
351	CC-1	9	15	1	52	2016-12-19 00:00:00	2017-12-18 00:00:00	no	no	inUse	2016-12-19 00:00:00		557	1	\N	\N
351	CC-1	9	15	2	52	2016-12-19 00:00:00	2017-12-18 00:00:00	yes	no	inUse	2016-12-19 00:00:00		558	1	\N	\N
351	CC-1	9	15	2	52	2016-12-19 00:00:00	2017-12-18 00:00:00	yes	no	registered	2018-01-24 19:37:23.197		559	1	\N	\N
695	K090908- PP 03	1	1	3	52	2011-01-10 00:00:00	2012-01-09 00:00:00	yes	no	collected	2011-01-10 00:00:00		560	1	\N	\N
137	PP-21 / K100310	1	1	43	52	2017-01-23 00:00:00	2018-01-22 00:00:00	no	no	collected	2018-01-31 17:05:21.252		561	1	\N	\N
193	PP-26 / K130202	1	1	7	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2017-11-23 00:00:00		562	1	\N	\N
139	PP-22 / K100614	1	1	53	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	collected	2018-01-09 14:01:46.032		563	1	\N	\N
139	PP-22 / K100614	1	1	53	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	inUse	2018-01-31 17:10:32.475		564	1	\N	\N
227	PP-28 / K140206	1	1	108	52	2017-12-19 00:00:00	2018-12-18 00:00:00	no	no	inUse	2017-12-19 00:00:00		565	1	\N	\N
139	PP-22 / K100614	1	1	43	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	collected	2018-01-31 19:46:36.508		566	1	\N	\N
193	PP-26 / K130202	1	1	53	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2017-11-23 00:00:00		567	1	\N	\N
193	PP-26 / K130202	1	1	53	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	registered	2018-01-31 19:50:05.341		568	1	\N	\N
139	PP-22 / K100614	1	1	47	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	inUse	2018-02-01 12:35:26.209		569	1	\N	\N
54	PP-12 / K091116	1	1	7	52	2017-12-08 00:00:00	2018-12-07 00:00:00	no	no	inUse	2017-12-08 00:00:00		570	1	\N	\N
222	VOLT-23	7	26	12	52	2018-01-11 00:00:00	2019-01-10 00:00:00	no	no	collected	2018-01-19 17:18:24.257		571	1	\N	\N
146	VOLT-04	7	25	14	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2017-11-23 00:00:00		572	1	\N	\N
194	VOLT-19	7	13	19	52	2017-05-11 00:00:00	2018-05-10 00:00:00	no	no	inUse	2017-05-11 00:00:00		573	1	\N	\N
1050	PM-42 / D101138	1	3	43	52	2017-11-20 00:00:00	2018-11-19 00:00:00	no	no	collected	2018-02-01 18:38:37.135		574	1	\N	\N
704	HPT-05 / 3201091	5	24	12	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2018-02-02 14:11:46.187		575	1	\N	\N
588	HPT-09 / 3201093	5	24	13	52	2017-02-03 00:00:00	2018-02-02 00:00:00	no	no	collected	2018-02-02 14:13:11.036		576	1	\N	\N
427	3201093/ HPT-01	5	24	43	52	2012-04-24 00:00:00	2013-04-23 00:00:00	yes	yes	collected	2012-04-24 00:00:00		577	1	\N	\N
427	HPT-01 / 3201093	5	24	43	52	2012-04-24 00:00:00	2013-04-23 00:00:00	yes	yes	collected	2012-04-24 00:00:00		578	1	\N	\N
214	BI-01	59	113	71	52	2017-12-27 00:00:00	2018-12-26 00:00:00	no	no	inUse	2016-12-30 00:00:00		579	1	\N	\N
94	PP-17 / K110428	1	1	54	52	2017-03-19 00:00:00	2018-03-18 00:00:00	no	no	inUse	2017-08-08 00:00:00		580	1	\N	\N
214	BI-01	59	113	71	52	2017-03-19 00:00:00	2018-03-18 00:00:00	no	no	inUse	2016-12-30 00:00:00		581	1	\N	\N
214	BI-01	59	113	71	36	2017-03-19 00:00:00	2017-11-26 00:00:00	no	no	inUse	2016-12-30 00:00:00		582	1	\N	\N
214	BI-01	59	113	71	36	2017-12-27 00:00:00	2018-09-05 00:00:00	no	no	inUse	2016-12-30 00:00:00		583	1	\N	\N
214	BI-01	59	113	71	26	2017-12-27 00:00:00	2018-06-27 00:00:00	no	no	inUse	2016-12-30 00:00:00		584	1	\N	\N
215	BI-02	59	114	71	26	2017-03-21 00:00:00	2017-09-19 00:00:00	no	no	inUse	2017-01-02 00:00:00		585	1	\N	\N
214	BI-01	59	113	71	25	2017-12-27 00:00:00	2018-06-20 00:00:00	no	no	inUse	2016-12-30 00:00:00		586	1	\N	\N
215	BI-02	59	114	71	25	2017-03-21 00:00:00	2017-09-12 00:00:00	no	no	inUse	2017-01-02 00:00:00		587	1	\N	\N
216	ED 8273	59	115	71	25	2014-11-15 00:00:00	2015-05-09 00:00:00	yes	no	collected	2014-11-15 00:00:00		588	1	\N	\N
219	BI-05	59	116	71	25	2017-03-24 00:00:00	2017-09-15 00:00:00	no	no	inUse	2017-01-15 00:00:00		589	1	\N	\N
217	BI-03	59	116	71	25	2017-03-23 00:00:00	2017-09-14 00:00:00	no	no	inUse	2017-01-04 00:00:00		590	1	\N	\N
218	BI-04	59	116	71	25	2017-03-23 00:00:00	2017-09-14 00:00:00	no	no	inUse	2017-01-06 00:00:00		591	1	\N	\N
214	BI-01	59	113	71	27	2017-12-27 00:00:00	2018-07-04 00:00:00	no	no	inUse	2016-12-30 00:00:00		592	1	\N	\N
214	BI-01	59	113	71	25	2017-12-27 00:00:00	2018-06-20 00:00:00	no	no	inUse	2016-12-30 00:00:00		593	1	\N	\N
215	BI-02	59	114	71	25	2017-12-28 00:00:00	2018-06-21 00:00:00	no	no	inUse	2017-01-02 00:00:00		594	1	\N	\N
217	BI-03	59	116	71	25	2017-12-23 00:00:00	2018-06-16 00:00:00	no	no	inUse	2017-01-04 00:00:00		595	1	\N	\N
218	BI-04	59	116	71	25	2017-12-24 00:00:00	2018-06-17 00:00:00	no	no	inUse	2017-01-06 00:00:00		596	1	\N	\N
219	BI-05	59	116	71	25	2017-12-26 00:00:00	2018-06-19 00:00:00	no	no	inUse	2017-01-15 00:00:00		597	1	\N	\N
236	TH-42 / 7DF20024	3	23	5	52	2016-12-01 00:00:00	2017-11-30 00:00:00	no	no	collected	2018-01-12 16:35:20.634		598	1	\N	\N
50	TH-31 / 7DB600099	3	6	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	collected	2018-01-15 13:01:43.22		599	1	\N	\N
43	TH-25 / 7DB600086	3	6	41	52	2016-10-21 00:00:00	2017-10-20 00:00:00	no	no	inUse	2016-10-21 00:00:00		600	1	\N	\N
43	TH-25 / 7DB600086	3	6	41	52	2016-10-21 00:00:00	2017-10-20 00:00:00	no	no	collected	2018-02-02 19:38:33.487		601	1	\N	\N
47	TH-28 / 7DB600099	3	6	70	52	2017-01-26 00:00:00	2018-01-25 00:00:00	yes	yes	collected	2018-02-02 19:40:42.559		602	1	\N	\N
239	TH-45 / 7DE902117	3	23	41	52	2017-02-28 00:00:00	2018-02-27 00:00:00	no	no	inUse	2017-02-28 00:00:00		603	1	\N	\N
29	PM-34 / D110124	1	3	13	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	collected	2018-01-06 16:04:05.088	Sala instrumentação	604	1	\N	\N
193	PP-26 / K130202	1	1	53	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2018-02-02 21:17:57.276		605	1	\N	\N
131	BM-03	61	151	72	27	2016-01-18 00:00:00	2016-07-25 00:00:00	yes	no	registered	2018-02-05 11:57:48.765		606	1	\N	\N
592	BM 06	61	128	79	27	2012-10-15 00:00:00	2013-04-22 00:00:00	yes	no	registered	2018-02-05 11:58:29.775		607	1	\N	\N
674	TH - 14 / 38769	4	132	13	52	2014-04-29 00:00:00	2015-04-28 00:00:00	no	no	collected	2014-04-29 00:00:00		609	1	\N	\N
672	TH - 12 / 38768	4	132	13	52	2014-04-29 00:00:00	2015-04-28 00:00:00	no	no	collected	2014-04-29 00:00:00		610	1	\N	\N
1062	TH-61 / HT208-6048	82	158	21	75	2018-01-23 00:00:00	2019-07-02 00:00:00	no	no	collected	2018-02-05 13:53:07.163		611	1	\N	\N
1061	TH-60 / HT208-6044	82	158	70	75	2018-01-23 00:00:00	2019-07-02 00:00:00	no	no	collected	2018-02-05 13:53:26.4		612	1	\N	\N
304	TH-59 / 09C0180	3	6	13	52	1900-01-01 00:00:00	1900-12-31 00:00:00	yes	yes	collected	1900-01-01 00:00:00		613	1	\N	\N
702	TH-24 /  09C0183	3	6	43	52	2016-06-09 00:00:00	2017-06-08 00:00:00	yes	yes	collected	2016-06-09 00:00:00		614	1	\N	\N
1056	TH-46 7DE902119	3	6	101	52	2015-03-26 00:00:00	2016-03-24 00:00:00	yes	yes	collected	2015-03-26 00:00:00		615	1	\N	\N
137	PP-21 / K100310	1	1	87	52	2017-01-23 00:00:00	2018-01-22 00:00:00	no	no	collected	2018-01-31 17:05:21.252		616	1	\N	\N
100	CRT / S.008/15	17	34	21	75	2016-08-09 00:00:00	2018-01-16 00:00:00	no	no	collected	2018-01-19 17:28:52.603		617	1	\N	\N
95	CRT / 04.11	17	152	21	75	2016-08-09 00:00:00	2018-01-16 00:00:00	no	no	collected	2018-01-19 17:30:40.102		618	1	\N	\N
1063	PAQ-09 / 530-114B-10	14	159	66	75	2018-02-04 00:00:00	2019-07-14 00:00:00	no	no	registered	2018-02-07 12:27:51.277		620	0	\N	\N
1064	PAQ-11 / D12841	14	159	66	75	2018-02-05 00:00:00	2019-07-15 00:00:00	no	no	registered	2018-02-07 12:29:09.978		621	0	\N	\N
1065	PAQ-12 / 71130329308	14	159	21	75	2017-02-05 00:00:00	2018-07-15 00:00:00	no	no	registered	2018-02-07 13:05:39.356		622	0	\N	\N
195	08.39/55	14	20	21	75	2012-02-07 00:00:00	2013-07-16 00:00:00	yes	no	collected	2012-02-07 00:00:00		623	1	\N	\N
1065	PAQ-12 / 71130329308	14	154	21	75	2017-02-05 00:00:00	2018-07-15 00:00:00	no	no	registered	2018-02-07 13:05:39.356		624	1	\N	\N
111	TORQ - 08 / 1169	26	54	43	75	2017-01-19 00:00:00	2018-06-28 00:00:00	yes	no	collected	2018-01-09 16:28:50.313		625	1	\N	\N
110	TORQ - 07	27	54	21	75	2017-11-23 00:00:00	2019-05-02 00:00:00	yes	no	collected	2018-01-12 18:40:57.29		626	1	\N	\N
359	TORQ -15	28	57	21	75	2017-07-04 00:00:00	2018-12-11 00:00:00	no	no	inUse	2017-07-04 00:00:00		627	1	\N	\N
353	TORQ - 14	28	57	21	75	2017-07-04 00:00:00	2018-12-11 00:00:00	no	no	inUse	2017-07-04 00:00:00		628	1	\N	\N
275	TORQ - 12	28	55	21	75	2016-06-01 00:00:00	2017-11-08 00:00:00	no	no	inUse	2016-06-01 00:00:00		629	1	\N	\N
112	TORQ - 09	26	53	21	75	2017-08-23 00:00:00	2019-01-30 00:00:00	no	no	inUse	2017-08-23 00:00:00		630	1	\N	\N
25	TH-02 / Q116844	4	103	12	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	collected	2018-02-07 13:56:40.324		631	1	\N	\N
277	T.H - 02 / M1830011	4	7	1	52	2016-08-09 00:00:00	2017-08-08 00:00:00	no	no	inCalibration	2016-08-09 00:00:00		632	1	\N	\N
25	TH-02 / Q116844	4	103	12	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	inCalibration	2018-02-07 14:37:59.594		633	1	\N	\N
1065	PAQ-12 / 71130329308	14	154	21	75	2017-02-05 00:00:00	2018-07-15 00:00:00	no	no	inCalibration	2018-02-07 14:38:40.964		634	1	\N	\N
262	TORQ-11/ 1604620052	25	52	21	75	2016-06-01 00:00:00	2017-11-08 00:00:00	no	no	inCalibration	2018-02-07 14:39:40.343		635	1	\N	\N
100	CRT / S.008/15	17	34	21	75	2016-08-09 00:00:00	2018-01-16 00:00:00	no	no	inCalibration	2018-02-07 14:40:00.286		636	1	\N	\N
95	CRT / 04.11	17	152	21	75	2016-08-09 00:00:00	2018-01-16 00:00:00	no	no	inCalibration	2018-02-07 14:40:14.519		637	1	\N	\N
359	TORQ -15 / 0CR006601	28	57	21	75	2017-09-04 00:00:00	2019-02-11 00:00:00	no	no	inUse	2017-07-04 00:00:00		638	1	\N	\N
353	TORQ - 14	28	57	21	75	2017-09-04 00:00:00	2019-02-11 00:00:00	no	no	inUse	2017-07-04 00:00:00		639	1	\N	\N
353	TORQ - 14 / 0DP013938	28	57	21	75	2017-12-21 00:00:00	2019-05-30 00:00:00	no	no	inUse	2017-07-04 00:00:00		640	1	\N	\N
359	TORQ -15 / 0CR006601	28	57	21	75	2017-12-21 00:00:00	2019-05-30 00:00:00	no	no	inUse	2017-07-04 00:00:00		641	1	\N	\N
303	TORQ - 03 / 800428	28	58	69	75	2017-11-24 00:00:00	2019-05-03 00:00:00	no	no	inUse	2018-01-12 16:26:22.668		642	1	\N	\N
1066	VOLT - 24	7	160	43	52	2018-02-04 00:00:00	2019-02-03 00:00:00	no	no	registered	2018-02-14 19:38:13.399		649	0	\N	\N
1066	VOLT-24	7	160	43	52	2018-02-04 00:00:00	2019-02-03 00:00:00	no	no	registered	2018-02-14 19:38:13.399		650	1	\N	\N
1067	VOLT-25	7	160	43	52	2018-02-04 00:00:00	2019-02-03 00:00:00	no	no	registered	2018-02-14 19:42:57.106		651	0	\N	\N
1068	VOLT-26	7	160	43	52	2018-02-04 00:00:00	2019-02-03 00:00:00	no	no	registered	2018-02-14 19:44:16.493		652	0	\N	\N
123	HTP-15 / 109611067G	5	105	43	52	2016-11-30 00:00:00	2017-11-29 00:00:00	no	no	collected	2018-02-14 20:16:07.518		653	1	\N	\N
298	HPT-25	5	107	1	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2017-08-16 00:00:00		654	1	\N	\N
123	HTP-15 / 109611067G	5	105	1	52	2016-11-30 00:00:00	2017-11-29 00:00:00	no	no	collected	2018-02-14 20:16:07.518		655	1	\N	\N
299	HPT-15 / 3201433	5	24	43	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	collected	2018-02-14 20:19:38.511		656	1	\N	\N
60	PM-35 / D101138	1	3	66	52	2017-12-06 00:00:00	2018-12-05 00:00:00	no	no	inUse	2017-11-17 00:00:00		659	1	\N	\N
1057	PM-41 / D100946	1	3	109	52	2017-11-17 00:00:00	2018-11-16 00:00:00	no	no	inUse	2017-12-06 00:00:00		660	1	\N	\N
1003	ION 8650 / 1309A694-01	60	120	71	52	2017-10-21 00:00:00	2018-10-20 00:00:00	no	no	registered	2018-01-10 13:09:18.976		661	1	\N	\N
102	PM-42 / D101138	1	3	10	52	2017-11-20 00:00:00	2018-11-19 00:00:00	no	no	inUse	2017-11-20 00:00:00		665	1	\N	\N
1050	PP-1000	66	145	75	52	1999-11-20 00:00:00	2000-11-18 00:00:00	yes	no	registered	2018-02-15 18:43:35.32		666	1	\N	\N
1007	DM 9070334	76	98	12	52	2016-03-28 00:00:00	2017-03-27 00:00:00	no	no	inCalibration	2018-02-15 18:49:29.6		667	1	\N	\N
1007	PP-1001	66	145	75	52	1990-03-28 00:00:00	1991-03-27 00:00:00	yes	no	registered	2018-02-15 18:55:57.937		668	1	\N	\N
251	CES-02	49	109	21	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2018-02-16 10:57:03.683		670	1	\N	\N
1064	PAQ-11 / D12841	14	159	21	75	2018-02-05 00:00:00	2019-07-15 00:00:00	no	no	registered	2018-02-07 12:29:09.978		699	1	\N	\N
244	ESP-01 / AF1404	51	111	33	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	collected	2018-02-16 11:13:16.194		700	1	\N	\N
299	HPT-15 / 3201433	5	24	110	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2018-02-16 11:16:51.765		703	1	\N	\N
1058	PAQ-07 / 71130329308	14	154	21	75	2017-12-28 00:00:00	2019-06-06 00:00:00	no	no	collected	2017-12-29 19:33:06.726		704	1	\N	\N
1063	PAQ-09 / 530-114B-10	14	159	66	75	2018-02-04 00:00:00	2019-07-14 00:00:00	no	no	inCalibration	2018-02-16 11:25:45.416		705	1	\N	\N
255	MULT-14	11	94	94	52	2016-11-30 00:00:00	2017-11-29 00:00:00	no	no	collected	2018-02-16 17:48:12.723		706	1	\N	\N
423	MULT-05 / ET405001046	44	93	12	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	inUse	2018-02-16 18:46:14.543		707	1	\N	\N
255	MULT-14	11	94	94	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	collected	2018-02-16 17:48:12.723		708	1	\N	\N
1069	MULT-23 / 22030278	11	94	66	75	2018-01-17 00:00:00	2019-06-26 00:00:00	no	no	registered	2018-02-16 19:03:24.216		709	0	\N	\N
1069	MULT-23 / 22030278	11	94	66	75	2018-01-17 00:00:00	2019-06-26 00:00:00	no	no	collected	2018-02-16 19:03:58.242		710	1	\N	\N
249	MULT-13 / 29900136	11	94	94	52	2016-10-07 00:00:00	2017-10-06 00:00:00	no	no	collected	2018-02-16 19:11:48.033		711	1	\N	\N
252	TH-50 / 7DE902109	3	6	76	52	2018-01-19 00:00:00	2019-01-18 00:00:00	no	no	collected	2018-02-16 19:24:58.193		712	1	\N	\N
249	MULT-13 / 29900136	11	94	94	52	2016-10-07 00:00:00	2017-10-06 00:00:00	no	yes	inRepair	2018-02-19 11:25:03.348		713	1	\N	\N
255	MULT-14	11	94	94	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	inUse	2018-02-19 11:26:10.935		714	1	\N	\N
244	ESP-01 / AF1404	51	111	33	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2018-02-19 11:28:03.784		715	1	\N	\N
1064	PAQ-11 / D12841	14	159	66	75	2018-02-05 00:00:00	2019-07-15 00:00:00	no	no	inUse	2018-02-19 11:57:05.444		716	1	\N	\N
1063	PAQ-09 / 530-114B-10	14	159	66	75	2018-02-04 00:00:00	2019-07-14 00:00:00	no	no	inUse	2018-02-19 12:03:03.883		717	1	\N	\N
194	VOLT-19	7	13	19	52	2017-05-16 00:00:00	2018-05-15 00:00:00	no	no	inUse	2017-05-11 00:00:00		718	1	\N	\N
253	AMP-01	6	9	5	52	2017-12-04 00:00:00	2018-12-03 00:00:00	no	no	inUse	2017-11-23 00:00:00		719	1	\N	\N
253	AMP-01	6	9	5	52	2017-12-01 00:00:00	2018-11-30 00:00:00	no	no	inUse	2017-11-23 00:00:00		720	1	\N	\N
254	AMP-02/1203372401	6	10	5	52	2017-12-04 00:00:00	2018-12-03 00:00:00	no	no	inUse	2017-12-04 00:00:00		721	1	\N	\N
194	VOLT-19/1203372403	7	13	19	52	2017-05-16 00:00:00	2018-05-15 00:00:00	no	no	inUse	2017-05-11 00:00:00		722	1	\N	\N
194	VOLT-19/1203372403	7	13	19	52	2017-05-11 00:00:00	2018-05-10 00:00:00	no	no	inUse	2017-05-11 00:00:00		723	1	\N	\N
226	PP-27 / K130913	1	2	52	52	2017-12-07 00:00:00	2018-12-06 00:00:00	no	no	inUse	2017-12-07 00:00:00		724	1	\N	\N
228	PP-29 / K090801	1	1	44	52	2017-12-21 00:00:00	2018-12-20 00:00:00	no	no	inUse	2016-12-28 00:00:00		726	1	\N	\N
246	PIN-01/140027839	50	110	12	52	2017-05-19 00:00:00	2018-05-18 00:00:00	no	no	inUse	2017-05-19 00:00:00		727	1	\N	\N
248	MCR-02/120880831	24	49	12	75	2016-08-12 00:00:00	2018-01-19 00:00:00	no	no	inUse	2016-08-12 00:00:00		728	1	\N	\N
257	TH-51 \\ 7DE902118	3	6	76	52	2018-01-19 00:00:00	2019-01-18 00:00:00	no	no	inUse	2018-02-20 14:02:12.8		737	1	\N	\N
257	TH-51 \\ 7DE902118	3	6	54	52	2018-01-19 00:00:00	2019-01-18 00:00:00	no	no	inUse	2018-02-20 14:02:12.8		739	1	\N	\N
252	TH-50 / 7DE902109	3	6	51	52	2018-01-19 00:00:00	2019-01-18 00:00:00	no	no	inUse	2018-02-20 14:03:29.486		740	1	\N	\N
279	TH-54 \\ 7E0800228	3	6	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	collected	2018-02-20 15:17:12.025		741	1	\N	\N
693	P. TEMP. - 01	2	5	1	52	2017-01-26 00:00:00	2018-01-25 00:00:00	no	no	collected	2018-02-20 17:13:19.429		744	1	\N	\N
292	MULT-18/94810005	10	16	1	52	2017-01-24 00:00:00	2018-01-23 00:00:00	no	no	collected	2018-02-20 17:13:44.009		745	1	\N	\N
559	MULT 02 / 88450278	11	97	35	52	2016-06-07 00:00:00	2017-06-06 00:00:00	no	no	collected	2018-02-20 17:14:51.327		746	1	\N	\N
227	PP-28 / K140206	1	2	108	52	2017-12-19 00:00:00	2018-12-18 00:00:00	no	no	inUse	2017-12-19 00:00:00		725	1	\N	\N
253	AMP-01	6	11	5	52	2017-12-01 00:00:00	2018-11-30 00:00:00	no	no	inUse	2017-11-23 00:00:00		729	1	\N	\N
244	ESP-01 / AF1412	51	111	33	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2018-02-19 11:28:03.784		730	1	\N	\N
249	MULT-13 / 29990136	11	94	94	52	2016-10-07 00:00:00	2017-10-06 00:00:00	no	yes	inRepair	2018-02-19 11:25:03.348		731	1	\N	\N
266	DOS-01/140201184	29	59	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2017-01-24 00:00:00		732	1	\N	\N
270	CAC-01/655473	32	63	23	52	2017-01-24 00:00:00	2018-01-23 00:00:00	no	no	inUse	2017-01-24 00:00:00		734	1	\N	\N
257	TH-51 \\ 7DE902118	3	6	76	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	inUse	2018-02-20 14:02:12.8		735	1	\N	\N
252	TH-50 / 7DE902109	3	6	76	52	2018-01-19 00:00:00	2019-01-18 00:00:00	no	no	inUse	2018-02-20 14:03:29.486		736	1	\N	\N
243	TH-49 7DE902097	3	23	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	collected	2018-02-20 15:14:11.181		738	1	\N	\N
145	PAQ-02	20	42	21	75	2016-08-03 00:00:00	2018-01-10 00:00:00	no	no	collected	2018-02-20 16:29:52.845		742	1	\N	\N
191	PAQ-03	14	43	21	75	2016-09-12 00:00:00	2018-02-19 00:00:00	no	yes	collected	2018-02-20 16:30:29.989		743	1	\N	\N
269	LUX-01/29354	31	62	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2017-01-24 00:00:00		733	1	\N	\N
274	CRA/1810140045	16	31	3	75	2017-11-28 00:00:00	2019-05-07 00:00:00	no	no	inCalibration	2017-11-28 00:00:00		747	1	\N	\N
274	CRA/1810140045	16	31	3	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inCalibration	2017-11-28 00:00:00		748	1	\N	\N
275	TORQ - 12	28	55	21	75	2015-06-01 00:00:00	2016-11-07 00:00:00	no	no	inUse	2016-06-01 00:00:00		749	1	\N	\N
275	TORQ-12/T045360	28	55	21	75	2015-06-01 00:00:00	2016-11-07 00:00:00	no	no	inUse	2016-06-01 00:00:00		750	1	\N	\N
278	TH-58 \\ 7E0800217	3	6	66	52	2016-11-28 00:00:00	2017-11-27 00:00:00	yes	no	inRepair	2018-01-19 09:22:36.701		751	1	\N	\N
281	TORQ-13/309524G	28	56	21	75	2017-08-23 00:00:00	2019-01-30 00:00:00	no	no	inUse	2017-08-23 00:00:00		752	1	\N	\N
284	VOLT-05	7	12	2	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2017-11-21 00:00:00		753	1	\N	\N
285	VOLT-06	7	162	2	52	2017-12-01 00:00:00	2018-11-30 00:00:00	no	no	inUse	2017-11-30 00:00:00		755	1	\N	\N
284	VOLT-05	7	162	2	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2017-11-21 00:00:00		756	1	\N	\N
298	HPT-25/3420393	5	107	1	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2017-08-16 00:00:00		757	1	\N	\N
298	HPT-25/3420393	5	163	1	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2017-08-16 00:00:00		759	1	\N	\N
301	Tridimensional/00221402	42	85	21	52	2017-03-14 00:00:00	2018-03-13 00:00:00	no	no	inUse	2017-03-14 00:00:00		760	1	\N	\N
248	MCR-02/120880831	24	49	21	75	2016-08-12 00:00:00	2018-01-19 00:00:00	no	no	inUse	2016-08-12 00:00:00		761	1	\N	\N
271	CRT / 1205157248	17	164	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 09:59:18.327		763	1	\N	\N
272	CRT / 1205157269	17	164	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 09:59:56.221		764	1	\N	\N
227	PP-28 / K140206	1	2	56	52	2017-12-19 00:00:00	2018-12-18 00:00:00	no	no	inUse	2017-12-19 00:00:00		765	1	\N	\N
30	PP-10 / K101214	1	1	108	52	2017-12-18 00:00:00	2018-12-17 00:00:00	no	no	inUse	2018-01-15 12:10:19.299		766	1	\N	\N
305	RA -121.342/G303168	22	46	21	75	2017-01-23 00:00:00	2018-07-02 00:00:00	yes	no	collected	2018-01-10 10:14:35.654		767	1	\N	\N
354	Perfil-1/S2K-01125	40	165	21	52	2017-01-16 00:00:00	2018-01-15 00:00:00	no	no	inUse	2017-01-16 00:00:00		770	1	\N	\N
358	TH-57	38	108	21	52	2017-01-20 00:00:00	2018-01-19 00:00:00	no	no	inUse	2017-01-20 00:00:00		771	1	\N	\N
360	MULT-15/25359248	39	82	26	52	2017-01-24 00:00:00	2018-01-23 00:00:00	no	no	inUse	2017-01-24 00:00:00		772	1	\N	\N
365	BPE-03	81	166	1	52	2016-10-27 00:00:00	2017-10-26 00:00:00	yes	no	collected	2016-10-27 00:00:00		774	1	\N	\N
387	ALT-01/03007181	78	44	12	75	2016-05-02 00:00:00	2017-10-09 00:00:00	yes	yes	collected	2016-05-02 00:00:00		775	1	\N	\N
425	HPT-02 / 3201194	5	24	40	52	2017-03-14 00:00:00	2018-03-13 00:00:00	no	no	inUse	2018-01-10 20:16:13.06		776	1	\N	\N
550	PAQ/06904252	14	20	21	75	2016-10-20 00:00:00	2018-03-29 00:00:00	no	no	inUse	2016-10-20 00:00:00		777	1	\N	\N
1064	PAQ-11/14824436	20	42	66	75	2018-01-08 00:00:00	2019-06-17 00:00:00	no	no	inUse	2018-02-19 11:57:05.444		778	1	\N	\N
1063	PAQ-09 / D12841	20	42	66	75	2018-02-04 00:00:00	2019-07-14 00:00:00	no	no	inUse	2018-02-19 12:03:03.883		779	1	\N	\N
1063	PAQ-09 / D12841	20	42	66	75	2018-01-08 00:00:00	2019-06-17 00:00:00	no	no	inUse	2018-02-19 12:03:03.883		780	1	\N	\N
13	CRA / 02.11 NP	16	29	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 10:01:18.711		781	1	\N	\N
13	CRA / 02.11 PNP	16	29	21	75	2017-11-22 00:00:00	2019-05-01 00:00:00	no	no	inUse	2018-01-19 10:01:18.711		782	1	\N	\N
251	CES-02/0046051	49	109	21	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2018-02-16 10:57:03.683		783	1	\N	\N
141	TORQ-10 /6JK100045	25	51	21	75	2017-11-24 00:00:00	2019-05-03 00:00:00	no	no	inUse	2018-01-19 10:01:56.773		784	1	\N	\N
121	HTP-13 / 109608214G	5	105	97	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	inCalibration	2016-11-30 00:00:00		785	1	\N	\N
123	HTP-15 / 109611067G	5	105	1	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	collected	2018-02-14 20:16:07.518		786	1	\N	\N
277	T.H - 02 / M1830011	4	7	1	52	2018-01-22 00:00:00	2019-01-21 00:00:00	no	no	collected	2018-02-22 13:34:52.362		787	1	\N	\N
121	HTP-13 / 109608214G	5	105	97	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	collected	2018-02-22 13:34:59.968		788	1	\N	\N
137	PP-21 / K100310	1	1	43	52	2017-01-23 00:00:00	2018-01-22 00:00:00	no	no	collected	2018-01-31 17:05:21.252		789	1	\N	\N
700	TH-22 / 7DE902160	3	6	20	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2017-08-15 00:00:00		790	1	\N	\N
702	TH-24 /  09C0183	3	6	43	52	2016-06-02 00:00:00	2017-06-01 00:00:00	yes	yes	collected	2016-06-09 00:00:00		791	1	\N	\N
702	TH-24 /  09C0183	3	6	43	75	2016-06-02 00:00:00	2017-11-09 00:00:00	yes	yes	collected	2016-06-09 00:00:00		792	1	\N	\N
702	TH-24 /  09C0183	3	6	43	75	2016-06-09 00:00:00	2017-11-16 00:00:00	yes	yes	collected	2016-06-09 00:00:00		793	1	\N	\N
708	PP-04 / K091118	1	2	41	52	2017-11-21 00:00:00	2018-11-20 00:00:00	no	no	inUse	2017-11-21 00:00:00		794	1	\N	\N
711	RA / LYE567	21	45	21	75	2015-02-18 00:00:00	2016-07-27 00:00:00	yes	no	collected	2015-02-18 00:00:00		795	1	\N	\N
22	PP-07 / K101216	1	21	55	52	2017-09-06 00:00:00	2018-09-05 00:00:00	no	no	inUse	2017-09-06 00:00:00		796	1	\N	\N
23	PP-08 / K101217	1	21	46	52	2017-12-27 00:00:00	2018-12-26 00:00:00	no	no	inUse	2018-01-09 14:03:55.226		797	1	\N	\N
24	PP-09 / K101218	1	21	43	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	collected	2018-01-15 12:13:00.392		798	1	\N	\N
30	PP-10 / K101214	1	2	108	52	2017-12-18 00:00:00	2018-12-17 00:00:00	no	no	inUse	2018-01-15 12:10:19.299		799	1	\N	\N
42	CRT / 5.008/14	17	34	21	75	2017-03-02 00:00:00	2018-08-09 00:00:00	no	no	inUse	2017-03-02 00:00:00		800	1	\N	\N
277	T.H - 02 / M1830011	4	7	1	52	2018-01-22 00:00:00	2019-01-21 00:00:00	no	no	inUse	2018-02-23 08:55:17.688		801	1	\N	\N
137	PP-21 / K100310	1	1	43	52	2018-02-19 00:00:00	2019-02-18 00:00:00	no	no	collected	2018-01-31 17:05:21.252		802	1	\N	\N
27	PM-32 / D110127	1	3	11	52	2017-12-14 00:00:00	2018-12-13 00:00:00	no	no	inUse	2018-02-23 09:41:34.946		803	1	\N	\N
140	PP-23 / K100618	1	1	50	52	2017-10-23 00:00:00	2018-10-22 00:00:00	no	no	inUse	2017-10-20 00:00:00		804	1	\N	\N
140	PP-23 / K100618	1	1	43	52	2017-10-23 00:00:00	2018-10-22 00:00:00	no	no	collected	2018-02-23 10:18:34.683		805	1	\N	\N
137	PP-21 / K100310	1	1	50	52	2018-02-19 00:00:00	2019-02-18 00:00:00	no	no	inUse	2018-02-23 10:22:22.911		806	1	\N	\N
587	HTP-24	5	8	1	52	2017-02-03 00:00:00	2018-02-02 00:00:00	no	no	collected	2018-02-26 10:56:20.414		807	1	\N	\N
609	FT-01	12	18	1	52	2017-02-09 00:00:00	2018-02-08 00:00:00	no	no	collected	2018-02-26 10:58:15.222		808	1	\N	\N
43	TH-25 / 7DB600086	3	6	41	52	2016-10-21 00:00:00	2017-10-20 00:00:00	no	no	inCalibration	2018-02-26 13:20:39.611		809	1	\N	\N
47	TH-28 / 7DB600099	3	6	70	52	2017-01-26 00:00:00	2018-01-25 00:00:00	yes	yes	inCalibration	2018-02-26 13:21:37.306		810	1	\N	\N
236	TH-42 / 7DF20024	3	23	5	52	2016-12-01 00:00:00	2017-11-30 00:00:00	no	no	inCalibration	2018-02-26 13:21:51.599		811	1	\N	\N
280	TH-55 \\ 7E0800225	3	6	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	inCalibration	2018-02-26 13:22:05.138		812	1	\N	\N
50	TH-31 / 7DB600099	3	6	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	inCalibration	2018-02-26 13:22:17.333		813	1	\N	\N
43	TH-25 / 7DB600086	3	6	43	52	2016-10-21 00:00:00	2017-10-20 00:00:00	no	no	inCalibration	2018-02-26 13:20:39.611		814	1	\N	\N
1061	TH-60 / HT208-6044	82	158	70	75	2018-02-21 00:00:00	2019-07-31 00:00:00	no	no	collected	2018-02-05 13:53:26.4		815	1	\N	\N
1062	TH-61 / HT208-6048	82	158	21	75	2018-02-21 00:00:00	2019-07-31 00:00:00	no	no	collected	2018-02-05 13:53:07.163		816	1	\N	\N
672	TH - 12 / 38770	4	132	13	52	2018-02-21 00:00:00	2019-02-20 00:00:00	no	no	collected	2014-04-29 00:00:00		817	1	\N	\N
674	TH - 14 / 38769	4	132	13	52	2018-02-21 00:00:00	2019-02-20 00:00:00	no	no	collected	2014-04-29 00:00:00		818	1	\N	\N
1061	TH-60 / HT208-6044	82	158	26	75	2018-02-21 00:00:00	2019-07-31 00:00:00	no	no	collected	2018-02-05 13:53:26.4		819	1	\N	\N
1062	TH-61 / HT208-6048	82	158	68	75	2018-02-21 00:00:00	2019-07-31 00:00:00	no	no	collected	2018-02-05 13:53:07.163		820	1	\N	\N
1062	TH-61 / HT208-6048	82	158	68	75	2018-02-21 00:00:00	2019-07-31 00:00:00	no	no	inUse	2018-02-27 10:11:01.828		821	1	\N	\N
224	TH-41 / 7DBC00319	3	23	43	52	2016-12-01 00:00:00	2017-11-30 00:00:00	no	no	collected	2018-02-27 10:11:38.922		822	1	\N	\N
278	TH-58 \\ 7E0800217	3	6	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	yes	no	collected	2018-02-27 10:11:58.029		823	1	\N	\N
279	TH-54 \\ 7E0800228	3	23	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	collected	2018-02-20 15:17:12.025		824	1	\N	\N
278	TH-58 \\ 7E0800217	3	23	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	yes	no	collected	2018-02-27 10:11:58.029		825	1	\N	\N
224	TH-41 / 7DBC00319	3	23	43	52	2016-12-01 00:00:00	2017-11-30 00:00:00	no	no	collected	2018-02-27 10:11:38.922	*UMIDADE	826	1	\N	\N
293	CES-03	49	108	69	52	2017-02-01 00:00:00	2018-01-31 00:00:00	no	no	inUse	2017-02-01 00:00:00		827	1	\N	\N
245	CES-01	49	108	69	52	2017-05-16 00:00:00	2018-05-15 00:00:00	no	no	collected	2018-02-27 13:45:01.591		828	1	\N	\N
293	CES-03	49	109	69	52	2017-02-01 00:00:00	2018-01-31 00:00:00	no	no	collected	2018-02-27 14:29:52.432		830	1	\N	\N
245	CES-01/0032144	49	108	69	52	2017-05-16 00:00:00	2018-05-15 00:00:00	no	no	collected	2018-02-27 13:45:01.591		831	1	\N	\N
293	CES-03/0031984	49	109	69	52	2017-02-01 00:00:00	2018-01-31 00:00:00	no	no	collected	2018-02-27 14:29:52.432		832	1	\N	\N
293	CES-03/0031984	49	108	69	52	2017-02-01 00:00:00	2018-01-31 00:00:00	no	no	collected	2018-02-27 14:29:52.432		833	1	\N	\N
145	PAQ-02	20	43	21	75	2016-08-03 00:00:00	2018-01-10 00:00:00	no	no	collected	2018-02-20 16:29:52.845		834	1	\N	\N
145	PAQ-02	20	167	21	75	2016-08-03 00:00:00	2018-01-10 00:00:00	no	no	collected	2018-02-20 16:29:52.845		837	1	\N	\N
357	TH-56	38	81	21	52	2017-01-20 00:00:00	2018-01-19 00:00:00	no	no	dropOff	2018-02-28 08:19:53.906		838	1	\N	\N
358	TH-57	38	108	21	52	2017-01-20 00:00:00	2018-01-19 00:00:00	no	no	dropOff	2018-02-28 08:20:15.659		839	1	\N	\N
242	TH-48 / 7DE902157	3	23	43	52	2017-02-28 00:00:00	2018-02-27 00:00:00	no	no	collected	2018-02-28 17:17:42.399		840	1	\N	\N
280	TH-55 \\ 7E0800225	3	6	6	52	2018-02-23 00:00:00	2019-02-22 00:00:00	no	no	inUse	2018-02-28 17:18:36.652		841	1	\N	\N
241	TH-47 / 7DE902156	3	6	43	52	2017-02-28 00:00:00	2018-02-27 00:00:00	no	no	collected	2018-02-28 17:20:32.656		842	1	\N	\N
50	TH-31 / 7DB600099	3	6	7	52	2018-02-23 00:00:00	2019-02-22 00:00:00	no	no	inUse	2018-02-28 17:21:00.512		843	1	\N	\N
239	TH-45 / 7DE902117	3	23	43	52	2017-02-28 00:00:00	2018-02-27 00:00:00	no	no	collected	2018-02-28 17:33:29.781		844	1	\N	\N
236	TH-42 / 7DF20024	3	23	41	52	2018-02-23 00:00:00	2019-02-22 00:00:00	no	no	inUse	2018-02-28 17:34:21.504		845	1	\N	\N
238	TH-44 / 7DE902112	3	23	43	52	2017-02-28 00:00:00	2018-02-27 00:00:00	no	no	collected	2018-02-28 17:37:13.093		846	1	\N	\N
43	TH-25 / 7DB600086	3	6	45	52	2018-02-23 00:00:00	2019-02-22 00:00:00	no	no	inUse	2018-02-28 17:37:46.687		847	1	\N	\N
46	TH-27 / 7DB600088	3	6	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	collected	2018-02-28 17:46:49.337		848	1	\N	\N
47	TH-28 / 7DB600099	3	6	67	52	2018-02-23 00:00:00	2019-02-22 00:00:00	yes	yes	inUse	2018-02-28 17:47:24.526		849	1	\N	\N
258	TH-52 \\ 7DE901217	3	6	64	52	2017-05-15 00:00:00	2018-05-14 00:00:00	no	no	inUse	2017-05-15 00:00:00		850	1	\N	\N
701	TH-23 / 09C0181	3	6	43	52	2017-02-28 00:00:00	2018-02-27 00:00:00	no	no	collected	2018-02-28 17:58:27.417		851	1	\N	\N
280	TH-55 \\ 7E0800225	3	6	6	52	2018-02-20 00:00:00	2019-02-19 00:00:00	no	no	inUse	2018-02-28 17:18:36.652		852	1	\N	\N
236	TH-42 / 7DF20024	3	23	41	52	2018-02-20 00:00:00	2019-02-19 00:00:00	no	no	inUse	2018-02-28 17:34:21.504		853	1	\N	\N
47	TH-28 / 7DB600099	3	6	67	52	2018-02-20 00:00:00	2019-02-19 00:00:00	yes	yes	inUse	2018-02-28 17:47:24.526		854	1	\N	\N
43	TH-25 / 7DB600086	3	6	45	52	2018-02-20 00:00:00	2019-02-19 00:00:00	no	no	inUse	2018-02-28 17:37:46.687		855	1	\N	\N
50	TH-31 / 7DB600099	3	6	7	52	2018-02-20 00:00:00	2019-02-19 00:00:00	no	no	inUse	2018-02-28 17:21:00.512		856	1	\N	\N
355	Testador1	37	79	21	52	2017-01-26 00:00:00	2018-01-25 00:00:00	no	no	inCalibration	2018-03-01 09:23:37.938		857	1	\N	\N
569	ED 8273	52	115	12	27	2016-02-16 00:00:00	2016-08-23 00:00:00	no	no	inRepair	2018-03-01 09:43:39.296		858	1	\N	\N
202	HTP-02	54	24	71	27	2017-01-07 00:00:00	2017-07-15 00:00:00	no	no	dropOff	2018-03-01 09:51:30.867		859	1	\N	\N
270	CAC-01/655473	32	63	23	52	2017-01-24 00:00:00	2018-01-23 00:00:00	no	no	collected	2018-03-01 10:13:33.118		860	1	\N	\N
248	MCR-02/120880831	24	49	21	75	2016-08-12 00:00:00	2018-01-19 00:00:00	no	no	collected	2018-03-01 10:31:59.509		861	1	\N	\N
270	CAC-01/655473	32	63	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	collected	2018-03-01 10:13:33.118		862	1	\N	\N
268	MST-01	30	61	23	104	2017-03-20 00:00:00	2019-03-18 00:00:00	no	no	inUse	2017-03-20 00:00:00		863	1	\N	\N
143	RA / 05.12	21	45	21	75	2016-08-12 00:00:00	2018-01-19 00:00:00	no	no	dropOff	2018-03-01 11:24:29.381		864	1	\N	\N
207	BI-02	55	117	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		865	1	\N	\N
208	BI-03	56	116	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		866	1	\N	\N
210	BI-05	56	116	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		867	1	\N	\N
209	BI-04	56	116	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		868	1	\N	\N
300	DH48S-S	34	65	12	52	2016-10-27 00:00:00	2017-10-26 00:00:00	no	no	registered	2018-03-02 17:57:41.005		869	1	\N	\N
206	BI-01	55	117	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		870	1	\N	\N
211	Zera	57	118	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		871	1	\N	\N
263	MULT.P-01 /1273581	33	64	24	52	2016-05-13 00:00:00	2017-05-12 00:00:00	no	no	registered	2018-03-05 10:59:44.925		872	1	\N	\N
354	Perfil-1/S2K-01125	40	165	21	52	2017-01-16 00:00:00	2018-01-15 00:00:00	no	no	collected	2018-03-05 11:00:04.002		873	1	\N	\N
360	MULT-15/25359248	39	82	26	52	2017-01-24 00:00:00	2018-01-23 00:00:00	no	no	registered	2018-03-05 11:01:33.81		874	1	\N	\N
360	MULT-15/25359248	39	82	26	52	2017-01-24 00:00:00	2018-01-23 00:00:00	no	no	registered	2018-03-05 11:01:33.81	Procurar	875	1	\N	\N
288	MULT-17	13	19	1	52	2017-01-27 00:00:00	2018-01-26 00:00:00	no	no	registered	2018-03-05 11:03:36.625		876	1	\N	\N
350	ETF-03/3233	8	14	1	52	2017-03-09 00:00:00	2018-03-08 00:00:00	no	no	registered	2018-03-05 11:04:04.07		877	1	\N	\N
354	Perfil-1/S2K-01125	40	165	69	52	2017-01-16 00:00:00	2018-01-15 00:00:00	no	no	collected	2018-03-05 11:00:04.002		878	1	\N	\N
354	Perfil-1/S2K-01125	40	168	69	52	2017-01-16 00:00:00	2018-01-15 00:00:00	no	no	collected	2018-03-05 11:00:04.002		880	1	\N	\N
35	BI-05	53	116	71	27	2016-04-27 00:00:00	2016-11-02 00:00:00	no	no	inUse	2016-04-27 00:00:00		881	1	\N	\N
94	PP-17 / K110428	1	1	54	52	2017-08-08 00:00:00	2018-08-07 00:00:00	no	no	inUse	2017-08-08 00:00:00		882	1	\N	\N
569	ED 8273	52	115	12	27	2016-02-16 00:00:00	2016-08-23 00:00:00	no	no	registered	2018-03-06 13:15:22.702		883	1	\N	\N
35	BI-05	53	116	71	27	2016-04-27 00:00:00	2016-11-02 00:00:00	no	no	registered	2018-03-06 13:15:38.811		884	1	\N	\N
63	PM-37 / D101046	1	3	57	52	2017-11-05 00:00:00	2018-11-04 00:00:00	no	no	inUse	2017-11-05 00:00:00		885	1	\N	\N
1070	BI-06	59	155	43	52	2017-03-27 00:00:00	2018-03-26 00:00:00	no	no	registered	2018-03-07 11:10:32.805		886	0	\N	\N
1070	BI-06	59	155	71	52	2017-03-27 00:00:00	2018-03-26 00:00:00	no	no	registered	2018-03-07 11:10:32.805		887	1	\N	\N
1070	BI-06	59	155	71	52	2017-03-27 00:00:00	2018-03-26 00:00:00	no	no	inUse	2018-03-07 11:11:15.644		888	1	\N	\N
211	ZERA	57	118	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	inUse	2017-08-02 00:00:00		890	1	\N	\N
216	ZERA	59	118	71	52	2017-03-25 00:00:00	2018-03-24 00:00:00	yes	no	collected	2014-11-15 00:00:00		893	1	\N	\N
216	ZERA	59	118	71	25	2017-03-25 00:00:00	2017-09-16 00:00:00	yes	no	collected	2014-11-15 00:00:00		889	1	\N	\N
211	ZERA	57	118	71	27	2018-03-02 00:00:00	2018-09-07 00:00:00	no	no	collected	2018-03-07 11:13:51.917		891	1	\N	\N
211	ZERA	57	118	71	26	2018-03-02 00:00:00	2018-08-31 00:00:00	no	no	collected	2018-03-07 11:13:51.917		892	1	\N	\N
57	VOLT-01	7	25	16	52	2017-03-07 00:00:00	2018-03-06 00:00:00	no	no	collected	2018-03-07 13:34:39.041		894	1	\N	\N
146	VOLT-04	7	25	16	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2017-11-23 00:00:00		895	1	\N	\N
52	VOLT-02	7	27	14	52	2018-01-11 00:00:00	2019-01-10 00:00:00	no	no	inUse	2018-03-07 13:35:27.972		896	1	\N	\N
57	VOLT-01	7	25	43	52	2017-03-07 00:00:00	2018-03-06 00:00:00	no	no	collected	2018-03-07 13:34:39.041		897	1	\N	\N
1071	TH-62 / 604468	82	132	26	75	2018-02-23 00:00:00	2019-08-02 00:00:00	no	no	registered	2018-03-07 14:22:57.738		898	0	\N	\N
1071	TH-62 / 604468	82	132	26	75	2018-02-23 00:00:00	2019-08-02 00:00:00	no	no	collected	2018-03-07 14:23:14.764		899	1	\N	\N
1072	TH-63/604421	4	132	26	75	2018-02-23 00:00:00	2019-08-02 00:00:00	no	no	registered	2018-03-07 14:24:45.535		900	0	\N	\N
1072	TH-63/604421	4	132	26	75	2018-02-23 00:00:00	2019-08-02 00:00:00	no	no	collected	2018-03-07 14:24:57.513		901	1	\N	\N
233	VOLT-22	7	10	43	52	2017-03-07 00:00:00	2018-03-06 00:00:00	no	no	collected	2018-03-07 15:48:06.778		902	1	\N	\N
233	VOLT-22	7	26	43	52	2017-03-07 00:00:00	2018-03-06 00:00:00	no	no	collected	2018-03-07 15:48:06.778		903	1	\N	\N
222	VOLT-23	7	26	18	52	2018-01-11 00:00:00	2019-01-10 00:00:00	no	no	inUse	2018-03-07 15:48:43.07		904	1	\N	\N
1061	TH-60 / HT208-6044	82	158	26	75	2018-02-21 00:00:00	2019-07-31 00:00:00	no	no	inUse	2018-03-07 17:12:49.747		905	1	\N	\N
275	TORQ-12/T045360	28	55	21	75	2015-06-01 00:00:00	2016-11-07 00:00:00	no	no	registered	2018-03-07 17:46:36.498		906	1	\N	\N
352	Testador2	37	79	76	52	2017-03-06 00:00:00	2018-03-05 00:00:00	no	no	collected	2018-03-07 17:47:23.981		907	1	\N	\N
344	CT-1	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		910	1	\N	\N
346	CT-3	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		911	1	\N	\N
347	CT-4	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		912	1	\N	\N
348	CT-5	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		913	1	\N	\N
345	CT-2	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		914	1	\N	\N
349	CT-6	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		915	1	\N	\N
40	BM-01	61	116	72	27	2017-12-26 00:00:00	2018-07-03 00:00:00	no	no	inUse	2017-06-30 00:00:00		916	1	\N	\N
85	C TEMP - 07	36	170	90	52	2013-03-25 00:00:00	2014-03-24 00:00:00	yes	no	collected	2013-03-25 00:00:00		940	1	\N	\N
85	C TEMP - 07	36	170	90	52	2013-03-25 00:00:00	2014-03-24 00:00:00	no	no	inUse	2018-03-07 20:41:42.372		941	1	\N	\N
235	BP-07	62	121	72	27	2017-09-25 00:00:00	2018-04-02 00:00:00	no	no	inUse	2017-09-04 00:00:00		942	1	\N	\N
234	BP-06	62	114	72	27	2017-09-26 00:00:00	2018-04-03 00:00:00	no	no	inUse	2017-09-04 00:00:00		943	1	\N	\N
39	BP-05	62	114	72	27	2017-12-16 00:00:00	2018-06-23 00:00:00	no	no	inUse	2017-07-14 00:00:00		944	1	\N	\N
41	BM-02	61	116	72	27	2017-12-27 00:00:00	2018-07-04 00:00:00	no	no	inUse	2017-07-31 00:00:00		945	1	\N	\N
131	BM-03	61	151	72	27	2017-12-27 00:00:00	2018-07-04 00:00:00	yes	no	inUse	2018-03-07 21:51:55.282		946	1	\N	\N
132	BM-04	61	116	72	27	2017-12-28 00:00:00	2018-07-05 00:00:00	no	no	inUse	2017-07-31 00:00:00		947	1	\N	\N
133	BM-05	61	116	72	27	2018-02-02 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-15 00:00:00		948	1	\N	\N
38	BP-01	62	114	72	27	2017-12-15 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-17 00:00:00		949	1	\N	\N
37	BP-02	62	114	72	27	2017-12-22 00:00:00	2018-06-29 00:00:00	no	no	inUse	2017-06-30 00:00:00		950	1	\N	\N
593	BP 10	62	129	79	27	2012-05-31 00:00:00	2012-12-06 00:00:00	yes	no	collected	2012-05-31 00:00:00		951	1	\N	\N
131	BM-03	61	151	72	27	2017-12-27 00:00:00	2018-07-04 00:00:00	yes	no	registered	2018-03-07 22:07:46.424		952	1	\N	\N
130	BP-04	62	114	72	27	2017-11-25 00:00:00	2018-06-02 00:00:00	no	no	inUse	2017-06-27 00:00:00		953	1	\N	\N
85	C TEMP - 07	36	170	90	52	2013-03-25 00:00:00	2014-03-24 00:00:00	no	no	registered	2018-03-08 09:05:42.932		954	1	\N	\N
1001	Injetora15 / 1205Q048	35	77	25	52	2017-01-24 00:00:00	2018-01-23 00:00:00	no	no	inUse	2017-08-02 00:00:00		965	1	\N	\N
343	Injetora14	35	75	25	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	inUse	2017-08-02 00:00:00		966	1	\N	\N
1001	Injetora15 / 1205Q048	35	77	25	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	inUse	2017-08-02 00:00:00		967	1	\N	\N
330	Injetora1	35	66	25	52	2017-06-01 00:00:00	2018-05-31 00:00:00	no	no	inUse	2017-08-01 00:00:00		968	1	\N	\N
1069	MULT-23 / 22030278	11	94	66	75	2018-01-17 00:00:00	2019-06-26 00:00:00	no	no	inUse	2018-03-08 11:09:16.495		969	1	\N	\N
330	Injetora1	35	66	25	52	2017-08-01 00:00:00	2018-07-31 00:00:00	no	no	inUse	2017-08-01 00:00:00		970	1	\N	\N
157	MULT-12 / EB294000257	11	96	12	52	2017-11-22 00:00:00	2018-11-21 00:00:00	no	no	inCalibration	2018-03-08 11:26:54.319		971	1	\N	\N
559	MULT 02 / 88450278	11	97	94	52	2016-06-07 00:00:00	2017-06-06 00:00:00	no	no	inCalibration	2018-03-08 11:33:01.686		972	1	\N	\N
201	C TEMP - 10	36	169	27	52	2017-08-28 00:00:00	2018-08-27 00:00:00	no	no	inUse	2018-03-08 11:57:26.101		973	1	\N	\N
86	C TEMP - 08	36	169	27	52	2017-08-29 00:00:00	2018-08-28 00:00:00	no	no	inUse	2018-03-08 12:00:38.449		974	1	\N	\N
83	C TEMP - 05	36	169	25	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2018-03-08 12:04:00.524		975	1	\N	\N
696	MULT 06 / 11470123	11	95	1	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-23 00:00:00		977	1	\N	\N
86	C TEMP - 08	36	171	27	52	2017-08-29 00:00:00	2018-08-28 00:00:00	no	no	inUse	2018-03-08 12:00:38.449		979	1	\N	\N
346	CT-3	36	169	25	52	2017-08-23 00:00:00	2018-08-22 00:00:00	no	no	inUse	2017-08-11 00:00:00		980	1	\N	\N
674	TH - 14 / 38769	4	132	4	52	2018-02-21 00:00:00	2019-02-20 00:00:00	no	no	inUse	2018-03-08 12:53:26.106		981	1	\N	\N
85	C TEMP - 07	36	170	90	52	2018-01-25 00:00:00	2019-01-24 00:00:00	no	no	inUse	2018-03-08 12:57:56.788		982	1	\N	\N
85	C TEMP - 07	36	169	90	52	2018-01-25 00:00:00	2019-01-24 00:00:00	no	no	inUse	2018-03-08 12:57:56.788		983	1	\N	\N
85	C TEMP - 07	36	169	25	52	2018-01-25 00:00:00	2019-01-24 00:00:00	no	no	inUse	2018-03-08 12:57:56.788		984	1	\N	\N
696	MULT 06 / 11470123	11	95	21	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-23 00:00:00		985	1	\N	\N
1001	Injetora15	35	77	25	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	inUse	2017-08-02 00:00:00		986	1	\N	\N
83	C TEMP - 05	36	169	25	52	2018-01-25 00:00:00	2019-01-24 00:00:00	no	no	inUse	2018-03-08 12:04:00.524		987	1	\N	\N
686	C TEMP - 02	36	169	25	52	2018-01-25 00:00:00	2019-01-24 00:00:00	no	no	inUse	2018-03-08 13:17:02.848		988	1	\N	\N
686	C TEMP - 02	36	169	25	52	2018-01-03 00:00:00	2019-01-02 00:00:00	yes	no	registered	2018-03-08 13:18:00.8		989	1	\N	\N
345	CT-2	36	169	25	52	2017-08-25 00:00:00	2018-08-24 00:00:00	no	no	inUse	2017-08-11 00:00:00		990	1	\N	\N
201	CT - 10	36	169	27	52	2017-08-28 00:00:00	2018-08-27 00:00:00	no	no	inUse	2018-03-08 11:57:26.101		991	1	\N	\N
201	CT-10	36	169	27	52	2017-08-28 00:00:00	2018-08-27 00:00:00	no	no	inUse	2018-03-08 11:57:26.101		992	1	\N	\N
86	CT-08	36	171	27	52	2017-08-29 00:00:00	2018-08-28 00:00:00	no	no	inUse	2018-03-08 12:00:38.449		993	1	\N	\N
85	CT-07	36	169	25	52	2018-01-25 00:00:00	2019-01-24 00:00:00	no	no	inUse	2018-03-08 12:57:56.788		994	1	\N	\N
344	CT-01	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		995	1	\N	\N
345	CT-02	36	169	25	52	2017-08-25 00:00:00	2018-08-24 00:00:00	no	no	inUse	2017-08-11 00:00:00		996	1	\N	\N
349	CT-06	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		998	1	\N	\N
347	CT-04	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		999	1	\N	\N
348	CT-05	36	169	25	52	2017-08-11 00:00:00	2018-08-10 00:00:00	no	no	inUse	2017-08-11 00:00:00		1000	1	\N	\N
1058	PAQ-07 / 71130329308	14	154	21	75	2017-12-28 00:00:00	2019-06-06 00:00:00	no	no	inCalibration	2018-03-08 15:28:44.49		1003	1	\N	\N
295	PAQ-05 / 06911085	14	20	4	75	2016-11-28 00:00:00	2018-05-07 00:00:00	no	no	inUse	2016-11-28 00:00:00		1004	1	\N	\N
296	PAQ-06 / BG003241	14	20	4	75	2017-01-20 00:00:00	2018-06-29 00:00:00	no	no	inUse	2017-01-20 00:00:00		1005	1	\N	\N
346	CT-03	36	169	25	52	2017-08-23 00:00:00	2018-08-22 00:00:00	no	no	inUse	2017-08-11 00:00:00		997	1	\N	\N
145	PAQ-02	20	167	21	75	2016-08-03 00:00:00	2018-01-10 00:00:00	no	no	inCalibration	2018-03-08 15:26:21.27		1001	1	\N	\N
1064	PAQ-11 / 14824436	20	42	66	75	2018-01-08 00:00:00	2019-06-17 00:00:00	no	no	inUse	2018-02-19 11:57:05.444		1002	1	\N	\N
28	PM-33 / D110128	1	3	58	52	2017-05-09 00:00:00	2018-05-08 00:00:00	no	no	inUse	2017-05-09 00:00:00		1008	1	\N	\N
701	TH-23 / 09C0181	3	6	43	52	2017-02-28 00:00:00	2018-02-27 00:00:00	no	no	inCalibration	2018-03-09 15:21:38.989		1009	1	\N	\N
46	TH-27 / 7DB600088	3	6	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	inCalibration	2018-03-09 15:21:58.132		1010	1	\N	\N
238	TH-44 / 7DE902112	3	23	43	52	2017-02-28 00:00:00	2018-02-27 00:00:00	no	no	inCalibration	2018-03-09 15:22:15.689		1011	1	\N	\N
239	TH-45 / 7DE902117	3	23	43	52	2017-02-28 00:00:00	2018-02-27 00:00:00	no	no	inCalibration	2018-03-09 15:22:33.519		1012	1	\N	\N
241	TH-47 / 7DE902156	3	6	43	52	2017-02-28 00:00:00	2018-02-27 00:00:00	no	no	inCalibration	2018-03-09 15:22:46.916		1013	1	\N	\N
242	TH-48 / 7DE902157	3	23	43	52	2017-02-28 00:00:00	2018-02-27 00:00:00	no	no	inCalibration	2018-03-09 15:23:50.357		1014	1	\N	\N
243	TH-49 7DE902097	3	23	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	inCalibration	2018-03-09 15:23:58.744		1015	1	\N	\N
279	TH-54 \\ 7E0800228	3	23	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	no	no	inCalibration	2018-03-09 15:24:09.143		1016	1	\N	\N
224	TH-41 / 7DBC00319	3	23	43	52	2016-12-01 00:00:00	2017-11-30 00:00:00	no	no	inCalibration	2018-03-09 15:24:21.388	*UMIDADE	1017	1	\N	\N
278	TH-58 \\ 7E0800217	3	23	43	52	2016-11-28 00:00:00	2017-11-27 00:00:00	yes	no	inCalibration	2018-03-09 15:24:42.775		1018	1	\N	\N
670	TH - 10 / 38771	4	132	13	52	2012-02-01 00:00:00	2013-01-30 00:00:00	yes	no	collected	2012-02-01 00:00:00		1019	1	\N	\N
1068	VOLT-26 / 20171201	7	160	43	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inCalibration	2018-03-09 17:00:44.161		1020	1	\N	\N
1067	VOLT-25 	7	160	43	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inCalibration	2018-03-09 17:01:28.393		1021	1	\N	\N
1068	VOLT-26	7	160	43	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inCalibration	2018-03-09 17:00:44.161		1022	1	\N	\N
1066	VOLT-24	7	160	43	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	collected	2018-03-09 17:02:10.615		1023	1	\N	\N
1068	VOLT-26	7	160	43	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	collected	2018-03-09 17:02:22.031		1024	1	\N	\N
1067	VOLT-25 	7	160	43	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	collected	2018-03-09 17:02:36.998		1025	1	\N	\N
233	VOLT-22	7	26	87	52	2017-03-07 00:00:00	2018-03-06 00:00:00	no	no	collected	2018-03-07 15:48:06.778		1026	1	\N	\N
57	VOLT-01	7	25	87	52	2017-03-07 00:00:00	2018-03-06 00:00:00	no	no	collected	2018-03-07 13:34:39.041		1027	1	\N	\N
1073	MULT-16	39	94	21	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	registered	2018-03-12 17:18:06.653		1028	0	\N	\N
1073	MULT-16	39	94	21	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inCalibration	2018-03-12 17:18:22.512		1029	1	\N	\N
559	MULT-02 / 88450278	11	97	12	52	2018-03-07 00:00:00	2019-03-06 00:00:00	no	no	inCalibration	2018-03-08 11:33:01.686		1030	1	\N	\N
559	MULT-02 / 88450278	11	97	12	52	2018-03-07 00:00:00	2019-03-06 00:00:00	no	no	inUse	2018-03-12 17:24:14.927		1031	1	\N	\N
1067	VOLT-25 	7	160	18	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inUse	2018-03-13 09:11:49.716		1032	1	\N	\N
222	VOLT-23	7	26	1	52	2018-01-11 00:00:00	2019-01-10 00:00:00	no	no	inUse	2018-03-07 15:48:43.07		1033	1	\N	\N
355	Testador1	37	79	76	52	2018-03-06 00:00:00	2019-03-05 00:00:00	no	no	inUse	2018-03-13 12:19:14.446		1034	1	\N	\N
1066	VOLT-24	7	160	14	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inUse	2018-03-13 15:29:33.592		1035	1	\N	\N
52	VOLT-02	7	27	43	52	2018-01-11 00:00:00	2019-01-10 00:00:00	no	no	collected	2018-03-13 15:30:19.845		1036	1	\N	\N
696	MULT 06 / 11470123	11	95	1	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-23 00:00:00		1037	1	\N	\N
559	MULT-02 / 88450278	11	97	21	52	2018-03-07 00:00:00	2019-03-06 00:00:00	no	no	inUse	2018-03-12 17:24:14.927		1038	1	\N	\N
123	HTP-15 / 109611067G	5	105	71	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	inUse	2018-03-13 16:24:42.968		1039	1	\N	\N
425	HPT-02 / 3201194	5	24	40	52	2017-03-14 00:00:00	2018-03-13 00:00:00	no	no	collected	2018-03-13 17:11:18.152		1040	1	\N	\N
704	HPT-05 / 3201091	5	24	40	52	2017-08-16 00:00:00	2018-08-15 00:00:00	no	no	inUse	2018-02-02 14:11:46.187		1041	1	\N	\N
66	VOLT-07	7	12	12	52	2014-01-20 00:00:00	2015-01-19 00:00:00	yes	no	collected	2014-01-20 00:00:00		1042	1	\N	\N
66	VOLT-07	7	12	12	52	2014-01-20 00:00:00	2015-01-19 00:00:00	no	no	collected	2014-01-20 00:00:00		1043	1	\N	\N
1074	MULT-16 / 30780021	39	172	1	52	2018-03-22 00:00:00	2019-03-21 00:00:00	no	no	registered	2018-03-15 11:23:06.506		1045	0	\N	\N
1074	MULT-24 / 30780021	39	172	1	52	2018-03-22 00:00:00	2019-03-21 00:00:00	no	no	registered	2018-03-15 11:23:06.506		1046	1	\N	\N
1074	MULT-24 / 30780021	39	172	1	52	2018-03-22 00:00:00	2019-03-21 00:00:00	no	no	collected	2018-03-15 11:23:48.913		1047	1	\N	\N
145	PAQ-02	20	167	21	75	2018-03-14 00:00:00	2019-08-21 00:00:00	no	no	collected	2018-03-16 16:44:58.55		1048	1	\N	\N
87	PM-40 / D101125	1	3	65	52	2017-03-20 00:00:00	2018-03-19 00:00:00	no	no	collected	2018-03-19 10:23:31.253		1049	1	\N	\N
712	PM-30 / D091001	1	3	65	52	2017-12-28 00:00:00	2018-12-27 00:00:00	no	no	inUse	2018-03-19 10:23:45.885		1050	1	\N	\N
87	PM-40 / D101125	1	3	13	52	2017-03-20 00:00:00	2018-03-19 00:00:00	no	no	collected	2018-03-19 10:23:31.253		1051	1	\N	\N
189	ANE-01	45	82	12	52	2018-01-30 00:00:00	2019-01-29 00:00:00	no	no	collected	2018-03-20 11:02:25.021		1052	1	\N	\N
189	ANE-01	45	173	12	52	2018-01-30 00:00:00	2019-01-29 00:00:00	no	no	collected	2018-03-20 11:02:25.021		1054	1	\N	\N
145	PAQ-02	20	167	21	75	2018-03-14 00:00:00	2019-08-21 00:00:00	no	no	inUse	2018-03-20 12:45:18.838		1055	1	\N	\N
195	08.39/55	14	20	21	75	2012-02-07 00:00:00	2013-07-16 00:00:00	yes	no	registered	2018-03-20 12:46:54.006		1056	1	\N	\N
225	08.39/45	14	20	21	75	2009-06-30 00:00:00	2010-12-07 00:00:00	yes	no	registered	2018-03-20 12:47:27.93		1057	1	\N	\N
151	08.39/49	14	20	21	75	2012-02-07 00:00:00	2013-07-16 00:00:00	yes	no	registered	2018-03-20 12:47:41.183		1058	1	\N	\N
550	PAQ/06904252	14	20	21	75	2016-10-20 00:00:00	2018-03-29 00:00:00	no	no	collected	2018-03-20 12:48:32.659		1059	1	\N	\N
550	PAQ-13 / 06904252	14	20	21	75	2016-10-20 00:00:00	2018-03-29 00:00:00	no	no	collected	2018-03-20 12:48:32.659		1060	1	\N	\N
1	CTL / 14.407	19	41	21	75	2016-10-20 00:00:00	2018-03-29 00:00:00	no	no	collected	2018-03-20 16:13:37.389		1061	1	\N	\N
107	CRA / 011.00PNP	16	29	21	75	2016-10-24 00:00:00	2018-04-02 00:00:00	no	no	collected	2018-03-20 16:14:01.526		1062	1	\N	\N
44	CRA / 006PNP	16	28	21	75	2016-10-24 00:00:00	2018-04-02 00:00:00	no	no	collected	2018-03-20 16:14:14.513		1063	1	\N	\N
248	MCR-02/120880831	24	49	21	75	2016-08-12 00:00:00	2018-01-19 00:00:00	no	no	inCalibration	2018-03-20 16:14:36.442		1064	1	\N	\N
88	PM-41 / D100946	1	3	1	52	2017-11-17 00:00:00	2018-11-16 00:00:00	no	no	inUse	2017-12-06 00:00:00		1065	1	\N	\N
159	PP-24 / H090303	1	1	101	0	2016-07-04 00:00:00	2016-07-04 00:00:00	yes	no	inRepair	2018-03-21 11:05:38.405		1066	1	\N	\N
1075	ESP-02 / BAG-27	51	111	33	52	2018-03-09 00:00:00	2019-03-08 00:00:00	no	no	registered	2018-03-21 12:39:25.636		1067	0	\N	\N
1075	ESP-02 / BAG-27	51	111	33	52	2018-03-09 00:00:00	2019-03-08 00:00:00	no	no	collected	2018-03-21 12:40:29.412		1068	1	\N	\N
232	PP-32 / k091111	1	2	2	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	inUse	2017-12-20 00:00:00		1069	1	\N	\N
124	PM-46 / D101039	1	3	2	52	2017-12-06 00:00:00	2018-12-05 00:00:00	no	no	inUse	2018-03-22 08:36:12.226		1070	1	\N	\N
29	PM-34 / D110124	1	3	43	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	collected	2018-01-06 16:04:05.088	Sala instrumentação	1071	1	\N	\N
87	PM-40 / D101125	1	3	43	52	2017-03-20 00:00:00	2018-03-19 00:00:00	no	no	collected	2018-03-19 10:23:31.253		1072	1	\N	\N
675	TH-15 / 38767	4	132	43	52	2012-03-15 00:00:00	2013-03-14 00:00:00	no	no	collected	2012-03-15 00:00:00		1073	1	\N	\N
673	TH-13 / 38765	4	132	43	52	2014-04-29 00:00:00	2015-04-28 00:00:00	no	no	collected	2014-04-29 00:00:00		1074	1	\N	\N
137	PP-21 / K100310	1	1	43	52	2018-02-19 00:00:00	2019-02-18 00:00:00	no	no	collected	2018-03-24 11:51:49.518		1075	1	\N	\N
1073	MULT-16	39	94	21	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inUse	2018-03-26 10:17:22.028		1078	1	\N	\N
230	PP-30 / K100616	1	2	50	52	2017-12-27 00:00:00	2018-12-26 00:00:00	no	no	inUse	2018-03-24 11:52:12.363		1076	1	\N	\N
559	MULT-02 / 88450278	11	97	12	52	2018-03-07 00:00:00	2019-03-06 00:00:00	no	no	inUse	2018-03-12 17:24:14.927		1077	1	\N	\N
1070	BI-06	59	155	71	52	2018-03-26 00:00:00	2019-03-25 00:00:00	no	no	inUse	2018-03-07 11:11:15.644		1079	1	\N	\N
235	BP-07	62	121	72	27	2018-03-28 00:00:00	2018-10-03 00:00:00	no	no	inUse	2017-09-04 00:00:00		1080	1	\N	\N
217	BI-03	59	116	71	52	2017-12-23 00:00:00	2018-12-22 00:00:00	no	no	inUse	2017-01-04 00:00:00		1081	1	\N	\N
218	BI-04	59	116	71	52	2017-12-24 00:00:00	2018-12-23 00:00:00	no	no	inUse	2017-01-06 00:00:00		1082	1	\N	\N
219	BI-05	59	116	71	52	2017-12-26 00:00:00	2018-12-25 00:00:00	no	no	inUse	2017-01-15 00:00:00		1083	1	\N	\N
215	BI-02	59	114	71	52	2017-12-28 00:00:00	2018-12-27 00:00:00	no	no	inUse	2017-01-02 00:00:00		1084	1	\N	\N
214	BI-01	59	113	71	52	2017-12-27 00:00:00	2018-12-26 00:00:00	no	no	inUse	2016-12-30 00:00:00		1085	1	\N	\N
219	BI-05	59	116	71	52	2018-03-24 00:00:00	2019-03-23 00:00:00	no	no	inUse	2017-01-15 00:00:00		1086	1	\N	\N
218	BI-04	59	116	71	52	2018-03-23 00:00:00	2019-03-22 00:00:00	no	no	inUse	2017-01-06 00:00:00		1087	1	\N	\N
217	BI-03	59	116	71	52	2018-03-22 00:00:00	2019-03-21 00:00:00	no	no	inUse	2017-01-04 00:00:00		1088	1	\N	\N
215	BI-02	59	114	71	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inUse	2017-01-02 00:00:00		1089	1	\N	\N
214	BI-01	59	113	71	52	2018-03-20 00:00:00	2019-03-19 00:00:00	no	no	inUse	2016-12-30 00:00:00		1090	1	\N	\N
234	BP-06	62	114	72	27	2018-03-29 00:00:00	2018-10-04 00:00:00	no	no	inUse	2017-09-04 00:00:00		1091	1	\N	\N
161	MCR-01	24	50	21	75	2016-10-20 00:00:00	2018-03-29 00:00:00	no	no	collected	2018-03-28 19:47:27.809		1092	1	\N	\N
301	Tridimensional/00221402	42	85	21	52	2017-03-14 00:00:00	2018-03-13 00:00:00	no	no	inCalibration	2018-03-28 19:47:47.41		1093	1	\N	\N
1071	TH-62 / 604468	82	132	31	75	2018-02-23 00:00:00	2019-08-02 00:00:00	no	no	inUse	2018-04-03 10:35:51.613		1094	1	\N	\N
100	CRT / S.008/15	17	34	21	75	2018-03-05 00:00:00	2019-08-12 00:00:00	no	no	collected	2018-04-03 12:43:33.262		1095	1	\N	\N
95	CRT / 04.11	17	152	21	75	2018-03-05 00:00:00	2019-08-12 00:00:00	no	no	collected	2018-04-03 12:44:24.028		1096	1	\N	\N
1058	PAQ-07 / 71130329308	14	154	21	75	2018-03-01 00:00:00	2019-08-08 00:00:00	no	no	inCalibration	2018-03-08 15:28:44.49		1097	1	\N	\N
25	TH-02 / Q116844	4	103	12	52	2018-03-13 00:00:00	2019-03-12 00:00:00	no	no	inUse	2018-04-03 12:45:28.796		1098	1	\N	\N
262	TORQ-11/ 1604620052	25	52	21	75	2018-03-07 00:00:00	2019-08-14 00:00:00	no	no	collected	2018-04-03 12:46:00.465		1099	1	\N	\N
1065	PAQ-12 / 71130329308	14	154	21	75	2017-02-05 00:00:00	2018-07-15 00:00:00	no	no	registered	2018-04-03 13:08:04.969		1100	1	\N	\N
301	Tridimensional/00221402	42	85	21	52	2018-03-16 00:00:00	2019-03-15 00:00:00	no	no	inUse	2018-04-03 15:40:28.496		1101	1	\N	\N
1072	TH-63/604421	4	132	31	75	2018-02-23 00:00:00	2019-08-02 00:00:00	no	no	inUse	2018-04-03 15:42:09.34		1102	1	\N	\N
29	PM-34 / D110124	1	3	62	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	inUse	2018-04-04 08:35:14.449	Sala instrumentação	1103	1	\N	\N
62	PM-36 / D101047	1	3	43	52	2017-08-04 00:00:00	2018-08-03 00:00:00	no	no	collected	2018-04-04 08:36:23.981	DANIFICADO	1104	1	\N	\N
25	TH-02 / Q116844	4	103	12	52	2018-03-09 00:00:00	2019-03-08 00:00:00	no	no	inUse	2018-04-03 12:45:28.796		1105	1	\N	\N
262	TORQ-11/ 1604620052	25	52	21	75	2018-03-07 00:00:00	2019-08-14 00:00:00	no	no	inUse	2018-04-04 10:35:58.354		1106	1	\N	\N
100	CRT / S.008/15	17	34	21	75	2018-03-05 00:00:00	2019-08-12 00:00:00	no	no	inUse	2018-04-04 10:36:15.303		1107	1	\N	\N
95	CRT / 04.11	17	152	21	75	2018-03-05 00:00:00	2019-08-12 00:00:00	no	no	inUse	2018-04-04 10:36:38.453		1108	1	\N	\N
278	TH-58 \\ 7E0800217	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	yes	no	collected	2018-04-04 13:16:06.098		1109	1	\N	\N
279	TH-54 \\ 7E0800228	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inCalibration	2018-03-09 15:24:09.143		1110	1	\N	\N
279	TH-54 \\ 7E0800228	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:25:57.693		1111	1	\N	\N
239	TH-45 / 7DE902117	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:27:37.932		1112	1	\N	\N
238	TH-44 / 7DE902112	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:28:01.647		1113	1	\N	\N
46	TH-27 / 7DB600088	3	6	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:29:21.175		1114	1	\N	\N
241	TH-47 / 7DE902156	3	6	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:30:16.76		1115	1	\N	\N
242	TH-48 / 7DE902157	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:31:04.718		1116	1	\N	\N
701	TH-23 / 09C0181	3	6	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:31:46.426		1117	1	\N	\N
243	TH-49 7DE902097	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:33:51.374		1118	1	\N	\N
224	TH-41 / 7DBC00319	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inUse	2018-04-04 13:55:00.004	*UMIDADE	1119	1	\N	\N
224	TH-41 / 7DBC00319	3	23	43	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-05 09:09:31.556	*UMIDADE	1120	1	\N	\N
701	TH-23 / 09C0181	3	6	9	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inUse	2018-04-06 11:10:02.112		1121	1	\N	\N
360	MULT-15 / 40920080	39	94	70	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	registered	2018-03-05 11:01:33.81		1122	1	\N	\N
28	PM-33 / D110128	1	3	43	52	2017-05-09 00:00:00	2018-05-08 00:00:00	no	no	collected	2018-04-09 12:34:37.798		1123	1	\N	\N
29	PM-34 / D110124	1	3	58	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	inUse	2018-04-04 08:35:14.449	Sala instrumentação	1124	1	\N	\N
94	PP-17 / K110428	1	1	43	52	2017-08-08 00:00:00	2018-08-07 00:00:00	no	no	collected	2018-04-09 12:35:53.417		1125	1	\N	\N
92	PP-15 / K110401	1	1	54	52	2018-01-18 00:00:00	2019-01-17 00:00:00	no	no	inUse	2018-04-09 12:36:32.341		1126	1	\N	\N
116	PM-45 / D101037	1	3	43	52	2017-08-07 00:00:00	2018-08-06 00:00:00	no	no	collected	2018-04-09 12:37:11.393		1127	1	\N	\N
61	PM-38 / D1011032	1	3	63	52	2017-12-11 00:00:00	2018-12-10 00:00:00	no	no	inUse	2018-04-09 12:37:29.3		1128	1	\N	\N
193	PP-26 / K130202	1	1	55	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2018-02-02 21:17:57.276		1129	1	\N	\N
30	PP-10 / K101214	1	2	53	52	2017-12-18 00:00:00	2018-12-17 00:00:00	no	no	inUse	2018-01-15 12:10:19.299		1130	1	\N	\N
47	TH-28 / 7DB600099	3	6	43	52	2018-02-20 00:00:00	2019-02-19 00:00:00	yes	yes	collected	2018-04-09 14:01:16.949		1131	1	\N	\N
279	TH-54 \\ 7E0800228	3	23	67	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	registered	2018-04-09 14:01:41.746		1132	1	\N	\N
279	TH-54 \\ 7E0800228	3	23	67	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inUse	2018-04-09 14:01:59.948		1133	1	\N	\N
136	PP-20 / K091005	1	21	6	52	2017-09-04 00:00:00	2018-09-03 00:00:00	no	no	inUse	2017-09-04 00:00:00	THS	1134	1	\N	\N
54	PP-12 / K091116	1	1	7	52	2017-12-08 00:00:00	2018-12-07 00:00:00	no	no	inUse	2017-12-08 00:00:00	THS	1135	1	\N	\N
1059	PP-35 / C140409	65	155	106	52	2017-12-22 00:00:00	2018-12-21 00:00:00	no	no	inUse	2017-12-29 21:01:22.44	THS	1136	1	\N	\N
197	24280339-MULT-13	11	94	94	52	1900-01-01 00:00:00	1900-12-31 00:00:00	yes	no	registered	2018-04-09 16:13:58.054		1137	1	\N	\N
57	VOLT-01	7	25	43	52	2018-03-14 00:00:00	2019-03-13 00:00:00	no	no	collected	2018-03-07 13:34:39.041		1138	1	\N	\N
233	VOLT-22	7	26	43	52	2018-03-15 00:00:00	2019-03-14 00:00:00	no	no	collected	2018-03-07 15:48:06.778		1139	1	\N	\N
248	MCR-02/120880831	24	49	21	75	2018-04-09 00:00:00	2019-09-16 00:00:00	no	no	inUse	2018-04-10 15:26:11.164		1140	1	\N	\N
57	VOLT-01	7	25	43	52	2018-03-15 00:00:00	2019-03-14 00:00:00	no	no	collected	2018-03-07 13:34:39.041		1141	1	\N	\N
1073	MULT-16	39	94	21	52	2018-03-14 00:00:00	2019-03-13 00:00:00	no	no	inUse	2018-03-26 10:17:22.028		1142	1	\N	\N
1074	MULT-24 / 30780021	39	172	1	52	2018-03-26 00:00:00	2019-03-25 00:00:00	no	no	inUse	2018-04-11 12:45:16.501		1143	1	\N	\N
693	P. TEMP. - 01	2	174	1	52	2017-01-26 00:00:00	2018-01-25 00:00:00	no	no	collected	2018-02-20 17:13:19.429		1145	1	\N	\N
50	TH-31 / 7DB600099	3	6	44	52	2018-02-20 00:00:00	2019-02-19 00:00:00	no	no	inUse	2018-02-28 17:21:00.512		1146	1	\N	\N
46	TH-27 / 7DB600088	3	6	11	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	collected	2018-04-04 13:29:21.175		1147	1	\N	\N
45	TH-26 / 7DB600098	3	6	7	52	2017-11-29 00:00:00	2018-11-28 00:00:00	no	no	inUse	2017-11-29 00:00:00		1148	1	\N	\N
256	TH-53 \\ 7DE901121	3	6	43	52	2017-05-15 00:00:00	2018-05-14 00:00:00	no	no	collected	2018-04-17 17:09:11.319		1149	1	\N	\N
280	TH-55 \\ 7E0800225	3	6	43	52	2018-02-20 00:00:00	2019-02-19 00:00:00	no	no	collected	2018-04-17 18:20:01.398		1150	1	\N	\N
241	TH-47 / 7DE902156	3	6	6	52	2018-03-21 00:00:00	2019-03-20 00:00:00	no	no	inUse	2018-04-17 18:20:13.273		1151	1	\N	\N
703	HPT-20 / 3202406	5	24	13	52	2011-08-30 00:00:00	2012-08-28 00:00:00	yes	no	collected	2011-08-30 00:00:00		1152	1	\N	\N
87	PM-40 / D101125	1	3	43	52	2018-03-13 00:00:00	2019-03-12 00:00:00	no	no	collected	2018-03-19 10:23:31.253		1153	1	\N	\N
157	MULT-12 / EB294000257	11	96	12	52	2018-03-26 00:00:00	2019-03-25 00:00:00	no	no	inUse	2018-04-18 17:59:08.495		1154	1	\N	\N
158	PAT-01	47	101	12	52	2018-03-26 00:00:00	2019-03-25 00:00:00	no	no	inUse	2018-04-18 17:59:30.369		1155	1	\N	\N
352	Testador2	37	79	76	52	2018-04-06 00:00:00	2019-04-05 00:00:00	no	no	inUse	2018-04-18 18:00:17.493		1156	1	\N	\N
1076	TH-64 / 2016380128	4	132	12	52	2018-04-18 00:00:00	2019-04-17 00:00:00	no	no	registered	2018-04-18 19:20:03.933		1157	0	\N	\N
1076	TH-64 / 2016380128	4	175	12	52	2018-04-18 00:00:00	2019-04-17 00:00:00	no	no	collected	2018-04-18 19:20:59.869		1159	1	\N	\N
675	TH-15 / 38767	4	132	43	52	2018-04-16 00:00:00	2019-04-15 00:00:00	no	no	collected	2012-03-15 00:00:00		1160	1	\N	\N
673	TH-13 / 38765	4	132	43	52	2018-04-16 00:00:00	2019-04-15 00:00:00	no	no	collected	2014-04-29 00:00:00		1161	1	\N	\N
245	CES-01/0032144	49	108	69	52	2018-04-13 00:00:00	2019-04-12 00:00:00	no	no	inUse	2018-04-19 15:49:22.708		1162	1	\N	\N
293	CES-03/0031984	49	108	69	52	2018-04-13 00:00:00	2019-04-12 00:00:00	no	no	inUse	2018-04-19 15:49:45.723		1163	1	\N	\N
1076	TH-64 / 2016380128	4	175	31	52	2018-04-18 00:00:00	2019-04-17 00:00:00	no	no	collected	2018-04-18 19:20:59.869		1164	1	\N	\N
57	VOLT-01	7	25	17	52	2018-03-15 00:00:00	2019-03-14 00:00:00	no	no	inUse	2018-04-30 11:34:27.697		1165	1	\N	\N
1068	VOLT-26	7	160	19	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inUse	2018-04-30 11:35:40.434		1166	1	\N	\N
98	VOLT-03	7	25	43	52	2017-05-11 00:00:00	2018-05-10 00:00:00	no	no	collected	2018-04-30 11:38:52.742		1167	1	\N	\N
194	VOLT-19/1203372403	7	13	43	52	2017-05-11 00:00:00	2018-05-10 00:00:00	no	no	collected	2018-04-30 11:40:02.294		1168	1	\N	\N
107	CRA / 011.00PNP	16	29	21	75	2018-04-02 00:00:00	2019-09-09 00:00:00	no	no	inUse	2018-05-02 14:01:57.984		1169	1	\N	\N
550	PAQ-13 / 06904252	14	20	21	75	2018-08-27 00:00:00	2020-02-03 00:00:00	no	no	inUse	2018-05-04 16:07:39.688		1170	1	\N	\N
550	PAQ-13 / 06904252	14	20	21	75	2018-04-27 00:00:00	2019-10-04 00:00:00	no	no	inUse	2018-05-04 16:07:39.688		1171	1	\N	\N
192	PP-25 / K100918	1	1	1	52	2018-04-27 00:00:00	2019-04-26 00:00:00	no	no	collected	2018-05-07 11:08:30.504	Ths\r\n	1172	1	\N	\N
28	PM-33 / D110128	1	3	43	52	2018-04-19 00:00:00	2019-04-18 00:00:00	no	no	collected	2018-04-09 12:34:37.798		1173	1	\N	\N
94	PP-17 / K110428	1	1	43	52	2018-04-20 00:00:00	2019-04-19 00:00:00	no	no	collected	2018-04-09 12:35:53.417		1174	1	\N	\N
116	PM-45 / D101037	1	3	43	52	2018-04-14 00:00:00	2019-04-13 00:00:00	no	no	collected	2018-04-09 12:37:11.393		1175	1	\N	\N
354	Perfil-1/S2K-01125	40	168	69	52	2018-04-04 00:00:00	2019-04-03 00:00:00	no	no	inUse	2018-05-08 09:26:26.644		1176	1	\N	\N
44	CRA / 006PNP	16	28	21	75	2018-04-02 00:00:00	2019-09-09 00:00:00	no	no	inUse	2018-05-08 09:46:09.539		1177	1	\N	\N
1	CTL / 14.407	19	41	21	75	2018-04-02 00:00:00	2019-09-09 00:00:00	no	no	inUse	2018-05-08 10:18:09.987		1178	1	\N	\N
703	HPT-20 / 3202406	5	24	13	52	2018-05-04 00:00:00	2019-05-03 00:00:00	yes	no	inCalibration	2018-05-08 13:05:08.269		1179	1	\N	\N
588	HPT-09 / 3201093	5	24	13	52	2018-05-04 00:00:00	2019-05-03 00:00:00	no	no	inCalibration	2018-05-08 13:49:38.255		1180	1	\N	\N
278	TH-58 \\ 7E0800217	3	23	43	52	2018-04-27 00:00:00	2019-04-26 00:00:00	yes	no	collected	2018-04-04 13:16:06.098		1181	1	\N	\N
224	TH-41 / 7DBC00319	3	23	43	52	2018-04-27 00:00:00	2019-04-26 00:00:00	no	no	collected	2018-04-05 09:09:31.556	*UMIDADE	1182	1	\N	\N
293	CES-03/0031984	49	108	69	52	2018-04-16 00:00:00	2019-04-15 00:00:00	no	no	inUse	2018-04-19 15:49:45.723		1183	1	\N	\N
270	CAC-01/655473	32	63	23	104	2017-01-24 00:00:00	2019-01-22 00:00:00	no	no	inUse	2018-05-14 09:55:58.517		1184	1	\N	\N
1077	ESF-01	84	176	111	104	2018-05-11 00:00:00	2020-05-08 00:00:00	no	no	registered	2018-05-15 09:31:43.232		1207	0	\N	\N
1078	ESF-02	84	177	111	104	2017-05-08 00:00:00	2019-05-06 00:00:00	no	no	registered	2018-05-15 09:34:36.097		1210	0	\N	\N
1079	ESF-03	84	176	111	104	2017-05-08 00:00:00	2019-05-06 00:00:00	no	no	registered	2018-05-15 09:35:49.479		1211	0	\N	\N
1080	TEMP	85	178	111	104	2017-05-05 00:00:00	2019-05-03 00:00:00	no	no	registered	2018-05-15 09:40:55.275		1215	0	\N	\N
1081	TEMP-2	85	178	111	104	2017-05-07 00:00:00	2019-05-05 00:00:00	no	no	registered	2018-05-15 09:41:53.926		1216	0	\N	\N
1081	TEMP-2	85	178	111	104	2018-05-07 00:00:00	2020-05-04 00:00:00	no	no	registered	2018-05-15 09:41:53.926		1217	1	\N	\N
1080	TEMP-1	85	178	111	104	2017-05-05 00:00:00	2019-05-03 00:00:00	no	no	registered	2018-05-15 09:40:55.275		1218	1	\N	\N
136	PP-20 / K091005	1	21	6	52	2017-09-04 00:00:00	2018-09-03 00:00:00	no	no	collected	2018-05-15 15:22:54.316	THS	1219	1	\N	\N
26	PM-31 / D110123	1	3	60	52	2017-08-07 00:00:00	2018-08-06 00:00:00	no	no	collected	2018-05-15 15:37:09.19		1220	1	\N	\N
116	PM-45 / D101037	1	3	60	52	2018-04-14 00:00:00	2019-04-13 00:00:00	no	no	inUse	2018-05-15 15:37:24.54		1221	1	\N	\N
23	PP-08 / K101217	1	21	108	52	2017-12-27 00:00:00	2018-12-26 00:00:00	no	no	inUse	2018-01-09 14:03:55.226		1222	1	\N	\N
94	PP-17 / K110428	1	1	48	52	2018-04-20 00:00:00	2019-04-19 00:00:00	no	no	inUse	2018-05-16 08:49:27.269		1223	1	\N	\N
91	PP-14 / K110502	1	1	45	52	2017-12-19 00:00:00	2018-12-18 00:00:00	no	no	inUse	2018-01-09 14:08:59.864		1224	1	\N	\N
231	PP-31 / K110208	1	2	51	52	2017-12-11 00:00:00	2018-12-10 00:00:00	no	no	inUse	2017-12-11 00:00:00		1225	1	\N	\N
140	PP-23 / K100618	1	1	46	52	2017-10-23 00:00:00	2018-10-22 00:00:00	no	no	inUse	2018-05-16 08:56:24.191		1226	1	\N	\N
29	PM-34 / D110124	1	3	62	52	2017-12-20 00:00:00	2018-12-19 00:00:00	no	no	inUse	2018-04-04 08:35:14.449	Sala instrumentação	1227	1	\N	\N
62	PM-36 / D101047	1	3	58	52	2017-08-04 00:00:00	2018-08-03 00:00:00	no	no	inUse	2018-05-16 09:03:27.046	DANIFICADO	1228	1	\N	\N
137	PP-21 / K100310	1	1	6	52	2018-02-19 00:00:00	2019-02-18 00:00:00	no	no	inUse	2018-05-16 09:08:34.133		1229	1	\N	\N
425	HPT-02 / 3201194	5	24	71	52	2017-03-14 00:00:00	2018-03-13 00:00:00	no	no	inUse	2018-05-16 14:07:36.017		1230	1	\N	\N
23	PP-08 / K101217	1	21	108	52	2017-12-27 00:00:00	2018-12-26 00:00:00	no	no	collected	2018-05-23 09:13:05.095		1231	1	\N	\N
92	PP-15 / K110401	1	1	54	52	2018-01-18 00:00:00	2019-01-17 00:00:00	no	no	collected	2018-05-23 09:18:12.418		1232	1	\N	\N
231	PP-31 / K110208	1	2	51	52	2017-12-11 00:00:00	2018-12-10 00:00:00	no	no	collected	2018-05-23 09:18:57.182		1233	1	\N	\N
63	PM-37 / D101046	1	3	57	52	2017-11-05 00:00:00	2018-11-04 00:00:00	no	no	collected	2018-05-23 09:27:14.388		1234	1	\N	\N
62	PM-36 / D101047	1	3	58	52	2017-08-04 00:00:00	2018-08-03 00:00:00	no	no	collected	2018-05-23 09:27:42.123	DANIFICADO	1235	1	\N	\N
1082	A	10	105	111	10	2018-06-11 00:00:00	2018-08-20 00:00:00	no	no	registered	2018-06-11 10:52:23.693	útima Tentiva	1279	0	2	\N
126	PM-48 / D101033	1	3	59	52	2017-11-16 00:00:00	2018-11-15 00:00:00	no	yes	inUse	2017-11-16 00:00:00		1280	1	2	\N
2	BD-01	43	86	27	70	2019-09-18 00:00:00	2021-01-20 00:00:00	no	no	inUse	2017-09-22 00:00:00		1281	1	2	\N
1083	ABCDSDAFSDFASDFFF	6	10	31	123	2022-06-30 00:00:00	2024-11-07 00:00:00	yes	yes	registered	2018-06-11 11:08:01.363		1282	0	1	\N
1084	zTest z	87	179	112	200	2027-06-01 00:00:00	2031-04-01 00:00:00	no	no	registered	2018-06-11 12:13:03.892	zTest zzTest zzTest z	1290	0	3	\N
1084	zTest z	87	179	112	200	2027-06-01 00:00:00	2031-04-01 00:00:00	no	no	collected	2018-06-11 12:13:52.458	zTest zzTest zzTest z	1291	1	3	\N
1082	Ab	10	131	110	10	2018-06-11 00:00:00	2018-08-20 00:00:00	no	no	registered	2018-06-11 10:52:23.693	útima Tentiva	1292	1	2	\N
1082	Ab	10	131	110	10	2018-06-11 00:00:00	2018-08-20 00:00:00	no	no	registered	2018-06-11 10:52:23.693	útima Tentiva 2	1293	1	2	\N
1084	zTest z	87	179	112	200	2027-06-01 00:00:00	2031-04-01 00:00:00	no	no	inRepair	2018-06-11 12:17:26.908	zTest zzTest zzTest z	1294	1	3	\N
1084	zTest z	87	179	112	200	2027-06-01 00:00:00	2031-04-01 00:00:00	no	no	registered	2018-06-11 12:18:55.634	zTest zzTest zzTest z	1295	1	3	\N
1084	zTest z	87	179	112	200	2027-06-01 00:00:00	2031-04-01 00:00:00	no	no	inUse	2018-06-11 12:19:39.873	zTest zzTest zzTest z	1296	1	3	\N
4	BD-03	43	88	29	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	inUse	2017-09-22 00:00:00		1297	1	3	\N
1085	TESTE	87	165	48	999	2028-01-01 00:00:00	2047-02-23 00:00:00	no	no	registered	2018-06-11 12:36:36.569		1298	0	3	\N
1085	TESTE	87	165	48	999	2028-01-01 00:00:00	2047-02-23 00:00:00	no	yes	registered	2018-06-11 12:36:36.569	fffffffffffffffffffffffffffffffffffffffffffff	1299	1	3	\N
355	Testador1	37	79	76	52	2018-03-06 00:00:00	2019-03-05 00:00:00	no	no	inUse	2018-03-13 12:19:14.446		1300	1	3	\N
287	MP-01	77	144	112	52	2016-10-27 00:00:00	2017-10-26 00:00:00	yes	yes	dropOff	2018-06-11 12:50:03.967		1301	1	2	\N
1084	zTest z	87	179	112	200	2027-06-01 00:00:00	2031-04-01 00:00:00	no	no	dropOff	2018-06-11 12:53:17.858	zTest zzTest zzTest z	1302	1	3	\N
1085	TESTE	87	165	48	999	2028-01-01 00:00:00	2047-02-23 00:00:00	no	yes	dropOff	2018-06-11 12:55:40.961	fffffffffffffffffffffffffffffffffffffffffffff	1303	1	3	\N
4	BD-03	43	88	29	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	dropOff	2018-06-11 12:55:51.846		1304	1	3	\N
94	PP-17 / K110428	1	1	48	52	2018-04-20 00:00:00	2019-04-19 00:00:00	no	no	inUse	2018-05-16 08:49:27.269		1305	1	3	\N
1068	VOLT-26	7	68	19	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inUse	2018-04-30 11:35:40.434		1306	1	2	\N
1066	VOLT-24	7	97	14	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inUse	2018-03-13 15:29:33.592		1307	1	2	\N
1067	VOLT-25 	7	106	18	52	2018-02-27 00:00:00	2019-02-26 00:00:00	no	no	inUse	2018-03-13 09:11:49.716		1308	1	1	\N
1086	1 Test 1	88	180	113	101	2018-06-01 00:00:00	2020-05-08 00:00:00	no	no	registered	2018-06-11 13:20:46.272	1 Test 1	1315	0	4	\N
1086	1 Test 1	10	180	31	101	2018-06-01 00:00:00	2020-05-08 00:00:00	no	no	registered	2018-06-11 13:20:46.272	1 Test 1	1316	1	2	\N
1086	1 Test 1	10	105	31	101	2018-06-01 00:00:00	2020-05-08 00:00:00	no	no	registered	2018-06-11 13:20:46.272	1 Test 1	1317	1	2	\N
139	PP-22 / K100614	1	1	47	52	2018-01-17 00:00:00	2019-01-16 00:00:00	no	no	inUse	2018-02-01 12:35:26.209		1339	1	3	\N
1087	126	44	86	111	999	2018-06-14 00:00:00	2037-08-06 00:00:00	no	no	registered	2018-06-14 15:06:16.091		1344	0	1	1
1088	ddddddddddddddddddd	44	86	111	108	2018-06-20 00:00:00	2020-07-15 00:00:00	yes	yes	registered	2018-06-14 15:19:46.591	sadf	1355	0	1	2
1089	123456789	45	10	93	99	2019-06-21 00:00:00	2021-05-14 00:00:00	no	no	registered	2018-06-14 15:21:37.975		1356	0	2	7
1090	123456	19	119	42	70	2018-06-30 00:00:00	2019-11-02 00:00:00	no	yes	registered	2018-06-14 15:30:44.975		1358	0	2	1
228	PP-29 / K090801	1	1	44	52	2017-12-21 00:00:00	2018-12-20 00:00:00	no	no	inUse	2016-12-28 00:00:00		1369	1	2	4
683	MR-01	45	100	12	2	2017-08-24 00:00:00	2017-09-07 00:00:00	no	no	inUse	2017-08-24 00:00:00		1370	1	1	7
114	PM-43 / D101034	1	3	61	52	2017-10-18 00:00:00	2018-10-17 00:00:00	no	no	inUse	2017-10-18 00:00:00		1371	1	2	5
92	PP-15 / K110401	1	1	54	52	2018-01-18 00:00:00	2019-01-17 00:00:00	no	no	collected	2018-05-23 09:18:12.418		1372	1	2	7
115	PM-44 / D100602	1	3	64	52	2017-10-19 00:00:00	2018-10-18 00:00:00	no	no	inUse	2017-10-19 00:00:00		1374	1	\N	1
5	BD-04	43	89	29	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	inUse	2017-09-22 00:00:00		1375	1	2	\N
11	HPT - 21 / 3202231	5	24	36	52	2017-06-23 00:00:00	2018-06-22 00:00:00	no	no	inUse	2017-06-23 00:00:00		1377	1	\N	2
63	PM-37 / D101046	10	3	57	52	2017-11-05 00:00:00	2018-11-04 00:00:00	no	no	collected	2018-05-23 09:27:14.388		1378	1	2	2
1091	11111111111111	76	26	6	111	2018-06-21 00:00:00	2020-08-06 00:00:00	no	no	registered	2018-06-14 17:09:38.979	111111111111	1379	0	1	2
193	PP-26 / K130202	1	26	55	52	2017-11-23 00:00:00	2018-11-22 00:00:00	no	no	inUse	2018-02-02 21:17:57.276		1383	1	\N	5
1092	5	10	12	31	5	2018-06-28 00:00:00	2018-08-02 00:00:00	no	no	registered	2018-06-14 17:37:23.89		1384	0	2	\N
1093	6	87	86	111	666	2018-06-13 00:00:00	2031-03-19 00:00:00	no	no	registered	2018-06-14 17:39:16.459		1386	0	1	6
121	HTP-13 / 109608214G	5	105	97	52	2018-01-24 00:00:00	2019-01-23 00:00:00	no	no	collected	2018-02-22 13:34:59.968		1387	1	\N	6
22	PP-07 / K101216	1	21	55	52	2017-09-06 00:00:00	2018-09-05 00:00:00	no	no	inUse	2017-09-06 00:00:00		1388	1	\N	6
3	BD-02	43	87	28	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	inUse	2017-09-22 00:00:00		1389	1	\N	1
93	PP-16 / K110505	1	1	48	52	2017-08-29 00:00:00	2018-08-28 00:00:00	no	no	inUse	2017-08-29 00:00:00		1390	1	\N	1
1094	126	56	107	3	111	2018-06-06 00:00:00	2020-07-22 00:00:00	no	no	registered	2018-06-14 18:44:49.247		1392	0	2	7
53	PM-39 / D101046	1	3	8	52	2017-12-12 00:00:00	2018-12-11 00:00:00	no	no	inUse	2017-12-12 00:00:00		1393	1	\N	7
8	BD-07	43	92	30	52	2017-09-22 00:00:00	2018-09-21 00:00:00	no	no	inUse	2017-09-22 00:00:00		1394	1	\N	1
\.


--
-- TOC entry 2319 (class 0 OID 57352)
-- Dependencies: 206
-- Data for Name: tb_measuring_instrument_department; Type: TABLE DATA; Schema: mecsw; Owner: postgres
--

COPY tb_measuring_instrument_department (code, name, description, version, user_code) FROM stdin;
2	Laboratório	Laboratório	0	\N
3	zTest z	zTest z	0	\N
1	Metrologia	Metrologia	1	6
\.


--
-- TOC entry 2320 (class 0 OID 57362)
-- Dependencies: 207
-- Data for Name: tb_measuring_instrument_department_aud; Type: TABLE DATA; Schema: mecsw; Owner: postgres
--

COPY tb_measuring_instrument_department_aud (code, name, description, code_review, type_review, user_code) FROM stdin;
1	Metrologia	Metrologia	1236	0	\N
2	Laboratório	Laboratório	1237	0	\N
3	123AABBCC123AABBCC123AABBCC123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	1260	0	\N
3	123AABBCC123AABBCC123AABBCC123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	1264	2	\N
3	zTest z	zTest z	1287	0	\N
4	1 Test 1		1313	0	\N
4	1 Test 1		1321	2	\N
1	Metrologia	Metrologia	1328	1	6
\.


--
-- TOC entry 2305 (class 0 OID 49400)
-- Dependencies: 192
-- Data for Name: tb_measuring_instrument_manufacturer; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_measuring_instrument_manufacturer (code, name, description, version) FROM stdin;
53	AINUO		1
45	ALFA		1
67	Associated Research		1
44	Bosch		1
38	BS260-III		1
6	C-Lin		1
22	Ceime		1
13	CHROMA		1
24	Digimess		1
30	EXTECH		1
11	Fluke		1
48	Fluke Corporation		1
2	Fluke mini 62		1
34	Fuji Electric		1
27	GEDORE		1
36	HAITIAN		1
68	Hameg		1
3	Hangzhou		1
50	Hesan Static		1
1	HEXING		1
69	ZAAS Precision		0
5	Toone		1
25	Torqueleader		1
80	G-TECH		0
29	Willdone		1
16	Zera		1
17	Zjuee		1
19	Neomatic		1
40	NEW TAT		1
64	Novus		1
47	OHAUS		1
14	Philips		1
49	Power		1
60	Radian Research		1
70	RMT		1
58	Salcas		1
57	SALVTERM		1
62	Schneider Eletric		1
54	SCI		1
35	Sinitron		1
18	Slaugther		1
55	SLD		1
63	SMS		1
61	Steck		1
51	Tensochck 100		1
65	Therma		1
28	TOHNICHI		1
46	TOLEDO		1
4	Vaisala		2
71	HPU		0
72	HT-208		0
73	ICEL		0
12	Minipa		1
15	Mitutoyo		1
10	NÃO INFORMADO		1
42	HIKARI		1
66	Honeywell		1
39	HOTFOW		1
41	HTC		1
56	INCOTERM		1
23	Insiize		1
21	Insize		1
32	INSTRUTEMP		1
31	INSTRUTHERM		1
37	KEBA		1
33	KEITHLEY		1
26	KING TONY		1
20	Kingtools		1
59	Landis Gyr		1
52	Lutron		1
7	Marconi		1
43	KIC START		2
75	HOTFLOW		0
76	HOTRUNNER		1
77	CONTEMP		0
78	OMROM		0
79	PREMIUM		0
74	test1		2
81	zTest z	zTest z	0
\.


--
-- TOC entry 2306 (class 0 OID 49406)
-- Dependencies: 193
-- Data for Name: tb_measuring_instrument_manufacturer_aud; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_measuring_instrument_manufacturer_aud (code, name, description, code_review, type_review) FROM stdin;
1	fabricante1	asdasdasd	15	0
2	fabricante2	asdasdasd	16	0
3	fabricante3	fabricante3	18	0
53	AINUO		45	1
45	ALFA		46	1
67	Associated Research		47	1
44	Bosch		48	1
38	BS260-III		49	1
6	C-Lin		50	1
22	Ceime		51	1
13	CHROMA		52	1
24	Digimess		53	1
30	EXTECH		54	1
11	Fluke		55	1
48	Fluke Corporation		56	1
2	Fluke mini 62		57	1
34	Fuji Electric		58	1
27	GEDORE		59	1
36	HAITIAN		60	1
68	Hameg		61	1
3	Hangzhou		62	1
50	Hesan Static		63	1
1	HEXING		64	1
69	ZAAS - Precision		100	0
69	ZAAS - Precision		102	2
69	ZAAS Precision		103	0
5	Toone		151	1
25	Torqueleader		152	1
4	vaisala		153	1
29	Willdone		154	1
16	Zera		155	1
17	Zjuee		156	1
69	ZAAS Precision		157	1
70	RMT	Real Meter Test	182	0
70	RMT	Real Meter Test	183	1
19	Neomatic		236	1
40	NEW TAT		237	1
64	Novus		238	1
47	OHAUS		239	1
14	Philips		240	1
49	Power		241	1
60	Radian Research		242	1
70	RMT		243	1
58	Salcas		244	1
57	SALVTERM		245	1
62	Schneider Eletric		246	1
54	SCI		247	1
35	Sinitron		248	1
18	Slaugther		249	1
55	SLD		250	1
63	SMS		251	1
61	Steck		252	1
51	Tensochck 100		253	1
65	Therma		254	1
28	TOHNICHI		255	1
46	TOLEDO		273	1
4	Vaisala		274	1
39	HOTFOW	\N	330	1
71	HPU		423	0
71	HPU		424	1
72	HT-208		499	0
73	ICEL		500	0
73	ICEL		504	1
69	ZAAS Precision		619	1
74	SHANGHAI		643	0
12	Minipa		644	1
15	Mitutoyo		645	1
10	NÃO INFORMADO		646	1
74	Shanghai Santa Fé		647	1
74	Shanghai Santa Fé		648	1
75	schneider		662	0
62	Schneider Eletric		663	1
75	schneider		664	2
42	HIKARI		671	1
66	Honeywell		672	1
39	HOTFOW		673	1
41	HTC		674	1
56	INCOTERM		675	1
23	Insiize		676	1
21	Insize		677	1
32	INSTRUTEMP		678	1
31	INSTRUTHERM		679	1
37	KEBA		680	1
33	KEITHLEY		681	1
43	KIK START		682	1
26	KING TONY		683	1
20	Kingtools		684	1
59	Landis Gyr		685	1
52	Lutron		686	1
7	Marconi		687	1
5	Toone		754	1
18	Slaugther		758	1
21	Insize		762	1
43	KIC START		768	1
43	KIC START		769	1
1	HEXING		773	1
24	Digimess		835	1
43	KIC START		879	1
75	HOTFLOW		908	0
75	HOTFLOW		909	1
76	HotRunner		917	0
76	HOTRUNNER		918	1
76	HOTRUNNER		924	1
37	KEBA		960	1
38	BS260-III		960	1
77	CONTEMP		976	0
77	CONTEMP		978	1
11	Fluke		1044	1
11	Fluke		1053	1
11	Fluke		1144	1
12	Minipa		1158	1
78	OMROM		1186	0
78	OMROM		1187	1
69	ZAAS Precision		1188	1
24	Digimess		1188	1
57	SALVTERM		1189	1
11	Fluke		1189	1
55	SLD		1190	1
11	Fluke		1190	1
15	Mitutoyo		1200	1
24	Digimess		1200	1
79	PREMIUM		1209	1
80	G-TECH		1214	1
34	Fuji Electric		1191	1
15	Mitutoyo		1191	1
24	Digimess		1192	1
69	ZAAS Precision		1192	1
24	Digimess		1194	1
15	Mitutoyo		1194	1
19	Neomatic		1196	1
55	SLD		1196	1
15	Mitutoyo		1197	1
56	INCOTERM		1197	1
12	Minipa		1201	1
11	Fluke		1201	1
11	Fluke		1202	1
34	Fuji Electric		1202	1
1	HEXING		1205	1
15	Mitutoyo		1205	1
79	PREMIUM		1208	0
80	G-TECH		1213	0
24	Digimess		1193	1
1	HEXING		1193	1
24	Digimess		1195	1
15	Mitutoyo		1195	1
15	Mitutoyo		1198	1
24	Digimess		1198	1
15	Mitutoyo		1199	1
24	Digimess		1199	1
11	Fluke		1203	1
57	SALVTERM		1203	1
11	Fluke		1204	1
12	Minipa		1204	1
16	Zera		1241	1
81	123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	1257	0
81	123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	1258	1
81	123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	1266	2
74	test1		1269	1
81	zTest z	zTest z	1284	0
81	zTest z	zTest z	1285	1
82	1 Test 1		1310	0
82	1 Test 1		1311	1
82	1 Test 1		1319	2
\.


--
-- TOC entry 2307 (class 0 OID 49412)
-- Dependencies: 194
-- Data for Name: tb_measuring_instrument_measuring_instrument_standard; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_measuring_instrument_measuring_instrument_standard (measuring_instrument_code, measuring_instrument_standard_code) FROM stdin;
116	2
14	1
178	1
42	1
389	1
390	1
391	1
394	1
15	1
231	2
170	1
139	2
392	1
393	1
395	1
396	1
397	1
398	1
156	1
123	2
149	1
250	1
712	2
303	1
426	2
358	1
683	2
423	2
143	1
255	2
226	2
246	2
300	1
88	2
232	2
230	2
331	1
332	1
333	1
334	1
335	1
336	1
337	1
338	1
339	1
340	1
341	1
342	1
559	2
301	1
25	2
100	1
243	2
701	2
360	1
244	2
275	1
92	2
189	2
266	1
7	1
504	1
505	1
61	2
30	2
248	1
16	2
46	2
45	2
257	2
252	2
256	2
87	2
191	1
352	1
1030	1
330	1
107	1
344	1
346	1
28	2
281	1
134	2
354	1
44	1
1	1
293	2
26	2
13	1
115	2
251	2
141	1
708	2
125	2
24	2
155	2
152	2
267	1
49	2
6	1
101	2
27	2
111	1
110	1
705	2
696	2
704	2
63	2
91	2
112	1
140	2
145	1
425	2
23	2
62	2
124	2
161	1
357	1
353	1
359	1
262	1
102	2
1050	2
258	2
95	1
43	2
239	2
269	1
268	1
238	2
120	2
263	1
274	1
709	1
162	1
144	1
12	1
264	1
259	2
449	2
298	2
271	1
272	1
343	1
279	2
227	2
305	1
54	2
50	2
280	2
1001	1
345	1
349	1
347	1
348	1
90	1
158	2
684	2
245	2
550	1
224	2
270	1
193	2
121	2
22	2
93	2
1090	4
1090	1
1090	3
1090	2
1090	6
114	2
157	2
5	1
11	2
1091	4
1091	1
1091	3
1091	2
1091	6
3	1
8	1
299	2
126	2
2	1
1082	3
355	1
1084	4
1084	1
1084	3
1084	2
1084	6
1085	4
1085	1
1085	3
1085	2
1085	6
4	1
4	3
4	6
94	4
94	1
94	2
94	6
1086	4
\.


--
-- TOC entry 2308 (class 0 OID 49415)
-- Dependencies: 195
-- Data for Name: tb_measuring_instrument_model; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_measuring_instrument_model (code, name, description, measuring_instrument_manufacturer_code, version) FROM stdin;
115	Bancada Polifásica	\N	16	0
118	ED 8273	\N	16	0
132	MT - 241	\N	12	0
136	TPZ308-6-2	\N	16	0
139	ET - 2907	\N	12	0
142	7CH03084	\N	25	0
143	M6 x 1 -  6H	\N	19	0
146	R1/2"x14	\N	19	0
147	G1"	\N	19	0
148	M4,0 X 0,7 – 6g	\N	19	0
149	ET-1100	\N	12	0
152	M6 x 1,00 - 6H	\N	19	0
5	mini 62	\N	2	0
6	ZDR-20	\N	3	0
8	CC2672C	\N	1	0
9	XYX-1	\N	5	0
10	SE-96-3A	\N	6	0
11	ZYX1-I	\N	5	0
12	ZYX1	\N	5	0
14	ZDR-20	\N	7	0
15	NÃO INFORMADO	\N	10	0
16	345	\N	11	0
18	61702	\N	13	0
19	PM-2525	\N	14	0
22	EPZ303-5-2	\N	16	0
23	ZDR-20	\N	17	0
24	2510	\N	18	0
26	SE963V	\N	6	0
28	M4,0 x 0,7 - 6g	\N	19	0
29	M6,0 x 1,0 - 6g	\N	19	0
31	M6,0 x 1,0 - 6g	\N	21	0
35	M6 x 1,00 - 6G	\N	19	0
36	M5 x 0.8 -  6H	\N	22	0
37	M6 x 1 -  6H	\N	23	0
46	até 5mm	\N	24	0
51	QSN600 FH	\N	25	0
52	34111-2DG	\N	26	0
56	DB12N4-S	\N	28	0
57	3N.m até 40N.m	\N	27	0
59	SL355/ 140201184	\N	30	0
60	SL355/ 140609432	\N	30	0
61	TBD400140510498	\N	31	0
64	2700	\N	33	0
66	BS-800	\N	35	0
67	MA-600	\N	36	0
68	BS-1200	\N	35	0
70	BS-2000	\N	35	0
71	BS-2600	\N	35	0
72	MA-2500	\N	36	0
75	BS- 1500	\N	35	0
85	QM-MEASURE 353	\N	15	0
93	ET - 4050	\N	12	0
95	289	\N	11	0
96	ET-2940	\N	12	0
48	0,001 - 25 mm		15	1
50	0,01 - 0,25mm		15	2
65	0,1s a 99h		11	2
47	0,01 - 10 mm		24	2
109	191A		19	2
20	0,01 - 150 mm		15	1
45	0,01 - 0,8 mm		24	2
82	106		57	2
112	1200K		11	2
42	0,01 - 200 mm		34	2
17	179		11	2
44	0,01 - 300 mm		1	2
49	0,001mm - 0-25mm		15	2
55	2-25 N.m		27	1
41	2,20 + 0,10/-0,10		19	1
92	2003/29-2180		46	1
144	HM-8115-2		68	1
138	Ultra III		67	1
134	DC1040		66	1
133	TH 92 M		65	1
128	DZ601-48		63	1
129	DZ603-24		63	1
140	MDP1000A		63	1
141	MDP2000A		63	1
135	MT3000D		63	1
137	SH15		63	1
127	PM 810		62	1
126	20º / 100ºC		61	1
145	RM -10		60	1
125	RM -10 -01		60	1
124	RM -10 -08		60	1
119	20º / 100ºC		58	1
58	HP-100		29	1
110	HT-425		42	1
80	HTC-1		41	1
153	HX-P301-C		1	1
3	HY5101C-23D		1	1
2	HY5303C-22		1	1
21	HY5303C-22D		1	1
1	HY5303C-23D		1	1
63	ITCAD 5000		32	1
62	ITLD270/ 29354		32	1
32	M 10 x 1,5 6 g		20	1
30	M 5 x 0.8 - 6H		20	1
38	M10 x 1.5 6 H		20	1
40	M3 x 0.5 6 H		20	1
34	M4 x 0,7 -   6g		19	1
33	M4 x 0,70 - 6g		19	1
13	SE-96		1	1
91	ARA 520		47	1
87	2098/59		46	1
89	3400/1		46	1
90	3400/3		46	1
25	SX48		1	1
54	Tongli-0 a 6N.m		1	1
150	XINLING SE-96		1	1
27	YKE		1	1
84	GLM 40 3601K72900		44	1
81	HK-T220		42	1
108	HK-191A		42	1
130	HB404		10	1
7	HM70		4	1
122	HL-S3200		1	1
114	HL-S3000A-24		1	1
113	HL-S3000		1	1
123	HL-S1200		1	1
151	HL-S1000A-49		1	1
116	HL-S1000A-48		1	1
117	HL-S1000A-24		1	1
4	HDS322-02		1	1
88	3400/6		46	1
86	3101		45	1
83	S2K-01125		43	1
79	NHE200/A		40	1
78	MD-68		39	1
69	BS-2000		37	1
76	MA- 1600		36	1
74	MA-1600		36	1
73	MA-2000		36	1
43	0,01 - 300 mm		69	2
104	MTH-1380	\N	12	0
106	2511	\N	18	0
131	Pinguim RTH		64	1
120	SAGA2000		59	1
155	HDS322 - 02		70	5
156	MD - 68		39	0
157	HC 3100H		71	0
158	HT-208	Temperatura de operação: De 0ºC a 50ºC	73	1
103	HT 3005-HA		52	1
101	HVP-40		12	1
39	M2 x 0.4 6 H		20	1
159	Até 600mm		69	0
161	ION 8650 		62	0
107	290		54	1
105	AN9602X		53	1
102	TC-01		51	1
100	surface		50	1
99	386		49	1
98	43B		48	1
162	ZYX1-U		5	0
163	295		18	0
164	M6 x 1 - 6H		21	0
165	Omega CL23A		43	0
166	HX-P3001-C		1	0
168	Kic Thermal Profile		43	0
169	Aquecedor de Molde		75	0
121	HL-SN1T24A		1	1
170	Não Informado		76	1
77	BS260-III		37	1
171	cpm 45		77	0
172	3000 FC		11	0
173	43 B		11	0
174	Mini 62		11	0
175	MT-240		12	0
176	HEM-6111		78	0
97	189		55	2
154	0-200mm		24	1
53	0 a 6N.m		24	2
111	-50+300ºC		15	3
167	0,01- 200 mm		15	2
94	179		12	2
177	0 - 300 mmhg		79	0
178	TH150		80	0
160	test2		74	1
179	zTest z	zTest z	81	0
\.


--
-- TOC entry 2309 (class 0 OID 49421)
-- Dependencies: 196
-- Data for Name: tb_measuring_instrument_model_aud; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_measuring_instrument_model_aud (code, name, description, measuring_instrument_manufacturer_code, code_review, type_review) FROM stdin;
154	0-200mm		69	157	0
53	0 a 6N.m		1	158	1
48	0,001 - 25 mm		15	159	1
49	0,001mm - 0-25mm		24	160	1
50	0,01 - 0,25mm		24	161	1
45	0,01 - 0,8 mm		15	162	1
47	0,01 - 10 mm		15	163	1
20	0,01 - 150 mm		15	164	1
42	0,01 - 200 mm		15	165	1
43	0,01 - 300 mm		24	166	1
44	0,01 - 300 mm		15	167	1
65	0,1s a 99h		34	168	1
82	106		11	169	1
112	1200K		57	170	1
94	179		11	171	1
17	179		12	172	1
97	189		11	173	1
109	191A		55	174	1
55	2-25 N.m		27	175	1
41	2,20 + 0,10/-0,10		19	176	1
92	2003/29-2180		46	177	1
155	H		70	183	0
155	HDS		70	205	1
144	HM-8115-2		68	206	1
138	Ultra III		67	207	1
134	DC1040		66	208	1
133	TH 92 M		65	209	1
131	Pinguim RTH		64	210	1
128	DZ601-48		63	211	1
129	DZ603-24		63	212	1
140	MDP1000A		63	213	1
141	MDP2000A		63	214	1
135	MT3000D		63	215	1
137	SH15		63	216	1
127	PM 810		62	217	1
126	20º / 100ºC		61	218	1
145	RM -10		60	219	1
125	RM -10 -01		60	220	1
124	RM -10 -08		60	221	1
120	SAGA2000		59	222	1
119	20º / 100ºC		58	223	1
155	HDS322		70	224	1
155	HDS32202		70	225	1
155	HDS322021		70	226	1
155	HDS322 - 02		70	227	1
156	MD - 68		39	330	0
157	HC 3100H		71	424	0
158	HT-208		73	504	0
58	HP-100		29	513	1
158	HT-208	Temperatura de operação: De 0ºC a 50ºC	73	514	1
110	HT-425		42	515	1
103	HT 3005-HA		52	516	1
80	HTC-1		41	517	1
101	HVP-40		12	518	1
153	HX-P301-C		1	519	1
3	HY5101C-23D		1	520	1
2	HY5303C-22		1	521	1
21	HY5303C-22D		1	522	1
1	HY5303C-23D		1	523	1
63	ITCAD 5000		32	524	1
62	ITLD270/ 29354		32	525	1
32	M 10 x 1,5 6 g		20	526	1
30	M 5 x 0.8 - 6H		20	527	1
38	M10 x 1.5 6 H		20	528	1
39	M2 x 0.4 6 H		20	529	1
40	M3 x 0.5 6 H		20	530	1
34	M4 x 0,7 -   6g		19	531	1
33	M4 x 0,70 - 6g		19	532	1
159	Até 600mm		69	619	0
160	AT194U-9X4		74	648	0
161	ION 8650 		62	663	0
111	-50*300ºC		56	688	1
107	290		54	689	1
105	AN9602X		53	690	1
102	TC-01		51	691	1
100	surface		50	692	1
99	386		49	693	1
98	43B		48	694	1
91	ARA 520		47	695	1
87	2098/59		46	696	1
89	3400/1		46	697	1
90	3400/3		46	698	1
111	-50+300ºC		56	702	1
162	ZYX1-U		5	754	0
163	295		18	758	0
164	M6 x 1 - 6H		21	762	0
165	Omega CL23A		43	769	0
166	HX-P3001-C		1	773	0
167	0,01 - 300 mm		24	835	0
167	0,01- 200 mm		24	836	1
168	Kic Thermal Profile		43	879	0
169	Aquecedor de Molde		75	909	0
13	SE-96		1	919	1
25	SX48		1	920	1
54	Tongli-0 a 6N.m		1	921	1
150	XINLING SE-96		1	922	1
27	YKE		1	923	1
170	a		76	924	0
84	GLM 40 3601K72900		44	925	1
81	HK-T220		42	926	1
108	HK-191A		42	927	1
130	HB404		10	928	1
7	HM70		4	929	1
121	HL-SN1T24A		1	930	1
122	HL-S3200		1	931	1
114	HL-S3000A-24		1	932	1
113	HL-S3000		1	933	1
123	HL-S1200		1	934	1
151	HL-S1000A-49		1	935	1
116	HL-S1000A-48		1	936	1
117	HL-S1000A-24		1	937	1
4	HDS322-02		1	938	1
170	Não Informado		76	939	1
88	3400/6		46	955	1
86	3101		45	956	1
83	S2K-01125		43	957	1
79	NHE200/A		40	958	1
78	MD-68		39	959	1
77	BS260-III		37	960	1
69	BS-2000		37	961	1
76	MA- 1600		36	962	1
74	MA-1600		36	963	1
73	MA-2000		36	964	1
171	cpm 45		77	978	0
172	3000 FC		11	1044	0
173	43 B		11	1053	0
174	Mini 62		11	1144	0
175	MT-240		12	1158	0
176	HEM-6111		78	1187	0
43	0,01 - 300 mm		69	1188	1
82	106		57	1189	1
97	189		55	1190	1
42	0,01 - 200 mm		34	1191	1
154	0-200mm		24	1192	1
53	0 a 6N.m		24	1193	1
45	0,01 - 0,8 mm		24	1194	1
47	0,01 - 10 mm		24	1195	1
109	191A		19	1196	1
111	-50+300ºC		15	1197	1
50	0,01 - 0,25mm		15	1200	1
49	0,001mm - 0-25mm		15	1198	1
167	0,01- 200 mm		15	1199	1
94	179		12	1201	1
65	0,1s a 99h		11	1202	1
44	0,01 - 300 mm		1	1205	1
112	1200K		11	1203	1
17	179		11	1204	1
177	0 - 300 mmhg		79	1209	0
178	TH150		80	1214	0
179	445556666	ghjfggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg	16	1241	0
179	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	81	1258	0
160	test2		74	1270	1
179	zTest z	zTest z	81	1285	0
180	1 Test 1	1 Test 1	82	1311	0
\.


--
-- TOC entry 2310 (class 0 OID 49427)
-- Dependencies: 197
-- Data for Name: tb_measuring_instrument_place_use; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_measuring_instrument_place_use (code, name, description, version) FROM stdin;
72	Verificação	\N	0
75	Weco 1	\N	0
95	PCM	\N	0
104	WDA	\N	0
31	ALMOX.SMT/PTH		1
42	Banc Interditada		1
93	Bancada de reparo		1
78	Bancada Zera		1
6	BI-01		1
7	BI-02		1
10	BI-03		1
11	BI-04		1
8	BI-05		1
96	BI 01		1
65	BM-01		1
64	BM-02		1
59	BM-04		1
67	BM-05		1
58	BMC-01		1
61	BMC-02		1
57	BMC-03		1
60	BMC-04		1
62	BMC-05		1
2	UNIFOR	\N	0
32	Plastico		1
9	ZERA	\N	1
25	Plásticos		1
69	Processo		1
73	Produção Eletrônico		1
33	PTH		1
68	QUALIDADE PLASTICO		1
90	R.C.T-01		1
91	R.C.T-02		1
89	R.C.T-03		1
21	Recebimento		1
100	Recebimento/ Cecilia		1
94	Reparo		1
35	RMA		1
43	Sala de Instrumentação		1
86	Sala de instrumentação - HD		1
13	Sala instrumentação		1
23	SESMT		1
26	SMT		1
105	travado		1
106	BI-06		0
63	BMC-06		1
44	BP-01		1
52	BP-02		1
41	BP-04		1
45	BP-05		1
51	BP-06		1
54	BP-07		1
49	BPC-04		1
53	BPC-05		1
48	BPC-06		1
50	BPC-07		1
56	BPC-08		1
47	BPC-09		1
46	BPC-10		1
55	BPVA-02		1
92	BTA-01		1
98	BTA-02		1
97	BTA-04		1
101	DANIFICADO		1
77	BM-06		2
34	Calibração		2
103	DANIFICADA		1
79	Desativada		1
87	Em Calibração		1
1	ENGENHARIA		1
20	ESTUFA		1
102	Estufa - ETF 07		1
88	ETF - 07		1
82	F.T.C-01		1
83	F.T.C-02		1
85	F.T.C-03		1
84	F.T.C-05		1
81	F.T.C-06		1
24	FCT		1
16	FM-01		1
17	FM-03		1
5	FME/Engenharia		1
19	FP-01		1
18	FP-02		1
15	FP-03		1
29	Galpão 2 / THC		1
39	Galpão 3		1
30	Galpão 3 / Almoxarifado		1
40	L3		1
37	L4		1
36	L5		1
38	L6		1
80	Lab. Eletrônico		1
99	Laboratório 01		1
71	Laboratório de Ensaio		1
66	Manutenção		1
12	Metrologia		1
28	Moinho		1
3	NÃO INFORMADO		1
4	P&D		1
22	Para Calibração		1
70	PCI		1
27	Plasticos		2
74	LI-03		2
76	Produção		2
107	BPC-02		0
108	BPC-01		0
109	BME - 01		0
110	L2		0
111	Ambulatório		0
14	test3		2
112	zTest z	zTest z	0
\.


--
-- TOC entry 2311 (class 0 OID 49433)
-- Dependencies: 198
-- Data for Name: tb_measuring_instrument_place_use_aud; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_measuring_instrument_place_use_aud (code, name, description, code_review, type_review) FROM stdin;
31	ALMOX.SMT/PTH		66	1
42	Banc Interditada		67	1
93	Bancada de reparo		68	1
78	Bancada Zera		69	1
6	BI-01		70	1
7	BI-02		71	1
10	BI-03		72	1
11	BI-04		73	1
8	BI-05		74	1
96	BI 01		75	1
65	BM-01		76	1
64	BM-02		77	1
59	BM-04		78	1
67	BM-05		79	1
77	BM 06-Produção Eletronico		80	1
58	BMC-01		81	1
61	BMC-02		82	1
57	BMC-03		83	1
60	BMC-04		84	1
62	BMC-05		85	1
32	Plastico		109	1
27	plasticos		110	1
25	Plásticos		111	1
69	Processo		112	1
73	Produção Eletrônico		113	1
33	PTH		114	1
68	QUALIDADE PLASTICO		115	1
90	R.C.T-01		116	1
91	R.C.T-02		117	1
89	R.C.T-03		118	1
21	Recebimento		119	1
100	Recebimento/ Cecilia		120	1
94	Reparo		121	1
35	RMA		122	1
43	Sala de Instrumentação		123	1
86	Sala de instrumentação - HD		124	1
13	Sala instrumentação		125	1
23	SESMT		126	1
26	SMT		127	1
105	travado		128	1
106	BI-06		184	0
63	BMC-06		185	1
44	BP-01		186	1
52	BP-02		187	1
41	BP-04		188	1
45	BP-05		189	1
51	BP-06		190	1
54	BP-07		191	1
76	BP 06-Produção Eletronico		192	1
49	BPC-04		193	1
53	BPC-05		194	1
48	BPC-06		195	1
50	BPC-07		196	1
56	BPC-08		197	1
47	BPC-09		198	1
46	BPC-10		199	1
55	BPVA-02		200	1
92	BTA-01		201	1
98	BTA-02		202	1
97	BTA-04		203	1
34	calibração		204	1
77	BM-06		275	1
34	Calibração		276	1
103	DANIFICADA		277	1
101	DANIFICADO		278	1
79	Desativada		279	1
87	Em Calibração		280	1
1	ENGENHARIA		281	1
20	ESTUFA		282	1
102	Estufa - ETF 07		283	1
88	ETF - 07		284	1
82	F.T.C-01		285	1
83	F.T.C-02		286	1
85	F.T.C-03		287	1
84	F.T.C-05		288	1
81	F.T.C-06		289	1
24	FCT		290	1
16	FM-01		291	1
14	FM-02		292	1
17	FM-03		293	1
5	FME/Engenharia		294	1
19	FP-01		295	1
18	FP-02		296	1
15	FP-03		297	1
29	Galpão 2 / THC		298	1
39	Galpão 3		299	1
30	Galpão 3 / Almoxarifado		300	1
40	L3		301	1
37	L4		302	1
36	L5		303	1
38	L6		304	1
80	Lab. Eletrônico		305	1
99	Laboratório 01		306	1
71	Laboratório de Ensaio		307	1
74	LI 03		308	1
66	Manutenção		309	1
12	Metrologia		310	1
28	Moinho		311	1
3	NÃO INFORMADO		312	1
4	P&D		313	1
22	Para Calibração		314	1
70	PCI		315	1
27	Plasticos		316	1
9	ZERA	\N	317	1
74	LI-03		318	1
76	Produção		401	1
107	BPC-02		406	0
108	BPC-01		417	0
109	BME - 01		426	0
110	L2		669	0
111	Ambulatório		1206	0
112	P&DEnergiaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa		1239	0
112	P&DEnergiaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa		1240	2
112	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	1259	0
112	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	1265	2
14	test3		1271	1
112	zTest z	zTest z	1286	0
113	1 Test 1		1312	0
113	1 Test 1		1320	2
\.


--
-- TOC entry 2312 (class 0 OID 49439)
-- Dependencies: 199
-- Data for Name: tb_measuring_instrument_standard; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_measuring_instrument_standard (code, name, description, version) FROM stdin;
1	ISO 9001		1
2	Portaria Inmetro n.º 400		1
3	ISO NBR 17025		0
4	ISO 123		0
6	zTest z		0
\.


--
-- TOC entry 2313 (class 0 OID 49445)
-- Dependencies: 200
-- Data for Name: tb_measuring_instrument_standard_aud; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_measuring_instrument_standard_aud (code, name, description, code_review, type_review) FROM stdin;
1	ISO 9001		86	1
2	Portaria Inmetro n.º 400		87	1
3	ISO NBR 17025		229	0
4	ISO 123		1238	0
5	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	1261	0
5	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	1263	2
5	123AABBCC 123AABBCC 123AABBCC 123AABBCC123AABBCC		1273	0
5	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC		1274	1
6	zTest z		1288	0
5	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC		1289	2
7	1 Test 1		1314	0
7	1 Test 1		1322	2
\.


--
-- TOC entry 2314 (class 0 OID 49451)
-- Dependencies: 201
-- Data for Name: tb_measuring_instrument_type; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_measuring_instrument_type (code, name, description, version) FROM stdin;
65	Padrão de Watt-hora	\N	0
69	Multimedidor - THD	\N	0
70	Multimedidor	\N	0
77	Medidor de Perdas	\N	0
81	Fonte Padrão Trifasica - HEXING	\N	0
10	Alicate Amperimetro Digital		1
44	Alicate Wattímetro		1
6	Amperímetro Digital		1
76	Analisador de Qual. de Energia-THD		1
45	Analisador de Qualidade de Energia		1
53	Bancada de Inspeção Mono		1
52	Bancada de Inspeção Poli		1
63	Bancada de Tensão Aplicada		1
61	Bancada de Verificação Mono		1
62	Bancada de Verificação Poli		1
59	Calibração das Bancadas de Ensaio		1
32	Calibrador Acústico		1
16	Calibrador Rosca Anel		1
17	Calibrador Rosca Tampão		1
18	Calibrador Rosca Tampão PnP		1
19	Calibrador Tampão Liso		1
9	Camara Climatica		1
60	Comparação Interlaboratorial		1
5	Hipot Tester Digital	\N	0
8	Estufa	\N	0
11	Multimetro Digital	\N	0
12	Fonte de Tensão	\N	0
13	Multimetro de Precisão	\N	0
24	Micrômetro Externo	\N	0
29	Dosímetro Digital	\N	0
31	Luxímetro	\N	0
33	Multímetro de precisão	\N	0
35	Injetora	\N	0
36	Controlador de temperatura	\N	0
39	Multímetro	\N	0
46	Medidor de resistividade	\N	0
48	Medidor de tensão de tecido digital	\N	0
41	Trena Infravermelho		3
1	Padrão de Watt-hora - HEXING		1
74	Padrão de Watt-hora - Portátil		1
72	Padrão de Watt-hora - Portátil - THD		1
64	Padrão de Watt-hora -Referência		1
79	Padrão de Watt-hora – Referência		1
75	Padrão de Watt-hora - SMS		1
67	Padrão de Watt-hora - Weco		1
15	Padrão de Watt-hora - Zera		1
73	Padrão de Watt-hora - Zera Portatil		1
20	Paquímetro - Analógico		1
42	Tridimensional		1
40	Perfil Termico		1
2	Pistola de temperatura		1
50	Pistola Infravermelho		1
47	Ponta Atenuadora de Tensão		1
21	Relógio Apalpador		1
22	Relógio Apalpador Centesimal		1
23	Relógio Comparador		1
68	Sensor PT-100		1
14	Paquímetro Digital		2
43	Balança Digital		2
80	Micrômetro Externo com Faca	\N	1
34	Temporizador Analógico		1
51	Termômetro  Digital Espeto 		3
49	Termômetro digital com sensor termopar		2
84	Esfigmomanômetro Aneróide		0
85	Termômetro digital som sensor termorresistivo		0
30	Termômetro de Globo Digital		1
37	Testador de Pulseira e Calcanheira		2
28	Torquimetro		1
26	Torquimetro Analógico		1
25	Torquimetro Axial		1
78	Traçador de Altura Digital		1
56	Validação de Software Mono		1
55	Validação de Software Poli		1
57	Validação de Software Zera		1
54	Verificação de Tensão Aplicada		1
58	Verificação dos Sensores da Estufa		1
27	Torquimetro Analógico 		2
66	Padrão de Watt-hora - Radian		2
82	Termo Higrômetro Digital		1
71	Termo Higrógrafo		2
38	Termo Higrômetro		2
3	Termo Higrômetro Digital - banc.		2
4	Termo Higrômetro Digital 		2
83	Bancada de calibração mono		0
86	teste 123		0
7	test		2
87	zTest z	zTest z	0
\.


--
-- TOC entry 2315 (class 0 OID 49457)
-- Dependencies: 202
-- Data for Name: tb_measuring_instrument_type_aud; Type: TABLE DATA; Schema: mecsw; Owner: postgres
--

COPY tb_measuring_instrument_type_aud (code, name, description, code_review, type_review) FROM stdin;
2	teste	teste	6	2
1	teste	teste	7	2
1	asdasd	asdasdasd	8	0
2	asdasdasd	asdasdasdasd	9	0
2	asdasdasd	asdasdasdasd	10	2
1	asdasd	asdasdasd	11	2
1	teste		12	0
2	teste2	teste3	13	0
3	teste3	teste3	14	0
4	teste5	teste5	17	0
10	Alicate Amperimetro Digital		25	1
44	Alicate Wattímetro		26	1
6	Amperímetro Digital		27	1
76	Analisador de Qual. de Energia-THD		28	1
45	Analisador de Qualidade de Energia		29	1
43	balança digital		30	1
53	Bancada de Inspeção Mono		31	1
52	Bancada de Inspeção Poli		32	1
63	Bancada de Tensão Aplicada		33	1
61	Bancada de Verificação Mono		34	1
62	Bancada de Verificação Poli		35	1
59	Calibração das Bancadas de Ensaio		36	1
32	Calibrador Acústico		37	1
49	Calibrador Estação de Solda		38	1
16	Calibrador Rosca Anel		39	1
17	Calibrador Rosca Tampão		40	1
18	Calibrador Rosca Tampão PnP		41	1
19	Calibrador Tampão Liso		42	1
9	Camara Climatica		43	1
60	Comparação Interlaboratorial		44	1
66	Padrão de Watt-hora - BME		130	1
1	Padrão de Watt-hora - HEXING		131	1
74	Padrão de Watt-hora - Portátil		132	1
72	Padrão de Watt-hora - Portátil - THD		133	1
64	Padrão de Watt-hora -Referência		134	1
79	Padrão de Watt-hora – Referência		135	1
75	Padrão de Watt-hora - SMS		136	1
67	Padrão de Watt-hora - Weco		137	1
15	Padrão de Watt-hora - Zera		138	1
73	Padrão de Watt-hora - Zera Portatil		139	1
20	Paquímetro - Analógico		140	1
14	Paquímetro Digital	aaa	141	1
40	Perfil Termico		142	1
2	Pistola de temperatura		143	1
50	Pistola Infravermelho		144	1
47	Ponta Atenuadora de Tensão		145	1
21	Relógio Apalpador		146	1
22	Relógio Apalpador Centesimal		147	1
23	Relógio Comparador		148	1
68	Sensor PT-100		149	1
14	Paquímetro Digital		150	1
43	Balança Digital		269	1
80	Micrômetro Externo com Faca	\N	270	1
37	Testador de Pulseira e Calcanheira	\N	271	1
7	Voltimetro		272	1
34	Temporizador Analógico		358	1
71	Termohigrógrafo		359	1
38	Termohigrômetro		360	1
4	Termohigrômetro Digital		361	1
3	Termohigrômetro Digital - banc.		362	1
30	Termômetro de Globo Digital		363	1
51	Termometro Espeto Digital		364	1
37	Testador de Pulseira e Calcanheira		365	1
27	Torquimetro Analógico 1		366	1
28	Torquimetro		367	1
26	Torquimetro Analógico		368	1
25	Torquimetro Axial		369	1
78	Traçador de Altura Digital		370	1
41	Trena com Infla-Vermelho		371	1
42	Tridimensional		372	1
56	Validação de Software Mono		373	1
55	Validação de Software Poli		374	1
57	Validação de Software Zera		375	1
54	Verificação de Tensão Aplicada		376	1
58	Verificação dos Sensores da Estufa		377	1
27	Torquimetro Analógico 		378	1
66	Padrão de Watt-hora - Radian		400	1
82	Termo Higrometro Digital		501	0
82	Termo Higrômetro Digital		502	1
71	Termo Higrógrafo		503	1
38	Termo Higrômetro		505	1
3	Termo Higrômetro Digital - banc.		506	1
51	Termômetro Espeto Digital		507	1
4	Termo Higrômetro Digital 		508	1
83	Bancada de calibração mono		608	0
41	Trena Infra-Vermelho		657	1
41	Trena Infravermelho		658	1
51	Termômetro  Digital Espeto 		701	1
49	Termômetro digital com sensor termopar		829	1
84	Esfigmomanômetro Aneróide		1185	0
85	Termômetro digital som sensor termorresistivo		1212	0
86	teste 123		1255	0
87	123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	1256	0
87	123AABBCC	123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC123AABBCC	1267	2
7	test		1268	1
87	zTest z	zTest z	1283	0
88	1 Test 1		1309	0
88	1 Test 1		1318	2
\.


--
-- TOC entry 2316 (class 0 OID 49463)
-- Dependencies: 203
-- Data for Name: tb_review; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_review (code, audit_date, user_code, transaction_code, transaction_title) FROM stdin;
2	2017-12-02 19:57:27.372	2	28	createLine
3	2017-12-02 19:57:42.69	2	28	createLine
6	2017-12-02 20:05:46.745	2	30	removeLine
7	2017-12-02 20:05:51.846	2	30	removeLine
8	2017-12-02 20:10:16.884	2	28	createLine
9	2017-12-02 20:10:21.048	2	28	createLine
10	2017-12-02 20:10:29.409	2	30	removeLine
11	2017-12-02 20:12:14.97	2	30	removeLine
12	2017-12-03 13:14:33.934	2	28	createLine
13	2017-12-03 13:38:55.798	2	28	createLine
14	2017-12-03 14:20:11.917	2	28	createLine
15	2017-12-03 14:22:40.026	2	28	createLine
16	2017-12-03 14:22:58.689	2	28	createLine
17	2017-12-03 15:08:13.155	2	28	createLine
18	2017-12-03 15:08:26.123	2	28	createLine
23	2017-12-28 01:14:16.006	2	5	updateMeasuringInstrument
24	2017-12-28 01:14:29.917	2	5	updateMeasuringInstrument
25	2017-12-28 01:15:18.971	2	5	updateMeasuringInstrument
26	2017-12-28 01:15:18.992	2	5	updateMeasuringInstrument
27	2017-12-28 01:15:18.996	2	5	updateMeasuringInstrument
28	2017-12-28 01:15:19.002	2	5	updateMeasuringInstrument
29	2017-12-28 01:15:19.01	2	5	updateMeasuringInstrument
30	2017-12-28 01:15:19.016	2	5	updateMeasuringInstrument
31	2017-12-28 01:15:19.023	2	5	updateMeasuringInstrument
32	2017-12-28 01:15:19.032	2	5	updateMeasuringInstrument
33	2017-12-28 01:15:19.039	2	5	updateMeasuringInstrument
34	2017-12-28 01:15:19.049	2	5	updateMeasuringInstrument
35	2017-12-28 01:15:19.073	2	5	updateMeasuringInstrument
36	2017-12-28 01:15:19.083	2	5	updateMeasuringInstrument
37	2017-12-28 01:15:19.092	2	5	updateMeasuringInstrument
38	2017-12-28 01:15:19.099	2	5	updateMeasuringInstrument
39	2017-12-28 01:15:19.106	2	5	updateMeasuringInstrument
40	2017-12-28 01:15:19.113	2	5	updateMeasuringInstrument
41	2017-12-28 01:15:19.119	2	5	updateMeasuringInstrument
42	2017-12-28 01:15:19.129	2	5	updateMeasuringInstrument
43	2017-12-28 01:15:19.135	2	5	updateMeasuringInstrument
44	2017-12-28 01:15:19.144	2	5	updateMeasuringInstrument
45	2017-12-28 01:15:26.352	2	8	removeMeasuringInstrument
46	2017-12-28 01:15:26.385	2	8	removeMeasuringInstrument
47	2017-12-28 01:15:26.392	2	8	removeMeasuringInstrument
48	2017-12-28 01:15:26.402	2	8	removeMeasuringInstrument
49	2017-12-28 01:15:26.409	2	8	removeMeasuringInstrument
50	2017-12-28 01:15:26.422	2	8	removeMeasuringInstrument
51	2017-12-28 01:15:26.43	2	8	removeMeasuringInstrument
52	2017-12-28 01:15:26.438	2	8	removeMeasuringInstrument
53	2017-12-28 01:15:26.483	2	8	removeMeasuringInstrument
54	2017-12-28 01:15:26.491	2	8	removeMeasuringInstrument
55	2017-12-28 01:15:26.501	2	8	removeMeasuringInstrument
56	2017-12-28 01:15:26.507	2	8	removeMeasuringInstrument
57	2017-12-28 01:15:26.515	2	8	removeMeasuringInstrument
58	2017-12-28 01:15:26.522	2	8	removeMeasuringInstrument
59	2017-12-28 01:15:26.53	2	8	removeMeasuringInstrument
60	2017-12-28 01:15:26.538	2	8	removeMeasuringInstrument
61	2017-12-28 01:15:26.551	2	8	removeMeasuringInstrument
62	2017-12-28 01:15:26.567	2	8	removeMeasuringInstrument
63	2017-12-28 01:15:26.576	2	8	removeMeasuringInstrument
64	2017-12-28 01:15:26.591	2	8	removeMeasuringInstrument
66	2017-12-28 01:15:48.348	2	8	removeMeasuringInstrument
67	2017-12-28 01:15:48.364	2	8	removeMeasuringInstrument
68	2017-12-28 01:15:48.371	2	8	removeMeasuringInstrument
69	2017-12-28 01:15:48.376	2	8	removeMeasuringInstrument
70	2017-12-28 01:15:48.382	2	8	removeMeasuringInstrument
71	2017-12-28 01:15:48.388	2	8	removeMeasuringInstrument
72	2017-12-28 01:15:48.402	2	8	removeMeasuringInstrument
73	2017-12-28 01:15:48.414	2	8	removeMeasuringInstrument
74	2017-12-28 01:15:48.423	2	8	removeMeasuringInstrument
75	2017-12-28 01:15:48.43	2	8	removeMeasuringInstrument
76	2017-12-28 01:15:48.439	2	8	removeMeasuringInstrument
77	2017-12-28 01:15:48.449	2	8	removeMeasuringInstrument
78	2017-12-28 01:15:48.457	2	8	removeMeasuringInstrument
79	2017-12-28 01:15:48.463	2	8	removeMeasuringInstrument
80	2017-12-28 01:15:48.475	2	8	removeMeasuringInstrument
81	2017-12-28 01:15:48.486	2	8	removeMeasuringInstrument
82	2017-12-28 01:15:48.492	2	8	removeMeasuringInstrument
83	2017-12-28 01:15:48.502	2	8	removeMeasuringInstrument
84	2017-12-28 01:15:48.509	2	8	removeMeasuringInstrument
85	2017-12-28 01:15:48.519	2	8	removeMeasuringInstrument
86	2017-12-28 01:15:52.681	2	8	removeMeasuringInstrument
87	2017-12-28 01:15:52.698	2	8	removeMeasuringInstrument
88	2017-12-29 15:01:21.545	3	5	updateMeasuringInstrument
89	2017-12-29 15:04:48.108	3	5	updateMeasuringInstrument
90	2017-12-29 15:17:47.426	4	5	updateMeasuringInstrument
91	2017-12-29 15:18:03.164	4	5	updateMeasuringInstrument
92	2017-12-29 15:18:41.84	4	5	updateMeasuringInstrument
93	2017-12-29 15:21:40.523	4	5	updateMeasuringInstrument
94	2017-12-29 15:26:11.469	4	5	updateMeasuringInstrument
95	2017-12-29 15:40:11.987	4	5	updateMeasuringInstrument
96	2017-12-29 15:42:14.468	4	5	updateMeasuringInstrument
97	2017-12-29 15:47:30.943	4	5	updateMeasuringInstrument
98	2017-12-29 15:48:16.677	4	5	updateMeasuringInstrument
99	2017-12-29 15:49:46.6	4	5	updateMeasuringInstrument
100	2017-12-29 16:29:01.805	4	7	removeMeasuringInstrument
102	2017-12-29 17:02:25.406	4	8	removeMeasuringInstrument
103	2017-12-29 17:03:38.858	4	7	removeMeasuringInstrument
130	2017-12-29 17:26:25.298	4	\N	\N
109	2017-12-29 17:09:13.621	4	8	removeMeasuringInstrument
110	2017-12-29 17:09:13.864	4	8	removeMeasuringInstrument
111	2017-12-29 17:09:13.942	4	8	removeMeasuringInstrument
112	2017-12-29 17:09:13.974	4	8	removeMeasuringInstrument
113	2017-12-29 17:09:13.997	4	8	removeMeasuringInstrument
114	2017-12-29 17:09:14.013	4	8	removeMeasuringInstrument
115	2017-12-29 17:09:14.038	4	8	removeMeasuringInstrument
116	2017-12-29 17:09:14.072	4	8	removeMeasuringInstrument
117	2017-12-29 17:09:14.147	4	8	removeMeasuringInstrument
118	2017-12-29 17:09:14.173	4	8	removeMeasuringInstrument
119	2017-12-29 17:09:14.216	4	8	removeMeasuringInstrument
120	2017-12-29 17:09:14.234	4	8	removeMeasuringInstrument
121	2017-12-29 17:09:14.254	4	8	removeMeasuringInstrument
122	2017-12-29 17:09:14.278	4	8	removeMeasuringInstrument
123	2017-12-29 17:09:14.296	4	8	removeMeasuringInstrument
124	2017-12-29 17:09:14.318	4	8	removeMeasuringInstrument
125	2017-12-29 17:09:14.334	4	8	removeMeasuringInstrument
126	2017-12-29 17:09:14.355	4	8	removeMeasuringInstrument
127	2017-12-29 17:09:14.37	4	8	removeMeasuringInstrument
128	2017-12-29 17:09:14.386	4	8	removeMeasuringInstrument
131	2017-12-29 17:26:25.406	4	\N	\N
132	2017-12-29 17:26:25.424	4	\N	\N
133	2017-12-29 17:26:25.437	4	\N	\N
135	2017-12-29 17:26:25.462	4	\N	\N
137	2017-12-29 17:26:25.498	4	\N	\N
139	2017-12-29 17:26:25.553	4	\N	\N
134	2017-12-29 17:26:25.449	4	\N	\N
136	2017-12-29 17:26:25.485	4	\N	\N
138	2017-12-29 17:26:25.511	4	\N	\N
140	2017-12-29 17:26:25.584	4	\N	\N
141	2017-12-29 17:26:25.605	4	\N	\N
142	2017-12-29 17:26:25.615	4	\N	\N
143	2017-12-29 17:26:25.623	4	\N	\N
144	2017-12-29 17:26:25.633	4	\N	\N
145	2017-12-29 17:26:25.649	4	\N	\N
146	2017-12-29 17:26:25.665	4	\N	\N
147	2017-12-29 17:26:25.682	4	\N	\N
148	2017-12-29 17:26:25.693	4	\N	\N
149	2017-12-29 17:26:25.705	4	\N	\N
150	2017-12-29 17:26:50.03	4	\N	\N
151	2017-12-29 17:27:47.987	4	8	removeMeasuringInstrument
152	2017-12-29 17:27:48.004	4	8	removeMeasuringInstrument
153	2017-12-29 17:27:48.025	4	8	removeMeasuringInstrument
154	2017-12-29 17:27:48.049	4	8	removeMeasuringInstrument
155	2017-12-29 17:27:48.085	4	8	removeMeasuringInstrument
156	2017-12-29 17:27:48.102	4	8	removeMeasuringInstrument
157	2017-12-29 19:23:51.066	4	\N	\N
158	2017-12-29 19:23:51.135	4	\N	\N
159	2017-12-29 19:23:51.162	4	\N	\N
160	2017-12-29 19:23:51.192	4	\N	\N
161	2017-12-29 19:23:51.219	4	\N	\N
162	2017-12-29 19:23:51.246	4	\N	\N
163	2017-12-29 19:23:51.271	4	\N	\N
164	2017-12-29 19:23:51.292	4	\N	\N
165	2017-12-29 19:23:51.314	4	\N	\N
166	2017-12-29 19:23:51.336	4	\N	\N
167	2017-12-29 19:23:51.364	4	\N	\N
168	2017-12-29 19:23:51.387	4	\N	\N
169	2017-12-29 19:23:51.42	4	\N	\N
170	2017-12-29 19:23:51.442	4	\N	\N
171	2017-12-29 19:23:51.458	4	\N	\N
172	2017-12-29 19:23:51.478	4	\N	\N
173	2017-12-29 19:23:51.494	4	\N	\N
174	2017-12-29 19:23:51.508	4	\N	\N
175	2017-12-29 19:23:51.523	4	\N	\N
176	2017-12-29 19:23:51.537	4	\N	\N
177	2017-12-29 19:23:51.552	4	\N	\N
178	2017-12-29 19:30:37.531	4	4	createMeasuringInstrument
179	2017-12-29 19:33:06.755	4	5	updateMeasuringInstrument
180	2017-12-29 19:36:49.824	4	5	updateMeasuringInstrument
181	2017-12-29 19:40:51.496	4	5	updateMeasuringInstrument
182	2017-12-29 20:20:45.294	4	7	removeMeasuringInstrument
183	2017-12-29 20:29:07.769	4	8	removeMeasuringInstrument
184	2017-12-29 20:31:31.028	4	8	removeMeasuringInstrument
185	2017-12-29 20:31:31.066	4	8	removeMeasuringInstrument
186	2017-12-29 20:31:31.072	4	8	removeMeasuringInstrument
187	2017-12-29 20:31:31.08	4	8	removeMeasuringInstrument
188	2017-12-29 20:31:31.087	4	8	removeMeasuringInstrument
189	2017-12-29 20:31:31.093	4	8	removeMeasuringInstrument
190	2017-12-29 20:31:31.102	4	8	removeMeasuringInstrument
191	2017-12-29 20:31:31.108	4	8	removeMeasuringInstrument
192	2017-12-29 20:31:31.114	4	8	removeMeasuringInstrument
193	2017-12-29 20:31:31.119	4	8	removeMeasuringInstrument
194	2017-12-29 20:31:31.125	4	8	removeMeasuringInstrument
195	2017-12-29 20:31:31.133	4	8	removeMeasuringInstrument
196	2017-12-29 20:31:31.14	4	8	removeMeasuringInstrument
197	2017-12-29 20:31:31.162	4	8	removeMeasuringInstrument
198	2017-12-29 20:31:31.172	4	8	removeMeasuringInstrument
199	2017-12-29 20:31:31.18	4	8	removeMeasuringInstrument
200	2017-12-29 20:31:31.186	4	8	removeMeasuringInstrument
201	2017-12-29 20:31:31.192	4	8	removeMeasuringInstrument
202	2017-12-29 20:31:31.198	4	8	removeMeasuringInstrument
203	2017-12-29 20:31:31.207	4	8	removeMeasuringInstrument
204	2017-12-29 20:31:31.217	4	8	removeMeasuringInstrument
205	2017-12-29 20:35:28.73	4	8	removeMeasuringInstrument
206	2017-12-29 20:35:28.749	4	8	removeMeasuringInstrument
207	2017-12-29 20:35:28.756	4	8	removeMeasuringInstrument
208	2017-12-29 20:35:28.761	4	8	removeMeasuringInstrument
209	2017-12-29 20:35:28.767	4	8	removeMeasuringInstrument
210	2017-12-29 20:35:28.776	4	8	removeMeasuringInstrument
211	2017-12-29 20:35:28.782	4	8	removeMeasuringInstrument
212	2017-12-29 20:35:28.789	4	8	removeMeasuringInstrument
213	2017-12-29 20:35:28.795	4	8	removeMeasuringInstrument
214	2017-12-29 20:35:28.801	4	8	removeMeasuringInstrument
215	2017-12-29 20:35:28.808	4	8	removeMeasuringInstrument
216	2017-12-29 20:35:28.821	4	8	removeMeasuringInstrument
217	2017-12-29 20:35:28.828	4	8	removeMeasuringInstrument
218	2017-12-29 20:35:28.842	4	8	removeMeasuringInstrument
219	2017-12-29 20:35:28.855	4	8	removeMeasuringInstrument
220	2017-12-29 20:35:28.884	4	8	removeMeasuringInstrument
221	2017-12-29 20:35:28.901	4	8	removeMeasuringInstrument
222	2017-12-29 20:35:28.907	4	8	removeMeasuringInstrument
223	2017-12-29 20:35:28.927	4	8	removeMeasuringInstrument
224	2017-12-29 20:35:44.512	4	8	removeMeasuringInstrument
225	2017-12-29 20:36:34.567	4	8	removeMeasuringInstrument
226	2017-12-29 20:36:55.404	4	8	removeMeasuringInstrument
227	2017-12-29 20:37:14.22	4	8	removeMeasuringInstrument
657	2018-02-15 16:32:49.384	4	\N	\N
229	2017-12-29 20:40:28.58	4	4	createMeasuringInstrument
236	2017-12-29 20:58:36.845	4	8	removeMeasuringInstrument
237	2017-12-29 20:58:36.872	4	8	removeMeasuringInstrument
238	2017-12-29 20:58:36.885	4	8	removeMeasuringInstrument
239	2017-12-29 20:58:36.901	4	8	removeMeasuringInstrument
240	2017-12-29 20:58:36.927	4	8	removeMeasuringInstrument
241	2017-12-29 20:58:36.952	4	8	removeMeasuringInstrument
242	2017-12-29 20:58:36.979	4	8	removeMeasuringInstrument
243	2017-12-29 20:58:36.991	4	8	removeMeasuringInstrument
244	2017-12-29 20:58:37.005	4	8	removeMeasuringInstrument
245	2017-12-29 20:58:37.019	4	8	removeMeasuringInstrument
246	2017-12-29 20:58:37.035	4	8	removeMeasuringInstrument
247	2017-12-29 20:58:37.051	4	8	removeMeasuringInstrument
250	2017-12-29 20:58:38.264	4	8	removeMeasuringInstrument
252	2017-12-29 20:58:38.303	4	8	removeMeasuringInstrument
254	2017-12-29 20:58:38.345	4	8	removeMeasuringInstrument
248	2017-12-29 20:58:37.072	4	8	removeMeasuringInstrument
249	2017-12-29 20:58:37.095	4	8	removeMeasuringInstrument
251	2017-12-29 20:58:38.276	4	8	removeMeasuringInstrument
253	2017-12-29 20:58:38.321	4	8	removeMeasuringInstrument
255	2017-12-29 20:58:38.356	4	8	removeMeasuringInstrument
259	2017-12-29 20:59:48.375	3	4	createMeasuringInstrument
267	2017-12-29 21:01:39.247	3	5	updateMeasuringInstrument
269	2017-12-29 21:03:35.289	3	5	updateMeasuringInstrument
270	2017-12-29 21:04:59.549	3	5	updateMeasuringInstrument
271	2017-12-29 21:04:59.645	3	5	updateMeasuringInstrument
272	2017-12-29 21:04:59.695	3	5	updateMeasuringInstrument
273	2017-12-29 21:05:42.875	3	8	removeMeasuringInstrument
274	2017-12-29 21:05:42.895	3	8	removeMeasuringInstrument
275	2017-12-29 21:08:53.366	3	8	removeMeasuringInstrument
276	2018-01-03 21:13:41.627	3	\N	\N
277	2018-01-03 21:13:41.756	3	\N	\N
278	2018-01-03 21:13:41.776	3	\N	\N
279	2018-01-03 21:13:41.796	3	\N	\N
280	2018-01-03 21:13:41.809	3	\N	\N
281	2018-01-03 21:13:41.821	3	\N	\N
282	2018-01-03 21:13:41.837	3	\N	\N
283	2018-01-03 21:13:41.852	3	\N	\N
284	2018-01-03 21:13:41.867	3	\N	\N
285	2018-01-03 21:13:41.89	3	\N	\N
286	2018-01-03 21:13:41.901	3	\N	\N
287	2018-01-03 21:13:41.914	3	\N	\N
288	2018-01-03 21:13:41.927	3	\N	\N
289	2018-01-03 21:13:41.94	3	\N	\N
290	2018-01-03 21:13:41.957	3	\N	\N
291	2018-01-03 21:13:41.974	3	\N	\N
292	2018-01-03 21:13:41.992	3	\N	\N
293	2018-01-03 21:13:42.008	3	\N	\N
294	2018-01-03 21:13:42.02	3	\N	\N
295	2018-01-03 21:13:42.042	3	\N	\N
296	2018-01-03 21:13:42.054	3	\N	\N
297	2018-01-03 21:13:42.096	3	\N	\N
298	2018-01-03 21:13:42.11	3	\N	\N
299	2018-01-03 21:13:42.136	3	\N	\N
300	2018-01-03 21:13:42.144	3	\N	\N
301	2018-01-03 21:13:42.169	3	\N	\N
302	2018-01-03 21:13:42.183	3	\N	\N
303	2018-01-03 21:13:42.201	3	\N	\N
304	2018-01-03 21:13:42.23	3	\N	\N
305	2018-01-03 21:13:42.243	3	\N	\N
306	2018-01-03 21:13:42.274	3	\N	\N
307	2018-01-03 21:13:42.292	3	\N	\N
308	2018-01-03 21:13:42.314	3	\N	\N
309	2018-01-03 21:13:42.329	3	\N	\N
310	2018-01-03 21:13:42.338	3	\N	\N
311	2018-01-03 21:13:42.349	3	\N	\N
312	2018-01-03 21:13:42.359	3	\N	\N
313	2018-01-03 21:13:42.374	3	\N	\N
314	2018-01-03 21:13:42.385	3	\N	\N
315	2018-01-03 21:13:42.397	3	\N	\N
316	2018-01-03 21:13:42.413	3	\N	\N
317	2018-01-03 21:13:42.491	3	\N	\N
318	2018-01-03 21:17:02.534	3	\N	\N
319	2018-01-03 21:30:55.021	3	5	updateMeasuringInstrument
320	2018-01-03 21:31:39.104	3	5	updateMeasuringInstrument
321	2018-01-03 21:32:27.419	3	5	updateMeasuringInstrument
322	2018-01-03 21:32:59.612	3	5	updateMeasuringInstrument
323	2018-01-03 21:37:26.306	3	5	updateMeasuringInstrument
324	2018-01-03 21:38:10.019	3	5	updateMeasuringInstrument
325	2018-01-03 21:38:38.151	3	5	updateMeasuringInstrument
326	2018-01-03 21:39:15.143	3	5	updateMeasuringInstrument
327	2018-01-03 21:42:43.913	3	5	updateMeasuringInstrument
328	2018-01-03 21:50:22.776	3	5	updateMeasuringInstrument
329	2018-01-03 21:51:06.588	3	5	updateMeasuringInstrument
330	2018-01-06 14:11:11.309	4	\N	\N
331	2018-01-06 14:25:20.732	4	5	updateMeasuringInstrument
332	2018-01-06 16:01:50.762	4	5	updateMeasuringInstrument
333	2018-01-06 16:04:05.109	4	5	updateMeasuringInstrument
334	2018-01-06 16:05:54.83	4	5	updateMeasuringInstrument
335	2018-01-06 16:08:33.72	4	5	updateMeasuringInstrument
336	2018-01-08 12:49:31.764	4	5	updateMeasuringInstrument
337	2018-01-08 12:54:04.258	4	5	updateMeasuringInstrument
338	2018-01-08 14:47:26.652	4	5	updateMeasuringInstrument
339	2018-01-08 14:50:47.565	4	5	updateMeasuringInstrument
340	2018-01-08 17:58:27.018	4	5	updateMeasuringInstrument
341	2018-01-08 17:59:12.816	4	5	updateMeasuringInstrument
342	2018-01-08 18:01:02.435	4	5	updateMeasuringInstrument
343	2018-01-09 10:53:55.075	4	5	updateMeasuringInstrument
344	2018-01-09 11:09:08.855	4	5	updateMeasuringInstrument
345	2018-01-09 11:35:51.186	4	5	updateMeasuringInstrument
346	2018-01-09 11:37:15.804	4	5	updateMeasuringInstrument
347	2018-01-09 11:37:55.273	4	5	updateMeasuringInstrument
348	2018-01-09 11:38:23.763	4	5	updateMeasuringInstrument
349	2018-01-09 11:57:10.233	4	5	updateMeasuringInstrument
350	2018-01-09 11:57:56.739	4	5	updateMeasuringInstrument
351	2018-01-09 11:58:29.605	4	5	updateMeasuringInstrument
352	2018-01-09 11:58:59.018	4	5	updateMeasuringInstrument
353	2018-01-09 14:01:46.111	4	5	updateMeasuringInstrument
354	2018-01-09 14:03:55.247	4	5	updateMeasuringInstrument
355	2018-01-09 14:06:27.274	4	5	updateMeasuringInstrument
356	2018-01-09 14:07:31.09	4	5	updateMeasuringInstrument
357	2018-01-09 14:08:59.921	4	5	updateMeasuringInstrument
358	2018-01-09 14:30:47.95	4	5	updateMeasuringInstrument
359	2018-01-09 14:30:47.969	4	5	updateMeasuringInstrument
360	2018-01-09 14:30:47.982	4	5	updateMeasuringInstrument
361	2018-01-09 14:30:48.001	4	5	updateMeasuringInstrument
362	2018-01-09 14:30:48.016	4	5	updateMeasuringInstrument
363	2018-01-09 14:30:48.042	4	5	updateMeasuringInstrument
364	2018-01-09 14:30:48.054	4	5	updateMeasuringInstrument
365	2018-01-09 14:30:48.065	4	5	updateMeasuringInstrument
368	2018-01-09 14:30:48.118	4	5	updateMeasuringInstrument
369	2018-01-09 14:30:48.14	4	5	updateMeasuringInstrument
372	2018-01-09 14:30:48.183	4	5	updateMeasuringInstrument
373	2018-01-09 14:30:48.191	4	5	updateMeasuringInstrument
376	2018-01-09 14:30:48.22	4	5	updateMeasuringInstrument
377	2018-01-09 14:30:48.229	4	5	updateMeasuringInstrument
379	2018-01-09 16:28:50.337	4	5	updateMeasuringInstrument
366	2018-01-09 14:30:48.078	4	5	updateMeasuringInstrument
367	2018-01-09 14:30:48.092	4	5	updateMeasuringInstrument
370	2018-01-09 14:30:48.155	4	5	updateMeasuringInstrument
371	2018-01-09 14:30:48.167	4	5	updateMeasuringInstrument
374	2018-01-09 14:30:48.201	4	5	updateMeasuringInstrument
375	2018-01-09 14:30:48.21	4	5	updateMeasuringInstrument
378	2018-01-09 14:31:09.657	4	5	updateMeasuringInstrument
380	2018-01-10 10:11:35.575	4	5	updateMeasuringInstrument
381	2018-01-10 10:14:35.765	4	5	updateMeasuringInstrument
382	2018-01-10 10:15:15.526	4	5	updateMeasuringInstrument
383	2018-01-10 10:15:58.332	4	5	updateMeasuringInstrument
384	2018-01-10 10:16:35.585	4	5	updateMeasuringInstrument
385	2018-01-10 13:09:18.997	3	5	updateMeasuringInstrument
386	2018-01-10 13:15:15.277	3	5	updateMeasuringInstrument
387	2018-01-10 13:16:47.761	3	5	updateMeasuringInstrument
388	2018-01-10 13:33:44.461	4	5	updateMeasuringInstrument
389	2018-01-10 13:35:31.886	4	5	updateMeasuringInstrument
390	2018-01-10 16:57:41.24	4	5	updateMeasuringInstrument
391	2018-01-10 17:28:02.781	4	5	updateMeasuringInstrument
392	2018-01-10 17:36:40.159	4	5	updateMeasuringInstrument
393	2018-01-10 17:49:56.058	3	5	updateMeasuringInstrument
394	2018-01-10 17:50:48.562	4	5	updateMeasuringInstrument
395	2018-01-10 17:51:11.766	4	5	updateMeasuringInstrument
396	2018-01-10 18:02:13.508	3	5	updateMeasuringInstrument
397	2018-01-10 18:13:50.031	4	5	updateMeasuringInstrument
398	2018-01-10 18:14:53.123	4	5	updateMeasuringInstrument
399	2018-01-10 18:16:23.42	4	5	updateMeasuringInstrument
400	2018-01-10 18:24:47.155	3	5	updateMeasuringInstrument
401	2018-01-10 19:23:31.39	3	5	updateMeasuringInstrument
402	2018-01-10 19:26:29.246	4	5	updateMeasuringInstrument
403	2018-01-10 19:33:28.762	3	5	updateMeasuringInstrument
404	2018-01-10 19:34:33.69	4	5	updateMeasuringInstrument
405	2018-01-10 19:39:24.803	4	5	updateMeasuringInstrument
406	2018-01-10 19:54:38.375	3	\N	\N
408	2018-01-10 20:09:22.5	4	5	updateMeasuringInstrument
409	2018-01-10 20:09:58.981	4	5	updateMeasuringInstrument
410	2018-01-10 20:10:43.157	3	5	updateMeasuringInstrument
413	2018-01-10 20:18:03.992	3	5	updateMeasuringInstrument
414	2018-01-10 20:18:41.376	4	5	updateMeasuringInstrument
415	2018-01-10 20:24:32.153	4	5	updateMeasuringInstrument
416	2018-01-10 20:31:10.236	3	5	updateMeasuringInstrument
417	2018-01-10 20:42:55.29	3	5	updateMeasuringInstrument
418	2018-01-10 20:55:03.999	3	5	updateMeasuringInstrument
419	2018-01-10 20:59:38.5	4	5	updateMeasuringInstrument
420	2018-01-10 21:09:00.054	4	5	updateMeasuringInstrument
421	2018-01-10 21:10:16.085	3	4	createMeasuringInstrument
422	2018-01-10 21:12:14.659	4	5	updateMeasuringInstrument
423	2018-01-10 21:27:04.593	3	7	removeMeasuringInstrument
424	2018-01-10 21:27:54.033	3	8	removeMeasuringInstrument
425	2018-01-10 21:30:07.213	3	5	updateMeasuringInstrument
426	2018-01-10 21:33:40.362	4	5	updateMeasuringInstrument
427	2018-01-10 21:35:49.554	4	5	updateMeasuringInstrument
428	2018-01-10 21:35:58.638	3	5	updateMeasuringInstrument
429	2018-01-11 11:53:20.032	4	5	updateMeasuringInstrument
430	2018-01-11 19:52:00.413	4	5	updateMeasuringInstrument
431	2018-01-12 14:36:07.167	4	5	updateMeasuringInstrument
432	2018-01-12 14:39:41.57	4	5	updateMeasuringInstrument
433	2018-01-12 14:56:30.19	4	5	updateMeasuringInstrument
434	2018-01-12 14:59:16.008	4	5	updateMeasuringInstrument
435	2018-01-12 16:26:22.692	4	5	updateMeasuringInstrument
436	2018-01-12 16:32:00.883	3	5	updateMeasuringInstrument
437	2018-01-12 16:35:20.659	3	5	updateMeasuringInstrument
438	2018-01-12 16:41:50.923	3	5	updateMeasuringInstrument
439	2018-01-12 16:42:51.361	3	5	updateMeasuringInstrument
440	2018-01-12 17:06:48.426	3	5	updateMeasuringInstrument
441	2018-01-12 17:08:05.278	3	5	updateMeasuringInstrument
442	2018-01-12 18:40:57.675	4	5	updateMeasuringInstrument
443	2018-01-12 18:44:43.037	4	5	updateMeasuringInstrument
444	2018-01-12 18:48:05.805	4	5	updateMeasuringInstrument
445	2018-01-12 18:50:28.518	4	5	updateMeasuringInstrument
446	2018-01-12 18:56:37.585	4	5	updateMeasuringInstrument
447	2018-01-12 18:57:08.068	4	5	updateMeasuringInstrument
448	2018-01-15 12:10:19.483	4	5	updateMeasuringInstrument
449	2018-01-15 12:13:00.428	4	5	updateMeasuringInstrument
450	2018-01-15 13:01:43.275	4	5	updateMeasuringInstrument
451	2018-01-15 13:09:00.717	4	5	updateMeasuringInstrument
452	2018-01-15 15:59:51.146	4	5	updateMeasuringInstrument
453	2018-01-15 16:45:09.783	4	5	updateMeasuringInstrument
454	2018-01-15 16:46:41.615	4	5	updateMeasuringInstrument
455	2018-01-16 18:31:39.738	4	5	updateMeasuringInstrument
456	2018-01-16 18:32:51.419	4	5	updateMeasuringInstrument
457	2018-01-16 18:44:50.156	4	5	updateMeasuringInstrument
458	2018-01-17 18:02:06.296	4	5	updateMeasuringInstrument
459	2018-01-17 18:03:19.453	4	5	updateMeasuringInstrument
460	2018-01-17 18:16:30.789	4	5	updateMeasuringInstrument
461	2018-01-17 18:18:24.5	4	5	updateMeasuringInstrument
462	2018-01-17 18:21:07.408	4	5	updateMeasuringInstrument
463	2018-01-19 09:22:36.739	4	5	updateMeasuringInstrument
464	2018-01-19 09:28:29.032	4	5	updateMeasuringInstrument
465	2018-01-19 09:59:18.384	4	5	updateMeasuringInstrument
466	2018-01-19 09:59:56.257	4	5	updateMeasuringInstrument
467	2018-01-19 10:01:18.746	4	5	updateMeasuringInstrument
468	2018-01-19 10:01:56.799	4	5	updateMeasuringInstrument
469	2018-01-19 10:03:20.935	4	5	updateMeasuringInstrument
470	2018-01-19 10:04:01.408	4	5	updateMeasuringInstrument
471	2018-01-19 10:04:13.313	4	5	updateMeasuringInstrument
472	2018-01-19 17:18:24.281	4	5	updateMeasuringInstrument
473	2018-01-19 17:27:31.701	4	5	updateMeasuringInstrument
474	2018-01-19 17:28:52.626	4	5	updateMeasuringInstrument
475	2018-01-19 17:30:40.13	4	5	updateMeasuringInstrument
476	2018-01-22 14:32:05.255	3	5	updateMeasuringInstrument
477	2018-01-22 16:06:47.306	3	5	updateMeasuringInstrument
478	2018-01-22 16:08:03.737	3	5	updateMeasuringInstrument
479	2018-01-22 16:09:07.023	3	5	updateMeasuringInstrument
480	2018-01-22 16:49:32.205	4	5	updateMeasuringInstrument
481	2018-01-22 17:37:56.496	3	5	updateMeasuringInstrument
482	2018-01-22 17:38:21.352	3	5	updateMeasuringInstrument
483	2018-01-22 17:38:38.147	3	5	updateMeasuringInstrument
484	2018-01-22 17:42:53.77	4	5	updateMeasuringInstrument
485	2018-01-22 17:44:28.073	4	5	updateMeasuringInstrument
486	2018-01-22 17:45:40.204	4	5	updateMeasuringInstrument
487	2018-01-22 17:46:50.438	4	5	updateMeasuringInstrument
488	2018-01-22 17:47:48.266	4	5	updateMeasuringInstrument
489	2018-01-22 17:48:41.84	4	5	updateMeasuringInstrument
490	2018-01-22 20:07:26.532	3	5	updateMeasuringInstrument
491	2018-01-22 20:08:30.521	3	5	updateMeasuringInstrument
492	2018-01-22 20:10:11.777	3	5	updateMeasuringInstrument
493	2018-01-22 20:19:07.616	3	5	updateMeasuringInstrument
494	2018-01-22 20:20:08.63	3	5	updateMeasuringInstrument
495	2018-01-22 20:21:16.059	3	5	updateMeasuringInstrument
496	2018-01-22 20:23:26.525	3	5	updateMeasuringInstrument
497	2018-01-22 20:24:13.833	3	5	updateMeasuringInstrument
498	2018-01-22 20:24:57.424	3	5	updateMeasuringInstrument
499	2018-01-23 11:26:37.074	4	7	removeMeasuringInstrument
500	2018-01-23 11:31:50.541	4	7	removeMeasuringInstrument
501	2018-01-23 11:42:58.631	4	8	removeMeasuringInstrument
502	2018-01-23 11:47:17.465	4	8	removeMeasuringInstrument
503	2018-01-23 11:47:17.494	4	8	removeMeasuringInstrument
504	2018-01-23 11:52:06.172	4	8	removeMeasuringInstrument
505	2018-01-23 11:56:51.793	4	8	removeMeasuringInstrument
506	2018-01-23 11:57:07.602	4	8	removeMeasuringInstrument
507	2018-01-23 11:57:27.868	4	8	removeMeasuringInstrument
508	2018-01-23 11:58:09.707	4	8	removeMeasuringInstrument
511	2018-01-23 12:07:39.021	4	4	createMeasuringInstrument
512	2018-01-23 12:20:20.289	4	5	updateMeasuringInstrument
513	2018-01-23 12:22:34.682	4	5	updateMeasuringInstrument
514	2018-01-23 12:22:34.719	4	5	updateMeasuringInstrument
515	2018-01-23 12:22:34.743	4	5	updateMeasuringInstrument
516	2018-01-23 12:22:34.76	4	5	updateMeasuringInstrument
517	2018-01-23 12:22:34.776	4	5	updateMeasuringInstrument
518	2018-01-23 12:22:34.805	4	5	updateMeasuringInstrument
519	2018-01-23 12:22:34.829	4	5	updateMeasuringInstrument
520	2018-01-23 12:22:34.847	4	5	updateMeasuringInstrument
521	2018-01-23 12:22:34.864	4	5	updateMeasuringInstrument
522	2018-01-23 12:22:34.886	4	5	updateMeasuringInstrument
523	2018-01-23 12:22:34.905	4	5	updateMeasuringInstrument
524	2018-01-23 12:22:34.924	4	5	updateMeasuringInstrument
525	2018-01-23 12:22:34.937	4	5	updateMeasuringInstrument
526	2018-01-23 12:22:34.953	4	5	updateMeasuringInstrument
527	2018-01-23 12:22:34.967	4	5	updateMeasuringInstrument
528	2018-01-23 12:22:34.981	4	5	updateMeasuringInstrument
529	2018-01-23 12:22:34.996	4	5	updateMeasuringInstrument
530	2018-01-23 12:22:35.016	4	5	updateMeasuringInstrument
531	2018-01-23 12:22:35.036	4	5	updateMeasuringInstrument
532	2018-01-23 12:22:35.055	4	5	updateMeasuringInstrument
534	2018-01-23 12:26:53.994	4	4	createMeasuringInstrument
535	2018-01-23 12:39:22.234	4	5	updateMeasuringInstrument
536	2018-01-23 12:41:41.631	4	5	updateMeasuringInstrument
537	2018-01-23 14:04:09.221	4	5	updateMeasuringInstrument
538	2018-01-23 14:04:43.834	4	5	updateMeasuringInstrument
539	2018-01-23 14:05:19.447	4	5	updateMeasuringInstrument
540	2018-01-23 14:05:40.954	4	5	updateMeasuringInstrument
541	2018-01-23 14:07:27.246	4	5	updateMeasuringInstrument
542	2018-01-23 14:07:46.951	4	5	updateMeasuringInstrument
543	2018-01-23 14:08:07.674	4	5	updateMeasuringInstrument
544	2018-01-23 14:08:35.59	4	5	updateMeasuringInstrument
545	2018-01-23 14:08:56.532	4	5	updateMeasuringInstrument
546	2018-01-23 14:09:17.726	4	5	updateMeasuringInstrument
547	2018-01-23 14:09:58.531	4	5	updateMeasuringInstrument
548	2018-01-23 14:10:59.133	4	5	updateMeasuringInstrument
549	2018-01-24 16:16:07.105	4	5	updateMeasuringInstrument
550	2018-01-24 16:17:12.162	4	5	updateMeasuringInstrument
551	2018-01-24 18:00:02.969	4	5	updateMeasuringInstrument
552	2018-01-24 18:09:21.948	4	5	updateMeasuringInstrument
553	2018-01-24 18:10:51.791	4	5	updateMeasuringInstrument
554	2018-01-24 18:12:54.971	4	5	updateMeasuringInstrument
555	2018-01-24 18:16:16.834	4	5	updateMeasuringInstrument
556	2018-01-24 18:50:49.53	4	5	updateMeasuringInstrument
557	2018-01-24 19:34:38.863	4	5	updateMeasuringInstrument
558	2018-01-24 19:36:14.652	4	5	updateMeasuringInstrument
559	2018-01-24 19:37:23.211	4	5	updateMeasuringInstrument
560	2018-01-30 17:07:27.412	4	5	updateMeasuringInstrument
561	2018-01-31 17:05:21.306	4	5	updateMeasuringInstrument
562	2018-01-31 17:05:57.08	4	5	updateMeasuringInstrument
563	2018-01-31 17:10:03.863	4	5	updateMeasuringInstrument
564	2018-01-31 17:10:32.517	4	5	updateMeasuringInstrument
565	2018-01-31 18:47:26.222	4	5	updateMeasuringInstrument
566	2018-01-31 19:46:36.547	4	5	updateMeasuringInstrument
567	2018-01-31 19:48:53.726	4	5	updateMeasuringInstrument
568	2018-01-31 19:50:05.352	4	5	updateMeasuringInstrument
569	2018-02-01 12:35:26.297	4	5	updateMeasuringInstrument
570	2018-02-01 12:35:55.707	4	5	updateMeasuringInstrument
571	2018-02-01 14:05:16.766	4	5	updateMeasuringInstrument
572	2018-02-01 14:08:07.471	4	5	updateMeasuringInstrument
573	2018-02-01 14:09:44.553	4	5	updateMeasuringInstrument
574	2018-02-01 18:38:37.186	4	5	updateMeasuringInstrument
575	2018-02-02 14:11:46.224	4	5	updateMeasuringInstrument
576	2018-02-02 14:13:11.068	4	5	updateMeasuringInstrument
577	2018-02-02 14:45:40.484	4	5	updateMeasuringInstrument
578	2018-02-02 14:48:15.317	4	5	updateMeasuringInstrument
579	2018-02-02 16:22:49.666	4	5	updateMeasuringInstrument
580	2018-02-02 16:23:32.571	4	5	updateMeasuringInstrument
581	2018-02-02 16:26:44.385	4	5	updateMeasuringInstrument
582	2018-02-02 16:30:41.083	4	5	updateMeasuringInstrument
583	2018-02-02 16:31:41.217	4	5	updateMeasuringInstrument
584	2018-02-02 16:32:31.877	4	5	updateMeasuringInstrument
585	2018-02-02 16:33:17.412	4	5	updateMeasuringInstrument
586	2018-02-02 16:33:57.347	4	5	updateMeasuringInstrument
587	2018-02-02 16:37:24.443	4	5	updateMeasuringInstrument
588	2018-02-02 16:37:38.282	4	5	updateMeasuringInstrument
589	2018-02-02 16:37:56.037	4	5	updateMeasuringInstrument
590	2018-02-02 16:38:13.443	4	5	updateMeasuringInstrument
591	2018-02-02 16:38:29.294	4	5	updateMeasuringInstrument
592	2018-02-02 16:42:20.751	4	5	updateMeasuringInstrument
593	2018-02-02 16:42:42.261	4	5	updateMeasuringInstrument
594	2018-02-02 16:52:15.677	4	5	updateMeasuringInstrument
595	2018-02-02 17:00:39.766	4	5	updateMeasuringInstrument
596	2018-02-02 17:09:28.346	4	5	updateMeasuringInstrument
597	2018-02-02 17:18:33.748	4	5	updateMeasuringInstrument
598	2018-02-02 19:21:17.818	4	5	updateMeasuringInstrument
599	2018-02-02 19:30:27.937	4	5	updateMeasuringInstrument
600	2018-02-02 19:34:30.203	4	5	updateMeasuringInstrument
601	2018-02-02 19:38:33.498	4	5	updateMeasuringInstrument
602	2018-02-02 19:40:42.571	4	5	updateMeasuringInstrument
603	2018-02-02 20:50:12.751	3	5	updateMeasuringInstrument
604	2018-02-02 20:50:57.122	4	5	updateMeasuringInstrument
605	2018-02-02 21:17:57.286	3	5	updateMeasuringInstrument
606	2018-02-05 11:57:48.822	4	5	updateMeasuringInstrument
607	2018-02-05 11:58:29.811	4	5	updateMeasuringInstrument
608	2018-02-05 12:04:28.499	4	5	updateMeasuringInstrument
609	2018-02-05 13:51:44.041	4	5	updateMeasuringInstrument
610	2018-02-05 13:52:35.674	4	5	updateMeasuringInstrument
611	2018-02-05 13:53:07.179	4	5	updateMeasuringInstrument
612	2018-02-05 13:53:26.423	4	5	updateMeasuringInstrument
613	2018-02-05 18:26:45.909	4	5	updateMeasuringInstrument
614	2018-02-05 18:27:12.022	4	5	updateMeasuringInstrument
615	2018-02-05 18:28:30.163	4	5	updateMeasuringInstrument
616	2018-02-06 19:20:35.361	4	5	updateMeasuringInstrument
617	2018-02-07 11:16:16.219	4	5	updateMeasuringInstrument
618	2018-02-07 11:16:52.35	4	5	updateMeasuringInstrument
619	2018-02-07 12:24:28.618	4	5	updateMeasuringInstrument
620	2018-02-07 12:27:51.374	4	4	createMeasuringInstrument
621	2018-02-07 12:29:10.01	4	4	createMeasuringInstrument
622	2018-02-07 13:05:39.389	4	4	createMeasuringInstrument
623	2018-02-07 13:06:34.867	4	5	updateMeasuringInstrument
624	2018-02-07 13:07:53.433	4	5	updateMeasuringInstrument
625	2018-02-07 13:25:21.592	4	5	updateMeasuringInstrument
626	2018-02-07 13:25:42.753	4	5	updateMeasuringInstrument
627	2018-02-07 13:26:04.56	4	5	updateMeasuringInstrument
628	2018-02-07 13:26:28.682	4	5	updateMeasuringInstrument
629	2018-02-07 13:26:54.274	4	5	updateMeasuringInstrument
630	2018-02-07 13:27:13.903	4	5	updateMeasuringInstrument
631	2018-02-07 13:56:40.339	4	5	updateMeasuringInstrument
632	2018-02-07 13:58:29.376	4	5	updateMeasuringInstrument
633	2018-02-07 14:37:59.609	4	5	updateMeasuringInstrument
634	2018-02-07 14:38:40.979	4	5	updateMeasuringInstrument
635	2018-02-07 14:39:40.369	4	5	updateMeasuringInstrument
636	2018-02-07 14:40:00.303	4	5	updateMeasuringInstrument
637	2018-02-07 14:40:14.54	4	5	updateMeasuringInstrument
638	2018-02-07 15:49:52.779	4	5	updateMeasuringInstrument
639	2018-02-07 16:02:33.94	4	5	updateMeasuringInstrument
640	2018-02-07 16:12:56.758	4	5	updateMeasuringInstrument
641	2018-02-07 16:19:25.813	4	5	updateMeasuringInstrument
642	2018-02-07 17:37:47.322	4	5	updateMeasuringInstrument
643	2018-02-14 19:29:25.595	4	7	removeMeasuringInstrument
644	2018-02-14 19:31:00.79	4	8	removeMeasuringInstrument
645	2018-02-14 19:31:00.823	4	8	removeMeasuringInstrument
646	2018-02-14 19:31:00.844	4	8	removeMeasuringInstrument
647	2018-02-14 19:31:00.934	4	8	removeMeasuringInstrument
648	2018-02-14 19:32:56.78	4	8	removeMeasuringInstrument
649	2018-02-14 19:38:13.505	4	4	createMeasuringInstrument
650	2018-02-14 19:40:23.12	4	5	updateMeasuringInstrument
651	2018-02-14 19:42:57.129	4	4	createMeasuringInstrument
652	2018-02-14 19:44:16.515	4	4	createMeasuringInstrument
653	2018-02-14 20:16:07.58	4	5	updateMeasuringInstrument
654	2018-02-14 20:17:14.507	4	5	updateMeasuringInstrument
655	2018-02-14 20:18:56.689	4	5	updateMeasuringInstrument
656	2018-02-14 20:19:38.541	4	5	updateMeasuringInstrument
658	2018-02-15 16:34:20.656	4	\N	\N
659	2018-02-15 18:10:21.158	4	5	updateMeasuringInstrument
660	2018-02-15 18:14:58.486	4	5	updateMeasuringInstrument
661	2018-02-15 18:32:14.86	4	5	updateMeasuringInstrument
662	2018-02-15 18:33:04.179	4	7	removeMeasuringInstrument
663	2018-02-15 18:35:25.629	4	8	removeMeasuringInstrument
664	2018-02-15 18:35:58.194	4	8	removeMeasuringInstrument
665	2018-02-15 18:38:48.299	4	5	updateMeasuringInstrument
666	2018-02-15 18:43:35.352	4	5	updateMeasuringInstrument
667	2018-02-15 18:49:29.608	4	5	updateMeasuringInstrument
668	2018-02-15 18:55:57.971	4	5	updateMeasuringInstrument
669	2018-02-16 10:43:34.725	4	\N	\N
670	2018-02-16 10:57:03.742	4	5	updateMeasuringInstrument
671	2018-02-16 11:03:19.112	4	8	removeMeasuringInstrument
673	2018-02-16 11:03:19.204	4	8	removeMeasuringInstrument
689	2018-02-16 11:04:53.84	4	8	removeMeasuringInstrument
672	2018-02-16 11:03:19.157	4	8	removeMeasuringInstrument
674	2018-02-16 11:03:19.234	4	8	removeMeasuringInstrument
675	2018-02-16 11:03:19.309	4	8	removeMeasuringInstrument
676	2018-02-16 11:03:19.334	4	8	removeMeasuringInstrument
677	2018-02-16 11:03:19.351	4	8	removeMeasuringInstrument
678	2018-02-16 11:03:19.374	4	8	removeMeasuringInstrument
679	2018-02-16 11:03:19.404	4	8	removeMeasuringInstrument
680	2018-02-16 11:03:19.435	4	8	removeMeasuringInstrument
681	2018-02-16 11:03:19.478	4	8	removeMeasuringInstrument
682	2018-02-16 11:03:19.502	4	8	removeMeasuringInstrument
683	2018-02-16 11:03:19.518	4	8	removeMeasuringInstrument
684	2018-02-16 11:03:19.536	4	8	removeMeasuringInstrument
685	2018-02-16 11:03:19.559	4	8	removeMeasuringInstrument
686	2018-02-16 11:03:19.582	4	8	removeMeasuringInstrument
687	2018-02-16 11:03:19.597	4	8	removeMeasuringInstrument
688	2018-02-16 11:04:53.785	4	8	removeMeasuringInstrument
690	2018-02-16 11:04:53.874	4	8	removeMeasuringInstrument
691	2018-02-16 11:04:53.899	4	8	removeMeasuringInstrument
692	2018-02-16 11:04:53.921	4	8	removeMeasuringInstrument
693	2018-02-16 11:04:53.951	4	8	removeMeasuringInstrument
694	2018-02-16 11:04:53.964	4	8	removeMeasuringInstrument
695	2018-02-16 11:04:53.979	4	8	removeMeasuringInstrument
696	2018-02-16 11:04:53.997	4	8	removeMeasuringInstrument
697	2018-02-16 11:04:54.012	4	8	removeMeasuringInstrument
698	2018-02-16 11:04:54.025	4	8	removeMeasuringInstrument
699	2018-02-16 11:10:24.245	4	5	updateMeasuringInstrument
700	2018-02-16 11:13:16.217	4	5	updateMeasuringInstrument
701	2018-02-16 11:13:58.78	4	5	updateMeasuringInstrument
702	2018-02-16 11:15:21.764	4	5	updateMeasuringInstrument
703	2018-02-16 11:16:51.776	4	5	updateMeasuringInstrument
704	2018-02-16 11:24:35.687	4	5	updateMeasuringInstrument
705	2018-02-16 11:25:45.428	4	5	updateMeasuringInstrument
706	2018-02-16 17:48:12.735	4	5	updateMeasuringInstrument
707	2018-02-16 18:46:14.564	4	5	updateMeasuringInstrument
708	2018-02-16 18:52:26.015	4	5	updateMeasuringInstrument
709	2018-02-16 19:03:24.252	4	4	createMeasuringInstrument
710	2018-02-16 19:03:58.254	4	5	updateMeasuringInstrument
711	2018-02-16 19:11:48.058	4	5	updateMeasuringInstrument
712	2018-02-16 19:24:58.213	4	5	updateMeasuringInstrument
713	2018-02-19 11:25:03.435	4	5	updateMeasuringInstrument
714	2018-02-19 11:26:11.039	4	5	updateMeasuringInstrument
715	2018-02-19 11:28:04.013	4	5	updateMeasuringInstrument
716	2018-02-19 11:57:05.474	4	5	updateMeasuringInstrument
717	2018-02-19 12:03:03.908	4	5	updateMeasuringInstrument
718	2018-02-20 09:13:22.93	4	5	updateMeasuringInstrument
719	2018-02-20 09:32:01.731	4	5	updateMeasuringInstrument
720	2018-02-20 09:33:30.502	4	5	updateMeasuringInstrument
721	2018-02-20 09:40:13.593	4	5	updateMeasuringInstrument
722	2018-02-20 12:14:24.438	4	5	updateMeasuringInstrument
723	2018-02-20 12:15:36.483	4	5	updateMeasuringInstrument
724	2018-02-20 12:18:31.958	4	5	updateMeasuringInstrument
725	2018-02-20 12:19:23.814	4	5	updateMeasuringInstrument
726	2018-02-20 12:19:51.563	4	5	updateMeasuringInstrument
727	2018-02-20 12:30:44.182	4	5	updateMeasuringInstrument
728	2018-02-20 12:31:44.542	4	5	updateMeasuringInstrument
729	2018-02-20 12:35:27.79	4	5	updateMeasuringInstrument
730	2018-02-20 12:51:25.918	4	5	updateMeasuringInstrument
731	2018-02-20 12:52:40.745	4	5	updateMeasuringInstrument
732	2018-02-20 12:59:27.876	4	5	updateMeasuringInstrument
733	2018-02-20 13:04:36.992	4	5	updateMeasuringInstrument
734	2018-02-20 13:06:18.233	4	5	updateMeasuringInstrument
735	2018-02-20 14:02:12.814	4	5	updateMeasuringInstrument
736	2018-02-20 14:03:29.524	4	5	updateMeasuringInstrument
737	2018-02-20 14:03:57.251	4	5	updateMeasuringInstrument
738	2018-02-20 15:14:11.202	4	5	updateMeasuringInstrument
739	2018-02-20 15:14:45.108	4	5	updateMeasuringInstrument
740	2018-02-20 15:15:10.91	4	5	updateMeasuringInstrument
741	2018-02-20 15:17:12.063	4	5	updateMeasuringInstrument
742	2018-02-20 16:29:52.857	4	5	updateMeasuringInstrument
743	2018-02-20 16:30:30.001	4	5	updateMeasuringInstrument
744	2018-02-20 17:13:19.467	4	5	updateMeasuringInstrument
745	2018-02-20 17:13:44.019	4	5	updateMeasuringInstrument
746	2018-02-20 17:14:51.339	4	5	updateMeasuringInstrument
747	2018-02-21 08:50:53.047	4	5	updateMeasuringInstrument
748	2018-02-21 08:52:22.77	4	5	updateMeasuringInstrument
749	2018-02-21 08:53:31.356	4	5	updateMeasuringInstrument
750	2018-02-21 08:54:13.411	4	5	updateMeasuringInstrument
751	2018-02-21 08:55:42.417	4	5	updateMeasuringInstrument
752	2018-02-21 08:58:09.007	4	5	updateMeasuringInstrument
753	2018-02-21 08:59:43.363	4	5	updateMeasuringInstrument
754	2018-02-21 09:00:33.84	4	5	updateMeasuringInstrument
755	2018-02-21 09:01:16.129	4	5	updateMeasuringInstrument
756	2018-02-21 09:02:10.035	4	5	updateMeasuringInstrument
757	2018-02-21 09:12:32.799	4	5	updateMeasuringInstrument
758	2018-02-21 09:14:19.221	4	5	updateMeasuringInstrument
759	2018-02-21 09:15:03.029	4	5	updateMeasuringInstrument
760	2018-02-21 09:18:13.621	4	5	updateMeasuringInstrument
761	2018-02-21 10:18:16.057	4	5	updateMeasuringInstrument
762	2018-02-21 10:27:42.536	4	5	updateMeasuringInstrument
763	2018-02-21 10:28:17.696	4	5	updateMeasuringInstrument
764	2018-02-21 10:28:38.34	4	5	updateMeasuringInstrument
765	2018-02-21 10:39:13.483	4	5	updateMeasuringInstrument
766	2018-02-21 10:39:29.324	4	5	updateMeasuringInstrument
767	2018-02-22 08:30:59.93	4	5	updateMeasuringInstrument
768	2018-02-22 08:37:23.44	4	8	removeMeasuringInstrument
769	2018-02-22 08:40:42.884	4	8	removeMeasuringInstrument
770	2018-02-22 08:41:29.607	4	5	updateMeasuringInstrument
771	2018-02-22 08:44:54.093	4	5	updateMeasuringInstrument
772	2018-02-22 08:47:10.813	4	5	updateMeasuringInstrument
773	2018-02-22 08:50:49.902	4	5	updateMeasuringInstrument
774	2018-02-22 08:51:06.932	4	5	updateMeasuringInstrument
775	2018-02-22 08:52:26.396	4	5	updateMeasuringInstrument
777	2018-02-22 09:03:58.576	4	5	updateMeasuringInstrument
778	2018-02-22 09:12:19.305	4	5	updateMeasuringInstrument
779	2018-02-22 09:17:41.04	4	5	updateMeasuringInstrument
780	2018-02-22 09:18:01.69	4	5	updateMeasuringInstrument
781	2018-02-22 09:38:59.885	4	5	updateMeasuringInstrument
785	2018-02-22 13:27:39.632	4	5	updateMeasuringInstrument
786	2018-02-22 13:27:59.933	4	5	updateMeasuringInstrument
788	2018-02-22 13:34:59.993	4	5	updateMeasuringInstrument
789	2018-02-22 13:57:42.565	4	5	updateMeasuringInstrument
791	2018-02-22 14:15:24.748	4	5	updateMeasuringInstrument
792	2018-02-22 14:16:03.04	4	5	updateMeasuringInstrument
797	2018-02-22 14:57:08.569	4	5	updateMeasuringInstrument
798	2018-02-22 14:57:42.886	4	5	updateMeasuringInstrument
776	2018-02-22 09:02:20.634	4	5	updateMeasuringInstrument
796	2018-02-22 14:56:40.927	4	5	updateMeasuringInstrument
782	2018-02-22 09:39:50.043	4	5	updateMeasuringInstrument
783	2018-02-22 09:53:22.297	4	5	updateMeasuringInstrument
784	2018-02-22 10:02:17.917	4	5	updateMeasuringInstrument
787	2018-02-22 13:34:52.376	4	5	updateMeasuringInstrument
790	2018-02-22 14:13:39.913	4	5	updateMeasuringInstrument
793	2018-02-22 14:17:02.756	4	5	updateMeasuringInstrument
794	2018-02-22 14:20:09.54	4	5	updateMeasuringInstrument
795	2018-02-22 14:21:43.844	4	5	updateMeasuringInstrument
799	2018-02-22 14:58:03.971	4	5	updateMeasuringInstrument
800	2018-02-22 14:59:14.583	4	5	updateMeasuringInstrument
801	2018-02-23 08:55:17.734	4	5	updateMeasuringInstrument
802	2018-02-23 09:33:08.408	4	5	updateMeasuringInstrument
803	2018-02-23 09:41:35.005	4	5	updateMeasuringInstrument
804	2018-02-23 10:16:17.226	4	5	updateMeasuringInstrument
805	2018-02-23 10:18:34.712	4	5	updateMeasuringInstrument
806	2018-02-23 10:22:22.961	4	5	updateMeasuringInstrument
807	2018-02-26 10:56:20.577	4	5	updateMeasuringInstrument
808	2018-02-26 10:58:15.277	4	5	updateMeasuringInstrument
809	2018-02-26 13:20:39.693	4	5	updateMeasuringInstrument
810	2018-02-26 13:21:37.328	4	5	updateMeasuringInstrument
811	2018-02-26 13:21:51.617	4	5	updateMeasuringInstrument
812	2018-02-26 13:22:05.16	4	5	updateMeasuringInstrument
813	2018-02-26 13:22:17.355	4	5	updateMeasuringInstrument
814	2018-02-26 13:57:26.858	4	5	updateMeasuringInstrument
815	2018-02-26 15:11:47.023	4	5	updateMeasuringInstrument
816	2018-02-26 15:16:18.435	4	5	updateMeasuringInstrument
817	2018-02-26 15:20:49.322	4	5	updateMeasuringInstrument
818	2018-02-26 15:22:28.353	4	5	updateMeasuringInstrument
819	2018-02-27 09:57:12.076	4	5	updateMeasuringInstrument
820	2018-02-27 09:57:49.277	4	5	updateMeasuringInstrument
821	2018-02-27 10:11:01.903	4	5	updateMeasuringInstrument
822	2018-02-27 10:11:38.957	4	5	updateMeasuringInstrument
823	2018-02-27 10:11:58.07	4	5	updateMeasuringInstrument
824	2018-02-27 10:14:23.578	4	5	updateMeasuringInstrument
825	2018-02-27 10:14:46.793	4	5	updateMeasuringInstrument
826	2018-02-27 11:07:36.169	4	5	updateMeasuringInstrument
827	2018-02-27 13:35:03.501	4	5	updateMeasuringInstrument
828	2018-02-27 13:45:01.623	4	5	updateMeasuringInstrument
829	2018-02-27 14:29:03.673	4	\N	\N
830	2018-02-27 14:29:52.47	4	5	updateMeasuringInstrument
831	2018-02-27 15:09:19.719	4	5	updateMeasuringInstrument
832	2018-02-27 15:09:42.88	4	5	updateMeasuringInstrument
833	2018-02-27 15:10:05.485	4	5	updateMeasuringInstrument
834	2018-02-27 17:12:54.512	4	5	updateMeasuringInstrument
835	2018-02-27 17:15:06.795	4	5	updateMeasuringInstrument
836	2018-02-27 17:19:58.686	4	5	updateMeasuringInstrument
837	2018-02-27 17:20:13.059	4	5	updateMeasuringInstrument
838	2018-02-28 08:19:54.092	4	5	updateMeasuringInstrument
839	2018-02-28 08:20:15.711	4	5	updateMeasuringInstrument
840	2018-02-28 17:17:42.441	4	5	updateMeasuringInstrument
841	2018-02-28 17:18:36.989	4	5	updateMeasuringInstrument
842	2018-02-28 17:20:32.677	4	5	updateMeasuringInstrument
843	2018-02-28 17:21:00.547	4	5	updateMeasuringInstrument
844	2018-02-28 17:33:29.805	6	5	updateMeasuringInstrument
845	2018-02-28 17:34:21.52	6	5	updateMeasuringInstrument
846	2018-02-28 17:37:13.119	6	5	updateMeasuringInstrument
847	2018-02-28 17:37:46.707	6	5	updateMeasuringInstrument
848	2018-02-28 17:46:49.353	6	5	updateMeasuringInstrument
849	2018-02-28 17:47:24.669	6	5	updateMeasuringInstrument
850	2018-02-28 17:52:04.232	6	5	updateMeasuringInstrument
851	2018-02-28 17:58:27.468	4	5	updateMeasuringInstrument
852	2018-03-01 08:48:31.168	4	5	updateMeasuringInstrument
853	2018-03-01 08:50:50.206	4	5	updateMeasuringInstrument
854	2018-03-01 08:51:52.072	4	5	updateMeasuringInstrument
855	2018-03-01 08:52:56.661	4	5	updateMeasuringInstrument
856	2018-03-01 08:53:47.734	4	5	updateMeasuringInstrument
857	2018-03-01 09:23:37.959	4	5	updateMeasuringInstrument
858	2018-03-01 09:43:39.313	4	5	updateMeasuringInstrument
859	2018-03-01 09:51:30.881	4	5	updateMeasuringInstrument
860	2018-03-01 10:13:33.149	4	5	updateMeasuringInstrument
861	2018-03-01 10:31:59.547	4	5	updateMeasuringInstrument
862	2018-03-01 10:43:20.811	4	5	updateMeasuringInstrument
863	2018-03-01 10:45:57.317	4	5	updateMeasuringInstrument
864	2018-03-01 11:24:29.394	4	5	updateMeasuringInstrument
865	2018-03-02 14:21:47.06	4	5	updateMeasuringInstrument
866	2018-03-02 14:22:17.037	4	5	updateMeasuringInstrument
867	2018-03-02 14:22:48.091	4	5	updateMeasuringInstrument
868	2018-03-02 14:23:10.131	4	5	updateMeasuringInstrument
869	2018-03-02 17:57:41.065	4	5	updateMeasuringInstrument
870	2018-03-02 19:31:11.001	4	5	updateMeasuringInstrument
871	2018-03-02 19:31:32.875	4	5	updateMeasuringInstrument
872	2018-03-05 10:59:45.034	6	5	updateMeasuringInstrument
873	2018-03-05 11:00:04.047	6	5	updateMeasuringInstrument
874	2018-03-05 11:01:33.861	6	5	updateMeasuringInstrument
875	2018-03-05 11:02:51.088	6	5	updateMeasuringInstrument
876	2018-03-05 11:03:36.646	6	5	updateMeasuringInstrument
877	2018-03-05 11:04:04.078	6	5	updateMeasuringInstrument
878	2018-03-05 11:08:20.559	6	5	updateMeasuringInstrument
879	2018-03-05 12:16:29.285	4	\N	\N
880	2018-03-05 12:17:24.507	4	5	updateMeasuringInstrument
881	2018-03-05 17:48:44.661	4	5	updateMeasuringInstrument
882	2018-03-05 19:15:26.793	6	5	updateMeasuringInstrument
883	2018-03-06 13:15:22.779	4	5	updateMeasuringInstrument
884	2018-03-06 13:15:38.835	4	5	updateMeasuringInstrument
885	2018-03-07 10:45:20.007	4	5	updateMeasuringInstrument
886	2018-03-07 11:10:32.865	4	4	createMeasuringInstrument
887	2018-03-07 11:11:02.134	4	5	updateMeasuringInstrument
888	2018-03-07 11:11:15.668	4	5	updateMeasuringInstrument
889	2018-03-07 11:13:13.727	4	5	updateMeasuringInstrument
891	2018-03-07 11:13:51.93	4	5	updateMeasuringInstrument
901	2018-03-07 14:24:57.525	4	5	updateMeasuringInstrument
902	2018-03-07 15:48:06.862	4	5	updateMeasuringInstrument
908	2018-03-07 18:13:25.974	4	7	removeMeasuringInstrument
915	2018-03-07 18:33:02.09	4	5	updateMeasuringInstrument
916	2018-03-07 19:35:45.352	4	5	updateMeasuringInstrument
918	2018-03-07 20:25:47.589	4	8	removeMeasuringInstrument
920	2018-03-07 20:26:56.138	4	8	removeMeasuringInstrument
922	2018-03-07 20:26:56.171	4	8	removeMeasuringInstrument
924	2018-03-07 20:38:38.577	4	8	removeMeasuringInstrument
929	2018-03-07 20:38:39.219	4	8	removeMeasuringInstrument
930	2018-03-07 20:38:39.299	4	8	removeMeasuringInstrument
931	2018-03-07 20:38:39.313	4	8	removeMeasuringInstrument
932	2018-03-07 20:38:39.326	4	8	removeMeasuringInstrument
934	2018-03-07 20:38:39.351	4	8	removeMeasuringInstrument
935	2018-03-07 20:38:39.364	4	8	removeMeasuringInstrument
890	2018-03-07 11:13:28.547	4	5	updateMeasuringInstrument
892	2018-03-07 11:14:14.991	4	5	updateMeasuringInstrument
893	2018-03-07 11:14:27.509	4	5	updateMeasuringInstrument
894	2018-03-07 13:34:39.08	4	5	updateMeasuringInstrument
895	2018-03-07 13:35:08.808	4	5	updateMeasuringInstrument
896	2018-03-07 13:35:28.003	4	5	updateMeasuringInstrument
897	2018-03-07 13:35:58.169	4	5	updateMeasuringInstrument
898	2018-03-07 14:22:57.759	4	4	createMeasuringInstrument
899	2018-03-07 14:23:14.776	4	5	updateMeasuringInstrument
900	2018-03-07 14:24:45.553	4	4	createMeasuringInstrument
903	2018-03-07 15:48:18.98	4	5	updateMeasuringInstrument
904	2018-03-07 15:48:43.088	4	5	updateMeasuringInstrument
905	2018-03-07 17:12:49.761	4	5	updateMeasuringInstrument
906	2018-03-07 17:46:36.516	4	5	updateMeasuringInstrument
907	2018-03-07 17:47:23.998	4	5	updateMeasuringInstrument
909	2018-03-07 18:30:49.688	4	8	removeMeasuringInstrument
910	2018-03-07 18:31:23.578	4	5	updateMeasuringInstrument
911	2018-03-07 18:31:38.022	4	5	updateMeasuringInstrument
912	2018-03-07 18:31:49.503	4	5	updateMeasuringInstrument
913	2018-03-07 18:32:04.049	4	5	updateMeasuringInstrument
914	2018-03-07 18:32:20.179	4	5	updateMeasuringInstrument
917	2018-03-07 20:17:52.375	4	7	removeMeasuringInstrument
919	2018-03-07 20:26:56.11	4	8	removeMeasuringInstrument
921	2018-03-07 20:26:56.152	4	8	removeMeasuringInstrument
923	2018-03-07 20:26:56.188	4	8	removeMeasuringInstrument
925	2018-03-07 20:38:38.778	4	8	removeMeasuringInstrument
926	2018-03-07 20:38:38.815	4	8	removeMeasuringInstrument
927	2018-03-07 20:38:38.83	4	8	removeMeasuringInstrument
928	2018-03-07 20:38:39.168	4	8	removeMeasuringInstrument
933	2018-03-07 20:38:39.339	4	8	removeMeasuringInstrument
936	2018-03-07 20:38:39.375	4	8	removeMeasuringInstrument
937	2018-03-07 20:38:39.391	4	8	removeMeasuringInstrument
938	2018-03-07 20:38:39.403	4	8	removeMeasuringInstrument
939	2018-03-07 20:39:20.863	4	8	removeMeasuringInstrument
940	2018-03-07 20:40:35.426	4	5	updateMeasuringInstrument
941	2018-03-07 20:41:42.382	4	5	updateMeasuringInstrument
942	2018-03-07 20:56:34.815	3	5	updateMeasuringInstrument
943	2018-03-07 21:13:35.63	3	5	updateMeasuringInstrument
944	2018-03-07 21:49:44.23	3	5	updateMeasuringInstrument
945	2018-03-07 21:50:19.645	4	5	updateMeasuringInstrument
946	2018-03-07 21:51:55.293	4	5	updateMeasuringInstrument
947	2018-03-07 21:52:21.599	4	5	updateMeasuringInstrument
948	2018-03-07 21:52:49.068	4	5	updateMeasuringInstrument
949	2018-03-07 21:56:49.62	4	5	updateMeasuringInstrument
950	2018-03-07 21:57:31.756	4	5	updateMeasuringInstrument
951	2018-03-07 21:59:38.047	4	5	updateMeasuringInstrument
952	2018-03-07 22:07:46.44	4	5	updateMeasuringInstrument
953	2018-03-07 22:09:51.346	4	5	updateMeasuringInstrument
954	2018-03-08 09:05:43.036	4	5	updateMeasuringInstrument
955	2018-03-08 10:04:02.9	4	\N	\N
956	2018-03-08 10:04:02.924	4	\N	\N
957	2018-03-08 10:04:02.957	4	\N	\N
958	2018-03-08 10:04:02.999	4	\N	\N
959	2018-03-08 10:04:03.037	4	\N	\N
960	2018-03-08 10:04:03.084	4	\N	\N
961	2018-03-08 10:04:03.121	4	\N	\N
962	2018-03-08 10:04:03.138	4	\N	\N
963	2018-03-08 10:04:03.156	4	\N	\N
964	2018-03-08 10:04:03.174	4	\N	\N
965	2018-03-08 10:06:46.243	4	5	updateMeasuringInstrument
966	2018-03-08 10:13:05.834	4	5	updateMeasuringInstrument
967	2018-03-08 10:13:47.142	4	5	updateMeasuringInstrument
968	2018-03-08 10:28:32.233	4	5	updateMeasuringInstrument
969	2018-03-08 11:09:16.516	4	5	updateMeasuringInstrument
970	2018-03-08 11:21:07.848	4	5	updateMeasuringInstrument
971	2018-03-08 11:26:54.34	4	5	updateMeasuringInstrument
972	2018-03-08 11:33:01.704	4	5	updateMeasuringInstrument
973	2018-03-08 11:57:26.121	4	5	updateMeasuringInstrument
974	2018-03-08 12:00:38.471	4	5	updateMeasuringInstrument
975	2018-03-08 12:04:00.541	4	5	updateMeasuringInstrument
976	2018-03-08 12:07:23.364	4	7	removeMeasuringInstrument
977	2018-03-08 12:08:21.511	4	5	updateMeasuringInstrument
978	2018-03-08 12:10:34.29	4	8	removeMeasuringInstrument
979	2018-03-08 12:11:21.265	4	5	updateMeasuringInstrument
980	2018-03-08 12:16:30.888	4	5	updateMeasuringInstrument
981	2018-03-08 12:53:26.131	4	5	updateMeasuringInstrument
982	2018-03-08 12:57:56.801	4	5	updateMeasuringInstrument
983	2018-03-08 13:01:36.161	4	5	updateMeasuringInstrument
984	2018-03-08 13:04:37.244	4	5	updateMeasuringInstrument
985	2018-03-08 13:07:02.242	4	5	updateMeasuringInstrument
986	2018-03-08 13:08:04.697	4	5	updateMeasuringInstrument
987	2018-03-08 13:15:33.601	4	5	updateMeasuringInstrument
988	2018-03-08 13:17:02.87	4	5	updateMeasuringInstrument
989	2018-03-08 13:18:00.81	4	5	updateMeasuringInstrument
990	2018-03-08 13:18:47.887	4	5	updateMeasuringInstrument
991	2018-03-08 13:24:57.203	4	5	updateMeasuringInstrument
992	2018-03-08 13:25:13.971	4	5	updateMeasuringInstrument
993	2018-03-08 13:25:48.986	4	5	updateMeasuringInstrument
994	2018-03-08 13:26:57.114	4	5	updateMeasuringInstrument
995	2018-03-08 13:27:37.951	4	5	updateMeasuringInstrument
996	2018-03-08 13:27:56.636	4	5	updateMeasuringInstrument
997	2018-03-08 13:28:09.047	4	5	updateMeasuringInstrument
998	2018-03-08 13:28:26.035	4	5	updateMeasuringInstrument
999	2018-03-08 13:28:42.676	4	5	updateMeasuringInstrument
1000	2018-03-08 15:24:04.909	4	5	updateMeasuringInstrument
1001	2018-03-08 15:26:21.285	4	5	updateMeasuringInstrument
1002	2018-03-08 15:27:24.488	4	5	updateMeasuringInstrument
1003	2018-03-08 15:28:44.499	4	5	updateMeasuringInstrument
1004	2018-03-08 15:29:08.028	4	5	updateMeasuringInstrument
1005	2018-03-08 15:29:53.069	4	5	updateMeasuringInstrument
1008	2018-03-09 14:14:39.838	4	5	updateMeasuringInstrument
1009	2018-03-09 15:21:39.015	4	5	updateMeasuringInstrument
1010	2018-03-09 15:21:58.17	4	5	updateMeasuringInstrument
1011	2018-03-09 15:22:15.712	4	5	updateMeasuringInstrument
1012	2018-03-09 15:22:33.543	4	5	updateMeasuringInstrument
1013	2018-03-09 15:22:46.932	4	5	updateMeasuringInstrument
1014	2018-03-09 15:23:50.416	4	5	updateMeasuringInstrument
1015	2018-03-09 15:23:58.766	4	5	updateMeasuringInstrument
1016	2018-03-09 15:24:09.162	4	5	updateMeasuringInstrument
1017	2018-03-09 15:24:21.398	4	5	updateMeasuringInstrument
1018	2018-03-09 15:24:42.792	4	5	updateMeasuringInstrument
1019	2018-03-09 15:26:06.907	4	5	updateMeasuringInstrument
1020	2018-03-09 17:00:44.18	4	5	updateMeasuringInstrument
1021	2018-03-09 17:01:28.424	4	5	updateMeasuringInstrument
1022	2018-03-09 17:01:42.153	4	5	updateMeasuringInstrument
1023	2018-03-09 17:02:10.633	4	5	updateMeasuringInstrument
1024	2018-03-09 17:02:22.051	4	5	updateMeasuringInstrument
1025	2018-03-09 17:02:37.013	4	5	updateMeasuringInstrument
1026	2018-03-12 17:16:50.399	4	5	updateMeasuringInstrument
1027	2018-03-12 17:17:02.338	4	5	updateMeasuringInstrument
1028	2018-03-12 17:18:06.765	4	4	createMeasuringInstrument
1029	2018-03-12 17:18:22.529	4	5	updateMeasuringInstrument
1030	2018-03-12 17:24:01.259	4	5	updateMeasuringInstrument
1031	2018-03-12 17:24:14.941	4	5	updateMeasuringInstrument
1032	2018-03-13 09:11:49.773	4	5	updateMeasuringInstrument
1033	2018-03-13 09:12:13.167	4	5	updateMeasuringInstrument
1034	2018-03-13 12:19:14.521	4	5	updateMeasuringInstrument
1035	2018-03-13 15:29:33.621	4	5	updateMeasuringInstrument
1036	2018-03-13 15:30:19.873	4	5	updateMeasuringInstrument
1037	2018-03-13 15:48:02.834	4	5	updateMeasuringInstrument
1038	2018-03-13 15:48:27.806	4	5	updateMeasuringInstrument
1039	2018-03-13 16:24:43.028	4	5	updateMeasuringInstrument
1040	2018-03-13 17:11:18.212	4	5	updateMeasuringInstrument
1041	2018-03-13 17:11:46.82	4	5	updateMeasuringInstrument
1042	2018-03-15 11:12:37.937	4	5	updateMeasuringInstrument
1043	2018-03-15 11:12:49.601	4	5	updateMeasuringInstrument
1044	2018-03-15 11:21:43.505	4	5	updateMeasuringInstrument
1045	2018-03-15 11:23:06.58	4	4	createMeasuringInstrument
1046	2018-03-15 11:23:25.216	4	5	updateMeasuringInstrument
1047	2018-03-15 11:23:48.941	4	5	updateMeasuringInstrument
1048	2018-03-16 16:44:58.687	4	5	updateMeasuringInstrument
1049	2018-03-19 10:23:31.41	4	5	updateMeasuringInstrument
1050	2018-03-19 10:23:45.936	4	5	updateMeasuringInstrument
1051	2018-03-19 10:24:04.077	4	5	updateMeasuringInstrument
1052	2018-03-20 11:02:25.079	4	5	updateMeasuringInstrument
1053	2018-03-20 11:04:17.921	4	5	updateMeasuringInstrument
1054	2018-03-20 11:04:37.859	4	5	updateMeasuringInstrument
1055	2018-03-20 12:45:18.865	4	5	updateMeasuringInstrument
1056	2018-03-20 12:46:54.035	4	5	updateMeasuringInstrument
1057	2018-03-20 12:47:27.945	4	5	updateMeasuringInstrument
1058	2018-03-20 12:47:41.199	4	5	updateMeasuringInstrument
1059	2018-03-20 12:48:32.705	4	5	updateMeasuringInstrument
1060	2018-03-20 12:50:09.596	4	5	updateMeasuringInstrument
1061	2018-03-20 16:13:37.421	4	5	updateMeasuringInstrument
1062	2018-03-20 16:14:01.541	4	5	updateMeasuringInstrument
1063	2018-03-20 16:14:14.524	4	5	updateMeasuringInstrument
1064	2018-03-20 16:14:36.454	4	5	updateMeasuringInstrument
1065	2018-03-21 10:35:56.379	4	5	updateMeasuringInstrument
1066	2018-03-21 11:05:38.442	4	5	updateMeasuringInstrument
1067	2018-03-21 12:39:25.699	4	4	createMeasuringInstrument
1068	2018-03-21 12:40:29.426	4	5	updateMeasuringInstrument
1069	2018-03-22 08:35:53.492	4	5	updateMeasuringInstrument
1070	2018-03-22 08:36:12.305	4	5	updateMeasuringInstrument
1071	2018-03-22 08:38:40.589	4	5	updateMeasuringInstrument
1072	2018-03-22 08:38:53.105	4	5	updateMeasuringInstrument
1073	2018-03-23 09:27:17.74	4	5	updateMeasuringInstrument
1074	2018-03-23 09:27:40.928	4	5	updateMeasuringInstrument
1075	2018-03-24 11:51:49.534	6	5	updateMeasuringInstrument
1076	2018-03-24 11:52:12.41	6	5	updateMeasuringInstrument
1077	2018-03-26 10:17:10.465	4	5	updateMeasuringInstrument
1078	2018-03-26 10:17:22.059	4	5	updateMeasuringInstrument
1079	2018-03-28 09:25:48.222	4	5	updateMeasuringInstrument
1080	2018-03-28 12:26:14.888	4	5	updateMeasuringInstrument
1081	2018-03-28 13:30:24.04	4	5	updateMeasuringInstrument
1082	2018-03-28 13:30:54.333	4	5	updateMeasuringInstrument
1083	2018-03-28 13:31:32.308	4	5	updateMeasuringInstrument
1084	2018-03-28 13:31:43.485	4	5	updateMeasuringInstrument
1085	2018-03-28 13:31:59.449	4	5	updateMeasuringInstrument
1086	2018-03-28 16:14:31.036	4	5	updateMeasuringInstrument
1087	2018-03-28 16:29:52.702	4	5	updateMeasuringInstrument
1088	2018-03-28 17:13:58.13	4	5	updateMeasuringInstrument
1089	2018-03-28 17:47:58.785	4	5	updateMeasuringInstrument
1090	2018-03-28 18:07:13.262	4	5	updateMeasuringInstrument
1091	2018-03-28 19:47:10.49	4	5	updateMeasuringInstrument
1092	2018-03-28 19:47:27.911	4	5	updateMeasuringInstrument
1093	2018-03-28 19:47:47.433	4	5	updateMeasuringInstrument
1094	2018-04-03 10:35:51.911	4	5	updateMeasuringInstrument
1095	2018-04-03 12:43:33.382	4	5	updateMeasuringInstrument
1096	2018-04-03 12:44:24.052	4	5	updateMeasuringInstrument
1097	2018-04-03 12:44:56.972	4	5	updateMeasuringInstrument
1098	2018-04-03 12:45:28.831	4	5	updateMeasuringInstrument
1099	2018-04-03 12:46:00.477	4	5	updateMeasuringInstrument
1100	2018-04-03 13:08:04.994	4	5	updateMeasuringInstrument
1101	2018-04-03 15:40:28.509	4	5	updateMeasuringInstrument
1102	2018-04-03 15:42:09.414	4	5	updateMeasuringInstrument
1103	2018-04-04 08:35:14.515	4	5	updateMeasuringInstrument
1104	2018-04-04 08:36:24.07	4	5	updateMeasuringInstrument
1105	2018-04-04 08:54:43.444	4	5	updateMeasuringInstrument
1106	2018-04-04 10:35:58.398	4	5	updateMeasuringInstrument
1107	2018-04-04 10:36:15.318	4	5	updateMeasuringInstrument
1108	2018-04-04 10:36:38.481	4	5	updateMeasuringInstrument
1109	2018-04-04 13:16:06.118	4	5	updateMeasuringInstrument
1111	2018-04-04 13:25:57.719	4	5	updateMeasuringInstrument
1112	2018-04-04 13:27:37.955	4	5	updateMeasuringInstrument
1113	2018-04-04 13:28:01.661	4	5	updateMeasuringInstrument
1114	2018-04-04 13:29:21.208	4	5	updateMeasuringInstrument
1115	2018-04-04 13:30:16.792	4	5	updateMeasuringInstrument
1116	2018-04-04 13:31:04.741	4	5	updateMeasuringInstrument
1117	2018-04-04 13:31:46.446	4	5	updateMeasuringInstrument
1119	2018-04-04 13:55:00.017	4	5	updateMeasuringInstrument
1110	2018-04-04 13:22:49.931	4	5	updateMeasuringInstrument
1118	2018-04-04 13:33:51.395	4	5	updateMeasuringInstrument
1120	2018-04-05 09:09:31.642	4	5	updateMeasuringInstrument
1121	2018-04-06 11:10:02.574	4	5	updateMeasuringInstrument
1122	2018-04-06 17:34:27.737	4	5	updateMeasuringInstrument
1123	2018-04-09 12:34:38.014	4	5	updateMeasuringInstrument
1124	2018-04-09 12:34:50.932	4	5	updateMeasuringInstrument
1125	2018-04-09 12:35:53.446	4	5	updateMeasuringInstrument
1126	2018-04-09 12:36:32.378	4	5	updateMeasuringInstrument
1127	2018-04-09 12:37:11.423	4	5	updateMeasuringInstrument
1128	2018-04-09 12:37:29.317	4	5	updateMeasuringInstrument
1129	2018-04-09 13:06:29.44	6	5	updateMeasuringInstrument
1130	2018-04-09 13:07:49.323	6	5	updateMeasuringInstrument
1131	2018-04-09 14:01:16.971	6	5	updateMeasuringInstrument
1132	2018-04-09 14:01:41.787	6	5	updateMeasuringInstrument
1133	2018-04-09 14:01:59.959	6	5	updateMeasuringInstrument
1134	2018-04-09 16:05:34.33	4	5	updateMeasuringInstrument
1135	2018-04-09 16:05:53.991	4	5	updateMeasuringInstrument
1136	2018-04-09 16:06:09.343	4	5	updateMeasuringInstrument
1137	2018-04-09 16:13:58.062	4	5	updateMeasuringInstrument
1138	2018-04-09 17:14:32.248	4	5	updateMeasuringInstrument
1139	2018-04-09 17:15:23.275	4	5	updateMeasuringInstrument
1140	2018-04-10 15:26:11.304	4	5	updateMeasuringInstrument
1141	2018-04-11 12:36:35.652	6	5	updateMeasuringInstrument
1142	2018-04-11 12:40:29.391	6	5	updateMeasuringInstrument
1143	2018-04-11 12:45:16.516	6	5	updateMeasuringInstrument
1144	2018-04-16 15:55:57.589	6	\N	\N
1145	2018-04-16 15:56:26.088	6	5	updateMeasuringInstrument
1146	2018-04-17 17:08:05.225	6	5	updateMeasuringInstrument
1147	2018-04-17 17:08:48.591	6	5	updateMeasuringInstrument
1148	2018-04-17 17:08:57.631	6	5	updateMeasuringInstrument
1149	2018-04-17 17:09:11.335	6	5	updateMeasuringInstrument
1150	2018-04-17 18:20:01.413	6	5	updateMeasuringInstrument
1151	2018-04-17 18:20:13.304	6	5	updateMeasuringInstrument
1152	2018-04-17 18:59:32.618	6	5	updateMeasuringInstrument
1153	2018-04-18 17:04:39.612	6	5	updateMeasuringInstrument
1154	2018-04-18 17:59:08.589	6	5	updateMeasuringInstrument
1155	2018-04-18 17:59:30.385	6	5	updateMeasuringInstrument
1156	2018-04-18 18:00:17.509	6	5	updateMeasuringInstrument
1157	2018-04-18 19:20:04.027	6	4	createMeasuringInstrument
1158	2018-04-18 19:20:39.245	6	4	createMeasuringInstrument
1159	2018-04-18 19:20:59.869	6	5	updateMeasuringInstrument
1160	2018-04-19 15:09:41.91	6	5	updateMeasuringInstrument
1161	2018-04-19 15:48:42.975	6	5	updateMeasuringInstrument
1162	2018-04-19 15:49:22.755	6	5	updateMeasuringInstrument
1163	2018-04-19 15:49:45.739	6	5	updateMeasuringInstrument
1164	2018-04-24 13:34:24.425	4	5	updateMeasuringInstrument
1165	2018-04-30 11:34:27.92	6	5	updateMeasuringInstrument
1166	2018-04-30 11:35:40.508	6	5	updateMeasuringInstrument
1167	2018-04-30 11:38:52.763	6	5	updateMeasuringInstrument
1168	2018-04-30 11:40:02.31	6	5	updateMeasuringInstrument
1169	2018-05-02 14:01:58.622	6	5	updateMeasuringInstrument
1170	2018-05-04 16:07:39.904	6	5	updateMeasuringInstrument
1171	2018-05-04 16:08:58.628	6	5	updateMeasuringInstrument
1172	2018-05-07 11:08:30.583	6	5	updateMeasuringInstrument
1173	2018-05-07 11:09:41.222	6	5	updateMeasuringInstrument
1174	2018-05-07 11:10:35.897	6	5	updateMeasuringInstrument
1175	2018-05-07 11:11:36.847	6	5	updateMeasuringInstrument
1176	2018-05-08 09:26:26.771	6	5	updateMeasuringInstrument
1177	2018-05-08 09:46:09.584	6	5	updateMeasuringInstrument
1178	2018-05-08 10:18:10.014	6	5	updateMeasuringInstrument
1179	2018-05-08 13:05:08.427	6	5	updateMeasuringInstrument
1180	2018-05-08 13:49:38.344	6	5	updateMeasuringInstrument
1181	2018-05-09 17:09:23.271	6	5	updateMeasuringInstrument
1182	2018-05-09 17:09:45.473	6	5	updateMeasuringInstrument
1183	2018-05-14 08:54:35.799	6	5	updateMeasuringInstrument
1184	2018-05-14 09:55:58.545	6	5	updateMeasuringInstrument
1185	2018-05-15 08:23:49.612	6	\N	\N
1186	2018-05-15 08:24:27.963	6	7	removeMeasuringInstrument
1187	2018-05-15 08:25:20.812	6	8	removeMeasuringInstrument
1188	2018-05-15 08:25:20.96	6	8	removeMeasuringInstrument
1189	2018-05-15 08:25:21.197	6	8	removeMeasuringInstrument
1190	2018-05-15 08:25:21.238	6	8	removeMeasuringInstrument
1191	2018-05-15 08:25:21.469	6	8	removeMeasuringInstrument
1192	2018-05-15 08:25:21.526	6	8	removeMeasuringInstrument
1193	2018-05-15 08:25:21.539	6	8	removeMeasuringInstrument
1194	2018-05-15 08:25:21.552	6	8	removeMeasuringInstrument
1195	2018-05-15 08:25:21.567	6	8	removeMeasuringInstrument
1196	2018-05-15 08:25:21.618	6	8	removeMeasuringInstrument
1197	2018-05-15 08:25:21.714	6	8	removeMeasuringInstrument
1198	2018-05-15 08:25:21.761	6	8	removeMeasuringInstrument
1199	2018-05-15 08:25:21.796	6	8	removeMeasuringInstrument
1200	2018-05-15 08:25:21.869	6	8	removeMeasuringInstrument
1201	2018-05-15 08:25:21.91	6	8	removeMeasuringInstrument
1202	2018-05-15 08:25:21.948	6	8	removeMeasuringInstrument
1203	2018-05-15 08:25:21.973	6	8	removeMeasuringInstrument
1204	2018-05-15 08:25:21.992	6	8	removeMeasuringInstrument
1205	2018-05-15 08:25:22.052	6	8	removeMeasuringInstrument
1206	2018-05-15 09:29:50.283	6	\N	\N
1207	2018-05-15 09:31:43.31	6	4	createMeasuringInstrument
1208	2018-05-15 09:32:25.932	6	7	removeMeasuringInstrument
1209	2018-05-15 09:34:06.454	6	8	removeMeasuringInstrument
1210	2018-05-15 09:34:36.148	6	4	createMeasuringInstrument
1211	2018-05-15 09:35:49.512	6	4	createMeasuringInstrument
1212	2018-05-15 09:36:59.993	6	4	createMeasuringInstrument
1213	2018-05-15 09:37:15.79	6	7	removeMeasuringInstrument
1214	2018-05-15 09:37:33.881	6	8	removeMeasuringInstrument
1215	2018-05-15 09:40:55.285	6	4	createMeasuringInstrument
1216	2018-05-15 09:41:53.95	6	4	createMeasuringInstrument
1219	2018-05-15 15:22:54.331	6	5	updateMeasuringInstrument
1221	2018-05-15 15:37:24.579	6	5	updateMeasuringInstrument
1217	2018-05-15 09:44:51.235	6	5	updateMeasuringInstrument
1218	2018-05-15 10:10:00.903	6	5	updateMeasuringInstrument
1220	2018-05-15 15:37:09.268	6	5	updateMeasuringInstrument
1222	2018-05-16 08:45:14.665	6	5	updateMeasuringInstrument
1223	2018-05-16 08:49:27.316	6	5	updateMeasuringInstrument
1224	2018-05-16 08:50:54.451	6	5	updateMeasuringInstrument
1225	2018-05-16 08:53:35.296	6	5	updateMeasuringInstrument
1226	2018-05-16 08:56:24.224	6	5	updateMeasuringInstrument
1227	2018-05-16 08:58:06.74	6	5	updateMeasuringInstrument
1228	2018-05-16 09:03:27.085	6	5	updateMeasuringInstrument
1229	2018-05-16 09:08:34.164	6	5	updateMeasuringInstrument
1230	2018-05-16 14:07:36.259	6	5	updateMeasuringInstrument
1231	2018-05-23 09:13:05.368	6	5	updateMeasuringInstrument
1232	2018-05-23 09:18:12.465	6	5	updateMeasuringInstrument
1233	2018-05-23 09:18:57.232	6	5	updateMeasuringInstrument
1234	2018-05-23 09:27:14.401	6	5	updateMeasuringInstrument
1235	2018-05-23 09:27:42.139	6	5	updateMeasuringInstrument
1236	2018-05-28 10:06:41.12	1	\N	\N
1237	2018-06-08 14:20:05.537	1	\N	\N
1238	2018-06-08 14:20:27.428	1	\N	\N
1239	2018-06-08 14:21:24.103	1	\N	\N
1240	2018-06-08 14:21:50.131	1	\N	\N
1241	2018-06-08 14:23:09.737	1	\N	\N
1255	2018-06-11 09:09:28.545	1	\N	\N
1256	2018-06-11 09:10:07.408	1	\N	\N
1257	2018-06-11 09:10:37.576	1	7	removeMeasuringInstrument
1258	2018-06-11 09:11:09.278	1	8	removeMeasuringInstrument
1259	2018-06-11 09:11:35.385	1	8	removeMeasuringInstrument
1260	2018-06-11 09:11:53.144	1	8	removeMeasuringInstrument
1261	2018-06-11 09:12:13.26	1	8	removeMeasuringInstrument
1263	2018-06-11 09:14:53.181	1	4	createMeasuringInstrument
1264	2018-06-11 09:15:11.051	1	4	createMeasuringInstrument
1265	2018-06-11 09:15:21.717	1	4	createMeasuringInstrument
1266	2018-06-11 09:15:44.213	1	4	createMeasuringInstrument
1267	2018-06-11 09:15:52.938	1	4	createMeasuringInstrument
1268	2018-06-11 09:16:20.764	1	4	createMeasuringInstrument
1269	2018-06-11 09:18:44.09	1	8	removeMeasuringInstrument
1270	2018-06-11 09:20:09.984	1	8	removeMeasuringInstrument
1271	2018-06-11 09:57:19.017	1	\N	\N
1273	2018-06-11 10:08:36.713	1	5	updateMeasuringInstrument
1274	2018-06-11 10:09:32.223	1	5	updateMeasuringInstrument
1279	2018-06-11 10:52:23.873	1	4	createMeasuringInstrument
1280	2018-06-11 10:53:05.795	1	5	updateMeasuringInstrument
1281	2018-06-11 10:59:44.149	1	5	updateMeasuringInstrument
1282	2018-06-11 11:08:02.032	1	4	createMeasuringInstrument
1283	2018-06-11 12:09:01.158	1	4	createMeasuringInstrument
1284	2018-06-11 12:09:18.847	1	7	removeMeasuringInstrument
1285	2018-06-11 12:09:42.134	1	8	removeMeasuringInstrument
1286	2018-06-11 12:10:51.749	1	8	removeMeasuringInstrument
1287	2018-06-11 12:11:04.61	1	8	removeMeasuringInstrument
1288	2018-06-11 12:11:13.735	1	8	removeMeasuringInstrument
1289	2018-06-11 12:11:47.024	1	8	removeMeasuringInstrument
1290	2018-06-11 12:13:03.911	1	4	createMeasuringInstrument
1291	2018-06-11 12:13:52.458	1	5	updateMeasuringInstrument
1292	2018-06-11 12:15:22.547	1	5	updateMeasuringInstrument
1293	2018-06-11 12:16:20.98	1	5	updateMeasuringInstrument
1294	2018-06-11 12:17:27.272	1	5	updateMeasuringInstrument
1295	2018-06-11 12:18:55.957	1	5	updateMeasuringInstrument
1296	2018-06-11 12:19:39.884	1	5	updateMeasuringInstrument
1297	2018-06-11 12:22:37.693	1	5	updateMeasuringInstrument
1298	2018-06-11 12:36:37.39	1	4	createMeasuringInstrument
1299	2018-06-11 12:37:41.757	1	5	updateMeasuringInstrument
1300	2018-06-11 12:40:24.607	1	5	updateMeasuringInstrument
1301	2018-06-11 12:50:03.967	1	5	updateMeasuringInstrument
1302	2018-06-11 12:53:17.873	1	5	updateMeasuringInstrument
1303	2018-06-11 12:55:41.055	1	5	updateMeasuringInstrument
1304	2018-06-11 12:55:51.846	1	5	updateMeasuringInstrument
1305	2018-06-11 13:05:12.591	1	5	updateMeasuringInstrument
1306	2018-06-11 13:06:34.68	1	5	updateMeasuringInstrument
1307	2018-06-11 13:06:51.226	1	5	updateMeasuringInstrument
1308	2018-06-11 13:07:06.394	1	5	updateMeasuringInstrument
1309	2018-06-11 13:09:45.703	1	5	updateMeasuringInstrument
1310	2018-06-11 13:13:37.035	1	7	removeMeasuringInstrument
1311	2018-06-11 13:15:25.078	1	8	removeMeasuringInstrument
1312	2018-06-11 13:15:42.686	1	8	removeMeasuringInstrument
1313	2018-06-11 13:15:59.748	1	8	removeMeasuringInstrument
1314	2018-06-11 13:16:14.573	1	8	removeMeasuringInstrument
1315	2018-06-11 13:20:46.272	1	4	createMeasuringInstrument
1316	2018-06-11 13:22:36.12	1	5	updateMeasuringInstrument
1317	2018-06-11 13:23:06.91	1	5	updateMeasuringInstrument
1318	2018-06-11 13:23:30.442	1	5	updateMeasuringInstrument
1319	2018-06-11 13:24:20.547	1	5	updateMeasuringInstrument
1320	2018-06-11 13:24:27.361	1	5	updateMeasuringInstrument
1321	2018-06-11 13:24:40.056	1	5	updateMeasuringInstrument
1322	2018-06-11 13:24:49.291	1	5	updateMeasuringInstrument
1326	2018-06-11 14:18:06.126	1	\N	\N
1327	2018-06-12 13:20:39.854	1	\N	\N
1328	2018-06-13 10:32:16.002	1	\N	\N
1329	2018-06-13 15:55:33.565	1	\N	\N
1330	2018-06-13 16:12:39.623	1	\N	\N
1331	2018-06-13 16:26:57.413	1	6	removeMeasuringInstrument
1332	2018-06-13 16:27:01.238	1	6	removeMeasuringInstrument
1333	2018-06-13 16:27:04.271	1	6	removeMeasuringInstrument
1334	2018-06-13 16:32:04.733	1	\N	\N
1335	2018-06-13 16:45:23.264	1	\N	\N
1336	2018-06-13 16:47:03.691	1	\N	\N
1337	2018-06-13 16:49:02.457	1	6	removeMeasuringInstrument
1338	2018-06-14 14:42:55.732	1	\N	\N
1339	2018-06-14 14:55:27.256	1	5	updateMeasuringInstrument
1340	2018-06-14 15:03:19.616	1	\N	\N
1341	2018-06-14 15:03:25.214	1	\N	\N
1342	2018-06-14 15:03:38.909	1	6	removeMeasuringInstrument
1343	2018-06-14 15:05:06.735	1	6	removeMeasuringInstrument
1344	2018-06-14 15:06:16.181	1	4	createMeasuringInstrument
1345	2018-06-14 15:08:07.97	1	4	createMeasuringInstrument
1346	2018-06-14 15:13:27.941	1	4	createMeasuringInstrument
1347	2018-06-14 15:13:51.719	1	4	createMeasuringInstrument
1348	2018-06-14 15:14:10.571	1	6	removeMeasuringInstrument
1349	2018-06-14 15:16:36.687	1	6	removeMeasuringInstrument
1350	2018-06-14 15:17:21.102	1	6	removeMeasuringInstrument
1351	2018-06-14 15:17:40.761	1	6	removeMeasuringInstrument
1352	2018-06-14 15:17:41.941	1	6	removeMeasuringInstrument
1353	2018-06-14 15:17:55.374	1	6	removeMeasuringInstrument
1354	2018-06-14 15:17:57.384	1	6	removeMeasuringInstrument
1355	2018-06-14 15:19:46.783	1	4	createMeasuringInstrument
1356	2018-06-14 15:21:37.986	1	4	createMeasuringInstrument
1359	2018-06-14 15:32:18.153	1	6	removeMeasuringInstrument
1360	2018-06-14 15:32:20.684	1	6	removeMeasuringInstrument
1361	2018-06-14 15:32:22.888	1	6	removeMeasuringInstrument
1362	2018-06-14 15:32:25.083	1	6	removeMeasuringInstrument
1363	2018-06-14 15:32:28.423	1	6	removeMeasuringInstrument
1364	2018-06-14 15:32:33.772	1	6	removeMeasuringInstrument
1365	2018-06-14 15:33:34.958	1	6	removeMeasuringInstrument
1357	2018-06-14 15:29:32.531	1	4	createMeasuringInstrument
1358	2018-06-14 15:30:45.142	1	4	createMeasuringInstrument
1366	2018-06-14 16:05:37.661	1	\N	\N
1367	2018-06-14 16:08:11.366	1	\N	\N
1368	2018-06-14 16:20:33.152	1	\N	\N
1369	2018-06-14 16:21:22.458	1	5	updateMeasuringInstrument
1370	2018-06-14 16:23:28.304	1	5	updateMeasuringInstrument
1371	2018-06-14 16:23:59.873	1	5	updateMeasuringInstrument
1372	2018-06-14 16:39:31.906	1	5	updateMeasuringInstrument
1373	2018-06-14 16:44:09.56	1	5	updateMeasuringInstrument
1374	2018-06-14 17:04:19.038	1	5	updateMeasuringInstrument
1375	2018-06-14 17:04:29.46	1	5	updateMeasuringInstrument
1376	2018-06-14 17:06:44.888	1	\N	\N
1377	2018-06-14 17:07:25.681	1	5	updateMeasuringInstrument
1378	2018-06-14 17:08:18.844	1	5	updateMeasuringInstrument
1379	2018-06-14 17:09:39.003	1	4	createMeasuringInstrument
1380	2018-06-14 17:10:29.687	1	4	createMeasuringInstrument
1381	2018-06-14 17:26:06.699	1	\N	\N
1382	2018-06-14 17:34:58.173	1	\N	\N
1383	2018-06-14 17:35:25.402	1	5	updateMeasuringInstrument
1384	2018-06-14 17:37:23.906	1	4	createMeasuringInstrument
1385	2018-06-14 17:38:11.127	1	4	createMeasuringInstrument
1386	2018-06-14 17:39:16.468	1	4	createMeasuringInstrument
1387	2018-06-14 17:39:40.14	1	5	updateMeasuringInstrument
1388	2018-06-14 17:39:59.064	1	5	updateMeasuringInstrument
1389	2018-06-14 18:34:17.847	1	5	updateMeasuringInstrument
1390	2018-06-14 18:37:42.937	1	5	updateMeasuringInstrument
1391	2018-06-14 18:41:38.808	1	\N	\N
1392	2018-06-14 18:44:49.278	1	4	createMeasuringInstrument
1393	2018-06-14 18:45:33.016	1	5	updateMeasuringInstrument
1394	2018-06-14 18:54:18.615	1	5	updateMeasuringInstrument
\.


--
-- TOC entry 2317 (class 0 OID 49466)
-- Dependencies: 204
-- Data for Name: tb_user; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_user (code, ldap_login, user_name, active, last_access, version, email) FROM stdin;
6	yuri.dourado	Yuri.Dourado	T	2018-05-23 08:12:24.006	61	\N
4	cirilo.silva	cirilo.silva	T	2018-04-24 13:33:36.973	238	\N
3	abenaias.melo	abenaias.melo	T	2018-05-10 20:46:26.728	41	\N
1	teamcity	teamcity	T	2018-06-14 18:52:52.317	252	teamcity@eletraenergy.com
2	janio.oliveira	janio.oliveira	T	2018-05-23 09:26:24.509	372	janio.oliveira@eletraenergy.com
\.


--
-- TOC entry 2318 (class 0 OID 49469)
-- Dependencies: 205
-- Data for Name: tb_user_functionality; Type: TABLE DATA; Schema: mecsw; Owner: mecsw
--

COPY tb_user_functionality (user_code, functionality_code) FROM stdin;
3	1
3	2
3	3
3	4
3	5
3	6
3	7
3	8
3	9
3	10
3	11
3	12
4	1
4	2
4	3
4	4
4	5
4	6
4	7
4	8
4	9
4	10
4	11
4	12
6	1
6	2
6	3
6	4
6	5
6	6
6	7
6	8
6	9
6	10
6	11
6	12
1	1
1	2
1	3
1	4
1	5
1	6
1	7
1	8
1	9
1	10
1	11
1	12
1	13
2	13
2	1
2	2
2	3
2	4
2	5
2	6
2	7
2	8
2	9
2	10
2	11
2	12
\.


--
-- TOC entry 2107 (class 2606 OID 49473)
-- Name: tb_functionality pk_functionality_code; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_functionality
    ADD CONSTRAINT pk_functionality_code PRIMARY KEY (code);


--
-- TOC entry 2151 (class 2606 OID 49475)
-- Name: tb_user_functionality pk_functionality_user; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_user_functionality
    ADD CONSTRAINT pk_functionality_user PRIMARY KEY (user_code, functionality_code);


--
-- TOC entry 2111 (class 2606 OID 49477)
-- Name: tb_measuring_instrument pk_measuring_instrument; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument
    ADD CONSTRAINT pk_measuring_instrument PRIMARY KEY (code);


--
-- TOC entry 2113 (class 2606 OID 49479)
-- Name: tb_measuring_instrument_aud pk_measuring_instrument_aud; Type: CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_measuring_instrument_aud
    ADD CONSTRAINT pk_measuring_instrument_aud PRIMARY KEY (code, code_review);


--
-- TOC entry 2153 (class 2606 OID 57359)
-- Name: tb_measuring_instrument_department pk_measuring_instrument_department; Type: CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_measuring_instrument_department
    ADD CONSTRAINT pk_measuring_instrument_department PRIMARY KEY (code);


--
-- TOC entry 2157 (class 2606 OID 57369)
-- Name: tb_measuring_instrument_department_aud pk_measuring_instrument_department_aud; Type: CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_measuring_instrument_department_aud
    ADD CONSTRAINT pk_measuring_instrument_department_aud PRIMARY KEY (code, code_review);


--
-- TOC entry 2115 (class 2606 OID 49481)
-- Name: tb_measuring_instrument_manufacturer pk_measuring_instrument_manufacturer; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_manufacturer
    ADD CONSTRAINT pk_measuring_instrument_manufacturer PRIMARY KEY (code);


--
-- TOC entry 2119 (class 2606 OID 49483)
-- Name: tb_measuring_instrument_manufacturer_aud pk_measuring_instrument_manufacturer_aud; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_manufacturer_aud
    ADD CONSTRAINT pk_measuring_instrument_manufacturer_aud PRIMARY KEY (code, code_review);


--
-- TOC entry 2123 (class 2606 OID 49485)
-- Name: tb_measuring_instrument_model pk_measuring_instrument_model; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_model
    ADD CONSTRAINT pk_measuring_instrument_model PRIMARY KEY (code);


--
-- TOC entry 2125 (class 2606 OID 49487)
-- Name: tb_measuring_instrument_model_aud pk_measuring_instrument_model_aud; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_model_aud
    ADD CONSTRAINT pk_measuring_instrument_model_aud PRIMARY KEY (code, code_review);


--
-- TOC entry 2127 (class 2606 OID 49489)
-- Name: tb_measuring_instrument_place_use pk_measuring_instrument_place_use; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_place_use
    ADD CONSTRAINT pk_measuring_instrument_place_use PRIMARY KEY (code);


--
-- TOC entry 2131 (class 2606 OID 49491)
-- Name: tb_measuring_instrument_place_use_aud pk_measuring_instrument_place_use_aud; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_place_use_aud
    ADD CONSTRAINT pk_measuring_instrument_place_use_aud PRIMARY KEY (code, code_review);


--
-- TOC entry 2133 (class 2606 OID 49493)
-- Name: tb_measuring_instrument_standard pk_measuring_instrument_standard; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_standard
    ADD CONSTRAINT pk_measuring_instrument_standard PRIMARY KEY (code);


--
-- TOC entry 2137 (class 2606 OID 49495)
-- Name: tb_measuring_instrument_standard_aud pk_measuring_instrument_standard_aud; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_standard_aud
    ADD CONSTRAINT pk_measuring_instrument_standard_aud PRIMARY KEY (code, code_review);


--
-- TOC entry 2143 (class 2606 OID 49497)
-- Name: tb_measuring_instrument_type_aud pk_measuring_instrument_type_aud_code; Type: CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_measuring_instrument_type_aud
    ADD CONSTRAINT pk_measuring_instrument_type_aud_code PRIMARY KEY (code, code_review);


--
-- TOC entry 2139 (class 2606 OID 49499)
-- Name: tb_measuring_instrument_type pk_measuring_instrument_type_code; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_type
    ADD CONSTRAINT pk_measuring_instrument_type_code PRIMARY KEY (code);


--
-- TOC entry 2121 (class 2606 OID 49501)
-- Name: tb_measuring_instrument_measuring_instrument_standard pk_mi_mi_standard; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_measuring_instrument_standard
    ADD CONSTRAINT pk_mi_mi_standard PRIMARY KEY (measuring_instrument_code, measuring_instrument_standard_code);


--
-- TOC entry 2145 (class 2606 OID 49503)
-- Name: tb_review pk_review; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_review
    ADD CONSTRAINT pk_review PRIMARY KEY (code);


--
-- TOC entry 2163 (class 2606 OID 57576)
-- Name: tb_calibration_laboratory_aud pk_tb_calibration_laboratory_aud; Type: CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_calibration_laboratory_aud
    ADD CONSTRAINT pk_tb_calibration_laboratory_aud PRIMARY KEY (code, code_review);


--
-- TOC entry 2147 (class 2606 OID 49505)
-- Name: tb_user pk_user_code; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_user
    ADD CONSTRAINT pk_user_code PRIMARY KEY (code);


--
-- TOC entry 2159 (class 2606 OID 57563)
-- Name: tb_calibration_laboratory tb_calibration_laboratory_pkey; Type: CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_calibration_laboratory
    ADD CONSTRAINT tb_calibration_laboratory_pkey PRIMARY KEY (code);


--
-- TOC entry 2161 (class 2606 OID 57568)
-- Name: tb_calibration_laboratory_telphone tb_calibration_laboratory_telphone_pkey; Type: CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_calibration_laboratory_telphone
    ADD CONSTRAINT tb_calibration_laboratory_telphone_pkey PRIMARY KEY (code);


--
-- TOC entry 2109 (class 2606 OID 49507)
-- Name: tb_functionality uc_functionality_mnemonic; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_functionality
    ADD CONSTRAINT uc_functionality_mnemonic UNIQUE (mnemonic);


--
-- TOC entry 2155 (class 2606 OID 57361)
-- Name: tb_measuring_instrument_department uc_measuring_instrument_department_name; Type: CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_measuring_instrument_department
    ADD CONSTRAINT uc_measuring_instrument_department_name UNIQUE (name);


--
-- TOC entry 2117 (class 2606 OID 49509)
-- Name: tb_measuring_instrument_manufacturer uc_measuring_instrument_manufacturer_name; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_manufacturer
    ADD CONSTRAINT uc_measuring_instrument_manufacturer_name UNIQUE (name);


--
-- TOC entry 2129 (class 2606 OID 49511)
-- Name: tb_measuring_instrument_place_use uc_measuring_instrument_place_use_name; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_place_use
    ADD CONSTRAINT uc_measuring_instrument_place_use_name UNIQUE (name);


--
-- TOC entry 2135 (class 2606 OID 49513)
-- Name: tb_measuring_instrument_standard uc_measuring_instrument_standard_name; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_standard
    ADD CONSTRAINT uc_measuring_instrument_standard_name UNIQUE (name);


--
-- TOC entry 2141 (class 2606 OID 49515)
-- Name: tb_measuring_instrument_type uc_measuring_instrument_type_name; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_type
    ADD CONSTRAINT uc_measuring_instrument_type_name UNIQUE (name);


--
-- TOC entry 2149 (class 2606 OID 49517)
-- Name: tb_user uc_user_ldap_login; Type: CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_user
    ADD CONSTRAINT uc_user_ldap_login UNIQUE (ldap_login);


--
-- TOC entry 2179 (class 2606 OID 49518)
-- Name: tb_user_functionality fk_functionality_user__functionality_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_user_functionality
    ADD CONSTRAINT fk_functionality_user__functionality_01 FOREIGN KEY (functionality_code) REFERENCES tb_functionality(code);


--
-- TOC entry 2180 (class 2606 OID 49523)
-- Name: tb_user_functionality fk_functionality_user__user_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_user_functionality
    ADD CONSTRAINT fk_functionality_user__user_01 FOREIGN KEY (user_code) REFERENCES tb_user(code);


--
-- TOC entry 2169 (class 2606 OID 49528)
-- Name: tb_measuring_instrument_aud fk_measuring_instrument_aud__review_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_measuring_instrument_aud
    ADD CONSTRAINT fk_measuring_instrument_aud__review_01 FOREIGN KEY (code_review) REFERENCES tb_review(code);


--
-- TOC entry 2182 (class 2606 OID 57370)
-- Name: tb_measuring_instrument_department_aud fk_measuring_instrument_department_aud__review_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_measuring_instrument_department_aud
    ADD CONSTRAINT fk_measuring_instrument_department_aud__review_01 FOREIGN KEY (code_review) REFERENCES tb_review(code);


--
-- TOC entry 2170 (class 2606 OID 49533)
-- Name: tb_measuring_instrument_manufacturer_aud fk_measuring_instrument_manufacturer_aud__review_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_manufacturer_aud
    ADD CONSTRAINT fk_measuring_instrument_manufacturer_aud__review_01 FOREIGN KEY (code_review) REFERENCES tb_review(code);


--
-- TOC entry 2174 (class 2606 OID 49538)
-- Name: tb_measuring_instrument_model_aud fk_measuring_instrument_model_aud__review_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_model_aud
    ADD CONSTRAINT fk_measuring_instrument_model_aud__review_01 FOREIGN KEY (code_review) REFERENCES tb_review(code);


--
-- TOC entry 2175 (class 2606 OID 49543)
-- Name: tb_measuring_instrument_place_use_aud fk_measuring_instrument_place_use_aud__review_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_place_use_aud
    ADD CONSTRAINT fk_measuring_instrument_place_use_aud__review_01 FOREIGN KEY (code_review) REFERENCES tb_review(code);


--
-- TOC entry 2176 (class 2606 OID 49548)
-- Name: tb_measuring_instrument_standard_aud fk_measuring_instrument_standard_aud__review_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_standard_aud
    ADD CONSTRAINT fk_measuring_instrument_standard_aud__review_01 FOREIGN KEY (code_review) REFERENCES tb_review(code);


--
-- TOC entry 2177 (class 2606 OID 49553)
-- Name: tb_measuring_instrument_type_aud fk_measuring_instrument_type_aud__review_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_measuring_instrument_type_aud
    ADD CONSTRAINT fk_measuring_instrument_type_aud__review_01 FOREIGN KEY (code_review) REFERENCES tb_review(code);


--
-- TOC entry 2171 (class 2606 OID 49558)
-- Name: tb_measuring_instrument_measuring_instrument_standard fk_mi__measuring_instrument_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_measuring_instrument_standard
    ADD CONSTRAINT fk_mi__measuring_instrument_01 FOREIGN KEY (measuring_instrument_code) REFERENCES tb_measuring_instrument(code);


--
-- TOC entry 2172 (class 2606 OID 49563)
-- Name: tb_measuring_instrument_measuring_instrument_standard fk_mi__measuring_instrument_standard_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_measuring_instrument_standard
    ADD CONSTRAINT fk_mi__measuring_instrument_standard_01 FOREIGN KEY (measuring_instrument_standard_code) REFERENCES tb_measuring_instrument_standard(code);


--
-- TOC entry 2167 (class 2606 OID 57375)
-- Name: tb_measuring_instrument fk_mi__mi_department_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument
    ADD CONSTRAINT fk_mi__mi_department_01 FOREIGN KEY (measuring_instrument_department_code) REFERENCES tb_measuring_instrument_department(code);


--
-- TOC entry 2164 (class 2606 OID 49568)
-- Name: tb_measuring_instrument fk_mi__mi_model_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument
    ADD CONSTRAINT fk_mi__mi_model_01 FOREIGN KEY (measuring_instrument_model_code) REFERENCES tb_measuring_instrument_model(code);


--
-- TOC entry 2165 (class 2606 OID 49573)
-- Name: tb_measuring_instrument fk_mi__mi_place_use_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument
    ADD CONSTRAINT fk_mi__mi_place_use_01 FOREIGN KEY (measuring_instrument_place_use_code) REFERENCES tb_measuring_instrument_place_use(code);


--
-- TOC entry 2166 (class 2606 OID 49578)
-- Name: tb_measuring_instrument fk_mi__mi_type_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument
    ADD CONSTRAINT fk_mi__mi_type_01 FOREIGN KEY (measuring_instrument_type_code) REFERENCES tb_measuring_instrument_type(code);


--
-- TOC entry 2173 (class 2606 OID 49583)
-- Name: tb_measuring_instrument_model fk_mi_model__mi_manufacturer_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument_model
    ADD CONSTRAINT fk_mi_model__mi_manufacturer_01 FOREIGN KEY (measuring_instrument_manufacturer_code) REFERENCES tb_measuring_instrument_manufacturer(code);


--
-- TOC entry 2183 (class 2606 OID 57577)
-- Name: tb_calibration_laboratory_aud fk_tb_calibration_laboratory_aud__review_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_calibration_laboratory_aud
    ADD CONSTRAINT fk_tb_calibration_laboratory_aud__review_01 FOREIGN KEY (code_review) REFERENCES tb_review(code);


--
-- TOC entry 2178 (class 2606 OID 49588)
-- Name: tb_review pk_review__user_01; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_review
    ADD CONSTRAINT pk_review__user_01 FOREIGN KEY (user_code) REFERENCES tb_user(code);


--
-- TOC entry 2181 (class 2606 OID 57585)
-- Name: tb_measuring_instrument_department tb_measuring_instrument_department_fk; Type: FK CONSTRAINT; Schema: mecsw; Owner: postgres
--

ALTER TABLE ONLY tb_measuring_instrument_department
    ADD CONSTRAINT tb_measuring_instrument_department_fk FOREIGN KEY (user_code) REFERENCES tb_user(code);


--
-- TOC entry 2168 (class 2606 OID 57595)
-- Name: tb_measuring_instrument tb_measuring_instrument_fk; Type: FK CONSTRAINT; Schema: mecsw; Owner: mecsw
--

ALTER TABLE ONLY tb_measuring_instrument
    ADD CONSTRAINT tb_measuring_instrument_fk FOREIGN KEY (calibration_laboratory_code) REFERENCES tb_calibration_laboratory(code);


-- Completed on 2018-06-14 18:57:34

--
-- PostgreSQL database dump complete
--

