package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;

public interface MeasuringInstrumentManufacturerDAO extends DAO<MeasuringInstrumentManufacturer> {
	
	public Long findLastId();
	
	public MeasuringInstrumentManufacturer checkName(String name);

}
