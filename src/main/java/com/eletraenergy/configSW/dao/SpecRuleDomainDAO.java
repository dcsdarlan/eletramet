package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.SpecRuleDomain;


public interface SpecRuleDomainDAO extends DAO<SpecRuleDomain> {
	
	public Long findLastId();
	public SpecRuleDomain checkIdentifier(String identifier);
	List<SpecRuleDomain> childs(SpecRuleDomain parent);

}
