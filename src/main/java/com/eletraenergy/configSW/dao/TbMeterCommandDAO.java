package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.TbMeterCommand;


public interface TbMeterCommandDAO extends DAO<TbMeterCommand> {
	
	public Long findLastId();
	public List<TbMeterCommand> findByConfig(Config config);
	public List<TbMeterCommand> findByConfigAndOption(Config config, Long option);

}
