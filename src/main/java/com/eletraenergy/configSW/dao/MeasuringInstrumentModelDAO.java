package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;
import com.eletraenergy.configSW.model.MeasuringInstrumentModel;

public interface MeasuringInstrumentModelDAO extends DAO<MeasuringInstrumentModel> {
	
	public Long findLastId();
	public List<MeasuringInstrumentModel> findByManufacturer(MeasuringInstrumentManufacturer measuringInstrumentManufacturer);
	public MeasuringInstrumentModel checkName(String name);

}
