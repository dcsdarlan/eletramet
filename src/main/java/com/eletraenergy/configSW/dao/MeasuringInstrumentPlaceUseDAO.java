package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.MeasuringInstrumentPlaceUse;

public interface MeasuringInstrumentPlaceUseDAO extends DAO<MeasuringInstrumentPlaceUse> {
	
	public Long findLastId();
	
	public MeasuringInstrumentPlaceUse checkName(String name);

}
