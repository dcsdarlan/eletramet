package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;


public interface MeterModelDAO extends DAO<MeterModel> {

	List<MeterModel> findByLine(MeterLine meterLine);
	public Long findLastId();
	public MeterModel checkIdentifier(String identifier);

}
