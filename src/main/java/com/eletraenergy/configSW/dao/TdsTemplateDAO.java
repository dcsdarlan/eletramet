package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.TdsTemplate;

public interface TdsTemplateDAO extends DAO<TdsTemplate> {
	
	public Long findLastId();
	
	public TdsTemplate checkIdentifier(String identifier);
	
	public List<TdsTemplate> findByMeterModel(MeterModel meterModel);
	
	public List<TdsTemplate> findByModelOrderIdentifier(MeterModel meterModel);

	
}
