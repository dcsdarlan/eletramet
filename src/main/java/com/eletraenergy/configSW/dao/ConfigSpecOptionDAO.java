package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.ConfigSpecOption;
import com.eletraenergy.configSW.model.SpecOption;


public interface ConfigSpecOptionDAO extends DAO<ConfigSpecOption> {
	
	public Long findLastId();
	public List<ConfigSpecOption> findByConfig(Config config);
	public List<ConfigSpecOption> findBySpec(List<SpecOption> spec);
    public void remove(Config config);

}
