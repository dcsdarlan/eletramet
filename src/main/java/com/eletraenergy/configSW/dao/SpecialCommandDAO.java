package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.SpecialCommand;


public interface SpecialCommandDAO extends DAO<SpecialCommand> {
	
	public Long findLastId();

}
