package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.SpecRule;


public interface SpecRuleDAO extends DAO<SpecRule> {
	
	public Long findLastId();
	public SpecRule checkIdentifier(String identifier);

}
