package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.TdsSpecOption;
import com.eletraenergy.configSW.model.TdsTemplate;


public interface TdsDAO extends DAO<Tds> {
	
	public Long findLastId();
	
	public Tds checkIdentifier(String identifier);
	public List<Tds> findByModel(MeterModel meterModel);
	public Tds checkTemplateOption(TdsTemplate template, TdsSpecOption option);
	public List<Tds> findByModelOrderIdentifier(MeterModel meterModel);
	public List<Tds> findByLine(MeterLine meterLine);

}
