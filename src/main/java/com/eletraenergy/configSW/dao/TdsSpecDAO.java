package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.TdsSpec;


public interface TdsSpecDAO extends DAO<TdsSpec> {
	
	public Long findLastId();
	public TdsSpec checkIdentifier(String identifier);

}
