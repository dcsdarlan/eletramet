package com.eletraenergy.configSW.dao;


import com.eletraenergy.configSW.model.CalibrationLaboratory;
import com.eletraenergy.configSW.model.CalibrationLaboratoryTelphone;
import com.eletraenergy.configSW.model.User;
import org.apache.commons.collections.map.HashedMap;

import java.util.List;
import java.util.Map;

public interface UserDAO extends DAO<User> {

	public User findByUsername(String username);
	public Long findLastId();
	public List<User> findAll();


}
