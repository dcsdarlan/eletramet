package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.Spec;
import com.eletraenergy.configSW.model.SpecOption;


public interface SpecOptionDAO extends DAO<SpecOption> {
	
	public Long findLastId();
	public SpecOption checkIdentifier(String identifier);
	public List<SpecOption> findAllBySpec(Spec spec);

}
