package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.ConfigSpecRuleDomain;

public interface ConfigSpecRuleDomainDAO extends DAO<ConfigSpecRuleDomain> {

	public List<ConfigSpecRuleDomain> findByConfig(Long specCode);
	public void remove(Config config);

}
