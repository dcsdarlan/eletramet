package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.MeasuringInstrument;
import com.eletraenergy.configSW.model.MeasuringInstrumentModel;

public interface MeasuringInstrumentDAO extends DAO<MeasuringInstrument> {
	
	public Long findLastId();
	
	public MeasuringInstrument checkSpecificationIdentifier(String identifier);
	
	public List<MeasuringInstrument> findByModel(MeasuringInstrumentModel measuringInstrumentModel);

	public List<MeasuringInstrument> findOverdueCalibrations();
	
	public List<MeasuringInstrument> findDelayInTheNext90Days();
	
	public List<MeasuringInstrument> findInCalibration();
	
}
