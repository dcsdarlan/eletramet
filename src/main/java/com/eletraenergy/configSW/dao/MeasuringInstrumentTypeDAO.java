package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.MeasuringInstrumentType;

public interface MeasuringInstrumentTypeDAO extends DAO<MeasuringInstrumentType> {
	
	public Long findLastId();
	
	public MeasuringInstrumentType checkName(String name);

}
