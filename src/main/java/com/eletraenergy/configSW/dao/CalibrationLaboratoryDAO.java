package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.CalibrationLaboratory;
import com.eletraenergy.configSW.model.Config;

import java.util.List;


public interface CalibrationLaboratoryDAO extends DAO<CalibrationLaboratory> {
	
	public Long findLastId();

}
