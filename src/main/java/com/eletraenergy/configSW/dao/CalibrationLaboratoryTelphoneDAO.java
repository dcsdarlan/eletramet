package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.CalibrationLaboratory;
import com.eletraenergy.configSW.model.CalibrationLaboratoryTelphone;
import com.eletraenergy.configSW.model.Config;
import org.apache.commons.collections.map.HashedMap;

import java.util.List;
import java.util.Map;


public interface CalibrationLaboratoryTelphoneDAO extends DAO<CalibrationLaboratoryTelphone> {
	
	public Long findLastId();
	public List<CalibrationLaboratoryTelphone> findById(List<Long> id);
	public void removeByLaboratory(CalibrationLaboratory lab);
	public List<CalibrationLaboratoryTelphone> findByLaboratory(CalibrationLaboratory lab);
}
