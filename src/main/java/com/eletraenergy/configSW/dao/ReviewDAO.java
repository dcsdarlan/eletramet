package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.Review;


public interface ReviewDAO extends DAO<Review> {
	
	public List<Review> findLast10();
	public List<Object> findByReview(Review entidade);

}
