package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.ViewSpecialCommand;

public interface ViewSpecialCommandDAO extends DAO<ViewSpecialCommand> {

	public List<ViewSpecialCommand> findByTds(Tds tds);
}
