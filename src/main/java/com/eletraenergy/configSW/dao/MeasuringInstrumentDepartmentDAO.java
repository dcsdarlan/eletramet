package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.MeasuringInstrumentDepartment;

public interface MeasuringInstrumentDepartmentDAO extends DAO<MeasuringInstrumentDepartment> {
	
	public Long findLastId();
	
	public MeasuringInstrumentDepartment checkName(String name);

}
