package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.Spec;
import com.eletraenergy.configSW.model.SpecCategory;


public interface SpecDAO extends DAO<Spec> {
	
	public Long findLastId();
	public Spec checkIdentifier(String identifier);
	public Spec checkOrder(Integer order, SpecCategory specCategory);
	public List<Spec> findBySpecCategory(SpecCategory specCategory);

}
