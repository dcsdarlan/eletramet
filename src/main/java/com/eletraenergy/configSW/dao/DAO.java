package com.eletraenergy.configSW.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.NamedQuery;

import com.eletraenergy.configSW.model.Model;

/**
 *
 * @author Raphael
 */

public interface DAO<Entity extends Model> extends Serializable {

	/**
	 * Create or update one entity
	 *
	 * @param entity
	 * @return entity save
	 */
	Entity save(Entity entity);

	/**
	 * Create or update entities
	 *
	 * @param entity
	 */
	void save(List<Entity> entities);

	/**
	 * Remove an entity
	 *
	 * @param entity
	 */
	void remove(Entity entity);

	/**
	 * Remove entities
	 *
	 * @param entity
	 */
	void remove(List<Entity> entities);

	/**
	 * find all entities 
	 *
	 * @return
	 */
	List<Entity> findAll();

	/**
	 * Get one entity by specific id
	 *
	 * @param idEntity
	 * @return entity if exists, null if not exists
	 */
	Entity findById(Long idEntity);

	/**
	 * get entities that has the same values by parameters
	 *
	 * @param entity
	 * @param indexStart
	 *            index of first entity that will be return
	 * @param amount
	 *            amount of entities
	 * @return
	 */
	public List<Entity> findByParameter(Entity entity, Integer indexStart, Integer amount);

	/**
	 * get amount of entities that has the same values by parameters
	 *
	 * @param entity
	 * @return
	 */
	int amount(Entity entity);

	/**
	 * get amount by {@link NamedQuery}
	 *
	 * @param query
	 * @param parametros
	 * @return
	 */
	public List<Entity> executeQuery(String query, Map<String, Object> parameters, Integer index, Integer maxItens);


	/**
	 * Find entity from reference id</code>
	 *
	 * @param id
	 * @return
	 */	
	Entity findByReference(Long id);

	/**
	 * Execute one query that return one entity</code>
	 *
	 * @param query
	 * @param parameters
	 * @return
	 */	
	Entity executeQuery(String query, Map<String, Object> parameters);	
	
	int executeCommand(String query, Map<String, Object> parameters);	
	
	@SuppressWarnings("hiding")
	<Entity> Entity initializeAndUnproxy(Entity entity);
}
