package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.MeterLine;


public interface MeterLineDAO extends DAO<MeterLine> {
	
	public Long findLastId();
	
	public MeterLine checkIdentifier(String identifier);

}
