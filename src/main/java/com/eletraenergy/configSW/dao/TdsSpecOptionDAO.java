package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.TdsSpecOption;


public interface TdsSpecOptionDAO extends DAO<TdsSpecOption> {
	
	public Long findLastId();
	public TdsSpecOption checkIdentifier(String identifier);
	public TdsSpecOption findByName(String name, String specName);

}
