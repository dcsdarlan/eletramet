package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.Config;


public interface ConfigDAO extends DAO<Config> {
	
	public Long findLastId();
	public List<Config> findById(List<Long> id);

}
