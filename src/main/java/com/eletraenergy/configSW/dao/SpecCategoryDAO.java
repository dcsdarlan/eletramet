package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.SpecCategory;


public interface SpecCategoryDAO extends DAO<SpecCategory> {
	
	public Long findLastId();
	public SpecCategory checkIdentifier(String identifier);
	public SpecCategory checkOrder(Integer order); 

}
