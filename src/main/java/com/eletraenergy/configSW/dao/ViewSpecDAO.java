package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.ViewSpec;

public interface ViewSpecDAO extends DAO<ViewSpec> {

}
