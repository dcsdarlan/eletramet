package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.ConfigSimulation;


public interface ConfigSimulationDAO extends DAO<ConfigSimulation> {

	List<ConfigSimulation> findAll();

	Long findLastId();


}
