package com.eletraenergy.configSW.dao;

import java.util.List;
import com.eletraenergy.configSW.model.FilterModel;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.TdsStateIdentifier;
import com.eletraenergy.configSW.model.ViewTDS;

public interface ViewTDSDAO extends DAO<ViewTDS> {

	public List<ViewTDS> findConfig(MeterLine meterLine, MeterModel meterModel, String tds, List<FilterModel> validFilters, TdsStateIdentifier state);
}
