package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.ViewTDSSpec;

public interface ViewTDSSpecDAO extends DAO<ViewTDSSpec> {

	public List<ViewTDSSpec> findAll();
	public List<ViewTDSSpec> findByLine(MeterLine meterLine);
	public List<ViewTDSSpec> findByLineAndModel(MeterLine meterLine, MeterModel meterModel);
}
