package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.Functionality;

public interface FunctionalityDAO extends DAO<Functionality> {

}
