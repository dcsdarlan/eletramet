package com.eletraenergy.configSW.dao;

import com.eletraenergy.configSW.model.MeasuringInstrumentStandard;

public interface MeasuringInstrumentStandardDAO extends DAO<MeasuringInstrumentStandard> {
	
	public Long findLastId();
	
	public MeasuringInstrumentStandard checkName(String name);

}
