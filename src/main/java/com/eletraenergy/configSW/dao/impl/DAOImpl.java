package com.eletraenergy.configSW.dao.impl;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.ejb.EntityManagerImpl;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.DAO;
import com.eletraenergy.configSW.model.Model;

/**
 *
 * @author Raphael
 */
@Repository
public class DAOImpl<Entity extends Model> implements DAO<Entity> {

	private static final long serialVersionUID = 7538940694726270539L;

	@PersistenceContext
	protected EntityManager entityManager;
	
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Entity save(Entity entity) {
		if (entity.getId() != null) {
			return entityManager.merge(entity);
		}
		entityManager.persist(entity);
		return entity;
	}

	public void save(List<Entity> entities) {
		for (Entity entity : entities) {
			save(entity);
		}
		entityManager.flush();
	}

	public void remove(Entity entity) {
		Entity e = entityManager.contains(entity) ? entity : entityManager.merge(entity);
		entityManager.remove(e);
		entityManager.flush();
	}

	public void remove(List<Entity> entities) {
		for (Entity entity : entities) {
			remove(entity);
		}
	}

	public List<Entity> findAll() {
		return entityManager.createQuery("FROM " + getClassEntity().getSimpleName(), getClassEntity()).getResultList();
	}

	public Entity findById(Long idEntidade) {
		return entityManager.find(getClassEntity(), idEntidade);
	}

	@SuppressWarnings("unchecked")
	public List<Entity> findByParameter(Entity entity, Integer indexStart, Integer amount) {
		Criteria criterio = createCriteriaExample(entity, MatchMode.START);
		if(indexStart!=null){
			criterio.setFirstResult(indexStart);
			criterio.setMaxResults(amount);
		}
		return criterio.list();
	}

	public Entity findByReference(Long id) {
		return this.entityManager.getReference(getClassEntity(),id);
	}	
	public int amount(Entity entity) {
		return ((Number) createCriteriaExample(entity, MatchMode.ANYWHERE).setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

	/**
	 *
	 * @param entity
	 * @return
	 */
	protected Criteria createCriteriaExample(Entity entity, MatchMode match) {
		Example criteriaExample = Example.create(entity);
		return getSession().createCriteria(getClassEntity()).add(criteriaExample.enableLike(match).ignoreCase().excludeZeroes());
	}

	/**
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected Class<Entity> getClassEntity() {
		return (Class<Entity>) ((java.lang.reflect.ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	protected Session getSession() {
		if (entityManager.getDelegate() instanceof EntityManagerImpl) {
			return ((EntityManagerImpl) entityManager.getDelegate()).getSession();
		} else if (entityManager.getDelegate() instanceof Session) {
			return (Session) entityManager.getDelegate();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Entity> executeQuery(String query, Map<String, Object> parameters, Integer index, Integer maxItem) {
		Query queryNamed = null;
		try {
			queryNamed = entityManager.createNamedQuery(query);
		} catch (Exception e) {
			queryNamed = entityManager.createQuery(query, getClassEntity());
		}
		if (index!=null){
			queryNamed.setFirstResult(index);
			queryNamed.setMaxResults(maxItem);
		}
		if(parameters!=null){
			Set<String> keyParametros = parameters.keySet();
			for (String nameParameter : keyParametros) {
				queryNamed.setParameter(nameParameter, parameters.get(nameParameter));
			}
		}
		return queryNamed.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Entity executeQuery(String query, Map<String, Object> parameters) {
		Query queryNamed = entityManager.createNamedQuery(query);
		if(parameters!=null){
			Set<String> keyParameters = parameters.keySet();
			for (String nameParameter : keyParameters) {
				queryNamed.setParameter(nameParameter, parameters.get(nameParameter));
			}
		}
		return (Entity) queryNamed.getSingleResult();
	}	

	public int executeCommand(String query, Map<String, Object> parameters) {
		Query queryNamed = entityManager.createNamedQuery(query);
		if(parameters!=null){
			Set<String> keyParameters = parameters.keySet();
			for (String nameParameter : keyParameters) {
				queryNamed.setParameter(nameParameter, parameters.get(nameParameter));
			}
		}
		return queryNamed.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> executeQueryString(String query, Map<String, Object> parameters, Integer index, Integer maxItem) {
		Query queryNamed = entityManager.createNamedQuery(query, String.class);
		if (index!=null){
			queryNamed.setFirstResult(index);
			queryNamed.setMaxResults(maxItem);
		}
		if(parameters!=null){
			Set<String> keyParameters = parameters.keySet();
			for (String nameParameter : keyParameters) {
				queryNamed.setParameter(nameParameter, parameters.get(nameParameter));
			}
		}
		return queryNamed.getResultList();
	}	

	@SuppressWarnings({ "unchecked", "hiding" })
	public <Entity> Entity initializeAndUnproxy(Entity entity) {
	    if (entity == null) {
	        throw new 
	           NullPointerException("Entity passed for initialization is null");
	    }

	    Hibernate.initialize(entity);
	    if (entity instanceof HibernateProxy) {
	        entity = (Entity) ((HibernateProxy) entity).getHibernateLazyInitializer()
	                .getImplementation();
	    }
	    return entity;
	}		
}
