package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.ViewSpecialCommandDAO;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.ViewSpecialCommand;

@Repository
public class ViewSpecialCommandDAOImpl extends DAOImpl<ViewSpecialCommand> implements ViewSpecialCommandDAO {

	private static final long serialVersionUID = -6229210526582864557L;

	static Logger logger = Logger.getLogger(ViewSpecialCommandDAOImpl.class);

	public List<ViewSpecialCommand> findByTds(Tds tds) {
		logger.debug("ViewSpecialCommand.findByTds by:" + tds.getIdentifier());
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pTds", tds.getIdentifier());

		return executeQuery("ViewSpecialCommand.findByTds", parametros, null, null);
	}
}
