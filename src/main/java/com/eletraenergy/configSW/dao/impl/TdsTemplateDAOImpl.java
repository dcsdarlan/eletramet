package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.TdsTemplateDAO;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.TdsTemplate;

@Repository
public class TdsTemplateDAOImpl extends DAOImpl<TdsTemplate> implements TdsTemplateDAO {
	
	private static final long serialVersionUID = 1946215270055087556L;
	static Logger logger = Logger.getLogger(TdsTemplateDAOImpl.class);

    @Override
    public List<TdsTemplate> findAll() {
    	return executeQuery("TdsTemplate.findAll", null, null, null);
    }

    public List<TdsTemplate> findByModelOrderIdentifier(MeterModel meterModel) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pMeterModel", meterModel);
    	List<TdsTemplate> ret = null;
    	try {
        	ret = executeQuery("TdsTemplate.findByModelOrderIdentifier", parametros, null, null);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }
    
    @Override
    public void remove(TdsTemplate tdsTemplate) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",tdsTemplate.getId());
        executeCommand("TdsTemplate.remove", parameters);
    }	
    
    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("TdsTemplate.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
    
    public List<TdsTemplate> findByMeterModel(MeterModel meterModel) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
		parameters.put("pMeterModel", meterModel);
		return executeQuery("TdsTemplate.byMeterModel", parameters, null, null);
    }
    
    public TdsTemplate checkIdentifier(String identifier) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pIdentifier", identifier);
    	TdsTemplate ret = null;
    	try {
        	ret = executeQuery("TdsTemplate.checkIdentifier", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    @Override
 	public Criteria createCriteriaExample(TdsTemplate entidade, MatchMode match) {

 		Criteria criteria = super.createCriteriaExample(entidade, match);
 				
 		criteria = prepararCriteria(entidade, criteria);		
 		return criteria;
 	}

 	private Criteria prepararCriteria(TdsTemplate entidadeLocal, Criteria criteria) {
 		
 		if (entidadeLocal.getSortField() != null){
 			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
 				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
 			}else{
 				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
 			}
 		}
 		
 		Criteria a = null;
 		if (entidadeLocal.getMeterModel() != null) {
 			a = criteria.createCriteria("meterModel");
 			a.add(Restrictions.eq("id", entidadeLocal.getMeterModel().getId()));
 		}		

 		return criteria;
 	}	
}
