package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.SpecialCommandDAO;
import com.eletraenergy.configSW.model.SpecialCommand;

@Repository
public class SpecialCommandDAOImpl extends DAOImpl<SpecialCommand> implements SpecialCommandDAO {
	
	private static final long serialVersionUID = 7022271214282183364L;

	static Logger logger = Logger.getLogger(SpecialCommandDAOImpl.class);

    @Override
    public List<SpecialCommand> findAll() {
    	return executeQuery("SpecialCommand.findAll", null, null, null);
    }

    @Override
    public void remove(SpecialCommand spec) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",spec.getId());
        executeCommand("SpecialCommand.remove", parameters);
    }	
    
    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("SpecialCommand.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
}
