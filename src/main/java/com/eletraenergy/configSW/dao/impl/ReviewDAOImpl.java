package com.eletraenergy.configSW.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.ReviewDAO;
import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.Review;
import com.eletraenergy.configSW.model.SpecCategory;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.TdsTemplate;

@Repository
public class ReviewDAOImpl extends DAOImpl<Review> implements ReviewDAO {
	
	private static final long serialVersionUID = -4752698679037542036L;
	static Logger logger = Logger.getLogger(ReviewDAOImpl.class);

    public List<Review> findLast10() {
    	return executeQuery("Review.findLast10", null, 0, 11);
    }

	@SuppressWarnings("unchecked")
    public List<Object> findByReview(Review entidade){
    	List<Object> objects = new ArrayList<Object>();
    	//Iniciando o AuditReader
    	AuditReader auditReader = AuditReaderFactory.get(entityManager.getEntityManagerFactory().createEntityManager());
    	
    	//Recuperando as TDS
        AuditQuery query = auditReader.createQuery().forRevisionsOfEntity(Tds.class, false, true);
        query.add(AuditEntity.revisionNumber().eq(entidade.getId()));
        query.addOrder(AuditEntity.revisionNumber().desc());
        List<Object> listTds =  query.getResultList();
        if (!listTds.isEmpty()){
            Object[] object2 = (Object[]) listTds.get(0);
        	Tds tds = (Tds) object2[0];
        	logger.info(tds.getMeterModel().getName());
        	List<Number> revisions = auditReader.getRevisions(Tds.class, tds.getId());
        	query = auditReader.createQuery().forRevisionsOfEntity(Tds.class, false, true);
            query.add(AuditEntity.revisionNumber().in(revisions));
            query.addOrder(AuditEntity.property("id").desc());
            query.addOrder(AuditEntity.revisionProperty("dateAudit").desc());
            listTds =  query.getResultList();
        }

        //o index 0 ser� a TDS, vamos adicionar mesmo n�o tendo para poder identificar pelo index no MB.
		objects.add(listTds);

		//Recuperando as Configs
        query = auditReader.createQuery().forRevisionsOfEntity(
        		Config.class, false, true);
        query.add(AuditEntity.revisionNumber().eq(entidade.getId()));
        query.addOrder(AuditEntity.revisionNumber().desc());
        List<Object> listConfigs =  query.getResultList();
        if (!listConfigs.isEmpty()){
            Object[] object2 = (Object[]) listConfigs.get(0);
            Config config = (Config) object2[0];
        	List<Number> revisions = auditReader.getRevisions(Config.class, config.getId());
        	query = auditReader.createQuery().forRevisionsOfEntity(Tds.class, false, true);
            query.add(AuditEntity.revisionNumber().in(revisions));
            query.addOrder(AuditEntity.property("id").desc());
            query.addOrder(AuditEntity.revisionProperty("dateAudit").desc());
            listConfigs =  query.getResultList();
       }
        //o index 1 ser� a Config
		objects.add(listConfigs);
		
		//Recuperando as SpecCategory
        query = auditReader.createQuery().forRevisionsOfEntity(
        		SpecCategory.class, false, true);
        query.add(AuditEntity.revisionNumber().eq(entidade.getId()));
        query.addOrder(AuditEntity.revisionNumber().desc());
        List<Object> listSpecCategory =  query.getResultList();
        if (!listSpecCategory.isEmpty()){
            Object[] object2 = (Object[]) listSpecCategory.get(0);
            SpecCategory specCategory = (SpecCategory) object2[0];
        	List<Number> revisions = auditReader.getRevisions(SpecCategory.class, specCategory.getId());
        	query = auditReader.createQuery().forRevisionsOfEntity(SpecCategory.class, false, true);
            query.add(AuditEntity.revisionNumber().in(revisions));
            query.addOrder(AuditEntity.property("id").desc());
            query.addOrder(AuditEntity.revisionProperty("dateAudit").desc());
            listSpecCategory =  query.getResultList();
        }
        //o index 2 ser� a SpecCategory
		objects.add(listSpecCategory);

		//Recuperando as MeterLine
        query = auditReader.createQuery().forRevisionsOfEntity(
        		MeterLine.class, false, true);
        query.add(AuditEntity.revisionNumber().eq(entidade.getId()));
        query.addOrder(AuditEntity.revisionNumber().desc());
        List<Object> listMeterLine =  query.getResultList();
        if (!listMeterLine.isEmpty()){
            Object[] object2 = (Object[]) listMeterLine.get(0);
            MeterLine meterLine = (MeterLine) object2[0];
        	List<Number> revisions = auditReader.getRevisions(MeterLine.class, meterLine.getId());
        	query = auditReader.createQuery().forRevisionsOfEntity(MeterLine.class, false, true);
            query.add(AuditEntity.revisionNumber().in(revisions));
            query.addOrder(AuditEntity.property("id").desc());
            query.addOrder(AuditEntity.revisionProperty("dateAudit").desc());
            listMeterLine =  query.getResultList();
       }
        //o index 3 ser� a MeterLine
		objects.add(listMeterLine);

		//Recuperando as MeterModel
        query = auditReader.createQuery().forRevisionsOfEntity(
        		MeterModel.class, false, true);
        query.add(AuditEntity.revisionNumber().eq(entidade.getId()));
        query.addOrder(AuditEntity.revisionNumber().desc());
        List<Object> listMeterModel =  query.getResultList();
        if (!listMeterModel.isEmpty()){
            Object[] object2 = (Object[]) listMeterModel.get(0);
            MeterModel meterModel = (MeterModel) object2[0];
        	List<Number> revisions = auditReader.getRevisions(MeterModel.class, meterModel.getId());
        	query = auditReader.createQuery().forRevisionsOfEntity(MeterModel.class, false, true);
            query.add(AuditEntity.revisionNumber().in(revisions));
            query.addOrder(AuditEntity.property("id").desc());
            query.addOrder(AuditEntity.revisionProperty("dateAudit").desc());
            listMeterModel =  query.getResultList();
       }
        //o index 4 ser� a MeterModel
		objects.add(listMeterModel);

		//Recuperando as TdsTemplate
        query = auditReader.createQuery().forRevisionsOfEntity(
        		TdsTemplate.class, false, true);
        query.add(AuditEntity.revisionNumber().eq(entidade.getId()));
        query.addOrder(AuditEntity.revisionNumber().desc());
        List<Object> listTdsTemplate =  query.getResultList();
        if (!listTdsTemplate.isEmpty()){
            Object[] object2 = (Object[]) listTdsTemplate.get(0);
            TdsTemplate tdsTemplate = (TdsTemplate) object2[0];
        	List<Number> revisions = auditReader.getRevisions(TdsTemplate.class, tdsTemplate.getId());
        	query = auditReader.createQuery().forRevisionsOfEntity(TdsTemplate.class, false, true);
            query.add(AuditEntity.revisionNumber().in(revisions));
            query.addOrder(AuditEntity.property("id").desc());
            query.addOrder(AuditEntity.revisionProperty("dateAudit").desc());
            listTdsTemplate =  query.getResultList();
     }
       //o index 5 ser� a TdsTemplate
		objects.add(listTdsTemplate);

		return objects;
    }
    @Override
	public Criteria createCriteriaExample(Review entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Review entidadeLocal, Criteria criteria) {
		
		return criteria;
	}
	
	
}
