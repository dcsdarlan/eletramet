package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.SpecCategoryDAO;
import com.eletraenergy.configSW.model.SpecCategory;

@Repository
public class SpecCategoryDAOImpl extends DAOImpl<SpecCategory> implements SpecCategoryDAO {
	
	private static final long serialVersionUID = 1381748556786953418L;
	static Logger logger = Logger.getLogger(SpecCategoryDAOImpl.class);

    @Override
    public List<SpecCategory> findAll() {
    	return executeQuery("SpecCategory.findAll", null, null, null);
    }

	public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("SpecCategory.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
	
    public SpecCategory checkIdentifier(String identifier) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pIdentifier", identifier);
    	SpecCategory ret = null;
    	try {
        	ret = executeQuery("SpecCategory.checkIdentifier", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    public SpecCategory checkOrder(Integer order) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pOrder", order);
    	SpecCategory ret = null;
    	try {
        	ret = executeQuery("SpecCategory.checkOrder", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    @Override
	public Criteria createCriteriaExample(SpecCategory entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(SpecCategory entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}


		return criteria;
	}	
}
