package com.eletraenergy.configSW.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.ViewSpecDAO;
import com.eletraenergy.configSW.model.ViewSpec;

@Repository
public class ViewSpecDAOImpl extends DAOImpl<ViewSpec> implements ViewSpecDAO {

	private static final long serialVersionUID = -5164114419283278588L;
	static Logger logger = Logger.getLogger(ViewSpecDAOImpl.class);
	
	@Override
	public List<ViewSpec> findAll() {
		logger.debug("ViewSpecDAOImpl.findAll.");
		return executeQuery("ViewSpec.findAll", null, null, null);
	}
}
