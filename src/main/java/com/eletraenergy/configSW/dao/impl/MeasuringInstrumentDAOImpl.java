package com.eletraenergy.configSW.dao.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.MeasuringInstrumentDAO;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeasuringInstrument;
import com.eletraenergy.configSW.model.MeasuringInstrumentModel;
import com.eletraenergy.configSW.model.MeasuringInstrumentStandard;
import com.eletraenergy.configSW.model.MeasuringInstrumentStateIdentifier;

@Repository
public class MeasuringInstrumentDAOImpl extends DAOImpl<MeasuringInstrument> implements MeasuringInstrumentDAO {
	
	private static final long serialVersionUID = 1946215270055087556L;
	static Logger logger = Logger.getLogger(MeasuringInstrumentDAOImpl.class);

    @Override
    public List<MeasuringInstrument> findAll() {
    	return executeQuery("MeasuringInstrument.findAll", null, null, null);
    }

    @Override
    public void remove(MeasuringInstrument measuringInstrument) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",measuringInstrument.getId());
        executeCommand("MeasuringInstrument.remove", parameters);
    }	
    
    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("MeasuringInstrument.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		return singleResult==null?1:singleResult + 1;
	}
	
    public MeasuringInstrument checkSpecificationIdentifier(String specificationIdentifier) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pSpecificationIdentifier", specificationIdentifier);
    	MeasuringInstrument ret = null;
    	try {
        	ret = executeQuery("MeasuringInstrument.checkSpecificationIdentifier", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    public List<MeasuringInstrument> findByModel(MeasuringInstrumentModel measuringInstrumentModel) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pMeasuringInstrumentModel", measuringInstrumentModel);
    	List<MeasuringInstrument> ret = null;
    	try {
        	ret = executeQuery("MeasuringInstrument.findByModel", parametros, null, null);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    public List<MeasuringInstrument> findByLine(MeterLine meterLine) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pMeterLine", meterLine);
    	List<MeasuringInstrument> ret = null;
    	try {
        	ret = executeQuery("MeasuringInstrument.findByLine", parametros, null, null);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    @Override
	public Criteria createCriteriaExample(MeasuringInstrument entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(MeasuringInstrument entidadeLocal, Criteria criteria) {
		
		 if (entidadeLocal.getSortField() != null){
			 if (entidadeLocal.getSortField().equals("measuringInstrumentModel.measuringInstrumentManufacturer.name")){
		 
				 Criteria a = criteria.createCriteria("measuringInstrumentModel");
				 Criteria b = a.createCriteria("measuringInstrumentManufacturer");
				 
				 if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
					 b.addOrder(Order.desc(entidadeLocal.getSortField().split("\\.")[2]));
				 }
				 else
				 {
					 b.addOrder(Order.asc(entidadeLocal.getSortField().split("\\.")[2]));
				 }
				 
			 } else {	
				 if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
					 criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
				 } else{
					 criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
				 }
			 }
		  }		
		
		 if (entidadeLocal.getId() != null) {
			 criteria.add(Restrictions.eq("id", entidadeLocal.getId()));
		 }
		 
		 if ((entidadeLocal.getNextCalibrationDateFilterFrom() != null) &&
				 (entidadeLocal.getNextCalibrationDateFilterTo() != null)) {
			 criteria.
					add(Restrictions.between("nextCalibrationDate", entidadeLocal.getNextCalibrationDateFilterFrom(),
							entidadeLocal.getNextCalibrationDateFilterTo()));
		 } else if ((entidadeLocal.getNextCalibrationDateFilterFrom() != null) &&
					 (entidadeLocal.getNextCalibrationDateFilterTo() == null)) {
				 criteria.
						add(Restrictions.ge("nextCalibrationDate", entidadeLocal.getNextCalibrationDateFilterFrom()));
		 		} else if ((entidadeLocal.getNextCalibrationDateFilterFrom() == null) &&
						 (entidadeLocal.getNextCalibrationDateFilterTo() != null)) {
						 criteria.
								add(Restrictions.lt("nextCalibrationDate", entidadeLocal.getNextCalibrationDateFilterTo()));
		 				}
		 
 		 if (entidadeLocal.getMeasuringInstrumentType() != null) {
 			criteria.
 				createCriteria("measuringInstrumentType").
 					add(Restrictions.eq("id", entidadeLocal.getMeasuringInstrumentType().getId()));
		 }		
 		 
 		 if (entidadeLocal.getMeasuringInstrumentPlaceUse() != null) {
 			criteria.
 				createCriteria("measuringInstrumentPlaceUse").
 					add(Restrictions.eq("id", entidadeLocal.getMeasuringInstrumentPlaceUse().getId()));
 		 }
 		 
 		 if (entidadeLocal.getMeasuringInstrumentStandards() != null &&
 				 entidadeLocal.getMeasuringInstrumentStandards().size() > 0) {
 			 
 			 List<Long> standardsIds = new ArrayList<Long>();
 			 
 			 for (MeasuringInstrumentStandard measuringInstrumentStandard : entidadeLocal.getMeasuringInstrumentStandards()){
 				standardsIds.add(measuringInstrumentStandard.getId());
 			 }
 			 	
 			criteria.
			 	createCriteria("measuringInstrumentStandards").
			 	add(Restrictions.in("id", standardsIds));
 		 }
 		 
 		 if ((entidadeLocal.getMeasuringInstrumentManufacturer() != null) && 
 				 (entidadeLocal.getMeasuringInstrumentModel() == null)  ) {
 			criteria.
				createCriteria("measuringInstrumentModel").
				createCriteria("measuringInstrumentManufacturer").
					add(Restrictions.eq("id", entidadeLocal.getMeasuringInstrumentManufacturer().getId()));
 			
 		 } else if ((entidadeLocal.getMeasuringInstrumentManufacturer() != null) && 
	 				 (entidadeLocal.getMeasuringInstrumentModel() != null)  ) {
		 			 	criteria.
				 			createCriteria("measuringInstrumentModel").
				 				add(Restrictions.eq("id", entidadeLocal.getMeasuringInstrumentModel().getId())).
							createCriteria("measuringInstrumentManufacturer").
								add(Restrictions.eq("id", entidadeLocal.getMeasuringInstrumentManufacturer().getId()));
			 		 }
		 
		return criteria;
	}

	@Override
	public List<MeasuringInstrument> findOverdueCalibrations() {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	
    	parametros.put("pNextCalibrationDate", null);
    	List<MeasuringInstrument> ret = null;
    	try {
        	ret = executeQuery("MeasuringInstrument.findOverdueCalibrations", parametros, null, null);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
	}

	@Override
	public List<MeasuringInstrument> findDelayInTheNext90Days() {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	
    	parametros.put("pNextCalibrationDateStart", null);
    	parametros.put("pNextCalibrationDateFinish", null);
    	
    	List<MeasuringInstrument> ret = null;
    	try {
        	ret = executeQuery("MeasuringInstrument.findDelayInTheNext90Days", parametros, null, null);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
	}

	@Override
	public List<MeasuringInstrument> findInCalibration() {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pMeasuringInstrumentStateIdentifier", MeasuringInstrumentStateIdentifier.inCalibration);
    	List<MeasuringInstrument> ret = null;
    	try {
        	ret = executeQuery("MeasuringInstrument.findInCalibration", parametros, null, null);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
	}	
}
