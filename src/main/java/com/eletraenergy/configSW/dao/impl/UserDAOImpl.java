package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import com.eletraenergy.configSW.model.CalibrationLaboratory;
import com.eletraenergy.configSW.model.CalibrationLaboratoryTelphone;
import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.UserDAO;
import com.eletraenergy.configSW.model.User;

@Repository
public class UserDAOImpl extends DAOImpl<User> implements UserDAO {
	
	private static final long serialVersionUID = 3515822517075460426L;
	static Logger logger = Logger.getLogger(UserDAOImpl.class);

	public User findByUsername(String username) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pUsername", username);

		return executeQuery("User.findByUsername", parametros);
	}

	public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("User.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
	
	@Override
	public Criteria createCriteriaExample(User entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(User entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}


		return criteria;
	}

	@Override
	public List<User> findAll() {
		return executeQuery("User.findAll", null, null, null);
	}



}
