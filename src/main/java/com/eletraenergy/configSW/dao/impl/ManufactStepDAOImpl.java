package com.eletraenergy.configSW.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.ManufactStepDAO;
import com.eletraenergy.configSW.model.ManufactStep;

@Repository
public class ManufactStepDAOImpl extends DAOImpl<ManufactStep> implements ManufactStepDAO {
	
	private static final long serialVersionUID = 2464256884597775977L;
	static Logger logger = Logger.getLogger(ManufactStepDAOImpl.class);

    @Override
    public List<ManufactStep> findAll() {
    	return executeQuery("ManufactStep.findAll", null, null, null);
    }

}
