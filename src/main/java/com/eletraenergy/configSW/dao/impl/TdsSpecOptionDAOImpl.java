package com.eletraenergy.configSW.dao.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.TdsSpecOptionDAO;
import com.eletraenergy.configSW.model.TdsSpec;
import com.eletraenergy.configSW.model.TdsSpecOption;

@Repository
public class TdsSpecOptionDAOImpl extends DAOImpl<TdsSpecOption> implements TdsSpecOptionDAO {
	
	private static final long serialVersionUID = 233879819677309176L;
	static Logger logger = Logger.getLogger(TdsSpecOptionDAOImpl.class);

    @Override
    public List<TdsSpecOption> findAll() {
    	return executeQuery("TdsSpecOption.findAll", null, null, null);
    }

    public TdsSpecOption findByName(String name, String specName) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pName", name);
    	parametros.put("pSpecName", specName);
    	TdsSpecOption ret = null;
    	try {
        	ret = executeQuery("TdsSpecOption.findByNameBySpecName", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }
    
    @Override
    public void remove(TdsSpecOption specOption) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",specOption.getId());
        executeCommand("TdsSpecOption.remove", parameters);
    }	
    
    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("TdsSpecOption.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
	
    public TdsSpecOption checkIdentifier(String identifier) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pIdentifier", identifier);
    	TdsSpecOption ret = null;
    	try {
        	ret = executeQuery("TdsSpecOption.checkIdentifier", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }
    
    @Override
	public Criteria createCriteriaExample(TdsSpecOption entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(TdsSpecOption entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}

		Criteria a = null;
		if (entidadeLocal.getTdsSpec() != null) {
			a = criteria.createCriteria("tdsSpec");
			a.add(Restrictions.eq("id", entidadeLocal.getTdsSpec().getId()));
		}		

		return criteria;
	}
}
