package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.MeasuringInstrumentModelDAO;
import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;
import com.eletraenergy.configSW.model.MeasuringInstrumentModel;

@Repository
public class MeasuringInstrumentModelDAOImpl extends DAOImpl<MeasuringInstrumentModel> implements MeasuringInstrumentModelDAO {
	
	private static final long serialVersionUID = -3860740303255846167L;
	static Logger logger = Logger.getLogger(MeasuringInstrumentModelDAOImpl.class);

	public List<MeasuringInstrumentModel> findByManufacturer(MeasuringInstrumentManufacturer measuringInstrumentManufacturer) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pMeasuringInstrumentManufacturer", measuringInstrumentManufacturer);
		return executeQuery("MeasuringInstrumentModel.byManufacturer", parametros, null, null);
	}
	
    @Override
    public void remove(MeasuringInstrumentModel measuringInstrumentModel) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",measuringInstrumentModel.getId());
        executeCommand("MeasuringInstrumentModel.remove", parameters);
    }	
    
    public MeasuringInstrumentModel checkName(String name) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pName", name);
    	MeasuringInstrumentModel ret = null;
    	try {
        	ret = executeQuery("MeasuringInstrumentModel.checkName", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    @Override
    public List<MeasuringInstrumentModel> findAll() {
    	return executeQuery("MeasuringInstrumentModel.findAll", null, null, null);
    }

	public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("MeasuringInstrumentModel.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
	
	@Override
	public Criteria createCriteriaExample(MeasuringInstrumentModel entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(MeasuringInstrumentModel entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}

		Criteria a = null;
		if (entidadeLocal.getMeasuringInstrumentManufacturer() != null) {
			a = criteria.createCriteria("measuringInstrumentManufacturer");
			a.add(Restrictions.eq("id", entidadeLocal.getMeasuringInstrumentManufacturer().getId()));
		}		
		return criteria;
	}	
}
