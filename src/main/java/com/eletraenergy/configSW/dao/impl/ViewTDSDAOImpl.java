package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import com.eletraenergy.configSW.dao.ViewTDSDAO;
import com.eletraenergy.configSW.model.FilterModel;
import com.eletraenergy.configSW.model.ViewTDS;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.TdsStateIdentifier;

@Repository
public class ViewTDSDAOImpl extends DAOImpl<ViewTDS> implements ViewTDSDAO {
	
	private static final long serialVersionUID = 5029532018166647406L;

	static Logger logger = Logger.getLogger(ViewTDSDAOImpl.class);

	@SuppressWarnings("unchecked")
	public List<ViewTDS> findConfig(MeterLine meterLine, MeterModel meterModel, String tds, List<FilterModel> validFilters, TdsStateIdentifier state) {
		logger.debug("ViewConfig.findAll");
		
		//pegr apenas as ativas
		StringBuilder query = new StringBuilder("SELECT NEW ViewTDS(vw_tds.state, vw_tds.id, vw_tds.tdsNumber, vw_tds.meterLineName, vw_tds.meterModelName, vw_tds.tdsSpecName, vw_tds.tdsSpecOptionName, vw_tds.configCode) FROM ViewTDS vw_tds WHERE 1=1 ");
		
		if (tds != null && !tds.isEmpty()){
			query.append(" AND vw_tds.tdsNumber = '" + tds +"'");
		}else{
			if (state != null) {
				query.append(" AND vw_tds.state = '" + state.getId() + "'");
			}
			if (meterLine != null){
				query.append(" AND vw_tds.meterLineIdentifier = '" + meterLine.getIdentifier() +"'");
			}
			if (meterModel != null){
				query.append(" AND vw_tds.meterModelIdentifier = '" + meterModel.getIdentifier() +"'");
			}
			StringBuilder queryCondition = new StringBuilder();
			StringBuilder queryNotIn = new StringBuilder();
			if (!validFilters.isEmpty()){
				queryNotIn.append(" AND vw_tds.tdsSpecName not in (");
				
				for (FilterModel filterModel : validFilters) {
					queryNotIn.append("'" + filterModel.getHeader() + "',");
					queryCondition.append(" AND EXISTS (SELECT 1 FROM ViewTDS tds_sub WHERE tds_sub.tdsNumber = vw_tds.tdsNumber and tds_sub.tdsSpecName = '" + filterModel.getHeader() + "' AND tds_sub.tdsSpecOptionName = '" + filterModel.getValue() + "')");
				}
				queryNotIn.deleteCharAt((queryNotIn.length()-1));
				queryNotIn.append(")");
				queryNotIn.append(queryCondition);
				query.append(queryNotIn);
			}
		}
		List<ViewTDS> resultList = entityManager.createQuery(query.toString()).getResultList();
		return resultList;
	}
}
