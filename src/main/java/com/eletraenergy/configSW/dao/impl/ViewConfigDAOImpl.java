package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import com.eletraenergy.configSW.dao.ViewConfigDAO;
import com.eletraenergy.configSW.model.FilterModel;
import com.eletraenergy.configSW.model.ViewConfig;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;

@Repository
public class ViewConfigDAOImpl extends DAOImpl<ViewConfig> implements ViewConfigDAO {

	private static final long serialVersionUID = 5029532018166647406L;

	static Logger logger = Logger.getLogger(ViewConfigDAOImpl.class);

	@SuppressWarnings("unchecked")
	public List<ViewConfig> findConfig(MeterLine meterLine, MeterModel meterModel, String tds, List<FilterModel> validFilters) {
		logger.debug("ViewConfig.findAll");
		
		StringBuilder query = new StringBuilder("SELECT NEW ViewConfig(vw_config.tdsNumber, vw_config.meterLineName, vw_config.meterModelName, vw_config.specName, vw_config.specOptionName, vw_config.configCode) FROM ViewConfig vw_config WHERE vw_config.specCategoryIdentifier = 'TDS'");
		
		if (tds != null && !tds.isEmpty()){
			query.append(" AND vw_config.tdsNumber = '" + tds +"'");
		}else{
			if (meterLine != null){
				query.append(" AND vw_config.meterLineIdentifier = '" + meterLine.getIdentifier() +"'");
			}
			if (meterModel != null){
				query.append(" AND vw_config.meterModelIdentifier = '" + meterModel.getIdentifier() +"'");
			}
			StringBuilder queryCondition = new StringBuilder();
			StringBuilder queryNotIn = new StringBuilder();
			if (!validFilters.isEmpty()){
				queryNotIn.append(" AND vw_config.specName not in (");
				for (FilterModel filterModel : validFilters) {
					queryNotIn.append("'" + filterModel.getHeader() + "',");
					queryCondition.append(" AND EXISTS (SELECT 1 FROM ViewConfig config_sub WHERE config_sub.tdsNumber = vw_config.tdsNumber and config_sub.specName = '" + filterModel.getHeader() + "' AND config_sub.specOptionName = '" + filterModel.getValue() + "')");
				}
				queryNotIn.deleteCharAt((queryNotIn.length()-1));
				queryNotIn.append(")");
				queryNotIn.append(queryCondition);
				query.append(queryNotIn);
			}
		}
		List<ViewConfig> resultList = entityManager.createQuery(query.toString()).getResultList();
		return resultList;
	}
}
