package com.eletraenergy.configSW.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import com.eletraenergy.configSW.dao.ViewTdsAudDAO;
import com.eletraenergy.configSW.model.ViewTdsAud;

@Repository
public class ViewTDSAudDAOImpl extends DAOImpl<ViewTdsAud> implements ViewTdsAudDAO {
	
	private static final long serialVersionUID = 5029532018166647406L;

	static Logger logger = Logger.getLogger(ViewTDSAudDAOImpl.class);

	@Override
	public List<ViewTdsAud> findAll() {
		return executeQuery("ViewTdsAud.findAll", null, null, null);
	}
}
