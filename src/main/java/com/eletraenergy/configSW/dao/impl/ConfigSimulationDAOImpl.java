package com.eletraenergy.configSW.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.ConfigSimulationDAO;
import com.eletraenergy.configSW.model.ConfigSimulation;

@Repository
public class ConfigSimulationDAOImpl extends DAOImpl<ConfigSimulation> implements ConfigSimulationDAO {
	
	private static final long serialVersionUID = 3695983387105305801L;
	static Logger logger = Logger.getLogger(ConfigSimulationDAOImpl.class);

    @Override
    public List<ConfigSimulation> findAll() {
    	return executeQuery("ConfigSimulation.findAll", null, null, null);
    }

    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("ConfigSimulation.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}

	@Override
	public Criteria createCriteriaExample(ConfigSimulation entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(ConfigSimulation entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}

		return criteria;
	}	
}
