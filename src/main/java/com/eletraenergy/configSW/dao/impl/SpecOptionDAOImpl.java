package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.SpecOptionDAO;
import com.eletraenergy.configSW.model.Spec;
import com.eletraenergy.configSW.model.SpecOption;

@Repository
public class SpecOptionDAOImpl extends DAOImpl<SpecOption> implements SpecOptionDAO {
	
	private static final long serialVersionUID = -9159678424246613013L;
	static Logger logger = Logger.getLogger(SpecOptionDAOImpl.class);

    @Override
    public List<SpecOption> findAll() {
    	return executeQuery("SpecOption.findAll", null, null, null);
    }

    public List<SpecOption> findAllBySpec(Spec spec) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pSpec",spec);

    	return executeQuery("SpecOption.findAllBySpec", parameters, null, null);
    }

    @Override
    public void remove(SpecOption specOption) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",specOption.getId());
        executeCommand("SpecOption.remove", parameters);
    }	
    
    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("SpecOption.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
	
    public SpecOption checkIdentifier(String identifier) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pIdentifier", identifier);
    	SpecOption ret = null;
    	try {
        	ret = executeQuery("SpecOption.checkIdentifier", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }
    
    @Override
	public Criteria createCriteriaExample(SpecOption entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(SpecOption entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}

		Criteria a = null;
		if (entidadeLocal.getSpec() != null) {
			a = criteria.createCriteria("spec");
			a.add(Restrictions.eq("id", entidadeLocal.getSpec().getId()));
		} else if (entidadeLocal.getSpecCategory() != null) {
			a = criteria.createCriteria("spec");
			Criteria b = a.createCriteria("specCategory");
			b.add(Restrictions.eq("id", entidadeLocal.getSpecCategory().getId()));
		}

		return criteria;
	}	
}
