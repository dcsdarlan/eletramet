package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.TdsDAO;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.TdsSpecOption;
import com.eletraenergy.configSW.model.TdsStateIdentifier;
import com.eletraenergy.configSW.model.TdsTemplate;

@Repository
public class TdsDAOImpl extends DAOImpl<Tds> implements TdsDAO {
	
	private static final long serialVersionUID = 1946215270055087556L;
	static Logger logger = Logger.getLogger(TdsDAOImpl.class);

    @Override
    public List<Tds> findAll() {
    	return executeQuery("Tds.findAll", null, null, null);
    }

    @Override
    public void remove(Tds tds) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",tds.getId());
        executeCommand("Tds.remove", parameters);
    }	
    
    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("Tds.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
	
    public Tds checkIdentifier(String identifier) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pIdentifier", identifier);
    	Tds ret = null;
    	try {
        	ret = executeQuery("Tds.checkIdentifier", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    public Tds checkTemplateOption(TdsTemplate template, TdsSpecOption option) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pTemplate", template);
    	parametros.put("pOption", option);
    	Tds ret = null;
    	try {
        	ret = executeQuery("Tds.checkTemplateOption", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }
    
    public List<Tds> findByModel(MeterModel meterModel) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pMeterModel", meterModel);
    	parametros.put("pState", TdsStateIdentifier.e);
    	List<Tds> ret = null;
    	try {
        	ret = executeQuery("Tds.findByModel", parametros, null, null);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    public List<Tds> findByLine(MeterLine meterLine) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pMeterLine", meterLine);
    	List<Tds> ret = null;
    	try {
        	ret = executeQuery("Tds.findByLine", parametros, null, null);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    public List<Tds> findByModelOrderIdentifier(MeterModel meterModel) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pMeterModel", meterModel);
    	List<Tds> ret = null;
    	try {
        	ret = executeQuery("Tds.findByModelOrderIdentifier", parametros, null, null);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    @Override
	public Criteria createCriteriaExample(Tds entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Tds entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}
		
		Criteria a = null;
		if (entidadeLocal.getMeterModel() != null) {
			a = criteria.createCriteria("meterModel");
			a.add(Restrictions.eq("id", entidadeLocal.getMeterModel().getId()));
		}		

		return criteria;
	}	
}
