package com.eletraenergy.configSW.dao.impl;

import com.eletraenergy.configSW.dao.CalibrationLaboratoryDAO;
import com.eletraenergy.configSW.dao.ConfigDAO;
import com.eletraenergy.configSW.model.CalibrationLaboratory;
import com.eletraenergy.configSW.model.Config;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class CalibrationLaboratoryDaoImp extends DAOImpl<CalibrationLaboratory> implements CalibrationLaboratoryDAO {
    @Override
    public Long findLastId() {
        Query queryNamed = entityManager.createNamedQuery("CalibrationLaboratory.lastId");
        Long singleResult =(Long) queryNamed.getSingleResult();
        return singleResult==null?1:singleResult + 1;
    }

}
