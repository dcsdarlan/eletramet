package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.MeasuringInstrumentManufacturerDAO;
import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;

@Repository
public class MeasuringInstrumentManufacturerDAOImpl extends DAOImpl<MeasuringInstrumentManufacturer> implements MeasuringInstrumentManufacturerDAO {
	
	private static final long serialVersionUID = -2693835078802970511L;
	static Logger logger = Logger.getLogger(MeasuringInstrumentManufacturerDAOImpl.class);

    @Override
    public List<MeasuringInstrumentManufacturer> findAll() {
    	return executeQuery("MeasuringInstrumentManufacturer.findAll", null, null, null);
    }

	public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("MeasuringInstrumentManufacturer.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}

	@Override
	public Criteria createCriteriaExample(MeasuringInstrumentManufacturer entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(MeasuringInstrumentManufacturer entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}

		return criteria;
	}

	@Override
	public MeasuringInstrumentManufacturer checkName(String name) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pName", name);
    	MeasuringInstrumentManufacturer ret = null;
    	try {
        	ret = executeQuery("MeasuringInstrumentManufacturer.checkName", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
	}
	
}
