package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.SpecRuleDomainDAO;
import com.eletraenergy.configSW.model.SpecRuleDomain;

@Repository
public class SpecRuleDomainDAOImpl extends DAOImpl<SpecRuleDomain> implements SpecRuleDomainDAO {
	
	private static final long serialVersionUID = -8679019849950624839L;
	static Logger logger = Logger.getLogger(SpecRuleDomainDAOImpl.class);

    @Override
    public List<SpecRuleDomain> findAll() {
    	return executeQuery("SpecRuleDomain.findAll", null, null, null);
    }

    public List<SpecRuleDomain> childs(SpecRuleDomain parent) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pParent", parent);

    	return executeQuery("SpecRuleDomain.childs", parametros, null, null); 
    }

    @Override
    public void remove(SpecRuleDomain spec) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",spec.getId());
        executeCommand("SpecRuleDomain.remove", parameters);
    }	
    
    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("SpecRuleDomain.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
	
    public SpecRuleDomain checkIdentifier(String identifier) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pIdentifier", identifier);
    	SpecRuleDomain ret = null;
    	try {
        	ret = executeQuery("SpecRuleDomain.checkIdentifier", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

}
