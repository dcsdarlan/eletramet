package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.SpecRuleDAO;
import com.eletraenergy.configSW.model.SpecRule;

@Repository
public class SpecRuleDAOImpl extends DAOImpl<SpecRule> implements SpecRuleDAO {
	
	private static final long serialVersionUID = -5902288491380124244L;

	static Logger logger = Logger.getLogger(SpecRuleDAOImpl.class);

    @Override
    public List<SpecRule> findAll() {
    	return executeQuery("SpecRule.findAll", null, null, null);
    }

    @Override
    public void remove(SpecRule spec) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",spec.getId());
        executeCommand("SpecRule.remove", parameters);
    }	
    
    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("SpecRule.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
	
    public SpecRule checkIdentifier(String identifier) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pIdentifier", identifier);
    	SpecRule ret = null;
    	try {
        	ret = executeQuery("SpecRule.checkIdentifier", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

}
