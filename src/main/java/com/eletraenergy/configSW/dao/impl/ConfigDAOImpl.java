package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.ConfigDAO;
import com.eletraenergy.configSW.model.Config;

@Repository
public class ConfigDAOImpl extends DAOImpl<Config> implements ConfigDAO {
	
	private static final long serialVersionUID = 5578313626313424594L;
	static Logger logger = Logger.getLogger(ConfigDAOImpl.class);

    @Override
    public List<Config> findAll() {
    	return executeQuery("Config.findAll", null, null, null);
    }

    public List<Config> findById(List<Long> id) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",id);
    	return executeQuery("Config.findById", parameters, null, null);
    }

    @Override
    public void remove(Config tds) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",tds.getId());
        executeCommand("Config.remove", parameters);
    }	
    
    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("Config.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
	
    @Override
	public Criteria createCriteriaExample(Config entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Config entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}
		
		Criteria a = null;
		if (entidadeLocal.getTds() != null) {
			a = criteria.createCriteria("tds");
			a.add(Restrictions.eq("id", entidadeLocal.getTds().getId()));
		}else if (entidadeLocal.getMeterModel() != null) {
			a = criteria.createCriteria("tds");
			Criteria b = a.createCriteria("meterModel");
			b.add(Restrictions.eq("id", entidadeLocal.getMeterModel().getId()));
		}else if (entidadeLocal.getMeterLine() != null) {
			a = criteria.createCriteria("tds");
			Criteria b = a.createCriteria("meterModel");
			Criteria c = b.createCriteria("meterLine");
			c.add(Restrictions.eq("id", entidadeLocal.getMeterLine().getId()));
		}

		return criteria;
	}	
}
