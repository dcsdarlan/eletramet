package com.eletraenergy.configSW.dao.impl;

import com.eletraenergy.configSW.dao.MeasuringInstrumentDepartmentDAO;
import com.eletraenergy.configSW.model.MeasuringInstrumentDepartment;
import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

@Repository
public class MeasuringInstrumentDepartmentDAOImpl extends DAOImpl<MeasuringInstrumentDepartment> implements MeasuringInstrumentDepartmentDAO {
	
	private static final long serialVersionUID = -2693835078802970511L;
	static Logger logger = Logger.getLogger(MeasuringInstrumentDepartmentDAOImpl.class);

    @Override
    public List<MeasuringInstrumentDepartment> findAll() {
    	return executeQuery("MeasuringInstrumentDepartment.findAll", null, null, null);
    }

	public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("MeasuringInstrumentDepartment.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}

	@Override
	public Criteria createCriteriaExample(MeasuringInstrumentDepartment entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(MeasuringInstrumentDepartment entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}

		return criteria;
	}

	@Override
	public MeasuringInstrumentDepartment checkName(String name) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pName", name);
    	MeasuringInstrumentDepartment ret = null;
    	try {
        	ret = executeQuery("MeasuringInstrumentDepartment.checkName", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
	}
	
}
