package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;
import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import com.eletraenergy.configSW.dao.ViewTDSSpecDAO;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.ViewTDSSpec;

@Repository
public class ViewTDSSpecDAOImpl extends DAOImpl<ViewTDSSpec> implements ViewTDSSpecDAO {

	private static final long serialVersionUID = -5481391855147107395L;

	static Logger logger = Logger.getLogger(ViewTDSSpecDAOImpl.class);

	public List<ViewTDSSpec> findByLine(MeterLine meterLine) {
		logger.debug("ViewTDSSpecDAOImpl.findByLine by:" + meterLine);
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pMeterLine", meterLine.getIdentifier());

		return executeQuery("ViewTDSSpec.byLine", parametros, null, null);
	}

	public List<ViewTDSSpec> findByLineAndModel(MeterLine meterLine, MeterModel meterModel) {
		logger.debug("ViewTDSSpecDAOImpl.findByLineandModel by meterLine:" + meterLine + " and meterModel:" + meterModel);
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pMeterLine", meterLine.getIdentifier());
		parametros.put("pMeterModel", meterModel.getIdentifier());

		return executeQuery("ViewTDSSpec.byLineAndModel", parametros, null, null);
	}

	public List<ViewTDSSpec> findAll() {
		logger.debug("ViewTDSSpecDAOImpl.findAll.");
		return executeQuery("ViewTDSSpec.findAll", null, null, null);
	}
}
