package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.ConfigSpecRuleDomainDAO;
import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.ConfigSpecRuleDomain;

@Repository
public class ConfigSpecRuleDomainDAOImpl extends DAOImpl<ConfigSpecRuleDomain> implements ConfigSpecRuleDomainDAO {
	
	private static final long serialVersionUID = -8698035170471618059L;
	static Logger logger = Logger.getLogger(ConfigSpecRuleDomainDAOImpl.class);

    @Override
    public List<ConfigSpecRuleDomain> findAll() {
    	return executeQuery("ConfigSpecRuleDomain.findAll", null, null, null);
    }

    public List<ConfigSpecRuleDomain> findByConfig(Long specCode) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pConfigCode",specCode);
    	return executeQuery("ConfigSpecRuleDomain.findByConfig", parameters, null, null);
    }

    public void remove(Config config) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pConfigCode",config.getId());
        executeCommand("ConfigSpecRuleDomain.remove", parameters);
    }	
    
}
