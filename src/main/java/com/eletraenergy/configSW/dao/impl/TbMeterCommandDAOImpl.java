package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.TbMeterCommandDAO;
import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.TbMeterCommand;

@Repository
public class TbMeterCommandDAOImpl extends DAOImpl<TbMeterCommand> implements TbMeterCommandDAO {
	
	private static final long serialVersionUID = 8058989530080167991L;

	static Logger logger = Logger.getLogger(TbMeterCommandDAOImpl.class);

    @Override
    public List<TbMeterCommand> findAll() {
    	return executeQuery("TbMeterCommand.findAll", null, null, null);
    }

    @Override
    public void remove(TbMeterCommand spec) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",spec.getId());
        executeCommand("TbMeterCommand.remove", parameters);
    }	
    
    public List<TbMeterCommand> findByConfig(Config config) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pConfig", config);
    	return executeQuery("TbMeterCommand.findByConfig", parameters, null, null);
    }

    public List<TbMeterCommand> findByConfigAndOption(Config config, Long option) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pConfig", config);
    	parameters.put("pOption", option);
    	return executeQuery("TbMeterCommand.findByConfigAndOption", parameters, null, null);
    }

    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("TbMeterCommand.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
}
