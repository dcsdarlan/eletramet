package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.SpecDAO;
import com.eletraenergy.configSW.model.Spec;
import com.eletraenergy.configSW.model.SpecCategory;

@Repository
public class SpecDAOImpl extends DAOImpl<Spec> implements SpecDAO {
	
	private static final long serialVersionUID = 3076348112149750710L;
	static Logger logger = Logger.getLogger(SpecDAOImpl.class);

    @Override
    public List<Spec> findAll() {
    	return executeQuery("Spec.findAll", null, null, null);
    }

    public List<Spec> findBySpecCategory(SpecCategory specCategory) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pSpecCategory", specCategory);
    	return executeQuery("Spec.findByCategory", parametros, null, null);
    }

    @Override
    public void remove(Spec spec) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",spec.getId());
        executeCommand("Spec.remove", parameters);
    }	
    
    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("Spec.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
	
    public Spec checkIdentifier(String identifier) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pIdentifier", identifier);
    	Spec ret = null;
    	try {
        	ret = executeQuery("Spec.checkIdentifier", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    public Spec checkOrder(Integer order, SpecCategory specCategory) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pOrder", order);
    	parametros.put("pSpecCategory", specCategory);
    	Spec ret = null;
    	try {
        	ret = executeQuery("Spec.checkOrder", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    @Override
	public Criteria createCriteriaExample(Spec entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Spec entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}
		
		Criteria a = null;
		if (entidadeLocal.getSpecCategory() != null) {
			a = criteria.createCriteria("specCategory");
			a.add(Restrictions.eq("id", entidadeLocal.getSpecCategory().getId()));
		}		


		return criteria;
	}	
}
