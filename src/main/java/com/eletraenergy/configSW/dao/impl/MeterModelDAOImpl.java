package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.MeterModelDAO;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;

@Repository
public class MeterModelDAOImpl extends DAOImpl<MeterModel> implements MeterModelDAO {
	
	private static final long serialVersionUID = -3860740303255846167L;
	static Logger logger = Logger.getLogger(MeterModelDAOImpl.class);

	public List<MeterModel> findByLine(MeterLine meterLine) {
		logger.debug("findByLine by:" + meterLine);
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pMeterLine", meterLine);

		return executeQuery("MeterModel.byLine", parametros, null, null);
	}
	
    @Override
    public void remove(MeterModel meterModel) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pId",meterModel.getId());
        executeCommand("MeterModel.remove", parameters);
    }	
    
    public MeterModel checkIdentifier(String identifier) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pIdentifier", identifier);
    	MeterModel ret = null;
    	try {
        	ret = executeQuery("MeterModel.checkIdentifier", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }

    @Override
    public List<MeterModel> findAll() {
    	return executeQuery("MeterModel.findAll", null, null, null);
    }

	public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("MeterModel.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
	
	@Override
	public Criteria createCriteriaExample(MeterModel entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(MeterModel entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}

		Criteria a = null;
		if (entidadeLocal.getMeterLine() != null) {
			a = criteria.createCriteria("meterLine");
			a.add(Restrictions.eq("id", entidadeLocal.getMeterLine().getId()));
		}		
		return criteria;
	}	
}
