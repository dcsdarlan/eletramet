package com.eletraenergy.configSW.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.FunctionalityDAO;
import com.eletraenergy.configSW.model.Functionality;

@Repository
public class FunctionalityDAOImpl extends DAOImpl<Functionality> implements FunctionalityDAO {

	private static final long serialVersionUID = 7832542278265773518L;
	static Logger logger = Logger.getLogger(FunctionalityDAOImpl.class);

    @Override
    public List<Functionality> findAll() {
    	return executeQuery("Functionality.all", null, null, null);
    }	
}
