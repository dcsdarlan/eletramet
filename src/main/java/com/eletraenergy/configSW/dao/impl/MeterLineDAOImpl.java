package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.MeterLineDAO;
import com.eletraenergy.configSW.model.MeterLine;

@Repository
public class MeterLineDAOImpl extends DAOImpl<MeterLine> implements MeterLineDAO {
	
	private static final long serialVersionUID = -2693835078802970511L;
	static Logger logger = Logger.getLogger(MeterLineDAOImpl.class);

    @Override
    public List<MeterLine> findAll() {
    	return executeQuery("MeterLine.findAll", null, null, null);
    }

	public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("MeterLine.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}

    public MeterLine checkIdentifier(String identifier) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pIdentifier", identifier);
    	MeterLine ret = null;
    	try {
        	ret = executeQuery("MeterLine.checkIdentifier", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
    }
    
	@Override
	public Criteria createCriteriaExample(MeterLine entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(MeterLine entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}


		return criteria;
	}	
}
