package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;
import com.eletraenergy.configSW.dao.MeasuringInstrumentPlaceUseDAO;
import com.eletraenergy.configSW.model.MeasuringInstrumentPlaceUse;

@Repository
public class MeasuringInstrumentPlaceUseDAOImpl extends DAOImpl<MeasuringInstrumentPlaceUse> implements MeasuringInstrumentPlaceUseDAO {
	
	private static final long serialVersionUID = -2693835078802970511L;
	static Logger logger = Logger.getLogger(MeasuringInstrumentPlaceUseDAOImpl.class);

    @Override
    public List<MeasuringInstrumentPlaceUse> findAll() {
    	return executeQuery("MeasuringInstrumentPlaceUse.findAll", null, null, null);
    }

	public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("MeasuringInstrumentPlaceUse.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}

	@Override
	public Criteria createCriteriaExample(MeasuringInstrumentPlaceUse entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(MeasuringInstrumentPlaceUse entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}

		return criteria;
	}

	@Override
	public MeasuringInstrumentPlaceUse checkName(String name) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pName", name);
    	MeasuringInstrumentPlaceUse ret = null;
    	try {
        	ret = executeQuery("MeasuringInstrumentPlaceUse.checkName", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
	}
	
}
