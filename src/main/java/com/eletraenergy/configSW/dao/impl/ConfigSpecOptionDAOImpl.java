package com.eletraenergy.configSW.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.eletraenergy.configSW.dao.ConfigSpecOptionDAO;
import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.ConfigSpecOption;
import com.eletraenergy.configSW.model.SpecOption;

@Repository
public class ConfigSpecOptionDAOImpl extends DAOImpl<ConfigSpecOption> implements ConfigSpecOptionDAO {
	
	private static final long serialVersionUID = -1320725193416361026L;

	static Logger logger = Logger.getLogger(ConfigSpecOptionDAOImpl.class);

    @Override
    public List<ConfigSpecOption> findAll() {
    	return executeQuery("ConfigSpecOption.findAll", null, null, null);
    }

    public List<ConfigSpecOption> findByConfig(Config config) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pConfig",config.getId());
    	return executeQuery("ConfigSpecOption.findByConfig", parameters, null, null);
    }
    
    public List<ConfigSpecOption> findBySpec(List<SpecOption> spec){
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pSpec",prepararParametros(spec));
    	return executeQuery("ConfigSpecOption.findBySpec", parameters, null, null);
    	
    }

    private List<Long> prepararParametros(List<SpecOption> entidades) {
		List<Long> codigos = new ArrayList<Long>();
		for (SpecOption s : entidades) {
			try {
				codigos.add(s.getId());
			} catch (NumberFormatException e) {
			}
		}
		return codigos;
	}	
    
    public void remove(Config config) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parameters = new HashedMap();
    	parameters.put("pConfig",config);
        executeCommand("ConfigSpecOption.remove", parameters);
    }	
    
    public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("ConfigSpecOption.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}
}
