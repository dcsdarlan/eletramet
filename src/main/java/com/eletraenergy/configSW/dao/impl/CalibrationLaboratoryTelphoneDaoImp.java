package com.eletraenergy.configSW.dao.impl;

import com.eletraenergy.configSW.dao.CalibrationLaboratoryDAO;
import com.eletraenergy.configSW.dao.CalibrationLaboratoryTelphoneDAO;
import com.eletraenergy.configSW.model.CalibrationLaboratory;
import com.eletraenergy.configSW.model.CalibrationLaboratoryTelphone;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class CalibrationLaboratoryTelphoneDaoImp extends DAOImpl<CalibrationLaboratoryTelphone> implements CalibrationLaboratoryTelphoneDAO {
    @Override
    public Long findLastId() {
        return null;
    }

    @Override
    public List<CalibrationLaboratoryTelphone> findById(List<Long> id) {
        return null;
    }

    public void removeByLaboratory(CalibrationLaboratory lab) {
        Map<String, Object> parameters = new HashedMap();
        parameters.put("pId", lab.getId());
        executeCommand("CalibrationLaboratoryTelphone.removeLaboratory", parameters);
    }

    @Override
    public List<CalibrationLaboratoryTelphone> findByLaboratory(CalibrationLaboratory lab) {
        Map<String, Object> parameters = new HashedMap();
        parameters.put("pId", lab.getId());
        return executeQuery("Config.findLaboratory", parameters, null, null);
    }


}
