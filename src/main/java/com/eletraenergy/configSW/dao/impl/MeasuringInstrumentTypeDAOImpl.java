package com.eletraenergy.configSW.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;
import com.eletraenergy.configSW.dao.MeasuringInstrumentTypeDAO;
import com.eletraenergy.configSW.model.MeasuringInstrumentType;

@Repository
public class MeasuringInstrumentTypeDAOImpl extends DAOImpl<MeasuringInstrumentType> implements MeasuringInstrumentTypeDAO {
	
	private static final long serialVersionUID = -2693835078802970511L;
	static Logger logger = Logger.getLogger(MeasuringInstrumentTypeDAOImpl.class);

    @Override
    public List<MeasuringInstrumentType> findAll() {
    	return executeQuery("MeasuringInstrumentType.findAll", null, null, null);
    }

	public Long findLastId() {
		Query queryNamed = entityManager.createNamedQuery("MeasuringInstrumentType.lastId");
		Long singleResult =(Long) queryNamed.getSingleResult();
		
		return singleResult==null?1:singleResult + 1;
	}

	@Override
	public Criteria createCriteriaExample(MeasuringInstrumentType entidade, MatchMode match) {

		Criteria criteria = super.createCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(MeasuringInstrumentType entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField() != null){
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(entidadeLocal.getSortField()));
			}else{
				criteria.addOrder(Order.asc(entidadeLocal.getSortField()));
			}
		}

		return criteria;
	}

	@Override
	public MeasuringInstrumentType checkName(String name) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pName", name);
    	MeasuringInstrumentType ret = null;
    	try {
        	ret = executeQuery("MeasuringInstrumentType.checkName", parametros);
		} catch (NoResultException e) {
			ret = null;
		}
    	return ret;
	}
	
}
