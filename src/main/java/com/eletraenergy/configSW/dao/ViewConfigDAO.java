package com.eletraenergy.configSW.dao;

import java.util.List;

import com.eletraenergy.configSW.model.FilterModel;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.ViewConfig;

public interface ViewConfigDAO extends DAO<ViewConfig> {

	public List<ViewConfig> findConfig(MeterLine meterLine, MeterModel meterModel, String tds, List<FilterModel> validFilters);
}
