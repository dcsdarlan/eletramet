package com.eletraenergy.configSW.dao;

import java.util.List;
import com.eletraenergy.configSW.model.ViewTdsAud;

public interface ViewTdsAudDAO extends DAO<ViewTdsAud> {

	public List<ViewTdsAud> findAll();
	
}
