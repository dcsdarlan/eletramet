package com.eletraenergy.configSW.service;

import java.util.List;
import com.eletraenergy.configSW.model.FilterModel;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.ViewConfig;

public interface ViewConfigService {

	public List<ViewConfig> findConfig(MeterLine meterLine, MeterModel meterModel, String tds, List<FilterModel> filters);
}
