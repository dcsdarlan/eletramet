package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.ViewTdsAud;

public interface ViewTdsAudService {

	public List<ViewTdsAud> findAll();
	public ViewTdsAud findById(long idEntity);
	
}
