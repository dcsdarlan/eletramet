package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.SpecRuleDomain;

public interface SpecRuleDomainService {

	List<SpecRuleDomain> childs(SpecRuleDomain parent);
	List<SpecRuleDomain> findAll();
	
}
