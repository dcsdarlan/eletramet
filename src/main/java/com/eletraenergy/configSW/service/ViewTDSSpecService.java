package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.ViewTDSSpec;
import com.eletraenergy.configSW.model.MeterLine;

public interface ViewTDSSpecService {

	public List<ViewTDSSpec> findAll();
	public List<ViewTDSSpec> findByLine(MeterLine line);
	public List<ViewTDSSpec> findByLineAndModel(MeterLine line, MeterModel model);
}
