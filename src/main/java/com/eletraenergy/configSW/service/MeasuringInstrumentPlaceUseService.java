package com.eletraenergy.configSW.service;

import java.util.List;
import com.eletraenergy.configSW.model.MeasuringInstrumentPlaceUse;

public interface MeasuringInstrumentPlaceUseService {

	List<MeasuringInstrumentPlaceUse> findAll();

	List<MeasuringInstrumentPlaceUse> search(MeasuringInstrumentPlaceUse MeasuringInstrumentPlaceUseFilter, Integer first, Integer pageSize);

	void remove(MeasuringInstrumentPlaceUse MeasuringInstrumentPlaceUseSelect);

	MeasuringInstrumentPlaceUse save(MeasuringInstrumentPlaceUse MeasuringInstrumentPlaceUse);

	MeasuringInstrumentPlaceUse update(MeasuringInstrumentPlaceUse MeasuringInstrumentPlaceUse);
	
	MeasuringInstrumentPlaceUse checkName(String name);
	
}
