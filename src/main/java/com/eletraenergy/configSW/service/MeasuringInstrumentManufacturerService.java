package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;

public interface MeasuringInstrumentManufacturerService {

	List<MeasuringInstrumentManufacturer> findAll();

	List<MeasuringInstrumentManufacturer> search(MeasuringInstrumentManufacturer measuringInstrumentManufacturerFilter, Integer first, Integer pageSize);

	void remove(MeasuringInstrumentManufacturer measuringInstrumentManufacturerSelect);

	MeasuringInstrumentManufacturer save(MeasuringInstrumentManufacturer measuringInstrumentManufacturerType);

	MeasuringInstrumentManufacturer update(MeasuringInstrumentManufacturer measuringInstrumentManufacturer);
	
	MeasuringInstrumentManufacturer checkName(String name);
	
}
