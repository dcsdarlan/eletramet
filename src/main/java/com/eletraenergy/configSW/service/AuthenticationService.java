package com.eletraenergy.configSW.service;

import com.eletraenergy.configSW.model.User;

public interface AuthenticationService {

	public String login(String username, String password);

	public void logout();
	
	public String getLastAccess();
	
	public User getUserLogon();
	
}
