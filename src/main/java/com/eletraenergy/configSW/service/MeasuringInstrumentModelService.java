package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;
import com.eletraenergy.configSW.model.MeasuringInstrumentModel;

public interface MeasuringInstrumentModelService {

	List<MeasuringInstrumentModel> findbyManufacturer(MeasuringInstrumentManufacturer measuringInstrumentManufacturer);

	List<MeasuringInstrumentModel> findAll();

	List<MeasuringInstrumentModel> search(MeasuringInstrumentModel measuringInstrumentModelFilter, Integer first, Integer pageSize);

	void remove(MeasuringInstrumentModel measuringInstrumentModelSelect);

	MeasuringInstrumentModel save(MeasuringInstrumentModel measuringInstrumentModel);

	MeasuringInstrumentModel update(MeasuringInstrumentModel measuringInstrumentModel);
	
	MeasuringInstrumentModel checkName(String name);
	
	int amount(MeasuringInstrumentModel measuringInstrumentModelFilter);
}
