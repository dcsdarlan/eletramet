package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.SpecRule;

public interface SpecRuleService {

	List<SpecRule> findAll();

	void remove(SpecRule specRuleSelect);

	SpecRule save(SpecRule specRule);

	SpecRule update(SpecRule specRule);
	
}
