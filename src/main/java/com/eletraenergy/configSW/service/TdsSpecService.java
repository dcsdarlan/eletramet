package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.TdsSpec;

public interface TdsSpecService {

	List<TdsSpec> findAll();

	List<TdsSpec> search(TdsSpec TdsSpecFilter, Integer first, Integer pageSize);

	void remove(TdsSpec TdsSpecSelect);

	TdsSpec save(TdsSpec TdsSpec);

	TdsSpec update(TdsSpec TdsSpec);
	
	TdsSpec checkIdentifier(String identifier);
}
