package com.eletraenergy.configSW.service;

import java.util.List;
import com.eletraenergy.configSW.model.MeasuringInstrumentStandard;

public interface MeasuringInstrumentStandardService {

	List<MeasuringInstrumentStandard> findAll();

	List<MeasuringInstrumentStandard> search(MeasuringInstrumentStandard MeasuringInstrumentStandardFilter, Integer first, Integer pageSize);

	void remove(MeasuringInstrumentStandard MeasuringInstrumentStandardSelect);

	MeasuringInstrumentStandard save(MeasuringInstrumentStandard MeasuringInstrumentStandard);

	MeasuringInstrumentStandard update(MeasuringInstrumentStandard MeasuringInstrumentStandard);
	
	MeasuringInstrumentStandard checkName(String name);
	
}
