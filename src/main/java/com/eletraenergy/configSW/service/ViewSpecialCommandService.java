package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.ViewSpecialCommand;

public interface ViewSpecialCommandService {

	public List<ViewSpecialCommand> findByTds(Tds tds);
}
