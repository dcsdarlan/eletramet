package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;

public interface MeterModelService {

	List<MeterModel> findbyLine(MeterLine line);

	List<MeterModel> findAll();

	List<MeterModel> search(MeterModel meterModelFilter, Integer first, Integer pageSize);

	void remove(MeterModel meterModelSelect);

	MeterModel save(MeterModel meterModel);

	MeterModel update(MeterModel meterModel);
	
	MeterModel checkIdentifier(String identifier);
	
	int amount(MeterModel modelFilter);
}
