package com.eletraenergy.configSW.service;

import com.eletraenergy.configSW.model.MeasuringInstrumentDepartment;

import java.util.List;

public interface MeasuringInstrumentDepartmentService {

	List<MeasuringInstrumentDepartment> findAll();

	List<MeasuringInstrumentDepartment> search(MeasuringInstrumentDepartment MeasuringInstrumentDepartmentFilter, Integer first, Integer pageSize);

	void remove(MeasuringInstrumentDepartment MeasuringInstrumentDepartmentSelect);

	MeasuringInstrumentDepartment save(MeasuringInstrumentDepartment MeasuringInstrumentDepartment);

	MeasuringInstrumentDepartment update(MeasuringInstrumentDepartment MeasuringInstrumentDepartment);
	
	MeasuringInstrumentDepartment checkName(String name);
	
}
