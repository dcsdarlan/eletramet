package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.Spec;
import com.eletraenergy.configSW.model.SpecCategory;

public interface SpecService {

	List<Spec> findAll();

	List<Spec> search(Spec specFilter, Integer first, Integer pageSize);

	void remove(Spec specSelect);

	Spec save(Spec spec);

	Spec update(Spec spec);
	
	Spec checkIdentifier(String identifier);

	Spec checkOrder(Integer order, SpecCategory specCategory);
	
	int amount(Spec specFilter);
	
	List<Spec> findByCategory(SpecCategory specCategory);
}
