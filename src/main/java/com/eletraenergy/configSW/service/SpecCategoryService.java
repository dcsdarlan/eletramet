package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.SpecCategory;

public interface SpecCategoryService {

	List<SpecCategory> findAll();

	List<SpecCategory> search(SpecCategory specCategoryFilter, Integer first, Integer pageSize);

	void remove(SpecCategory specCategorySelect);

	SpecCategory save(SpecCategory specCategory);

	SpecCategory update(SpecCategory specCategory);
	
	SpecCategory checkIdentifier(String identifier);

	SpecCategory checkOrder(Integer order);
}
