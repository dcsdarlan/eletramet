package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.MeterLine;

public interface MeterLineService {

	List<MeterLine> findAll();

	List<MeterLine> search(MeterLine lineFilter, Integer first, Integer pageSize);

	void remove(MeterLine lineSelect);

	MeterLine save(MeterLine line);

	MeterLine update(MeterLine line);
	
	MeterLine checkIdentifier(String identifier);
}
