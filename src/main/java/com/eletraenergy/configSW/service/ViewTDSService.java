package com.eletraenergy.configSW.service;

import java.util.List;
import com.eletraenergy.configSW.model.FilterModel;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.TdsStateIdentifier;
import com.eletraenergy.configSW.model.ViewTDS;

public interface ViewTDSService {
	public List<ViewTDS> findConfig(MeterLine meterLine, MeterModel meterModel, String tds, List<FilterModel> filters, TdsStateIdentifier state);
}
