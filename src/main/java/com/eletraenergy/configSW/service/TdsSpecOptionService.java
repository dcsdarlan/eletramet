package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.TdsSpecOption;

public interface TdsSpecOptionService {

	List<TdsSpecOption> findAll();

	List<TdsSpecOption> search(TdsSpecOption tdsSpecOptionFilter, Integer first, Integer pageSize);

	void remove(TdsSpecOption tdsSpecOptionSelect);

	TdsSpecOption save(TdsSpecOption tdsSpecOption);

	TdsSpecOption update(TdsSpecOption tdsSpecOption);
	
	TdsSpecOption checkIdentifier(String identifier);
	
	int amount(TdsSpecOption tdsSpecOptionFilter);
	
	TdsSpecOption findByName(String name, String specName);
}
