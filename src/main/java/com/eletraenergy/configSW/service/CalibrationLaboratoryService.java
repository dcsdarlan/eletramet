package com.eletraenergy.configSW.service;

import com.eletraenergy.configSW.model.CalibrationLaboratory;
import com.eletraenergy.configSW.model.MeasuringInstrument;

import java.util.List;

public interface CalibrationLaboratoryService {

	List<CalibrationLaboratory> findAll();
	
	List<CalibrationLaboratory> search(CalibrationLaboratory calibrationLaboratoryFilter, Integer first, Integer pageSize);

	void remove(CalibrationLaboratory calibrationLaboratorySelect);

	CalibrationLaboratory save(CalibrationLaboratory calibrationLaboratory);

	CalibrationLaboratory update(CalibrationLaboratory calibrationLaboratory);
	
	int amount(CalibrationLaboratory calibrationLAboratoryFilter);

}
