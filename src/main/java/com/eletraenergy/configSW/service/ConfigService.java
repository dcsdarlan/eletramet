package com.eletraenergy.configSW.service;

import java.util.List;
import java.util.Map;

import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.ConfigSpecOption;
import com.eletraenergy.configSW.model.ConfigSpecRuleDomain;
import com.eletraenergy.configSW.model.SpecRule;
import com.eletraenergy.configSW.model.TbMeterCommand;
import com.eletraenergy.configSW.model.ViewSpec;

public interface ConfigService {

	List<Config> findAll();

	List<Config> search(Config configFilter, Integer first, Integer pageSize);

	void remove(Config configSelect);

	Config save(Config config, Map<ViewSpec, List<SpecRule>> rules, List<ViewSpec> options, List<TbMeterCommand> listCommand);

	Config update(Config config, Map<ViewSpec, List<SpecRule>> rules, List<ViewSpec> options, List<TbMeterCommand> listCommand);
	
	int amount(Config config);
	
	public List<Config> findAllAbleToCopy();
	
	public List<ConfigSpecRuleDomain> getSpecRules(Config config);
	
	public List<TbMeterCommand> findByConfig(Config config);
	
	public List<TbMeterCommand> findByConfigAndOption(Config config, Long option);

	public void removeCommand(TbMeterCommand c);
	
	public List<ConfigSpecOption> findOptionByConfig(Config config);
	
}
