package com.eletraenergy.configSW.service;

import java.util.List;
import com.eletraenergy.configSW.model.MeasuringInstrumentType;

public interface MeasuringInstrumentTypeService {

	List<MeasuringInstrumentType> findAll();

	List<MeasuringInstrumentType> search(MeasuringInstrumentType measuringInstrumentTypeFilter, Integer first, Integer pageSize);

	void remove(MeasuringInstrumentType measuringInstrumentTypeSelect);

	MeasuringInstrumentType save(MeasuringInstrumentType measuringInstrumentType);

	MeasuringInstrumentType update(MeasuringInstrumentType measuringInstrumentType);
	
	MeasuringInstrumentType checkName(String name);
	
}
