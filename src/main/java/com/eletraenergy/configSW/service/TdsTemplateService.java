package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.TdsTemplate;

public interface TdsTemplateService {

	List<TdsTemplate> findAll();
	
	List<TdsTemplate> findbyMeterModel(MeterModel meterModel);

	List<TdsTemplate> search(TdsTemplate tdsFilter, Integer first, Integer pageSize);

	void remove(TdsTemplate tdsSelect);

	TdsTemplate save(TdsTemplate tds);
	
	TdsTemplate update(TdsTemplate tds);
	
	int amount(TdsTemplate tdsTemplate);
	
	TdsTemplate checkIdentifier(String identifier);
	
	List<TdsTemplate> findByModelOrderIdentifier(MeterModel meterModel);

}
