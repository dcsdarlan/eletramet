package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.ViewSpec;

public interface ViewSpecService {

	public List<ViewSpec> findAll();
}
