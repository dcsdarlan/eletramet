package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.ConfigSimulation;

public interface ConfigSimulationService {

	List<ConfigSimulation> findAll();
	ConfigSimulation save(ConfigSimulation entity);
	List<ConfigSimulation> search(ConfigSimulation configFilter, int first, int pageSize);
	ConfigSimulation update(ConfigSimulation simulation);
	int amount(ConfigSimulation configFilter);
}
