package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.TdsSpecOption;
import com.eletraenergy.configSW.model.TdsTemplate;

public interface TdsService {

	List<Tds> findAll();

	List<Tds> findByModel(MeterModel meterModel);
	
	List<Tds> findByLine(MeterLine meterLine);

	List<Tds> findByModelOrderIdentifier(MeterModel meterModel);

	List<Tds> search(Tds tdsFilter, Integer first, Integer pageSize);

	void remove(Tds tdsSelect);

	Tds save(Tds tds);

	Tds update(Tds tds);
	
	Tds checkIdentifier(String identifier);
	
	Tds checkTemplateOption(TdsTemplate template, TdsSpecOption option);

	int amount(Tds specFilter);

	Tds findaByID(Long id);
}
