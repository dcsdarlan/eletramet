package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.MeasuringInstrumentPlaceUseDAO;
import com.eletraenergy.configSW.model.MeasuringInstrumentPlaceUse;
import com.eletraenergy.configSW.service.MeasuringInstrumentPlaceUseService;

@Service("MeasuringInstrumentPlaceUseService")
public class MeasuringInstrumentPlaceUseServiceImpl implements MeasuringInstrumentPlaceUseService {

	@Autowired
	private MeasuringInstrumentPlaceUseDAO MeasuringInstrumentPlaceUseDAO;

	public List<MeasuringInstrumentPlaceUse> findAll() {
		return MeasuringInstrumentPlaceUseDAO.findAll();
	}

	@Transactional
	public List<MeasuringInstrumentPlaceUse> search(MeasuringInstrumentPlaceUse MeasuringInstrumentPlaceUseFilter, Integer first, Integer pageSize) {
		validate(MeasuringInstrumentPlaceUseFilter);
		return MeasuringInstrumentPlaceUseDAO.findByParameter(MeasuringInstrumentPlaceUseFilter, first, pageSize);
	}

	private void validate(MeasuringInstrumentPlaceUse MeasuringInstrumentPlaceUseFilter) {
		if (MeasuringInstrumentPlaceUseFilter.getName()!=null && MeasuringInstrumentPlaceUseFilter.getName().isEmpty()) {
			MeasuringInstrumentPlaceUseFilter.setName(null);
		}
	}

	@Transactional
	public void remove(MeasuringInstrumentPlaceUse MeasuringInstrumentPlaceUseSelect) {
		MeasuringInstrumentPlaceUseDAO.remove(MeasuringInstrumentPlaceUseSelect);
	}
	
	@Transactional
	public MeasuringInstrumentPlaceUse save(MeasuringInstrumentPlaceUse MeasuringInstrumentPlaceUse) {
		MeasuringInstrumentPlaceUse.setId(MeasuringInstrumentPlaceUseDAO.findLastId());
		return update(MeasuringInstrumentPlaceUse);
	}

	@Transactional
	public MeasuringInstrumentPlaceUse update(MeasuringInstrumentPlaceUse MeasuringInstrumentPlaceUse) {
		return MeasuringInstrumentPlaceUseDAO.save(MeasuringInstrumentPlaceUse);
	}

	@Override
	public MeasuringInstrumentPlaceUse checkName(String name) {
		return MeasuringInstrumentPlaceUseDAO.checkName(name);
	}
	
}

