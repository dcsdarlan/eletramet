package com.eletraenergy.configSW.service.impl;

import com.eletraenergy.configSW.model.MeasuringInstrumentDepartment;
import com.eletraenergy.configSW.service.MeasuringInstrumentDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("MeasuringInstrumentDepartmentService")
public class MeasuringInstrumentDepartmentServiceImpl implements MeasuringInstrumentDepartmentService {

	@Autowired
	private com.eletraenergy.configSW.dao.MeasuringInstrumentDepartmentDAO MeasuringInstrumentDepartmentDAO;

	public List<MeasuringInstrumentDepartment> findAll() {
		return MeasuringInstrumentDepartmentDAO.findAll();
	}

	@Transactional
	public List<MeasuringInstrumentDepartment> search(MeasuringInstrumentDepartment MeasuringInstrumentDepartmentFilter, Integer first, Integer pageSize) {
		validate(MeasuringInstrumentDepartmentFilter);
		return MeasuringInstrumentDepartmentDAO.findByParameter(MeasuringInstrumentDepartmentFilter, first, pageSize);
	}

	private void validate(MeasuringInstrumentDepartment MeasuringInstrumentDepartmentFilter) {
		if (MeasuringInstrumentDepartmentFilter.getName()!=null && MeasuringInstrumentDepartmentFilter.getName().isEmpty()) {
			MeasuringInstrumentDepartmentFilter.setName(null);
		}
	}

	@Transactional
	public void remove(MeasuringInstrumentDepartment MeasuringInstrumentDepartmentSelect) {
		MeasuringInstrumentDepartmentDAO.remove(MeasuringInstrumentDepartmentSelect);
	}
	
	@Transactional
	public MeasuringInstrumentDepartment save(MeasuringInstrumentDepartment MeasuringInstrumentDepartment) {
		MeasuringInstrumentDepartment.setId(MeasuringInstrumentDepartmentDAO.findLastId());
		return update(MeasuringInstrumentDepartment);
	}

	@Transactional
	public MeasuringInstrumentDepartment update(MeasuringInstrumentDepartment MeasuringInstrumentDepartment) {
		return MeasuringInstrumentDepartmentDAO.save(MeasuringInstrumentDepartment);
	}

	@Override
	public MeasuringInstrumentDepartment checkName(String name) {
		return MeasuringInstrumentDepartmentDAO.checkName(name);
	}
	
}

