package com.eletraenergy.configSW.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.ConfigDAO;
import com.eletraenergy.configSW.dao.ConfigSpecOptionDAO;
import com.eletraenergy.configSW.dao.SpecOptionDAO;
import com.eletraenergy.configSW.dao.TbMeterCommandDAO;
import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.ConfigSpecOption;
import com.eletraenergy.configSW.model.Spec;
import com.eletraenergy.configSW.model.SpecOption;
import com.eletraenergy.configSW.model.StateIdentifier;
import com.eletraenergy.configSW.model.TbMeterCommand;
import com.eletraenergy.configSW.service.SpecOptionService;

@Service("specOptionService")
public class SpecOptionServiceImpl implements SpecOptionService {

	@Autowired
	private SpecOptionDAO specOptionDAO;
	@Autowired
	private ConfigSpecOptionDAO configSpecOptionDAO;
	@Autowired
	private ConfigDAO configDAO;
	@Autowired
	private TbMeterCommandDAO tbMeterCommandDAO;

	public List<SpecOption> findAll() {
		return specOptionDAO.findAll();
	}

	@Transactional
	public List<Config> findAllBySpec(Spec spec) {
		List<Config> res = null;
		List<Long> ids = new ArrayList<Long>();
		List<SpecOption> listOption = specOptionDAO.findAllBySpec(spec);
		if (!listOption.isEmpty()) {
			List<ConfigSpecOption> list = configSpecOptionDAO.findBySpec(listOption);
			for (ConfigSpecOption configSpecOption : list) {
				
				List<TbMeterCommand> commands = tbMeterCommandDAO.findByConfig(configSpecOption.getConfig());
				boolean findCommand = false;
				for (TbMeterCommand tbMeterCommand : commands) {
					if (tbMeterCommand.getConfigSpecOption() !=null && tbMeterCommand.getConfigSpecOption().equals(configSpecOption)){
						findCommand = true;
						break;
					}
				}
				
				if (!findCommand){
					Long id = configSpecOption.getConfig().getId();
					if (!ids.contains(id)){
						ids.add(id);
					}
				}
			}
			if (!ids.isEmpty()){
				res = configDAO.findById(ids);
				for (Config c : res) {
					c.getTds().getMeterModel().getMeterLine();
				}
			}
		}
		
		return res;
	}

	@Transactional
	public Boolean isSpecOptionUsed(SpecOption specOption) {
		List<SpecOption> listOption = new ArrayList<SpecOption>();
		listOption.add(specOption);
		List<ConfigSpecOption> list = configSpecOptionDAO.findBySpec(listOption);
		return list!=null && !list.isEmpty();
	}

	public SpecOption findById(Long id) {
		return specOptionDAO.findById(id);
	}

	public SpecOption checkIdentifier(String identifier){
		return specOptionDAO.checkIdentifier(identifier);
	}
	
	@Transactional
	public List<SpecOption> search(SpecOption specOptionFilter, Integer first, Integer pageSize) {
		validate(specOptionFilter);
		return specOptionDAO.findByParameter(specOptionFilter, first, pageSize);
	}

	@Transactional
	public int amount(SpecOption specOptionFilter) {
		validate(specOptionFilter);
		specOptionFilter.setSortField(null);
		specOptionFilter.setSortOrder(null);
		
		return specOptionDAO.amount(specOptionFilter);
	}
	
	private void validate(SpecOption specOptionFilter) {
		if (specOptionFilter.getName()!=null && specOptionFilter.getName().isEmpty()) {
			specOptionFilter.setName(null);
			
		}
		if (specOptionFilter.getIdentifier()!=null && specOptionFilter.getIdentifier().isEmpty()) {
			specOptionFilter.setIdentifier(null);
			
		}
	}

	@Transactional
	public void remove(SpecOption specOptionSelect) {
		specOptionDAO.remove(specOptionSelect);
	}
	
	@Transactional
	public SpecOption save(SpecOption specOption, List<Config> configs) {
		specOption.setId(specOptionDAO.findLastId());
		specOption = update(specOption, configs);
		
		return specOption;
	}

	@Transactional
	public SpecOption update(SpecOption specOption, List<Config> configs) {
		
		specOption = specOptionDAO.save(specOption);
		
		if (configs != null){
			saveConfigs(specOption, configs);
		}

		return specOption;

	
	}

	private void saveConfigs(SpecOption specOption, List<Config> configs) {
		Long findLastId = configSpecOptionDAO.findLastId();
		for (Config config : configs) {
			ConfigSpecOption e = new ConfigSpecOption(findLastId++, config, specOption.getId());
			configSpecOptionDAO.save(e);
			config.setChangeVersion(config.getChangeVersion() + 1);
			config.setChangeModified(new Date(System.currentTimeMillis()));
			config.setChangeIdentifier(StateIdentifier.notm.toString());
			configDAO.save(config);				
		}
	}


}