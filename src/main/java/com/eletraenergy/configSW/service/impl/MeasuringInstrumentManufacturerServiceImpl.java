package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.MeasuringInstrumentManufacturerDAO;
import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;
import com.eletraenergy.configSW.service.MeasuringInstrumentManufacturerService;

@Service("MeasuringInstrumentManufacturerService")
public class MeasuringInstrumentManufacturerServiceImpl implements MeasuringInstrumentManufacturerService {

	@Autowired
	private MeasuringInstrumentManufacturerDAO MeasuringInstrumentManufacturerDAO;

	public List<MeasuringInstrumentManufacturer> findAll() {
		return MeasuringInstrumentManufacturerDAO.findAll();
	}

	@Transactional
	public List<MeasuringInstrumentManufacturer> search(MeasuringInstrumentManufacturer measuringInstrumentManufacturerFilter, Integer first, Integer pageSize) {
		validate(measuringInstrumentManufacturerFilter);
		return MeasuringInstrumentManufacturerDAO.findByParameter(measuringInstrumentManufacturerFilter, first, pageSize);
	}

	private void validate(MeasuringInstrumentManufacturer measuringInstrumentManufacturerFilter) {
		if (measuringInstrumentManufacturerFilter.getName()!=null && measuringInstrumentManufacturerFilter.getName().isEmpty()) {
			measuringInstrumentManufacturerFilter.setName(null);
		}
	}

	@Transactional
	public void remove(MeasuringInstrumentManufacturer measuringInstrumentManufacturerSelect) {
		MeasuringInstrumentManufacturerDAO.remove(measuringInstrumentManufacturerSelect);
	}
	
	@Transactional
	public MeasuringInstrumentManufacturer save(MeasuringInstrumentManufacturer measuringInstrumentManufacturer) {
		measuringInstrumentManufacturer.setId(MeasuringInstrumentManufacturerDAO.findLastId());
		return update(measuringInstrumentManufacturer);
	}

	@Transactional
	public MeasuringInstrumentManufacturer update(MeasuringInstrumentManufacturer measuringInstrumentManufacturer) {
		return MeasuringInstrumentManufacturerDAO.save(measuringInstrumentManufacturer);
	}

	@Override
	public MeasuringInstrumentManufacturer checkName(String name) {
		return MeasuringInstrumentManufacturerDAO.checkName(name);
	}
	
}

