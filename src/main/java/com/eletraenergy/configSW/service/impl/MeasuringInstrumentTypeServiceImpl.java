package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.MeasuringInstrumentTypeDAO;
import com.eletraenergy.configSW.model.MeasuringInstrumentType;
import com.eletraenergy.configSW.service.MeasuringInstrumentTypeService;

@Service("MeasuringInstrumentTypeService")
public class MeasuringInstrumentTypeServiceImpl implements MeasuringInstrumentTypeService {

	@Autowired
	private MeasuringInstrumentTypeDAO MeasuringInstrumentTypeDAO;

	public List<MeasuringInstrumentType> findAll() {
		return MeasuringInstrumentTypeDAO.findAll();
	}

	@Transactional
	public List<MeasuringInstrumentType> search(MeasuringInstrumentType measuringInstrumentTypeFilter, Integer first, Integer pageSize) {
		validate(measuringInstrumentTypeFilter);
		return MeasuringInstrumentTypeDAO.findByParameter(measuringInstrumentTypeFilter, first, pageSize);
	}

	private void validate(MeasuringInstrumentType measuringInstrumentTypeFilter) {
		if (measuringInstrumentTypeFilter.getName()!=null && measuringInstrumentTypeFilter.getName().isEmpty()) {
			measuringInstrumentTypeFilter.setName(null);
		}
	}

	@Transactional
	public void remove(MeasuringInstrumentType measuringInstrumentTypeSelect) {
		MeasuringInstrumentTypeDAO.remove(measuringInstrumentTypeSelect);
	}
	
	@Transactional
	public MeasuringInstrumentType save(MeasuringInstrumentType measuringInstrumentType) {
		measuringInstrumentType.setId(MeasuringInstrumentTypeDAO.findLastId());
		return update(measuringInstrumentType);
	}

	@Transactional
	public MeasuringInstrumentType update(MeasuringInstrumentType measuringInstrumentType) {
		return MeasuringInstrumentTypeDAO.save(measuringInstrumentType);
	}

	@Override
	public MeasuringInstrumentType checkName(String name) {
		return MeasuringInstrumentTypeDAO.checkName(name);
	}
	
}

