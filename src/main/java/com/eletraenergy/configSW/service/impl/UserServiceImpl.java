package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.FunctionalityDAO;
import com.eletraenergy.configSW.dao.UserDAO;
import com.eletraenergy.configSW.exception.ConfigSWException;
import com.eletraenergy.configSW.model.Functionality;
import com.eletraenergy.configSW.model.User;
import com.eletraenergy.configSW.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService, UserDetailsService {

	@Autowired
	private UserDAO userDAO;
	@Autowired
	private FunctionalityDAO functionalityDAO;

	public List<Functionality> FindAllFunctionalities() {
		return functionalityDAO.findAll();
	}
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		return findByLogin(username);
	}
	
	public User findByLogin(String username) throws ConfigSWException {
		try {
			return userDAO.findByUsername(username);	
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Transactional
	public int amount(User userFilter) {
		validate(userFilter);
		userFilter.setSortField(null);
		userFilter.setSortOrder(null);
		
		return userDAO.amount(userFilter);
	}
	
	@Transactional
	public List<User> search(User userFilter, int first, int pageSize) {
		validate(userFilter);
		return userDAO.findByParameter(userFilter, first, pageSize);
	}

	@Override
	public List<User> findAll() {
		return userDAO.findAll();
	}

	private void validate(User userFilter) {
		if (userFilter.getName()!=null && userFilter.getName().isEmpty()) {
			userFilter.setName(null);
			
		}
		if (userFilter.getUsername()!=null && userFilter.getUsername().isEmpty()) {
			userFilter.setUsername(null);
			
		}
	}

	@Transactional
	public void remove(User userSelect) {
		userDAO.remove(userSelect);
	}
	
	@Transactional
	public User save(User user) {
		user.setId(userDAO.findLastId());
		return update(user);
	}
	
	@Transactional
	public User update(User user) {
		return userDAO.save(user);
	}
	
}

