package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.TdsDAO;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.TdsSpecOption;
import com.eletraenergy.configSW.model.TdsTemplate;
import com.eletraenergy.configSW.service.TdsService;

@Service("tdsService")
public class TdsServiceImpl implements TdsService {

	@Autowired
	private TdsDAO tdsDAO;

	public List<Tds> findAll() {
		return tdsDAO.findAll();
	}

	public List<Tds> findByModelOrderIdentifier(MeterModel meterModel){
		return tdsDAO.findByModelOrderIdentifier(meterModel);
	}
	public List<Tds> findByModel(MeterModel meterModel){
		return tdsDAO.findByModel(meterModel);
	}

	public List<Tds> findByLine(MeterLine meterLine){
		return tdsDAO.findByLine(meterLine);
	}

	public Tds checkIdentifier(String identifier){
		return tdsDAO.checkIdentifier(identifier);
	}
	
	public Tds checkTemplateOption(TdsTemplate template, TdsSpecOption option){
		return tdsDAO.checkTemplateOption(template, option);
	}

	@Transactional
	public List<Tds> search(Tds tdsFilter, Integer first, Integer pageSize) {
		validate(tdsFilter);
		return tdsDAO.findByParameter(tdsFilter, first, pageSize);
	}

	@Transactional
	public int amount(Tds tdsFilter) {
		validate(tdsFilter);
		tdsFilter.setSortField(null);
		tdsFilter.setSortOrder(null);
		
		return tdsDAO.amount(tdsFilter);
	}
	
	private void validate(Tds tdsFilter) {
		if (tdsFilter.getIdentifier()!=null && tdsFilter.getIdentifier().isEmpty()) {
			tdsFilter.setIdentifier(null);
			
		}
	}

	@Transactional
	public void remove(Tds tdsSelect) {
		tdsDAO.remove(tdsSelect);
	}
	
	@Transactional
	public Tds save(Tds tds) {
		tds.setId(tdsDAO.findLastId());
		return update(tds);
	}

	@Transactional
	public Tds update(Tds tds) {
		return tdsDAO.save(tds);
	}

	public Tds findaByID(Long id) {
		return tdsDAO.findById(id);
				
	}
}