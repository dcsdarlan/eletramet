package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eletraenergy.configSW.dao.ViewTdsAudDAO;
import com.eletraenergy.configSW.model.ViewTdsAud;
import com.eletraenergy.configSW.service.ViewTdsAudService;

@Service("viewTdsAud")
public class ViewTdsAudServiceImpl implements ViewTdsAudService {

	@Autowired
	private ViewTdsAudDAO viewTdsAudDAO;

	public List<ViewTdsAud> findAll() {
		return viewTdsAudDAO.findAll();
	}
	
	public ViewTdsAud findById(long idEntity) {
		return viewTdsAudDAO.findById(idEntity);
	}
}

