package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.SpecRuleDAO;
import com.eletraenergy.configSW.model.SpecRule;
import com.eletraenergy.configSW.service.SpecRuleService;

@Service("specRuleService")
public class SpecRuleServiceImpl implements SpecRuleService {

	@Autowired
	private SpecRuleDAO specRuleDAO;

	public List<SpecRule> findAll() {
		List<SpecRule> all = specRuleDAO.findAll();
		return all;
	}

	@Transactional
	public void remove(SpecRule specRuleSelect) {
		specRuleDAO.remove(specRuleSelect);
	}
	
	@Transactional
	public SpecRule save(SpecRule specRule) {
		specRule.setId(specRuleDAO.findLastId());
		return update(specRule);
	}

	@Transactional
	public SpecRule update(SpecRule specRule) {
		return specRuleDAO.save(specRule);
	}
}