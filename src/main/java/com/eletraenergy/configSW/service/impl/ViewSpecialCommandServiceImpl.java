package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eletraenergy.configSW.dao.ViewSpecialCommandDAO;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.ViewSpecialCommand;
import com.eletraenergy.configSW.service.ViewSpecialCommandService;

@Service("viewSpecialCommandService")
public class ViewSpecialCommandServiceImpl implements ViewSpecialCommandService {

	@Autowired
	private ViewSpecialCommandDAO viewSpecialCommandDAO;

	public List<ViewSpecialCommand> findByTds(Tds tds) {
		return viewSpecialCommandDAO.findByTds(tds);
	}

}

