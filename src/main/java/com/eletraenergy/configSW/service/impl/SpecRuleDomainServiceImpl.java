package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eletraenergy.configSW.dao.SpecRuleDomainDAO;
import com.eletraenergy.configSW.model.SpecRuleDomain;
import com.eletraenergy.configSW.service.SpecRuleDomainService;

@Service("specRuleDomainService")
public class SpecRuleDomainServiceImpl implements SpecRuleDomainService {

	@Autowired
	private SpecRuleDomainDAO specRuleDomainDAO;

	public List<SpecRuleDomain> childs(SpecRuleDomain specRuleDomain) {
		List<SpecRuleDomain> all = specRuleDomainDAO.childs(specRuleDomain);
		return all;
	}

	public List<SpecRuleDomain> findAll() {
		return specRuleDomainDAO.findAll();
	}
}