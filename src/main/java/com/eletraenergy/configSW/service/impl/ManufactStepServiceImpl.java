package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eletraenergy.configSW.dao.ManufactStepDAO;
import com.eletraenergy.configSW.model.ManufactStep;
import com.eletraenergy.configSW.service.ManufactStepService;

@Service("manufactStepService")
public class ManufactStepServiceImpl implements ManufactStepService {

	@Autowired
	private ManufactStepDAO meterLineDAO;

	public List<ManufactStep> findAll() {
		return meterLineDAO.findAll();
	}
}

