package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.MeterLineDAO;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.service.MeterLineService;

@Service("meterLineService")
public class MeterLineServiceImpl implements MeterLineService {

	@Autowired
	private MeterLineDAO meterLineDAO;

	public List<MeterLine> findAll() {
		return meterLineDAO.findAll();
	}

	public MeterLine checkIdentifier(String identifier){
		return meterLineDAO.checkIdentifier(identifier);
	}
	
	@Transactional
	public List<MeterLine> search(MeterLine userFilter, Integer first, Integer pageSize) {
		validate(userFilter);
		return meterLineDAO.findByParameter(userFilter, first, pageSize);
	}

	private void validate(MeterLine userFilter) {
		if (userFilter.getName()!=null && userFilter.getName().isEmpty()) {
			userFilter.setName(null);
			
		}
		if (userFilter.getIdentifier()!=null && userFilter.getIdentifier().isEmpty()) {
			userFilter.setIdentifier(null);
			
		}
	}

	@Transactional
	public void remove(MeterLine userSelect) {
		meterLineDAO.remove(userSelect);
	}
	
	@Transactional
	public MeterLine save(MeterLine line) {
		line.setId(meterLineDAO.findLastId());
		return update(line);
	}

	@Transactional
	public MeterLine update(MeterLine line) {
		return meterLineDAO.save(line);
	}
}

