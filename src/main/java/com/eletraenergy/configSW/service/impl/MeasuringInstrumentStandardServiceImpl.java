package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.MeasuringInstrumentStandardDAO;
import com.eletraenergy.configSW.model.MeasuringInstrumentStandard;
import com.eletraenergy.configSW.service.MeasuringInstrumentStandardService;

@Service("MeasuringInstrumentStandardService")
public class MeasuringInstrumentStandardServiceImpl implements MeasuringInstrumentStandardService {

	@Autowired
	private MeasuringInstrumentStandardDAO MeasuringInstrumentStandardDAO;

	public List<MeasuringInstrumentStandard> findAll() {
		return MeasuringInstrumentStandardDAO.findAll();
	}

	@Transactional
	public List<MeasuringInstrumentStandard> search(MeasuringInstrumentStandard MeasuringInstrumentStandardFilter, Integer first, Integer pageSize) {
		validate(MeasuringInstrumentStandardFilter);
		return MeasuringInstrumentStandardDAO.findByParameter(MeasuringInstrumentStandardFilter, first, pageSize);
	}

	private void validate(MeasuringInstrumentStandard MeasuringInstrumentStandardFilter) {
		if (MeasuringInstrumentStandardFilter.getName()!=null && MeasuringInstrumentStandardFilter.getName().isEmpty()) {
			MeasuringInstrumentStandardFilter.setName(null);
		}
	}

	@Transactional
	public void remove(MeasuringInstrumentStandard MeasuringInstrumentStandardSelect) {
		MeasuringInstrumentStandardDAO.remove(MeasuringInstrumentStandardSelect);
	}
	
	@Transactional
	public MeasuringInstrumentStandard save(MeasuringInstrumentStandard MeasuringInstrumentStandard) {
		MeasuringInstrumentStandard.setId(MeasuringInstrumentStandardDAO.findLastId());
		return update(MeasuringInstrumentStandard);
	}

	@Transactional
	public MeasuringInstrumentStandard update(MeasuringInstrumentStandard MeasuringInstrumentStandard) {
		return MeasuringInstrumentStandardDAO.save(MeasuringInstrumentStandard);
	}

	@Override
	public MeasuringInstrumentStandard checkName(String name) {
		return MeasuringInstrumentStandardDAO.checkName(name);
	}
	
}

