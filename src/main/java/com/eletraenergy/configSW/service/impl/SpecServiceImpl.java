package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.SpecDAO;
import com.eletraenergy.configSW.model.Spec;
import com.eletraenergy.configSW.model.SpecCategory;
import com.eletraenergy.configSW.service.SpecService;

@Service("specService")
public class SpecServiceImpl implements SpecService {

	@Autowired
	private SpecDAO specDAO;

	public List<Spec> findAll() {
		return specDAO.findAll();
	}

	public List<Spec> findByCategory(SpecCategory specCategory) {
		return specDAO.findBySpecCategory(specCategory);
	}

	public Spec checkIdentifier(String identifier){
		return specDAO.checkIdentifier(identifier);
	}
	
	public Spec checkOrder(Integer order, SpecCategory specCategory){
		return specDAO.checkOrder(order, specCategory);
	}

	@Transactional
	public List<Spec> search(Spec specFilter, Integer first, Integer pageSize) {
		validate(specFilter);
		return specDAO.findByParameter(specFilter, first, pageSize);
	}

	@Transactional
	public int amount(Spec specFilter) {
		validate(specFilter);
		specFilter.setSortField(null);
		specFilter.setSortOrder(null);
		
		return specDAO.amount(specFilter);
	}
	
	private void validate(Spec specFilter) {
		if (specFilter.getName()!=null && specFilter.getName().isEmpty()) {
			specFilter.setName(null);
			
		}
		if (specFilter.getIdentifier()!=null && specFilter.getIdentifier().isEmpty()) {
			specFilter.setIdentifier(null);
			
		}
	}

	@Transactional
	public void remove(Spec specSelect) {
		specDAO.remove(specSelect);
	}
	
	@Transactional
	public Spec save(Spec spec) {
		spec.setId(specDAO.findLastId());
		return update(spec);
	}

	@Transactional
	public Spec update(Spec spec) {
		return specDAO.save(spec);
	}
}