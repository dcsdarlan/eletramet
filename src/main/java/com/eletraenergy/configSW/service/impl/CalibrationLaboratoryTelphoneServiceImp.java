package com.eletraenergy.configSW.service.impl;

import com.eletraenergy.configSW.dao.CalibrationLaboratoryDAO;
import com.eletraenergy.configSW.dao.CalibrationLaboratoryTelphoneDAO;
import com.eletraenergy.configSW.model.CalibrationLaboratory;
import com.eletraenergy.configSW.model.CalibrationLaboratoryTelphone;
import com.eletraenergy.configSW.service.CalibrationLaboratoryService;
import com.eletraenergy.configSW.service.CalibrationLaboratoryTelphoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("CalibrationLaboratoryTelphoneService")
public class CalibrationLaboratoryTelphoneServiceImp implements CalibrationLaboratoryTelphoneService {

    @Autowired
    private CalibrationLaboratoryTelphoneDAO calibrationLaboratoryDAO;


    public List<CalibrationLaboratoryTelphone> findAll() {
        return calibrationLaboratoryDAO.findAll();
    }

    @Transactional
    public List<CalibrationLaboratoryTelphone> search(CalibrationLaboratoryTelphone calibrationLaboratoryFilter, Integer first, Integer pageSize) {

        return calibrationLaboratoryDAO.findByParameter(calibrationLaboratoryFilter, first, pageSize);
    }

    @Transactional
    public int amount(CalibrationLaboratoryTelphone calibrationLaboratoryFilter) {
        calibrationLaboratoryFilter.setSortField(null);
        calibrationLaboratoryFilter.setSortOrder(null);

        return calibrationLaboratoryDAO.amount(calibrationLaboratoryFilter);
    }

    @Override
    public void removeByCalibrationLaboratory(CalibrationLaboratory calibrationLaboratorySelect) {
        calibrationLaboratoryDAO.removeByLaboratory(calibrationLaboratorySelect);
    }

    @Override
    public List<CalibrationLaboratoryTelphone> findByCalibrationLaboratory(CalibrationLaboratory calibrationLaboratorySelect) {
        return calibrationLaboratoryDAO.findByLaboratory(calibrationLaboratorySelect);
    }

    @Transactional
    public void remove(CalibrationLaboratoryTelphone calibrationLaboratorySelect) {
        calibrationLaboratoryDAO.remove(calibrationLaboratorySelect);
    }

    @Transactional
    public CalibrationLaboratoryTelphone save(CalibrationLaboratoryTelphone calibrationLaboratory) {
        calibrationLaboratory.setId(calibrationLaboratoryDAO.findLastId());
        return update(calibrationLaboratory);
    }

    @Transactional
    public CalibrationLaboratoryTelphone update(CalibrationLaboratoryTelphone calibrationLaboratory) {
        return calibrationLaboratoryDAO.save(calibrationLaboratory);
    }


}
