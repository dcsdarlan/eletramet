package com.eletraenergy.configSW.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eletraenergy.configSW.dao.ViewConfigDAO;
import com.eletraenergy.configSW.model.FilterModel;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.ViewConfig;
import com.eletraenergy.configSW.service.ViewConfigService;

@Service("viewConfigService")
public class ViewConfigServiceImpl implements ViewConfigService {

	@Autowired
	private ViewConfigDAO viewConfigDAO;

	public List<ViewConfig> findConfig(MeterLine meterLine, MeterModel meterModel, String tds, List<FilterModel> filters) {
		return viewConfigDAO.findConfig(meterLine, meterModel, tds, filters);
	}

}

