package com.eletraenergy.configSW.service.impl;

import com.eletraenergy.configSW.dao.CalibrationLaboratoryDAO;
import com.eletraenergy.configSW.dao.MeasuringInstrumentDAO;
import com.eletraenergy.configSW.model.CalibrationLaboratory;
import com.eletraenergy.configSW.model.MeasuringInstrument;
import com.eletraenergy.configSW.service.CalibrationLaboratoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("CalibrationLaboratoryService")
public class CalibrationLaboratoryServiceImp implements CalibrationLaboratoryService {

    @Autowired
    private CalibrationLaboratoryDAO calibrationLaboratoryDAO;


    public List<CalibrationLaboratory> findAll() {
        return calibrationLaboratoryDAO.findAll();
    }

    @Transactional
    public List<CalibrationLaboratory> search(CalibrationLaboratory calibrationLaboratoryFilter, Integer first, Integer pageSize) {

        return calibrationLaboratoryDAO.findByParameter(calibrationLaboratoryFilter, first, pageSize);
    }

    @Transactional
    public int amount(CalibrationLaboratory calibrationLaboratoryFilter) {
        calibrationLaboratoryFilter.setSortField(null);
        calibrationLaboratoryFilter.setSortOrder(null);

        return calibrationLaboratoryDAO.amount(calibrationLaboratoryFilter);
    }

    @Transactional
    public void remove(CalibrationLaboratory calibrationLaboratorySelect) {
        calibrationLaboratoryDAO.remove(calibrationLaboratorySelect);
    }

    @Transactional
    public CalibrationLaboratory save(CalibrationLaboratory calibrationLaboratory) {
        calibrationLaboratory.setId(calibrationLaboratoryDAO.findLastId());
        return update(calibrationLaboratory);
    }

    @Transactional
    public CalibrationLaboratory update(CalibrationLaboratory calibrationLaboratory) {
        return calibrationLaboratoryDAO.save(calibrationLaboratory);
    }
}
