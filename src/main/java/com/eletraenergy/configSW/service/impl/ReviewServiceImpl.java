package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eletraenergy.configSW.dao.ReviewDAO;
import com.eletraenergy.configSW.model.Review;
import com.eletraenergy.configSW.service.ReviewService;

@Service("reviewService")
public class ReviewServiceImpl implements ReviewService {

	@Autowired
	private ReviewDAO reviewDAO;

	@Override
	public List<Review> findLast10() {
		return reviewDAO.findLast10();
	}

	@Override
	public List<Object> findByReview(Review entidade) {
		return reviewDAO.findByReview(entidade);
	}
	
	
}