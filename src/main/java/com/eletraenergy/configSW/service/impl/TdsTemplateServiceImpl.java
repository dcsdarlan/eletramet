package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.TdsTemplateDAO;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.TdsTemplate;
import com.eletraenergy.configSW.service.TdsTemplateService;

@Service("tdsTemplateService")
public class TdsTemplateServiceImpl implements TdsTemplateService {

	@Autowired
	private TdsTemplateDAO tdsTemplateDAO;

	public List<TdsTemplate> findAll() {
		return tdsTemplateDAO.findAll();
	}

	@Transactional
	public List<TdsTemplate> search(TdsTemplate tdsFilter, Integer first, Integer pageSize) {
		validate(tdsFilter);
		return tdsTemplateDAO.findByParameter(tdsFilter, first, pageSize);
	}

	public List<TdsTemplate> findByModelOrderIdentifier(MeterModel meterModel){
		return tdsTemplateDAO.findByModelOrderIdentifier(meterModel);
	}

	@Transactional
	public void remove(TdsTemplate tdsTemplateSelect) {
		tdsTemplateDAO.remove(tdsTemplateSelect);
	}
	
	@Transactional
	public TdsTemplate save(TdsTemplate tdsTemplate) {
		tdsTemplate.setId(tdsTemplateDAO.findLastId());
		return update(tdsTemplate);
	}

	@Transactional
	public TdsTemplate update(TdsTemplate tdsTemplate) {
		return tdsTemplateDAO.save(tdsTemplate);
	}

	private void validate(TdsTemplate tdsTemplateFilter) {
		if (tdsTemplateFilter.getIdentifier()!=null && tdsTemplateFilter.getIdentifier().isEmpty()) {
			tdsTemplateFilter.setIdentifier(null);
		}
	}

	public List<TdsTemplate> findbyMeterModel(MeterModel meterModel) {
		return tdsTemplateDAO.findByMeterModel(meterModel);
	}
	
	@Transactional
	public int amount(TdsTemplate tdsTemplate) {
		validate(tdsTemplate);
		tdsTemplate.setSortField(null);
		tdsTemplate.setSortOrder(null);
		
		return tdsTemplateDAO.amount(tdsTemplate);
	}

	public TdsTemplate checkIdentifier(String identifier){
		return tdsTemplateDAO.checkIdentifier(identifier);
	}

}