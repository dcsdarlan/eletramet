package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.TdsSpecDAO;
import com.eletraenergy.configSW.model.TdsSpec;
import com.eletraenergy.configSW.service.TdsSpecService;

@Service("tdsSpecService")
public class TdsSpecServiceImpl implements TdsSpecService {

	@Autowired
	private TdsSpecDAO tdsSpecDAO;

	public List<TdsSpec> findAll() {
		return tdsSpecDAO.findAll();
	}

	public TdsSpec checkIdentifier(String identifier){
		return tdsSpecDAO.checkIdentifier(identifier);
	}
	
	@Transactional
	public List<TdsSpec> search(TdsSpec tdsSpecFilter, Integer first, Integer pageSize) {
		validate(tdsSpecFilter);
		return tdsSpecDAO.findByParameter(tdsSpecFilter, first, pageSize);
	}

	private void validate(TdsSpec tdsSpecFilter) {
		if (tdsSpecFilter.getName()!=null && tdsSpecFilter.getName().isEmpty()) {
			tdsSpecFilter.setName(null);
			
		}
		if (tdsSpecFilter.getIdentifier()!=null && tdsSpecFilter.getIdentifier().isEmpty()) {
			tdsSpecFilter.setIdentifier(null);
			
		}
	}

	@Transactional
	public void remove(TdsSpec tdsSpecSelect) {
		tdsSpecDAO.remove(tdsSpecSelect);
	}
	
	@Transactional
	public TdsSpec save(TdsSpec tdsSpec) {
		tdsSpec.setId(tdsSpecDAO.findLastId());
		return update(tdsSpec);
	}

	@Transactional
	public TdsSpec update(TdsSpec tdsSpec) {
		return tdsSpecDAO.save(tdsSpec);
	}
}