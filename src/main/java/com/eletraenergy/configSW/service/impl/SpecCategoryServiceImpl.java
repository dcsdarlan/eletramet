package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.SpecCategoryDAO;
import com.eletraenergy.configSW.model.SpecCategory;
import com.eletraenergy.configSW.service.SpecCategoryService;

@Service("specCategoryService")
public class SpecCategoryServiceImpl implements SpecCategoryService {

	@Autowired
	private SpecCategoryDAO specCategoryDAO;

	public List<SpecCategory> findAll() {
		return specCategoryDAO.findAll();
	}

	public SpecCategory checkIdentifier(String identifier){
		return specCategoryDAO.checkIdentifier(identifier);
	}
	
	public SpecCategory checkOrder(Integer order){
		return specCategoryDAO.checkOrder(order);
	}

	@Transactional
	public List<SpecCategory> search(SpecCategory specCategoryFilter, Integer first, Integer pageSize) {
		validate(specCategoryFilter);
		return specCategoryDAO.findByParameter(specCategoryFilter, first, pageSize);
	}

	private void validate(SpecCategory specCategoryFilter) {
		if (specCategoryFilter.getName()!=null && specCategoryFilter.getName().isEmpty()) {
			specCategoryFilter.setName(null);
			
		}
		if (specCategoryFilter.getIdentifier()!=null && specCategoryFilter.getIdentifier().isEmpty()) {
			specCategoryFilter.setIdentifier(null);
			
		}
	}

	@Transactional
	public void remove(SpecCategory specCategorySelect) {
		specCategoryDAO.remove(specCategorySelect);
	}
	
	@Transactional
	public SpecCategory save(SpecCategory specCategory) {
		specCategory.setId(specCategoryDAO.findLastId());
		return update(specCategory);
	}

	@Transactional
	public SpecCategory update(SpecCategory specCategory) {
		return specCategoryDAO.save(specCategory);
	}
}