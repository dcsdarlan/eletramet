package com.eletraenergy.configSW.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eletraenergy.configSW.dao.ViewTDSDAO;
import com.eletraenergy.configSW.model.FilterModel;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.TdsStateIdentifier;
import com.eletraenergy.configSW.model.ViewTDS;
import com.eletraenergy.configSW.service.ViewTDSService;

@Service("viewTDSService")
public class ViewTDSServiceImpl implements ViewTDSService {

	@Autowired
	private ViewTDSDAO viewTDSDAO;

	public List<ViewTDS> findConfig(MeterLine meterLine, MeterModel meterModel, String tds, List<FilterModel> filters, TdsStateIdentifier state) {
		return viewTDSDAO.findConfig(meterLine, meterModel, tds, filters, state);
	}

}

