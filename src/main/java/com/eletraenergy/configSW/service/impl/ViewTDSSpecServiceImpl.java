package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eletraenergy.configSW.dao.ViewTDSSpecDAO;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.ViewTDSSpec;
import com.eletraenergy.configSW.service.ViewTDSSpecService;

@Service("viewTDSSpecService")
public class ViewTDSSpecServiceImpl implements ViewTDSSpecService {

	@Autowired
	private ViewTDSSpecDAO viewTDSSpecDAO;

	public List<ViewTDSSpec> findAll() {
		return viewTDSSpecDAO.findAll();
	}

	public List<ViewTDSSpec> findByLine(MeterLine line) {
		return viewTDSSpecDAO.findByLine(line);
	}

	public List<ViewTDSSpec> findByLineAndModel(MeterLine line, MeterModel model) {
		return viewTDSSpecDAO.findByLineAndModel(line, model);
	}
}

