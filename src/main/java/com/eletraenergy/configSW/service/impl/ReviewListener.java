package com.eletraenergy.configSW.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.eletraenergy.configSW.model.Review;
import com.eletraenergy.configSW.model.User;
import com.eletraenergy.configSW.util.Constantes;

public class ReviewListener implements RevisionListener {

	@Override
	public void newRevision(Object reviewObj) {
		if(SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null) {
			Review review = (Review) reviewObj;
			review.setUser((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
			
			//Pegar o identificador de transa��o
			Integer codeTransacation = (Integer) getSession().getAttribute(Constantes.CODE_TRANSACTION);
			String titleTransacation = (String) getSession().getAttribute(Constantes.TITLE_TRANSACTION);
			review.setCodeTransacation(codeTransacation);
			review.setTitleTransacation(titleTransacation);
		}
	}

//	@Override
//    public void entityChanged(@SuppressWarnings("rawtypes") Class entityClass, String entityName,
//                              Serializable entityId, RevisionType revisionType,
//                              Object revisionEntity) {
//        String type = entityClass.getName();
//        ((Review)revisionEntity).addModifiedEntityType(type);
//    }
	
	private HttpServletRequest getRequest() {
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		return requestAttributes.getRequest();
	}

	private HttpSession getSession() {
		return getRequest().getSession();
	}
}
