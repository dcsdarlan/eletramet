package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.MeasuringInstrumentDAO;
import com.eletraenergy.configSW.model.MeasuringInstrument;
import com.eletraenergy.configSW.model.MeasuringInstrumentModel;
import com.eletraenergy.configSW.service.MeasuringInstrumentService;

@Service("MeasuringInstrumentService")
public class MeasuringInstrumentServiceImpl implements MeasuringInstrumentService {

	@Autowired
	private MeasuringInstrumentDAO measuringInstrumentDAO;

	public List<MeasuringInstrument> findAll() {
		return measuringInstrumentDAO.findAll();
	}
	
	public MeasuringInstrument checkSpecificationIdentifier(String specificationIdentifier){
		return measuringInstrumentDAO.checkSpecificationIdentifier(specificationIdentifier);
	}
	
	@Transactional
	public List<MeasuringInstrument> search(MeasuringInstrument measuringInstrumentFilter, Integer first, Integer pageSize) {
		validate(measuringInstrumentFilter);
		return measuringInstrumentDAO.findByParameter(measuringInstrumentFilter, first, pageSize);
	}

	@Transactional
	public int amount(MeasuringInstrument measuringInstrumentFilter) {
		validate(measuringInstrumentFilter);
		measuringInstrumentFilter.setSortField(null);
		measuringInstrumentFilter.setSortOrder(null);
		
		return measuringInstrumentDAO.amount(measuringInstrumentFilter);
	}
	
	private void validate(MeasuringInstrument measuringInstrumentFilter) {
		if (measuringInstrumentFilter.getSpecificationIdentifier()!=null && measuringInstrumentFilter.getSpecificationIdentifier().isEmpty()) {
			measuringInstrumentFilter.setSpecificationIdentifier(null);
		}
	}
	
	@Transactional
	public void remove(MeasuringInstrument measuringInstrumentSelect) {
		measuringInstrumentDAO.remove(measuringInstrumentSelect);
	}
	
	@Transactional
	public MeasuringInstrument save(MeasuringInstrument measuringInstrument) {
		measuringInstrument.setId(measuringInstrumentDAO.findLastId());
		return update(measuringInstrument);
	}

	@Transactional
	public MeasuringInstrument update(MeasuringInstrument measuringInstrument) {
		return measuringInstrumentDAO.save(measuringInstrument);
	}

	@Override
	public List<MeasuringInstrument> findOverdueCalibrations() {
		return measuringInstrumentDAO.findOverdueCalibrations();
	}

	@Override
	public List<MeasuringInstrument> findDelayInTheNext90Days() {
		return measuringInstrumentDAO.findDelayInTheNext90Days();
	}

	@Override
	public List<MeasuringInstrument> findInCalibration() {
		return measuringInstrumentDAO.findInCalibration();
	}

}