package com.eletraenergy.configSW.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.ConfigDAO;
import com.eletraenergy.configSW.dao.ConfigSpecOptionDAO;
import com.eletraenergy.configSW.dao.ConfigSpecRuleDomainDAO;
import com.eletraenergy.configSW.dao.TbMeterCommandDAO;
import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.ConfigSpecOption;
import com.eletraenergy.configSW.model.ConfigSpecRuleDomain;
import com.eletraenergy.configSW.model.ConfigSpecRuleDomainKey;
import com.eletraenergy.configSW.model.SpecRule;
import com.eletraenergy.configSW.model.SpecRuleDomain;
import com.eletraenergy.configSW.model.TbMeterCommand;
import com.eletraenergy.configSW.model.ViewSpec;
import com.eletraenergy.configSW.service.ConfigService;

@Service("configService")
public class ConfigServiceImpl implements ConfigService {

	@Autowired
	private ConfigDAO configDAO;
	@Autowired
	private ConfigSpecOptionDAO configSpecOptionDAO;
	@Autowired
	private ConfigSpecRuleDomainDAO configSpecRuleDomainDAO;
	@Autowired
	private TbMeterCommandDAO tbMeterCommandDAO;

	public List<Config> findAll() {
		return configDAO.findAll();
	}
	
	@Transactional
	public List<ConfigSpecOption> findOptionByConfig(Config config) {
		List<ConfigSpecOption> list = configSpecOptionDAO.findByConfig(config);
		return list;
	}

	public List<TbMeterCommand> findByConfig(Config config) {
		return tbMeterCommandDAO.findByConfig(config);
	}

	public List<TbMeterCommand> findByConfigAndOption(Config config, Long option) {
		return tbMeterCommandDAO.findByConfigAndOption(config, option);
	}

	@Transactional
	public List<Config> findAllAbleToCopy() {
		List<Config> ableConfigs = new ArrayList<Config>();
		List<Config> configs = configDAO.findAll();
		for (Config config : configs) {
			//Inicializa o objeto
			config.getTds().getIdentifier();
//			config.setSpecOptions(configSpecOptionDAO.findByConfig(config));
			if (config.getSpecOptions()!=null && !config.getSpecOptions().isEmpty()) {
				for (ConfigSpecOption c : config.getSpecOptions()) {
					c.getId().toString();
				}
				ableConfigs.add(config);
			}
		}
		return ableConfigs;
	}

	@Transactional
	public List<Config> search(Config configFilter, Integer first, Integer pageSize) {
		validate(configFilter);
		List<Config> configs = configDAO.findByParameter(configFilter, first, pageSize);
		for (Config config : configs) {
			//Inicializa o objeto
			configDAO.initializeAndUnproxy(config.getTds());
		}
		return configs;
	}

	@Transactional
	public int amount(Config configFilter) {
		validate(configFilter);
		configFilter.setSortField(null);
		configFilter.setSortOrder(null);
		
		return configDAO.amount(configFilter);
	}
	
	private void validate(Config configFilter) {
		if (configFilter.getState()!=null && configFilter.getState().isEmpty()) {
			configFilter.setState(null);
		}
	}

	@Transactional
	public void remove(Config configSelect) {
		configDAO.remove(configSelect);
	}
	
	@Transactional
	public Config save(Config config, Map<ViewSpec, List<SpecRule>> rules, List<ViewSpec> options, List<TbMeterCommand> listCommand) {

		config.setId(configDAO.findLastId());

		config = update(config, rules, options, listCommand);
		
		return config;
	}

	@Transactional
	public Config update(Config config, Map<ViewSpec, List<SpecRule>> rules, List<ViewSpec> options, List<TbMeterCommand> listCommand) {
		List<ConfigSpecOption> specOptions = new ArrayList<ConfigSpecOption>();
		config.setSpecOptions(configSpecOptionDAO.findByConfig(config));

		//Verificar as op��es que foram removidas
		for (ConfigSpecOption configSpecOption : config.getSpecOptions()) {
			boolean exists = false;
			for (ViewSpec specOption : options) {
				if (configSpecOption.getSpecOptionCode().equals(specOption.getId())){
					exists = true;
					break;
				}
			}
			if (!exists){
				specOptions.add(configSpecOption);
			}
		}
		config.getSpecOptions().removeAll(specOptions);
		configSpecOptionDAO.remove(specOptions);
		Long lastId = configSpecOptionDAO.findLastId();
		//Verificar as novas op��es adicionadas
		for (ViewSpec specOption : options) {
			boolean exists = false;
			for (ConfigSpecOption configSpecOption : config.getSpecOptions()) {
				if (configSpecOption.getSpecOptionCode().equals(specOption.getId())){
					exists = true;
					break;
				}
			}
			if (!exists){
				ConfigSpecOption configSpecOption = new ConfigSpecOption(lastId++, config, specOption.getId());
				config.getSpecOptions().add(configSpecOption);
			}
		}

		config = configDAO.save(config);

		//Apagar todas as regras anteriores
		configSpecRuleDomainDAO.remove(config);

		for (Map.Entry<ViewSpec,List<SpecRule>> entry : rules.entrySet()) {
			ViewSpec key = entry.getKey();
			if (key.getChecked()) {
				List<SpecRule> specRules = entry.getValue();
				for (SpecRule specRule : specRules) {
					List<SpecRuleDomain> domains = specRule.getDomains();
					for (SpecRuleDomain domain : domains) {
						if (domain.getCheck() != null && domain.getCheck()){
							ConfigSpecRuleDomainKey configSpecRuleDomainKeyRoot = new ConfigSpecRuleDomainKey(config.getId(), key.getSpecCode(), domain.getId());
							ConfigSpecRuleDomain configSpecRuleDomainRoot = new ConfigSpecRuleDomain(configSpecRuleDomainKeyRoot, domain.getText());
							configSpecRuleDomainDAO.save(configSpecRuleDomainRoot);
							
							List<SpecRuleDomain> childs = domain.getChilds();
							for (SpecRuleDomain child : childs) {
								if (child.getCheck() != null && child.getCheck()){
									ConfigSpecRuleDomainKey configSpecRuleDomainKeyParent = new ConfigSpecRuleDomainKey(config.getId(), key.getSpecCode(), child.getId());
									ConfigSpecRuleDomain configSpecRuleDomainParent = new ConfigSpecRuleDomain(configSpecRuleDomainKeyParent, child.getText());
									configSpecRuleDomainDAO.save(configSpecRuleDomainParent);

									List<SpecRuleDomain> c = child.getChilds();
									for (SpecRuleDomain specRuleDomain : c) {
										if (specRuleDomain.getCheck() != null && specRuleDomain.getCheck()) {
											ConfigSpecRuleDomainKey configSpecRuleDomainKey = new ConfigSpecRuleDomainKey(config.getId(), key.getSpecCode(), specRuleDomain.getId());
											ConfigSpecRuleDomain configSpecRuleDomain = new ConfigSpecRuleDomain(configSpecRuleDomainKey, specRuleDomain.getText());
											configSpecRuleDomainDAO.save(configSpecRuleDomain);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		for (TbMeterCommand c : listCommand) {
			if (c.getId()== null){
				Long lastId2 = tbMeterCommandDAO.findLastId();
				c.setId(lastId2);
				c.setConfig(config);
			}
			if (c.getConfigSpecOption() != null){
				for (ConfigSpecOption configSpecOption : config.getSpecOptions()) {
					if (configSpecOption.getSpecOptionCode().equals(c.getConfigSpecOption().getSpecOptionCode())){
						c.setConfigSpecOption(configSpecOption);
					}
				}
			}
			tbMeterCommandDAO.save(c);
		}
		return config;
	}

	public List<ConfigSpecRuleDomain> getSpecRules(Config config) {
		
		return configSpecRuleDomainDAO.findByConfig(config.getId());
	}

	@Transactional
	public void removeCommand(TbMeterCommand c) {
		
		tbMeterCommandDAO.remove(c);
	}
}