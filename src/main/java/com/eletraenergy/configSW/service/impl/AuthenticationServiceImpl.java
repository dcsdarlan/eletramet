package com.eletraenergy.configSW.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.model.User;
import com.eletraenergy.configSW.service.AuthenticationService;
import com.eletraenergy.configSW.service.UserService;
import com.eletraenergy.configSW.util.LDAPServices;
import com.eletraenergy.configSW.util.ResourceBundle;

@Component
public class AuthenticationServiceImpl implements AuthenticationService {

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;
	
	private Date lastAccess; 
	
	@Transactional
	public String login(String username, String password) {
		
		User user = userService.findByLogin(username);
		if (user == null){
			return ResourceBundle.getMessage("invalidLogin");
		}
		
		try {
			// veryfy LDAP
			//TODO::Modificar ap�s restabelecimento da rede
			//LDAPServices.getAuthorization(username, password)
			if (true) {
				// authentic spring security
				UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, "T");
				Authentication authenticate = authenticationManager.authenticate(token);
				SecurityContextHolder.getContext().setAuthentication(authenticate);
	
				user = (User) getUserLogon();
				
				if (!user.isEnabled()) {
					return ResourceBundle.getMessage("userInactive");
				}
	
				if (!user.isAccountNonExpired()) {
					return ResourceBundle.getMessage("passwordExpire");
				}
	
				if (authenticate.isAuthenticated()) {
					
					lastAccess = user.getLastAccess();
					user.setLastAccess(new Date(System.currentTimeMillis()));
					userService.update(user);
					
					return "ok";
				}
				
			}else{
					return ResourceBundle.getMessage("invalidLogin");
			}
		} catch (AuthenticationException e) {
			e.printStackTrace();
		}
		
		return ResourceBundle.getMessage("invalidLogin");
		
	}

	public void logout() {
		SecurityContextHolder.getContext().setAuthentication(null);
		invalidateSession();
	}
	
	public String getLastAccess() {
		
		if (lastAccess==null) {
			return ResourceBundle.getMessage("noPreviousAccess");
		}
		
		return (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).format(lastAccess);
	}
	
	public User getUserLogon() {
		return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	private void invalidateSession() {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
		session.invalidate();
	}

}
