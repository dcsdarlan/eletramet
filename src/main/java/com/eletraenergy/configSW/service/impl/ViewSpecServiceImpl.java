package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eletraenergy.configSW.dao.ViewSpecDAO;
import com.eletraenergy.configSW.model.ViewSpec;
import com.eletraenergy.configSW.service.ViewSpecService;

@Service("viewSpecService")
public class ViewSpecServiceImpl implements ViewSpecService {

	@Autowired
	private ViewSpecDAO viewSpecDAO;

	public List<ViewSpec> findAll() {
		return viewSpecDAO.findAll();
	}
}

