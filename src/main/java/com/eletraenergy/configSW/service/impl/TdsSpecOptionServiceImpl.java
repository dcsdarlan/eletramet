package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.TdsSpecOptionDAO;
import com.eletraenergy.configSW.model.TdsSpecOption;
import com.eletraenergy.configSW.service.TdsSpecOptionService;

@Service("tdsSpecOptionService")
public class TdsSpecOptionServiceImpl implements TdsSpecOptionService {

	@Autowired
	private TdsSpecOptionDAO tdsSpecOptionDAO;

	public List<TdsSpecOption> findAll() {
		return tdsSpecOptionDAO.findAll();
	}

	public TdsSpecOption checkIdentifier(String identifier){
		return tdsSpecOptionDAO.checkIdentifier(identifier);
	}
	
	public TdsSpecOption findByName(String name, String specName){
		return tdsSpecOptionDAO.findByName(name, specName);
	}

	@Transactional
	public List<TdsSpecOption> search(TdsSpecOption tdsSpecOptionFilter, Integer first, Integer pageSize) {
		validate(tdsSpecOptionFilter);
		return tdsSpecOptionDAO.findByParameter(tdsSpecOptionFilter, first, pageSize);
	}

	@Transactional
	public int amount(TdsSpecOption tdsSpecOptionFilter) {
		validate(tdsSpecOptionFilter);
		tdsSpecOptionFilter.setSortField(null);
		tdsSpecOptionFilter.setSortOrder(null);
		
		return tdsSpecOptionDAO.amount(tdsSpecOptionFilter);
	}
	
	private void validate(TdsSpecOption tdsSpecOptionFilter) {
		if (tdsSpecOptionFilter.getName()!=null && tdsSpecOptionFilter.getName().isEmpty()) {
			tdsSpecOptionFilter.setName(null);
			
		}
		if (tdsSpecOptionFilter.getIdentifier()!=null && tdsSpecOptionFilter.getIdentifier().isEmpty()) {
			tdsSpecOptionFilter.setIdentifier(null);
			
		}
	}

	@Transactional
	public void remove(TdsSpecOption tdsSpecOptionSelect) {
		tdsSpecOptionDAO.remove(tdsSpecOptionSelect);
	}
	
	@Transactional
	public TdsSpecOption save(TdsSpecOption tdsSpecOption) {
		tdsSpecOption.setId(tdsSpecOptionDAO.findLastId());
		return update(tdsSpecOption);
	}

	@Transactional
	public TdsSpecOption update(TdsSpecOption tdsSpecOption) {
		return tdsSpecOptionDAO.save(tdsSpecOption);
	}
}