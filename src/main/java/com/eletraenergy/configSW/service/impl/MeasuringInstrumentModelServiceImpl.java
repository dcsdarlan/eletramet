package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.MeasuringInstrumentModelDAO;
import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;
import com.eletraenergy.configSW.model.MeasuringInstrumentModel;
import com.eletraenergy.configSW.service.MeasuringInstrumentModelService;

@Service("MeasuringInstrumentModelService")
public class MeasuringInstrumentModelServiceImpl implements MeasuringInstrumentModelService {

	@Autowired
	private MeasuringInstrumentModelDAO MeasuringInstrumentModelDAO;

	public List<MeasuringInstrumentModel> findbyManufacturer(MeasuringInstrumentManufacturer measuringInstrumentManufacturer) {
		return MeasuringInstrumentModelDAO.findByManufacturer(measuringInstrumentManufacturer);
	}

	public MeasuringInstrumentModel checkName(String name){
		return MeasuringInstrumentModelDAO.checkName(name);
	}
	
	public List<MeasuringInstrumentModel> findAll() {
		return MeasuringInstrumentModelDAO.findAll();
	}

	@Transactional
	public List<MeasuringInstrumentModel> search(MeasuringInstrumentModel modelFilter, Integer first, Integer pageSize) {
		validate(modelFilter);
		return MeasuringInstrumentModelDAO.findByParameter(modelFilter, first, pageSize);
	}

	@Transactional
	public int amount(MeasuringInstrumentModel modelFilter) {
		validate(modelFilter);
		modelFilter.setSortField(null);
		modelFilter.setSortOrder(null);
		
		return MeasuringInstrumentModelDAO.amount(modelFilter);
	}
	
	private void validate(MeasuringInstrumentModel measuringInstrumentModelFilter) {
		if (measuringInstrumentModelFilter.getName()!=null && measuringInstrumentModelFilter.getName().isEmpty()) {
			measuringInstrumentModelFilter.setName(null);
			
		}
	}

	@Transactional
	public void remove(MeasuringInstrumentModel measuringInstrumentModelSelect) {
		MeasuringInstrumentModelDAO.remove(measuringInstrumentModelSelect);
	}
	
	@Transactional
	public MeasuringInstrumentModel save(MeasuringInstrumentModel measuringInstrumentModel) {
		measuringInstrumentModel.setId(MeasuringInstrumentModelDAO.findLastId());
		return update(measuringInstrumentModel);
	}

	@Transactional
	public MeasuringInstrumentModel update(MeasuringInstrumentModel measuringInstrumentModel) {
		return MeasuringInstrumentModelDAO.save(measuringInstrumentModel);
	}
}

