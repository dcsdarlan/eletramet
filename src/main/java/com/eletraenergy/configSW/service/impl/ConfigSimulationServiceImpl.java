package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.ConfigSimulationDAO;
import com.eletraenergy.configSW.model.ConfigSimulation;
import com.eletraenergy.configSW.service.ConfigSimulationService;

@Service("configSimulationService")
public class ConfigSimulationServiceImpl implements ConfigSimulationService {

	@Autowired
	private ConfigSimulationDAO configSimulationDAO;

	public List<ConfigSimulation> findAll() {
		return configSimulationDAO.findAll();
	}
	
	@Transactional
	public List<ConfigSimulation> search(ConfigSimulation configFilter, int first, int pageSize) {
		return configSimulationDAO.findByParameter(configFilter, first, pageSize);
	}

	@Transactional
	public int amount(ConfigSimulation configFilter) {
		configFilter.setSortField(null);
		configFilter.setSortOrder(null);
		
		return configSimulationDAO.amount(configFilter);
	}
	
	@Transactional
	public ConfigSimulation save(ConfigSimulation configSimulation) {
		configSimulation.setId(configSimulationDAO.findLastId());
		return update(configSimulation);
	}

	@Transactional
	public ConfigSimulation update(ConfigSimulation configSimulation) {
		return configSimulationDAO.save(configSimulation);
	}
}

