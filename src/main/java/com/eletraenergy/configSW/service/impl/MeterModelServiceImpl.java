package com.eletraenergy.configSW.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eletraenergy.configSW.dao.MeterModelDAO;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.service.MeterModelService;

@Service("meterModelService")
public class MeterModelServiceImpl implements MeterModelService {

	@Autowired
	private MeterModelDAO meterModelDAO;

	public List<MeterModel> findbyLine(MeterLine line) {
		return meterModelDAO.findByLine(line);
	}

	public MeterModel checkIdentifier(String identifier){
		return meterModelDAO.checkIdentifier(identifier);
	}
	
	public List<MeterModel> findAll() {
		return meterModelDAO.findAll();
	}

	@Transactional
	public List<MeterModel> search(MeterModel modelFilter, Integer first, Integer pageSize) {
		validate(modelFilter);
		return meterModelDAO.findByParameter(modelFilter, first, pageSize);
	}

	@Transactional
	public int amount(MeterModel modelFilter) {
		validate(modelFilter);
		modelFilter.setSortField(null);
		modelFilter.setSortOrder(null);
		
		return meterModelDAO.amount(modelFilter);
	}
	
	private void validate(MeterModel modelFilter) {
		if (modelFilter.getName()!=null && modelFilter.getName().isEmpty()) {
			modelFilter.setName(null);
			
		}
		if (modelFilter.getIdentifier()!=null && modelFilter.getIdentifier().isEmpty()) {
			modelFilter.setIdentifier(null);
			
		}
	}

	@Transactional
	public void remove(MeterModel modelSelect) {
		meterModelDAO.remove(modelSelect);
	}
	
	@Transactional
	public MeterModel save(MeterModel model) {
		model.setId(meterModelDAO.findLastId());
		return update(model);
	}

	@Transactional
	public MeterModel update(MeterModel model) {
		return meterModelDAO.save(model);
	}
}

