package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.ManufactStep;

public interface ManufactStepService {

	List<ManufactStep> findAll();

}
