package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.Review;

public interface ReviewService {

	List<Review> findLast10();
	List<Object> findByReview(Review entidade);
}
