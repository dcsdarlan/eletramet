package com.eletraenergy.configSW.service;

import com.eletraenergy.configSW.model.CalibrationLaboratory;
import com.eletraenergy.configSW.model.CalibrationLaboratoryTelphone;

import java.util.List;

public interface CalibrationLaboratoryTelphoneService {

	List<CalibrationLaboratoryTelphone> findAll();
	
	List<CalibrationLaboratoryTelphone> search(CalibrationLaboratoryTelphone calibrationLaboratoryFilter, Integer first, Integer pageSize);

	void remove(CalibrationLaboratoryTelphone calibrationLaboratorySelect);

	CalibrationLaboratoryTelphone save(CalibrationLaboratoryTelphone calibrationLaboratory);

	CalibrationLaboratoryTelphone update(CalibrationLaboratoryTelphone calibrationLaboratory);
	
	int amount(CalibrationLaboratoryTelphone calibrationLAboratoryFilter);

	void removeByCalibrationLaboratory(CalibrationLaboratory calibrationLaboratorySelect);

	List<CalibrationLaboratoryTelphone> findByCalibrationLaboratory(CalibrationLaboratory calibrationLaboratorySelect);

}
