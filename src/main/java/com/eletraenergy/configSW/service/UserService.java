package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.exception.ConfigSWException;
import com.eletraenergy.configSW.model.Functionality;
import com.eletraenergy.configSW.model.User;

public interface UserService {

	User findByLogin(String username) throws ConfigSWException;

	int amount(User userFilter);

	List<User> search(User userFilter, int first, int pageSize);

	List<User> findAll();

	void remove(User userSelect);

	User save(User user);

	User update(User user);
	
	public List<Functionality> FindAllFunctionalities();


}
