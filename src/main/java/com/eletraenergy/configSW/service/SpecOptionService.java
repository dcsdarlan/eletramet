package com.eletraenergy.configSW.service;

import java.util.List;

import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.Spec;
import com.eletraenergy.configSW.model.SpecOption;

public interface SpecOptionService {

	List<SpecOption> findAll();

	List<SpecOption> search(SpecOption specOptionFilter, Integer first, Integer pageSize);

	void remove(SpecOption specOptionSelect);

	SpecOption findById(Long id);

	SpecOption save(SpecOption specOption, List<Config> configs);

	SpecOption update(SpecOption specOption, List<Config> configs);
	
	SpecOption checkIdentifier(String identifier);
	
	int amount(SpecOption specOptionFilter);

	List<Config>findAllBySpec(Spec spec);
	
	public Boolean isSpecOptionUsed(SpecOption specOption);

}
