package com.eletraenergy.configSW.service;

import java.util.List;
import com.eletraenergy.configSW.model.MeasuringInstrument;

public interface MeasuringInstrumentService {

	List<MeasuringInstrument> findAll();
	
	List<MeasuringInstrument> findOverdueCalibrations();
	
	List<MeasuringInstrument> findDelayInTheNext90Days();
	
	List<MeasuringInstrument> findInCalibration();
	
	List<MeasuringInstrument> search(MeasuringInstrument measuringInstrumentFilter, Integer first, Integer pageSize);

	void remove(MeasuringInstrument measuringInstrumentSelect);

	MeasuringInstrument save(MeasuringInstrument measuringInstrument);

	MeasuringInstrument update(MeasuringInstrument measuringInstrument);
	
	MeasuringInstrument checkSpecificationIdentifier(String specificationIdentifier);
	
	int amount(MeasuringInstrument measuringInstrumentFilter);

}
