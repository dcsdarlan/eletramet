package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.MeasuringInstrumentPlaceUse;

@FacesConverter(value = "measuringInstrumentPlaceUseConverter")
public class MeasuringInstrumentPlaceUseConverter implements Converter {

	private static final Map<MeasuringInstrumentPlaceUse, String> MeasuringInstrumentPlaceUse = new HashMap<MeasuringInstrumentPlaceUse, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<MeasuringInstrumentPlaceUse, String> entry : MeasuringInstrumentPlaceUse.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object measuringInstrumentPlaceUse) {
		if ("".equals(measuringInstrumentPlaceUse.toString())) {
			return "";
		}

		synchronized (MeasuringInstrumentPlaceUse) {
			if (MeasuringInstrumentPlaceUse.containsKey(measuringInstrumentPlaceUse)) {
				MeasuringInstrumentPlaceUse.remove(measuringInstrumentPlaceUse);
			}
			MeasuringInstrumentPlaceUse.put((MeasuringInstrumentPlaceUse) measuringInstrumentPlaceUse, ((MeasuringInstrumentPlaceUse) measuringInstrumentPlaceUse).getId().toString());
			return MeasuringInstrumentPlaceUse.get(measuringInstrumentPlaceUse);
		}
	}
}
