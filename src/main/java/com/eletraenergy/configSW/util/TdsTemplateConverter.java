package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.TdsTemplate;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "tdsTemplateConverter")
public class TdsTemplateConverter implements Converter {

	private static final Map<TdsTemplate, String> TDSTEMPLATE = new HashMap<TdsTemplate, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<TdsTemplate, String> entry : TDSTEMPLATE.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object tdsTemplate) {
		if ("".equals(tdsTemplate.toString())) {
			return "";
		}

		synchronized (TDSTEMPLATE) {
			if (TDSTEMPLATE.containsKey(tdsTemplate)) {
				TDSTEMPLATE.remove(tdsTemplate);
			}
			TDSTEMPLATE.put((TdsTemplate) tdsTemplate, ((TdsTemplate) tdsTemplate).getId().toString());
			return TDSTEMPLATE.get(tdsTemplate);
		}
	}
}
