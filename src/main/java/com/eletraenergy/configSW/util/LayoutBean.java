package com.eletraenergy.configSW.util;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("layoutBean")
@Scope("session")
public class LayoutBean
{
    private String theme = "ui-lightness";
    public String getTheme()
    {
        return theme;
    }
 
}