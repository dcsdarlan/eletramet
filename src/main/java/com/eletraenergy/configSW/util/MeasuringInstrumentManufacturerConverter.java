package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;

@FacesConverter(value = "measuringInstrumentManufacturerConverter")
public class MeasuringInstrumentManufacturerConverter implements Converter {

	private static final Map<MeasuringInstrumentManufacturer, String> MeasuringInstrumentManufacturer = new HashMap<MeasuringInstrumentManufacturer, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<MeasuringInstrumentManufacturer, String> entry : MeasuringInstrumentManufacturer.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object measuringInstrumentManufacturer) {
		if ("".equals(measuringInstrumentManufacturer.toString())) {
			return "";
		}

		synchronized (MeasuringInstrumentManufacturer) {
			if (MeasuringInstrumentManufacturer.containsKey(measuringInstrumentManufacturer)) {
				MeasuringInstrumentManufacturer.remove(measuringInstrumentManufacturer);
			}
			MeasuringInstrumentManufacturer.put((MeasuringInstrumentManufacturer) measuringInstrumentManufacturer, ((MeasuringInstrumentManufacturer) measuringInstrumentManufacturer).getId().toString());
			return MeasuringInstrumentManufacturer.get(measuringInstrumentManufacturer);
		}
	}
}
