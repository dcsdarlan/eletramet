package com.eletraenergy.configSW.util;

import java.security.MessageDigest;

public class HashCodeUtils {

   public static String generateHashCode(String writeFileListSrt) {
       try {
               MessageDigest md = MessageDigest.getInstance("SHA-256");
               md.update(writeFileListSrt.getBytes("UTF-8"));
               byte[] digest = md.digest();
               return String.format("%064x", new java.math.BigInteger(1, digest));
       } catch (Exception e) {
               e.printStackTrace();
               return null;
       }
   }

}