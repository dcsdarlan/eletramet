package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.SpecRuleDomain;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "specRuleDomainConverter")
public class SpecRuleDomainConverter implements Converter {

	private static final Map<SpecRuleDomain, String> SPECS = new HashMap<SpecRuleDomain, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<SpecRuleDomain, String> entry : SPECS.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object specRuleDomain) {
		if ("".equals(specRuleDomain.toString())) {
			return "";
		}

		synchronized (SPECS) {
			if (SPECS.containsKey(specRuleDomain)) {
				SPECS.remove(specRuleDomain);
			}
			SPECS.put((SpecRuleDomain) specRuleDomain, ((SpecRuleDomain) specRuleDomain).getId().toString());
			return SPECS.get(specRuleDomain);
		}
	}
}
