package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.MeterModel;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "meterModelConverter")
public class MeterModelConverter implements Converter {

	private static final Map<MeterModel, String> METERMODELS = new HashMap<MeterModel, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<MeterModel, String> entry : METERMODELS.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object meterModel) {
		if ("".equals(meterModel.toString())) {
			return "";
		}

		synchronized (METERMODELS) {
			if (METERMODELS.containsKey(meterModel)) {
				METERMODELS.remove(meterModel);
			}
			METERMODELS.put((MeterModel) meterModel, ((MeterModel) meterModel).getId().toString());
			return METERMODELS.get(meterModel);
		}
	}
}
