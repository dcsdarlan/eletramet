package com.eletraenergy.configSW.util;

import java.util.Locale;
import java.util.MissingResourceException;
import javax.faces.context.FacesContext;

/**
 *
 * @author Raphael Ferreira
 */
public class ResourceBundle {

	private final static String BUNDLE_NAME = "com.eletraenergy.configSW.bundles.Resources";
	private final static String ENGLISH_LOCALE = "en";
	
    public static String getMessage(String key) {
        return getMessageFromResourceBundle(key);
    }

    private static String getMessageFromResourceBundle(String key) {
        java.util.ResourceBundle bundle = null;
        String message = "";

        try {
            bundle = java.util.ResourceBundle.getBundle(BUNDLE_NAME, getLocale(), getCurrentLoader(BUNDLE_NAME));
            message = bundle.getString(key);
        } catch (MissingResourceException e) {
        }
        return message;
    }

    public static ClassLoader getCurrentLoader(Object fallbackClass) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        return loader==null?fallbackClass.getClass().getClassLoader():loader;
    }
    
    public static Locale getLocale() {
    	return FacesContext.getCurrentInstance().getViewRoot().getLocale();
    }
    
    public static boolean isEnglishLocale() {
    	return getLocale().getLanguage().equals(ENGLISH_LOCALE);
    }
    
}