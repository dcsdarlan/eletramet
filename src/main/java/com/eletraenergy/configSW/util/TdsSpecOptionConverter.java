package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.TdsSpecOption;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "tdsSpecOptionConverter")
public class TdsSpecOptionConverter implements Converter {

	private static final Map<TdsSpecOption, String> SPECSCAT = new HashMap<TdsSpecOption, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<TdsSpecOption, String> entry : SPECSCAT.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object tdsSpecOption) {
		if ("".equals(tdsSpecOption.toString())) {
			return "";
		}

		synchronized (SPECSCAT) {
			if (SPECSCAT.containsKey(tdsSpecOption)) {
				SPECSCAT.remove(tdsSpecOption);
			}
			SPECSCAT.put((TdsSpecOption) tdsSpecOption, ((TdsSpecOption) tdsSpecOption).getId().toString());
			return SPECSCAT.get(tdsSpecOption);
		}
	}
}
