package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.Config;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "configConverter")
public class ConfigConverter implements Converter {

	private static final Map<Config, String> CONFIGS = new HashMap<Config, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<Config, String> entry : CONFIGS.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object config) {
		if ("".equals(config.toString())) {
			return "";
		}

		synchronized (CONFIGS) {
			if (CONFIGS.containsKey(config)) {
				CONFIGS.remove(config);
			}
			CONFIGS.put((Config) config, ((Config) config).getId().toString());
			return CONFIGS.get(config);
		}
	}
}
