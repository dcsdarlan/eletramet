package com.eletraenergy.configSW.util;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

public class ConfigSWExceptionHandlerFactory extends ExceptionHandlerFactory {

	private final ExceptionHandlerFactory delegateFactory;

	public ConfigSWExceptionHandlerFactory(ExceptionHandlerFactory delegateFactory) {
		this.delegateFactory = delegateFactory;
	}

	@Override
	public ExceptionHandler getExceptionHandler() {
		return new ConfigSWExceptionHandler(delegateFactory.getExceptionHandler());
	}
}