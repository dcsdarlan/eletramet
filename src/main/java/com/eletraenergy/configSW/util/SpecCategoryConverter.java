package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.SpecCategory;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "specCategoryConverter")
public class SpecCategoryConverter implements Converter {

	private static final Map<SpecCategory, String> SPECSCAT = new HashMap<SpecCategory, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<SpecCategory, String> entry : SPECSCAT.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object specCategory) {
		if ("".equals(specCategory.toString())) {
			return "";
		}

		synchronized (SPECSCAT) {
			if (SPECSCAT.containsKey(specCategory)) {
				SPECSCAT.remove(specCategory);
			}
			SPECSCAT.put((SpecCategory) specCategory, ((SpecCategory) specCategory).getId().toString());
			return SPECSCAT.get(specCategory);
		}
	}
}
