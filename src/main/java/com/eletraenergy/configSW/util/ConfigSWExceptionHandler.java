package com.eletraenergy.configSW.util;

import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;
import org.springframework.transaction.CannotCreateTransactionException;

import com.eletraenergy.configSW.exception.ConfigSWException;




public class ConfigSWExceptionHandler extends ExceptionHandlerWrapper {

	private final ExceptionHandler wrapped;

	public ConfigSWExceptionHandler(ExceptionHandler wrapped) {
		this.wrapped = wrapped;
	}

	@Override
	public javax.faces.context.ExceptionHandler getWrapped() {
		return wrapped;
	}

	@Override
	public void handle() {
		Iterator<?> eventIt = getUnhandledExceptionQueuedEvents().iterator();

		// Iterate through the queued exceptions
		while (eventIt.hasNext()) {
			ExceptionQueuedEvent event = (ExceptionQueuedEvent) eventIt.next();
			ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();
			Throwable ex = context.getException();
			ex.printStackTrace();

			// See if it's an exception I'm interested in
			try {
				if (getRootCause(ex) != null && getRootCause(ex).getCause() != null) {
					if (getRootCause(ex).getCause() instanceof CannotCreateTransactionException){
						addErrorMessage(ResourceBundle.getMessage("problemBD"));
					}
					if (getRootCause(ex).getCause() instanceof ConfigSWException) {
						String erro = "";
						// Pega o erro do array de mensagens.
						if(((ConfigSWException) getRootCause(ex).getCause()).getMessages()!=null){
							for (String msg : ((ConfigSWException) getRootCause(ex).getCause()).getMessages()) {
								erro += msg + " \n ";
							}
						}
						// Pega o erro da mensagem unica.
						if (((ConfigSWException) getRootCause(ex).getCause()).getMessage() != null) {
							erro += ((ConfigSWException) getRootCause(ex).getCause()).getMessage();
						}
						addErrorMessage(erro);
						
					} else if (getRootCause(ex).getCause() instanceof DataIntegrityViolationException) {
						addErrorMessage(ResourceBundle.getMessage("problemBD"));
					} else if (getRootCause(ex).getCause() instanceof HibernateOptimisticLockingFailureException) {
						addErrorMessage(ResourceBundle.getMessage("anotherUserChanged"));
					} else if (getRootCause(ex).getCause() instanceof EmptyResultDataAccessException) {
						addAvisoMessage(ResourceBundle.getMessage("withoutResult"));
					}
				}
				if (!hasMessages()) {
					addErrorMessage(ResourceBundle.getMessage("exceptionOccurred"));
				}
			} finally {
				eventIt.remove();
			}
		}
		getWrapped().handle();
	}

	private boolean hasMessages() {
		return !FacesContext.getCurrentInstance().getMessageList().isEmpty();
	}

	private void addErrorMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null ));
	}
	private void addAvisoMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message , null));
	}
	
}