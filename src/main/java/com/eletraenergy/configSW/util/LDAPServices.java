package com.eletraenergy.configSW.util;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

/**
 * Created by janio.oliveira on 12/12/2016.
 */
public class LDAPServices {

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static boolean getAuthorization(String astrUser, String astrPassword) {
    	
    	//return true;
    	
    	boolean isValidUser = false;
        
		Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_AUTHENTICATION,"simple");
        env.put(Context.REFERRAL, "follow");
        env.put(Context.SECURITY_PRINCIPAL,"ELETRA\\" + astrUser);
        env.put(Context.SECURITY_CREDENTIALS,astrPassword);
        env.put(Context.PROVIDER_URL,"ldap://192.168.0.2:389");
        try {
            DirContext ctx = new InitialDirContext(env);
            isValidUser = true;
            ctx.close();
        } catch (NamingException e) {
            e.printStackTrace();
        }

        return isValidUser;
    	
    }

}
