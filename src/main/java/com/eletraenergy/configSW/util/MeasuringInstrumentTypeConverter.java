package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.MeasuringInstrumentType;

@FacesConverter(value = "measuringInstrumentTypeConverter")
public class MeasuringInstrumentTypeConverter implements Converter {

	private static final Map<MeasuringInstrumentType, String> MeasuringInstrumentType = new HashMap<MeasuringInstrumentType, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<MeasuringInstrumentType, String> entry : MeasuringInstrumentType.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object measuringInstrumentType) {
		if ("".equals(measuringInstrumentType.toString())) {
			return "";
		}

		synchronized (MeasuringInstrumentType) {
			if (MeasuringInstrumentType.containsKey(measuringInstrumentType)) {
				MeasuringInstrumentType.remove(measuringInstrumentType);
			}
			MeasuringInstrumentType.put((MeasuringInstrumentType) measuringInstrumentType, ((MeasuringInstrumentType) measuringInstrumentType).getId().toString());
			return MeasuringInstrumentType.get(measuringInstrumentType);
		}
	}
}
