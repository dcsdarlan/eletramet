package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.ManufactStep;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "manufactStepConverter")
public class ManufactStepConverter implements Converter {

	private static final Map<ManufactStep, String> MANUFACTSTEP = new HashMap<ManufactStep, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<ManufactStep, String> entry : MANUFACTSTEP.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object manufactStep) {
		if ("".equals(manufactStep.toString())) {
			return "";
		}

		synchronized (MANUFACTSTEP) {
			if (MANUFACTSTEP.containsKey(manufactStep)) {
				MANUFACTSTEP.remove(manufactStep);
			}
			MANUFACTSTEP.put((ManufactStep) manufactStep, ((ManufactStep) manufactStep).getId().toString());
			return MANUFACTSTEP.get(manufactStep);
		}
	}
}
