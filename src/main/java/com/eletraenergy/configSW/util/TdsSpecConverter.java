package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.TdsSpec;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "tdsSpecConverter")
public class TdsSpecConverter implements Converter {

	private static final Map<TdsSpec, String> SPECSCAT = new HashMap<TdsSpec, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<TdsSpec, String> entry : SPECSCAT.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object tdsSpec) {
		if ("".equals(tdsSpec.toString())) {
			return "";
		}

		synchronized (SPECSCAT) {
			if (SPECSCAT.containsKey(tdsSpec)) {
				SPECSCAT.remove(tdsSpec);
			}
			SPECSCAT.put((TdsSpec) tdsSpec, ((TdsSpec) tdsSpec).getId().toString());
			return SPECSCAT.get(tdsSpec);
		}
	}
}
