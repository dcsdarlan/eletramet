package com.eletraenergy.configSW.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

@FacesConverter(value = "measuringInstrumentDepartmentConverter")
public class MeasuringInstrumentDepartmentConverter implements Converter {

	private static final Map<com.eletraenergy.configSW.model.MeasuringInstrumentDepartment, String> MeasuringInstrumentDepartment = new HashMap<com.eletraenergy.configSW.model.MeasuringInstrumentDepartment, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<com.eletraenergy.configSW.model.MeasuringInstrumentDepartment, String> entry : MeasuringInstrumentDepartment.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object measuringInstrumentDepartment) {
		if ("".equals(measuringInstrumentDepartment.toString())) {
			return "";
		}

		synchronized (MeasuringInstrumentDepartment) {
			if (MeasuringInstrumentDepartment.containsKey(measuringInstrumentDepartment)) {
				MeasuringInstrumentDepartment.remove(measuringInstrumentDepartment);
			}
			MeasuringInstrumentDepartment.put((com.eletraenergy.configSW.model.MeasuringInstrumentDepartment) measuringInstrumentDepartment, ((com.eletraenergy.configSW.model.MeasuringInstrumentDepartment) measuringInstrumentDepartment).getId().toString());
			return MeasuringInstrumentDepartment.get(measuringInstrumentDepartment);
		}
	}
}
