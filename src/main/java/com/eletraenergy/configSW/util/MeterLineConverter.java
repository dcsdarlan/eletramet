package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.MeterLine;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "meterLineConverter")
public class MeterLineConverter implements Converter {

	private static final Map<MeterLine, String> METERLINES = new HashMap<MeterLine, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<MeterLine, String> entry : METERLINES.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object meterLine) {
		if ("".equals(meterLine.toString())) {
			return "";
		}

		synchronized (METERLINES) {
			if (METERLINES.containsKey(meterLine)) {
				METERLINES.remove(meterLine);
			}
			METERLINES.put((MeterLine) meterLine, ((MeterLine) meterLine).getId().toString());
			return METERLINES.get(meterLine);
		}
	}
}
