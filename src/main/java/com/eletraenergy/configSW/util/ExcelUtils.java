package com.eletraenergy.configSW.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.HSSFRegionUtil;
import org.apache.poi.hssf.util.Region;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.TdsSpec;
import com.eletraenergy.configSW.model.TdsSpecOption;
//import com.eletraenergy.configSW.model.ViewTdsAud;
import com.eletraenergy.configSW.model.ViewTdsAud;

/**
 * Excel Sheet generation helper utility
 */
public class ExcelUtils {
    private HSSFSheet sheet;
    private HSSFCellStyle csBold;
    private HSSFCellStyle csDate;
	private HSSFCellStyle csHeader;
	private HSSFCellStyle csSpecTypeHeader;
    private HSSFWorkbook wb;
    
    private static String ELETRA_LOGO = "/img/logo_eletra.jpg";
    
    public ExcelUtils(boolean protect) {
		ConfigureHSSF(protect);
    }

    private void ConfigureHSSF(boolean protect) {
        this.wb = new HSSFWorkbook();
        this.sheet = wb.createSheet();
        this.sheet.setDefaultColumnWidth((short) 12);
        if(protect){
    		this.sheet.protectSheet("*(&(*SJK1kb3gayt5zmlaoiohjkhI*(*");
        }
        HSSFFont fBold = wb.createFont();
        fBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        this.csBold = wb.createCellStyle();
        this.csBold.setFont(fBold);
        
        this.csDate = wb.createCellStyle();
        this.csDate.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));	
		
		this.csHeader = wb.createCellStyle();
		this.csHeader.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
		this.csHeader.setAlignment(HSSFCellStyle.ALIGN_LEFT);	
		this.csHeader.setWrapText(true);
		this.csHeader.setFont(fBold);
		this.csHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		this.csHeader.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());			
		
		HSSFFont csSpecTypeFontHeader = wb.createFont();
		csSpecTypeFontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		csSpecTypeFontHeader.setColor(HSSFColor.WHITE.index);
        
		this.csSpecTypeHeader = wb.createCellStyle();
		this.csSpecTypeHeader.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
		this.csSpecTypeHeader.setAlignment(HSSFCellStyle.ALIGN_LEFT);	
		this.csSpecTypeHeader.setWrapText(true);
		this.csSpecTypeHeader.setFont(csSpecTypeFontHeader);
		this.csSpecTypeHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		this.csSpecTypeHeader.setFillForegroundColor(new HSSFColor.GREY_50_PERCENT().getIndex());
	}
		
	private HSSFCell getCell(int row, int col) {
        HSSFRow sheetRow = sheet.getRow(row);
        if (sheetRow == null) {
            sheetRow = sheet.createRow(row);
        }
        HSSFCell cell = sheetRow.getCell((short) col);
        if (cell == null) {
            cell = sheetRow.createCell((short) col);
        }
        return cell;
    }
    
    private void setText(int row, int col, String text) {
        HSSFCell cell = getCell(row, col);
        cell.setCellType(HSSFCell.CELL_TYPE_STRING);
        cell.setCellValue(text);
    }

    private void setTextWithColSpan(int row, int col, String text, int colSpan) {
        HSSFCell cell = getCell(row, col);
        cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		
		if (text!=null) {
			cell.setCellValue(text);	
		}
		
		HSSFCellStyle CellStyle = wb.createCellStyle();
		setCustomStyle(CellStyle, row, col, colSpan);
		CellStyle.setWrapText(true);
		cell.setCellStyle(CellStyle);			
    }
    
    private void setTextWithColSpan(int row, int col, RichTextString text, int colSpan) {
        HSSFCell cell = getCell(row, col);
        cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		
		if (text!=null) {
			cell.setCellValue(text);	
		}
		
		HSSFCellStyle CellStyle = wb.createCellStyle();
		
		setCustomStyle(CellStyle, row, col, colSpan);
		
		CellStyle.setWrapText(true);
		cell.setCellStyle(CellStyle);			
    }
    
    private void setDate(int row, int col, Date date) {
        if (date == null) {
            return;
        }
        HSSFCell cell = getCell(row, col);
        cell.setCellValue(date);
        cell.setCellStyle(csDate);
    }
    
    private void setDateWithColSpan(int row, int col, Date date, int colSpan) {
		HSSFCell cell = getCell(row, col);
		if (date!=null) {
			cell.setCellValue(date);    
        }
		HSSFCellStyle CellStyle = wb.createCellStyle();
		CellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));	
		setCustomStyle(CellStyle, row, col, colSpan);
		cell.setCellStyle(CellStyle);	
    }
	
	private void setDouble(int row, int col, Double value) {
        if (value == null) {
            return;
        }
        HSSFCell cell = getCell(row, col);
        cell.setCellValue(value);
    }

    private void setDoubleWithColSpan(int row, int col, Double value, int colSpan) {
        HSSFCell cell = getCell(row, col);
		if (value!=null) {
			cell.setCellValue(value);
        }
        HSSFCellStyle CellStyle = wb.createCellStyle();
		setCustomStyle(CellStyle, row, col, colSpan);
		cell.setCellStyle(CellStyle);			
    }
    
	private void setCustomStyle(HSSFCellStyle CellStyle, int row, int col, int colSpan) {
		
		if (colSpan>0) {
			int maxCol = col+colSpan-1;
			sheet.addMergedRegion(new Region(row, (short) col, row, (short) maxCol));
		}

		CellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
		CellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);	
	}
	
	private int calculateColAndRowWidth(int width){ 
		if(width > 254) 
				return 65280; // Maximum allowed column width. 
		if(width > 1){ 
				int floor = (int)(Math.floor(((double)width)/5)); 
				int factor = (30*floor); 
				int value = 450 + factor + ((width-1) * 250); 
				return value;	
		} 
		else 
				return 450; // default to column size 1 if zero, one or negative number is passed. 
	}
	
	private void setColumnWidth(int colIdx, int width) 
	{ 
		sheet.setColumnWidth((short) colIdx, (short) calculateColAndRowWidth(width)); 
	}
		
    private void setHeader(int row, int col, String text) {
        HSSFCell cell = getCell(row, col);
        cell.setCellStyle(csBold);
        cell.setCellType(HSSFCell.CELL_TYPE_STRING);
        cell.setCellValue(text);
    }
	
    private void setBox(int row1, int col1, int row2, int col2) {
		// Border Columns
		CellRangeAddress cellRangeAddress1 = new CellRangeAddress(row1, row2, col1, col2);
		HSSFRegionUtil.setBorderTop(HSSFCellStyle.BORDER_MEDIUM, cellRangeAddress1, sheet, wb);
		HSSFRegionUtil.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM, cellRangeAddress1, sheet, wb);
		HSSFRegionUtil.setBorderRight(HSSFCellStyle.BORDER_MEDIUM, cellRangeAddress1, sheet, wb);
		HSSFRegionUtil.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM, cellRangeAddress1, sheet, wb);
    }
    
    private void setItemViewHeader(int row, int col, String text, int colSpan) {
    	
    	if (colSpan>0) {
			int maxCol = col+colSpan-1;
			sheet.addMergedRegion(new Region(row, (short) col, row, (short) maxCol));
		}
    	
        HSSFCell cell = getCell(row, col);
        cell.setCellStyle(csHeader);
        cell.setCellType(HSSFCell.CELL_TYPE_STRING);
        cell.setCellValue(text);
    }	

    private void setTitle(int row, int col, String text) {
    	setTitle(row, col, text, 0, csHeader);
    }
    
    private void setTitle(int row, int col, String text, int colSpan, HSSFCellStyle style) {
    	if (colSpan>0) {
			int maxCol = col+colSpan-1;
			sheet.addMergedRegion(new Region(row, (short) col, row, (short) maxCol));
		}
        HSSFCell cell = getCell(row, col);
        
        cell.setCellStyle(style);
        cell.setCellType(HSSFCell.CELL_TYPE_STRING);
        cell.setCellValue(text);
    }
    
    private void setTitle(int row, int col, String text, int colSpan) {
    	setTitle(row, col, text, colSpan, csHeader);
    }
    
    private void setItemViewHeader(int row, int col, String text) {
    	setItemViewHeader(row, col, text, 0);
    }	
    
    private void addPicture(String picture) {
    	
		try {
			InputStream inputStream = this.getClass().getResourceAsStream(picture);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] b = new byte[1024];
			int bytesRead;
			while ((bytesRead = inputStream.read(b)) != -1) {
			   bos.write(b, 0, bytesRead);
			}
			byte[] bytes = bos.toByteArray();
			
	    	final CreationHelper helper = wb.getCreationHelper();
	    	final Drawing drawing = sheet.createDrawingPatriarch();
	    	final ClientAnchor anchor = helper.createClientAnchor();
	    	final int pictureIndex = wb.addPicture( bytes, Workbook.PICTURE_TYPE_JPEG);
	    	
	    	anchor.setAnchorType( ClientAnchor.DONT_MOVE_AND_RESIZE );
	    	anchor.setCol1(0);
	    	anchor.setRow1(0);
	    	anchor.setDx1(30);
	    	anchor.setDy1(30);
	    	
	    	final Picture pict = drawing.createPicture( anchor, pictureIndex );
	    	pict.resize(0.5);
	    	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void setTdsHeader() {
    	
    	HSSFCell cell = getCell(0, 0);
        cell.setCellType(HSSFCell.CELL_TYPE_STRING);
        cell.setCellValue(ResourceBundle.getMessage("tdsVerbose"));
        
        HSSFCellStyle CellStyle = wb.createCellStyle();
		sheet.addMergedRegion(new Region(0, (short) 0, 4, (short) 2));
		
		CellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		CellStyle.setFillForegroundColor(new HSSFColor.AQUA().getIndex());
		
		CellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		CellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		
		HSSFFont txtFont = (HSSFFont)wb.createFont();
		txtFont.setFontName("Arial");
		txtFont.setFontHeightInPoints((short)16);
		txtFont.setBoldweight((short)4.5);
		CellStyle.setFont(txtFont);
		CellStyle.setWrapText(true);
		
		CellStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		CellStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		CellStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		CellStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		CellStyle.setBottomBorderColor(new HSSFColor.BLACK().getIndex());
		CellStyle.setLeftBorderColor(new HSSFColor.BLACK().getIndex());
		CellStyle.setRightBorderColor(new HSSFColor.BLACK().getIndex());
		CellStyle.setTopBorderColor(new HSSFColor.BLACK().getIndex());
		
		cell.setCellStyle(CellStyle);			
    	
		addPicture(ELETRA_LOGO);
		
		// Border Columns
		setBox(0,0,4,2);
    }
    
    public HSSFWorkbook exportTdsToExcel(List<TdsSpec> listOfTdsSpec, List<TdsSpecOption> listOfTdsSpecOption, 
    		Tds tds, ViewTdsAud viewTdsAud) {
    	
    	final int INFO_SPEC_TYPE = 0;
    	final int CONF_SPEC_TYPE = 1;
    	
    	setTdsHeader();
    	
    	wb.setSheetName(0, ResourceBundle.getMessage("tdsNumber") + " (" + tds.getIdentifier() + ")");
    	
    	
		//Header Column
  		setColumnWidth(0,30);
  		setColumnWidth(1,30);
  		setColumnWidth(2,30);
		
		// Fixed Columns
  		setTitle(5, 0, ResourceBundle.getMessage("meterLine"));
  		setTitle(5, 1, ResourceBundle.getMessage("meterModel"));
  		setTitle(5, 2, ResourceBundle.getMessage("tdsNumber"));
		
  		setTitle(6, 0, tds.getMeterModel().getMeterLine().getName());
  		setTitle(6, 1, tds.getMeterModel().getName());
  		setTitle(6, 2, tds.getIdentifier());
		
		// Border Columns
		setBox(5,0,6,2);
		
		int row = 7;
		int specTypeTitleRow = 0;
				
		setTitle(row, 0, ResourceBundle.getMessage("information").toUpperCase(),3, csSpecTypeHeader);
		
		specTypeTitleRow = row;
		
		// Border Columns
		setBox(row,0,row,2);
		
		row++;
		
		row = printSpecs(row,INFO_SPEC_TYPE,listOfTdsSpec,listOfTdsSpecOption,tds);
		
		// Border Columns
		setBox(specTypeTitleRow+1,0,row,2);
		
		setTitle(row, 0, ResourceBundle.getMessage("configuration").toUpperCase(),3, csSpecTypeHeader);
		
		specTypeTitleRow = row;
		
		// Border Columns
		setBox(row,0,row,2);
		
		row++;
		
		row = printSpecs(row,CONF_SPEC_TYPE,listOfTdsSpec,listOfTdsSpecOption,tds);
		
		// Border Columns
		setBox(specTypeTitleRow+1,0,row,2);
		
		// Border Columns
		setBox(7,0,row-1,2);
		
		setTitle(row, 0, ResourceBundle.getMessage("lastModifiedBy"));
		setTitle(row, 1, ResourceBundle.getMessage("lastModifiedOn"));
		setTitle(row, 2, ResourceBundle.getMessage("revisionNumber"));
		
		if (viewTdsAud==null) {
			setTextWithColSpan(row+1, 0, "-" ,0);
			setTextWithColSpan(row+1, 1, "-" ,0);
			setTextWithColSpan(row+1, 2, "-" ,0);
		} else {
			setTextWithColSpan(row+1, 0, viewTdsAud.getIdUserLdap(),0);
			setTextWithColSpan(row+1, 1, (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).format(viewTdsAud.getChangeModified()),0);
			setTextWithColSpan(row+1, 2, String.valueOf(viewTdsAud.getChangeVersion()),0);	
		}
		
		
		
		// Border Columns
		setBox(row,0,row+1,2);
		
        return wb;
    }
    
    private int printSpecs(int row, int SpecType,
    		List<TdsSpec> listOfTdsSpec, List<TdsSpecOption> listOfTdsSpecOption, Tds tds) {
    	
    	List<SpecEntry> specEntries = buildSpecEntries(SpecType, listOfTdsSpec, listOfTdsSpecOption, tds);
    	
        for(SpecEntry specEntry: specEntries) {
        	
        	String specName = specEntry.specIntName;
        	if ((!specEntry.specName.equals(specEntry.specIntName)) &&
        			(specEntry.specNameCn!=null)) {
        		specName+= " (" + specEntry.specNameCn + ")";
        	}
        	
        	setTitle(row, 0, specName,3);
			row++;
        
	        HSSFFont redFont = wb.createFont();
	        redFont.setColor(HSSFColor.RED.index);
	        redFont.setBoldweight((HSSFFont.BOLDWEIGHT_BOLD));
	        HSSFRichTextString richString = new HSSFRichTextString(specEntry.specOptions);
	        richString.applyFont(specEntry.indexOfSelectedItem, specEntry.indexOfSelectedItem+3, redFont);
			setTextWithColSpan(row, 0, richString,3);
			
			row++;
        }
        
        return row;
    }
    
    private List<SpecEntry> buildSpecEntries(int SpecType, List<TdsSpec> listOfTdsSpec, List<TdsSpecOption> listOfTdsSpecOption, 
    		Tds tds) {
    	
    	final String selectedItem = "[X]";
    	final String unselectedItem = "[  ]";
    	final int minSpecOptionsSize = SpecType==0?1:2;
    	final int maxSpecOptionsSize = SpecType==0?1:Byte.MAX_VALUE;
    	
    	
    	List<SpecEntry> specEntries = new ArrayList<SpecEntry>();
    	
        for(TdsSpec tdsSpec: listOfTdsSpec) {
        	
        	List<String> tdsSpecOptions = new ArrayList<String>();

			for(TdsSpecOption tdsSpecOption: listOfTdsSpecOption) {
				if (tdsSpecOption.getTdsSpec().equals(tdsSpec)) {
					tdsSpecOptions.add(tdsSpecOption.getIntName());
				}
			}
			
			if ((!tdsSpecOptions.isEmpty()) && 
					(tdsSpecOptions.size()<=maxSpecOptionsSize && tdsSpecOptions.size()>=minSpecOptionsSize)) {
				
				Collections.sort(tdsSpecOptions);
				
				String selectedTdsSpecOption = null;
				for(TdsSpecOption tdsSpecOption:  tds.getTdsSpecOptions()) {
					if (tdsSpecOption.getTdsSpec().equals(tdsSpec)) {
						selectedTdsSpecOption = tdsSpecOption.getIntName();
						break;
					}
				}
				
				String tdsSpecOptionsConcat = StringUtils.EMPTY;
				for(String tdsSpecOption: tdsSpecOptions) {
					tdsSpecOptionsConcat+= (tdsSpecOption.equals(selectedTdsSpecOption)?selectedItem:unselectedItem) +
							" " + tdsSpecOption + "  ";
				}
				
				specEntries.add(new SpecEntry(tdsSpec.getName(), tdsSpec.getIntName(), tdsSpec.getNameCn(), tdsSpecOptionsConcat, tdsSpecOptionsConcat.indexOf(selectedItem)));
				
				tdsSpecOptions.clear();
			}
		}
    	
        Collections.sort(specEntries);
        
        return specEntries;
    }
        
    class SpecEntry implements Comparable<SpecEntry> {
    	String specName;
    	String specIntName;
    	String specNameCn;
    	String specOptions;
    	int indexOfSelectedItem;
    	
    	SpecEntry(String specName, String specIntName, String specNameCn, String specOptions, int indexOfSelectedItem) {
        	this.specName = specName;
        	this.specIntName = specIntName;
        	this.specNameCn = specNameCn;
        	this.specOptions = specOptions;    		
        	this.indexOfSelectedItem = indexOfSelectedItem;
    	}

		public int compareTo(SpecEntry o) {
			return this.specName.compareTo(o.specName);
		}
    }

}