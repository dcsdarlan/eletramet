package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.Tds;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "tdsConverter")
public class TdsConverter implements Converter {

	private static final Map<Tds, String> TDS = new HashMap<Tds, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<Tds, String> entry : TDS.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object tds) {
		if ("".equals(tds.toString())) {
			return "";
		}

		synchronized (TDS) {
			if (TDS.containsKey(tds)) {
				TDS.remove(tds);
			}
			TDS.put((Tds) tds, ((Tds) tds).getId().toString());
			return TDS.get(tds);
		}
	}
}
