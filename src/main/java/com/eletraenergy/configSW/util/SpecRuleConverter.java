package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.SpecRule;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "specRuleConverter")
public class SpecRuleConverter implements Converter {

	private static final Map<SpecRule, String> SPECS = new HashMap<SpecRule, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<SpecRule, String> entry : SPECS.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object specRule) {
		if ("".equals(specRule.toString())) {
			return "";
		}

		synchronized (SPECS) {
			if (SPECS.containsKey(specRule)) {
				SPECS.remove(specRule);
			}
			SPECS.put((SpecRule) specRule, ((SpecRule) specRule).getId().toString());
			return SPECS.get(specRule);
		}
	}
}
