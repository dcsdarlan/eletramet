package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.Spec;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "specConverter")
public class SpecConverter implements Converter {

	private static final Map<Spec, String> SPECS = new HashMap<Spec, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<Spec, String> entry : SPECS.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object spec) {
		if ("".equals(spec.toString())) {
			return "";
		}

		synchronized (SPECS) {
			if (SPECS.containsKey(spec)) {
				SPECS.remove(spec);
			}
			SPECS.put((Spec) spec, ((Spec) spec).getId().toString());
			return SPECS.get(spec);
		}
	}
}
