package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.MeasuringInstrumentStandard;

@FacesConverter(value = "measuringInstrumentStandardConverter")
public class MeasuringInstrumentStandardConverter implements Converter {

	private static final Map<MeasuringInstrumentStandard, String> MeasuringInstrumentStandard = new HashMap<MeasuringInstrumentStandard, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<MeasuringInstrumentStandard, String> entry : MeasuringInstrumentStandard.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object measuringInstrumentStandard) {
		if ("".equals(measuringInstrumentStandard.toString())) {
			return "";
		}

		synchronized (MeasuringInstrumentStandard) {
			if (MeasuringInstrumentStandard.containsKey(measuringInstrumentStandard)) {
				MeasuringInstrumentStandard.remove(measuringInstrumentStandard);
			}
			MeasuringInstrumentStandard.put((MeasuringInstrumentStandard) measuringInstrumentStandard, ((MeasuringInstrumentStandard) measuringInstrumentStandard).getId().toString());
			return MeasuringInstrumentStandard.get(measuringInstrumentStandard);
		}
	}
}
