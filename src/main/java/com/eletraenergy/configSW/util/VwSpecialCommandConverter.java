package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.ViewSpecialCommand;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "vwSpecialCommandConverter")
public class VwSpecialCommandConverter implements Converter {

	private static final Map<ViewSpecialCommand, String> SPECSCAT = new HashMap<ViewSpecialCommand, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<ViewSpecialCommand, String> entry : SPECSCAT.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object viewSpecialCommand) {
		if ("".equals(viewSpecialCommand.toString())) {
			return "";
		}

		synchronized (SPECSCAT) {
			if (SPECSCAT.containsKey(viewSpecialCommand)) {
				SPECSCAT.remove(viewSpecialCommand);
			}
			SPECSCAT.put((ViewSpecialCommand) viewSpecialCommand, ((ViewSpecialCommand) viewSpecialCommand).getId().toString());
			return SPECSCAT.get(viewSpecialCommand);
		}
	}
}
