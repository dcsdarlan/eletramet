package com.eletraenergy.configSW.util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;

import org.springframework.security.core.context.SecurityContextHolder;

import com.eletraenergy.configSW.model.User;


/**
 * Classe utilit�ria
 * @author Raphael
 * @since 08/06/2017
 *
 */
public class ConfigSWUtils {

	public static final String FORMATO_DATA = "dd/MM/yyyy"; 
	
	/**
	 * Formata um valor para o real(R$)
	 * @param valor
	 * @return valor formatado
	 */
	 public static String formataValor(Double valor) {
		 
		 NumberFormat formato = NumberFormat.getInstance(); 
		 formato.setMinimumFractionDigits(2);  
		 if(valor != null) {			  
			 return formato.format(valor);
		 } else {
			 return formato.format(0.0);
		 }
	 }
	 
	 /**
	  * Formata uma data de acordo com o par�metro passado
	  * Ex. par�metro formato: dd/MM/yyyy, dd/MM/yyyy HH:mm:ss, dd/MM
	  * @param formato
	  * @param data
	  * @return data formatada
	  */
	 public static String formataData(String formato, Date data) {
		 
		 SimpleDateFormat sdf = new SimpleDateFormat(formato);
		 
		 return sdf.format(data);
		 
	 }
	 
	 /**
	  * Recebe o m�s ordinal e o transforma em extenso
	  * @param mes
	  * @return
	  * @throws ParseException
	  */
	 public static String converteMesEmExtenso(String mes)
			throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("MM",new Locale("pt","br"));
		Date mesDate = sdf.parse(mes);
		sdf.applyPattern("MMMM");
		return sdf.format(mesDate);
	}
	 
	/**
	 * Obt�m o usu�rio logado na sess�o
	 * @return
	 */
	public static User getUserLogado() {
		if (SecurityContextHolder.getContext().getAuthentication() == null) {
			return null;
		}
		return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	
	public static void cleanSubmittedValues(UIComponent component) {
		if (component instanceof EditableValueHolder) {
			EditableValueHolder evh = (EditableValueHolder) component;
			evh.setSubmittedValue(null);
			evh.setValue(null);
			evh.setLocalValueSet(false);
			evh.setValid(true);
		}
		if(component.getChildCount()>0){
			for (UIComponent child : component.getChildren()) {
				cleanSubmittedValues(child);
			}
		}
	}
}
