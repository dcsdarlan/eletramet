package com.eletraenergy.configSW.util;

import com.eletraenergy.configSW.model.CalibrationLaboratoryTelphone;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("telphoneConverter")
public class TelphoneConverter implements Converter {

    public String getAsString(FacesContext context, UIComponent component,
                              Object value) {
        if (value == null) {
            return null;
        }
        CalibrationLaboratoryTelphone telefone = (CalibrationLaboratoryTelphone) value;
        return "(" + telefone.getDdd() + ")" + telefone.getNumber();
    }

    public Object getAsObject(FacesContext context, UIComponent component,
                              String value) {
        if (value == null) {
            return null;
        }

        try {
            String ddd = value.substring(0, 1);
            String numero = value.substring(1);

            CalibrationLaboratoryTelphone telefone = new CalibrationLaboratoryTelphone();
            telefone.setDdd(ddd);
            telefone.setNumber(numero);

            return telefone;
        } catch (IndexOutOfBoundsException e) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Problemas na convers�o do telefone.",
                    "Ele deve ser informado com o ddd do estado de origem. Ex: (85)99999999");

            throw new ConverterException(facesMessage);
        }
    }
}