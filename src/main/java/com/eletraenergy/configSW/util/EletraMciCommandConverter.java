package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.EletraMciCommand;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "eletraMciCommand")
public class EletraMciCommandConverter implements Converter {

	private static final Map<EletraMciCommand, String> COMMANDS = new HashMap<EletraMciCommand, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<EletraMciCommand, String> entry : COMMANDS.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object eletraMciCommand) {
		if ("".equals(eletraMciCommand.toString())) {
			return "";
		}

		synchronized (COMMANDS) {
			if (COMMANDS.containsKey(eletraMciCommand)) {
				COMMANDS.remove(eletraMciCommand);
			}
			COMMANDS.put((EletraMciCommand) eletraMciCommand, ((EletraMciCommand) eletraMciCommand).getCommandIdentifier());
			return COMMANDS.get(eletraMciCommand);
		}
	}
}
