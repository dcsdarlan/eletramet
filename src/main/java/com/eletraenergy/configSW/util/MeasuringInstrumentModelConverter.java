package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.MeasuringInstrumentModel;

@FacesConverter(value = "measuringInstrumentModelConverter")
public class MeasuringInstrumentModelConverter implements Converter {

	private static final Map<MeasuringInstrumentModel, String> MeasuringInstrumentModel = new HashMap<MeasuringInstrumentModel, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<MeasuringInstrumentModel, String> entry : MeasuringInstrumentModel.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object measuringInstrumentModel) {
		if ("".equals(measuringInstrumentModel.toString())) {
			return "";
		}

		synchronized (MeasuringInstrumentModel) {
			if (MeasuringInstrumentModel.containsKey(measuringInstrumentModel)) {
				MeasuringInstrumentModel.remove(measuringInstrumentModel);
			}
			MeasuringInstrumentModel.put((MeasuringInstrumentModel) measuringInstrumentModel, ((MeasuringInstrumentModel) measuringInstrumentModel).getId().toString());
			return MeasuringInstrumentModel.get(measuringInstrumentModel);
		}
	}
}
