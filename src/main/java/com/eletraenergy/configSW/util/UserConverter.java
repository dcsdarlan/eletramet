package com.eletraenergy.configSW.util;

import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;
import com.eletraenergy.configSW.model.User;
import com.eletraenergy.configSW.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

@FacesConverter(value = "userConverter")
public class UserConverter implements Converter {

	private static final Map<User, String> MeasuringInstrumentManufacturer = new HashMap<User, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<User, String> entry : MeasuringInstrumentManufacturer.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object measuringInstrumentManufacturer) {
		if ("".equals(measuringInstrumentManufacturer.toString())) {
			return "";
		}

		synchronized (MeasuringInstrumentManufacturer) {
			if (MeasuringInstrumentManufacturer.containsKey(measuringInstrumentManufacturer)) {
				MeasuringInstrumentManufacturer.remove(measuringInstrumentManufacturer);
			}
			MeasuringInstrumentManufacturer.put((User) measuringInstrumentManufacturer, ((User) measuringInstrumentManufacturer).getId().toString());
			return MeasuringInstrumentManufacturer.get(measuringInstrumentManufacturer);
		}
	}
}
