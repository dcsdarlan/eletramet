package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.SchemaCommand;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "schemaCommand")
public class SchemaCommandConverter implements Converter {

	private static final Map<SchemaCommand, String> SCHEMAS = new HashMap<SchemaCommand, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<SchemaCommand, String> entry : SCHEMAS.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object schemaCommand) {
		if ("".equals(schemaCommand.toString())) {
			return "";
		}

		synchronized (SCHEMAS) {
			if (SCHEMAS.containsKey(schemaCommand)) {
				SCHEMAS.remove(schemaCommand);
			}
			SCHEMAS.put((SchemaCommand) schemaCommand, ((SchemaCommand) schemaCommand).getSchemaIdentifier());
			return SCHEMAS.get(schemaCommand);
		}
	}
}
