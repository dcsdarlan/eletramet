package com.eletraenergy.configSW.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.eletraenergy.configSW.model.EletraMciAttribute;

/**
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "eletraMciAttribute")
public class EletraMciAttributeConverter implements Converter {

	private static final Map<EletraMciAttribute, String> ATTRIBS = new HashMap<EletraMciAttribute, String>();

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValue) {

		if ("".equals(stringValue)) {
			return null;
		}
		for (Entry<EletraMciAttribute, String> entry : ATTRIBS.entrySet()) {
			if (entry.getValue().equals(stringValue)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Error on converter " + getClass().getName());
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object eletraMciAttribute) {
		if ("".equals(eletraMciAttribute.toString())) {
			return "";
		}

		synchronized (ATTRIBS) {
			if (ATTRIBS.containsKey(eletraMciAttribute)) {
				ATTRIBS.remove(eletraMciAttribute);
			}
			ATTRIBS.put((EletraMciAttribute) eletraMciAttribute, ((EletraMciAttribute) eletraMciAttribute).getAttributeIdentifier());
			return ATTRIBS.get(eletraMciAttribute);
		}
	}
}
