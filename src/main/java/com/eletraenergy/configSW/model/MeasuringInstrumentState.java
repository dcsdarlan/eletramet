package com.eletraenergy.configSW.model;

public interface MeasuringInstrumentState {

	void collected(MeasuringInstrument measuringInstrument);
	void inUse(MeasuringInstrument measuringInstrument);
	void registered(MeasuringInstrument measuringInstrument);
	void intCalibration(MeasuringInstrument measuringInstrument);
	void inRepair(MeasuringInstrument measuringInstrument);
	void dropOff(MeasuringInstrument measuringInstrument);
	
	MeasuringInstrumentStateIdentifier[] getPossibleStates();
		
}
