package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RevisionType;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.util.ResourceBundle;
import com.google.gson.annotations.Expose;

@Audited
@AuditTable(value = "tb_calibration_laboratory_aud")
@Entity
@Table(name = "tb_calibration_laboratory")
@NamedQueries({
		@NamedQuery(name = "CalibrationLaboratory.findAll", query = "SELECT c FROM CalibrationLaboratory c ORDER BY c.id ASC"),
		@NamedQuery(name = "CalibrationLaboratory.lastId", query = "SELECT max(c.id) FROM CalibrationLaboratory c"),
		@NamedQuery(name = "CalibrationLaboratory.remove", query = "DELETE FROM CalibrationLaboratory c WHERE c.id = :pId")
})

public class CalibrationLaboratory extends Model implements Serializable {

	private static final long serialVersionUID = -3176063343118662782L;

	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "cnpj")
	private String cnpj;

	@Column(name = "email")
	private String email;

	@Column(name = "tel1")
	private String tel1;

	@Column(name = "tel2")
	private String tel2;

	@Column(name = "tel3")
	private String tel3;

	@Column(name = "contact")
	private String contact;

	@Column(name = "place")
	private String place;

	@Column(name = "number")
	private String number;

	@Column(name = "complement")
	private String complement;

	@Column(name = "neighborhood")
	private String neighborhood;

	@Column(name = "city")
	private String city;

	@Column(name = "country")
	private String country;

	@Column(name = "cep")
	private String cep;

	@OneToMany(mappedBy = "calibration_laboratory",
			targetEntity = CalibrationLaboratoryTelphone.class,
			fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<CalibrationLaboratoryTelphone> phones;

	@Transient
	private SortOrder sortOrder;

	@Transient
	private String sortField;

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getTel1() {
		return tel1;
	}

	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}

	public String getTel2() {
		return tel2;
	}

	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}

	public String getTel3() {
		return tel3;
	}

	public void setTel3(String tel3) {
		this.tel3 = tel3;
	}
}
