package com.eletraenergy.configSW.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.annotations.Subselect;

@Entity
@Table(name = "vw_tds_spec")
@Subselect("select * from vw_tds_spec")
@NamedQueries({
	@NamedQuery(name = "ViewTDSSpec.findAll", 
			   query = "SELECT new com.eletraenergy.configSW.model.ViewTDSSpec(tds_spec.id, tds_spec.identifier, tds_spec.name, tds_spec.optionCode, tds_spec.optionIdentifier, tds_spec.optionName)  from ViewTDSSpec tds_spec"),
	@NamedQuery(name = "ViewTDSSpec.byLine", 
			   query = "SELECT new com.eletraenergy.configSW.model.ViewTDSSpec(tds_spec.id, tds_spec.identifier, tds_spec.name, tds_spec.optionCode, tds_spec.optionIdentifier, tds_spec.optionName)  from ViewTDSSpec tds_spec WHERE EXISTS (select 1 from ViewTDS vtds where vtds.tdsSpecCode = tds_spec.id and vtds.meterLineIdentifier = :pMeterLine)"), 			  
	@NamedQuery(name = "ViewTDSSpec.byLineAndModel", 
		query = "SELECT new com.eletraenergy.configSW.model.ViewTDSSpec(tds_spec.id, tds_spec.identifier, tds_spec.name, tds_spec.optionCode, tds_spec.optionIdentifier, tds_spec.optionName)  from ViewTDSSpec tds_spec WHERE EXISTS (SELECT 1 FROM ViewTDS vtds where vtds.tdsSpecCode = tds_spec.id and vtds.meterLineIdentifier = :pMeterLine and vtds.meterModelIdentifier = :pMeterModel)")
   })
public class ViewTDSSpec extends Model implements Serializable {

	private static final long serialVersionUID = 4190201801306753218L;

	public ViewTDSSpec(){
		
	}
	
	public ViewTDSSpec(Long id, String identifier, String name,
			Integer optionCode, String optionIdentifier, String optionName) {
		super();
		this.id = id;
		this.identifier = identifier;
		this.name = name;
		this.optionCode = optionCode;
		this.optionIdentifier = optionIdentifier;
		this.optionName = optionName;
	}

	@Id
	@Column(name = "spec_code")
	private Long id;

	@Column(name = "spec_identifier")
	private String identifier;

	@Column(name = "spec_name")
	private String name;
	
	@Column(name = "spec_option_code")
	private Integer optionCode;

	@Column(name = "spec_option_identifier")
	private String optionIdentifier;

	@Column(name = "spec_option_name")
	private String optionName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Integer getOptionCode() {
		return optionCode;
	}

	public void setOptionCode(Integer optionCode) {
		this.optionCode = optionCode;
	}

	public String getOptionIdentifier() {
		return optionIdentifier;
	}

	public void setOptionIdentifier(String optionIdentifier) {
		this.optionIdentifier = optionIdentifier;
	}

	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

}
