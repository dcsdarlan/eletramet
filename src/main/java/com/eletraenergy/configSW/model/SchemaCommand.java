package com.eletraenergy.configSW.model;

import java.util.ArrayList;

public class SchemaCommand {
	private String schemaIdentifier;
	private String schemaName;
	private ArrayList<EletraMciCommand> listOfEletraMciCommand = new ArrayList<EletraMciCommand>();
	public String getSchemaIdentifier() {
		return schemaIdentifier;
	}
	public void setSchemaIdentifier(String schemaIdentifier) {
		this.schemaIdentifier = schemaIdentifier;
	}
	public String getSchemaName() {
		return schemaName;
	}
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}
	public ArrayList<EletraMciCommand> getListOfEletraMciCommand() {
		return listOfEletraMciCommand;
	}
	public void setListOfEletraMciCommand(ArrayList<EletraMciCommand> listOfEletraMciCommand) {
		this.listOfEletraMciCommand = listOfEletraMciCommand;
	}
	@Override
	public String toString() {
		return "ConfigTechCommand [schemaIdentifier=" + schemaIdentifier
				+ ", schemaName=" + schemaName + ", listOfEletraMciCommand="
				+ listOfEletraMciCommand + "]";
	}
	
}
