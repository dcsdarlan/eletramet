package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.List;

import com.eletraenergy.configSW.util.ResourceBundle;

public class FilterTdsTemplateModel implements Serializable {
	  
	private static final long serialVersionUID = -2727832094197115100L;

	private String header;
	private String headerEn;
    private Boolean headerCheck;
    private TdsSpecOption[] selectedOptions;
	private List<TdsSpecOption> options;
	
    public FilterTdsTemplateModel(String header,  String headerEn, List<TdsSpecOption> options) {
        this.header = header;
        this.options = options;
        this.headerEn = headerEn;
    }

    public TdsSpecOption[] getSelectedOptions() {
        return selectedOptions;
    }
 
    public void setSelectedOptions(TdsSpecOption[] selectedOptions) {
        this.selectedOptions = selectedOptions;
    }
    
    public String getHeader() {
        return header;
    }

	public List<TdsSpecOption> getOptions() {
		return options;
	}

    public Boolean getHeaderCheck() {
		return headerCheck;
	}

	public void setHeaderCheck(Boolean headerCheck) {
		this.headerCheck = headerCheck;
	}

	public String getHeaderEn() {
		return headerEn;
	}

	public void setHeaderEn(String headerEn) {
		this.headerEn = headerEn;
	}

	public String getIntHeader() {
		return (ResourceBundle.isEnglishLocale() ? headerEn :header);
	}

}	
