package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RevisionType;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.util.ResourceBundle;
import com.google.gson.annotations.Expose;

@Entity
@Audited
@AuditTable(value = "tb_tds_template_aud")
@Table(name = "tb_tds_template")
@NamedQueries({
	@NamedQuery(name = "TdsTemplate.byMeterModel", query = "SELECT t FROM TdsTemplate t WHERE t.meterModel = :pMeterModel"),
	@NamedQuery(name = "TdsTemplate.findByModelOrderIdentifier", query = "SELECT t FROM TdsTemplate t WHERE t.meterModel = :pMeterModel ORDER BY t.identifier DESC"),
	@NamedQuery(name = "TdsTemplate.findAll", query = "SELECT t FROM TdsTemplate t ORDER BY t.meterModel ASC"),
	@NamedQuery(name = "TdsTemplate.lastId", query = "SELECT max(t.id) FROM TdsTemplate t"),
	@NamedQuery(name = "TdsTemplate.checkIdentifier", query = "SELECT t FROM TdsTemplate t WHERE t.identifier = :pIdentifier"),
	@NamedQuery(name = "TdsTemplate.remove", query = "DELETE FROM TdsTemplate t WHERE t.id = :pId")
   })
public class TdsTemplate extends Model implements Serializable {

	private static final long serialVersionUID = -6987824603864709615L;

	@Expose
	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "identifier")
	private String identifier;
	
	@ManyToOne()
	@JoinColumn(name="meter_model_code", nullable=false)
	private MeterModel meterModel;
	
	@Version
	@Column
	private Long version;

	@NotAudited
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size=50)
	@JoinTable(name = "tb_tds_template_tds_spec_option", schema="configSW" ,  
			joinColumns = @JoinColumn(name = "tds_template_code"),
			inverseJoinColumns = @JoinColumn(name = "tds_spec_option_code"))
	@OrderBy("tdsSpec ASC, name ASC")
	private List<TdsSpecOption> tdsSpecOptions;
	
	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;

	@Transient
	private Long transaction;

	@Transient
	private User userReview;

	@Transient
	private Date dateAudit;

	@Transient
	private RevisionType typeReview;
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TdsTemplate other = (TdsTemplate) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public MeterModel getMeterModel() {
		return meterModel;
	}

	public void setMeterModel(MeterModel meterModel) {
		this.meterModel = meterModel;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public List<TdsSpecOption> getTdsSpecOptions() {
		return tdsSpecOptions;
	}

	public void setTdsSpecOptions(List<TdsSpecOption> tdsSpecOptions) {
		this.tdsSpecOptions = tdsSpecOptions;
	}

	public Date getDateAudit() {
		return dateAudit;
	}

	public void setDateAudit(Date dateAudit) {
		this.dateAudit = dateAudit;
	}

	public RevisionType getTypeReview() {
		return typeReview;
	}

	public void setTypeReview(RevisionType typeReview) {
		this.typeReview = typeReview;
	}

	public String getTypeReviewInt() {
		return ResourceBundle.getMessage(typeReview.name());
	}

	public Long getTransaction() {
		return transaction;
	}

	public void setTransaction(Long transaction) {
		this.transaction = transaction;
	}

	public User getUserReview() {
		return userReview;
	}

	public void setUserReview(User userReview) {
		this.userReview = userReview;
	}


}
