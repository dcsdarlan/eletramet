package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.List;

public class FilterModel implements Serializable, Comparable<FilterModel>  {
	  
	private static final long serialVersionUID = -2727832094197115100L;

	private String header;
	private String intHeader;
    private Boolean headerCheck;
    private String value;
	private List<String> options;
	private List<String> intOptions;
	
    public FilterModel(String header, String intHeader, List<String> options, List<String> intOptions) {
        this.header = header;
        this.intHeader = intHeader;
        this.options = options;
        this.intOptions = options;
    }

    public String getHeader() {
        return header;
    }

    public String getIntHeader() {
        return intHeader;
    }
    
	public String getValue() {
		return value;
	}

	public List<String> getOptions() {
		return options;
	}
	
	public List<String> getintOptions() {
		return intOptions;
	}

    public void setValue(String value) {
		this.value = value;
	}

	public Boolean getHeaderCheck() {
		return headerCheck;
	}

	public void setHeaderCheck(Boolean headerCheck) {
		this.headerCheck = headerCheck;
	}
	
	public int compareTo(FilterModel o) {
		if (this.options.size()<o.options.size()){
            return -1;
        }else{
            return 1;
        }
	}
	
}	
