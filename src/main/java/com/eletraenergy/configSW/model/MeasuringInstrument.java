package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RevisionType;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.util.ResourceBundle;
import com.google.gson.annotations.Expose;

@Audited
@AuditTable(value = "tb_measuring_instrument_aud")
@Entity
@Table(name = "tb_measuring_instrument")
@NamedQueries({
	@NamedQuery(name = "MeasuringInstrument.byModel", query = "SELECT m FROM MeasuringInstrument m WHERE m.measuringInstrumentModel = :pMeasuringInstrumentModel ORDER BY m.measuringInstrumentModel ASC"),
	@NamedQuery(name = "MeasuringInstrument.findAll", query = "SELECT m FROM MeasuringInstrument m ORDER BY m.measuringInstrumentModel ASC"),
	@NamedQuery(name = "MeasuringInstrument.lastId", query = "SELECT max(m.id) FROM MeasuringInstrument m"),
	@NamedQuery(name = "MeasuringInstrument.checkSpecificationIdentifier", query = "SELECT m FROM MeasuringInstrument m WHERE m.specificationIdentifier = :pSpecificationIdentifier"),
	@NamedQuery(name = "MeasuringInstrument.remove", query = "DELETE FROM MeasuringInstrument m WHERE m.id = :pId"),
	@NamedQuery(name = "MeasuringInstrument.findOverdueCalibrations", query = "SELECT m FROM MeasuringInstrument m WHERE m.nextCalibrationDate = :pNextCalibrationDate ORDER BY m.nextCalibrationDate DESC"),
	@NamedQuery(name = "MeasuringInstrument.findDelayInTheNext90Days", query = "SELECT m FROM MeasuringInstrument m WHERE m.nextCalibrationDate BETWEEN :pNextCalibrationDateStart AND :pNextCalibrationDateFinish ORDER BY m.nextCalibrationDate DESC"),
	@NamedQuery(name = "MeasuringInstrument.findInCalibration", query = "SELECT m FROM MeasuringInstrument m WHERE m.measuringInstrumentStateIdentifier = :pMeasuringInstrumentStateIdentifier ORDER BY m.nextCalibrationDate DESC")
   })
public class MeasuringInstrument extends Model implements Serializable {

	private static final long serialVersionUID = -3176063343118662782L;

	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "specification_identifier")
	private String specificationIdentifier;

	@ManyToOne
	@JoinColumn(name="measuring_instrument_type_code", nullable=false)
	private MeasuringInstrumentType measuringInstrumentType;
	
	@ManyToOne
	@JoinColumn(name="measuring_instrument_model_code", nullable=false)
	private MeasuringInstrumentModel measuringInstrumentModel;

	@ManyToOne
	@JoinColumn(name="measuring_instrument_place_use_code", nullable=false)
	private MeasuringInstrumentPlaceUse measuringInstrumentPlaceUse;

	@ManyToOne
	@JoinColumn(name="calibration_laboratory_code")
	private CalibrationLaboratory calibrationLaboratory;

	@ManyToOne
	@JoinColumn(name="measuring_instrument_department_code")
	private MeasuringInstrumentDepartment measuringInstrumentDepartment;

	@Column(name = "calibration_frequency_weeks")
	private Integer calibrationFrequencyWeeks;
	
	@Column(name = "last_calibration_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastCalibrationDate;

	@Column(name = "next_calibration_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date nextCalibrationDate;
	
	@Expose
	@Column(name = "obsolete_identifier")
	@Enumerated(EnumType.STRING)
	private MeasuringInstrumentObsoleteIdentifier measuringInstrumentObsoleteIdentifier;
	
	@Expose
	@Column(name = "damaged_identifier")
	@Enumerated(EnumType.STRING)
	private MeasuringInstrumentDamagedIdentifier measuringInstrumentDamagedIdentifier;
	  
	@Expose
	@Column(name = "state_identifier")
	@Enumerated(EnumType.STRING)
	private MeasuringInstrumentStateIdentifier measuringInstrumentStateIdentifier;

	@Column(name = "state_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date measuringInstrumentStateTimestamp;


	@Column(name = "description")
	private String description;
	
	@NotAudited
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size=50)
	@JoinTable(name = "tb_measuring_instrument_measuring_instrument_standard",  
			joinColumns = @JoinColumn(name = "measuring_instrument_code"),
			inverseJoinColumns = @JoinColumn(name = "measuring_instrument_standard_code"))
	private List<MeasuringInstrumentStandard> measuringInstrumentStandards;
	
	@Version
	@Column
	private Long version;
	
	@Transient
	private MeasuringInstrumentManufacturer measuringInstrumentManufacturer;
	
	@Transient
	private Date nextCalibrationDateFilterFrom;
	
	@Transient
    private Date nextCalibrationDateFilterTo;
	
	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;
	
	@Transient
	private Long transaction;

	@Transient
	private Date dateAudit;

	@Transient
	private RevisionType typeReview;
	
	@Transient
	private User userReview;
	
	public MeasuringInstrument() {
		super();
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpecificationIdentifier() {
		return specificationIdentifier;
	}

	public void setSpecificationIdentifier(String specificationIdentifier) {
		this.specificationIdentifier = specificationIdentifier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MeasuringInstrumentModel getMeasuringInstrumentModel() {
		return measuringInstrumentModel;
	}

	public void setMeasuringInstrumentModel(MeasuringInstrumentModel measuringInstrumentModel) {
		this.measuringInstrumentModel = measuringInstrumentModel;
	}
	
	public MeasuringInstrumentType getMeasuringInstrumentType() {
		return measuringInstrumentType;
	}

	public void setMeasuringInstrumentType(MeasuringInstrumentType measuringInstrumentType) {
		this.measuringInstrumentType = measuringInstrumentType;
	}

	public MeasuringInstrumentPlaceUse getMeasuringInstrumentPlaceUse() {
		return measuringInstrumentPlaceUse;
	}

	public void setMeasuringInstrumentPlaceUse(MeasuringInstrumentPlaceUse measuringInstrumentPlaceUse) {
		this.measuringInstrumentPlaceUse = measuringInstrumentPlaceUse;
	}

	public List<MeasuringInstrumentStandard> getMeasuringInstrumentStandards() {
		return measuringInstrumentStandards;
	}

	public void setMeasuringInstrumentStandards(List<MeasuringInstrumentStandard> measuringInstrumentStandards) {
		this.measuringInstrumentStandards = measuringInstrumentStandards;
	}
	
	public Date getLastCalibrationDate() {
		return lastCalibrationDate;
	}

	public void setLastCalibrationDate(Date lastCalibrationDate) {
		this.lastCalibrationDate = lastCalibrationDate;
	}

	public Date getNextCalibrationDate() {
		return nextCalibrationDate;
	}
	
	public void setNextCalibrationDate(Date nextCalibrationDate) {
		this.nextCalibrationDate = nextCalibrationDate;
	}
	
	public Integer getCalibrationFrequencyWeeks() {
		return calibrationFrequencyWeeks;
	}

	public void setCalibrationFrequencyWeeks(Integer calibrationFrequencyWeeks) {
		this.calibrationFrequencyWeeks = calibrationFrequencyWeeks;
	}

	public MeasuringInstrumentStateIdentifier getMeasuringInstrumentStateIdentifier() {
		return measuringInstrumentStateIdentifier;
	}

	public void setMeasuringInstrumentStateIdentifier(MeasuringInstrumentStateIdentifier measuringInstrumentStateIdentifier) {
		this.measuringInstrumentStateIdentifier = measuringInstrumentStateIdentifier;
	}
	
	public MeasuringInstrumentObsoleteIdentifier getMeasuringInstrumentObsoleteIdentifier() {
		return measuringInstrumentObsoleteIdentifier;
	}

	public void setMeasuringInstrumentObsoleteIdentifier(MeasuringInstrumentObsoleteIdentifier measuringInstrumentObsoleteIdentifier) {
		this.measuringInstrumentObsoleteIdentifier = measuringInstrumentObsoleteIdentifier;
	}
	
	public MeasuringInstrumentDamagedIdentifier getMeasuringInstrumentDamagedIdentifier() {
		return measuringInstrumentDamagedIdentifier;
	}

	public void setMeasuringInstrumentDamagedIdentifier(MeasuringInstrumentDamagedIdentifier measuringInstrumentDamagedIdentifier) {
		this.measuringInstrumentDamagedIdentifier = measuringInstrumentDamagedIdentifier;
	}
	
	public Date getMeasuringInstrumentStateTimestamp() {
		return measuringInstrumentStateTimestamp;
	}

	public void setMeasuringInstrumentStateTimestamp(Date measuringInstrumentStateTimestamp) {
		this.measuringInstrumentStateTimestamp = measuringInstrumentStateTimestamp;
	}
	
	public void setMeasuringInstrumentManufacturer(MeasuringInstrumentManufacturer measuringInstrumentManufacturer) {
		this.measuringInstrumentManufacturer = measuringInstrumentManufacturer;
	}
	
	public MeasuringInstrumentManufacturer getMeasuringInstrumentManufacturer() {
		return measuringInstrumentManufacturer;
	}
	
	public void setNextCalibrationDateFilterFrom(Date date) {
		this.nextCalibrationDateFilterFrom = date;
	}
	
	public Date getNextCalibrationDateFilterFrom() {
		return nextCalibrationDateFilterFrom;
	}
	
	public void setNextCalibrationDateFilterTo(Date date) {
		this.nextCalibrationDateFilterTo = date;
	}
	
	public Date getNextCalibrationDateFilterTo() {
		return nextCalibrationDateFilterTo;
	}

	@Override
	public String toString() {
		return "measuring Instrument [id=" + id + ", specification Identifier=" + specificationIdentifier + "," +
				", description=" + description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MeasuringInstrument other = (MeasuringInstrument) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	
	public Date getDateAudit() {
		return dateAudit;
	}

	public void setDateAudit(Date dateAudit) {
		this.dateAudit = dateAudit;
	}

	public RevisionType getTypeReview() {
		return typeReview;
	}

	public void setTypeReview(RevisionType typeReview) {
		this.typeReview = typeReview;
	}
	
	public String getTypeReviewInt() {
		return ResourceBundle.getMessage(typeReview.name());
	}

	public Long getTransaction() {
		return transaction;
	}

	public void setTransaction(Long transaction) {
		this.transaction = transaction;
	}

	public User getUserReview() {
		return userReview;
	}

	public void setUserReview(User userReview) {
		this.userReview = userReview;
	}

	public MeasuringInstrumentDepartment getMeasuringInstrumentDepartment() {
		return measuringInstrumentDepartment;
	}

	public void setMeasuringInstrumentDepartment(MeasuringInstrumentDepartment measuringInstrumentDepartment) {
		this.measuringInstrumentDepartment = measuringInstrumentDepartment;
	}

	public CalibrationLaboratory getCalibrationLaboratory() {
		return calibrationLaboratory;
	}

	public void setCalibrationLaboratory(CalibrationLaboratory calibrationLaboratory) {
		this.calibrationLaboratory = calibrationLaboratory;
	}
}
