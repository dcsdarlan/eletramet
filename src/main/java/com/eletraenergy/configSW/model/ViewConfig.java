package com.eletraenergy.configSW.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "vw_config")
public class ViewConfig extends Model implements Serializable {

	private static final long serialVersionUID = -7980363609975535882L;

	
	public ViewConfig() {
		super();
	}

	public ViewConfig(String tdsNumber, String meterLineName, String meterModelName,
			 String specName, String specOptionName, String configCode) {
		super();
		this.meterLineName = meterLineName;
		this.meterModelName = meterModelName;
		this.tdsNumber = tdsNumber;
		this.specName = specName;
		this.specOptionName = specOptionName;
		this.configCode = configCode;
	}

	@Id
	@Column(name = "meter_line_code")
	private Long id;

	@Column(name = "config_code")
	private String configCode;

	@Column(name = "meter_line_identifier")
	private String meterLineIdentifier;

	@Column(name = "meter_line_name")
	private String meterLineName;
	
	@Column(name = "meter_model_code,")
	private Integer meterModelCode;

	@Column(name = "meter_model_identifier")
	private String meterModelIdentifier;

	@Column(name = "meter_model_name")
	private String meterModelName;
	
	@Column(name = "tds_number")
	private String tdsNumber;
	
	@Column(name = "spec_category_code")
	private Integer specCategoryCode;

	@Column(name = "spec_category_identifier")
	private String specCategoryIdentifier;
	
	@Column(name = "spec_category_name")
	private String specCategoryName;

	@Column(name = "spec_code")
	private Long specCode;

	@Column(name = "spec_identifier")
	private String specIdentifier;

	@Column(name = "spec_name")
	private String specName;

	@Column(name = "spec_option_code")
	private Integer specOptionCode;

	@Column(name = "spec_option_identifier")
	private String specOptionIdentifier;

	@Column(name = "spec_option_name")
	private String specOptionName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMeterLineIdentifier() {
		return meterLineIdentifier;
	}

	public void setMeterLineIdentifier(String meterLineIdentifier) {
		this.meterLineIdentifier = meterLineIdentifier;
	}

	public String getMeterLineName() {
		return meterLineName;
	}

	public void setMeterLineName(String meterLineName) {
		this.meterLineName = meterLineName;
	}

	public Integer getMeterModelCode() {
		return meterModelCode;
	}

	public void setMeterModelCode(Integer meterModelCode) {
		this.meterModelCode = meterModelCode;
	}

	public String getMeterModelIdentifier() {
		return meterModelIdentifier;
	}

	public void setMeterModelIdentifier(String meterModelIdentifier) {
		this.meterModelIdentifier = meterModelIdentifier;
	}

	public String getMeterModelName() {
		return meterModelName;
	}

	public void setMeterModelName(String meterModelName) {
		this.meterModelName = meterModelName;
	}

	public String getTdsNumber() {
		return tdsNumber;
	}

	public void setTdsNumber(String tdsNumber) {
		this.tdsNumber = tdsNumber;
	}

	public Integer getSpecCategoryCode() {
		return specCategoryCode;
	}

	public void setSpecCategoryCode(Integer specCategoryCode) {
		this.specCategoryCode = specCategoryCode;
	}

	public String getSpecCategoryIdentifier() {
		return specCategoryIdentifier;
	}

	public void setSpecCategoryIdentifier(String specCategoryIdentifier) {
		this.specCategoryIdentifier = specCategoryIdentifier;
	}

	public String getSpecCategoryName() {
		return specCategoryName;
	}

	public void setSpecCategoryName(String specCategoryName) {
		this.specCategoryName = specCategoryName;
	}

	public Long getSpecCode() {
		return specCode;
	}

	public void setSpecCode(Long specCode) {
		this.specCode = specCode;
	}

	public String getSpecIdentifier() {
		return specIdentifier;
	}

	public void setSpecIdentifier(String specIdentifier) {
		this.specIdentifier = specIdentifier;
	}

	public String getSpecName() {
		return specName;
	}

	public void setSpecName(String specName) {
		this.specName = specName;
	}

	public Integer getSpecOptionCode() {
		return specOptionCode;
	}

	public void setSpecOptionCode(Integer specOptionCode) {
		this.specOptionCode = specOptionCode;
	}

	public String getSpecOptionIdentifier() {
		return specOptionIdentifier;
	}

	public void setSpecOptionIdentifier(String specOptionIdentifier) {
		this.specOptionIdentifier = specOptionIdentifier;
	}

	public String getSpecOptionName() {
		return specOptionName;
	}

	public void setSpecOptionName(String specOptionName) {
		this.specOptionName = specOptionName;
	}

	public String getConfigCode() {
		return configCode;
	}

	public void setConfigCode(String configCode) {
		this.configCode = configCode;
	}
}
