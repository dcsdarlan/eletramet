package com.eletraenergy.configSW.model;

import java.io.IOException;
import java.sql.SQLException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.postgresql.util.PGobject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Converter
public class ConfigCommandConverter implements AttributeConverter<ConfigCommand, PGobject> {

    public PGobject convertToDatabaseColumn(ConfigCommand configCommand) {
        try {
            PGobject po = new PGobject();
            po.setType("json");
            po.setValue((new ObjectMapper()).writeValueAsString(configCommand));
            return po;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ConfigCommand convertToEntityAttribute(PGobject po) {
        try {
            return (new ObjectMapper()).readValue(po.getValue(),ConfigCommand.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}