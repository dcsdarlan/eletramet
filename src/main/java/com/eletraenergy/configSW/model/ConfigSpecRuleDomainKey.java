package com.eletraenergy.configSW.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ConfigSpecRuleDomainKey implements Serializable {

	private static final long serialVersionUID = 5090959098660683582L;

	public ConfigSpecRuleDomainKey() {
		super();
	}
	
	public ConfigSpecRuleDomainKey(Long configCode, Long specCode,
			Long specRuleDomainCode) {
		super();
		this.configCode = configCode;
		this.specCode = specCode;
		this.specRuleDomainCode = specRuleDomainCode;
	}

	@Column(name = "config_code", nullable = false)
    private Long configCode;

    @Column(name = "spec_code", nullable = false)
    private Long specCode;

    @Column(name = "spec_rule_domain_code", nullable = false)
    private Long specRuleDomainCode;

	public Long getConfigCode() {
		return configCode;
	}

	public void setConfigCode(Long configCode) {
		this.configCode = configCode;
	}

	public Long getSpecCode() {
		return specCode;
	}

	public void setSpecCode(Long specCode) {
		this.specCode = specCode;
	}

	public Long getSpecRuleDomainCode() {
		return specRuleDomainCode;
	}

	public void setSpecRuleDomainCode(Long specRuleDomainCode) {
		this.specRuleDomainCode = specRuleDomainCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((configCode == null) ? 0 : configCode.hashCode());
		result = prime * result
				+ ((specCode == null) ? 0 : specCode.hashCode());
		result = prime
				* result
				+ ((specRuleDomainCode == null) ? 0 : specRuleDomainCode
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfigSpecRuleDomainKey other = (ConfigSpecRuleDomainKey) obj;
		if (configCode == null) {
			if (other.configCode != null)
				return false;
		} else if (!configCode.equals(other.configCode))
			return false;
		if (specCode == null) {
			if (other.specCode != null)
				return false;
		} else if (!specCode.equals(other.specCode))
			return false;
		if (specRuleDomainCode == null) {
			if (other.specRuleDomainCode != null)
				return false;
		} else if (!specRuleDomainCode.equals(other.specRuleDomainCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ConfigSpecRuleDomainKey [configCode=" + configCode
				+ ", specCode=" + specCode + ", specRuleDomainCode="
				+ specRuleDomainCode + "]";
	}
	
}