package com.eletraenergy.configSW.model;

import com.eletraenergy.configSW.util.ResourceBundle;



public enum StateIdentifier {

	pcim(0, "pcim"),
	allm(1, "allm"),
	notm(2, "notm");
	
	private Integer id;
	
	private String name;

	private StateIdentifier(Integer id, String name){
		this.id = id;
		this.name = name;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return ResourceBundle.getMessage(this.name);
	}
	
	/**
	 * @param the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public String getIntName() {
		return ResourceBundle.getMessage(this.name);
	}

}
