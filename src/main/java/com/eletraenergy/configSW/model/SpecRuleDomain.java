package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.primefaces.model.SortOrder;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "tb_spec_rule_domain")
@NamedQueries({
	@NamedQuery(name = "SpecRuleDomain.findAll", query = "SELECT s FROM SpecRuleDomain s ORDER BY s.name ASC"),
	@NamedQuery(name = "SpecRuleDomain.childs", query = "SELECT s FROM SpecRuleDomain s WHERE s.parent = :pParent ORDER BY s.name ASC"),
	@NamedQuery(name = "SpecRuleDomain.lastId", query = "SELECT max(s.id) FROM SpecRuleDomain s"),
	@NamedQuery(name = "SpecRuleDomain.checkIdentifier", query = "SELECT s FROM SpecRuleDomain s WHERE s.identifier = :pIdentifier"),
	@NamedQuery(name = "SpecRuleDomain.remove", query = "DELETE FROM SpecRuleDomain s WHERE s.id = :pId")
   })
public class SpecRuleDomain extends Model implements Serializable {

	private static final long serialVersionUID = 7465101871148062038L;

	
	public SpecRuleDomain() {
		super();
	}

	public SpecRuleDomain(Long id, String identifier, String name,
			SpecRule specRule, SpecRuleDomain parent,
			List<SpecRuleDomain> childs, Boolean check, String text) {
		super();
		this.id = id;
		this.identifier = identifier;
		this.name = name;
		this.specRule = specRule;
		this.parent = parent;
		this.childs = childs;
		this.check = check;
		this.text = text;
	}

	public SpecRuleDomain(SpecRuleDomain specRuleDomainR) {
		super();
		this.id = specRuleDomainR.getId();
		this.identifier = specRuleDomainR.getIdentifier();
		this.name = specRuleDomainR.getName();
		this.specRule = specRuleDomainR.getSpecRule();
		this.parent = specRuleDomainR.getParent();
		this.childs = specRuleDomainR.getChilds();
		this.check = specRuleDomainR.getCheck();
		this.text = specRuleDomainR.getText();
		this.isInput = specRuleDomainR.getIsInput();
		this.isNode = true;

	}

	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "identifier")
	private String identifier;
	
	@Column(name = "name")
	private String name;

	@ManyToOne()
	@JoinColumn(name="spec_rule_code", nullable=false)
	private SpecRule specRule;

	@ManyToOne()
	@JoinColumn(name="parent_code")
	private SpecRuleDomain parent;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="parent", fetch=FetchType.LAZY)
	@BatchSize(size=10)
	private List<SpecRuleDomain> childs;

	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;

	@Expose
	@Transient
	private Boolean check;
	
	@Transient
	private Boolean isInput;

	@Transient
	private Boolean isNode;

	@Expose
	@Transient
	private String text;

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public SpecRuleDomain getParent() {
		return parent;
	}

	public void setParent(SpecRuleDomain parent) {
		this.parent = parent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpecRuleDomain other = (SpecRuleDomain) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public SpecRule getSpecRule() {
		return specRule;
	}

	public void setSpecRule(SpecRule specRule) {
		this.specRule = specRule;
	}

	public List<SpecRuleDomain> getChilds() {
		return childs;
	}

	public void setChilds(List<SpecRuleDomain> childs) {
		this.childs = childs;
	}

	public Boolean getCheck() {
		return check;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "SpecRuleDomain [id=" + id + ", identifier=" + identifier
				+ ", name=" + name + ", specRule=" + specRule + ", parent="
				+ parent + ", childs=" + childs + ", check=" + check
				+ ", text=" + text + "]";
	}

	public Boolean getIsInput() {
		return isInput;
	}

	public void setIsInput(Boolean isInput) {
		this.isInput = isInput;
	}

	public Boolean getIsNode() {
		return isNode;
	}

	public void setIsNode(Boolean isNode) {
		this.isNode = isNode;
	}

}
