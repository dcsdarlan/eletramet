package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RevisionType;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.util.ResourceBundle;

@Audited
@AuditTable(value = "tb_measuring_instrument_model_aud")
@Entity
@Table(name = "tb_measuring_instrument_model")
@NamedQueries({
	@NamedQuery(name = "MeasuringInstrumentModel.byManufacturer", query = "SELECT m FROM MeasuringInstrumentModel m WHERE m.measuringInstrumentManufacturer = :pMeasuringInstrumentManufacturer ORDER BY m.name ASC"),
	@NamedQuery(name = "MeasuringInstrumentModel.findAll", query = "SELECT m FROM MeasuringInstrumentModel m ORDER BY m.name ASC"),
	@NamedQuery(name = "MeasuringInstrumentModel.lastId", query = "SELECT max(m.id) FROM MeasuringInstrumentModel m"),
	@NamedQuery(name = "MeasuringInstrumentModel.checkName", query = "SELECT m FROM MeasuringInstrumentModel m WHERE m.name = :pName"),
	@NamedQuery(name = "MeasuringInstrumentModel.remove", query = "DELETE FROM MeasuringInstrumentModel m WHERE m.id = :pId")
   })
public class MeasuringInstrumentModel extends Model implements Serializable, Comparable<MeasuringInstrumentType> {

	private static final long serialVersionUID = -3176063343118662782L;

	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@ManyToOne
	@JoinColumn(name="measuring_instrument_manufacturer_code", nullable=false)
	private MeasuringInstrumentManufacturer measuringInstrumentManufacturer;
	
	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;

	@Transient
	private Long transaction;

	@Transient
	private Date dateAudit;

	@Transient
	private RevisionType typeReview;
	
	@Transient
	private User userReview;
	
	public MeasuringInstrumentModel() {
		super();
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MeasuringInstrumentManufacturer getMeasuringInstrumentManufacturer() {
		return measuringInstrumentManufacturer;
	}

	public void setMeasuringInstrumentManufacturer(MeasuringInstrumentManufacturer measuringInstrumentManufacturer) {
		this.measuringInstrumentManufacturer = measuringInstrumentManufacturer;
	}

	@Override
	public String toString() {
		return "MeterModel [id=" + id + ", name=" + name + "," +
				", description=" + description + ", measuringInstrumentManufacturer="
				+ measuringInstrumentManufacturer + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MeasuringInstrumentModel other = (MeasuringInstrumentModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	@Override
	public int compareTo(MeasuringInstrumentType o) {
		return (this.hashCode() - o.hashCode());
	}
	
	public Date getDateAudit() {
		return dateAudit;
	}

	public void setDateAudit(Date dateAudit) {
		this.dateAudit = dateAudit;
	}

	public RevisionType getTypeReview() {
		return typeReview;
	}

	public void setTypeReview(RevisionType typeReview) {
		this.typeReview = typeReview;
	}
	
	public String getTypeReviewInt() {
		return ResourceBundle.getMessage(typeReview.name());
	}

	public Long getTransaction() {
		return transaction;
	}

	public void setTransaction(Long transaction) {
		this.transaction = transaction;
	}

	public User getUserReview() {
		return userReview;
	}

	public void setUserReview(User userReview) {
		this.userReview = userReview;
	}
	
}
