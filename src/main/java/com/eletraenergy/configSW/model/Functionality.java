package com.eletraenergy.configSW.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.security.core.GrantedAuthority;

import com.eletraenergy.configSW.util.ResourceBundle;


/**
 * Classe que representa uma funcionalidade do sistema. 
 * 
 * @author Raphael Ferreira 
 *
 */
@Entity
@Table(name = "tb_functionality")
@BatchSize(size=20)
@NamedQueries({
	@NamedQuery(name = "Functionality.all", 
			   query = "SELECT f FROM Functionality f ORDER BY f.orderFunction ASC") 			  
   })
public class Functionality extends Model implements GrantedAuthority {
	
	private static final long serialVersionUID = 7432832529733779589L;

	@Id
	@Column(name = "code")
	private Long id;
	
	@Column
	private String description;

	@Column(name = "order_function")
	private Integer orderFunction;

	@Column(unique = true)
	private String mnemonic;

	public String getDescription() {
		return ResourceBundle.getMessage(getMnemonic());
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Retorna o c�digo da permiss�o, que � utilizado no sistema.
	 */
	public String getAuthority() {
		return mnemonic;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getOrderFunction() {
		return orderFunction;
	}

	public void setOrderFunction(Integer orderFunction) {
		this.orderFunction = orderFunction;
	}
}
