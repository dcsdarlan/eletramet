package com.eletraenergy.configSW.model;

import java.util.ArrayList;

public class ItemSpecDepend {
	private String ItemSpecID;
	private String ItemSpecName;
	private ArrayList<ItemOptionSpecDepend> ListOfItemOptionSpecDepend = new ArrayList<ItemOptionSpecDepend>();
	
	public String getID_ItemSpec() {
		return ItemSpecID;
	}
	public void setID_ItemSpec(String iD_ItemSpec) {
		ItemSpecID = iD_ItemSpec;
	}
	public String getNM_ItemSpec() {
		return ItemSpecName;
	}
	public void setNM_ItemSpec(String nM_ItemSpec) {
		ItemSpecName = nM_ItemSpec;
	}
	public ArrayList<ItemOptionSpecDepend> getListOfItemOptionSpecDepend() {
		return ListOfItemOptionSpecDepend;
	}
	public void setListOfItemOptionSpecDepend(ArrayList<ItemOptionSpecDepend> listOfItemOptionSpecDepend) {
		ListOfItemOptionSpecDepend = listOfItemOptionSpecDepend;
	}
}
