package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RevisionType;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.util.ResourceBundle;
import com.google.gson.annotations.Expose;

@Entity
@Audited
@AuditTable(value = "tb_tds_aud")
@Table(name = "tb_tds")
@NamedQueries({
	@NamedQuery(name = "Tds.findAll", query = "SELECT t FROM Tds t ORDER BY t.identifier ASC"),
	@NamedQuery(name = "Tds.findByModel", query = "SELECT t FROM Tds t WHERE t.meterModel = :pMeterModel AND t.state <> :pState ORDER BY t.identifier ASC"),
	@NamedQuery(name = "Tds.findByLine", query = "SELECT t FROM Tds t JOIN t.meterModel m WHERE m.meterLine = :pMeterLine ORDER BY t.identifier ASC"),
	@NamedQuery(name = "Tds.findByModelOrderIdentifier", query = "SELECT t FROM Tds t WHERE t.meterModel = :pMeterModel ORDER BY t.identifier DESC"),
	@NamedQuery(name = "Tds.lastId", query = "SELECT max(t.id) FROM Tds t"),
	@NamedQuery(name = "Tds.checkIdentifier", query = "SELECT t FROM Tds t WHERE t.identifier = :pIdentifier"),
	@NamedQuery(name = "Tds.checkTemplateOption", query = "SELECT t FROM Tds t WHERE t.template = :pTemplate AND :pOption member of t.tdsSpecOptions"),
	@NamedQuery(name = "Tds.remove", query = "DELETE FROM Tds t WHERE t.id = :pId")
   })
public class Tds extends Model implements Serializable {

	private static final long serialVersionUID = -6987824603864709615L;

	@Expose
	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "identifier")
	private String identifier;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="meter_model_code", nullable=false)
	private MeterModel meterModel;

	@ManyToOne()
	@JoinColumn(name="tds_template_code", nullable=false)
	private TdsTemplate template;	
	
	@Expose
	@Column(name = "state_identifier")
	@Enumerated(EnumType.STRING)
	private TdsStateIdentifier state;

	@Column(name = "state_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date stateModified;

	@Column(name = "change_version")
	private Integer changeVersion;

	@Column(name = "change_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date changeModified;

	@Expose
	@Column(name = "change_state_identifier")
	private String changeIdentifier;
	
	@Version
	@Column
	private Long version;

	@NotAudited
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size=50)
	@JoinTable(name = "tb_tds_tds_spec_option", schema="configSW" ,  
			joinColumns = @JoinColumn(name = "tds_code"),
			inverseJoinColumns = @JoinColumn(name = "tds_spec_option_code"))
	private List<TdsSpecOption> tdsSpecOptions;

	@Transient
	private String stateName;

	@Transient
	private String changeIdentifierName;
	
	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;

	@Transient
	private Boolean possoAtivar;

	@Transient
	private Boolean possoCriar;

	@Transient
	private Boolean possoDesativar;

	@Transient
	private Boolean possoAguardarBOM;
	
	@Transient
	private Boolean possoCancelar;
	
	@Transient
	private Long transaction;

	@Transient
	private Date dateAudit;

	@Transient
	private RevisionType typeReview;
	
	@Transient
	private User userReview;
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public TdsStateIdentifier getState() {
		return state;
	}

	public void setState(TdsStateIdentifier state) {
		this.state = state;
	}

	public Date getStateModified() {
		return stateModified;
	}

	public void setStateModified(Date stateModified) {
		this.stateModified = stateModified;
	}

	public Integer getChangeVersion() {
		return changeVersion;
	}

	public void setChangeVersion(Integer changeVersion) {
		this.changeVersion = changeVersion;
	}

	public Date getChangeModified() {
		return changeModified;
	}

	public void setChangeModified(Date changeModified) {
		this.changeModified = changeModified;
	}

	public String getChangeIdentifier() {
		return this.changeIdentifier;
	}

	public void setChangeIdentifier(String changeIdentifier) {
		this.changeIdentifier = changeIdentifier;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tds other = (Tds) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public MeterModel getMeterModel() {
		return meterModel;
	}

	public void setMeterModel(MeterModel meterModel) {
		this.meterModel = meterModel;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public List<TdsSpecOption> getTdsSpecOptions() {
		return tdsSpecOptions;
	}

	public void setTdsSpecOptions(List<TdsSpecOption> tdsSpecOptions) {
		this.tdsSpecOptions = tdsSpecOptions;
	}

	public TdsTemplate getTemplate() {
		return template;
	}

	public void setTemplate(TdsTemplate tdsTemplate) {
		this.template = tdsTemplate;
	}
	
	public String getStateName() {
		return state==null?null:ResourceBundle.getMessage(state.getId());
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	
	public void setChangeIdentifierName(String changeIdentifierName) {
		this.changeIdentifierName = changeIdentifierName;
	}

	public String getChangeIdentifierName() {
		return ResourceBundle.getMessage(changeIdentifier);
	}

	public Boolean getPossoAtivar() {
		possoAtivar = false;
		TdsStateIdentifier[] verEstadosPossiveis = state.getState().verEstadosPossiveis();
		for (TdsStateIdentifier tdsStateIdentifier : verEstadosPossiveis) {
			if (tdsStateIdentifier.equals(TdsStateIdentifier.a)){
				possoAtivar = true;
				break;
			}
		}
		
		return possoAtivar;
	}

	public Boolean getPossoAguardarBOM() {
		possoAguardarBOM = false;
		TdsStateIdentifier[] verEstadosPossiveis = state.getState().verEstadosPossiveis();
		for (TdsStateIdentifier tdsStateIdentifier : verEstadosPossiveis) {
			if (tdsStateIdentifier.equals(TdsStateIdentifier.w)){
				possoAguardarBOM = true;
				break;
			}
		}
		
		return possoAguardarBOM;
	}

	public Boolean getPossoCancelar() {
		possoCancelar = false;
		TdsStateIdentifier[] verEstadosPossiveis = state.getState().verEstadosPossiveis();
		for (TdsStateIdentifier tdsStateIdentifier : verEstadosPossiveis) {	
			if (tdsStateIdentifier.equals(TdsStateIdentifier.e)){
				possoCancelar = true;
				break;
			}
		}
		
		return possoCancelar;
	}

	public Boolean getPossoDesativar() {
		possoDesativar = false;
		TdsStateIdentifier[] verEstadosPossiveis = state.getState().verEstadosPossiveis();
		for (TdsStateIdentifier tdsStateIdentifier : verEstadosPossiveis) {
			if (tdsStateIdentifier.equals(TdsStateIdentifier.u)){
				possoDesativar = true;
				break;
			}
		}
		
		return possoDesativar;
	}

	public Date getDateAudit() {
		return dateAudit;
	}

	public void setDateAudit(Date dateAudit) {
		this.dateAudit = dateAudit;
	}

	public RevisionType getTypeReview() {
		return typeReview;
	}

	public void setTypeReview(RevisionType typeReview) {
		this.typeReview = typeReview;
	}

	public String getTypeReviewInt() {
		return ResourceBundle.getMessage(typeReview.name());
	}

	public Long getTransaction() {
		return transaction;
	}

	public void setTransaction(Long transaction) {
		this.transaction = transaction;
	}

	public User getUserReview() {
		return userReview;
	}

	public void setUserReview(User userReview) {
		this.userReview = userReview;
	}

	public Boolean getPossoCriar() {
		possoCriar = false;
		TdsStateIdentifier[] verEstadosPossiveis = state.getState().verEstadosPossiveis();
		for (TdsStateIdentifier tdsStateIdentifier : verEstadosPossiveis) {
			if (tdsStateIdentifier.equals(TdsStateIdentifier.c)){
				possoCriar = true;
				break;
			}
		}
		
		return possoCriar;
	}

}
