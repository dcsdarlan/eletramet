package com.eletraenergy.configSW.model;

import java.io.IOException;
import java.sql.SQLException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.postgresql.util.PGobject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Converter
public class ConfigTechSpecConverter implements AttributeConverter<ConfigTechSpec, PGobject> {

    public PGobject convertToDatabaseColumn(ConfigTechSpec configTechSpec) {
        try {
            PGobject po = new PGobject();
            po.setType("json");
            po.setValue((new ObjectMapper()).writeValueAsString(configTechSpec));
            return po;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ConfigTechSpec convertToEntityAttribute(PGobject po) {
        try {
            return (new ObjectMapper()).readValue(po.getValue(),ConfigTechSpec.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}