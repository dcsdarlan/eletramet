package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.List;

public class TdsModel implements Serializable {
  	 
	private static final long serialVersionUID = 834405375829171124L;
	private String tds;
	private TdsStateIdentifier state;
	private String configCode;
	private String line;
	private String model;
	private List<String> values;

    public TdsModel() {
    }
    
    public TdsModel(TdsStateIdentifier state, String tds, String line, String model, String configCode, List<String> values) {
    	this.configCode = configCode;
    	this.line = line;
    	this.model = model;
        this.tds = tds;
        this.values = values;
        this.state = state;
    }

	public String getTds() {
		return tds;
	}

	public List<String> getValues() {
		return values;
	}

	public void setTds(String tds) {
		this.tds = tds;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public String getConfigCode() {
		return configCode;
	}

	public void setConfigCode(String configCode) {
		this.configCode = configCode;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public TdsStateIdentifier getState() {
		return state;
	}

	public void setState(TdsStateIdentifier state) {
		this.state = state;
	}
}	