package com.eletraenergy.configSW.model;

import com.eletraenergy.configSW.util.ResourceBundle;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RevisionType;
import org.primefaces.model.SortOrder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

@Entity
@Audited
@AuditTable(value = "tb_measuring_instrument_department_aud")
@Table(name = "tb_measuring_instrument_department")
@NamedQueries({
	@NamedQuery(name = "MeasuringInstrumentDepartment.findAll", query = "SELECT m FROM MeasuringInstrumentDepartment m ORDER BY m.name ASC"),
	@NamedQuery(name = "MeasuringInstrumentDepartment.lastId", query = "SELECT max(l.id) FROM MeasuringInstrumentDepartment l"),
	@NamedQuery(name = "MeasuringInstrumentDepartment.checkName", query = "SELECT l FROM MeasuringInstrumentDepartment l WHERE l.name = :pName")
   })
public class MeasuringInstrumentDepartment extends Model implements Serializable {

	private static final long serialVersionUID = 9165168647087327530L;

	@Id
	@Column(name = "code")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user_code")
	@Audited(targetAuditMode = NOT_AUDITED)
	private User user;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;

	@Transient
	private Long transaction;

	@Transient
	private Date dateAudit;

	@Transient
	private RevisionType typeReview;
	
	@Transient
	private User userReview;
	
	public MeasuringInstrumentDepartment() {
		super();
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Measuring Instrument Department [id=" + id + ", name=" + name + ","  +
				", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MeasuringInstrumentDepartment other = (MeasuringInstrumentDepartment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	
	public Date getDateAudit() {
		return dateAudit;
	}

	public void setDateAudit(Date dateAudit) {
		this.dateAudit = dateAudit;
	}

	public RevisionType getTypeReview() {
		return typeReview;
	}

	public void setTypeReview(RevisionType typeReview) {
		this.typeReview = typeReview;
	}

	public String getTypeReviewInt() {
		return ResourceBundle.getMessage(typeReview.name());
	}

	public Long getTransaction() {
		return transaction;
	}

	public void setTransaction(Long transaction) {
		this.transaction = transaction;
	}

	public User getUserReview() {
		return userReview;
	}

	public void setUserReview(User userReview) {
		this.userReview = userReview;
	}

}
