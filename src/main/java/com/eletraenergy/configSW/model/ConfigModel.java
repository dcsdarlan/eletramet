package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.List;

public class ConfigModel implements Serializable {
  	 
	private static final long serialVersionUID = -3642811566165452043L;
	private String category;
	private String configuration;
	private List<String> options;
	private String value;
	private String valueInt;
	private String valueDec;

	private List<String> values;
	private OptionType optionType;

	private String validation;
	private String type;
	private String valMaxDigInt;
	private String valMaxDigDec;
	private String valMaxDig;
	private String maxSel;
	private String valMin;
	private String valMax;
	private Boolean valid;
	private Boolean isRequired;
	
    public ConfigModel() {
    }


	public ConfigModel(String category, String configuration,
			List<String> options, String value, OptionType optionType,
			String validation, String type, String valMaxDigInt,
			String valMaxDigDec, String valMaxDig, String maxSel,
			String valMin, String valMax, boolean isRequired) {
		super();
		this.category = category;
		this.configuration = configuration;
		this.options = options;
		this.value = value;
		this.optionType = optionType;
		this.validation = validation;
		this.type = type;
		this.valMaxDigInt = valMaxDigInt;
		this.valMaxDigDec = valMaxDigDec;
		this.valMaxDig = valMaxDig;
		this.maxSel = maxSel;
		this.valMin = valMin;
		this.valMax = valMax;
		this.valid = true;
		this.isRequired = isRequired;
	}


	public String getCategory() {
		return category;
	}


	public String getConfiguration() {
		return configuration;
	}


	public List<String> getOptions() {
		return options;
	}


	public String getValue() {
		return value;
	}


	public OptionType getOptionType() {
		return optionType;
	}


	public String getValidation() {
		return validation;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}


	public void setOptions(List<String> options) {
		this.options = options;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public void setOptionType(OptionType optionType) {
		this.optionType = optionType;
	}


	public void setValidation(String validation) {
		this.validation = validation;
	}

	public String getType() {
		return type;
	}

	public String getValMaxDigInt() {
		return valMaxDigInt;
	}

	public String getValMaxDigDec() {
		return valMaxDigDec;
	}

	public String getValMaxDig() {
		return valMaxDig;
	}

	public String getMaxSel() {
		return maxSel;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setValMaxDigInt(String valMaxDigInt) {
		this.valMaxDigInt = valMaxDigInt;
	}

	public void setValMaxDigDec(String valMaxDigDec) {
		this.valMaxDigDec = valMaxDigDec;
	}

	public void setValMaxDig(String valMaxDig) {
		this.valMaxDig = valMaxDig;
	}

	public void setMaxSel(String maxSel) {
		this.maxSel = maxSel;
	}

	public String getValMin() {
		return valMin;
	}

	public String getValMax() {
		return valMax;
	}

	public void setValMin(String valMin) {
		this.valMin = valMin;
	}

	public void setValMax(String valMax) {
		this.valMax = valMax;
	}


	public List<String> getValues() {
		return values;
	}


	public Boolean getValid() {
		return valid;
	}


	public void setValid(Boolean valid) {
		this.valid = valid;
	}


	public void setValues(List<String> values) {
		this.values = values;
	}


	public Boolean getIsRequired() {
		return isRequired;
	}


	public void setIsRequired(Boolean isRequired) {
		this.isRequired = isRequired;
	}


	public String getValueInt() {
		return valueInt;
	}


	public String getValueDec() {
		return valueDec;
	}


	public void setValueInt(String valueInt) {
		this.valueInt = valueInt;
	}


	public void setValueDec(String valueDec) {
		this.valueDec = valueDec;
	}
    
	public String getValueMaxInt(){
		String r = "";
		for (int i = 0; i < Integer.parseInt(valMaxDigInt); i++) {
			r = r + "9";
		}
		return r;
	}

	public String getValueMaxDec(){
		String r = "";
		for (int i = 0; i < Integer.parseInt(valMaxDigDec); i++) {
			r = r + "9";
		}
		return r;
	}
}	