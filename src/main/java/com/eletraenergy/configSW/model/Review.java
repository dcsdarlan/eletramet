package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import com.eletraenergy.configSW.service.impl.ReviewListener;
import com.eletraenergy.configSW.util.ConfigSWUtils;
import com.eletraenergy.configSW.util.ResourceBundle;

@Entity
@Table(name = "tb_review")
@RevisionEntity(ReviewListener.class)
@NamedQueries({
	@NamedQuery(name = "Review.findLast10", query = "SELECT r FROM Review r ORDER BY r.dateAudit DESC")
   })
public class Review extends Model implements Serializable {

	@Override
	public String toString() {
		return ResourceBundle.getMessage(titleTransacation);
	}

	private static final long serialVersionUID = -8832558336473945065L;

	@Id
	@Column(name = "code")
	@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_review", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entity_sequence")
	@RevisionNumber
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@RevisionTimestamp
	@Column(name = "audit_date")
	private Date dateAudit;

	@Column(name = "transaction_code")
	private Integer codeTransacation;

	@Column(name = "transaction_title")
	private String titleTransacation;

	@NotAudited
	@ManyToOne
	@JoinColumn(name="user_code", nullable=false)
	private User user;
	
	@Transient
	private Class<?> entity;
	
	@Transient
	private TypeReview typeReview;
	
	@Transient
	private List<Restriction> restriction;
	
	@Transient
	private Date periodoInicial;
	
	@Transient
	private Date periodoFinal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateAudit() {
		return dateAudit;
	}

	public void setDateAudit(Date dateAudit) {
		this.dateAudit = dateAudit;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setEntity(Class<?> entity) {
		this.entity = entity;
	}

	public Class<?> getEntity() {
		return entity;
	}

	public void setTypeReview(TypeReview typeReview) {
		this.typeReview = typeReview;
	}

	public TypeReview getTypeReview() {
		return typeReview;
	}

	public void setRestriction(List<Restriction> restriction) {
		this.restriction = restriction;
	}

	public List<Restriction> getRestriction() {
		if (restriction == null) {
			restriction = new ArrayList<Restriction>();
		}
		return restriction;
	}

	public void setPeriodoInicial(Date periodoInicial) {
		this.periodoInicial = periodoInicial;
	}

	public Date getPeriodoInicial() {
		return periodoInicial;
	}

	public void setPeriodoFinal(Date periodoFinal) {
		this.periodoFinal = periodoFinal;
	}

	public Date getPeriodoFinal() {
		return periodoFinal;
	}
	
	@Override
	public Review clone() {
		Review review = new Review();
		review.setDateAudit(getDateAudit());
		review.setId(getId());
		review.setUser(getUser());
		review.setCodeTransacation(getCodeTransacation());
		review.setTitleTransacation(getTitleTransacation());
		return review;
	}
	
	public Integer getCodeTransacation() {
		return codeTransacation;
	}

	public void setCodeTransacation(Integer codeTransacation) {
		this.codeTransacation = codeTransacation;
	}

	public String getTitleTransacation() {
		return ResourceBundle.getMessage(titleTransacation);
	}

	public void setTitleTransacation(String titleTransacation) {
		this.titleTransacation = titleTransacation;
	}
	

	public String getStyle() {
		String style = null;
		switch (codeTransacation) {
		case 1:
		case 4:
		case 7:
		case 10:
		case 13:
		case 16:
		case 19:
		case 22:
		case 25:
		case 28:
		case 31:
			style = "createStyle"; 
			break;
		case 2:
		case 5:
		case 8:
		case 11:
		case 14:
		case 17:
		case 20:
		case 23:
		case 26:
		case 29:
		case 32:
			style = "alterStyle"; 
			break;
		case 3:
		case 6:
		case 9:
		case 12:
		case 15:
		case 18:
		case 21:
		case 24:
		case 27:
		case 30:
		case 33:
			style = "deleteStyle";
			break;
		}
		return style;
	}	

	public static class Restriction implements Serializable, Comparable<Restriction> {
		
		private static final long serialVersionUID = 1955725361200101617L;
		
		private String attribute;
		private Class<?> type;
		private Object value;
		
		public Restriction(String attribute, Class<?> type, Object value) {
			super();
			this.attribute = attribute;
			this.type = type;
			this.value = value;
		}

		public void setAttribute(String attribute) {
			this.attribute = attribute;
		}

		public String getAttribute() {
			return attribute;
		}

		public void setValue(Object value) {
			this.value = value;
		}

		public Object getValue() {
			return value;
		}

		public void setType(Class<?> type) {
			this.type = type;
		}

		public Class<?> getType() {
			return type;
		}
		
		public Object getValueAsString() {
			if (value instanceof Date) {
				return ConfigSWUtils.formataData(ConfigSWUtils.FORMATO_DATA, (Date) value);
			}
			return value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((attribute == null) ? 0 : attribute.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Restriction other = (Restriction) obj;
			if (attribute == null) {
				if (other.attribute != null)
					return false;
			} else if (!attribute.equals(other.attribute))
				return false;
			if (type == null) {
				if (other.type != null)
					return false;
			} else if (!type.equals(other.type))
				return false;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}

		public int compareTo(Restriction o) {
			return this.getAttribute().compareTo(o.getAttribute());
		}
	}

}
