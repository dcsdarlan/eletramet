package com.eletraenergy.configSW.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.primefaces.model.SortOrder;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "tb_config_spec_option")
@NamedQueries({
	@NamedQuery(name = "ConfigSpecOption.findAll", query = "SELECT c FROM ConfigSpecOption c ORDER BY c.config, c.specOptionCode ASC"),
	@NamedQuery(name = "ConfigSpecOption.findByConfig", query = "SELECT c FROM ConfigSpecOption c WHERE c.config.id = :pConfig ORDER BY c.specOptionCode ASC"),
	@NamedQuery(name = "ConfigSpecOption.lastId", query = "SELECT max(c.id) FROM ConfigSpecOption c"),
	@NamedQuery(name = "ConfigSpecOption.findBySpec", query = "SELECT c FROM ConfigSpecOption c WHERE c.specOptionCode IN (:pSpec) order by c.config"),
	@NamedQuery(name = "ConfigSpecOption.remove", query = "DELETE FROM ConfigSpecOption c WHERE c.config = :pConfig")
   })
public class ConfigSpecOption extends Model implements Serializable {

	private static final long serialVersionUID = 5059346626518002833L;

	public ConfigSpecOption() {
		super();
	}
	
	public ConfigSpecOption(Long id, Config config, Long specOptionCode) {
		super();
		this.id = id;
		this.config = config;
		this.specOptionCode = specOptionCode;
	}

	public ConfigSpecOption(ConfigSpecOption configSpecOption) {
		super();
		this.id = configSpecOption.getId();
		this.config = configSpecOption.getConfig();
		this.specOptionCode = configSpecOption.getSpecOptionCode();
	}

	@Expose
	@Id
	@Column(name = "code")
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="config_code", nullable=false)
	private Config config;

	@Expose
	@Column(name="spec_option_code")
	private Long specOptionCode;

	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfigSpecOption other = (ConfigSpecOption) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	public Long getSpecOptionCode() {
		return specOptionCode;
	}

	public void setSpecOptionCode(Long specOptionCode) {
		this.specOptionCode = specOptionCode;
	}

	@Override
	public String toString() {
		return "ConfigSpecOption [id=" + id + ", config=" + config
				+ ", specOptionCode=" + specOptionCode + "]";
	}
	
	
}
