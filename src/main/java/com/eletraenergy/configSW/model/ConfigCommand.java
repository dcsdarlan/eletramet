package com.eletraenergy.configSW.model;

import java.util.ArrayList;

public class ConfigCommand {
	
	private String hashCode;
	private String meterFamilyName;
	private String meterModelName;
	private String tdsNumber;
	private Firmware firmware;
	private ArrayList<ManufactStep2> listOfManufactStep = new ArrayList<ManufactStep2>();

	public String getHashCode() {
		return hashCode;
	}
	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}
	public String getMeterFamilyName() {
		return meterFamilyName;
	}
	public void setMeterFamilyName(String meterFamilyName) {
		this.meterFamilyName = meterFamilyName;
	}
	public String getMeterModelName() {
		return meterModelName;
	}
	public void setMeterModelName(String meterModelName) {
		this.meterModelName = meterModelName;
	}
	public String getTdsNumber() {
		return tdsNumber;
	}
	public void setTdsNumber(String tdsNumber) {
		this.tdsNumber = tdsNumber;
	}
	public Firmware getFirmware() {
		return firmware;
	}
	public void setFirmware(Firmware firmware) {
		this.firmware = firmware;
	}
	public ArrayList<ManufactStep2> getListOfManufactStep() {
		return listOfManufactStep;
	}
	public void setListOfManufactStep(ArrayList<ManufactStep2> listOfManufactStep) {
		this.listOfManufactStep = listOfManufactStep;
	}
}
