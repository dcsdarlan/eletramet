package com.eletraenergy.configSW.model;

import com.eletraenergy.configSW.util.ResourceBundle;

public enum MeasuringInstrumentStateIdentifier {

	collected("collected", new MeasuringInstrumentCollectedState()),
	inUse("inUse", new MeasuringInstrumentInUseState()),
	registered("registered", new MeasuringInstrumentRegisteredState()),
	inCalibration("inCalibration", new MeasuringInstrumentInCalibrationState()),
	inRepair("inRepair", new MeasuringInstrumentInRepairState()),
	dropOff("dropOff", new MeasuringInstrumentDropOffState());
	
	private String id;
	
	private MeasuringInstrumentState measuringInstrumentState;

	private MeasuringInstrumentStateIdentifier(String id, MeasuringInstrumentState measuringInstrumentState){
		this.id = id;
		this.measuringInstrumentState = measuringInstrumentState;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getIntName() {
		return ResourceBundle.getMessage(this.id);
	}

	public MeasuringInstrumentState getMeasuringInstrumentState() {
		return measuringInstrumentState;
	}

	public void setState(MeasuringInstrumentState measuringInstrumentState) {
		this.measuringInstrumentState = measuringInstrumentState;
	}

}
