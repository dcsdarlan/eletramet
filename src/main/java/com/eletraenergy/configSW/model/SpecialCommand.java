package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.primefaces.model.SortOrder;

@Entity
@Table(name = "tb_special_command")
@NamedQueries({
	@NamedQuery(name = "SpecialCommand.findAll", query = "SELECT s FROM SpecialCommand s ORDER BY s.identifier ASC"),
	@NamedQuery(name = "SpecialCommand.lastId", query = "SELECT max(s.id) FROM SpecialCommand s"),
	@NamedQuery(name = "SpecialCommand.remove", query = "DELETE FROM SpecialCommand s WHERE s.id = :pId")
   })
public class SpecialCommand extends Model implements Serializable {

	private static final long serialVersionUID = -7522067937153704222L;

	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "identifier")
	private String identifier;
	  
	@Column(name = "description")
	private String description;

	@Column(name = "execution_way")
	private String executionWay;

	@Column(name = "data_length")
	private Integer dataLength;

	@Column(name = "data_value")
	private String dataValue;

	@Column(name = "wait_time")
	private Integer waitTime;
	
	@Column(name = "remark")
	private String remark;

	@Column(name = "protocol_identifier")
	private String protocolIdentifier;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size=50)
	@JoinTable(name = "tb_special_command_tds", schema="configSW" ,  
			joinColumns = @JoinColumn(name = "special_command_code"),
			inverseJoinColumns = @JoinColumn(name = "tds_code"))
	@Transient
	private List<Tds> tds;
	
	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExecutionWay() {
		return executionWay;
	}

	public void setExecutionWay(String executionWay) {
		this.executionWay = executionWay;
	}

	public Integer getDataLength() {
		return dataLength;
	}

	public void setDataLength(Integer dataLength) {
		this.dataLength = dataLength;
	}

	public String getDataValue() {
		return dataValue;
	}

	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}

	public Integer getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(Integer waitTime) {
		this.waitTime = waitTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getProtocolIdentifier() {
		return protocolIdentifier;
	}

	public void setProtocolIdentifier(String protocolIdentifier) {
		this.protocolIdentifier = protocolIdentifier;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public List<Tds> getTds() {
		return tds;
	}

	public void setTds(List<Tds> tds) {
		this.tds = tds;
	}
}
