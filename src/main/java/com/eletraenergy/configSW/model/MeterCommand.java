package com.eletraenergy.configSW.model;

import javax.persistence.Transient;

public class MeterCommand {
	private String CmdID;
	private String CmdTitle;
	private String CmdHint;
	private String ExecWay;
	private String DataLen;
	private String WriteData;
	private String WaitTime;
	private String Remark;
	private String ExecOrder;

	@Transient
	private String stage;
	
	public String getCmdID() {
		return CmdID;
	}
	public void setCmdID(String cmdID) {
		CmdID = cmdID;
	}
	public String getCmdTitle() {
		return CmdTitle;
	}
	public void setCmdTitle(String cmdTitle) {
		CmdTitle = cmdTitle;
	}
	public String getCmdHint() {
		return CmdHint;
	}
	public void setCmdHint(String cmdHint) {
		CmdHint = cmdHint;
	}	
	public String getExecWay() {
		return ExecWay;
	}
	public void setExecWay(String execWay) {
		ExecWay = execWay;
	}
	public String getDataLen() {
		return DataLen;
	}
	public void setDataLen(String dataLen) {
		DataLen = dataLen;
	}
	public String getWriteData() {
		return WriteData;
	}
	public void setWriteData(String writeData) {
		WriteData = writeData;
	}
	public String getExecOrder() {
		return ExecOrder;
	}
	public void setExecOrder(String execOrder) {
		ExecOrder = execOrder;
	}
	public String getWaitTime() {
		return WaitTime;
	}
	public void setWaitTime(String waitTime) {
		WaitTime = waitTime;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	public String getRemark() {
		return Remark;
	}
	public void setRemark(String remark) {
		Remark = remark;
	}
}
