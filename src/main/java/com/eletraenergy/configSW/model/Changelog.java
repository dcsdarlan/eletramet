package com.eletraenergy.configSW.model;

import java.util.ArrayList;
import java.util.List;

public class Changelog {

	String buildIdentifier = "";
	List<VersionHistory> versionsHistory = new ArrayList<VersionHistory>();
	
	Changelog() {
	}
	
	public VersionHistory addVersionHistory(int majorVersion, int minorVersion, 
			int patchVersion) {
		
		VersionHistory versionHistory = new VersionHistory(majorVersion, minorVersion, 
				patchVersion);
		
		versionsHistory.add(versionHistory);
		
		return versionHistory;
	}
	
	class VersionHistory {
		
		int majorVersion;
		int minorVersion;
		int patchVersion;
		
		List<ChangeHistory> changesHistory = new ArrayList<ChangeHistory>();

		VersionHistory(int majorVersion, int minorVersion, int patchVersion) {
			setVersionHistory(majorVersion, minorVersion, patchVersion);
		}
		
		void setVersionHistory(int majorVersion, int minorVersion, int patchVersion) {
			this.majorVersion = majorVersion;
			this.minorVersion = minorVersion;
			this.patchVersion = patchVersion;
		}
		
		public VersionHistory addChangeHistory(ChangeType type, String description) {
			ChangeHistory changeHistory = new ChangeHistory(type,description);
			changesHistory.add(changeHistory);
			return this;
		}
		
	}
	
	class ChangeHistory {
		ChangeType type;
		String description;
		
		ChangeHistory(ChangeType type, String description) {
			setChangeHistory(type, description);
		}
		
		void setChangeHistory(ChangeType type, String description){
			this.type = type;
			this.description = description;
		}
	}
	
	enum ChangeType {
		ADDED,
		CHANGED,
		DEPRECATED,
		REMOVED,
		FIXED,
		SECURITY
	}
	
}
