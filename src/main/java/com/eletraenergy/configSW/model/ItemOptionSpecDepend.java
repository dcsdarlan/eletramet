package com.eletraenergy.configSW.model;

public class ItemOptionSpecDepend {
	private String ItemOptionSpecID;
	private String ItemOptionSpecName;
	
	public String getID_ItemOptionSpec() {
		return ItemOptionSpecID;
	}
	public void setID_ItemOptionSpec(String ID_itemOptionSpec) {
		ItemOptionSpecID = ID_itemOptionSpec;
	}
	public String getNM_ItemOptionSpec() {
		return ItemOptionSpecName;
	}
	public void setNM_ItemOptionSpec(String NM_itemOptionSpec) {
		ItemOptionSpecName = NM_itemOptionSpec;
	}

}
