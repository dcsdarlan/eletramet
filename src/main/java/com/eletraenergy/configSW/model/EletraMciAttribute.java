package com.eletraenergy.configSW.model;

import java.util.ArrayList;

public class EletraMciAttribute {
	private String AttributeIdentifier;
	private String AttributeName;
	private String AttributeWayType;
	
	private ArrayList<EletraMciValue> listOfNamedItem = new ArrayList<EletraMciValue>();

	public String getAttributeIdentifier() {
		return AttributeIdentifier;
	}
	public void setAttributeIdentifier(String attributeIdentifier) {
		AttributeIdentifier = attributeIdentifier;
	}
	public String getAttributeName() {
		return AttributeName;
	}
	public void setAttributeName(String attributeName) {
		AttributeName = attributeName;
	}
	@Override
	public String toString() {
		return "EletraMciAttribute [AttributeIdentifier=" + AttributeIdentifier
				+ ", AttributeName=" + AttributeName + "]";
	}
	public ArrayList<EletraMciValue> getListOfNamedItem() {
		return listOfNamedItem;
	}
	public void setListOfNamedItem(ArrayList<EletraMciValue> listOfNamedItem) {
		this.listOfNamedItem = listOfNamedItem;
	}
	public String getAttributeWayType() {
		return AttributeWayType;
	}
	public void setAttributeWayType(String attributeWayType) {
		AttributeWayType = attributeWayType;
	}

}
