package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.primefaces.model.SortOrder;

@Entity
@Table(name = "tb_config_simulation")
@NamedQueries({
	@NamedQuery(name = "ConfigSimulation.findAll", 
			   	query = "SELECT c FROM ConfigSimulation c ORDER BY c.timestamp ASC"),
	@NamedQuery(name = "ConfigSimulation.lastId", 
				query = "SELECT max(c.id) FROM ConfigSimulation c")
   })
public class ConfigSimulation extends Model implements Serializable {

	private static final long serialVersionUID = 809993725455202320L;

	@Id
	@Column(name = "code")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	@Column(name = "config_code")
	private Integer configCode;
	
	@Column(name = "input_spec", columnDefinition = "json")
    @Convert(converter = ConfigTechSpecConverter.class)
	private String configTechSpec;
	
	@Column(name="output_command", columnDefinition = "json")
    @Convert(converter = ConfigCommandConverter.class)
	private String configCommand;
	
	@Transient
	private List<MeterCommand> meterCommands;

	@Transient
	private List<ConfigModel> configModel;

	@Transient
	private String MeterFamilyName;

	@Transient
	private String MeterModelName;
	
	@Transient
	private String TDS_Number;

	
	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;
	
	@Version
	@Column
	private Long version;

	public ConfigSimulation() {
		super();
	}
	
	public ConfigSimulation(Date timestamp, Integer configCode,
			String configTechSpec, String configCommand) {
		super();
		this.timestamp = timestamp;
		this.configCode = configCode;
		this.configTechSpec = configTechSpec;
		this.configCommand = configCommand;
	}

    public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public Integer getConfigCode() {
		return configCode;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public void setConfigCode(Integer configCode) {
		this.configCode = configCode;
	}

	public void setConfigTechSpec(String configTechSpec) {
		this.configTechSpec = configTechSpec;
	}

	public void setConfigCommand(String configCommand) {
		this.configCommand = configCommand;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfigSimulation other = (ConfigSimulation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ConfigSimulation [id=" + id + ", timestamp=" + timestamp
				+ ", configCode=" + configCode + ", configTechSpec="
				+ configTechSpec + ", configCommand=" + configCommand
				+ ", version=" + version + "]";
	}

	public String getConfigTechSpec() {
		return configTechSpec;
	}

	public String getConfigCommand() {
		return configCommand;
	}

	public List<MeterCommand> getMeterCommands() {
		return meterCommands;
	}

	public List<ConfigModel> getConfigModel() {
		return configModel;
	}

	public void setMeterCommands(List<MeterCommand> meterCommands) {
		this.meterCommands = meterCommands;
	}

	public void setConfigModel(List<ConfigModel> configModel) {
		this.configModel = configModel;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getNM_MeterFamily() {
		return MeterFamilyName;
	}
	public void setNM_MeterFamily(String nM_MeterFamily) {
		MeterFamilyName = nM_MeterFamily;
	}
	public String getNM_MeterModel() {
		return MeterModelName;
	}
	public void setNM_MeterModel(String nM_MeterModel) {
		MeterModelName = nM_MeterModel;
	}
	public String getTDS_Number() {
		return TDS_Number;
	}
	public void setTDS_Number(String tDS_Number) {
		TDS_Number = tDS_Number;
	}	
}
