package com.eletraenergy.configSW.model;

import java.util.ArrayList;

public class ItemSpec {
	private String ItemSpecID;
	private String ItemSpecName;
	private String ItemSpecDisplayOrder;
	private String generateCommandIdentifier;
	private String ItemSpecValue;
	private ArrayList<ItemOptionSpec> ListOfItemOptionSpec = new ArrayList<ItemOptionSpec>();
	private ArrayList<ItemSpecRuleDomain> ListOfItemSpecRuleDomain = new ArrayList<ItemSpecRuleDomain>();
	
	public String getID_ItemSpec() {
		return ItemSpecID;
	}
	public void setID_ItemSpec(String iD_ItemSpec) {
		ItemSpecID = iD_ItemSpec;
	}
	public String getNM_ItemSpec() {
		return ItemSpecName;
	}
	public void setNM_ItemSpec(String nM_ItemSpec) {
		ItemSpecName = nM_ItemSpec;
	}
	public ArrayList<ItemOptionSpec> getListOfItemOptionSpec() {
		return ListOfItemOptionSpec;
	}
	public void setListOfItemOptionSpec(ArrayList<ItemOptionSpec> listOfItemOptionSpec) {
		ListOfItemOptionSpec = listOfItemOptionSpec;
	}
	public ArrayList<ItemSpecRuleDomain> getListOfItemSpecRuleDomain() {
		return ListOfItemSpecRuleDomain;
	}
	public void setListOfItemSpecRuleDomain(ArrayList<ItemSpecRuleDomain> listOfItemSpecRuleDomain) {
		ListOfItemSpecRuleDomain = listOfItemSpecRuleDomain;
	}
	public String getItemSpecDisplayOrder() {
		return ItemSpecDisplayOrder;
	}
	public void setItemSpecDisplayOrder(String itemSpecDisplayOrder) {
		ItemSpecDisplayOrder = itemSpecDisplayOrder;
	}
	public String getItemSpecValue() {
		return ItemSpecValue;
	}
	public void setItemSpecValue(String itemSpecValue) {
		ItemSpecValue = itemSpecValue;
	}
	public String getGenerateCommandIdentifier() {
		return generateCommandIdentifier;
	}
	public void setGenerateCommandIdentifier(String generateCommandIdentifier) {
		this.generateCommandIdentifier = generateCommandIdentifier;
	}
}
