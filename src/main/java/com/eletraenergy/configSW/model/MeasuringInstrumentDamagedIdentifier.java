package com.eletraenergy.configSW.model;

import com.eletraenergy.configSW.util.ResourceBundle;

public enum MeasuringInstrumentDamagedIdentifier {

	yes("yes"),
	no("no");
	
	private String id;
	
	private MeasuringInstrumentDamagedIdentifier(String id){
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getIntName() {
		return ResourceBundle.getMessage(this.id);
	}

}
