package com.eletraenergy.configSW.model;

import java.util.ArrayList;

public class ManufactStep2 {
	private String ManufactStepID;
	private String ManufactStepName;
	private ArrayList<MeterCommand> ListOfMeterCommand = new ArrayList<MeterCommand>();

	public String getManufactStepID() {
		return ManufactStepID;
	}
	public void setManufactStepID(String manufactStepID) {
		ManufactStepID = manufactStepID;
	}
	public String getManufactStepName() {
		return ManufactStepName;
	}
	public void setManufactStepName(String manufactStepName) {
		ManufactStepName = manufactStepName;
	}
	public ArrayList<MeterCommand> getListOfMeterCommand() {
		return ListOfMeterCommand;
	}
	public void setListOfMeterCommand(ArrayList<MeterCommand> listOfMeterCommand) {
		ListOfMeterCommand = listOfMeterCommand;
	}
}
