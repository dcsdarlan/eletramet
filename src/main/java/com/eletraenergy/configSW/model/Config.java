package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RevisionType;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.util.ResourceBundle;
import com.google.gson.annotations.Expose;

@Audited
@AuditTable(value = "tb_config_aud")
@Entity
@Table(name = "tb_config")
@NamedQueries({
	@NamedQuery(name = "Config.findAll", query = "SELECT c FROM Config c ORDER BY c.tds.identifier ASC, c.changeModified DESC"),
	@NamedQuery(name = "Config.findById", query = "SELECT c FROM Config c WHERE c.id IN (:pId) ORDER BY c.tds.identifier ASC, c.changeModified DESC"),
	@NamedQuery(name = "Config.lastId", query = "SELECT max(c.id) FROM Config c"),
	@NamedQuery(name = "Config.remove", query = "DELETE FROM Config c WHERE c.id = :pId")
   })
public class Config extends Model implements Serializable {

	private static final long serialVersionUID = 6823759211734433233L;

	@Expose
	@Id
	@Column(name = "code")
	private Long id;

	@Expose
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="tds_code", nullable=false)
	@BatchSize(size=50)
	private Tds tds;
	
	@Expose
	@Column(name = "state_identifier")
	private String state;

	@Column(name = "state_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date stateModified;

	@Column(name = "change_version")
	private Integer changeVersion;

	@Column(name = "change_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date changeModified;

	@Expose
	@Column(name = "change_state_identifier")
	private String changeIdentifier;
	
	@Expose
	@OneToMany(cascade = CascadeType.ALL, mappedBy="config")
	@BatchSize(size=50)
	@NotAudited
	private List<ConfigSpecOption> specOptions;
	
	@Transient
	@Expose
	@NotAudited
	private List<ConfigSpecRuleDomain> specRulesDomain;
	
	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;
	
	@Transient
	private String stateName;

	@Transient
	private String changeIdentifierName;

	@Transient
	private Long transaction;

	@Transient
	private Date dateAudit;

	@Transient
	private RevisionType typeReview;

	@Transient
	private User userReview;
	
	@Transient
	private MeterLine meterLine;
	
	@Transient
	private MeterModel meterModel;

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Tds getTds() {
		return tds;
	}

	public void setTds(Tds tds) {
		this.tds = tds;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getStateModified() {
		return stateModified;
	}

	public void setStateModified(Date stateModified) {
		this.stateModified = stateModified;
	}

	public Integer getChangeVersion() {
		return changeVersion;
	}

	public void setChangeVersion(Integer changeVersion) {
		this.changeVersion = changeVersion;
	}

	public Date getChangeModified() {
		return changeModified;
	}

	public void setChangeModified(Date changeModified) {
		this.changeModified = changeModified;
	}

	public String getChangeIdentifier() {
		return this.changeIdentifier;
	}

	public void setChangeIdentifier(String changeIdentifier) {
		this.changeIdentifier = changeIdentifier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((changeIdentifier == null) ? 0 : changeIdentifier.hashCode());
		result = prime * result
				+ ((changeModified == null) ? 0 : changeModified.hashCode());
		result = prime * result
				+ ((changeVersion == null) ? 0 : changeVersion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((specOptions == null) ? 0 : specOptions.hashCode());
		result = prime * result
				+ ((specRulesDomain == null) ? 0 : specRulesDomain.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result
				+ ((stateModified == null) ? 0 : stateModified.hashCode());
		result = prime * result + ((tds == null) ? 0 : tds.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!obj.getClass().isAssignableFrom(getClass()) && !getClass().isAssignableFrom(obj.getClass()))
			return false;
		Config other = (Config) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}


	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public List<ConfigSpecOption> getSpecOptions() {
		return specOptions;
	}

	public void setSpecOptions(List<ConfigSpecOption> specOptions) {
		this.specOptions = specOptions;
	}

	public List<ConfigSpecRuleDomain> getSpecRulesDomain() {
		return specRulesDomain;
	}

	public void setSpecRulesDomain(List<ConfigSpecRuleDomain> specRulesDomain) {
		this.specRulesDomain = specRulesDomain;
	}

	public String getStateName() {
		return state==null?null:ResourceBundle.getMessage(state);
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public void setChangeIdentifierName(String changeIdentifierName) {
		this.changeIdentifierName = changeIdentifierName;
	}

	public String getChangeIdentifierName() {
		return ResourceBundle.getMessage(changeIdentifier);
	}

	public Date getDateAudit() {
		return dateAudit;
	}

	public void setDateAudit(Date dateAudit) {
		this.dateAudit = dateAudit;
	}

	public RevisionType getTypeReview() {
		return typeReview;
	}

	public void setTypeReview(RevisionType typeReview) {
		this.typeReview = typeReview;
	}

	public String getTypeReviewInt() {
		return ResourceBundle.getMessage(typeReview.name());
	}

	public Long getTransaction() {
		return transaction;
	}

	public void setTransaction(Long transaction) {
		this.transaction = transaction;
	}

	public User getUserReview() {
		return userReview;
	}

	public void setUserReview(User userReview) {
		this.userReview = userReview;
	}

	public MeterLine getMeterLine() {
		return meterLine;
	}

	public void setMeterLine(MeterLine meterLine) {
		this.meterLine = meterLine;
	}

	public MeterModel getMeterModel() {
		return meterModel;
	}

	public void setMeterModel(MeterModel meterModel) {
		this.meterModel = meterModel;
	}
}
