package com.eletraenergy.configSW.model;

import java.util.ArrayList;

public class ItemOptionSpec {
	private String ItemOptionSpecID;
	private String ItemOptionSpecName;
	private ArrayList<ItemSpecDepend> ListOfItemSpecDepend = new ArrayList<ItemSpecDepend>();
	
	public String getID_ItemOptionSpec() {
		return ItemOptionSpecID;
	}
	public void setID_ItemOptionSpec(String iD_ItemOptionSpec) {
		ItemOptionSpecID = iD_ItemOptionSpec;
	}
	public String getNM_ItemOptionSpec() {
		return ItemOptionSpecName;
	}
	public void setNM_ItemOptionSpec(String nM_ItemOptionSpec) {
		ItemOptionSpecName = nM_ItemOptionSpec;
	}
	public ArrayList<ItemSpecDepend> getListOfItemSpecDepend() {
		return ListOfItemSpecDepend;
	}
	public void setListOfItemSpecDepend(ArrayList<ItemSpecDepend> listOfItemSpecDepend) {
		ListOfItemSpecDepend = listOfItemSpecDepend;
	}
}
