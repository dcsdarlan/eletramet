package com.eletraenergy.configSW.model;

import java.util.ArrayList;

public class ConfigTechSpec {
	private String MeterFamilyName;
	private String MeterModelName;
	private String TDS_Number;
	private String state_identifier;
	private String state_timestamp;
	private String change_version;
	private String change_timestamp;
	private String change_state_identifier;
	private ArrayList<ItemSpecCategory> ListOfItemSpecCategory = new ArrayList<ItemSpecCategory>();
	
	public String getNM_MeterFamily() {
		return MeterFamilyName;
	}
	public void setNM_MeterFamily(String nM_MeterFamily) {
		MeterFamilyName = nM_MeterFamily;
	}
	public String getNM_MeterModel() {
		return MeterModelName;
	}
	public void setNM_MeterModel(String nM_MeterModel) {
		MeterModelName = nM_MeterModel;
	}
	public String getTDS_Number() {
		return TDS_Number;
	}
	public void setTDS_Number(String tDS_Number) {
		TDS_Number = tDS_Number;
	}
	public ArrayList<ItemSpecCategory> getListOfItemSpecCategory() {
		return ListOfItemSpecCategory;
	}
	public void setListOfItemSpecCategory(ArrayList<ItemSpecCategory> listOfItemSpecCategory) {
		ListOfItemSpecCategory = listOfItemSpecCategory;
	}
	public String getState_identifier() {
		return state_identifier;
	}
	public void setState_identifier(String state_identifier) {
		this.state_identifier = state_identifier;
	}
	public String getState_timestamp() {
		return state_timestamp;
	}
	public void setState_timestamp(String state_timestamp) {
		this.state_timestamp = state_timestamp;
	}
	public String getChange_version() {
		return change_version;
	}
	public void setChange_version(String change_version) {
		this.change_version = change_version;
	}
	public String getChange_timestamp() {
		return change_timestamp;
	}
	public void setChange_timestamp(String change_timestamp) {
		this.change_timestamp = change_timestamp;
	}
	public String getChange_state_identifier() {
		return change_state_identifier;
	}
	public void setChange_state_identifier(String change_state_identifier) {
		this.change_state_identifier = change_state_identifier;
	}
	@Override
	public String toString() {
		return "ConfigTechSpec [MeterFamilyName=" + MeterFamilyName
				+ ", MeterModelName=" + MeterModelName + ", TDS_Number="
				+ TDS_Number + ", state_identifier=" + state_identifier
				+ ", state_timestamp=" + state_timestamp + ", change_version="
				+ change_version + ", change_timestamp=" + change_timestamp
				+ ", change_state_identifier=" + change_state_identifier
				+ ", ListOfItemSpecCategory=" + ListOfItemSpecCategory + "]";
	}
	
	
}
