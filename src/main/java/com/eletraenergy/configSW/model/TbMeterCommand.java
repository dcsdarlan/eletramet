package com.eletraenergy.configSW.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.envers.NotAudited;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.util.ResourceBundle;

@Entity
@Table(name = "tb_meter_command")
@NamedQueries({
	@NamedQuery(name = "TbMeterCommand.findByConfig", query = "SELECT a FROM TbMeterCommand a WHERE a.config = :pConfig ORDER BY a.type ASC"),
	@NamedQuery(name = "TbMeterCommand.findByConfigAndOption", query = "SELECT a FROM TbMeterCommand a WHERE a.config = :pConfig and a.configSpecOption.specOptionCode = :pOption ORDER BY a.type ASC"),
	@NamedQuery(name = "TbMeterCommand.findAll", query = "SELECT a FROM TbMeterCommand a ORDER BY a.type ASC"),
	@NamedQuery(name = "TbMeterCommand.lastId", query = "SELECT max(a.id) FROM TbMeterCommand a"),
	@NamedQuery(name = "TbMeterCommand.remove", query = "DELETE FROM TbMeterCommand a WHERE a.id = :pId")
   })
public class TbMeterCommand extends Model implements Serializable {

	private static final long serialVersionUID = -4523191861370358211L;

	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "type")
	private String type;

	@Column(name = "identifier")
	private String identifier;
	  
	@Column(name = "title")
	private String title;

	@Column(name = "hint")
	private String hint;

	@Column(name = "wait_time")
	private Integer waitTime;

	@Column(name = "execution_way")
	private String executionWay;

	@Column(name = "remark")
	private String remark;

	@Column(name = "data_length")
	private Integer dataLength;
	
	@Column(name = "data_value")
	private String dataValue;
	
	@Column(name = "mci_schema_identifier")
	private String mciSchemaIdentifier;

	@Column(name = "mci_command_identifier")
	private String mciCommandIdentifier;
	
	@Column(name = "mci_attribute_identifier")
	private String mciAttributeIdentifier;

	@Column(name = "mci_atribute_value")
	private String mciAtributeValue;

	@Column(name = "execution_order")
	private Integer executionOrder;

	@NotAudited	
	@ManyToOne()
	@JoinColumn(name="manufact_step_code", nullable=false)
	private ManufactStep manufactStep;

	@NotAudited	
	@ManyToOne()
	@JoinColumn(name="config_code", nullable=false)
	private Config config;

	@NotAudited	
	@ManyToOne()
	@JoinColumn(name="config_spec_option_code", nullable=false)
	private ConfigSpecOption configSpecOption;

	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;
	
	@Transient
	private String executionWayDesc;

	@Transient
	private String commandName;

	@Transient
	private String attributeName;

	@Transient
	private String valueName;

	@Transient
	private String specOptionName;

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getTypeName() {
		String ret = null;
		if (type.equals("m")) {
			ret = "manual";
		}
		if (type.equals("i")) {
			ret = "integration";
		}
		if (type.equals("s")) {
			ret = "especial";
		}
		return ret==null?"":ResourceBundle.getMessage(ret);
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public String getHint() {
		return hint;
	}

	public String getHintTruncate() {
		return hint.length()>30?hint.substring(0, hint.length()-3)+"...":hint;
	}
	
	public void setHint(String hint) {
		this.hint = hint;
	}

	public Integer getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(Integer waitTime) {
		this.waitTime = waitTime;
	}

	public String getExecutionWay() {
		return executionWay;
	}

	public void setExecutionWay(String executionWay) {
		this.executionWay = executionWay;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getDataLength() {
		return dataLength;
	}

	public void setDataLength(Integer dataLength) {
		this.dataLength = dataLength;
	}

	public String getDataValue() {
		return dataValue;
	}

	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}

	public String getMciSchemaIdentifier() {
		return mciSchemaIdentifier;
	}

	public void setMciSchemaIdentifier(String mciSchemaIdentifier) {
		this.mciSchemaIdentifier = mciSchemaIdentifier;
	}

	public String getMciCommandIdentifier() {
		return mciCommandIdentifier;
	}

	public void setMciCommandIdentifier(String mciCommandIdentifier) {
		this.mciCommandIdentifier = mciCommandIdentifier;
	}

	public String getMciAttributeIdentifier() {
		return mciAttributeIdentifier;
	}

	public void setMciAttributeIdentifier(String mciAttributeIdentifier) {
		this.mciAttributeIdentifier = mciAttributeIdentifier;
	}

	public String getMciAtributeValue() {
		return mciAtributeValue;
	}

	public void setMciAtributeValue(String mciAtributeValue) {
		this.mciAtributeValue = mciAtributeValue;
	}

	public Integer getExecutionOrder() {
		return executionOrder;
	}

	public void setExecutionOrder(Integer executionOrder) {
		this.executionOrder = executionOrder;
	}

	public ManufactStep getManufactStep() {
		return manufactStep;
	}

	public void setManufactStep(ManufactStep manufactStep) {
		this.manufactStep = manufactStep;
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getExecutionWayDesc() {
		return executionWay==null?null:ResourceBundle.getMessage(executionWay);
	}

	public void setCommandName(String commandName) {
		this.commandName = commandName;
	}

	public String getCommandName() {
		return this.commandName;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getValueName() {
		return valueName;
	}

	public void setValueName(String valueName) {
		this.valueName = valueName;
	}

	public String getSpecOptionName() {
		return specOptionName;
	}

	public void setSpecOptionName(String specOptionName) {
		this.specOptionName = specOptionName;
	}

	public ConfigSpecOption getConfigSpecOption() {
		return configSpecOption;
	}

	public void setConfigSpecOption(ConfigSpecOption configSpecOption) {
		this.configSpecOption = configSpecOption;
	}
}