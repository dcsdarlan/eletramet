package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.primefaces.model.SortOrder;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "tb_spec_rule")
@NamedQueries({
	@NamedQuery(name = "SpecRule.findAll", query = "SELECT s FROM SpecRule s WHERE s.parent is null ORDER BY s.id ASC"),
	@NamedQuery(name = "SpecRule.lastId", query = "SELECT max(s.id) FROM SpecRule s"),
	@NamedQuery(name = "SpecRule.checkIdentifier", query = "SELECT s FROM SpecRule s WHERE s.identifier = :pIdentifier"),
	@NamedQuery(name = "SpecRule.remove", query = "DELETE FROM SpecRule s WHERE s.id = :pId")
   })
public class SpecRule extends Model implements Serializable {

	private static final long serialVersionUID = 282783272608086133L;

	public SpecRule() {
		super();
	}

	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "identifier")
	private String identifier;
	
	@Column(name = "name")
	private String name;

	@ManyToOne()
	@JoinColumn(name="parent_code")
	private SpecRule parent;
	
	@Expose
	@OneToMany(cascade = CascadeType.ALL, mappedBy="specRule", fetch=FetchType.EAGER)
	@BatchSize(size=10)
	private List<SpecRuleDomain> domains;

	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;

	@Transient
	@Expose
	private Boolean check;
	
	@Transient
	private Boolean isInput;
	
	@Transient
	private Boolean isNode;
	
	public SpecRule(SpecRule specRule2) {
		this.id = specRule2.getId();
		this.identifier = specRule2.getIdentifier();
		this.name = specRule2.getName();
		this.parent = specRule2.getParent();
		this.domains = specRule2.getDomains();
		this.check = specRule2.getCheck();	
		isInput = false;
		isNode = false;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public SpecRule getParent() {
		return parent;
	}

	public void setParent(SpecRule parent) {
		this.parent = parent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpecRule other = (SpecRule) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public List<SpecRuleDomain> getDomains() {
		return domains;
	}

	public void setDomains(List<SpecRuleDomain> domains) {
		this.domains = domains;
	}

	public Boolean getCheck() {
		return check;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}

	@Override
	public String toString() {
		return "SpecRule [id=" + id + ", identifier=" + identifier + ", name="
				+ name + ", parent=" + parent + ", domains=" + domains
				+ ", check=" + check + "]";
	}

	public Boolean getIsInput() {
		return isInput;
	}

	public void setIsInput(Boolean isInput) {
		this.isInput = isInput;
	}

	public Boolean getIsNode() {
		return isNode;
	}

	public void setIsNode(Boolean isNode) {
		this.isNode = isNode;
	}

}
