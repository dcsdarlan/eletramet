package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RevisionType;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.util.ResourceBundle;
import com.google.gson.annotations.Expose;

@Audited
@AuditTable(value = "tb_measuring_instrument_aud")
@Entity
@Table(name = "tb_calibration_laboratory_telphone")
@NamedQueries({
		@NamedQuery(name = "CalibrationLaboratoryTelphone.findAll", query = "SELECT c FROM CalibrationLaboratoryTelphone c ORDER BY c.id ASC"),
		@NamedQuery(name = "CalibrationLaboratoryTelphone.findLaboratory", query = "SELECT c FROM CalibrationLaboratoryTelphone c WHERE c.calibration_laboratory = :pId ORDER BY c.id ASC"),
		@NamedQuery(name = "CalibrationLaboratoryTelphone.lastId", query = "SELECT max(c.id) FROM CalibrationLaboratoryTelphone c"),
		@NamedQuery(name = "CalibrationLaboratoryTelphone.remove", query = "DELETE FROM CalibrationLaboratoryTelphone c WHERE c.id = :pId"),
		@NamedQuery(name = "CalibrationLaboratoryTelphone.removeLaboratory", query = "DELETE FROM CalibrationLaboratoryTelphone c WHERE c.calibration_laboratory = :pId")
})

public class CalibrationLaboratoryTelphone extends Model implements Serializable {

	private static final long serialVersionUID = -3176063343118662782L;

	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "number")
	private String number;

	@Column(name = "ddd")
	private String ddd;

	@Column(name = "calibration_laboratory_code")
	private long calibration_laboratory;


	@Transient
	private SortOrder sortOrder;

	@Transient
	private String sortField;


	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public long getCalibration_laboratory() {
		return calibration_laboratory;
	}

	public void setCalibration_laboratory(long calibration_laboratory) {
		this.calibration_laboratory = calibration_laboratory;
	}
}
