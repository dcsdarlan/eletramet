package com.eletraenergy.configSW.model;

import com.eletraenergy.configSW.util.ResourceBundle;

public enum TdsStateIdentifier {

	c("c", new TdsStateCreate()), 		//criada
	w("w", new TdsStateWait()),			//aguardando bom
	a("a", new TdsStateActive()),		//ativa
	u("u", new TdsStateDeactivate()),	//desativada
	e("e", new TdsStateCancel());		//cancelada
	
	private String id;
	
	private TdsState state;

	private TdsStateIdentifier(String id, TdsState state){
		this.id = id;
		this.state = state;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getIntName() {
		return ResourceBundle.getMessage(this.id);
	}

	public TdsState getState() {
		return state;
	}

	public void setState(TdsState state) {
		this.state = state;
	}

}
