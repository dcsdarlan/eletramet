package com.eletraenergy.configSW.model;

public enum TransactionReview {

	CREATE_USER								(1, "createUser"), 
	ALTER_USER								(2, "updateUser"), 
	DELETE_USER								(3, "removeUser"), 

	CREATE_MEASURING_INSTRUMENT				(4, "createMeasuringInstrument"), 
	ALTER_MEASURING_INSTRUMENT				(5, "updateMeasuringInstrument"), 
	DELETE_MEASURING_INSTRUMENT				(6, "removeMeasuringInstrument"),
	
	CREATE_MEASURING_INSTRUMENT_MANUFACTURER (7, "removeMeasuringInstrument"),
	ALTER_MEASURING_INSTRUMENT_MANUFACTURER  (8, "removeMeasuringInstrument"),
	DELETE_MEASURING_INSTRUMENT_MANUFACTURER (9, "removeMeasuringInstrument");
	
	private TransactionReview(Integer code, String description) {
		this.setCode(code);
		this.description = description;
	}
	
	private Integer code;
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}
