package com.eletraenergy.configSW.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.util.ResourceBundle;

@Entity
@Table(name = "tb_tds_spec")
@NamedQueries({
	@NamedQuery(name = "TdsSpec.findAll", query = "SELECT s FROM TdsSpec s ORDER BY s.name ASC"),
	@NamedQuery(name = "TdsSpec.lastId", query = "SELECT max(s.id) FROM TdsSpec s"),
	@NamedQuery(name = "TdsSpec.checkIdentifier", query = "SELECT s FROM TdsSpec s WHERE s.identifier = :pIdentifier")
   })
public class TdsSpec extends Model implements Serializable {

	private static final long serialVersionUID = 2850518210542746661L;

	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "name_en")
	private String nameEn;
	
	@Column(name = "name_cn")
	private String nameCn;
	
	@Column(name = "identifier")
	private String identifier;
	
	@Column(name = "description")
	private String description;

	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}
	
	public String getNameCn() {
		return nameCn;
	}

	public void setNameCn(String nameCn) {
		this.nameCn = nameCn;
	}
	
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public String getIntName() {
		return (ResourceBundle.isEnglishLocale() ? nameEn :name);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TdsSpec other = (TdsSpec) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

}
