package com.eletraenergy.configSW.model;

public class ItemSpecRuleDomain {
	private String ItemSpecRuleDomainName;
	private String ItemSpecRuleDomainID;
	private String ItemSpecRuleDomainValue;
	
	public String getNM_ItemSpecRuleDomain() {
		return ItemSpecRuleDomainName;
	}
	public void setNM_ItemSpecRuleDomain(String nM_ItemSpecRuleDomain) {
		ItemSpecRuleDomainName = nM_ItemSpecRuleDomain;
	}
	public String getID_ItemSpecRuleDomain() {
		return ItemSpecRuleDomainID;
	}
	public void setID_ItemSpecRuleDomain(String iD_ItemSpecRuleDomain) {
		ItemSpecRuleDomainID = iD_ItemSpecRuleDomain;
	}
	public String getVL_ItemSpecRuleDomain() {
		return ItemSpecRuleDomainValue;
	}
	public void setVL_ItemSpecRuleDomain(String vL_ItemSpecRuleDomain) {
		ItemSpecRuleDomainValue = vL_ItemSpecRuleDomain;
	}
}
