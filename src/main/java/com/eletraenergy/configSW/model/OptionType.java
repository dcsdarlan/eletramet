package com.eletraenergy.configSW.model;


public enum OptionType {

	InputText(0, "inputText"),
	SelectOne(1, "selectOne"),
	SelectMulti(2, "selectMulti"),
	Radio(3, "radio"),
	Read(4, "read"),
	InputNumber(5, "inputNumber"),
	InputDecimal(6, "inputDecimal");
	
	private Integer id;
	
	private String name;

	private OptionType(Integer id, String name){
		this.id = id;
		this.name = name;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}	
}
