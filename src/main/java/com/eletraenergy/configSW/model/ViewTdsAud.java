package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "vw_tds_aud")
@NamedQueries({
	@NamedQuery(name = "ViewTdsAud.findAll", 
			   query = "SELECT new com.eletraenergy.configSW.model.ViewTdsAud(tds_aud.id, tds_aud.changeVersion, tds_aud.changeModified, tds_aud.idUserLdap, tds_aud.dateAudit) from ViewTdsAud tds_aud"),
	@NamedQuery(name = "ViewTdsAud.byId", 
			   query = "SELECT new com.eletraenergy.configSW.model.ViewTdsAud(tds_aud.id, tds_aud.changeVersion, tds_aud.changeModified, tds_aud.idUserLdap, tds_aud.dateAudit) from ViewTdsAud tds_aud WHERE tds_aud.id = :pTdsId") 			  
   })

public class ViewTdsAud extends Model implements Serializable {

	private static final long serialVersionUID = -3453628350746939658L;

	public ViewTdsAud() {
		super();
	}

	public ViewTdsAud(Long id, Integer tdsChangeVersion, Date tdsChangeTimestamp, String idUserLdap,
			Date dateAudit) {
		super();
		this.id = id;
		this.changeVersion = tdsChangeVersion;
		this.changeModified = tdsChangeTimestamp;
		this.idUserLdap = idUserLdap;
		this.dateAudit = dateAudit;
	}
	
	@Id
	@Column(name = "tds_code")
	private Long id;
	
	@Column(name = "tds_change_version")
	private Integer changeVersion;

	@Column(name = "tds_change_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date changeModified;	
	
	@Column(name = "id_user_ldap")
	private String idUserLdap;

	@Column(name = "dateaudit")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateAudit;
	
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getChangeVersion() {
		return changeVersion;
	}

	public void setChangeVersion(Integer changeVersion) {
		this.changeVersion = changeVersion;
	}

	public Date getChangeModified() {
		return changeModified;
	}

	public void setChangeModified(Date changeModified) {
		this.changeModified = changeModified;
	}

	public String getIdUserLdap() {
		return this.idUserLdap;
	}

	public void setIdUserLdap(String idUserLdap) {
		this.idUserLdap = idUserLdap;
	}
	
	public Date getDateAudit() {
		return dateAudit;
	}

	public void setDateAudit(Date dateAudit) {
		this.dateAudit = dateAudit;
	}
	
}