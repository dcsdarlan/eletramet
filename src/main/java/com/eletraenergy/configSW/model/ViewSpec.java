package com.eletraenergy.configSW.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "vw_spec")
@NamedQueries({
	@NamedQuery(name = "ViewSpec.findAll", 
			   query = "SELECT v from ViewSpec v ORDER BY v.specCategoryOrder, v.specOrder, v.specOptionName ASC")
   })
public class ViewSpec extends Model implements Serializable, Comparable<ViewSpec> {

	private static final long serialVersionUID = -3484139962834390566L;

	public ViewSpec() {
		super();
	}

    public ViewSpec(Long specCategorCode, String specCategoryIdentifier,
			String specCategoryName, String specCategoryDescription,
			Integer specCategoryOrder, Long specCode, String specIdentifier,
			String specName, String specDescription, Integer specOrder,
			Long specOptionCode, String specOptionIdentifier,
			String specOptionName, String specOptionDescription, String title, Boolean isItem) {
		super();
		this.specCategorCode = specCategorCode;
		this.specCategoryIdentifier = specCategoryIdentifier;
		this.specCategoryName = specCategoryName;
		this.specCategoryDescription = specCategoryDescription;
		this.specCategoryOrder = specCategoryOrder;
		this.specOrder = specOrder;
		this.specOptionName = specOptionName;
		this.specCode = specCode;
		this.specIdentifier = specIdentifier;
		this.specName = specName;
		this.specDescription = specDescription;
		this.id = specOptionCode;
		this.specOptionIdentifier = specOptionIdentifier;
		this.specOptionDescription = specOptionDescription;
		this.title = title;
		this.isItem = isItem;
	}

	@Column(name = "spec_category_order")
	private Integer specCategoryOrder;

	@Column(name = "spec_order")
	private Integer specOrder;
	
	@Column(name = "spec_option_name")
	private String specOptionName;
    
    @Column(name = "spec_category_code")
	private Long specCategorCode;

	@Column(name = "spec_category_identifier")
	private String specCategoryIdentifier;

	@Column(name = "spec_category_name")
	private String specCategoryName;

	@Column(name = "spec_category_description")
	private String specCategoryDescription;

	@Column(name = "spec_code")
	private Long specCode;

	@Column(name = "spec_identifier")
	private String specIdentifier;
	
	@Column(name = "spec_name")
	private String specName;
	
	@Column(name = "spec_description")
	private String specDescription;
	
	@Id
	@Column(name = "spec_option_code")
	private Long id;

	@Column(name = "spec_option_identifier")
	private String specOptionIdentifier;

	@Column(name = "spec_option_description")
	private String specOptionDescription;
	
	@Transient
	private String title;

	@Transient
	private Boolean isItem;

	@Expose
	@Transient
	private Boolean checked;

	@Transient
	private Boolean rulesInformed;

	public Long getSpecCategorCode() {
		return specCategorCode;
	}

	public void setSpecCategorCode(Long specCategorCode) {
		this.specCategorCode = specCategorCode;
	}

	public String getSpecCategoryIdentifier() {
		return specCategoryIdentifier;
	}

	public void setSpecCategoryIdentifier(String specCategoryIdentifier) {
		this.specCategoryIdentifier = specCategoryIdentifier;
	}

	public String getSpecCategoryName() {
		return specCategoryName;
	}

	public void setSpecCategoryName(String specCategoryName) {
		this.specCategoryName = specCategoryName;
	}

	public String getSpecCategoryDescription() {
		return specCategoryDescription;
	}

	public void setSpecCategoryDescription(String specCategoryDescription) {
		this.specCategoryDescription = specCategoryDescription;
	}

	public Long getSpecCode() {
		return specCode;
	}

	public void setSpecCode(Long specCode) {
		this.specCode = specCode;
	}

	public String getSpecIdentifier() {
		return specIdentifier;
	}

	public void setSpecIdentifier(String specIdentifier) {
		this.specIdentifier = specIdentifier;
	}

	public String getSpecName() {
		return specName;
	}

	public void setSpecName(String specName) {
		this.specName = specName;
	}

	public String getSpecDescription() {
		return specDescription;
	}

	public void setSpecDescription(String specDescription) {
		this.specDescription = specDescription;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpecOptionIdentifier() {
		return specOptionIdentifier;
	}

	public void setSpecOptionIdentifier(String specOptionIdentifier) {
		this.specOptionIdentifier = specOptionIdentifier;
	}

	public String getSpecOptionDescription() {
		return specOptionDescription;
	}

	public Integer getSpecCategoryOrder() {
		return specCategoryOrder;
	}

	public void setSpecCategoryOrder(Integer specCategoryOrder) {
		this.specCategoryOrder = specCategoryOrder;
	}

	public Integer getSpecOrder() {
		return specOrder;
	}

	public void setSpecOrder(Integer specOrder) {
		this.specOrder = specOrder;
	}

	public String getSpecOptionName() {
		return specOptionName;
	}

	public void setSpecOptionName(String specOptionName) {
		this.specOptionName = specOptionName;
	}

	public void setSpecOptionDescription(String specOptionDescription) {
		this.specOptionDescription = specOptionDescription;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int compareTo(ViewSpec o) {
		return title.compareTo(title);
	}

	public Boolean getIsItem() {
		return isItem;
	}

	public void setIsItem(Boolean isItem) {
		this.isItem = isItem;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public Boolean getRulesInformed() {
		return rulesInformed;
	}

	public void setRulesInformed(Boolean rulesInformed) {
		this.rulesInformed = rulesInformed;
	}

	@Override
	public String toString() {
		return "ViewSpec [specOptionName=" + specOptionName
				+ ", specCategorCode=" + specCategorCode
				+ ", specCategoryIdentifier=" + specCategoryIdentifier
				+ ", specCategoryName=" + specCategoryName
				+ ", specCategoryDescription=" + specCategoryDescription
				+ ", specCode=" + specCode + ", specIdentifier="
				+ specIdentifier + ", specName=" + specName
				+ ", specDescription=" + specDescription + ", id=" + id
				+ ", specOptionIdentifier=" + specOptionIdentifier
				+ ", specOptionDescription=" + specOptionDescription + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ViewSpec other = (ViewSpec) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}