package com.eletraenergy.configSW.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.primefaces.model.SortOrder;

@Entity
@Table(name = "tb_spec")
@NamedQueries({
	@NamedQuery(name = "Spec.findAll", query = "SELECT s FROM Spec s ORDER BY s.name ASC"),
	@NamedQuery(name = "Spec.findByCategory", query = "SELECT s FROM Spec s WHERE s.specCategory = :pSpecCategory ORDER BY s.name ASC"),
	@NamedQuery(name = "Spec.lastId", query = "SELECT max(s.id) FROM Spec s"),
	@NamedQuery(name = "Spec.checkIdentifier", query = "SELECT s FROM Spec s WHERE s.identifier = :pIdentifier"),
	@NamedQuery(name = "Spec.checkOrder", query = "SELECT s FROM Spec s WHERE s.specCategory = :pSpecCategory AND s.order = :pOrder"),
	@NamedQuery(name = "Spec.remove", query = "DELETE FROM Spec s WHERE s.id = :pId")
   })
public class Spec extends Model implements Serializable {

	private static final long serialVersionUID = -168503170393689956L;

	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "identifier")
	private String identifier;
	
	@Column(name = "description")
	private String description;
	
	@ManyToOne()
	@JoinColumn(name="spec_category_code", nullable=false)
	private SpecCategory specCategory;
	
	@Column(name = "item_order")
	private Integer order;

	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SpecCategory getSpecCategory() {
		return specCategory;
	}

	public void setSpecCategory(SpecCategory specCategory) {
		this.specCategory = specCategory;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Spec other = (Spec) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

}
