package com.eletraenergy.configSW.model;

public interface TdsState {

	public void tdsAtivar(Tds tds);
	public void tdsDesativar(Tds tds);
	public void tdsCancelar(Tds tds);
	public void tdsAguardarBOM(Tds tds);
	public void tdsCriada(Tds tds);
	
	public TdsStateIdentifier[] verEstadosPossiveis();
	
}
