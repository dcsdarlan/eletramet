package com.eletraenergy.configSW.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
@Entity
@Table(name = "vw_special_command")
@NamedQueries({
	@NamedQuery(name = "ViewSpecialCommand.findByTds", 
			   query = "SELECT new com.eletraenergy.configSW.model.ViewSpecialCommand(v.id, v.tdsIdentifier, v.identifier, v.description, v.executionWay, v.dataLength, v.dataValue, v.waitTime, v.obs) from ViewSpecialCommand v WHERE v.tdsIdentifier = :pTds") 			  
   })
public class ViewSpecialCommand extends Model implements Serializable {

	private static final long serialVersionUID = 5292833964519727242L;

	public ViewSpecialCommand() {
		super();
	}

	public ViewSpecialCommand(Long id, String tdsIdentifier, String identifier,
			String description, String executionWay, Integer dataLength,
			String dataValue, Integer waitTime, String obs) {
		super();
		this.id = id;
		this.tdsIdentifier = tdsIdentifier;
		this.identifier = identifier;
		this.description = description;
		this.executionWay = executionWay;
		this.dataLength = dataLength;
		this.dataValue = dataValue;
		this.waitTime = waitTime;
		this.obs = obs;
	}

	@Id
	@Column(name = "special_command_code")
	private Long id;

	@Column(name = "tds_identifier")
	private String tdsIdentifier;

	@Column(name = "special_command_identifier")
	private String identifier;

	@Column(name = "special_command_description")
	private String description;
	
	@Column(name = "special_command_execution_way")
	private String executionWay;

	@Column(name = "special_command_data_length")
	private Integer dataLength;

	@Column(name = "special_command_data_value")
	private String dataValue;

	@Column(name = "special_command_wait_time")
	private Integer waitTime;
	
	@Column(name = "special_command_remark")
	private String obs;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTdsIdentifier() {
		return tdsIdentifier;
	}

	public void setTdsIdentifier(String tdsIdentifier) {
		this.tdsIdentifier = tdsIdentifier;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExecutionWay() {
		return executionWay;
	}

	public void setExecutionWay(String executionWay) {
		this.executionWay = executionWay;
	}

	public Integer getDataLength() {
		return dataLength;
	}

	public void setDataLength(Integer dataLength) {
		this.dataLength = dataLength;
	}

	public String getDataValue() {
		return dataValue;
	}

	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataLength == null) ? 0 : dataLength.hashCode());
		result = prime * result
				+ ((dataValue == null) ? 0 : dataValue.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((executionWay == null) ? 0 : executionWay.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((identifier == null) ? 0 : identifier.hashCode());
		result = prime * result + ((obs == null) ? 0 : obs.hashCode());
		result = prime * result
				+ ((tdsIdentifier == null) ? 0 : tdsIdentifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ViewSpecialCommand other = (ViewSpecialCommand) obj;
		if (dataLength == null) {
			if (other.dataLength != null)
				return false;
		} else if (!dataLength.equals(other.dataLength))
			return false;
		if (dataValue == null) {
			if (other.dataValue != null)
				return false;
		} else if (!dataValue.equals(other.dataValue))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (executionWay == null) {
			if (other.executionWay != null)
				return false;
		} else if (!executionWay.equals(other.executionWay))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (identifier == null) {
			if (other.identifier != null)
				return false;
		} else if (!identifier.equals(other.identifier))
			return false;
		if (obs == null) {
			if (other.obs != null)
				return false;
		} else if (!obs.equals(other.obs))
			return false;
		if (tdsIdentifier == null) {
			if (other.tdsIdentifier != null)
				return false;
		} else if (!tdsIdentifier.equals(other.tdsIdentifier))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ViewSpecialCommand [id=" + id + ", tdsIdentifier="
				+ tdsIdentifier + ", identifier=" + identifier
				+ ", description=" + description + ", executionWay="
				+ executionWay + ", dataLength=" + dataLength + ", dataValue="
				+ dataValue + ", obs=" + obs + "]";
	}

	public Integer getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(Integer waitTime) {
		this.waitTime = waitTime;
	}

}
