package com.eletraenergy.configSW.model;

import java.util.ArrayList;

public class ItemSpecCategory {
	private String ItemSpecCategoryID;
	private String ItemSpecCategoryName;
	private String ItemSpecCategoryDisplayOrder;
	
	private ArrayList<ItemSpec> ListOfItemSpec = new ArrayList<ItemSpec>();
	
	public String getID_TypeItemSpec() {
		return ItemSpecCategoryID;
	}
	public void setID_TypeItemSpec(String iD_TypeItemSpec) {
		ItemSpecCategoryID = iD_TypeItemSpec;
	}
	public String getNM_TypeItemSpec() {
		return ItemSpecCategoryName;
	}
	public void setNM_TypeItemSpec(String nM_TypeItemSpec) {
		ItemSpecCategoryName = nM_TypeItemSpec;
	}
	public ArrayList<ItemSpec> getListOfItemSpec() {
		return ListOfItemSpec;
	}
	public void setListOfItemSpec(ArrayList<ItemSpec> listOfItemSpec) {
		ListOfItemSpec = listOfItemSpec;
	}
	public String getItemSpecCategoryDisplayOrder() {
		return ItemSpecCategoryDisplayOrder;
	}
	public void setItemSpecCategoryDisplayOrder(String itemSpecCategoryDisplayOrder) {
		ItemSpecCategoryDisplayOrder = itemSpecCategoryDisplayOrder;
	}
	@Override
	public String toString() {
		return "ItemSpecCategory [ItemSpecCategoryID=" + ItemSpecCategoryID
				+ ", ItemSpecCategoryName=" + ItemSpecCategoryName
				+ ", ItemSpecCategoryDisplayOrder="
				+ ItemSpecCategoryDisplayOrder + ", ListOfItemSpec="
				+ ListOfItemSpec + "]";
	}
	
}
