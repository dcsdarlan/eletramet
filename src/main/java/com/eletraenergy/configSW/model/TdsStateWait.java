package com.eletraenergy.configSW.model;

import java.util.Date;

public class TdsStateWait implements TdsState {

	public TdsStateIdentifier[] verEstadosPossiveis() {
		TdsStateIdentifier[] newStates = new TdsStateIdentifier[2];
		newStates[0] = TdsStateIdentifier.c;
		newStates[1] = TdsStateIdentifier.a;
		return newStates;
	}

	public void tdsAtivar(Tds tds) {
		tds.setState(TdsStateIdentifier.a);
		tds.setStateModified(new Date());
	}

	public void tdsDesativar(Tds tds) {
	}

	public void tdsCancelar(Tds tds) {
	}

	public void tdsAguardarBOM(Tds tds) {
	}

	public void tdsCriada(Tds tds) {
		tds.setState(TdsStateIdentifier.c);
		tds.setStateModified(new Date());
		
	}
}
