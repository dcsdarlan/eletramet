package com.eletraenergy.configSW.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.primefaces.model.SortOrder;

@Entity
@Table(name = "tb_config_spec_rule_domain")
@NamedQueries({
	@NamedQuery(name = "ConfigSpecRuleDomain.findAll", query = "SELECT c FROM ConfigSpecRuleDomain c ORDER BY c.id ASC"),
	@NamedQuery(name = "ConfigSpecRuleDomain.findByConfig", query = "SELECT c FROM ConfigSpecRuleDomain c WHERE c.id.configCode = :pConfigCode ORDER BY c.id.specCode, c.id.specRuleDomainCode ASC"),
	@NamedQuery(name = "ConfigSpecRuleDomain.remove", query = "DELETE FROM ConfigSpecRuleDomain c WHERE c.id.configCode = :pConfigCode")
   })
public class ConfigSpecRuleDomain extends Model implements Serializable {

	private static final long serialVersionUID = 1837439695785892992L;

	public ConfigSpecRuleDomain() {
		super();
	}

	public ConfigSpecRuleDomain(ConfigSpecRuleDomainKey id,
			String specRuleDomainValue) {
		super();
		this.id = id;
		this.specRuleDomainValue = specRuleDomainValue;
		this.version = 0L;
	}

	@EmbeddedId
	private ConfigSpecRuleDomainKey id;

	@Column(name = "spec_rule_domain_value")
	private String specRuleDomainValue;

	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public ConfigSpecRuleDomainKey getId() {
		return id;
	}

	public void setId(ConfigSpecRuleDomainKey id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfigSpecRuleDomain other = (ConfigSpecRuleDomain) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSpecRuleDomainValue() {
		return specRuleDomainValue;
	}

	public void setSpecRuleDomainValue(String specRuleDomainValue) {
		this.specRuleDomainValue = specRuleDomainValue;
	}

	@Override
	public String toString() {
		return "ConfigSpecRuleDomain [id=" + id + ", specRuleDomainValue="
				+ specRuleDomainValue + "]";
	}

}
