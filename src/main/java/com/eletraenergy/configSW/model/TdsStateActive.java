package com.eletraenergy.configSW.model;

import java.util.Date;

public class TdsStateActive implements TdsState {

	public TdsStateIdentifier[] verEstadosPossiveis() {
		TdsStateIdentifier[] newStates = new TdsStateIdentifier[3];
		newStates[0] = TdsStateIdentifier.w;
		newStates[1] = TdsStateIdentifier.u;
		newStates[2] = TdsStateIdentifier.e;
		return newStates;
	}

	public void tdsAtivar(Tds tds) {
	}

	public void tdsDesativar(Tds tds) {
		tds.setState(TdsStateIdentifier.u);
		tds.setStateModified(new Date());
	}

	public void tdsCancelar(Tds tds) {
		tds.setState(TdsStateIdentifier.e);
		tds.setStateModified(new Date());
	}

	public void tdsAguardarBOM(Tds tds) {
		tds.setState(TdsStateIdentifier.w);
		tds.setStateModified(new Date());
	}
	
	public void tdsCriada(Tds tds) {
	}

}
