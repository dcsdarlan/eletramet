package com.eletraenergy.configSW.model;

import java.util.ArrayList;

public class EletraMciCommand {
	private String commandIdentifier;
	private String commandName;
	private ArrayList<EletraMciAttribute> listOfEletraMciAttribute = new ArrayList<EletraMciAttribute>();

	public String getCommandIdentifier() {
		return commandIdentifier;
	}
	public void setCommandIdentifier(String commandIdentifier) {
		this.commandIdentifier = commandIdentifier;
	}
	public String getCommandName() {
		return commandName;
	}
	public void setCommandName(String commandName) {
		this.commandName = commandName;
	}
	public ArrayList<EletraMciAttribute> getListOfEletraMciAttribute() {
		return listOfEletraMciAttribute;
	}
	public void setListOfEletraMciAttribute(ArrayList<EletraMciAttribute> listOfEletraMciAttribute) {
		this.listOfEletraMciAttribute = listOfEletraMciAttribute;
	}
	
	public String getCommandNameTrunc(){
		return commandName.length() > 30 ? commandName.substring(0, 29)+ "...": commandName;
	}
	
	@Override
	public String toString() {
		return "EletraMciCommand [commandIdentifier=" + commandIdentifier
				+ ", commandName=" + commandName
				+ ", listOfEletraMciAttribute=" + listOfEletraMciAttribute
				+ "]";
	}
	
}
