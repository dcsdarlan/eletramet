package com.eletraenergy.configSW.model;

import java.util.Date;

public class TdsStateCreate implements TdsState {

	public TdsStateIdentifier[] verEstadosPossiveis() {
		TdsStateIdentifier[] newStates = new TdsStateIdentifier[1];
		newStates[0] = TdsStateIdentifier.w;
		return newStates;
	}

	public void tdsCriada(Tds tds) {
	}

	public void tdsAtivar(Tds tds) {
	}

	public void tdsDesativar(Tds tds) {
		
	}

	public void tdsCancelar(Tds tds) {
		
	}

	public void tdsAguardarBOM(Tds tds) {
		tds.setState(TdsStateIdentifier.w);
		tds.setStateModified(new Date());
	}

}
