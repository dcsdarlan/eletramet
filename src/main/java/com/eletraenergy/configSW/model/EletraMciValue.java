package com.eletraenergy.configSW.model;

public class EletraMciValue {
	
	private String entryIdentifier;
	private String entryName;

	public String getEntryName() {
		return entryName;
	}

	public void setEntryName(String entryName) {
		this.entryName = entryName;
	}

	@Override
	public String toString() {
		return "EletraMciValue [entryName=" + getEntryName() + "]";
	}

	public String getEntryIdentifier() {
		return entryIdentifier;
	}

	public void setEntryIdentifier(String entryIdentifier) {
		this.entryIdentifier = entryIdentifier;
	}

}
