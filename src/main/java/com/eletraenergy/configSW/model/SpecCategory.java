package com.eletraenergy.configSW.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RevisionType;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.util.ResourceBundle;

@Audited
@AuditTable(value = "tb_spec_category_aud")
@Entity
@Table(name = "tb_spec_category")
@NamedQueries({
	@NamedQuery(name = "SpecCategory.findAll", query = "SELECT s FROM SpecCategory s ORDER BY s.name ASC"),
	@NamedQuery(name = "SpecCategory.lastId", query = "SELECT max(s.id) FROM SpecCategory s"),
	@NamedQuery(name = "SpecCategory.checkIdentifier", query = "SELECT s FROM SpecCategory s WHERE s.identifier = :pIdentifier"),
	@NamedQuery(name = "SpecCategory.checkOrder", query = "SELECT s FROM SpecCategory s WHERE s.order = :pOrder")
   })
public class SpecCategory extends Model implements Serializable {

	private static final long serialVersionUID = 8377942043115895408L;

	@Id
	@Column(name = "code")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "identifier")
	private String identifier;
	
	@Column(name = "description")
	private String description;

	@Column(name = "item_order")
	private Integer order;

	@Version
	@Column
	private Long version;

	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private String sortField;

	@Transient
	private Long transaction;

	@Transient
	private Date dateAudit;

	@Transient
	private RevisionType typeReview;
	
	@Transient
	private User userReview;
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpecCategory other = (SpecCategory) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Date getDateAudit() {
		return dateAudit;
	}

	public void setDateAudit(Date dateAudit) {
		this.dateAudit = dateAudit;
	}

	public RevisionType getTypeReview() {
		return typeReview;
	}

	public void setTypeReview(RevisionType typeReview) {
		this.typeReview = typeReview;
	}

	public String getTypeReviewInt() {
		return ResourceBundle.getMessage(typeReview.name());
	}

	public Long getTransaction() {
		return transaction;
	}

	public void setTransaction(Long transaction) {
		this.transaction = transaction;
	}

	public User getUserReview() {
		return userReview;
	}

	public void setUserReview(User userReview) {
		this.userReview = userReview;
	}

}
