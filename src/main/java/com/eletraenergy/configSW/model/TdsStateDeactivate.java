package com.eletraenergy.configSW.model;

import java.util.Date;

public class TdsStateDeactivate implements TdsState {

	public TdsStateIdentifier[] verEstadosPossiveis() {
		TdsStateIdentifier[] newStates = new TdsStateIdentifier[2];
		newStates[0] = TdsStateIdentifier.a;
		newStates[1] = TdsStateIdentifier.e;
		return newStates;
	}

	public void tdsAtivar(Tds tds) {
		tds.setState(TdsStateIdentifier.a);
		tds.setStateModified(new Date());
	}

	public void tdsDesativar(Tds tds) {
	}

	public void tdsCancelar(Tds tds) {
		tds.setState(TdsStateIdentifier.e);
		tds.setStateModified(new Date());
	}

	public void tdsAguardarBOM(Tds tds) {
	}

	public void tdsCriada(Tds tds) {
	}

}
