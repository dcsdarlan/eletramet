package com.eletraenergy.configSW.model;

public enum TypeReview {
	INCLUSION("inclusion"), ALTERATION("alteration"), EXCLUSION("exclusion");
	
	private TypeReview(String description) {
		this.description = description;
	}
	
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
