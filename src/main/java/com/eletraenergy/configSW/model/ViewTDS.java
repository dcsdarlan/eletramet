package com.eletraenergy.configSW.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vw_tds")
public class ViewTDS extends Model implements Serializable {

	private static final long serialVersionUID = -3453628350746939658L;

	public ViewTDS() {
		super();
	}

	public ViewTDS(TdsStateIdentifier state, Long id, String tdsNumber, String meterLineName, String meterModelName,
			 String tdsSpecName, String tdsSpecOptionName, String configCode) {
		super();
		this.id = id;
		this.meterLineName = meterLineName;
		this.meterModelName = meterModelName;
		this.tdsNumber = tdsNumber;
		this.tdsSpecName = tdsSpecName;
		this.tdsSpecOptionName = tdsSpecOptionName;
		this.configCode = configCode;
		this.state = state;
	}
	
	@Id
	@Column(name = "tds_code")
	private Long id;
	
	@Column(name = "meter_line_code")
	private String meterLineCode;

	@Column(name = "meter_line_identifier")
	private String meterLineIdentifier;

	@Column(name = "meter_line_name")
	private String meterLineName;
	
	@Column(name = "meter_model_code")
	private Integer meterModelCode;

	@Column(name = "meter_model_identifier")
	private String meterModelIdentifier;

	@Column(name = "meter_model_name")
	private String meterModelName;
	
	@Column(name = "tds_number")
	private String tdsNumber;
	
	@Column(name = "tds_state_identifier")
	private TdsStateIdentifier state;

	@Column(name = "tds_spec_code")
	private Long tdsSpecCode;

	@Column(name = "tds_spec_identifier")
	private String tdsSpecIdentifier;

	@Column(name = "tds_spec_name")
	private String tdsSpecName;

	@Column(name = "tds_spec_option_code")
	private Integer tdsSpecOptionCode;

	@Column(name = "tds_spec_option_identifier")
	private String tdsSpecOptionIdentifier;

	@Column(name = "tds_spec_option_name")
	private String tdsSpecOptionName;
	
	@Column(name = "config_code")
	private String configCode;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMeterLineIdentifier() {
		return meterLineIdentifier;
	}

	public void setMeterLineIdentifier(String meterLineIdentifier) {
		this.meterLineIdentifier = meterLineIdentifier;
	}

	public String getMeterLineName() {
		return meterLineName;
	}

	public void setMeterLineName(String meterLineName) {
		this.meterLineName = meterLineName;
	}

	public Integer getMeterModelCode() {
		return meterModelCode;
	}

	public void setMeterModelCode(Integer meterModelCode) {
		this.meterModelCode = meterModelCode;
	}

	public String getMeterModelIdentifier() {
		return meterModelIdentifier;
	}

	public void setMeterModelIdentifier(String meterModelIdentifier) {
		this.meterModelIdentifier = meterModelIdentifier;
	}

	public String getMeterModelName() {
		return meterModelName;
	}

	public void setMeterModelName(String meterModelName) {
		this.meterModelName = meterModelName;
	}

	public String getTdsNumber() {
		return tdsNumber;
	}

	public void setTdsNumber(String tdsNumber) {
		this.tdsNumber = tdsNumber;
	}

	public Long getTdsSpecCode() {
		return tdsSpecCode;
	}

	public void setTdsSpecCode(Long tdsSpecCode) {
		this.tdsSpecCode = tdsSpecCode;
	}

	public String getTdsSpecIdentifier() {
		return tdsSpecIdentifier;
	}

	public void setTdsSpecIdentifier(String tdsSpecIdentifier) {
		this.tdsSpecIdentifier = tdsSpecIdentifier;
	}

	public String getTdsSpecName() {
		return tdsSpecName;
	}

	public void setTdsSpecName(String tdsSpecName) {
		this.tdsSpecName = tdsSpecName;
	}

	public Integer getTdsSpecOptionCode() {
		return tdsSpecOptionCode;
	}

	public void setTdsSpecOptionCode(Integer tdsSpecOptionCode) {
		this.tdsSpecOptionCode = tdsSpecOptionCode;
	}

	public String getTdsSpecOptionIdentifier() {
		return tdsSpecOptionIdentifier;
	}

	public void setTdsSpecOptionIdentifier(String tdsSpecOptionIdentifier) {
		this.tdsSpecOptionIdentifier = tdsSpecOptionIdentifier;
	}

	public String getTdsSpecOptionName() {
		return tdsSpecOptionName;
	}

	public void setTdsSpecOptionName(String tdsSpecOptionName) {
		this.tdsSpecOptionName = tdsSpecOptionName;
	}

	public String getConfigCode() {
		return configCode;
	}

	public void setConfigCode(String configCode) {
		this.configCode = configCode;
	}

	public TdsStateIdentifier getState() {
		return state;
	}

	public void setState(TdsStateIdentifier state) {
		this.state = state;
	}

}