package com.eletraenergy.configSW.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

/**
 *
 * @author Raphael
 */

@MappedSuperclass
public abstract class Model implements Serializable {

	private static final long serialVersionUID = -9107527412541149651L;

	public abstract Object getId();
	
}
