package com.eletraenergy.configSW.exception;

import java.util.List;

/**
 * Exce��o para propagar mensagens de valida��o de regras de neg�cio Como a
 * exce��o herda de RuntimeException, o container faz rollback da transa��o
 * corrente automaticamente.
 * 
 * @author raphael.ferreira
 */
public class ConfigSWException extends RuntimeException {


	private static final long serialVersionUID = 1057944920073266703L;
	private List<String> messages;

	public ConfigSWException(Throwable throwable) {
		super(throwable);
	}

	public ConfigSWException(String message) {
		super(message);
	}

	public ConfigSWException(String message, Exception exception) {
		super(message, exception);
	}

	public ConfigSWException(List<String> messages) {
		this.messages = messages;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
}
