package com.eletraenergy.configSW.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.model.TdsSpecOption;
import com.eletraenergy.configSW.service.TdsSpecOptionService;

public class LazyTdsSpecOptionDataModel extends LazyDataModel<TdsSpecOption>{

	private static final long serialVersionUID = 7650512837220639405L;
	private List<TdsSpecOption> tdsSpecOptions;
	private TdsSpecOptionService tdsSpecOptionService;
	private TdsSpecOption tdsSpecOptionFilter;
	
     
    public LazyTdsSpecOptionDataModel(TdsSpecOptionService tdsSpecOptionService, TdsSpecOption tdsSpecOptionFilter) {
		super();
		this.tdsSpecOptionService = tdsSpecOptionService;
		this.tdsSpecOptionFilter = tdsSpecOptionFilter;
	}

	@Override
    public TdsSpecOption getRowData(String rowKey) {
        for(TdsSpecOption s : tdsSpecOptions) {
            if(s.getId().equals(rowKey))
                return s;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(TdsSpecOption specOption) {
        return specOption.getId();
    }
 
    @Override
    public List<TdsSpecOption> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<TdsSpecOption> data = new ArrayList<TdsSpecOption>();
 
		tdsSpecOptionFilter.setSortField(sortField);
		tdsSpecOptionFilter.setSortOrder(sortOrder);

		data = tdsSpecOptionService.search(tdsSpecOptionFilter, first,
				pageSize);

        //rowCount
        int dataSize = tdsSpecOptionService.amount(tdsSpecOptionFilter);
        this.setRowCount(dataSize);
 
        return data;
    }
}