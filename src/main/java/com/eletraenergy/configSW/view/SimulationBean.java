package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.ColumnModel;
import com.eletraenergy.configSW.model.FilterModel;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.TdsModel;
import com.eletraenergy.configSW.model.TdsSpec;
import com.eletraenergy.configSW.model.TdsSpecOption;
import com.eletraenergy.configSW.model.TdsStateIdentifier;
import com.eletraenergy.configSW.model.ViewTDS;
import com.eletraenergy.configSW.model.ViewTDSSpec;
import com.eletraenergy.configSW.model.ViewTdsAud;
import com.eletraenergy.configSW.service.MeterLineService;
import com.eletraenergy.configSW.service.MeterModelService;
import com.eletraenergy.configSW.service.TdsService;
import com.eletraenergy.configSW.service.TdsSpecService;
import com.eletraenergy.configSW.service.ViewTDSService;
import com.eletraenergy.configSW.service.ViewTDSSpecService;
import com.eletraenergy.configSW.service.ViewTdsAudService;
import com.eletraenergy.configSW.util.ExcelUtils;
import com.eletraenergy.configSW.util.ResourceBundle;

/**
* Use case : Simulation
* 
* @since   : Jan 22, 2017, 23:01:20 AM
* @author  : raphael.ferreira
*/

@Component("simulationBean")
@Scope("view")
public class SimulationBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -1054159219288063324L;

	@Autowired
	private MeterLineService meterLineService;
	@Autowired
	private MeterModelService meterModelService;
	@Autowired
	private ViewTDSSpecService viewTDSSpecService;
	@Autowired
	private ViewTDSService viewTDSService;
	@Autowired
	private TdsSpecService tdsSpecService;
	@Autowired
	private TdsService tdsService;
	@Autowired
	private ViewTdsAudService viewTdsAudService;

	
	MeterLine meterLine;
	MeterModel meterModel;
	String tds;
	private TdsModel selectedTds;
	List<MeterLine> meterLines;
	List<MeterModel> meterModels;
	private TdsStateIdentifier state;
	private List<FilterModel> filters;
	private List<ViewTDS> tdss;
	private List<TdsModel> tdsList;
	private List<ColumnModel> columns;
	
	public void loadInit() {
		if (meterLines == null){
			meterLines = meterLineService.findAll();
			loadFilters();
			search();
		}
	}
	
	public void loadMeterModels() {
		if(meterLine==null){
			meterModels = meterModelService.findAll();
		}else{
			meterModels = meterModelService.findbyLine(meterLine);
		}
		meterModel = null;
		loadFilters();
	}
	
	public void exportToPDF(TdsModel tdsModel) {
		
		ExcelUtils excelUtils = new ExcelUtils(true);
		
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    final ExternalContext externalContext = facesContext.getExternalContext();
	    
	    externalContext.responseReset();
	    externalContext.setResponseContentType("application/octet-stream");
	    externalContext.setResponseHeader("Content-Disposition", "attachment;filename=tds-export.xls");

	    try {
	    	
	    	Tds tds = tdsService.checkIdentifier(tdsModel.getTds());
	    	List<TdsSpecOption> listOfTdsSpecOption = tds.getTemplate().getTdsSpecOptions();
	    	List<TdsSpec> listOfTdsSpec = tdsSpecService.findAll();
	    	ViewTdsAud viewTdsAud = viewTdsAudService.findById(tds.getId());
	    	
			HSSFWorkbook exportTdsToExcel = excelUtils.exportTdsToExcel(listOfTdsSpec, listOfTdsSpecOption, tds, viewTdsAud);
			
			exportTdsToExcel.write(
					((HttpServletResponse) externalContext.getResponse()).getOutputStream());
			
			facesContext.responseComplete();
			
		} catch (Exception e) {
			
			externalContext.responseReset();
			errorMsg(ResourceBundle.getMessage("tdsExportToExcelError"), null);
		}
	}

	public void loadFilters() {
		List<ViewTDSSpec> listTDSSpec = null;
		if (meterLine == null && meterModel == null){
			listTDSSpec = viewTDSSpecService.findAll();
		}else{
			if (meterModel == null) {
				listTDSSpec = viewTDSSpecService.findByLine(meterLine);	
			} else {
				listTDSSpec = viewTDSSpecService.findByLineAndModel(meterLine, meterModel);
			}
		}
		
		filters = new ArrayList<FilterModel>();
		Long id = null;
		List<String> listOptions = null;
		String option = null;
		for (ViewTDSSpec viewTDSSpec : listTDSSpec) {
			if (id == null || !id.equals(viewTDSSpec.getId())){
				if (listOptions != null){
					FilterModel filtroModel = new FilterModel(option, null, listOptions, null);
					filters.add(filtroModel);
				}
				id = viewTDSSpec.getId();
				option = viewTDSSpec.getName();
				listOptions = new ArrayList<String>();
			}
			listOptions.add(viewTDSSpec.getOptionName());
		}
		//The last
		if (listOptions != null){
			FilterModel filtroModel = new FilterModel(option, null, listOptions, null);
			filters.add(filtroModel);
		}
	}

	//Limpa filtros de tds
	public void clear() {
		state = null;
		meterModel = null;
		meterLine = null;
		tds = null;
		loadFilters();
	}
	
	public void search() {
		tdsList = new ArrayList<TdsModel>();
		List<FilterModel> validFilters = new ArrayList<FilterModel>();
		if (tds == null || tds.isEmpty() ){
			for (FilterModel filterModel : filters) {
				if(filterModel.getValue() != null && !filterModel.getValue().isEmpty()){
					validFilters.add(filterModel);
				}
			}
		}
		
		tdss = viewTDSService.findConfig(meterLine, meterModel, tds, validFilters, state);
		
		List<String> specNames = new ArrayList<String>();

		//Descubrir todos os specNames
		for (ViewTDS tds : tdss) {
			boolean isFilter = false;
			for (FilterModel validFilter : validFilters) {
				if(validFilter.getHeader().equals(tds.getTdsSpecName())){
					isFilter = true;
				}
			}
			if (!isFilter && !specNames.contains(tds.getTdsSpecName())) {
				specNames.add(tds.getTdsSpecName());
			}
		}
		
		//Montar as colunas
		columns = new ArrayList<ColumnModel>();
		for (String spec : specNames) {
			columns.add(new ColumnModel(spec, spec));
		}
		

		//Criar os TdsModel
		String tdsNumber = null; 
		TdsModel tdsModel = null;
		for (ViewTDS tds : tdss) {
			if(tdsNumber == null || !tdsNumber.equals(tds.getTdsNumber())){
				if(tdsModel != null){
					tdsList.add(tdsModel);
				}
				tdsNumber = tds.getTdsNumber();
				tdsModel = new TdsModel(tds.getState(), tdsNumber, tds.getMeterLineName(), tds.getMeterModelName(), tds.getConfigCode(), new ArrayList<String>());
//				tdsModel.setTds(tdsNumber);
//				tdsModel.setConfigCode(tds.getConfigCode());
				tdsModel.setValues(new ArrayList<String>());
				//Inicializar todos os valores
				for (int i = 0; i < columns.size(); i++) {
					tdsModel.getValues().add(ResourceBundle.getMessage("notDefined"));
				}
			}
			int i = 0;
			for (ColumnModel column : columns) {
				if (column.getHeader().equals(tds.getTdsSpecName())){
					tdsModel.getValues().set(i, tds.getTdsSpecOptionName());
				}
				i = i + 1;
			}
		}
				
		//The Last
		if (tdsModel != null){
			tdsList.add(tdsModel);
		}
	}

	public MeterLine getMeterLine() {
		return meterLine;
	}

	public void setMeterLine(MeterLine meterLine) {
		this.meterLine = meterLine;
	}

	public MeterModel getMeterModel() {
		return meterModel;
	}

	public void setMeterModel(MeterModel meterModel) {
		this.meterModel = meterModel;
	}

	public String getTds() {
		return tds;
	}

	public void setTds(String tds) {
		this.tds = tds;
	}

	public List<MeterLine> getMeterLines() {
		return meterLines;
	}

	public void setMeterLines(List<MeterLine> meterLines) {
		this.meterLines = meterLines;
	}

	public List<MeterModel> getMeterModels() {
		return meterModels;
	}

	public void setMeterModels(List<MeterModel> meterModels) {
		this.meterModels = meterModels;
	}

 	public List<FilterModel> getFilters() {
		return filters;
	}

	public void setFilters(List<FilterModel> filters) {
		this.filters = filters;
	}

	public List<TdsModel> getTdsList() {
		return tdsList;
	}

	public void setTdsList(List<TdsModel> tdsList) {
		this.tdsList = tdsList;
	}

	public List<ColumnModel> getColumns() {
		return columns;
	}

	public void setColumns(List<ColumnModel> columns) {
		this.columns = columns;
	}

	public TdsModel getSelectedTds() {
		return selectedTds;
	}

	public void setSelectedTds(TdsModel selectedTds) {
		this.selectedTds = selectedTds;
	}

	public Boolean getSelect() {
		String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
		
		return viewId.contains("simulation");
	}

	public String getTitle() {
		if (getSelect()){
			return ResourceBundle.getMessage("simulation");
		}else{
			return ResourceBundle.getMessage("tdsAdvanced");
		}
	}

	public TdsStateIdentifier[] getStates() {
		return TdsStateIdentifier.values();
	}

	public TdsStateIdentifier getState() {
		return state;
	}

	public void setState(TdsStateIdentifier state) {
		this.state = state;
	}
}