package com.eletraenergy.configSW.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import com.eletraenergy.configSW.model.SpecOption;
import com.eletraenergy.configSW.service.SpecOptionService;

public class LazySpecOptionDataModel extends LazyDataModel<SpecOption>{

	private static final long serialVersionUID = 3941285463152226908L;
	private List<SpecOption> specOptions;
	private SpecOptionService specOptionService;
	private SpecOption specOptionFilter;
	
     
    public LazySpecOptionDataModel(SpecOptionService specOptionService, SpecOption specOptionFilter) {
		super();
		this.specOptionService = specOptionService;
		this.specOptionFilter = specOptionFilter;
	}

	@Override
    public SpecOption getRowData(String rowKey) {
        for(SpecOption s : specOptions) {
            if(s.getId().equals(rowKey))
                return s;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(SpecOption specOption) {
        return specOption.getId();
    }
 
    @Override
    public List<SpecOption> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<SpecOption> data = new ArrayList<SpecOption>();
 
		specOptionFilter.setSortField(sortField);
		specOptionFilter.setSortOrder(sortOrder);

		data = specOptionService.search(specOptionFilter, first,
				pageSize);

        //rowCount
        int dataSize = specOptionService.amount(specOptionFilter);
        this.setRowCount(dataSize);
 
        return data;
    }
}