package com.eletraenergy.configSW.view;

import com.eletraenergy.configSW.model.*;
import com.eletraenergy.configSW.service.*;
import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component("searchCalibrationLaboratoryBean")
@SessionScoped
public class SearchCalibrationLaboratoryBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -8102482899401242347L;

	public static Logger logger = Logger.getLogger(SearchCalibrationLaboratoryBean.class);

	@Autowired
	private CalibrationLaboratoryService calibrationLaboratoryService;

	private LazyDataModel<CalibrationLaboratory> calibrationLaboratories;
	private CalibrationLaboratory calibrationLaboratoryFilter;

	private CalibrationLaboratory measuringInstrument;


	public void loadInit() {

		//calibrationLaboratories = calibrationLaboratoryService.findAll();

		String msg = getSessionAtribute(Constantes.MSG);
		if (msg !=null){
			infoMsg(msg, null);
			clearSessionMsgAtribute();
		}

		if (calibrationLaboratoryFilter == null) {
			calibrationLaboratoryFilter = new CalibrationLaboratory();
			search();
		}
	}

	public String register() {
		return "createCalibrationLaboratory?faces-redirect=true";
	}

	public void remove(CalibrationLaboratory calibrationLaboratory) {

		//Identificando a Transa��o
		addSession(Constantes.CODE_TRANSACTION, TransactionReview.DELETE_MEASURING_INSTRUMENT.getCode());
		addSession(Constantes.TITLE_TRANSACTION, TransactionReview.DELETE_MEASURING_INSTRUMENT.getDescription());


		calibrationLaboratoryService.update(calibrationLaboratory);
		calibrationLaboratoryService.remove(calibrationLaboratory);

		search();

		infoMsg(ResourceBundle.getMessage("measuringInstrumentDelete"), null);
	}



	public void search() {
		calibrationLaboratories = new LazyCalibrationLaboratoryDataModel(calibrationLaboratoryService, calibrationLaboratoryFilter);
	}


	public CalibrationLaboratoryService getCalibrationLaboratoryService() {
		return calibrationLaboratoryService;
	}

	public void setCalibrationLaboratoryService(CalibrationLaboratoryService calibrationLaboratoryService) {
		this.calibrationLaboratoryService = calibrationLaboratoryService;
	}

	public LazyDataModel<CalibrationLaboratory> getCalibrationLaboratories() {
		return calibrationLaboratories;
	}

	public void setCalibrationLaboratories(LazyDataModel<CalibrationLaboratory> calibrationLaboratories) {
		this.calibrationLaboratories = calibrationLaboratories;
	}

	public CalibrationLaboratory getCalibrationLaboratoryFilter() {
		return calibrationLaboratoryFilter;
	}

	public void setCalibrationLaboratoryFilter(CalibrationLaboratory calibrationLaboratoryFilter) {
		this.calibrationLaboratoryFilter = calibrationLaboratoryFilter;
	}
}