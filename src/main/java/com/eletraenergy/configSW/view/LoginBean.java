package com.eletraenergy.configSW.view;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.service.AuthenticationService;

@SuppressWarnings("serial")
@Component("LoginBean")
@Scope("request")
public class LoginBean extends ManagerBean implements Serializable {

	@Autowired
	private AuthenticationService authenticationService;
	
	private String login;
	private String password;
	private String msg;
	
	public void initialLoad() {
	}
	
	public String authenticate() {
		String retorno = "home?faces-redirect=true";
	    
	    String loginMessage = authenticationService.login(login, password);
	    if (!loginMessage.equalsIgnoreCase("ok")) {
	    	this.login = new String();
	    	this.password = new String();
	    	setMsg(loginMessage);
	    	errorMsg(loginMessage, null);
	    	return "invalidLogin";
	    }
	    
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		request.getSession().setAttribute("login", true);
		request.getSession().setAttribute("user", login);
		request.getSession().setAttribute("lastAccess", authenticationService.getLastAccess());
		
		return retorno;
	}
	
	public boolean anyGranted(String value) {
		String[] listGrants = value.split(",");
		if (authenticationService.getUserLogon()!= null) {
			for (GrantedAuthority permissao : authenticationService.getUserLogon().getAuthorities()) {
				for (String grant : listGrants) {
					if (permissao.getAuthority().equals(grant))
						return true;				
				}
			}
		}
		return false;
	}
	
	public String logout() {
		authenticationService.logout();
		return "/login?faces-redirect=true";
	}

	public String getUserName() {
		return authenticationService.getUserLogon().getName();
	}

	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
		getFlashScope().put("msg", msg);
	}
}
