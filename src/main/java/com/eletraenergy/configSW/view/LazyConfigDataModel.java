package com.eletraenergy.configSW.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.service.ConfigService;

public class LazyConfigDataModel extends LazyDataModel<Config>{

	private static final long serialVersionUID = 8813749239923104005L;
	private List<Config> config;
	private ConfigService configService;
	private Config configFilter;
	
    public LazyConfigDataModel(ConfigService configService, Config configFilter) {
		super();
		this.configService = configService;
		this.configFilter = configFilter;
	}

	@Override
    public Config getRowData(String rowKey) {
        for(Config s : config) {
            if(s.getId().equals(rowKey))
                return s;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(Config config) {
        return config.getId();
    }
 
    @Override
    public List<Config> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<Config> data = new ArrayList<Config>();
 
		configFilter.setSortField(sortField);
		configFilter.setSortOrder(sortOrder);

		data = configService.search(configFilter, first,
				pageSize);

        //rowCount
        int dataSize = configService.amount(configFilter);
        this.setRowCount(dataSize);
 
        return data;
    }
}