package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.Functionality;
import com.eletraenergy.configSW.model.TransactionReview;
import com.eletraenergy.configSW.model.User;
import com.eletraenergy.configSW.service.UserService;
import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.ResourceBundle;

/**
* Use case : Create User
* 
* @since   : Jan 26, 2017, 14:23:20 PM
* @author  : raphael.ferreira
*/

@Component("createUserBean")
@Scope("session")
public class CreateUserBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -821466037704377491L;

	public static Logger logger = Logger.getLogger(CreateUserBean.class);

	@Autowired
	private UserService userService;
	
	private User user;
	private String operation;
	private FunctionalityDataModel functionalities;
	private Functionality[] functionalitiesSelected;
	
	public void loadInit() {
		//Se for um novo e ainda tiver nulo criar objeto.
		if (operation.equals("NEW") && user == null) {
			user = new User();
		}else{
			if(functionalitiesSelected == null && user.getFunctionalities() !=null){
				int i = 0;
				functionalitiesSelected = new Functionality[user.getFunctionalities().size()];
				for (Functionality func : user.getFunctionalities()) {
					functionalitiesSelected[i++] = func;
				}
			}
		}
		if (getFunctionalities() == null){
			setFunctionalities(new FunctionalityDataModel(userService.FindAllFunctionalities()));
		}
	}
	
	private void prepareFunctionalities() {
		if (functionalitiesSelected.length > 0) {
			List<Functionality> funcs  = new ArrayList<Functionality>();
			for (int i = 0; i < functionalitiesSelected.length; i++) {
				funcs.add(functionalitiesSelected[i]);
			}
			user.setFunctionalities(funcs);
		}
	}
	public String save() {
		String ret = "searchUsers?faces-redirect=true";
		
		prepareFunctionalities();
		if (operation.equals("NEW")){
			//Identificando a Transa��o
			addSession(Constantes.CODE_TRANSACTION, TransactionReview.CREATE_USER.getCode());
			addSession(Constantes.TITLE_TRANSACTION, TransactionReview.CREATE_USER.getDescription());
			
			user.setActive("T");
			user = userService.save(user);
		}else{
			//Identificando a Transa��o
			addSession(Constantes.CODE_TRANSACTION, TransactionReview.ALTER_USER.getCode());
			addSession(Constantes.TITLE_TRANSACTION, TransactionReview.ALTER_USER.getDescription());

			user = userService.update(user);
		}
		cleanObjects();
		if (operation.equals("NEW")) {
			infoMsg(ResourceBundle.getMessage("userCreate"), null);
			ret=null;
		}else{
			setSessionMsgAtribute(ResourceBundle.getMessage("userUpdate"));
		}
		
		return ret;
    }

	public String back() {
		cleanObjects();
		return "searchUsers?faces-redirect=true";
	}

	public void cleanObjects(){
		user = null;
		functionalities=null;
		functionalitiesSelected=null;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Functionality[] getFunctionalitiesSelected() {
		return functionalitiesSelected;
	}

	public void setFunctionalitiesSelected(Functionality[] functionalitiesSelected) {
		this.functionalitiesSelected = functionalitiesSelected;
	}

	public FunctionalityDataModel getFuncionalidades() {
		return getFunctionalities();
	}

	public void setFuncionalidades(FunctionalityDataModel funcionalidades) {
		this.setFunctionalities(funcionalidades);
	}

	public FunctionalityDataModel getFunctionalities() {
		return functionalities;
	}

	public void setFunctionalities(FunctionalityDataModel functionalities) {
		this.functionalities = functionalities;
	}
}
