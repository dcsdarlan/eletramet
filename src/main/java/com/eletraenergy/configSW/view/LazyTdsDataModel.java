package com.eletraenergy.configSW.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.service.TdsService;

public class LazyTdsDataModel extends LazyDataModel<Tds>{

	private static final long serialVersionUID = -2625265806840013215L;
	private List<Tds> tds;
	private TdsService tdsService;
	private Tds tdsFilter;
	
    public LazyTdsDataModel(TdsService tdsService, Tds tdsFilter) {
		super();
		this.tdsService = tdsService;
		this.tdsFilter = tdsFilter;
	}

	@Override
    public Tds getRowData(String rowKey) {
        for(Tds s : tds) {
            if(s.getId().equals(rowKey))
                return s;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(Tds tds) {
        return tds.getId();
    }
 
    @Override
    public List<Tds> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<Tds> data = new ArrayList<Tds>();
 
		tdsFilter.setSortField(sortField);
		tdsFilter.setSortOrder(sortOrder);

		data = tdsService.search(tdsFilter, first,
				pageSize);

        //rowCount
        int dataSize = tdsService.amount(tdsFilter);
        this.setRowCount(dataSize);
 
        return data;
    }
}