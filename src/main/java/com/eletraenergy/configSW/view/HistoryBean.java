package com.eletraenergy.configSW.view;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.ConfigSimulation;
import com.eletraenergy.configSW.service.ConfigSimulationService;

/**
 * Use case : History SImulation
 * 
 * @since : Fev 13, 2017, 14:23:20 PM
 * @author : raphael.ferreira
 */

@Component("historyBean")
@Scope("session")
public class HistoryBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 5322229497882948457L;

	public static Logger logger = Logger.getLogger(HistoryBean.class);

	@Autowired
	private ConfigSimulationService configService;
	
	private String json;

	private LazyDataModel<ConfigSimulation> simulations;
	public void loadInit() {
		if(simulations==null){
			search();
		}
	}

	public void search() {
		simulations = new LazyConfigSimulationDataModel(configService, new ConfigSimulation());
	}

	public LazyDataModel<ConfigSimulation> getSimulations() {
		return simulations;
	}

	public void setSimulations(LazyDataModel<ConfigSimulation> simulations) {
		this.simulations = simulations;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

}
