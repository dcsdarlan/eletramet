package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.List;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.MeasuringInstrumentPlaceUse;
import com.eletraenergy.configSW.model.TransactionReview;
import com.eletraenergy.configSW.service.MeasuringInstrumentPlaceUseService;
import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.ResourceBundle;

@Component("createMeasuringInstrumentPlaceUseBean")
@Scope("view")
public class CreateMeasuringInstrumentPlaceUseBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -6834442723163022371L;

	public static Logger logger = Logger.getLogger(CreateMeasuringInstrumentPlaceUseBean.class);

	@Autowired
	private MeasuringInstrumentPlaceUseService measuringInstrumentPlaceUseService;
	
	private List<MeasuringInstrumentPlaceUse> measuringInstrumentPlaceUses;
	private MeasuringInstrumentPlaceUse measuringInstrumentPlaceUseFilter;
	
	public void loadInit() {
		if (measuringInstrumentPlaceUseFilter == null){
			measuringInstrumentPlaceUseFilter = new MeasuringInstrumentPlaceUse();
			measuringInstrumentPlaceUseFilter.setSortField("name");
			measuringInstrumentPlaceUseFilter.setSortOrder(SortOrder.ASCENDING);
			
			search();	
		}
	}

	public void addMeasuringInstrumentPlaceUse(ActionEvent actionEvent) {
		MeasuringInstrumentPlaceUse measuringInstrumentPlaceUse = new MeasuringInstrumentPlaceUse();
		measuringInstrumentPlaceUses.add(0,measuringInstrumentPlaceUse);
    }

	public void save(ActionEvent actionEvent) {
		if (measuringInstrumentPlaceUses.size()> 0){
			for (MeasuringInstrumentPlaceUse measuringInstrumentPlaceUse : measuringInstrumentPlaceUses) {
				//Esse teste eh no caso do usuario clicar em novo e salvar sem preencher nada. Essas linhas ser�o ignoradas.
				if (measuringInstrumentPlaceUse.getName() != null && !measuringInstrumentPlaceUse.getName().isEmpty()){
					if (measuringInstrumentPlaceUse.getId()==null){
						//Identificando a Transa��o
						//addSession(Constantes.CODE_TRANSACTION, TransactionReview.CREATE_LINE.getCode());
						//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.CREATE_LINE.getDescription());
						
						measuringInstrumentPlaceUseService.save(measuringInstrumentPlaceUse);
					}else{
						//Identificando a Transa��o
						//addSession(Constantes.CODE_TRANSACTION, TransactionReview.ALTER_LINE.getCode());
						//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.ALTER_LINE.getDescription());

						measuringInstrumentPlaceUseService.update(measuringInstrumentPlaceUse);
					}
				}
			}
			search();
			infoMsg(ResourceBundle.getMessage("measuringInstrumentPlaceUseSave"), null);
		}
    }

	public void checkName(MeasuringInstrumentPlaceUse measuringInstrumentPlaceUse) {
		MeasuringInstrumentPlaceUse checkName = measuringInstrumentPlaceUseService.checkName(measuringInstrumentPlaceUse.getName());
		if (checkName != null){
	      if ((measuringInstrumentPlaceUse.getId()!=null && !checkName.getId().equals(measuringInstrumentPlaceUse.getId())) || (measuringInstrumentPlaceUse.getId()==null && checkName != null)){
	    	  errorMsg(ResourceBundle.getMessage("checkName") + System.lineSeparator() + "[" +  measuringInstrumentPlaceUse.getName() + "]" , null);
	    	  measuringInstrumentPlaceUse.setName(null);
	      }
		}
	}
	
	public void search(){	
		measuringInstrumentPlaceUses = measuringInstrumentPlaceUseService.search(measuringInstrumentPlaceUseFilter, null, null);
	}

	public void remove(MeasuringInstrumentPlaceUse measuringInstrumentPlaceUseSelected){
		//Identificando a Transa��o
		//addSession(Constantes.CODE_TRANSACTION, TransactionReview.DELETE_LINE.getCode());
		//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.DELETE_LINE.getDescription());

		measuringInstrumentPlaceUseService.remove(measuringInstrumentPlaceUseSelected);
		measuringInstrumentPlaceUseFilter = new MeasuringInstrumentPlaceUse();
		measuringInstrumentPlaceUseFilter.setSortField("name");
		measuringInstrumentPlaceUseFilter.setSortOrder(SortOrder.ASCENDING);
		search();
	    infoMsg(ResourceBundle.getMessage("measuringInstrumentPlaceUseDelete"),"");
	}

	public List<MeasuringInstrumentPlaceUse> getMeasuringInstrumentPlaceUses() {
		return measuringInstrumentPlaceUses;
	}

	public MeasuringInstrumentPlaceUse getMeasuringInstrumentPlaceUseFilter() {
		return measuringInstrumentPlaceUseFilter;
	}

	public void setMeasuringInstrumentPlaceUses(List<MeasuringInstrumentPlaceUse> measuringInstrumentPlaceUses) {
		this.measuringInstrumentPlaceUses = measuringInstrumentPlaceUses;
	}

	public void setMeasuringInstrumentPlaceUseFilter(MeasuringInstrumentPlaceUse measuringInstrumentPlaceUseFilter) {
		this.measuringInstrumentPlaceUseFilter = measuringInstrumentPlaceUseFilter;
	}

}
