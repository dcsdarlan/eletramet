package com.eletraenergy.configSW.view;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.TransactionReview;
import com.eletraenergy.configSW.model.User;
import com.eletraenergy.configSW.service.UserService;
import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.ResourceBundle;

/**
 * Use case : Search Users
 * 
 * @since : Jan 26, 2017, 14:23:20 PM
 * @author : raphael.ferreira
 */

@Component("searchUsersBean")
@Scope("view")
public class SearchUsersBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 9214653707763104608L;

	public static Logger logger = Logger.getLogger(SearchUsersBean.class);

	@Autowired
	private UserService userService;

	private LazyDataModel<User> users;
	private User userFilter;
	
	public void loadInit() {
		String msg = getSessionAtribute(Constantes.MSG);
		if (msg !=null){
			infoMsg(msg, null);
			clearSessionMsgAtribute();
		}
		
		if (userFilter == null) {
			userFilter = new User();
			search();
		}
	}

	public String register() {
		return "createUser?faces-redirect=true";
	}

	public void remove(User user) {
		//Identificando a Transa��o
		addSession(Constantes.CODE_TRANSACTION, TransactionReview.DELETE_USER.getCode());
		addSession(Constantes.TITLE_TRANSACTION, TransactionReview.DELETE_USER.getDescription());

		user.setFunctionalities(null);
		userService.remove(user);
		userFilter = null;
		search();
		infoMsg(ResourceBundle.getMessage("userRemovePhrase"), null);
	}

	public void search() {
		users = new LazyUserDataModel(userService, userFilter);
	}

	public void setUsers(LazyDataModel<User> users) {
		this.users = users;
	}

	public User getUserFilter() {
		return userFilter;
	}

	public void setUserFilter(User userFilter) {
		this.userFilter = userFilter;
	}

	public LazyDataModel<User> getUsers() {
		return users;
	}
}
