package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.ConfigCommand;
import com.eletraenergy.configSW.model.ConfigModel;
import com.eletraenergy.configSW.model.ConfigTechSpec;
import com.eletraenergy.configSW.model.ItemOptionSpec;
import com.eletraenergy.configSW.model.ItemSpec;
import com.eletraenergy.configSW.model.ItemSpecCategory;
import com.eletraenergy.configSW.model.ManufactStep2;
import com.eletraenergy.configSW.model.MeterCommand;
import com.eletraenergy.configSW.model.OptionType;
import com.eletraenergy.configSW.util.ResourceBundle;


/**
* Use case : Config Command
* 
* @since   : Fev 10, 2017, 19:01:20
* @author  : raphael.ferreira
*/

@Component("configCommandBean")
@Scope("session")
public class ConfigCommandBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -2632711068730591195L;
	private List<MeterCommand> meterCommands;
	private ConfigTechSpec configTechSpec;
	private List<ConfigModel> configModel;


	public void loadInit() {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		Map<String, Object> sessionMap = externalContext.getSessionMap();
		ConfigCommand configCommand = (ConfigCommand) sessionMap.get("configCommand"); 
		configTechSpec = (ConfigTechSpec) sessionMap.get("configTechSpec"); 
		prepareConfigModel();
		meterCommands = new ArrayList<MeterCommand>();
		for (ManufactStep2 manufactStep : configCommand.getListOfManufactStep()) {
			String manufactStepName = manufactStep.getManufactStepName();
			if (manufactStep.getListOfMeterCommand()==null || manufactStep.getListOfMeterCommand().isEmpty()){
				MeterCommand meterCommand = new MeterCommand();
				meterCommand.setCmdTitle(ResourceBundle.getMessage("withoutCommands"));
				meterCommand.setStage(manufactStepName);
				meterCommands.add(meterCommand);
			}else{
				for (MeterCommand meterCommand : manufactStep.getListOfMeterCommand()) {
					meterCommand.setStage(manufactStepName);
					meterCommands.add(meterCommand);
				}
			}
		}
	}
	
	private void prepareConfigModel() {
		configModel = new ArrayList<ConfigModel>();
		for (ItemSpecCategory item : configTechSpec.getListOfItemSpecCategory()) {
			String nm_TypeItemSpec = null;
		    for (ItemSpec itemSpec : item.getListOfItemSpec()) {
		    	OptionType optionType = OptionType.Read;;
		    	List<String> options = new ArrayList<String>();
		    	String validation = null;
		    	String type = null;
		    	String valMaxDigInt = null;
		    	String valMaxDigDec = null;
		    	String valMaxDig = null;
		    	String maxSel = null;
		    	String valMin = null;
		    	String valMax = null;
		    	boolean isRequired = false;
		    	String value = "";
		    	if (itemSpec.getItemSpecValue()==null || itemSpec.getItemSpecValue().isEmpty()){
		    		if(itemSpec.getListOfItemOptionSpec() != null && !itemSpec.getListOfItemOptionSpec().isEmpty()){
				    	for (ItemOptionSpec i : itemSpec.getListOfItemOptionSpec()) {
							value = value + i.getNM_ItemOptionSpec() + ", ";
						}
				    	value = value.substring(0, value.length()-2);
		    		}
		    	}else{
			    	value = itemSpec.getItemSpecValue();
		    	}
		    		
    			
		    	String category = "";
		    	if (nm_TypeItemSpec == null || !nm_TypeItemSpec.equals(item.getNM_TypeItemSpec())) {
			    	nm_TypeItemSpec = item.getNM_TypeItemSpec();
			    	category = nm_TypeItemSpec;
				}else{
					category = "";
				}
				configModel.add(new ConfigModel(category, itemSpec.getNM_ItemSpec(), options, value, optionType, validation, type, valMaxDigInt, valMaxDigDec, valMaxDig, maxSel, valMin, valMax, isRequired));
			}
			
		}
	}	
	public String back(){
		return "configTechSpec";
	}

	public String export(){
		return "configTechSpec";
	}

	public String exportCommands(){
		return null;
	}
	
	public List<MeterCommand> getMeterCommands() {
		return meterCommands;
	}

	public void setMeterCommands(List<MeterCommand> meterCommands) {
		this.meterCommands = meterCommands;
	}

	public List<ConfigModel> getConfigModel() {
		return configModel;
	}

	public void setConfigModel(List<ConfigModel> configModel) {
		this.configModel = configModel;
	}
	

}