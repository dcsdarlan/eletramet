package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.eletraenergy.configSW.model.*;
import com.eletraenergy.configSW.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.ResourceBundle;

@Component("createMeasuringInstrumentBean")
@Scope("session")
public class CreateMeasuringInstrumentBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 7473427567463939699L;

	public static Logger logger = Logger.getLogger(CreateMeasuringInstrumentBean.class);

	@Autowired
	private MeasuringInstrumentService measuringInstrumentService;

	@Autowired
	private CalibrationLaboratoryService calibrationLaboratoryService;
	
	@Autowired
	private MeasuringInstrumentTypeService measuringInstrumentTypeService;

	@Autowired
	private MeasuringInstrumentManufacturerService measuringInstrumentManufacturerService;

	@Autowired
	private MeasuringInstrumentModelService measuringInstrumentModelService;
	
	@Autowired
	private MeasuringInstrumentPlaceUseService measuringInstrumentPlaceUseService;

	@Autowired
	private MeasuringInstrumentDepartmentService measuringInstrumentDepartmentService;

	@Autowired
	private MeasuringInstrumentStandardService measuringInstrumentStandardService;
	
	private MeasuringInstrument measuringInstrument;
	private String operation;
	
	private List<MeasuringInstrumentType> measuringInstrumentTypes;
	private List<MeasuringInstrumentManufacturer> measuringInstrumentManufacturers;
	private List<MeasuringInstrumentModel> measuringInstrumentModels;
	private List<MeasuringInstrumentPlaceUse> measuringInstrumentPlacesUse;
	private List<MeasuringInstrumentDepartment> measuringInstrumentDepartments;
	private List<MeasuringInstrumentStandard> measuringInstrumentStandards;
	private List<MeasuringInstrumentStandard> measuringInstrumentStandardsSelected;
	private List<CalibrationLaboratory> calibrationLaboratories;
	
	private MeasuringInstrumentManufacturer measuringInstrumentManufacturer;
	
	private Boolean validateIdentifier;
	private List<FilterModel> filters;
	private String currentHashCode;
	private MeasuringInstrumentStateIdentifier selectedMeasuringInstrumentStateIdentifier;
	private MeasuringInstrumentObsoleteIdentifier selectedMeasuringInstrumentObsoleteIdentifier;
	private MeasuringInstrumentDamagedIdentifier selectedMeasuringInstrumentDamagedIdentifier;
	
	public void loadInit() {
		
		measuringInstrumentTypes = measuringInstrumentTypeService.findAll();
		measuringInstrumentManufacturers = measuringInstrumentManufacturerService.findAll();
		measuringInstrumentPlacesUse = measuringInstrumentPlaceUseService.findAll();
		measuringInstrumentStandards = measuringInstrumentStandardService.findAll();
		measuringInstrumentDepartments = measuringInstrumentDepartmentService.findAll();
		calibrationLaboratories = calibrationLaboratoryService.findAll();
		
		if (operation.equals("NEW") && measuringInstrument == null) {
				
			cleanObjects();
			
			measuringInstrument = new MeasuringInstrument();
			
			selectedMeasuringInstrumentStateIdentifier = MeasuringInstrumentStateIdentifier.registered;
			measuringInstrument.setMeasuringInstrumentStateIdentifier(selectedMeasuringInstrumentStateIdentifier);
			
			selectedMeasuringInstrumentObsoleteIdentifier = MeasuringInstrumentObsoleteIdentifier.no;
			measuringInstrument.setMeasuringInstrumentObsoleteIdentifier(selectedMeasuringInstrumentObsoleteIdentifier);
			
			selectedMeasuringInstrumentDamagedIdentifier = MeasuringInstrumentDamagedIdentifier.no;
			measuringInstrument.setMeasuringInstrumentDamagedIdentifier(selectedMeasuringInstrumentDamagedIdentifier);
			
		}else{
			
			if (operation.equals("UPDATE") && measuringInstrument !=null){
				
				if (measuringInstrumentManufacturer==null) {
					measuringInstrumentManufacturer = measuringInstrument.getMeasuringInstrumentModel().getMeasuringInstrumentManufacturer();
					measuringInstrumentModels = measuringInstrumentModelService.findbyManufacturer(measuringInstrumentManufacturer);
				}
				
				if (measuringInstrumentStandardsSelected==null) {
					measuringInstrumentStandardsSelected = new ArrayList<MeasuringInstrumentStandard>(measuringInstrument.getMeasuringInstrumentStandards());
				}
					
				selectedMeasuringInstrumentStateIdentifier = measuringInstrument.getMeasuringInstrumentStateIdentifier();
				selectedMeasuringInstrumentObsoleteIdentifier = measuringInstrument.getMeasuringInstrumentObsoleteIdentifier();
				selectedMeasuringInstrumentDamagedIdentifier = measuringInstrument.getMeasuringInstrumentDamagedIdentifier();
			}
		}
	}
	
	private Date getNextCalibrationDate() {
		
		Date nextCalibrationDate = null;
		
		Calendar calendar = Calendar.getInstance(); 
		calendar.setTime((Date)measuringInstrument.getLastCalibrationDate().clone()); 
		calendar.add(Calendar.DATE, measuringInstrument.getCalibrationFrequencyWeeks() * 7);
		nextCalibrationDate = calendar.getTime();
		
		return nextCalibrationDate;
	}
	
	public String save() throws Exception {
		
		String ret = "searchMeasuringInstrument?faces-redirect=true";
		
		measuringInstrument.setMeasuringInstrumentObsoleteIdentifier(selectedMeasuringInstrumentObsoleteIdentifier);
		measuringInstrument.setMeasuringInstrumentDamagedIdentifier(selectedMeasuringInstrumentDamagedIdentifier);
		measuringInstrument.setMeasuringInstrumentStandards(measuringInstrumentStandardsSelected);
		measuringInstrument.setNextCalibrationDate(getNextCalibrationDate());
		
		if (operation.equals("NEW")){
			
			//Identificando a Transa��o
			addSession(Constantes.CODE_TRANSACTION, TransactionReview.CREATE_MEASURING_INSTRUMENT.getCode());
			addSession(Constantes.TITLE_TRANSACTION, TransactionReview.CREATE_MEASURING_INSTRUMENT.getDescription());
			
			measuringInstrument.setMeasuringInstrumentStateIdentifier(selectedMeasuringInstrumentStateIdentifier);
			measuringInstrument.setMeasuringInstrumentStateTimestamp(new Date(System.currentTimeMillis()));
			measuringInstrument = measuringInstrumentService.save(measuringInstrument);
			
		}else{

			//Identificando a Transa��o
			addSession(Constantes.CODE_TRANSACTION, TransactionReview.ALTER_MEASURING_INSTRUMENT.getCode());
			addSession(Constantes.TITLE_TRANSACTION, TransactionReview.ALTER_MEASURING_INSTRUMENT.getDescription());
			
			if (!selectedMeasuringInstrumentStateIdentifier.equals(measuringInstrument.getMeasuringInstrumentStateIdentifier())) {
				measuringInstrument.setMeasuringInstrumentStateIdentifier(selectedMeasuringInstrumentStateIdentifier);
				measuringInstrument.setMeasuringInstrumentStateTimestamp(new Date(System.currentTimeMillis()));
			}
			
			measuringInstrument = measuringInstrumentService.update(measuringInstrument);
		}
		
		cleanObjects();
		
		if (operation.equals("NEW")) {
			infoMsg(ResourceBundle.getMessage("measuringInstrumentSave"), null);
			ret = null;
		}else{
			setSessionMsgAtribute(ResourceBundle.getMessage("measuringInstrumentUpdate"));
		}
		return ret;
    }
	
	public void loadMeasuringInstrumentModels() {
		measuringInstrumentModels = measuringInstrumentModelService.findbyManufacturer(measuringInstrumentManufacturer);
	}
	
	public void cleanObjects(){
		measuringInstrument = null;
		measuringInstrumentManufacturer = null;
		selectedMeasuringInstrumentStateIdentifier = null;
		measuringInstrumentStandardsSelected = null;
		selectedMeasuringInstrumentObsoleteIdentifier = null;
		selectedMeasuringInstrumentDamagedIdentifier = null;
	}

	public String back() {
		cleanObjects();
		return "searchMeasuringInstrument?faces-redirect=true";
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Boolean getValidateIdentifier() {
		return validateIdentifier;
	}

	public void setValidateIdentifier(Boolean validateIdentifier) {
		this.validateIdentifier = validateIdentifier;
	}

	public MeasuringInstrument getMeasuringInstrument() {
		return measuringInstrument;
	}

	public void setMeasuringInstrument(MeasuringInstrument measuringInstrument) {
		this.measuringInstrument = measuringInstrument;
	}

	public List<MeasuringInstrumentModel> getMeasuringInstrumentModels() {
		return measuringInstrumentModels;
	}

	public void setMeasuringInstrumentModels(List<MeasuringInstrumentModel> measuringInstrumentModels) {
		this.measuringInstrumentModels = measuringInstrumentModels;
	}

	public List<MeasuringInstrumentType> getMeasuringInstrumentTypes() {
		return measuringInstrumentTypes;
	}

	public void setMeasuringInstrumentTypes(List<MeasuringInstrumentType> measuringInstrumentTypes) {
		this.measuringInstrumentTypes = measuringInstrumentTypes;
	}
	
	public List<MeasuringInstrumentManufacturer> getMeasuringInstrumentManufacturers() {
		return measuringInstrumentManufacturers;
	}

	public void setMeasuringInstrumentManufacturers(List<MeasuringInstrumentManufacturer> measuringInstrumentManufacturers) {
		this.measuringInstrumentManufacturers = measuringInstrumentManufacturers;
	}

	public MeasuringInstrumentManufacturer getMeasuringInstrumentManufacturer() {
		return measuringInstrumentManufacturer;
	}

	public void setMeasuringInstrumentManufacturer(MeasuringInstrumentManufacturer measuringInstrumentManufacturer) {
		this.measuringInstrumentManufacturer = measuringInstrumentManufacturer;
	}

	public List<MeasuringInstrumentPlaceUse> getMeasuringInstrumentPlacesUse() {
		return measuringInstrumentPlacesUse;
	}

	public void setMeasuringInstrumentPlacesUse(List<MeasuringInstrumentPlaceUse> measuringInstrumentPlacesUse) {
		this.measuringInstrumentPlacesUse = measuringInstrumentPlacesUse;
	}

	public List<MeasuringInstrumentStandard> getMeasuringInstrumentStandards() {
		return measuringInstrumentStandards;
	}

	public void setMeasuringInstrumentStandards(List<MeasuringInstrumentStandard> measuringInstrumentStandards) {
		this.measuringInstrumentStandards = measuringInstrumentStandards;
	}

	public List<MeasuringInstrumentStandard> getMeasuringInstrumentStandardsSelected() {
		return this.measuringInstrumentStandardsSelected;
	}
	
	public void setMeasuringInstrumentStandardsSelected(List<MeasuringInstrumentStandard> measuringInstrumentStandardsSelected) {
		this.measuringInstrumentStandardsSelected = measuringInstrumentStandardsSelected;
	}
	
	public List<FilterModel> getFilters() {
		return filters;
	}

	public void setFilters(List<FilterModel> filters) {
		this.filters = filters;
	}
	
	public MeasuringInstrumentStateIdentifier getSelectedMeasuringInstrumentStateIdentifier() {
		return selectedMeasuringInstrumentStateIdentifier;
	}

	public void setSelectedMeasuringInstrumentStateIdentifier(MeasuringInstrumentStateIdentifier selectedState) {
		this.selectedMeasuringInstrumentStateIdentifier = selectedState;
	}

	public List<MeasuringInstrumentDepartment> getMeasuringInstrumentDepartments() {
		return measuringInstrumentDepartments;
	}

	public void setMeasuringInstrumentDepartments(List<MeasuringInstrumentDepartment> measuringInstrumentDepartments) {
		this.measuringInstrumentDepartments = measuringInstrumentDepartments;
	}

	public String getCurrentHashCode() {
		return currentHashCode;
	}

	public void setCurrentHashCode(String currentHashCode) {
		this.currentHashCode = currentHashCode;
	}

	public MeasuringInstrumentStateIdentifier[] getStates() {
		return MeasuringInstrumentStateIdentifier.values();
	}

	public MeasuringInstrumentDamagedIdentifier[] getMeasuringInstrumentDamagedIdentifiers() {
		return MeasuringInstrumentDamagedIdentifier.values();
	}
	
	public MeasuringInstrumentDamagedIdentifier getSelectedMeasuringInstrumentDamagedIdentifier() {
		return selectedMeasuringInstrumentDamagedIdentifier;
	}

	public void setSelectedMeasuringInstrumentDamagedIdentifier(MeasuringInstrumentDamagedIdentifier measuringInstrumentDamagedIdentifier) {
		this.selectedMeasuringInstrumentDamagedIdentifier = measuringInstrumentDamagedIdentifier;
	}
	
	public MeasuringInstrumentObsoleteIdentifier[] getMeasuringInstrumentObsoleteIdentifiers() {
		return MeasuringInstrumentObsoleteIdentifier.values();
	}
	
	public MeasuringInstrumentObsoleteIdentifier getSelectedMeasuringInstrumentObsoleteIdentifier() {
		return selectedMeasuringInstrumentObsoleteIdentifier;
	}

	public void setSelectedMeasuringInstrumentObsoleteIdentifier(MeasuringInstrumentObsoleteIdentifier measuringInstrumentObsoleteIdentifier) {
		this.selectedMeasuringInstrumentObsoleteIdentifier = measuringInstrumentObsoleteIdentifier;
	}

	public List<CalibrationLaboratory> getCalibrationLaboratories() {
		return calibrationLaboratories;
	}

	public void setCalibrationLaboratories(List<CalibrationLaboratory> calibrationLaboratories) {
		this.calibrationLaboratories = calibrationLaboratories;
	}
}
