package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("localeBean")
@Scope("session")
public class LocaleBean implements Serializable {

	private static final long serialVersionUID = -6245679697902443966L;
	private static Logger log = Logger.getLogger(LocaleBean.class);
	private Locale appLocale = new Locale("pt", "BR");

	@PostConstruct
	public void init() {
		FacesContext.getCurrentInstance().getViewRoot()
				.setLocale(this.appLocale);
		log.debug(">>> locale inited to: " + this.appLocale.getLanguage());
	}

	public Locale getAppLocale() {
		return appLocale;
	}

	public void setAppLocale(Locale appLocale) {
		this.appLocale = appLocale;
	}

	// as an action listener
	public void change2English(ActionEvent e) {
		this.appLocale = Locale.ENGLISH;
		FacesContext.getCurrentInstance().getViewRoot()
				.setLocale(this.appLocale);
		log.debug(">>> locale changed to English: "
				+ this.appLocale.getLanguage());
	}

	// as an action method
	public String change2Brasil() {
		this.appLocale = new Locale("pt", "BR");
		FacesContext.getCurrentInstance().getViewRoot()
				.setLocale(this.appLocale);
		log.debug(">>> locale changed to Default: "
				+ this.appLocale.getLanguage());
		return null;
	}
}