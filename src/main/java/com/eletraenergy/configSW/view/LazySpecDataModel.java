package com.eletraenergy.configSW.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import com.eletraenergy.configSW.model.Spec;
import com.eletraenergy.configSW.service.SpecService;

public class LazySpecDataModel extends LazyDataModel<Spec>{

	private static final long serialVersionUID = 4212737496768519554L;
	private List<Spec> specs;
	private SpecService specService;
	private Spec specFilter;
	
     
    public LazySpecDataModel(SpecService specService, Spec specFilter) {
		super();
		this.specService = specService;
		this.specFilter = specFilter;
	}

	@Override
    public Spec getRowData(String rowKey) {
        for(Spec s : specs) {
            if(s.getId().equals(rowKey))
                return s;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(Spec spec) {
        return spec.getId();
    }
 
    @Override
    public List<Spec> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<Spec> data = new ArrayList<Spec>();
 
		specFilter.setSortField(sortField);
		specFilter.setSortOrder(sortOrder);

		data = specService.search(specFilter, first,
				pageSize);

        //rowCount
        int dataSize = specService.amount(specFilter);
        this.setRowCount(dataSize);
 
        return data;
    }
}