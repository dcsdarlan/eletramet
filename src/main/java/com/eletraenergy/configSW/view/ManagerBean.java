package com.eletraenergy.configSW.view;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIViewRoot;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.context.SecurityContextHolder;

import com.eletraenergy.configSW.model.User;
import com.eletraenergy.configSW.util.Constantes;
import com.sun.faces.component.visit.FullVisitContext;

/**
 *
 * @author Raphael
 */
public class ManagerBean {

	public User userLogin;
	
	
	public Flash getFlashScope() {
		return FacesContext.getCurrentInstance().getExternalContext().getFlash();
	}

	public String getRequestParameter(String key) {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
	}
	/**
	 *
	 * @param uiParentComponent
	 */
	protected void resetForm(UIComponent uiParentComponent) {
		for (UIComponent uiComponent : uiParentComponent.getChildren()) {
			if (uiComponent instanceof UIInput) {
				UIInput uii = (UIInput) uiComponent;
				uii.setValue(null);
				uii.setSubmittedValue(null);
				uii.setLocalValueSet(false);
			}
			resetForm(uiComponent);
		}
	}

	public static UIComponent findComponent(final String id) {

	    FacesContext context = FacesContext.getCurrentInstance(); 
	    UIViewRoot root = context.getViewRoot();
	    final UIComponent[] found = new UIComponent[1];
	
	    root.visitTree(new FullVisitContext(context), new VisitCallback() {     
	        public VisitResult visit(VisitContext context, UIComponent component) {
	            if(component.getId().equals(id)){
	                found[0] = component;
	                return VisitResult.COMPLETE;
	            }
	            return VisitResult.ACCEPT;              
	        }
	    });
	
	    return found[0];
	}
	
	public HttpServletRequest getRequestSession() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}	
	
	public void clearSessionMsgAtribute(){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.setAttribute(Constantes.MSG, null);
	}
	
	public void setSessionMsgAtribute(String value){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.setAttribute(Constantes.MSG, value);
	}

	public String getSessionAtribute(String key){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		return (String) session.getAttribute(key);
	}
	
	public HttpServletRequest getSession() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	}	

	public void addSession(String key, Object add){
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		Map<String, Object> sessionMap = externalContext.getSessionMap();
		sessionMap.put(key, add);
	}
	
	public static void errorMsg(String title, String msg){

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,title, msg);
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(null, fm);
	}

	public static void infoMsg(String title, String msg){

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, title, msg);
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(null, fm);
	}
	
	public static void warnMsg(String title, String msg){

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_WARN, title, msg);
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(null, fm);
	}

	public User getUserLogin(){
		return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	public void setUserLogin(User userLogin) {
		this.userLogin = userLogin;
	}
}
