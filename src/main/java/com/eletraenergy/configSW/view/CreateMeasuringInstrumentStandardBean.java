package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.List;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.MeasuringInstrumentStandard;
import com.eletraenergy.configSW.model.TransactionReview;
import com.eletraenergy.configSW.service.MeasuringInstrumentStandardService;
import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.ResourceBundle;

@Component("createMeasuringInstrumentStandardBean")
@Scope("view")
public class CreateMeasuringInstrumentStandardBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -6834442723163022371L;

	public static Logger logger = Logger.getLogger(CreateMeasuringInstrumentStandardBean.class);

	@Autowired
	private MeasuringInstrumentStandardService measuringInstrumentStandardService;
	
	private List<MeasuringInstrumentStandard> measuringInstrumentStandards;
	private MeasuringInstrumentStandard measuringInstrumentStandardFilter;
	
	public void loadInit() {
		if (measuringInstrumentStandardFilter == null){
			measuringInstrumentStandardFilter = new MeasuringInstrumentStandard();
			measuringInstrumentStandardFilter.setSortField("name");
			measuringInstrumentStandardFilter.setSortOrder(SortOrder.ASCENDING);
			
			search();	
		}
	}

	public void addMeasuringInstrumentStandard(ActionEvent actionEvent) {
		MeasuringInstrumentStandard measuringInstrumentStandard = new MeasuringInstrumentStandard();
		measuringInstrumentStandards.add(0,measuringInstrumentStandard);
    }

	public void save(ActionEvent actionEvent) {
		if (measuringInstrumentStandards.size()> 0){
			for (MeasuringInstrumentStandard measuringInstrumentStandard : measuringInstrumentStandards) {
				//Esse teste eh no caso do usuario clicar em novo e salvar sem preencher nada. Essas linhas ser�o ignoradas.
				if (measuringInstrumentStandard.getName() != null && !measuringInstrumentStandard.getName().isEmpty()){
					if (measuringInstrumentStandard.getId()==null){
						//Identificando a Transa��o
						//addSession(Constantes.CODE_TRANSACTION, TransactionReview.CREATE_LINE.getCode());
						//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.CREATE_LINE.getDescription());
						
						measuringInstrumentStandardService.save(measuringInstrumentStandard);
					}else{
						//Identificando a Transa��o
						//addSession(Constantes.CODE_TRANSACTION, TransactionReview.ALTER_LINE.getCode());
						//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.ALTER_LINE.getDescription());

						measuringInstrumentStandardService.update(measuringInstrumentStandard);
					}
				}
			}
			search();
			infoMsg(ResourceBundle.getMessage("measuringInstrumentStandardSave"), null);
		}
    }

	public void checkName(MeasuringInstrumentStandard measuringInstrumentStandard) {
		MeasuringInstrumentStandard checkName = measuringInstrumentStandardService.checkName(measuringInstrumentStandard.getName());
		if (checkName != null){
	      if ((measuringInstrumentStandard.getId()!=null && !checkName.getId().equals(measuringInstrumentStandard.getId())) || (measuringInstrumentStandard.getId()==null && checkName != null)){
	    	  errorMsg(ResourceBundle.getMessage("checkName") + System.lineSeparator() + "[" +  measuringInstrumentStandard.getName() + "]" , null);
	    	  measuringInstrumentStandard.setName(null);
	      }
		}
	}
	
	public void search(){	
		measuringInstrumentStandards = measuringInstrumentStandardService.search(measuringInstrumentStandardFilter, null, null);
	}

	public void remove(MeasuringInstrumentStandard measuringInstrumentStandardSelected){
		//Identificando a Transa��o
		//addSession(Constantes.CODE_TRANSACTION, TransactionReview.DELETE_LINE.getCode());
		//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.DELETE_LINE.getDescription());

		measuringInstrumentStandardService.remove(measuringInstrumentStandardSelected);
		measuringInstrumentStandardFilter = new MeasuringInstrumentStandard();
		measuringInstrumentStandardFilter.setSortField("name");
		measuringInstrumentStandardFilter.setSortOrder(SortOrder.ASCENDING);
		search();
	    infoMsg(ResourceBundle.getMessage("measuringInstrumentStandardDelete"),"");
	}

	public List<MeasuringInstrumentStandard> getMeasuringInstrumentStandards() {
		return measuringInstrumentStandards;
	}

	public MeasuringInstrumentStandard getMeasuringInstrumentStandardFilter() {
		return measuringInstrumentStandardFilter;
	}

	public void setMeasuringInstrumentStandards(List<MeasuringInstrumentStandard> measuringInstrumentStandards) {
		this.measuringInstrumentStandards = measuringInstrumentStandards;
	}

	public void setMeasuringInstrumentStandardFilter(MeasuringInstrumentStandard measuringInstrumentStandardFilter) {
		this.measuringInstrumentStandardFilter = measuringInstrumentStandardFilter;
	}

}
