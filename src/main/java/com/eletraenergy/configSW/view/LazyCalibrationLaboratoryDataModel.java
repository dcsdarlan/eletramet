package com.eletraenergy.configSW.view;

import com.eletraenergy.configSW.model.CalibrationLaboratory;
import com.eletraenergy.configSW.model.MeasuringInstrument;
import com.eletraenergy.configSW.service.CalibrationLaboratoryService;
import com.eletraenergy.configSW.service.MeasuringInstrumentService;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LazyCalibrationLaboratoryDataModel extends LazyDataModel<CalibrationLaboratory>{

	private static final long serialVersionUID = -2625265806840013215L;
	private List<CalibrationLaboratory> measuringInstrument;
	private CalibrationLaboratoryService measuringInstrumentService;
	private CalibrationLaboratory measuringInstrumentFilter;

    public LazyCalibrationLaboratoryDataModel(CalibrationLaboratoryService measuringInstrumentService, CalibrationLaboratory measuringInstrumentFilter) {
		super();
		this.measuringInstrumentService = measuringInstrumentService;
		this.measuringInstrumentFilter = measuringInstrumentFilter;
	}

	@Override
    public CalibrationLaboratory getRowData(String rowKey) {
        for(CalibrationLaboratory s : measuringInstrument) {
            if(s.getId().equals(rowKey))
                return s;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(CalibrationLaboratory measuringInstrument) {
        return measuringInstrument.getId();
    }
 
    @Override
    public List<CalibrationLaboratory> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<CalibrationLaboratory> data = new ArrayList<CalibrationLaboratory>();
 
		measuringInstrumentFilter.setSortField(sortField);
		measuringInstrumentFilter.setSortOrder(sortOrder);

		data = measuringInstrumentService.search(measuringInstrumentFilter, first,
				pageSize);

        //rowCount
        int dataSize = measuringInstrumentService.amount(measuringInstrumentFilter);
        this.setRowCount(dataSize);
 
        return data;
    }
}