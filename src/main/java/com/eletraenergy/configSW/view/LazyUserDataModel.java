package com.eletraenergy.configSW.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.model.User;
import com.eletraenergy.configSW.service.UserService;
 

public class LazyUserDataModel extends LazyDataModel<User>{

	private static final long serialVersionUID = -6147078426805601940L;
	private List<User> users;
	private UserService userService;
	private User userFilter;
	
     
    public LazyUserDataModel(UserService userService, User userFilter) {
		super();
		this.userService = userService;
		this.userFilter = userFilter;
	}

	@Override
    public User getRowData(String rowKey) {
        for(User u : users) {
            if(u.getId().equals(rowKey))
                return u;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(User user) {
        return user.getId();
    }
 
    @Override
    public List<User> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<User> data = new ArrayList<User>();
 
		userFilter.setSortField(sortField);
		userFilter.setSortOrder(sortOrder);

		data = userService.search(userFilter, first,
				pageSize);

        //rowCount
        int dataSize = userService.amount(userFilter);
        this.setRowCount(dataSize);
 
        return data;
    }
}