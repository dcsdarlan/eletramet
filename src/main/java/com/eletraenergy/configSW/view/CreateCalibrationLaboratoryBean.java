package com.eletraenergy.configSW.view;

import com.eletraenergy.configSW.model.*;
import com.eletraenergy.configSW.service.*;
import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component("createCalibrationLaboratoryBean")
@Scope("session")
public class CreateCalibrationLaboratoryBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 7473427567463939699L;

	public static Logger logger = Logger.getLogger(CreateCalibrationLaboratoryBean.class);

	@Autowired
	private CalibrationLaboratoryService calibrationLaboratoryService;
	@Autowired
	private CalibrationLaboratoryTelphoneService calibrationLaboratoryTelphoneService;


	private Boolean validateIdentifier;
	private List<FilterModel> filters;
	private List<CalibrationLaboratoryTelphone> calibrationLaboratoryPhones;
	private CalibrationLaboratory calibrationLaboratory;
	private String operation;

	private String clPhone;

	
	public void loadInit() {
		calibrationLaboratoryPhones = new ArrayList<>();
		CalibrationLaboratoryTelphone tel = new CalibrationLaboratoryTelphone();
		tel.setDdd("85");
		tel.setNumber("99999999");
		calibrationLaboratoryPhones.add(tel);

		if (operation.equals("NEW") && calibrationLaboratory == null) {
			calibrationLaboratory = new CalibrationLaboratory();
		} else{
			if(calibrationLaboratory != null) {
				//calibrationLaboratoryPhones = calibrationLaboratoryTelphoneService.findByCalibrationLaboratory(calibrationLaboratory);
			}
			if (operation.equals("UPDATE") && calibrationLaboratory !=null){


			}
		}
	}
	

	public String save() throws Exception {

		String ret = "searchCalibrationLaboratory?faces-redirect=true";
		if (operation.equals("NEW")){
			calibrationLaboratory.setCnpj(calibrationLaboratory.getCnpj().replace(".", "").replace("/", "").replace("-", ""));
			calibrationLaboratory.setCep(calibrationLaboratory.getCep().replace("-", ""));
			calibrationLaboratory = calibrationLaboratoryService.save(calibrationLaboratory);

		}else{
			calibrationLaboratory.setCnpj(calibrationLaboratory.getCnpj().replace(".", "").replace("/", "").replace("-", ""));
			calibrationLaboratory.setCep(calibrationLaboratory.getCep().replace("-", ""));
			calibrationLaboratory = calibrationLaboratoryService.update(calibrationLaboratory);
		}
		try {
			calibrationLaboratoryTelphoneService.removeByCalibrationLaboratory(calibrationLaboratory);
			for(CalibrationLaboratoryTelphone cPhone: calibrationLaboratoryPhones) {
				calibrationLaboratoryTelphoneService.save(cPhone);
			}
		} catch (Exception e){
			e.printStackTrace();
		}

		if (operation.equals("NEW")) {
			infoMsg(ResourceBundle.getMessage("measuringInstrumentSave"), null);
			calibrationLaboratory = new CalibrationLaboratory();
			ret = null;
		}else{
			setSessionMsgAtribute(ResourceBundle.getMessage("measuringInstrumentUpdate"));
		}
		return ret;
	}

	public List<CalibrationLaboratoryTelphone> getCalibrationLaboratoryPhones() {
		return calibrationLaboratoryPhones;
	}

	public void setCalibrationLaboratoryPhones(List<CalibrationLaboratoryTelphone> calibrationLaboratoryPhones) {
		this.calibrationLaboratoryPhones = calibrationLaboratoryPhones;
	}

	public String back() {
		return "searchCalibrationLaboratory?faces-redirect=true";
	}

	public Boolean getValidateIdentifier() {
		return validateIdentifier;
	}

	public void setValidateIdentifier(Boolean validateIdentifier) {
		this.validateIdentifier = validateIdentifier;
	}

	public List<FilterModel> getFilters() {
		return filters;
	}

	public void setFilters(List<FilterModel> filters) {
		this.filters = filters;
	}

	public CalibrationLaboratory getCalibrationLaboratory() {
		return calibrationLaboratory;
	}

	public void setCalibrationLaboratory(CalibrationLaboratory calibrationLaboratory) {
		this.calibrationLaboratory = calibrationLaboratory;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public void addTel() {
		RequestContext ctx = RequestContext.getCurrentInstance();
		ctx.execute("PF('telPanel').hide();");
		System.out.println("Telefone:" + getClPhone());
		String[] phone =  getClPhone().split(" ");
		phone[0].replace("(", "").replace(")", "");
		phone[1].replace("-", "");
		CalibrationLaboratoryTelphone tel = new CalibrationLaboratoryTelphone();
		tel.setDdd(phone[0].replace("(", "").replace(")", ""));
		tel.setNumber(phone[1].replace("-", ""));
		calibrationLaboratoryPhones.add(tel);
		System.out.println("TElefones:" + calibrationLaboratoryPhones.size());
		//setClPhone("");
	}

	public void rmTel(CalibrationLaboratoryTelphone tel) {
		calibrationLaboratoryPhones.remove(tel);
		System.out.println("TElefones:" + calibrationLaboratoryPhones.size());
		//setClPhone("");
	}

	public void addLabTel() {
		CalibrationLaboratoryTelphone tel = new CalibrationLaboratoryTelphone();
		calibrationLaboratoryPhones.add(0, tel);
	}

	public String getClPhone() {
		return clPhone;
	}



	public void setClPhone(String clPhone) {
		this.clPhone = clPhone;
	}

	public String register() {
		return "createCalibrationLaboratory?faces-redirect=true";
	}
}
