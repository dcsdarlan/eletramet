package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.envers.RevisionType;
import org.primefaces.event.timeline.TimelineSelectEvent;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.Config;
import com.eletraenergy.configSW.model.MeterLine;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.Review;
import com.eletraenergy.configSW.model.SpecCategory;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.TdsTemplate;
import com.eletraenergy.configSW.service.ReviewService;

/**
 * Use case : Home bean
 * 
 * @since : Ago 18, 2017, 14:23:20 PM
 * @author : raphael.ferreira
 */

@Component("homeBean")
@Scope("view")
public class HomeBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 5680914267998554474L;

	public static Logger logger = Logger.getLogger(HomeBean.class);

	@Autowired
	private ReviewService reviewService;
	
	private TimelineModel model; 	
    
    private Review reviewSelected;
    
    private List<Tds> tdsAud;

    private List<Config> configAud;

    private List<SpecCategory> specCategoryAud;

    private List<MeterLine> meterLineAud;

    private List<MeterModel> meterModelAud;

    private List<TdsTemplate> tdsTemplateAud;

    public void loadInit() {
    	
    	/*
		//Iniciar objet de time line
		model = new TimelineModel();
		//carregar as 10 ultimas alterações do sistema
		List<Review> findLast10 = reviewService.findLast10();
		for (Review review : findLast10) {
			//Criando evento
			model.add(new TimelineEvent(review, review.getDateAudit(), false, review.getUser().getName(), review.getStyle()));  
		}
		*/
    	
	}

	@SuppressWarnings("unchecked")
	public void onSelect(TimelineSelectEvent e) {  
		tdsAud = new ArrayList<Tds>();
    	tdsTemplateAud = new ArrayList<TdsTemplate>();
    	configAud = new ArrayList<Config>();
    	specCategoryAud = new ArrayList<SpecCategory>();
    	meterLineAud = new ArrayList<MeterLine>();
    	meterModelAud = new ArrayList<MeterModel>();
	
		
        TimelineEvent timelineEvent = e.getTimelineEvent();  
        setReviewSelected((Review) timelineEvent.getData());
        
        List<Object> revTables = reviewService.findByReview(getReviewSelected());
        //Verificar se tem TDS, index = 0
        List<Object>  tdsList = (List<Object>) revTables.get(0);
        if (!tdsList.isEmpty()){
    		for (Object object : tdsList) {
    	        Object[] object2 = (Object[]) object;
            	Tds tds = (Tds) object2[0];
            	Review review = (Review) object2[1];
            	RevisionType reviewType = (RevisionType) object2[2];
            	
            	tds.setTransaction(review.getId());
            	tds.setDateAudit(review.getDateAudit());
            	tds.setTypeReview(reviewType);
            	tds.setUserReview(review.getUser());
            	
            	tdsAud.add(tds);
			}
        }
        //Verificar se tem Config, index = 1
    	List<Object>  configList = (List<Object>) revTables.get(1);
        if (!configList.isEmpty()){
        	for (Object object : configList) {
    	        Object[] object2 = (Object[]) object;
            	Config config = (Config) object2[0];
            	Review review = (Review) object2[1];
            	RevisionType reviewType = (RevisionType) object2[2];
            	
            	config.setTransaction(review.getId());
            	config.setDateAudit(review.getDateAudit());
            	config.setTypeReview(reviewType);
            	config.setUserReview(review.getUser());

            	configAud.add(config);
			}
        }

        //Verificar se tem SpecCategory, index = 2
        List<Object>  specCategoryList = (List<Object>) revTables.get(2);
        if (!specCategoryList.isEmpty()){
    		for (Object object : specCategoryList) {
    	        Object[] object2 = (Object[]) object;
    	        SpecCategory specCategory = (SpecCategory) object2[0];
            	Review review = (Review) object2[1];
            	RevisionType reviewType = (RevisionType) object2[2];
            	
            	specCategory.setTransaction(review.getId());
            	specCategory.setDateAudit(review.getDateAudit());
            	specCategory.setTypeReview(reviewType);
            	specCategory.setUserReview(review.getUser());

            	specCategoryAud.add(specCategory);
			}
        }

        //Verificar de Tem MeterLine, index = 3
        List<Object>  meterLineList = (List<Object>) revTables.get(3);
        if (!meterLineList.isEmpty()){
        	for (Object object : meterLineList) {
    	        Object[] object2 = (Object[]) object;
    	        MeterLine meterLine = (MeterLine) object2[0];
            	Review review = (Review) object2[1];
            	RevisionType reviewType = (RevisionType) object2[2];
            	
            	meterLine.setTransaction(review.getId());
            	meterLine.setDateAudit(review.getDateAudit());
            	meterLine.setTypeReview(reviewType);
            	meterLine.setUserReview(review.getUser());

            	meterLineAud.add(meterLine);
			}
        }

        //Verificar de Tem MeterModel, index = 4
        List<Object>  meterModelList = (List<Object>) revTables.get(4);
        if (!meterModelList.isEmpty()){
    		for (Object object : meterModelList) {
    	        Object[] object2 = (Object[]) object;
    	        MeterModel meterModel = (MeterModel) object2[0];
            	Review review = (Review) object2[1];
            	RevisionType reviewType = (RevisionType) object2[2];
            	
            	meterModel.setTransaction(review.getId());
            	meterModel.setDateAudit(review.getDateAudit());
            	meterModel.setTypeReview(reviewType);
            	meterModel.setUserReview(review.getUser());

            	meterModelAud.add(meterModel);
			}
        }

        //Verificar de Tem TdsTemplate, index = 5
        List<Object>  tdsTemplateList = (List<Object>) revTables.get(5);
        if (!tdsTemplateList.isEmpty()){
    		for (Object object : tdsTemplateList) {
    	        Object[] object2 = (Object[]) object;
    	        TdsTemplate tdsTemplate = (TdsTemplate) object2[0];
            	Review review = (Review) object2[1];
            	RevisionType reviewType = (RevisionType) object2[2];
            	
            	tdsTemplate.setTransaction(review.getId());
            	tdsTemplate.setDateAudit(review.getDateAudit());
            	tdsTemplate.setTypeReview(reviewType);
            	tdsTemplate.setUserReview(review.getUser());

            	tdsTemplateAud.add(tdsTemplate);
			}
        }

    }  
	
	public ReviewService getReviewService() {
		return reviewService;
	}

	public void setReviewService(ReviewService reviewService) {
		this.reviewService = reviewService;
	}

	public TimelineModel getModel() {
		return model;
	}

	public void setModel(TimelineModel model) {
		this.model = model;
	}

	public Review getReviewSelected() {
		return reviewSelected;
	}

	public void setReviewSelected(Review reviewSelected) {
		this.reviewSelected = reviewSelected;
	}

	public List<Config> getConfigAud() {
		return configAud;
	}

	public void setConfigAud(List<Config> configAud) {
		this.configAud = configAud;
	}

	public List<SpecCategory> getSpecCategoryAud() {
		return specCategoryAud;
	}

	public void setSpecCategoryAud(List<SpecCategory> specCategoryAud) {
		this.specCategoryAud = specCategoryAud;
	}

	public List<MeterLine> getMeterLineAud() {
		return meterLineAud;
	}

	public void setMeterLineAud(List<MeterLine> meterLineAud) {
		this.meterLineAud = meterLineAud;
	}

	public List<MeterModel> getMeterModelAud() {
		return meterModelAud;
	}

	public void setMeterModelAud(List<MeterModel> meterModelAud) {
		this.meterModelAud = meterModelAud;
	}

	public List<TdsTemplate> getTdsTemplateAud() {
		return tdsTemplateAud;
	}

	public void setTdsTemplateAud(List<TdsTemplate> tdsTemplateAud) {
		this.tdsTemplateAud = tdsTemplateAud;
	}

	public List<Tds> getTdsAud() {
		return tdsAud;
	}

	public void setTdsAud(List<Tds> tdsAud) {
		this.tdsAud = tdsAud;
	}

}