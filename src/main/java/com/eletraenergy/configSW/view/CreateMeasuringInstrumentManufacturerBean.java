package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.List;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;
import com.eletraenergy.configSW.model.TransactionReview;
import com.eletraenergy.configSW.service.MeasuringInstrumentManufacturerService;
import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.ResourceBundle;

@Component("createMeasuringInstrumentManufacturerBean")
@Scope("session")
public class CreateMeasuringInstrumentManufacturerBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -6834442723163022371L;

	public static Logger logger = Logger.getLogger(CreateMeasuringInstrumentManufacturerBean.class);

	@Autowired
	private MeasuringInstrumentManufacturerService measuringInstrumentManufacturerService;
	
	private List<MeasuringInstrumentManufacturer> measuringInstrumentManufacturers;
	private MeasuringInstrumentManufacturer measuringInstrumentManufacturerFilter;
	
	public void loadInit() {
		if (measuringInstrumentManufacturerFilter == null){
			measuringInstrumentManufacturerFilter = new MeasuringInstrumentManufacturer();
			measuringInstrumentManufacturerFilter.setSortField("name");
			measuringInstrumentManufacturerFilter.setSortOrder(SortOrder.ASCENDING);
			
			search();	
		}
	}

	public void addMeasuringInstrumentManufacturer(ActionEvent actionEvent) {
		MeasuringInstrumentManufacturer measuringInstrumentManufacturer = new MeasuringInstrumentManufacturer();
		measuringInstrumentManufacturers.add(0,measuringInstrumentManufacturer);
    }

	public void save(ActionEvent actionEvent) {
		if (measuringInstrumentManufacturers.size()> 0){
			for (MeasuringInstrumentManufacturer measuringInstrumentManufacturer : measuringInstrumentManufacturers) {
				//Esse teste eh no caso do usuario clicar em novo e salvar sem preencher nada. Essas linhas ser�o ignoradas.
				if (measuringInstrumentManufacturer.getName() != null && !measuringInstrumentManufacturer.getName().isEmpty()){
					if (measuringInstrumentManufacturer.getId()==null){
						//Identificando a Transa��o
						addSession(Constantes.CODE_TRANSACTION, TransactionReview.CREATE_MEASURING_INSTRUMENT_MANUFACTURER.getCode());
						addSession(Constantes.TITLE_TRANSACTION, TransactionReview.CREATE_MEASURING_INSTRUMENT_MANUFACTURER.getDescription());
						
						measuringInstrumentManufacturerService.save(measuringInstrumentManufacturer);
					}else{
						//Identificando a Transa��o
						addSession(Constantes.CODE_TRANSACTION, TransactionReview.ALTER_MEASURING_INSTRUMENT_MANUFACTURER.getCode());
						addSession(Constantes.TITLE_TRANSACTION, TransactionReview.ALTER_MEASURING_INSTRUMENT_MANUFACTURER.getDescription());

						measuringInstrumentManufacturerService.update(measuringInstrumentManufacturer);
					}
				}
			}
			search();
			infoMsg(ResourceBundle.getMessage("measuringInstrumentManufacturerSave"), null);
		}
    }

	public void checkName(MeasuringInstrumentManufacturer measuringInstrumentManufacturer) {
		MeasuringInstrumentManufacturer checkName = measuringInstrumentManufacturerService.checkName(measuringInstrumentManufacturer.getName());
		if (checkName != null){
	      if ((measuringInstrumentManufacturer.getId()!=null && !checkName.getId().equals(measuringInstrumentManufacturer.getId())) || (measuringInstrumentManufacturer.getId()==null && checkName != null)){
	    	  errorMsg(ResourceBundle.getMessage("checkName") + System.lineSeparator() + "[" +  measuringInstrumentManufacturer.getName() + "]" , null);
	    	  measuringInstrumentManufacturer.setName(null);
	      }
		}
	}
	
	public void search(){	
		measuringInstrumentManufacturers = measuringInstrumentManufacturerService.search(measuringInstrumentManufacturerFilter, null, null);
	}

	public void remove(MeasuringInstrumentManufacturer measuringInstrumentManufacturerSelected){
		
		if (!measuringInstrumentManufacturerSelected.getMeasuringInstrumentModels().isEmpty()) {
			errorMsg(ResourceBundle.getMessage("measuringInstrumentManufacturerCantDelete"),"");	
			return;
		}
		
		measuringInstrumentManufacturerService.remove(measuringInstrumentManufacturerSelected);
		measuringInstrumentManufacturerFilter = new MeasuringInstrumentManufacturer();
		measuringInstrumentManufacturerFilter.setSortField("name");
		measuringInstrumentManufacturerFilter.setSortOrder(SortOrder.ASCENDING);
		search();
	    infoMsg(ResourceBundle.getMessage("measuringInstrumentManufacturerDelete"),"");
	}

	public List<MeasuringInstrumentManufacturer> getMeasuringInstrumentManufacturers() {
		return measuringInstrumentManufacturers;
	}

	public MeasuringInstrumentManufacturer getMeasuringInstrumentManufacturerFilter() {
		return measuringInstrumentManufacturerFilter;
	}

	public void setMeasuringInstrumentManufacturers(List<MeasuringInstrumentManufacturer> measuringInstrumentManufacturers) {
		this.measuringInstrumentManufacturers = measuringInstrumentManufacturers;
	}

	public void setMeasuringInstrumentManufacturerFilter(MeasuringInstrumentManufacturer measuringInstrumentManufacturerFilter) {
		this.measuringInstrumentManufacturerFilter = measuringInstrumentManufacturerFilter;
	}

}
