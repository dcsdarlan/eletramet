package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import com.eletraenergy.configSW.model.*;
import com.eletraenergy.configSW.service.*;
import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.ResourceBundle;

@Component("searchMeasuringInstrumentBean")
@SessionScoped
public class SearchMeasuringInstrumentBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -8102482899401242347L;

	public static Logger logger = Logger.getLogger(SearchMeasuringInstrumentBean.class);

	@Autowired
	private MeasuringInstrumentService measuringInstrumentService;

	@Autowired
	private CalibrationLaboratoryService calibrationLaboratoryService;

	@Autowired
	private MeasuringInstrumentTypeService measuringInstrumentTypeService;

	@Autowired
	private MeasuringInstrumentManufacturerService measuringInstrumentManufacturerService;

	@Autowired
	private MeasuringInstrumentModelService measuringInstrumentModelService;

	@Autowired
	private MeasuringInstrumentPlaceUseService measuringInstrumentPlaceUseService;

	@Autowired
	private MeasuringInstrumentStandardService measuringInstrumentStandardService;

	@Autowired
	private MeasuringInstrumentDepartmentService measuringInstrumentDepartmentService;

	private List<MeasuringInstrumentType> measuringInstrumentTypes;
	private List<MeasuringInstrumentManufacturer> measuringInstrumentManufacturers;
	private List<MeasuringInstrumentModel> measuringInstrumentModels;
	private List<MeasuringInstrumentPlaceUse> measuringInstrumentPlacesUse;
	private List<MeasuringInstrumentStandard> measuringInstrumentStandards;
	private List<MeasuringInstrumentDepartment> measuringInstrumentDepartments;
	private List<MeasuringInstrumentStandard> measuringInstrumentStandardsSelected;
	private LazyDataModel<MeasuringInstrument> measuringInstrument;
	private List<CalibrationLaboratory> calibrationLaboratories;
	private MeasuringInstrument measuringInstrumentFilter;
	private Date nextCalibrationDateFilterFrom;
	private Date nextCalibrationDateFilterTo;


	public void loadInit() {

		measuringInstrumentTypes = measuringInstrumentTypeService.findAll();
		measuringInstrumentManufacturers = measuringInstrumentManufacturerService.findAll();
		measuringInstrumentPlacesUse = measuringInstrumentPlaceUseService.findAll();
		measuringInstrumentStandards = measuringInstrumentStandardService.findAll();
		measuringInstrumentDepartments = measuringInstrumentDepartmentService.findAll();
		calibrationLaboratories = calibrationLaboratoryService.findAll();

		String msg = getSessionAtribute(Constantes.MSG);
		if (msg !=null){
			infoMsg(msg, null);
			clearSessionMsgAtribute();
		}

		if (measuringInstrumentStandardsSelected==null) {
			measuringInstrumentStandardsSelected = new ArrayList<MeasuringInstrumentStandard>();
		}

		if (measuringInstrumentFilter == null) {
			measuringInstrumentFilter = new MeasuringInstrument();
			search();
		}
	}

	public String register() {
		return "createMeasuringInstrument?faces-redirect=true";
	}

	public void remove(MeasuringInstrument measuringInstrument) {

		//Identificando a Transa��o
		addSession(Constantes.CODE_TRANSACTION, TransactionReview.DELETE_MEASURING_INSTRUMENT.getCode());
		addSession(Constantes.TITLE_TRANSACTION, TransactionReview.DELETE_MEASURING_INSTRUMENT.getDescription());

		measuringInstrument.setMeasuringInstrumentStandards(null);
		measuringInstrumentService.update(measuringInstrument);
		measuringInstrumentService.remove(measuringInstrument);

		search();

		infoMsg(ResourceBundle.getMessage("measuringInstrumentDelete"), null);
	}

	public void loadMeasuringInstrumentModels() {
		measuringInstrumentModels = measuringInstrumentModelService.findbyManufacturer(measuringInstrumentFilter.getMeasuringInstrumentManufacturer());
	}


	/*
	public void exportToExcel(MeasuringInstrument measuringInstrument) {

		ExcelUtils excelUtils = new ExcelUtils(false);

		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    final ExternalContext externalContext = facesContext.getExternalContext();

	    externalContext.responseReset();
	    externalContext.setResponseContentType("application/octet-stream");
	    externalContext.setResponseHeader("Content-Disposition", "attachment;filename=measuringInstrument-export.xls");

	    try {

	    	List<MeasuringInstrumentSpecOption> listOfMeasuringInstrumentSpecOption = measuringInstrument.getTemplate().getMeasuringInstrumentSpecOptions();
	    	List<MeasuringInstrumentSpec> listOfMeasuringInstrumentSpec = measuringInstrumentSpecService.findAll();
	    	ViewMeasuringInstrumentAud viewMeasuringInstrumentAud = viewMeasuringInstrumentAudService.findById(measuringInstrument.getId());

			excelUtils.exportMeasuringInstrumentToExcel(listOfMeasuringInstrumentSpec, listOfMeasuringInstrumentSpecOption, measuringInstrument, viewMeasuringInstrumentAud).write(
					((HttpServletResponse) externalContext.getResponse()).getOutputStream());

			facesContext.responseComplete();

		} catch (Exception e) {

			externalContext.responseReset();
			errorMsg(ResourceBundle.getMessage("measuringInstrumentExportToExcelError"), null);
		}
	}
	*/

	public void search() {
		measuringInstrumentFilter.setMeasuringInstrumentStandards(measuringInstrumentStandardsSelected);
		measuringInstrument = new LazyMeasuringInstrumentDataModel(measuringInstrumentService, measuringInstrumentFilter);
	}

	public void clear() {
		measuringInstrumentFilter.setMeasuringInstrumentStateIdentifier(null);
		measuringInstrumentFilter.setMeasuringInstrumentType(null);
		measuringInstrumentFilter.setMeasuringInstrumentManufacturer(null);
		measuringInstrumentFilter.setMeasuringInstrumentModel(null);
		measuringInstrumentFilter.setMeasuringInstrumentPlaceUse(null);
		measuringInstrumentFilter.setMeasuringInstrumentDepartment(null);
		measuringInstrumentFilter.setSpecificationIdentifier(null);
		measuringInstrumentFilter.setMeasuringInstrumentStandards(null);
		measuringInstrumentFilter.setMeasuringInstrumentObsoleteIdentifier(null);
		measuringInstrumentFilter.setMeasuringInstrumentDamagedIdentifier(null);
		measuringInstrumentFilter.setId(null);
		measuringInstrumentStandardsSelected = null;
		nextCalibrationDateFilterFrom = null;
		nextCalibrationDateFilterTo = null;
		measuringInstrumentFilter.setNextCalibrationDateFilterFrom(null);
		measuringInstrumentFilter.setNextCalibrationDateFilterTo(null);
	}

	public LazyDataModel<MeasuringInstrument> getMeasuringInstrument() {
		return measuringInstrument;
	}

	public void setMeasuringInstrument(LazyDataModel<MeasuringInstrument> measuringInstrument) {
		this.measuringInstrument = measuringInstrument;
	}

	public MeasuringInstrument getMeasuringInstrumentFilter() {
		return measuringInstrumentFilter;
	}

	public void setMeasuringInstrumentFilter(MeasuringInstrument measuringInstrumentFilter) {
		this.measuringInstrumentFilter = measuringInstrumentFilter;
	}

	public MeasuringInstrumentStateIdentifier[] getStates() {
		return MeasuringInstrumentStateIdentifier.values();
	}

	public List<MeasuringInstrumentType> getMeasuringInstrumentTypes() {
		return measuringInstrumentTypes;
	}

	public void setMeasuringInstrumentTypes(List<MeasuringInstrumentType> measuringInstrumentTypes) {
		this.measuringInstrumentTypes = measuringInstrumentTypes;
	}

	public List<MeasuringInstrumentManufacturer> getMeasuringInstrumentManufacturers() {
		return measuringInstrumentManufacturers;
	}

	public void setMeasuringInstrumentManufacturers(List<MeasuringInstrumentManufacturer> measuringInstrumentManufacturers) {
		this.measuringInstrumentManufacturers = measuringInstrumentManufacturers;
	}

	public List<MeasuringInstrumentPlaceUse> getMeasuringInstrumentPlacesUse() {
		return measuringInstrumentPlacesUse;
	}

	public void setMeasuringInstrumentPlacesUse(List<MeasuringInstrumentPlaceUse> measuringInstrumentPlacesUse) {
		this.measuringInstrumentPlacesUse = measuringInstrumentPlacesUse;
	}

	public List<MeasuringInstrumentDepartment> getMeasuringInstrumentDepartments() {
		return measuringInstrumentDepartments;
	}

	public void setMeasuringInstrumentDepartments(List<MeasuringInstrumentDepartment> measuringInstrumentDepartments) {
		this.measuringInstrumentDepartments = measuringInstrumentDepartments;
	}

	public List<MeasuringInstrumentStandard> getMeasuringInstrumentStandards() {
		return measuringInstrumentStandards;
	}

	public void setMeasuringInstrumentStandards(List<MeasuringInstrumentStandard> measuringInstrumentStandards) {
		this.measuringInstrumentStandards = measuringInstrumentStandards;
	}

	public List<MeasuringInstrumentModel> getMeasuringInstrumentModels() {
		return measuringInstrumentModels;
	}

	public List<MeasuringInstrumentStandard> getMeasuringInstrumentStandardsSelected() {
		return this.measuringInstrumentStandardsSelected;
	}

	public void setMeasuringInstrumentStandardsSelected(List<MeasuringInstrumentStandard> measuringInstrumentStandardsSelected) {
		this.measuringInstrumentStandardsSelected = measuringInstrumentStandardsSelected;
	}

	public MeasuringInstrumentDamagedIdentifier[] getMeasuringInstrumentDamagedIdentifiers() {
		return MeasuringInstrumentDamagedIdentifier.values();
	}

	public MeasuringInstrumentObsoleteIdentifier[] getMeasuringInstrumentObsoleteIdentifiers() {
		return MeasuringInstrumentObsoleteIdentifier.values();
	}

	public List<CalibrationLaboratory> getCalibrationLaboratories() {
		return calibrationLaboratories;
	}

	public void setCalibrationLaboratories(List<CalibrationLaboratory> calibrationLaboratories) {
		this.calibrationLaboratories = calibrationLaboratories;
	}
}