package com.eletraenergy.configSW.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.service.MeterModelService;

public class LazyMeterModelDataModel extends LazyDataModel<MeterModel>{

	private static final long serialVersionUID = 5250017367171927063L;
	private List<MeterModel> meterModels;
	private MeterModelService meterModelService;
	private MeterModel meterModelFilter;
	
     
    public LazyMeterModelDataModel(MeterModelService meterModelService, MeterModel meterModelFilter) {
		super();
		this.meterModelService = meterModelService;
		this.meterModelFilter = meterModelFilter;
	}

	@Override
    public MeterModel getRowData(String rowKey) {
        for(MeterModel s : meterModels) {
            if(s.getId().equals(rowKey))
                return s;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(MeterModel meterModel) {
        return meterModel.getId();
    }
 
    @Override
    public List<MeterModel> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<MeterModel> data = new ArrayList<MeterModel>();
 
		meterModelFilter.setSortField(sortField);
		meterModelFilter.setSortOrder(sortOrder);

		data = meterModelService.search(meterModelFilter, first,
				pageSize);

        //rowCount
        int dataSize = meterModelService.amount(meterModelFilter);
        this.setRowCount(dataSize);
 
        return data;
    }
}