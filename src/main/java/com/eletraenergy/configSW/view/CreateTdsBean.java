package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.FilterModel;
import com.eletraenergy.configSW.model.MeterModel;
import com.eletraenergy.configSW.model.StateIdentifier;
import com.eletraenergy.configSW.model.Tds;
import com.eletraenergy.configSW.model.TdsSpecOption;
import com.eletraenergy.configSW.model.TdsStateIdentifier;
import com.eletraenergy.configSW.model.TdsTemplate;
import com.eletraenergy.configSW.model.TransactionReview;
import com.eletraenergy.configSW.service.MeterModelService;
import com.eletraenergy.configSW.service.TdsService;
import com.eletraenergy.configSW.service.TdsSpecOptionService;
import com.eletraenergy.configSW.service.TdsTemplateService;
import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.HashCodeUtils;
import com.eletraenergy.configSW.util.ResourceBundle;
import com.google.gson.Gson;

/**
* Use case : Create Tds
* 
* @since   : Mar 24, 2017, 11:23:20 PM
* @author  : raphael.ferreira
*/

@Component("createTdsBean")
@Scope("session")
public class CreateTdsBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 7473427567463939699L;

	public static Logger logger = Logger.getLogger(CreateTdsBean.class);

	@Autowired
	private TdsService tdsService;
	@Autowired
	private TdsSpecOptionService tdsSpecOptionService;
	@Autowired
	private MeterModelService meterModelService;
	@Autowired
	private TdsTemplateService tdsTemplateService;
	
	private Tds tds;
	private String operation;
	private List<MeterModel> models;
	private Boolean validateIdentifier;
	private List<TdsSpecOption> specOptions;
	private List<FilterModel> filters;
	private List<Tds> tdsCopy;
	private Tds tdsToCopy;
	private String currentHashCode;
	private String selectedChangeState;
	private TdsStateIdentifier selectedState;
	private List<TdsTemplate> tdsTemplates;
	private MeterModel meterModel;
	
	public void loadInit() {
		
		models = meterModelService.findAll();
		
		//Se for um novo e ainda tiver nulo criar objeto.
		if (operation.equals("NEW") && tds == null) {
				
			cleanObjects();
			
			tds = new Tds();
			
			selectedChangeState = StateIdentifier.notm.getName();
			tds.setChangeIdentifier(selectedChangeState);
			tds.setState(TdsStateIdentifier.c);
			tds.setStateModified(new Date());
			
			validateIdentifier = null;
			
		}else{
			
			if (tds !=null){
				//Carregar os valores da combo de template de acordo com o modelo da TDS que est� editando, assim a combo mostrarar o valor do template se houver.
				if (tds.getMeterModel() != null && tdsTemplates == null) {
					tdsTemplates = tdsTemplateService.findbyMeterModel(tds.getMeterModel());
				} 
				
				if (tdsCopy==null && tds.getMeterModel()!=null){
					tdsCopy = new ArrayList<Tds>();
					List<Tds> all = tdsService.findByModel(tds.getMeterModel());
					for (Tds tds : all) {
						if (!this.tds.getId().equals(tds.getId()) && tds.getTdsSpecOptions()!= null && !tds.getTdsSpecOptions().isEmpty()){
							tdsCopy.add(tds);
						}
					}
				}

				
				if (tds.getTemplate()==null) {
					logger.debug("loadInit: Template=> null");
				} else {
					logger.debug("loadInit: Template=>" + tds.getTemplate().getIdentifier());	
				}
				
				if((filters==null) && tds.getTemplate()!=null) {
					
					specOptions = tds.getTemplate().getTdsSpecOptions();
					
					List<TdsSpecOption> valueOptions = tds.getTdsSpecOptions();
					currentHashCode = generateHash(valueOptions);
					loadFilters(valueOptions);
				}
				
				selectedState = tds.getState();
				selectedChangeState = tds.getChangeIdentifier();
			}
		}
	}
	
	private String generateHash(List<TdsSpecOption> tdsSpecOptions) {
		Map<Object,Object> valueOptionsMap = new HashMap<Object,Object>();
		valueOptionsMap.put(1, tdsSpecOptions);
		Gson gson = new Gson();
		String json = gson.toJson(tdsSpecOptions);
		return HashCodeUtils.generateHashCode(json);
	}
	
	public void copyTds(){
		if (tdsToCopy != null) {
			setFilter(tdsToCopy.getTdsSpecOptions());
			infoMsg(ResourceBundle.getMessage("tdsCopied"), null);
		}
	}
	
	public void checkSpec(FilterModel filter){
	
		if (filter.getValue()!=null){
			filter.setHeaderCheck(true);
		}else{
			filter.setHeaderCheck(false);
		}

	}
	
	public void loadTdsTemplates() {
		
		tdsTemplates = null;
		specOptions = null;
		filters = null;
		tds.setTemplate(null);
		
		if (tds.getMeterModel() != null) {
			tdsTemplates = tdsTemplateService.findbyMeterModel(tds.getMeterModel());
			
			//Combobox de copia de op��es de tds
			tdsCopy = new ArrayList<Tds>();
			List<Tds> all = tdsService.findByModel(tds.getMeterModel());
			for (Tds tds : all) {
				if (tds.getTdsSpecOptions()!= null & !tds.getTdsSpecOptions().isEmpty()){
					//Retirar ela mesmo da lista de copiar em caso de altera��o.
					if (this.tds.getId()==null || !this.tds.getId().equals(tds.getId())){
						tdsCopy.add(tds);
					}
				}
			}
			//Zerar possivel sugest�o anterior
			this.tds.setIdentifier(null);
			//Buscar a lista de tds por modelo em ordem de identificador para sugerir o proximo
			List<Tds> listTdsIdentifier = tdsService.findByModelOrderIdentifier(tds.getMeterModel());
			for (Tds tds : listTdsIdentifier) {
				try {
					int newIdentifier = Integer.valueOf(tds.getIdentifier()) + 1;
					this.tds.setIdentifier(String.valueOf(newIdentifier));
					break;
				} catch (NumberFormatException e) {
				}
			}
			

		} 
	}
	
	public void loadTemplateFilters() {
		
		specOptions = null;
		filters = null;
		
		System.out.println("loadTemplateFilters: Template=>" + tds.getTemplate().getIdentifier());
		
		if (tds.getTemplate() != null) {
			specOptions = tds.getTemplate().getTdsSpecOptions();
			loadFilters(null);
		}
	}
	
	public void loadFilters(List<TdsSpecOption> valueOptions) {
		
		buildFilters();
		
		if (valueOptions!=null){
			setFilter(valueOptions);
		}
	}
	
	private void buildFilters() {
		filters = new ArrayList<FilterModel>();
		Long id = null;
		List<String> listOptions = null;
		List<String> listIntOptions = null;
		String option = null;
		String intOption = null;
		
		for (TdsSpecOption specOption : specOptions) {
			if (id == null || !id.equals(specOption.getTdsSpec().getId())){
				if (listOptions != null){
					FilterModel filtroModel = new FilterModel(option, intOption, listOptions, listIntOptions);
					
					if (filtroModel.getOptions().size()==1) {
						filtroModel.setValue(filtroModel.getOptions().get(0));
						filtroModel.setHeaderCheck(true);
					}
					
					filters.add(filtroModel);
				}
				id = specOption.getTdsSpec().getId();
				option = specOption.getTdsSpec().getName();
				intOption = specOption.getTdsSpec().getIntName();
				listOptions = new ArrayList<String>();
				listIntOptions = new ArrayList<String>();
			}
			listOptions.add(specOption.getName());
			listIntOptions.add(specOption.getIntName());
		}
		//The last
		if (listOptions != null){
			FilterModel filtroModel = new FilterModel(option, intOption, listOptions, listIntOptions);
			if (filtroModel.getOptions().size()==1) {
				filtroModel.setValue(filtroModel.getOptions().get(0));
				filtroModel.setHeaderCheck(true);
			}
			filters.add(filtroModel);
		}
		
		Collections.sort(filters);
	}
	
	private void setFilter(List<TdsSpecOption> valueOptions) {
		for (FilterModel filterModel : filters) {
			if (filterModel.getOptions().size()>1) {
				for (TdsSpecOption tdsSpecOption : valueOptions) {
					if (filterModel.getHeader().equals(tdsSpecOption.getTdsSpec().getName())){
						filterModel.setValue(tdsSpecOption.getName());
						filterModel.setHeaderCheck(true);
					}
				}
			}
		}
	}
	
	public void checkIdentifier() {
		Tds checkIdentifier = tdsService.checkIdentifier(tds.getIdentifier());
		if (checkIdentifier != null){
			if ( (tds.getId()!=null && !checkIdentifier.getId().equals(tds.getId())) || (tds.getId()==null && checkIdentifier != null)){
				errorMsg(ResourceBundle.getMessage("checkIdentifier") + " " +  tds.getIdentifier() , null);
				tds.setIdentifier(null);
				validateIdentifier = false;
			}else{
				validateIdentifier = true;
			}
		}else{
			validateIdentifier = true;
		}
	}

	private void validateRequiredFields() throws Exception {
		
		boolean validated = true;
		
		for (FilterModel filterModel : filters) {
			if(filterModel.getHeaderCheck()==null){
				errorMsg(ResourceBundle.getMessage("requiredField") + ": [" + filterModel.getIntHeader()+"]",null);
				validated=false;
			}
		}
		
		if (!validated) {
			throw new Exception("");
		}
	}	
		
	private void validateDuplicateTds(List<TdsSpecOption> tdsSpecOptions) throws Exception {
		
		boolean validated = true;
		boolean specOptionNotFound = true;
		
		List<Tds> listOftds= tdsService.findByModel(tds.getMeterModel());
		
		for (Tds tds: listOftds) {
			
			if (this.tds.getId() == null || (this.tds.getId() !=null && !this.tds.getId().equals(tds.getId()))) {
			
				specOptionNotFound = false;
				
				for (TdsSpecOption tdsSpecOption: tds.getTdsSpecOptions()) {
					if (tdsSpecOptions.indexOf(tdsSpecOption)==-1) {
						specOptionNotFound = true;		
						break;
					} 
				}
				
				if ((!specOptionNotFound) && 
					(tdsSpecOptions.size()==tds.getTdsSpecOptions().size())) {
					errorMsg(ResourceBundle.getMessage("duplicateTds") + ": [" + tds.getIdentifier() + "]",null);
					validated=false;
					break;
				}
			}
		}
		
		if (!validated) {
			throw new Exception("");
		}
	}
	
	public List<TdsSpecOption> buildSelectedOptions() {
		//Preparando a lista de op��es.
		List<TdsSpecOption> tdsSpecOptions = new ArrayList<TdsSpecOption>();
		for (FilterModel filterModel : filters) {
			if(filterModel.getHeaderCheck() && filterModel.getValue()!=null){
				TdsSpecOption tdsSpecOption = tdsSpecOptionService.findByName(filterModel.getValue(), filterModel.getHeader());
				tdsSpecOptions.add(tdsSpecOption);
			}
		}
		
		return tdsSpecOptions;
	}
	
	public String save() throws Exception {
		
		String ret = "searchTds?faces-redirect=true";
		
		validateRequiredFields();
		
		List<TdsSpecOption> tdsSpecOptions = buildSelectedOptions();
		
		validateDuplicateTds(tdsSpecOptions);
		
		tds.setTdsSpecOptions(tdsSpecOptions);
		
		if (operation.equals("NEW")){
			//Identificando a Transa��o
			////addSession(Constantes.CODE_TRANSACTION, TransactionReview.CREATE_TDS.getCode());
			//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.CREATE_TDS.getDescription());

			tds.setStateModified(new Date(System.currentTimeMillis()));
			tds.setChangeVersion(0);
			tds.setChangeModified(new Date(System.currentTimeMillis()));
			tds.setChangeIdentifier(selectedChangeState);
			tds = tdsService.save(tds);
		}else{
			//Identificando a Transa��o
			//addSession(Constantes.CODE_TRANSACTION, TransactionReview.ALTER_TDS.getCode());
			//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.ALTER_TDS.getDescription());
			
			if (!selectedState.equals(tds.getState())) {
				tds.setStateModified(new Date(System.currentTimeMillis()));
			}
			
			String updateHashCode = generateHash(tds.getTdsSpecOptions());
			if (!currentHashCode.equals(updateHashCode)) {
				tds.setChangeVersion(tds.getChangeVersion() + 1);
				tds.setChangeModified(new Date(System.currentTimeMillis()));
				tds.setChangeIdentifier(selectedChangeState);
			}	
			
			tds = tdsService.update(tds);
		}
		
		cleanObjects();
		
		if (operation.equals("NEW")) {
			infoMsg(ResourceBundle.getMessage("tdsCreate"), null);
			ret = null;
		}else{
			setSessionMsgAtribute(ResourceBundle.getMessage("tdsUpdate"));
		}
		return ret;
    }
	
	public void cleanObjects(){
		tds = null;
		validateIdentifier = null;
		filters = null;
		tdsCopy = null;
		specOptions = null;
		tdsTemplates = null;
	}

	public String back() {
		cleanObjects();
		return "searchTds?faces-redirect=true";
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Boolean getValidateIdentifier() {
		return validateIdentifier;
	}

	public void setValidateIdentifier(Boolean validateIdentifier) {
		this.validateIdentifier = validateIdentifier;
	}

	public Tds getTds() {
		return tds;
	}

	public void setTds(Tds tds) {
		this.tds = tds;
	}

	public List<MeterModel> getModels() {
		return models;
	}

	public void setModels(List<MeterModel> models) {
		this.models = models;
	}

	public List<TdsSpecOption> getSpecOptions() {
		return specOptions;
	}

	public void setSpecOptions(List<TdsSpecOption> specOptions) {
		this.specOptions = specOptions;
	}

	public List<FilterModel> getFilters() {
		return filters;
	}

	public List<Tds> getTdsCopy() {
		return tdsCopy;
	}

	public void setTdsCopy(List<Tds> tdsCopy) {
		this.tdsCopy = tdsCopy;
	}

	public Tds getTdsToCopy() {
		return tdsToCopy;
	}

	public void setTdsToCopy(Tds tdsToCopy) {
		this.tdsToCopy = tdsToCopy;
	}

	public void setFilters(List<FilterModel> filters) {
		this.filters = filters;
	}
	
	public TdsStateIdentifier getSelectedState() {
		return selectedState;
	}

	public void setSelectedState(TdsStateIdentifier selectedState) {
		this.selectedState = selectedState;
	}

	public String getCurrentHashCode() {
		return currentHashCode;
	}

	public void setCurrentHashCode(String currentHashCode) {
		this.currentHashCode = currentHashCode;
	}

	public StateIdentifier[] getStates() {
		return StateIdentifier.values();
	}

	public String getSelectedChangeState() {
		return selectedChangeState;
	}

	public void setSelectedChangeState(String selectedChangeState) {
		this.selectedChangeState = selectedChangeState;
	}
	
	public List<TdsTemplate> getTdsTemplates() {
		return tdsTemplates;
	}

	public void setTdsTemplates(List<TdsTemplate> tdsTemplates) {
		this.tdsTemplates = tdsTemplates;
	}

	public MeterModel getMeterModel() {
		return meterModel;
	}

	public void setMeterModel(MeterModel meterModel) {
		this.meterModel = meterModel;
	}

}
