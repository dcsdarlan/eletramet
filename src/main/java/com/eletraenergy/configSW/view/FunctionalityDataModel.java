package com.eletraenergy.configSW.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.eletraenergy.configSW.model.Functionality;
  
public class FunctionalityDataModel extends ListDataModel<Functionality> implements SelectableDataModel<Functionality> {    
  
    public FunctionalityDataModel() {  
    }  
  
    public FunctionalityDataModel(List<Functionality> data) {  
        super(data);  
    }  
      
   public Functionality getRowData(String rowKey) {  
          
        @SuppressWarnings("unchecked")
		List<Functionality> datas = (List<Functionality>) getWrappedData();  
          
        for(Functionality data : datas) {  
            if(data.getId().equals(rowKey))  
                return data;  
        }  
          
        return null;  
    }  
  
    public Object getRowKey(Functionality data) {  
        return data.getId();  
    }  
}  
                     