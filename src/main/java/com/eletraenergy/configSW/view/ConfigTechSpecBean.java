package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.ConfigCommand;
import com.eletraenergy.configSW.model.ConfigModel;
import com.eletraenergy.configSW.model.ConfigSimulation;
import com.eletraenergy.configSW.model.ConfigTechSpec;
import com.eletraenergy.configSW.model.ItemOptionSpec;
import com.eletraenergy.configSW.model.ItemSpec;
import com.eletraenergy.configSW.model.ItemSpecCategory;
import com.eletraenergy.configSW.model.ItemSpecRuleDomain;
import com.eletraenergy.configSW.model.OptionType;
import com.eletraenergy.configSW.model.TdsModel;
import com.eletraenergy.configSW.service.ConfigSimulationService;
import com.eletraenergy.configSW.util.ResourceBundle;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;


/**
* Use case : Config Technical Specifications
* 
* @since   : Fev 07, 2017, 19:01:20
* @author  : raphael.ferreira
*/

@Component("configTechSpecBean")
@Scope("session")
public class ConfigTechSpecBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 8405467385841817203L;

	private TdsModel selectedTds;

	@Value("${url.service.configSpecByTDS}")
	private String urlServiceConfig;
	@Value("${url.service.configCommand}")
	private String urlServiceCommand;
	@Value("${url.service.userAuth}")
	private String user;
	@Value("${url.service.passwordAuth}")
	private String password;
	private int trying = 0;
	private String configCode;
	
	private ConfigTechSpec config;
	private List<ConfigModel> configModel;
	
	@Autowired
	private ConfigSimulationService configService;
	
	public void loadInit() {
	}
	
	public String loadTds() {
		String result = "configTechSpec?faces-redirect=true";
		if (selectedTds != null && selectedTds.getTds() != null){
			configCode = selectedTds.getConfigCode(); 
			trying = 0;
			Client c = Client.create();
			c.addFilter(new HTTPBasicAuthFilter(user, password));
		    WebResource wr = c.resource(urlServiceConfig + selectedTds.getTds());
		    try {
			    String json = wr.get(String.class);
			    Gson gson = new Gson();
			    config = gson.fromJson(json, new TypeToken<ConfigTechSpec>(){}.getType());
			    prepareConfigModel();
			    } catch (ClientHandlerException e) {
					errorMsg(ResourceBundle.getMessage("webServiceFail"), null);
					result = "simulation";
			}

		}else{
			errorMsg(ResourceBundle.getMessage("choiceTDS"), null);
			result = "simulation";
		}
		return result;
	}

	private void prepareConfigModel() {
		configModel = new ArrayList<ConfigModel>();
		for (ItemSpecCategory item : config.getListOfItemSpecCategory()) {
			String nm_TypeItemSpec = null;
		    for (ItemSpec itemSpec : item.getListOfItemSpec()) {
		    	OptionType optionType = null;
		    	List<String> options = new ArrayList<String>();
		    	String validation = null;
		    	String value = null;
		    	String type = null;
		    	String valMaxDigInt = null;
		    	String valMaxDigDec = null;
		    	String valMaxDig = null;
		    	String maxSel = null;
		    	String valMin = null;
		    	String valMax = null;
		    	for (ItemOptionSpec itemOptionSpec : itemSpec.getListOfItemOptionSpec()) {
		    		options.add(itemOptionSpec.getNM_ItemOptionSpec());
				}
		    	for (ItemSpecRuleDomain itemSpecRuleDomain : itemSpec.getListOfItemSpecRuleDomain()) {
		    		//OptionType
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("RD")){
		    			optionType = OptionType.Read;
		    			value = options.get(0);
		    		}
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("MonoSel")){
		    			optionType = OptionType.SelectOne;
		    		}
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("MultiSel")){
		    			optionType = OptionType.SelectMulti;
		    		}
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("Str")){
		    			optionType = OptionType.InputText;
		    		}
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("Dec")){
		    			type = itemSpecRuleDomain.getVL_ItemSpecRuleDomain();
		    			optionType = OptionType.InputDecimal;
		    		}
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("Int")){
		    			type = itemSpecRuleDomain.getVL_ItemSpecRuleDomain();
		    			optionType = OptionType.InputNumber;
		    		}

		    		//Validation
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("ValMax")){
		    			valMax = itemSpecRuleDomain.getVL_ItemSpecRuleDomain();
		    		}
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("ValMin")){
		    			valMin = itemSpecRuleDomain.getVL_ItemSpecRuleDomain();
		    		}
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("MaxSel")){
		    			maxSel = itemSpecRuleDomain.getVL_ItemSpecRuleDomain();
		    		}
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("ValMaxDigInt")){
		    			valMaxDigInt = itemSpecRuleDomain.getVL_ItemSpecRuleDomain();
		    		}
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("ValMaxDigDec")){
		    			valMaxDigDec = itemSpecRuleDomain.getVL_ItemSpecRuleDomain();
		    		}
		    		if(itemSpecRuleDomain.getID_ItemSpecRuleDomain().equals("ValMaxDig")){
		    			valMaxDig = itemSpecRuleDomain.getVL_ItemSpecRuleDomain();
		    		}
		    		
					
				}
		    	String category = "";
		    	if (nm_TypeItemSpec == null || !nm_TypeItemSpec.equals(item.getNM_TypeItemSpec())) {
			    	nm_TypeItemSpec = item.getNM_TypeItemSpec();
			    	category = nm_TypeItemSpec;
				}else{
					category = "";
				}
		    	boolean isRequired = itemSpec.getGenerateCommandIdentifier().equals("y");
				configModel.add(new ConfigModel(category, itemSpec.getNM_ItemSpec(), options, value, optionType, validation, type, valMaxDigInt, valMaxDigDec, valMaxDig, maxSel, valMin, valMax, isRequired));
			}
			
		}
	}

	public String loadCommands() {
		String result = "configCommand?faces-redirect=true";
		
		if (trying == 1 || valid(configModel)) {
		    Gson gson = new Gson();

		    //Pegar os valores digitados e alterar o json para deixar as op��es apena com valor selecionado.
		    for (ConfigModel cm : configModel) {
		    	for (ItemSpecCategory item : config.getListOfItemSpecCategory()) {
			    	for (ItemSpec i : item.getListOfItemSpec()) {
						if (i.getNM_ItemSpec().equals(cm.getConfiguration())){
							i.setItemSpecValue(cm.getValue());
							
							if (cm.getOptionType().equals(OptionType.InputDecimal)){
								i.setItemSpecValue(cm.getValueInt() +"." + cm.getValueDec());
							}

							if ((cm.getOptionType().equals(OptionType.SelectOne) || cm.getOptionType().equals(OptionType.SelectMulti)) && cm.getOptions()!=null && !cm.getOptions().isEmpty()){
								ArrayList<ItemOptionSpec> itens = new ArrayList<ItemOptionSpec>();
								i.setItemSpecValue("");
								if (cm.getValues()!=null && !cm.getValues().isEmpty()) {
									for(String s : cm.getValues()){
										for (ItemOptionSpec ios : i.getListOfItemOptionSpec()) {
											if(ios.getNM_ItemOptionSpec().equals(s)){
												itens.add(ios);
											}
										}
									}
								} else {
									for (ItemOptionSpec ios : i.getListOfItemOptionSpec()) {
										if(ios.getNM_ItemOptionSpec().equals(cm.getValue())){
											itens.add(ios);
											break;
										}
									}
								}
								i.setListOfItemOptionSpec(itens);
							}
						}
					}
				}
			}
		    String configJson = gson.toJson(config);

		    ConfigSimulation simulation = new ConfigSimulation(new Date(), Integer.parseInt(configCode), configJson, null );
		    simulation = configService.save(simulation);

			Client c = Client.create();
			c.addFilter(new HTTPBasicAuthFilter(user, password));
		    WebResource wr = c.resource(urlServiceCommand + simulation.getId());
		    try {
			    String commandJson = wr.get(String.class);
			    ConfigCommand configCommand = gson.fromJson(commandJson, new TypeToken<ConfigCommand>(){}.getType());
			    simulation.setConfigCommand(commandJson);
				addSession("configCommand", configCommand);
			} catch (UniformInterfaceException e) {
				errorMsg(ResourceBundle.getMessage("webServiceFail"), null);
				return "configTechSpec";
			}

		    simulation = configService.update(simulation);
			addSession("configTechSpec", config);

		}else{
			result = "configTechSpec";
			trying = trying + 1;
		}
		
		return result;
	}
	
	private boolean valid(List<ConfigModel> configModelToValidate) {
		for (ConfigModel configModel : configModelToValidate) {

			if (configModel.getValidation() != null && !configModel.getValidation().isEmpty()){
				if (!configModel.getValue().matches(configModel.getValidation())){
					configModel.setValid(false);
					errorMsg(ResourceBundle.getMessage("regexError") + " " + configModel.getConfiguration() + " " + ResourceBundle.getMessage("is") + " " + configModel.getValidation(), null);
				}
			}

			if (configModel.getValue()!= null && !configModel.getValue().isEmpty() && configModel.getMaxSel() != null && !configModel.getMaxSel().isEmpty()){
				if (configModel.getValues().size() > Integer.parseInt(configModel.getMaxSel())){
					configModel.setValid(false);
					errorMsg(ResourceBundle.getMessage("maxSelError") + " " + configModel.getConfiguration() + " " + ResourceBundle.getMessage("is") + " " + configModel.getMaxSel() + ", " + ResourceBundle.getMessage("maxSelErrorObs")+ " "  + configModel.getValues().size() + ".", null);
				}
			}
			if (configModel.getValue()!= null && !configModel.getValue().isEmpty() && configModel.getValMin() != null && !configModel.getValMin().isEmpty()){
				if (Integer.parseInt(configModel.getValue()) < Integer.parseInt(configModel.getValMin())) {
					configModel.setValid(false);
					errorMsg(ResourceBundle.getMessage("minValError") + " " + configModel.getConfiguration() + " " + ResourceBundle.getMessage("is") + " " + configModel.getValMin() + ", " + ResourceBundle.getMessage("minValErrorObs")+ " "  + configModel.getValue() + ".", null);
				}
			}
			if (configModel.getValue()!= null && !configModel.getValue().isEmpty() && configModel.getValMax() != null && !configModel.getValMax().isEmpty()){
				if (Integer.parseInt(configModel.getValue()) > Integer.parseInt(configModel.getValMax())) {
					configModel.setValid(false);
					errorMsg(ResourceBundle.getMessage("maxValError") + " " + configModel.getConfiguration() + " " + ResourceBundle.getMessage("is") + " " + configModel.getValMax() + ", " + ResourceBundle.getMessage("maxValErrorObs")+ " "  + configModel.getValue() + ".", null);
				}
			}
			
		}
		
		boolean isValid = true;
		
		for (ConfigModel configModel : configModelToValidate) {
			if (!configModel.getValid()){
				isValid = false;
			}
		}

		return isValid;
	}

	public TdsModel getSelectedTds() {
		return selectedTds;
	}

	public void setSelectedTds(TdsModel selectedTds) {
		this.selectedTds = selectedTds;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ConfigTechSpec getConfig() {
		return config;
	}

	public void setConfig(ConfigTechSpec config) {
		this.config = config;
	}

	public List<ConfigModel> getConfigModel() {
		return configModel;
	}

	public void setConfigModel(List<ConfigModel> configModel) {
		this.configModel = configModel;
	}

}