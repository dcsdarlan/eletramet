package com.eletraenergy.configSW.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.eletraenergy.configSW.model.ConfigCommand;
import com.eletraenergy.configSW.model.ConfigModel;
import com.eletraenergy.configSW.model.ConfigSimulation;
import com.eletraenergy.configSW.model.ConfigTechSpec;
import com.eletraenergy.configSW.model.ItemOptionSpec;
import com.eletraenergy.configSW.model.ItemSpec;
import com.eletraenergy.configSW.model.ItemSpecCategory;
import com.eletraenergy.configSW.model.ManufactStep2;
import com.eletraenergy.configSW.model.MeterCommand;
import com.eletraenergy.configSW.model.OptionType;
import com.eletraenergy.configSW.service.ConfigSimulationService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
 

public class LazyConfigSimulationDataModel extends LazyDataModel<ConfigSimulation>{

	private static final long serialVersionUID = 6392394292277085396L;
	private List<ConfigSimulation> simulations;
	private ConfigSimulationService configSimulationService;
	private ConfigSimulation configSimulationFilter;
     
    public LazyConfigSimulationDataModel(ConfigSimulationService configSimulationService, ConfigSimulation configSimulationFilter) {
		super();
		this.configSimulationService = configSimulationService;
		this.configSimulationFilter = configSimulationFilter;
	}

	@Override
    public ConfigSimulation getRowData(String rowKey) {
        for(ConfigSimulation c : simulations) {
            if(c.getId().equals(rowKey))
                return c;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(ConfigSimulation user) {
        return user.getId();
    }
 
    @Override
    public List<ConfigSimulation> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
 
    	configSimulationFilter.setSortField(sortField);
    	configSimulationFilter.setSortOrder(sortOrder);
		
    	List<ConfigSimulation> data = configSimulationService.search(configSimulationFilter, first, pageSize);

    	Gson gson = new Gson();
    	for (ConfigSimulation configSimulation : data) {
			String json = configSimulation.getConfigTechSpec();
		    ConfigTechSpec config = gson.fromJson(json, new TypeToken<ConfigTechSpec>(){}.getType());
		    List<ConfigModel> configModel = prepareConfigModel(config);
		    configSimulation.setNM_MeterFamily(config.getNM_MeterFamily());
		    configSimulation.setNM_MeterModel(config.getNM_MeterModel());
		    configSimulation.setTDS_Number(config.getTDS_Number());
			configSimulation.setConfigModel(configModel);

			json = configSimulation.getConfigCommand();
			ConfigCommand command = gson.fromJson(json, new TypeToken<ConfigCommand>(){}.getType());
			if (command!=null){
				ArrayList<MeterCommand> meterCommands = new ArrayList<MeterCommand>();
				for (ManufactStep2 manufactStep : command.getListOfManufactStep()) {
					String manufactStepName = manufactStep.getManufactStepName();
					for (MeterCommand meterCommand : manufactStep.getListOfMeterCommand()) {
						meterCommand.setStage(manufactStepName);
						meterCommands.add(meterCommand);
					}
				}
				configSimulation.setMeterCommands(meterCommands);
			}
		}

        //rowCount
        int dataSize = configSimulationService.amount(configSimulationFilter);
        this.setRowCount(dataSize);
 
        return data;
    }

	private List<ConfigModel> prepareConfigModel(ConfigTechSpec configTechSpec) {
		List<ConfigModel> configModel = new ArrayList<ConfigModel>();
		for (ItemSpecCategory item : configTechSpec.getListOfItemSpecCategory()) {
			String nm_TypeItemSpec = null;
		    for (ItemSpec itemSpec : item.getListOfItemSpec()) {
		    	OptionType optionType = OptionType.Read;;
		    	List<String> options = new ArrayList<String>();
		    	String validation = null;
		    	String type = null;
		    	String valMaxDigInt = null;
		    	String valMaxDigDec = null;
		    	String valMaxDig = null;
		    	String maxSel = null;
		    	String valMin = null;
		    	String valMax = null;
		    	boolean isRequired = false;
		    	String value = "";
		    	if (itemSpec.getItemSpecValue()==null || itemSpec.getItemSpecValue().isEmpty()){
		    		if (itemSpec.getListOfItemOptionSpec()!=null && !itemSpec.getListOfItemOptionSpec().isEmpty()){
				    	for (ItemOptionSpec i : itemSpec.getListOfItemOptionSpec()) {
							value = value + i.getNM_ItemOptionSpec() + ", ";
						}
				    	value = value.substring(0, value.length()-2);
		    		}
		    	}else{
			    	value = itemSpec.getItemSpecValue();
		    	}
		    		
    			
		    	String category = "";
		    	if (nm_TypeItemSpec == null || !nm_TypeItemSpec.equals(item.getNM_TypeItemSpec())) {
			    	nm_TypeItemSpec = item.getNM_TypeItemSpec();
			    	category = nm_TypeItemSpec;
				}else{
					category = "";
				}
				configModel.add(new ConfigModel(category, itemSpec.getNM_ItemSpec(), options, value, optionType, validation, type, valMaxDigInt, valMaxDigDec, valMaxDig, maxSel, valMin, valMax, isRequired));
			}
			
		}
		return configModel;
	}	
    
}