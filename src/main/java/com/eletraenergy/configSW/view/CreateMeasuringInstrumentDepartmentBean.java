package com.eletraenergy.configSW.view;

import com.eletraenergy.configSW.model.MeasuringInstrumentDepartment;
import com.eletraenergy.configSW.model.User;
import com.eletraenergy.configSW.service.MeasuringInstrumentDepartmentService;
import com.eletraenergy.configSW.service.UserService;
import com.eletraenergy.configSW.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;

@Component("createMeasuringInstrumentDepartmentBean")
@Scope("view")
public class CreateMeasuringInstrumentDepartmentBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -6834442723163022371L;

	public static Logger logger = Logger.getLogger(CreateMeasuringInstrumentDepartmentBean.class);

	@Autowired
	private MeasuringInstrumentDepartmentService measuringInstrumentDepartmentService;

	@Autowired
	private UserService userService;
	
	private List<MeasuringInstrumentDepartment> measuringInstrumentDepartments;
	private List<User> users;
	private MeasuringInstrumentDepartment measuringInstrumentDepartmentFilter;
	
	public void loadInit() {
		users = userService.findAll();
		if (measuringInstrumentDepartmentFilter == null){
			measuringInstrumentDepartmentFilter = new MeasuringInstrumentDepartment();
			measuringInstrumentDepartmentFilter.setSortField("name");
			measuringInstrumentDepartmentFilter.setSortOrder(SortOrder.ASCENDING);
			
			search();	
		}
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public void addMeasuringInstrumentDepartment(ActionEvent actionEvent) {
		MeasuringInstrumentDepartment measuringInstrumentDepartment = new MeasuringInstrumentDepartment();
		measuringInstrumentDepartments.add(0,measuringInstrumentDepartment);
    }

	public void save(ActionEvent actionEvent) {
		if (measuringInstrumentDepartments.size()> 0){
			for (MeasuringInstrumentDepartment measuringInstrumentDepartment : measuringInstrumentDepartments) {
				//Esse teste eh no caso do usuario clicar em novo e salvar sem preencher nada. Essas linhas ser�o ignoradas.
				if (measuringInstrumentDepartment.getName() != null && !measuringInstrumentDepartment.getName().isEmpty()){
					if (measuringInstrumentDepartment.getId()==null){
						//Identificando a Transa��o
						//addSession(Constantes.CODE_TRANSACTION, TransactionReview.CREATE_LINE.getCode());
						//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.CREATE_LINE.getDescription());
						
						measuringInstrumentDepartmentService.save(measuringInstrumentDepartment);
					}else{
						//Identificando a Transa��o
						//addSession(Constantes.CODE_TRANSACTION, TransactionReview.ALTER_LINE.getCode());
						//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.ALTER_LINE.getDescription());

						measuringInstrumentDepartmentService.update(measuringInstrumentDepartment);
					}
				}
			}
			search();
			infoMsg(ResourceBundle.getMessage("measuringInstrumentDepartmentSave"), null);
		}
    }

	public void checkName(MeasuringInstrumentDepartment measuringInstrumentDepartment) {
		MeasuringInstrumentDepartment checkName = measuringInstrumentDepartmentService.checkName(measuringInstrumentDepartment.getName());
		if (checkName != null){
	      if ((measuringInstrumentDepartment.getId()!=null && !checkName.getId().equals(measuringInstrumentDepartment.getId())) || (measuringInstrumentDepartment.getId()==null && checkName != null)){
	    	  errorMsg(ResourceBundle.getMessage("checkName") + System.lineSeparator() + "[" +  measuringInstrumentDepartment.getName() + "]" , null);
	    	  measuringInstrumentDepartment.setName(null);
	      }
		}
	}
	
	public void search(){	
		measuringInstrumentDepartments = measuringInstrumentDepartmentService.search(measuringInstrumentDepartmentFilter, null, null);
	}

	public void remove(MeasuringInstrumentDepartment measuringInstrumentDepartmentSelected){
		//Identificando a Transa��o
		//addSession(Constantes.CODE_TRANSACTION, TransactionReview.DELETE_LINE.getCode());
		//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.DELETE_LINE.getDescription());

		measuringInstrumentDepartmentService.remove(measuringInstrumentDepartmentSelected);
		measuringInstrumentDepartmentFilter = new MeasuringInstrumentDepartment();
		measuringInstrumentDepartmentFilter.setSortField("name");
		measuringInstrumentDepartmentFilter.setSortOrder(SortOrder.ASCENDING);
		search();
	    infoMsg(ResourceBundle.getMessage("measuringInstrumentDepartmentDelete"),"");
	}

	public List<MeasuringInstrumentDepartment> getMeasuringInstrumentDepartments() {
		return measuringInstrumentDepartments;
	}

	public MeasuringInstrumentDepartment getMeasuringInstrumentDepartmentFilter() {
		return measuringInstrumentDepartmentFilter;
	}

	public void setMeasuringInstrumentDepartments(List<MeasuringInstrumentDepartment> measuringInstrumentDepartments) {
		this.measuringInstrumentDepartments = measuringInstrumentDepartments;
	}

	public void setMeasuringInstrumentDepartmentFilter(MeasuringInstrumentDepartment measuringInstrumentDepartmentFilter) {
		this.measuringInstrumentDepartmentFilter = measuringInstrumentDepartmentFilter;
	}

}
