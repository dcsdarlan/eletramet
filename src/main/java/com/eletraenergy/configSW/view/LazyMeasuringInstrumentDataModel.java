package com.eletraenergy.configSW.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import com.eletraenergy.configSW.model.MeasuringInstrument;
import com.eletraenergy.configSW.service.MeasuringInstrumentService;

public class LazyMeasuringInstrumentDataModel extends LazyDataModel<MeasuringInstrument>{

	private static final long serialVersionUID = -2625265806840013215L;
	private List<MeasuringInstrument> measuringInstrument;
	private MeasuringInstrumentService measuringInstrumentService;
	private MeasuringInstrument measuringInstrumentFilter;
	
    public LazyMeasuringInstrumentDataModel(MeasuringInstrumentService measuringInstrumentService, MeasuringInstrument measuringInstrumentFilter) {
		super();
		this.measuringInstrumentService = measuringInstrumentService;
		this.measuringInstrumentFilter = measuringInstrumentFilter;
	}

	@Override
    public MeasuringInstrument getRowData(String rowKey) {
        for(MeasuringInstrument s : measuringInstrument) {
            if(s.getId().equals(rowKey))
                return s;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(MeasuringInstrument measuringInstrument) {
        return measuringInstrument.getId();
    }
 
    @Override
    public List<MeasuringInstrument> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<MeasuringInstrument> data = new ArrayList<MeasuringInstrument>();
 
		measuringInstrumentFilter.setSortField(sortField);
		measuringInstrumentFilter.setSortOrder(sortOrder);

		data = measuringInstrumentService.search(measuringInstrumentFilter, first,
				pageSize);

        //rowCount
        int dataSize = measuringInstrumentService.amount(measuringInstrumentFilter);
        this.setRowCount(dataSize);
 
        return data;
    }
}