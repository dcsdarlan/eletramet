package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.List;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.MeasuringInstrumentManufacturer;
import com.eletraenergy.configSW.model.MeasuringInstrumentModel;
import com.eletraenergy.configSW.model.TransactionReview;
import com.eletraenergy.configSW.service.MeasuringInstrumentManufacturerService;
import com.eletraenergy.configSW.service.MeasuringInstrumentModelService;
import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.ResourceBundle;

@Component("createMeasuringInstrumentModelBean")
@Scope("view")
public class CreateMeasuringInstrumentModelBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -6834442723163022371L;

	public static Logger logger = Logger.getLogger(CreateMeasuringInstrumentModelBean.class);

	@Autowired
	private MeasuringInstrumentModelService measuringInstrumentModelService;
	
	@Autowired
	private MeasuringInstrumentManufacturerService measuringInstrumentManufacturerService;
	
	private List<MeasuringInstrumentModel> measuringInstrumentModels;
	private MeasuringInstrumentModel measuringInstrumentModelFilter;
	private List<MeasuringInstrumentManufacturer> measuringInstrumentManufacturers;
	
	public void loadInit() {
		if (measuringInstrumentModelFilter == null){
			measuringInstrumentModelFilter = new MeasuringInstrumentModel();
			measuringInstrumentModelFilter.setSortField("name");
			measuringInstrumentModelFilter.setSortOrder(SortOrder.ASCENDING);
			search();	
		}
		measuringInstrumentManufacturers = measuringInstrumentManufacturerService.findAll();
	}

	public void addMeasuringInstrumentModel(ActionEvent actionEvent) {
		MeasuringInstrumentModel measuringInstrumentModel = new MeasuringInstrumentModel();
		measuringInstrumentModels.add(0,measuringInstrumentModel);
    }

	public void save(ActionEvent actionEvent) {
		if (measuringInstrumentModels.size()> 0){
			for (MeasuringInstrumentModel measuringInstrumentModel : measuringInstrumentModels) {
				//Esse teste eh no caso do usuario clicar em novo e salvar sem preencher nada. Essas linhas ser�o ignoradas.
				if (measuringInstrumentModel.getName() != null && !measuringInstrumentModel.getName().isEmpty()){
					if (measuringInstrumentModel.getId()==null){
						//Identificando a Transa��o
						//addSession(Constantes.CODE_TRANSACTION, TransactionReview.CREATE_LINE.getCode());
						//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.CREATE_LINE.getDescription());
						
						measuringInstrumentModelService.save(measuringInstrumentModel);
					}else{
						//Identificando a Transa��o
						//addSession(Constantes.CODE_TRANSACTION, TransactionReview.ALTER_LINE.getCode());
						//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.ALTER_LINE.getDescription());

						measuringInstrumentModelService.update(measuringInstrumentModel);
					}
				}
			}
			search();
			infoMsg(ResourceBundle.getMessage("measuringInstrumentModelSave"), null);
		}
    }

	public void checkName(MeasuringInstrumentModel measuringInstrumentModel) {
		MeasuringInstrumentModel checkName = measuringInstrumentModelService.checkName(measuringInstrumentModel.getName());
		if (checkName != null){
	      if ((measuringInstrumentModel.getId()!=null && !checkName.getId().equals(measuringInstrumentModel.getId())) || (measuringInstrumentModel.getId()==null && checkName != null)){
	    	  errorMsg(ResourceBundle.getMessage("checkName") + System.lineSeparator() + "[" +  measuringInstrumentModel.getName() + "]" , null);
	    	  measuringInstrumentModel.setName(null);
	      }
		}
	}
	
	public void search(){	
		measuringInstrumentModels = measuringInstrumentModelService.search(measuringInstrumentModelFilter, null, null);
	}

	public void remove(MeasuringInstrumentModel measuringInstrumentModelSelected){
		//Identificando a Transa��o
		//addSession(Constantes.CODE_TRANSACTION, TransactionReview.DELETE_LINE.getCode());
		//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.DELETE_LINE.getDescription());

		measuringInstrumentModelService.remove(measuringInstrumentModelSelected);
		measuringInstrumentModelFilter = new MeasuringInstrumentModel();
		measuringInstrumentModelFilter.setSortField("name");
		measuringInstrumentModelFilter.setSortOrder(SortOrder.ASCENDING);
		search();
	    infoMsg(ResourceBundle.getMessage("measuringInstrumentModelDelete"),"");
	}

	public List<MeasuringInstrumentModel> getMeasuringInstrumentModels() {
		return measuringInstrumentModels;
	}

	public MeasuringInstrumentModel getMeasuringInstrumentModelFilter() {
		return measuringInstrumentModelFilter;
	}

	public void setMeasuringInstrumentModels(List<MeasuringInstrumentModel> measuringInstrumentModels) {
		this.measuringInstrumentModels = measuringInstrumentModels;
	}

	public void setMeasuringInstrumentModelFilter(MeasuringInstrumentModel measuringInstrumentModelFilter) {
		this.measuringInstrumentModelFilter = measuringInstrumentModelFilter;
	}

	public List<MeasuringInstrumentManufacturer> getMeasuringInstrumentManufacturers() {
		return measuringInstrumentManufacturers;
	}

	public void setMeasuringInstrumentManufacturers(List<MeasuringInstrumentManufacturer> measuringInstrumentManufacturers) {
		this.measuringInstrumentManufacturers = measuringInstrumentManufacturers;
	}
}
