package com.eletraenergy.configSW.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import com.eletraenergy.configSW.model.TdsTemplate;
import com.eletraenergy.configSW.service.TdsTemplateService;

public class LazyTdsTemplateDataModel extends LazyDataModel<TdsTemplate>{

	private static final long serialVersionUID = -2625265806840013215L;
	private List<TdsTemplate> tdsTemplates;
	private TdsTemplateService tdsTemplateService;
	private TdsTemplate tdsTemplate;
	
    public LazyTdsTemplateDataModel(TdsTemplateService tdsTemplateService, TdsTemplate tdsTemplate) {
		super();
		this.tdsTemplateService = tdsTemplateService;
		this.tdsTemplate = tdsTemplate;
	}

	@Override
    public TdsTemplate getRowData(String rowKey) {
        for(TdsTemplate s : tdsTemplates) {
            if(s.getId().equals(rowKey))
                return s;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(TdsTemplate tdsTemplate) {
        return tdsTemplate.getId();
    }
 
    @Override
    public List<TdsTemplate> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<TdsTemplate> data = new ArrayList<TdsTemplate>();
 
        tdsTemplate.setSortField(sortField);
        tdsTemplate.setSortOrder(sortOrder);

		data = tdsTemplateService.search(tdsTemplate, first,
				pageSize);

        //rowCount
        int dataSize = tdsTemplateService.amount(tdsTemplate);
        this.setRowCount(dataSize);
 
        return data;
    }
}