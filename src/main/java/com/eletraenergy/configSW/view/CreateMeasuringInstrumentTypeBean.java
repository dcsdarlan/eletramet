package com.eletraenergy.configSW.view;

import java.io.Serializable;
import java.util.List;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eletraenergy.configSW.model.MeasuringInstrumentType;
import com.eletraenergy.configSW.model.TransactionReview;
import com.eletraenergy.configSW.service.MeasuringInstrumentTypeService;
import com.eletraenergy.configSW.util.Constantes;
import com.eletraenergy.configSW.util.ResourceBundle;

@Component("createMeasuringInstrumentTypeBean")
@Scope("view")
public class CreateMeasuringInstrumentTypeBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -6834442723163022371L;

	public static Logger logger = Logger.getLogger(CreateMeasuringInstrumentTypeBean.class);

	@Autowired
	private MeasuringInstrumentTypeService measuringInstrumentTypeService;
	
	private List<MeasuringInstrumentType> measuringInstrumentTypes;
	private MeasuringInstrumentType measuringInstrumentTypeFilter;
	
	public void loadInit() {
		if (measuringInstrumentTypeFilter == null){
			measuringInstrumentTypeFilter = new MeasuringInstrumentType();
			measuringInstrumentTypeFilter.setSortField("name");
			measuringInstrumentTypeFilter.setSortOrder(SortOrder.ASCENDING);
			
			search();	
		}
	}

	public void addMeasuringInstrumentType(ActionEvent actionEvent) {
		MeasuringInstrumentType measuringInstrumentType = new MeasuringInstrumentType();
		measuringInstrumentTypes.add(0,measuringInstrumentType);
    }

	public void save(ActionEvent actionEvent) {
		if (measuringInstrumentTypes.size()> 0){
			for (MeasuringInstrumentType measuringInstrumentType : measuringInstrumentTypes) {
				//Esse teste eh no caso do usuario clicar em novo e salvar sem preencher nada. Essas linhas ser�o ignoradas.
				if (measuringInstrumentType.getName() != null && !measuringInstrumentType.getName().isEmpty()){
					if (measuringInstrumentType.getId()==null){
						//Identificando a Transa��o
						//addSession(Constantes.CODE_TRANSACTION, TransactionReview.CREATE_LINE.getCode());
						//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.CREATE_LINE.getDescription());
						
						measuringInstrumentTypeService.save(measuringInstrumentType);
					}else{
						//Identificando a Transa��o
						//addSession(Constantes.CODE_TRANSACTION, TransactionReview.ALTER_LINE.getCode());
						//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.ALTER_LINE.getDescription());

						measuringInstrumentTypeService.update(measuringInstrumentType);
					}
				}
			}
			search();
			infoMsg(ResourceBundle.getMessage("measuringInstrumentTypeSave"), null);
		}
    }

	public void checkName(MeasuringInstrumentType measuringInstrumentType) {
		MeasuringInstrumentType checkName = measuringInstrumentTypeService.checkName(measuringInstrumentType.getName());
		if (checkName != null){
	      if ((measuringInstrumentType.getId()!=null && !checkName.getId().equals(measuringInstrumentType.getId())) || (measuringInstrumentType.getId()==null && checkName != null)){
	    	  errorMsg(ResourceBundle.getMessage("checkName") + System.lineSeparator() + "[" +  measuringInstrumentType.getName() + "]" , null);
	    	  measuringInstrumentType.setName(null);
	      }
		}
	}
	
	public void search(){	
		measuringInstrumentTypes = measuringInstrumentTypeService.search(measuringInstrumentTypeFilter, null, null);
	}

	public void remove(MeasuringInstrumentType measuringInstrumentTypeSelected){
		//Identificando a Transa��o
		//addSession(Constantes.CODE_TRANSACTION, TransactionReview.DELETE_LINE.getCode());
		//addSession(Constantes.TITLE_TRANSACTION, TransactionReview.DELETE_LINE.getDescription());

		measuringInstrumentTypeService.remove(measuringInstrumentTypeSelected);
		measuringInstrumentTypeFilter = new MeasuringInstrumentType();
		measuringInstrumentTypeFilter.setSortField("name");
		measuringInstrumentTypeFilter.setSortOrder(SortOrder.ASCENDING);
		search();
	    infoMsg(ResourceBundle.getMessage("measuringInstrumentTypeDelete"),"");
	}

	public List<MeasuringInstrumentType> getMeasuringInstrumentTypes() {
		return measuringInstrumentTypes;
	}

	public MeasuringInstrumentType getMeasuringInstrumentTypeFilter() {
		return measuringInstrumentTypeFilter;
	}

	public void setMeasuringInstrumentTypes(List<MeasuringInstrumentType> measuringInstrumentTypes) {
		this.measuringInstrumentTypes = measuringInstrumentTypes;
	}

	public void setMeasuringInstrumentTypeFilter(MeasuringInstrumentType measuringInstrumentTypeFilter) {
		this.measuringInstrumentTypeFilter = measuringInstrumentTypeFilter;
	}

}
