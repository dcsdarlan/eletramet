PGDMP                          v            mecsw    9.6.8    10.3 b    
	           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            	           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            	           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            	           1262    49378    mecsw    DATABASE     �   CREATE DATABASE mecsw WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Portuguese_Brazil.1252' LC_CTYPE = 'Portuguese_Brazil.1252';
    DROP DATABASE mecsw;
             postgres    false                        2615    49379    mecsw    SCHEMA        CREATE SCHEMA mecsw;
    DROP SCHEMA mecsw;
             mecsw    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            	           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12387    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            	           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    49380 	   sq_review    SEQUENCE     q   CREATE SEQUENCE mecsw.sq_review
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE mecsw.sq_review;
       mecsw       mecsw    false    6            �            1259    57556    tb_calibration_laboratory    TABLE     �  CREATE TABLE mecsw.tb_calibration_laboratory (
    code integer NOT NULL,
    name character varying NOT NULL,
    cnpj character varying(14) NOT NULL,
    email character varying(1),
    contact character varying NOT NULL,
    place character varying NOT NULL,
    number character varying(50) NOT NULL,
    complement character varying,
    neighborhood character varying NOT NULL,
    city character varying(1) NOT NULL,
    country character varying(2) NOT NULL,
    cep character varying(8) NOT NULL
);
 ,   DROP TABLE mecsw.tb_calibration_laboratory;
       mecsw         postgres    false    6            �            1259    57564 "   tb_calibration_laboratory_telphone    TABLE     �   CREATE TABLE mecsw.tb_calibration_laboratory_telphone (
    code integer NOT NULL,
    calibration_laboratory_code integer NOT NULL,
    ddd character varying(2) NOT NULL,
    number character varying(9) NOT NULL
);
 5   DROP TABLE mecsw.tb_calibration_laboratory_telphone;
       mecsw         postgres    false    6            �            1259    49382    tb_functionality    TABLE     �   CREATE TABLE mecsw.tb_functionality (
    code bigint NOT NULL,
    description character varying(255),
    mnemonic character varying(255),
    order_function integer
);
 #   DROP TABLE mecsw.tb_functionality;
       mecsw         mecsw    false    6            �            1259    49388    tb_measuring_instrument    TABLE     G  CREATE TABLE mecsw.tb_measuring_instrument (
    code integer NOT NULL,
    specification_identifier character varying(30) NOT NULL,
    measuring_instrument_type_code integer NOT NULL,
    measuring_instrument_model_code integer NOT NULL,
    measuring_instrument_place_use_code integer NOT NULL,
    calibration_frequency_weeks integer NOT NULL,
    last_calibration_date timestamp without time zone NOT NULL,
    next_calibration_date timestamp without time zone NOT NULL,
    obsolete_identifier character varying(3) NOT NULL,
    damaged_identifier character varying(3) NOT NULL,
    state_identifier character varying(30) NOT NULL,
    state_timestamp timestamp without time zone NOT NULL,
    description text,
    version bigint NOT NULL,
    calibration_frequency_days integer,
    measuring_instrument_department_code integer
);
 *   DROP TABLE mecsw.tb_measuring_instrument;
       mecsw         mecsw    false    6            �            1259    49394    tb_measuring_instrument_aud    TABLE       CREATE TABLE mecsw.tb_measuring_instrument_aud (
    code integer NOT NULL,
    specification_identifier character varying(30) NOT NULL,
    measuring_instrument_type_code integer NOT NULL,
    measuring_instrument_model_code integer NOT NULL,
    measuring_instrument_place_use_code integer NOT NULL,
    calibration_frequency_weeks integer NOT NULL,
    last_calibration_date timestamp without time zone NOT NULL,
    next_calibration_date timestamp without time zone NOT NULL,
    obsolete_identifier character varying(3) NOT NULL,
    damaged_identifier character varying(3) NOT NULL,
    state_identifier character varying(30) NOT NULL,
    state_timestamp timestamp without time zone NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint
);
 .   DROP TABLE mecsw.tb_measuring_instrument_aud;
       mecsw         postgres    false    6            �            1259    57352 "   tb_measuring_instrument_department    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_department (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    version bigint
);
 5   DROP TABLE mecsw.tb_measuring_instrument_department;
       mecsw         postgres    false    6            �            1259    57362 &   tb_measuring_instrument_department_aud    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_department_aud (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint
);
 9   DROP TABLE mecsw.tb_measuring_instrument_department_aud;
       mecsw         postgres    false    6            �            1259    49400 $   tb_measuring_instrument_manufacturer    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_manufacturer (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    version bigint
);
 7   DROP TABLE mecsw.tb_measuring_instrument_manufacturer;
       mecsw         mecsw    false    6            �            1259    49406 (   tb_measuring_instrument_manufacturer_aud    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_manufacturer_aud (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint
);
 ;   DROP TABLE mecsw.tb_measuring_instrument_manufacturer_aud;
       mecsw         mecsw    false    6            �            1259    49412 5   tb_measuring_instrument_measuring_instrument_standard    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_measuring_instrument_standard (
    measuring_instrument_code bigint NOT NULL,
    measuring_instrument_standard_code bigint NOT NULL
);
 H   DROP TABLE mecsw.tb_measuring_instrument_measuring_instrument_standard;
       mecsw         mecsw    false    6            �            1259    49415    tb_measuring_instrument_model    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_model (
    code integer NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    measuring_instrument_manufacturer_code integer NOT NULL,
    version bigint NOT NULL
);
 0   DROP TABLE mecsw.tb_measuring_instrument_model;
       mecsw         mecsw    false    6            �            1259    49421 !   tb_measuring_instrument_model_aud    TABLE       CREATE TABLE mecsw.tb_measuring_instrument_model_aud (
    code integer NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    measuring_instrument_manufacturer_code integer NOT NULL,
    code_review integer NOT NULL,
    type_review smallint
);
 4   DROP TABLE mecsw.tb_measuring_instrument_model_aud;
       mecsw         mecsw    false    6            �            1259    49427 !   tb_measuring_instrument_place_use    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_place_use (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    version bigint
);
 4   DROP TABLE mecsw.tb_measuring_instrument_place_use;
       mecsw         mecsw    false    6            �            1259    49433 %   tb_measuring_instrument_place_use_aud    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_place_use_aud (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint
);
 8   DROP TABLE mecsw.tb_measuring_instrument_place_use_aud;
       mecsw         mecsw    false    6            �            1259    49439     tb_measuring_instrument_standard    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_standard (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    version bigint
);
 3   DROP TABLE mecsw.tb_measuring_instrument_standard;
       mecsw         mecsw    false    6            �            1259    49445 $   tb_measuring_instrument_standard_aud    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_standard_aud (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint
);
 7   DROP TABLE mecsw.tb_measuring_instrument_standard_aud;
       mecsw         mecsw    false    6            �            1259    49451    tb_measuring_instrument_type    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_type (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    version bigint
);
 /   DROP TABLE mecsw.tb_measuring_instrument_type;
       mecsw         mecsw    false    6            �            1259    49457     tb_measuring_instrument_type_aud    TABLE     �   CREATE TABLE mecsw.tb_measuring_instrument_type_aud (
    code bigint NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    code_review integer NOT NULL,
    type_review smallint
);
 3   DROP TABLE mecsw.tb_measuring_instrument_type_aud;
       mecsw         postgres    false    6            �            1259    49463 	   tb_review    TABLE     �   CREATE TABLE mecsw.tb_review (
    code bigint NOT NULL,
    audit_date timestamp without time zone,
    user_code bigint,
    transaction_code integer,
    transaction_title character varying(100)
);
    DROP TABLE mecsw.tb_review;
       mecsw         mecsw    false    6            �            1259    49466    tb_user    TABLE       CREATE TABLE mecsw.tb_user (
    code integer NOT NULL,
    ldap_login character varying(60) NOT NULL,
    user_name character varying(120) NOT NULL,
    active character varying(1) NOT NULL,
    last_access timestamp without time zone,
    version bigint
);
    DROP TABLE mecsw.tb_user;
       mecsw         mecsw    false    6            �            1259    49469    tb_user_functionality    TABLE     t   CREATE TABLE mecsw.tb_user_functionality (
    user_code bigint NOT NULL,
    functionality_code bigint NOT NULL
);
 (   DROP TABLE mecsw.tb_user_functionality;
       mecsw         mecsw    false    6            	          0    57556    tb_calibration_laboratory 
   TABLE DATA               �   COPY mecsw.tb_calibration_laboratory (code, name, cnpj, email, contact, place, number, complement, neighborhood, city, country, cep) FROM stdin;
    mecsw       postgres    false    208   �       	          0    57564 "   tb_calibration_laboratory_telphone 
   TABLE DATA               k   COPY mecsw.tb_calibration_laboratory_telphone (code, calibration_laboratory_code, ddd, number) FROM stdin;
    mecsw       postgres    false    209   )�       �          0    49382    tb_functionality 
   TABLE DATA               V   COPY mecsw.tb_functionality (code, description, mnemonic, order_function) FROM stdin;
    mecsw       mecsw    false    189   F�       �          0    49388    tb_measuring_instrument 
   TABLE DATA               �  COPY mecsw.tb_measuring_instrument (code, specification_identifier, measuring_instrument_type_code, measuring_instrument_model_code, measuring_instrument_place_use_code, calibration_frequency_weeks, last_calibration_date, next_calibration_date, obsolete_identifier, damaged_identifier, state_identifier, state_timestamp, description, version, calibration_frequency_days, measuring_instrument_department_code) FROM stdin;
    mecsw       mecsw    false    190   .�       �          0    49394    tb_measuring_instrument_aud 
   TABLE DATA               x  COPY mecsw.tb_measuring_instrument_aud (code, specification_identifier, measuring_instrument_type_code, measuring_instrument_model_code, measuring_instrument_place_use_code, calibration_frequency_weeks, last_calibration_date, next_calibration_date, obsolete_identifier, damaged_identifier, state_identifier, state_timestamp, description, code_review, type_review) FROM stdin;
    mecsw       postgres    false    191   k�       	          0    57352 "   tb_measuring_instrument_department 
   TABLE DATA               ]   COPY mecsw.tb_measuring_instrument_department (code, name, description, version) FROM stdin;
    mecsw       postgres    false    206   f�       	          0    57362 &   tb_measuring_instrument_department_aud 
   TABLE DATA               r   COPY mecsw.tb_measuring_instrument_department_aud (code, name, description, code_review, type_review) FROM stdin;
    mecsw       postgres    false    207   ��       �          0    49400 $   tb_measuring_instrument_manufacturer 
   TABLE DATA               _   COPY mecsw.tb_measuring_instrument_manufacturer (code, name, description, version) FROM stdin;
    mecsw       mecsw    false    192   ��       �          0    49406 (   tb_measuring_instrument_manufacturer_aud 
   TABLE DATA               t   COPY mecsw.tb_measuring_instrument_manufacturer_aud (code, name, description, code_review, type_review) FROM stdin;
    mecsw       mecsw    false    193   ��       �          0    49412 5   tb_measuring_instrument_measuring_instrument_standard 
   TABLE DATA               �   COPY mecsw.tb_measuring_instrument_measuring_instrument_standard (measuring_instrument_code, measuring_instrument_standard_code) FROM stdin;
    mecsw       mecsw    false    194   n      �          0    49415    tb_measuring_instrument_model 
   TABLE DATA               �   COPY mecsw.tb_measuring_instrument_model (code, name, description, measuring_instrument_manufacturer_code, version) FROM stdin;
    mecsw       mecsw    false    195         �          0    49421 !   tb_measuring_instrument_model_aud 
   TABLE DATA               �   COPY mecsw.tb_measuring_instrument_model_aud (code, name, description, measuring_instrument_manufacturer_code, code_review, type_review) FROM stdin;
    mecsw       mecsw    false    196   �	      �          0    49427 !   tb_measuring_instrument_place_use 
   TABLE DATA               \   COPY mecsw.tb_measuring_instrument_place_use (code, name, description, version) FROM stdin;
    mecsw       mecsw    false    197   3      �          0    49433 %   tb_measuring_instrument_place_use_aud 
   TABLE DATA               q   COPY mecsw.tb_measuring_instrument_place_use_aud (code, name, description, code_review, type_review) FROM stdin;
    mecsw       mecsw    false    198   �      �          0    49439     tb_measuring_instrument_standard 
   TABLE DATA               [   COPY mecsw.tb_measuring_instrument_standard (code, name, description, version) FROM stdin;
    mecsw       mecsw    false    199   �      �          0    49445 $   tb_measuring_instrument_standard_aud 
   TABLE DATA               p   COPY mecsw.tb_measuring_instrument_standard_aud (code, name, description, code_review, type_review) FROM stdin;
    mecsw       mecsw    false    200         �          0    49451    tb_measuring_instrument_type 
   TABLE DATA               W   COPY mecsw.tb_measuring_instrument_type (code, name, description, version) FROM stdin;
    mecsw       mecsw    false    201   v       	          0    49457     tb_measuring_instrument_type_aud 
   TABLE DATA               l   COPY mecsw.tb_measuring_instrument_type_aud (code, name, description, code_review, type_review) FROM stdin;
    mecsw       postgres    false    202   �      	          0    49463 	   tb_review 
   TABLE DATA               d   COPY mecsw.tb_review (code, audit_date, user_code, transaction_code, transaction_title) FROM stdin;
    mecsw       mecsw    false    203   N!      	          0    49466    tb_user 
   TABLE DATA               [   COPY mecsw.tb_user (code, ldap_login, user_name, active, last_access, version) FROM stdin;
    mecsw       mecsw    false    204   K      	          0    49469    tb_user_functionality 
   TABLE DATA               M   COPY mecsw.tb_user_functionality (user_code, functionality_code) FROM stdin;
    mecsw       mecsw    false    205   �K      	           0    0 	   sq_review    SEQUENCE SET     9   SELECT pg_catalog.setval('mecsw.sq_review', 1236, true);
            mecsw       mecsw    false    188            5           2606    49473 &   tb_functionality pk_functionality_code 
   CONSTRAINT     e   ALTER TABLE ONLY mecsw.tb_functionality
    ADD CONSTRAINT pk_functionality_code PRIMARY KEY (code);
 O   ALTER TABLE ONLY mecsw.tb_functionality DROP CONSTRAINT pk_functionality_code;
       mecsw         mecsw    false    189            a           2606    49475 +   tb_user_functionality pk_functionality_user 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_user_functionality
    ADD CONSTRAINT pk_functionality_user PRIMARY KEY (user_code, functionality_code);
 T   ALTER TABLE ONLY mecsw.tb_user_functionality DROP CONSTRAINT pk_functionality_user;
       mecsw         mecsw    false    205    205            9           2606    49477 /   tb_measuring_instrument pk_measuring_instrument 
   CONSTRAINT     n   ALTER TABLE ONLY mecsw.tb_measuring_instrument
    ADD CONSTRAINT pk_measuring_instrument PRIMARY KEY (code);
 X   ALTER TABLE ONLY mecsw.tb_measuring_instrument DROP CONSTRAINT pk_measuring_instrument;
       mecsw         mecsw    false    190            ;           2606    49479 7   tb_measuring_instrument_aud pk_measuring_instrument_aud 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_aud
    ADD CONSTRAINT pk_measuring_instrument_aud PRIMARY KEY (code, code_review);
 `   ALTER TABLE ONLY mecsw.tb_measuring_instrument_aud DROP CONSTRAINT pk_measuring_instrument_aud;
       mecsw         postgres    false    191    191            c           2606    57359 E   tb_measuring_instrument_department pk_measuring_instrument_department 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_department
    ADD CONSTRAINT pk_measuring_instrument_department PRIMARY KEY (code);
 n   ALTER TABLE ONLY mecsw.tb_measuring_instrument_department DROP CONSTRAINT pk_measuring_instrument_department;
       mecsw         postgres    false    206            g           2606    57369 M   tb_measuring_instrument_department_aud pk_measuring_instrument_department_aud 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_department_aud
    ADD CONSTRAINT pk_measuring_instrument_department_aud PRIMARY KEY (code, code_review);
 v   ALTER TABLE ONLY mecsw.tb_measuring_instrument_department_aud DROP CONSTRAINT pk_measuring_instrument_department_aud;
       mecsw         postgres    false    207    207            =           2606    49481 I   tb_measuring_instrument_manufacturer pk_measuring_instrument_manufacturer 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_manufacturer
    ADD CONSTRAINT pk_measuring_instrument_manufacturer PRIMARY KEY (code);
 r   ALTER TABLE ONLY mecsw.tb_measuring_instrument_manufacturer DROP CONSTRAINT pk_measuring_instrument_manufacturer;
       mecsw         mecsw    false    192            A           2606    49483 Q   tb_measuring_instrument_manufacturer_aud pk_measuring_instrument_manufacturer_aud 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_manufacturer_aud
    ADD CONSTRAINT pk_measuring_instrument_manufacturer_aud PRIMARY KEY (code, code_review);
 z   ALTER TABLE ONLY mecsw.tb_measuring_instrument_manufacturer_aud DROP CONSTRAINT pk_measuring_instrument_manufacturer_aud;
       mecsw         mecsw    false    193    193            E           2606    49485 ;   tb_measuring_instrument_model pk_measuring_instrument_model 
   CONSTRAINT     z   ALTER TABLE ONLY mecsw.tb_measuring_instrument_model
    ADD CONSTRAINT pk_measuring_instrument_model PRIMARY KEY (code);
 d   ALTER TABLE ONLY mecsw.tb_measuring_instrument_model DROP CONSTRAINT pk_measuring_instrument_model;
       mecsw         mecsw    false    195            G           2606    49487 C   tb_measuring_instrument_model_aud pk_measuring_instrument_model_aud 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_model_aud
    ADD CONSTRAINT pk_measuring_instrument_model_aud PRIMARY KEY (code, code_review);
 l   ALTER TABLE ONLY mecsw.tb_measuring_instrument_model_aud DROP CONSTRAINT pk_measuring_instrument_model_aud;
       mecsw         mecsw    false    196    196            I           2606    49489 C   tb_measuring_instrument_place_use pk_measuring_instrument_place_use 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_place_use
    ADD CONSTRAINT pk_measuring_instrument_place_use PRIMARY KEY (code);
 l   ALTER TABLE ONLY mecsw.tb_measuring_instrument_place_use DROP CONSTRAINT pk_measuring_instrument_place_use;
       mecsw         mecsw    false    197            M           2606    49491 K   tb_measuring_instrument_place_use_aud pk_measuring_instrument_place_use_aud 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_place_use_aud
    ADD CONSTRAINT pk_measuring_instrument_place_use_aud PRIMARY KEY (code, code_review);
 t   ALTER TABLE ONLY mecsw.tb_measuring_instrument_place_use_aud DROP CONSTRAINT pk_measuring_instrument_place_use_aud;
       mecsw         mecsw    false    198    198            O           2606    49493 A   tb_measuring_instrument_standard pk_measuring_instrument_standard 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_standard
    ADD CONSTRAINT pk_measuring_instrument_standard PRIMARY KEY (code);
 j   ALTER TABLE ONLY mecsw.tb_measuring_instrument_standard DROP CONSTRAINT pk_measuring_instrument_standard;
       mecsw         mecsw    false    199            S           2606    49495 I   tb_measuring_instrument_standard_aud pk_measuring_instrument_standard_aud 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_standard_aud
    ADD CONSTRAINT pk_measuring_instrument_standard_aud PRIMARY KEY (code, code_review);
 r   ALTER TABLE ONLY mecsw.tb_measuring_instrument_standard_aud DROP CONSTRAINT pk_measuring_instrument_standard_aud;
       mecsw         mecsw    false    200    200            Y           2606    49497 F   tb_measuring_instrument_type_aud pk_measuring_instrument_type_aud_code 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_type_aud
    ADD CONSTRAINT pk_measuring_instrument_type_aud_code PRIMARY KEY (code, code_review);
 o   ALTER TABLE ONLY mecsw.tb_measuring_instrument_type_aud DROP CONSTRAINT pk_measuring_instrument_type_aud_code;
       mecsw         postgres    false    202    202            U           2606    49499 >   tb_measuring_instrument_type pk_measuring_instrument_type_code 
   CONSTRAINT     }   ALTER TABLE ONLY mecsw.tb_measuring_instrument_type
    ADD CONSTRAINT pk_measuring_instrument_type_code PRIMARY KEY (code);
 g   ALTER TABLE ONLY mecsw.tb_measuring_instrument_type DROP CONSTRAINT pk_measuring_instrument_type_code;
       mecsw         mecsw    false    201            C           2606    49501 G   tb_measuring_instrument_measuring_instrument_standard pk_mi_mi_standard 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_measuring_instrument_standard
    ADD CONSTRAINT pk_mi_mi_standard PRIMARY KEY (measuring_instrument_code, measuring_instrument_standard_code);
 p   ALTER TABLE ONLY mecsw.tb_measuring_instrument_measuring_instrument_standard DROP CONSTRAINT pk_mi_mi_standard;
       mecsw         mecsw    false    194    194            [           2606    49503    tb_review pk_review 
   CONSTRAINT     R   ALTER TABLE ONLY mecsw.tb_review
    ADD CONSTRAINT pk_review PRIMARY KEY (code);
 <   ALTER TABLE ONLY mecsw.tb_review DROP CONSTRAINT pk_review;
       mecsw         mecsw    false    203            ]           2606    49505    tb_user pk_user_code 
   CONSTRAINT     S   ALTER TABLE ONLY mecsw.tb_user
    ADD CONSTRAINT pk_user_code PRIMARY KEY (code);
 =   ALTER TABLE ONLY mecsw.tb_user DROP CONSTRAINT pk_user_code;
       mecsw         mecsw    false    204            i           2606    57563 8   tb_calibration_laboratory tb_calibration_laboratory_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY mecsw.tb_calibration_laboratory
    ADD CONSTRAINT tb_calibration_laboratory_pkey PRIMARY KEY (code);
 a   ALTER TABLE ONLY mecsw.tb_calibration_laboratory DROP CONSTRAINT tb_calibration_laboratory_pkey;
       mecsw         postgres    false    208            k           2606    57568 J   tb_calibration_laboratory_telphone tb_calibration_laboratory_telphone_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_calibration_laboratory_telphone
    ADD CONSTRAINT tb_calibration_laboratory_telphone_pkey PRIMARY KEY (code);
 s   ALTER TABLE ONLY mecsw.tb_calibration_laboratory_telphone DROP CONSTRAINT tb_calibration_laboratory_telphone_pkey;
       mecsw         postgres    false    209            7           2606    49507 *   tb_functionality uc_functionality_mnemonic 
   CONSTRAINT     h   ALTER TABLE ONLY mecsw.tb_functionality
    ADD CONSTRAINT uc_functionality_mnemonic UNIQUE (mnemonic);
 S   ALTER TABLE ONLY mecsw.tb_functionality DROP CONSTRAINT uc_functionality_mnemonic;
       mecsw         mecsw    false    189            e           2606    57361 J   tb_measuring_instrument_department uc_measuring_instrument_department_name 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_department
    ADD CONSTRAINT uc_measuring_instrument_department_name UNIQUE (name);
 s   ALTER TABLE ONLY mecsw.tb_measuring_instrument_department DROP CONSTRAINT uc_measuring_instrument_department_name;
       mecsw         postgres    false    206            ?           2606    49509 N   tb_measuring_instrument_manufacturer uc_measuring_instrument_manufacturer_name 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_manufacturer
    ADD CONSTRAINT uc_measuring_instrument_manufacturer_name UNIQUE (name);
 w   ALTER TABLE ONLY mecsw.tb_measuring_instrument_manufacturer DROP CONSTRAINT uc_measuring_instrument_manufacturer_name;
       mecsw         mecsw    false    192            K           2606    49511 H   tb_measuring_instrument_place_use uc_measuring_instrument_place_use_name 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_place_use
    ADD CONSTRAINT uc_measuring_instrument_place_use_name UNIQUE (name);
 q   ALTER TABLE ONLY mecsw.tb_measuring_instrument_place_use DROP CONSTRAINT uc_measuring_instrument_place_use_name;
       mecsw         mecsw    false    197            Q           2606    49513 F   tb_measuring_instrument_standard uc_measuring_instrument_standard_name 
   CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_standard
    ADD CONSTRAINT uc_measuring_instrument_standard_name UNIQUE (name);
 o   ALTER TABLE ONLY mecsw.tb_measuring_instrument_standard DROP CONSTRAINT uc_measuring_instrument_standard_name;
       mecsw         mecsw    false    199            W           2606    49515 >   tb_measuring_instrument_type uc_measuring_instrument_type_name 
   CONSTRAINT     x   ALTER TABLE ONLY mecsw.tb_measuring_instrument_type
    ADD CONSTRAINT uc_measuring_instrument_type_name UNIQUE (name);
 g   ALTER TABLE ONLY mecsw.tb_measuring_instrument_type DROP CONSTRAINT uc_measuring_instrument_type_name;
       mecsw         mecsw    false    201            _           2606    49517    tb_user uc_user_ldap_login 
   CONSTRAINT     Z   ALTER TABLE ONLY mecsw.tb_user
    ADD CONSTRAINT uc_user_ldap_login UNIQUE (ldap_login);
 C   ALTER TABLE ONLY mecsw.tb_user DROP CONSTRAINT uc_user_ldap_login;
       mecsw         mecsw    false    204            z           2606    49518 =   tb_user_functionality fk_functionality_user__functionality_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_user_functionality
    ADD CONSTRAINT fk_functionality_user__functionality_01 FOREIGN KEY (functionality_code) REFERENCES mecsw.tb_functionality(code);
 f   ALTER TABLE ONLY mecsw.tb_user_functionality DROP CONSTRAINT fk_functionality_user__functionality_01;
       mecsw       mecsw    false    2101    205    189            {           2606    49523 4   tb_user_functionality fk_functionality_user__user_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_user_functionality
    ADD CONSTRAINT fk_functionality_user__user_01 FOREIGN KEY (user_code) REFERENCES mecsw.tb_user(code);
 ]   ALTER TABLE ONLY mecsw.tb_user_functionality DROP CONSTRAINT fk_functionality_user__user_01;
       mecsw       mecsw    false    205    204    2141            p           2606    49528 B   tb_measuring_instrument_aud fk_measuring_instrument_aud__review_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_aud
    ADD CONSTRAINT fk_measuring_instrument_aud__review_01 FOREIGN KEY (code_review) REFERENCES mecsw.tb_review(code);
 k   ALTER TABLE ONLY mecsw.tb_measuring_instrument_aud DROP CONSTRAINT fk_measuring_instrument_aud__review_01;
       mecsw       postgres    false    203    191    2139            |           2606    57370 X   tb_measuring_instrument_department_aud fk_measuring_instrument_department_aud__review_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_department_aud
    ADD CONSTRAINT fk_measuring_instrument_department_aud__review_01 FOREIGN KEY (code_review) REFERENCES mecsw.tb_review(code);
 �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_department_aud DROP CONSTRAINT fk_measuring_instrument_department_aud__review_01;
       mecsw       postgres    false    207    2139    203            q           2606    49533 \   tb_measuring_instrument_manufacturer_aud fk_measuring_instrument_manufacturer_aud__review_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_manufacturer_aud
    ADD CONSTRAINT fk_measuring_instrument_manufacturer_aud__review_01 FOREIGN KEY (code_review) REFERENCES mecsw.tb_review(code);
 �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_manufacturer_aud DROP CONSTRAINT fk_measuring_instrument_manufacturer_aud__review_01;
       mecsw       mecsw    false    2139    193    203            u           2606    49538 N   tb_measuring_instrument_model_aud fk_measuring_instrument_model_aud__review_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_model_aud
    ADD CONSTRAINT fk_measuring_instrument_model_aud__review_01 FOREIGN KEY (code_review) REFERENCES mecsw.tb_review(code);
 w   ALTER TABLE ONLY mecsw.tb_measuring_instrument_model_aud DROP CONSTRAINT fk_measuring_instrument_model_aud__review_01;
       mecsw       mecsw    false    2139    196    203            v           2606    49543 V   tb_measuring_instrument_place_use_aud fk_measuring_instrument_place_use_aud__review_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_place_use_aud
    ADD CONSTRAINT fk_measuring_instrument_place_use_aud__review_01 FOREIGN KEY (code_review) REFERENCES mecsw.tb_review(code);
    ALTER TABLE ONLY mecsw.tb_measuring_instrument_place_use_aud DROP CONSTRAINT fk_measuring_instrument_place_use_aud__review_01;
       mecsw       mecsw    false    203    198    2139            w           2606    49548 T   tb_measuring_instrument_standard_aud fk_measuring_instrument_standard_aud__review_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_standard_aud
    ADD CONSTRAINT fk_measuring_instrument_standard_aud__review_01 FOREIGN KEY (code_review) REFERENCES mecsw.tb_review(code);
 }   ALTER TABLE ONLY mecsw.tb_measuring_instrument_standard_aud DROP CONSTRAINT fk_measuring_instrument_standard_aud__review_01;
       mecsw       mecsw    false    203    200    2139            x           2606    49553 L   tb_measuring_instrument_type_aud fk_measuring_instrument_type_aud__review_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_type_aud
    ADD CONSTRAINT fk_measuring_instrument_type_aud__review_01 FOREIGN KEY (code_review) REFERENCES mecsw.tb_review(code);
 u   ALTER TABLE ONLY mecsw.tb_measuring_instrument_type_aud DROP CONSTRAINT fk_measuring_instrument_type_aud__review_01;
       mecsw       postgres    false    202    203    2139            r           2606    49558 T   tb_measuring_instrument_measuring_instrument_standard fk_mi__measuring_instrument_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_measuring_instrument_standard
    ADD CONSTRAINT fk_mi__measuring_instrument_01 FOREIGN KEY (measuring_instrument_code) REFERENCES mecsw.tb_measuring_instrument(code);
 }   ALTER TABLE ONLY mecsw.tb_measuring_instrument_measuring_instrument_standard DROP CONSTRAINT fk_mi__measuring_instrument_01;
       mecsw       mecsw    false    190    2105    194            s           2606    49563 ]   tb_measuring_instrument_measuring_instrument_standard fk_mi__measuring_instrument_standard_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_measuring_instrument_standard
    ADD CONSTRAINT fk_mi__measuring_instrument_standard_01 FOREIGN KEY (measuring_instrument_standard_code) REFERENCES mecsw.tb_measuring_instrument_standard(code);
 �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_measuring_instrument_standard DROP CONSTRAINT fk_mi__measuring_instrument_standard_01;
       mecsw       mecsw    false    194    199    2127            o           2606    57375 /   tb_measuring_instrument fk_mi__mi_department_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument
    ADD CONSTRAINT fk_mi__mi_department_01 FOREIGN KEY (measuring_instrument_department_code) REFERENCES mecsw.tb_measuring_instrument_department(code);
 X   ALTER TABLE ONLY mecsw.tb_measuring_instrument DROP CONSTRAINT fk_mi__mi_department_01;
       mecsw       mecsw    false    206    190    2147            l           2606    49568 *   tb_measuring_instrument fk_mi__mi_model_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument
    ADD CONSTRAINT fk_mi__mi_model_01 FOREIGN KEY (measuring_instrument_model_code) REFERENCES mecsw.tb_measuring_instrument_model(code);
 S   ALTER TABLE ONLY mecsw.tb_measuring_instrument DROP CONSTRAINT fk_mi__mi_model_01;
       mecsw       mecsw    false    195    2117    190            m           2606    49573 .   tb_measuring_instrument fk_mi__mi_place_use_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument
    ADD CONSTRAINT fk_mi__mi_place_use_01 FOREIGN KEY (measuring_instrument_place_use_code) REFERENCES mecsw.tb_measuring_instrument_place_use(code);
 W   ALTER TABLE ONLY mecsw.tb_measuring_instrument DROP CONSTRAINT fk_mi__mi_place_use_01;
       mecsw       mecsw    false    190    197    2121            n           2606    49578 )   tb_measuring_instrument fk_mi__mi_type_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument
    ADD CONSTRAINT fk_mi__mi_type_01 FOREIGN KEY (measuring_instrument_type_code) REFERENCES mecsw.tb_measuring_instrument_type(code);
 R   ALTER TABLE ONLY mecsw.tb_measuring_instrument DROP CONSTRAINT fk_mi__mi_type_01;
       mecsw       mecsw    false    2133    190    201            t           2606    49583 =   tb_measuring_instrument_model fk_mi_model__mi_manufacturer_01    FK CONSTRAINT     �   ALTER TABLE ONLY mecsw.tb_measuring_instrument_model
    ADD CONSTRAINT fk_mi_model__mi_manufacturer_01 FOREIGN KEY (measuring_instrument_manufacturer_code) REFERENCES mecsw.tb_measuring_instrument_manufacturer(code);
 f   ALTER TABLE ONLY mecsw.tb_measuring_instrument_model DROP CONSTRAINT fk_mi_model__mi_manufacturer_01;
       mecsw       mecsw    false    192    2109    195            y           2606    49588    tb_review pk_review__user_01    FK CONSTRAINT        ALTER TABLE ONLY mecsw.tb_review
    ADD CONSTRAINT pk_review__user_01 FOREIGN KEY (user_code) REFERENCES mecsw.tb_user(code);
 E   ALTER TABLE ONLY mecsw.tb_review DROP CONSTRAINT pk_review__user_01;
       mecsw       mecsw    false    2141    204    203            	      x������ � �      	      x������ � �      �   �   x���Kj1�t����3y.��~�]�@K�E����4mR�&x'���z����r.��4s�4|��A��Bځ��:�C�Ț�P2�K�p-�a���C2{��T�,�E5$[���ԝ��Z�R���T��F6{S�b���d�>�t�lkΑ�{�R��-.�>h���e��:�������k+\W,�Q�p򕍚{�t�^���R      �      x��}�rGr������F�Q���@�5#R	9loh���]�rP��~��ffUw�_��hfb�(~���ϬL@w�}si����KP��>�Al<X<�	p	�B����߂|��/��?����ǧ�o�_����^ � :X�%����S���
�
�I��)L�������xx��R���Cp������0͚F
�?`8�-0��L0V`�����x�q��L0A`���A�s�J�.���80��4��Έ��h3�.�E�����iyQ�)��޼#:��b<@��p���}ӧ�V�)�\ЂDr�|HQnoY>^]�@��ۄ
�H��@j֔GH"�� *!$�XJ��~	O�Y��<� D �$D&+����hY���5����ѿ��h���;���,h@���+��9�WJ�-����"��_���O���_�:�` G�G��B8�����d{*ߟ#���YU����C���i��p�N��6Q���@�P�/U����I]��{��6��2I�׷wD)���	�`HM�\�"�m��?'L� s�}7���\A�2#�
�\��&t���u���N+�!!�'X<,{5�w�_}w��9���ǵ4���x��8;��8|�WK�ܛ_~�ヸ��X����}E����D��m���H��N�L��"�|�bY��VPɲtՅ��j�[�؟.]:أ�G'��f�
!�LH@
XI���Dw�;]%[z�S�Y���LD�s�$\})�;��@�-\ �=��P��,^���-h�Wl��R+ے-}3�%[�|���B��-�>�VZK����	T����?|����˟n�ϯ�n���މ�h����rI�<���(�$���u�c��CtL�Bt�q�8LQ����G�)w��I��,���@�sn�Y�o'�Y�"�p4|�:");�Q�(����(-��*�߻e���k
yo�M�ҁ�L�r�hm�̜$���H���&���j�ڮj�E'`�y��]�dᲡ���[x#�����"�n�f` ��I"��p�jb��������/��zR�.�^2�5�.���U�3s����B��B;B=��� 2�DgA�MbL�T!�S�`[��&EߗF�*sH�'�e���F��B��d����ݭfZ8A��O�Զ�t����"9Y̚0�b�};fN:�GPobǴ�v
L��B @R/l�$"Rr�d-��|���O��ħ��h��]�8YՐ�j@Q�t��/�e�M�'>͕�bHR�^i�JG>Xp��
�q�5�Ƅ�1u֘*�&g���5f�0���QRҚ��5���A.���F�R�SN���:�#!���@���RJ�����O0��O���~�Z�&m�N�4�I�'ÚY�՝��!�c$b�~��q�����B�
���[�}&^M��V.og\�m�n�
�9q1������~���хY� 
�"�r�Ȑ���AC�Y�y]�EmG�Rc����(�Y h��U��X���P'9�^���x*.p!��p�x�I�h��I��,�
)��;�\���}:=Bցf�r���,J�D�0)L����ѱ�?O����Lax2N^_��N�x\y��H���RD�X������u�#X�;S����X\#r	�=j^�R(x�v�} ��ڱ5��r�半?�3 �;�R��|��S�<]���!�3L�,��EHaKtY�`�ЂE z��9� �Q�9R��� bG/b'���KLKB1��!#dͥ=m��\�Rbv�N_.l�0͎�+���@4��P/1����N�V����Y�k'�eL1����-gv8Z{�����Pp<��OlǪC
6=�X�5eb"�Db	��阾����F$��$U���Ld<�ʐkmG�v�[n֔Y� ����_�p$�H��թ3(͐:C�(E��@�3!"1��
2����s$EaԺ7�y����6��ߺZs�v�8}�#�G�%�M7h
H{fHs�ywMxJO �ooɻ��4�6d��Ś��	�ԭ)}��D��!ɵ;�1C:k�x�
жI�~=ez7kJ��V@�,���g4+�}@⃛ww3�^�No'��Y�n�e���!݌Ux5�:"U�s\he\lӉ#Vܐql�'8:�(g�9�z�K��S� ���Ą1�	�=���I6^�j9{&Q�1*Ю�";6�w[��fod���ݧ������H�=5jSZ�:y�*GV�T
Ё��+�r.�^9�d�"��X���� �(H	ޠ�,o]���{�F��KDNÄ���]JȚ���"��0G=+�x�`�@TJ~hOF#��$��**��W���o��M�|��T䋓cSl��^�tȅ�\!����#�2X�^H�1�2�iI��\Ɋ���l�VTL"�ř!˴��, ���C�s�L7�0��(ن=/<r��r�~Ҝn^��%����H>_O����r�i��p�+R�¾
�(wOiF�%n ����SYt#@��\%�sO��M�/��Z?�&���PFǱ�lE a�9���{�j2d�	��nPl�L:�i6H�
�V=i��!���[�#slܹGD�R�m�]"q��c&�����Xq�n�������a�C����dX��ɲ,�PgO��⭎�q�G�'�˼�lT���'�+V�y�݁��*wG�y{�s��+T X�Lh2����{�@�[S��M�t���Pk��
��_Q� �[M���^�6\|�p�_��H�xi�ѷ��r������Xu���_���	@�:`�o���%��~����lKB�%�cm�*M�Rf[2\E8WŹ��R�uɸp�d��X<�1tpEU�o�7��z�b_wn�m��0|d��p�8y���#��
h�`��2I����[$����>��׏~��˺vTT�Ći�������$��<A�Ú_̲�&�fZ��$.jqk�p7�������Z�L�xq{ɓ��#B�G��T��𝤤�'n�}W���ͦ�,�Y��N��dA}��}�$֊-v��1�i��(�0m�R'C�d�0R.rx_3���/:C<!��դ�~��R�Z����]�&���/]�%*�:�_�(����&Grd9Wm����w�3W5�ݥ�b��rQa����$�mD�/�q�Mj��2\�J�N2f03�ܙz`����bѐ��`�.��h{�=�݉6k�"�bJ������Q;�$h�)B���}}�-]���.6��Ʉ���֨ ��a�T���iT�쿱F�l�r�
�3�@���{�MƬ�W:v�g	�8w:]ܽzC�M�U�MÕ�����	�On���A�YOA\�F� @?�`�gpBB[�rL���@ʯ��rҧ}�g%%.��s�J ˗�)�����/n7S:V�F�ӳ��S�Ȟ��q����w��D8�O�Isi&%]ߧ�;�?^�鄆ϋf��hB3	M�	�'4���9��`D�߾�@y|
��c}������Ku+T˨>���Bu��3�}&T��\�H�����q���7h߸�P��e΃=p�MA�σʕ7��������d�4�Z�G~����������H0��)��^���������;���7@�]`粬F���j_�e�E�֋ds$���O;��	cd����:2�#�2e��7�ϼA#yy�E1GJ8,�q��A<�us��@P��IH�䷃g�$��e���>|�pJ+�%���Y")?G�]�&�5�(��x����r:)�P���ٳZ�Xl�d��1{��;��0���W�S��.oW0l|�5�%S��\|������;Ɉw�Ѱq���)�"X������=胎�.��
��m_���NV`�k
y�E>H�
��+�~�ApB�<*���-&��ժ��d���D{��Y8ZE^�dI��LC���l�m�p=�^�N��{cݺ��2W���	zx��q$�AvI��V�w~�ȹm���	�c�[���F�5`�.�n[�J���/�	��.�*\�����l���H���xCv�.:�X�z��s�xOg��5��_�n;��dc��G���W<s���6a��P�r_^���}�ѓ����b�t]˾�    �L$�ӧ0G�0'9ˡ� E���~z{�UR���5Yl��`I�ʫd˲�Z�~VTR��Fn��,I��W�9Ȏ0gWH�Z��z��M���7Qbe��pyRh�Uc��~��"pq��6�ף�͍�v�:j�ɓ-�Z��^��L���w` �I��RV���k�jѩ��Q�[��ȰEN�#�A]��	E�?r�_)ҕ�&!]�d������S�R�n;���6���zI*��v,1�`(�7�[2��AO+���f�%K���懗�/_-���}a`��o��V�#�#8����M�]\���ueO�'\�(�lECan$�5�i�s2��tx�LG$h��3ʚ���K8�3̚�0�LW��'�!�:���8��3��F;��:�7�!m�XI� gvwOk|��&���W��D;�n܉݄*Tx*Iڷ0E:�����1�>��O��aM�3eu_�^��qU<G�Hv�}�ɯd��v�v*Y����X��e+���n�K�8�;���(:C�18]��C�v8]��L�'J���_����t�d����!k/08�����%D7���g�1#�:)7���{Ӷc��:�b����:@� �8PI2q�.��4���h���uHA�*p݈(��5+~�q�YX��wQڇ �j.g�=�$�G������Ɣpσj����&DFe�Q]������i-N=��.a��7d���˂��gZz��$���O��p��{&ܐp1����"w(Uւ�ۅ�]�Ģ�ђnR%ϩ5i7��U�bp��/�PMBY����`Sj����E~�� Т�=	�/���\L'��S1�E�G��}n����+T�{ńj��:�8ǉ4���^mtY.2**Ή���vݫo�P�5�6�Z/�Pg�(��\��LT|T�66ib��M�6mM�`�N�������ς��&@�Hޞw����R�/ņ�1[� .����*�0T�a �s����HC�3�O���W魏 ���ˀ� �L4f9R�b�Z�|3���;�,N�b�+W�V��vZ����y��;Q5ׁ�&Q��m�D��ʁ��i��y��|�\���az@Ͻ� zÕo��??�ۧ_�ؐ���
>��
�o;b�[X�k���G�m	/5#_�L��w�,.��nQC����,�]���r�((�uw�ɡ�����nau�~>-"a "�>"r����_��N.N��.UK��!A-�е��v���YX��z���i�nT�����].���<g��)��[%���V��ƺ�YXk�d�C�	�^�6�8�VZ�|�un���?$08뭹�%]�h�ED,�̴|�	�uK�L#�i˽5AoӖul�m�{��@g4}N�8w�ͽݛ��?�f ��2SzM�X��2���l͙�d�a�����\V)h��h���R������Y>���V`v�p\��u�(��t)��{~řX5R?�7ZZ����˸.ᒢxܘqc�/�����~w4p��q*�J��5σ�!�2��>��b��f�H�o�H�e��^�K�g�l��r��"_���v-��?
\��M���FN;^p䘍*Ҙ1'�D5���@YZ$�G�6p���Dԇ��p�],9�=�������L�0S.��y�F�O���n���;�Y��C̝A�[����R��Z�� ���z��vw�����3��Lm{�M���o;ү[XI!{������[Z��g����x�q˺#�+@c5�~5�}�0G?T`�i�B�2a݆�L[��!��VX����̘Q��r&�k�t�j�K���YX����;�������f!��Ҁ�x��*���ʸ.��N�}\ȵ���6
�*�xĨ��]�����_^M*��4�KO�y=�m�q�E�e�ԅ��d�����da�h��l�a���:bϫ��ypwj*T4��1���H�YX���Ԙ���aj�յ�O�z���V�.=�U�>.y�2���NjRQ��n�m�x:�^Xb#?�N���K�5LFnl]Ւ��Zt�({i|�i���%�Qb���ޥ`��u)���%�.b3��[Y��B����%v|�na	f`�pC��O-ӭ�{mH�ih�����d�>7��N�%���L��.�t��+7z_TFt`��m�i�(q1��Q!G�ooY�
.����!�/�^����1'~\��0��Vû���4ZX��r�G�y�3���y.@?�� �Si����ؖf0�t3�L[���)�e�X�D���R*�
}΃ʃ@e(���D���NXOٷ�b���S�[�b�������BX��8u��d8�}J���0<��4)�v7�NbFF�g�Ę�w���/̸�t�Stض��j�kO6��.�X�t䪵&,�9��J���~Sc,��ĺ��kn�\f#軀v���0@�|ː%��$��5L|Zń�_Y�����@��M���<��ʊsb*�ö�R�6�c�V������aSOቘ���4��c��75hcm�M�n�O�תp�>�M�bIvN*u�5���sE n#���n��B	�����^�G�ȳ��uku?o+Y��g�_e�_�쓈�5NH����0ݲ��}E��y��
Fq�"�[XB��������h��ߡ?7H�5+��B����r�%��8�6������`e��n�z�N����L�q�c�W��	��X�����Ay�+G�3���l��U�h�*�FwX�_X�����J/�+�<a�-�ܴ��U�^IF�p%[��x�i�_M�L�\��]G~l;�l�����-��o������Bн�ُ�	���]D%�D%]d�xT� ƶE��]������j�Xt�t~2T�a҆��?��a�橞��rg,��qr���?��ND;���"�F�:ѐ;�[X0��y0't�9U�u���|��6sZ+휵/��HQFvp��>U���X���$���G&��̃@�72\m&�b�}<#��r�t�?6���vM�tEvGe�&�j�O���1��	���x{�7y{�p����P8՟��K%=�M,��@��t��M>��ok:8FT�5��F�+���1H�(�������y�M5�f3���o��B���D9y�&���~XD�$���Ҋ��L1�C�~�*�NCw+����(�0���L��h#7]_a/�������zw�\#�S�����{�����&�� Qe�e�0P"x%�-, �3�x8ؠm4��XX�����ë��f��/Gj��M�?K�Hk=J�J�چ������mG�DӶ���sl�Ev��b��1O\�"d�x�Ր��/�\�C��I1��;v#�>�h�g�V;V	�e��L�,S���r��P��nب[Y�mn�$j;^�<�ꝶw�^L�� �n�b��⚦U�f�{�S�����t���X H�?�Nk�G���e�ю��G�Z5>�W@�U@_��	��G/�ʈ�,��@�Ѩs"��<"����q���3#�o<%���.L:^q��:�:"����
�G�y�m2�<�'��Tm���\�y��"�W�_Ϡ�������Sq{:��y`D1��I̱�I������J;�ѡlү��j����+�z�`�|���a��k���(��{s��J���ׇ�:Uҭ\C6��_��t�1�*i7ݷ%pSM��I<�D�;U�V!��I} A�O6����j��=�dTCꐝ6t��Б	����d쾹y�X�\7'��K��޿u�O�����ޙN'�
��'�*co�t�<�X���"�Фt�ДH9���9�N7�j-<��<��2�9�[�i�6��ι�G�{�`+RNl�PH��ݟ�v.��*��VM��AlJ�#K�x��㤸�+,��Y�J$N��i��efS�s��,��;�Hy��?ꘆJ-�G�a�Y��:�f�c�X���j�c��{WN��1CJu��y��s<�%pO�}$�;j��!�rkRn"�")w�s�y�����Go%;Aح\�������5����_��MO���s�o�^SD�$�@V��-������i8Lb+���x2W	��U���?�Y���So.,w�"�>�&��v #	  �@h�ܣv�[Xt8K}�<���>w'����YҸ�+]��}J��ڸ��O�k�tq� �X�p��r�8S�i)g��OS��K9�g=E]f�a=A�w��\�\�����j���+ln ?�ġ���b(H�^��%f�:�WaH;&!�I���^ �z�}qĸq:[}.(t��t�\F�8���!�]���c��v��gW�ς������
��K"�2O-��1�����/ҥ�'��Zz�zv*d�/�x,9�)�Xn8�y/شbAT�l�O�ɑ���;�O�pEF��<���p���4�$��>��ń1�ŕ�+�~�Q�L;К#�b��ӥ9��ui����S.����ekF�'7|�\�Ηͮ5��1@�,sԡ�0|�m̓��*�X����=��, -F�t(�{��pM���A�����
�1C9䒸�|0f���{�vI�C��+�����;��I�!���tӗ�F�uQ�Hر�g��:UWL��×����߿~ă�ܱm����>����U�Z��,�L/`f��}��3��؀u����6���
�.`���j���~t^�ъL��4`�Y��=vk*0���vg�`����pg͝HR�`Q�tK �;;��:l�Fk0��u��p�С[S������� Ŀ��}���*C@�������ӣ��V�T�U��H������˶�����XM���}����F/��Y�M����QB��C7d����p媘Q{��k�m3�r8�h�-|�ڽ*�[3��SG����Q1񸜺��3�GA�:�Z4]<j=qυ¬72�{���U��F~g.A1���|k�P��o��5<��1k�Ji
0��(��u�)�S��='LdkT���Z9�"��c�Ƭ��'��Jr�#�$�R����`4k%�F<����sen��Ia1�ȳ8�/C�)ܸrI_�iv���k�N�tUcI�L�݆��ȉ�	�_?����>}����_>�v����e�>��,�ZJ�;�I�,~0L&�2�A^��?�����_I�{~�D�B��؝�	!w��8�=���.)��D%�I��n4���*�	�G�YM^�+,ln�!N!^ݑ;�9�xJY���%�m���if�ߎ��6
պ��b�*Ku��cJu<��y�j:v_ߑ'q�����dq%F�����PN�#S�"��A��O_����K�z��䙟�3��*�E6b��N�xj*J��MΔ>wZ�4����;4�	2��Y�rM�0��DE0�1r��V�w�Ğu��u�h���3t��\('�fp}�o��h_�5pm�����t��	�D�0�5ǳ嵑��R�ly�쒖mao�T"���89�xx��������� 1����������i����,����+�T6S��o��F��S��hl��W2��l���"�u��������̻bB2iRy�������K�L�����;ԓ�ѢR&W���`���	�N9ϻ.�@� ��q+��d���/��RXH�_a�ܮ���N#*�6�fm�J���� }d��biG8��iQ�G�$���z�]���qш����ciJO��Z��J3�<�@���Ӂ<���G,��:�|m�����' <$����P�I�N�5��bw�N��ch���m8Ƿ����4�GCW�%�W����Yg�!����G��<�/��0�r@�l�����FhOï�RE�<%�	��Ιc�=�@&�ϛ��0:i~�aW�IrJ�f���t�3;fۊC�q��N�����,I*��~Һ��I�ux�:_�T Ot�{997�W�_�|s��V:RE\E��޷�cF��U,&��O&��(>�sCtb���#�)��I��<�R�̈�F��s��]�uވ)n���q��6�6��4��:��-�� �o��U{*����5K��ף����]h�톆��:.�b��V��E11��
O�b,��Ri���g�ֶ�v�
�x+y\%��7E6;�2k�%?�2N�c5E���71�Q �n���Rj�٘g�Q�d<r�G�c_>�ۨ��U�=y�p�+Cb��q
v�6�`�g�>h��0Ž("��D���nX	7��@7�m|��s�=5q+�B�� n6�_\��}� �tR�ץ� 9
���z��]�o�h�'����F��n/�8��$s������W���1,۩:(��Kv������(��zAR���\��?O�����N�:[��
���(�!�4-�d��Pc�	�/A}�s-�������b���G�;}Pv�'�c	*����^JTD���[�1��팹MP�Ĳf_�l'�h�i��yT��J�d'v.�����/�����      �      x��}ٲǑ�s�+��ݼ�/��e$� �ʹ�-B3�HH=���C�~l��g��Xr��Y�1A����S���qw������	ww��VeM��v�f0J�{�S���ğ�����_��?���_>u������~Ɵ5,GXV��݋�{m�h}�\����TI�Ǖ\%T�А�2j���[�"���Ak��׃��'�~�U�����<4��`�w/����`m]��"Z�W$��E��}+8�CCV�q�q��.p��y�Y��zhȴ��*o^���Si��!8�E��^-��7�F�[�W>4d�/���Sz���x�}a�= 0�g�yh���a�Zg�#��|E�;8�u���U.����gze�W��]+���W��HXc����t,`�����߿��_��i���?:����c$4�h8��{Mh�!�2�s4�?��є�g��̣v���r��������+�ITh{PN�r��R�^ǿ��㏟����k��h՘	1���O/?��ÿ~���?�T��G�@�9�W�M�����nI���#�w��2o_���	�FM�ٚl��G��p\������_?}��3-򣍣�N�1�6�}�	�>�.�& ����ׯ��Z{(8']e�M{�_��n�3V��	eЉu)i�Wo�ZRn�_�y�P�֦�e��
��<K�Z�lƅk��K�#%H>�`����5��Wjԣ��F���������?�}���_?����������_�������t��~�:��5o��G���
���ξ�|��_�M�aN�,_Ts��u]�W#i�]t�s��`5v�a�H%��QG���چ��w0�nL��G�΄ix;�\$�,']d���F�v��f��y4q���	T�UA�:�JAE����G���y�8w�^\��
tg�EU��ҭ.��]��ф�$P���A���yE^\]�j�wQzTy̙1a�%��u,'��ɺ�7w0��6�$H�A��i�C��Lgi��ܿ��Q{�d9��P[������;h�����
Wдg�:߷�D������`ɰ#��?Y��}���y�sK�|f�V���P"R��J�+��q����vH����Ԝ��1���?~�U������O�~�m��q�tf���;�@���{F��J��O�dc_��l�T���P�s�5~}ϿBM�����/ώ��Z'�x̓����,b�Ue��r7��`�K��ծ��KXiV�`�+��4!G���}zm�y�F5�ޮZfU��:�
�\˩-�B��I����+uF���2f`G�O��ua�����FQ��B��(˯��@]��ai+eB�^�#+�^�<2V	N��X���Bሴ7B,�L�o �N�W7�c.��$��\�'��6���Z�w��q�ӻ3\���w�b�}[� �\�w�ZL�LZv��9�|����0����l��ﴃ���K�@�q.�3+�L����A��z�"��7*&v���9�2���1#�r�<}Xvȼ��!�.l���ȳ>�壇߽��^���h�i�I�Yʣ]�P��k�G�`[[���g���[X�.4BϏ֏�Q8�v`jg�1x,���J�,z8��ce�90���h2Ne��%E��C�xS����pG�sp�P�[�Wf��-k�*C�gT���Nc��ʖ��7����-z 5�3�a��d�Bg�x���B���FTt-o�8����j����J��-���7w�w��� �~�L��xr�UG5�W�,��M�)�J�w��]PVE�X�i�a�d�:��Fl`l�g�Z��,�oN�:���_�����-c���;#`@�>>:=4I�*B���z��d��v�Z�E�g[�E(]e��Q�ؗ/r�B���s��\4��2oߐ�3���#�(kʉ�J��:;�<H�=���N�zR�^gc���%4U��)��d�+�,�w��>E:�Č62��A6�U���9��xi9xJw�7�崚�Ƨ�����Ѡ_�����m$��w�e��������3tK'X�!��<��2(M�~+�z2=�ytyR��I}����dپ��|��?�yd*dV@�X������򃊏�	���Z�̖��-p�'G��� W�'�Y6fC���g;:��&��z��ՠ�JKL�Ÿ��	��|��1��:��z������b泘���F�La��p�3'<1�'�H����P��Ve�r���0���hy%��ܡQD���$]+'kö��곞DҰY��H�G@v	�/$�Dڔ���C�N;Qg3m�x��'�ηg8c䔝���`l���(Y2���f�H�*��':�O�����J:��˷�?�Kl�VE��gٟ�8'��P}!���A��4"�>8ɴ+rm�=p2�̛���ǫ���+mಓ5"�H0�)�A����Ƭ{��UT�I���	����VLeВ�VQ^d�U~�e.��^�_[�s������|:��	Qi�d�`��:T}0@��eg�n��L^��2���r��x�Ķ�<���iq����H�n;�G�ӕe��j://߽@�ˌZ���ہt$-Vj:��~^�,�����ܳU�-��JEz���Ta��^~�Ȕv�u��I*ߞtH�����/Mvlpt5S1�fJG�n���{����-��G��BH@�ڠ��5*��2���8X������L�dq�9/m���}e�-��!"���?5�[@��I�{�b�k2��x�,eIf���"ŲY�<C_�V�YXT��T~I���hdt�1�P��^4�y����u�g�ÒD%o�^jJ����3֚)���!.����A�@%p;�wr���dBKX��<���{����%��}A�q��ށg;x;�$-�NV�����I�h�JEd�H)yU#�t'9n�8^�~O�>	�Ώ}����4�LX����[J��n	�c����pq����3<�4% �a�6Ӗ�=�Af�9�|�K�t޵)�	�°Etp���I��]�+�>��ĥ@9�O� UO��>�k��j���S��D��:/!fz����i��?#�4���2mI�r��&����'��\@�,!qNuŚor�Ho(k�g��aT-YK��pT���h�5�3^l�$��#��n�Xys������u4��z��7,�}��������G*�~�>ݩv�9��� 0�x{������R)�'n�T!Ns
{.G��g/��ps)���<Ǔ#�Ti�x�i���4�]��zǤ|rpA�9����C��Ѹ�%잠�`��,�,��㰤�a�!\u8kXR���G�HZ�`�y��d5������9��h=�<VB���w�n����\�����fL�w��	��������1}�"�p��^�-B�4	5��D'��$f���pӲi����/�!��2O�v�:�Ӵ�ؐ�\dF���0��^d����	#���I� �Bh.5���tD�Ԩ�5�ҋ'��ν�����B���d�t4ߏdW> �@��Ϝ��3�w��5cP��\��'P��5ې�5>�*�0�ދ�AV�8��ę>1S�D��P�ܡ�<���*dC����%+	��Y��v虠Q|]�4��%� ��9)cĈ$�i��ܚuZ�@�H3G�X{��^���B���Qz�B����� ʤ�PKf��gV�Ϯ�5"Ya$2W��|fe��zF�Y>��v�Y�mv�|��ۄ����1��P
��cb�.�n��٩ ��Z�p�m8A��������w�m"9cm�6��D������@>"·�2��wf�U�Ֆ\�R�tR�]���	��Bg���'����P�8�L�;B5v�����f̴/F�Ze��#��"�Qi&`v�Z��5ڲ.�r��;B�/�dQ��BtY��������g>��X�������$�4l��\r>���Pk|��1�zx��)��rz���	�T�7�d��c�͟����}����6���1��M�ё��|�W߽ǁ O��z(w����т�z�`�l߃��0��
#��(00�w��    &.��u�e-҃I�����a��5(y�լ���qѫ������5[�L�`�"�Y�Y%�40f���,%�7�ϒ���~�D���	��PQy����'`Ȅ	�0��D9G��V֝}vM�K�k�:��wy)O���\_����)g�b��ԕAa�^����������K��D�$SYIv��'���7��vA�9?M���}bh�f�*W�#����T�]w�Ї�˺��Kv��'�L2es��ݒ�z�@�Z�ܗ���x31Q4�G�H��\'*�_���#?'I� �>��B��E$�N���H�@�O��"���Qg>�P5�%d�bwo��M�H��u���Yh�<8x�b��
�=�,�+����Z���@�<����"<�˭k�m�+>���-ϙ��V�֌�^�F^�T�8Z�0U�<U?\^J�<Ch~E�c"s8�
fw�K����%]y X�����-���P��.>o�0߂�\�&�hNVz��2�����:���n�iϬ�S�3��4eiC�p3�3��L^�Q̍w�.��$��N�.|"b+���7�����$!�To�SA��UA�i��;1�zek$�[G4E&Twx�d���Yk��/ͯ�.�n����I֢�X� �0������bT�YJ`
T-�@�F���6m��s�2�N������:�U�؜ʫ�T)������t$Jz���J��VQ����Pzoy޵�u���ͬ�����-$d�ӊ5>�lN�����dO�.�Fen%KX�ů˲�F(e�h��v�6�gǪ��gzfm�̓��$���\���̛Q�
%/�_�(���J8��/~1��\�P�,��Y�v���no������nl�^��n���=e��>P:uث�����o(M. l4�k�~�8/�%yF�Pg�yɱG�>s��<�pu�sF�S��oX����e������Kb}톯�|��G������A"!���p�ztfD��p��;G��
j2Su�znF��7(�� R�Q���yf�߼�s��'7	�V�"oQA��	8o����Rt�Ѱ�.�Nw�&<��@�9B#�4���ǅ#tI|���?�Ȯ&{[�w�--[S�6�ȉ�c���"jG���A�M1d�Q~=����_ϟ��n���ß�L�f�L7��:��3�UV�ނ䣃�.���+V�M���tXI���"�;�X��+*�*�9v�؉\���"u҅���f�MiwA�'	ԯ��g,�#c�<�`��>'�HU:.RՏ��QՃ�(PV�:�w�E{=�%:��8������b�=�19�	f�ҍh9���^K3�@��"��6�댜yzIL@��X^�MoTQ��§��-6��<�����]�4���9�J�q4�s��{��PU� �I�sUVwCYݬ��6`��͟o\Y�Oϩz�7�
F�yDj3��j���q5��X>C8��=�/�޹�6�+O@���U5Bs���3i���U�氅D�Uٽ g�eV�j �
D���3�!;��]M1&�P9�1�K�o�V�����Fs��=J�H�z����L���1��&�n��_���4G�k�o4	��	�Ǚ�ڍ��`c�� ��R�{��rǐ�T?ٴC�k H��-]�٦�mc��ʺ��A�κE����y7��h��DO�3Z��R� @�LO�A�X%�M%mG4�%H�9A���O�q� �- ��1�F8��i7t� ��#e	��M�����u=�)\�K��G>h��|�%���l�3���3C�I"����Ǒy�FO��h�Wf�������ص#�U7�4��v's�D(�,�ٹ��2��@7ަ�f���O-����w�K��d��!;���m@;��{FC��)��hUXw�P*n�*��g�:f�g �9M����[ %�wp/yP,\!LFA�a�����a��?�hOmvⳝf焚e�5�'PS7b��>�5䖘�C�f�F��u�� �k�<�M+nzfǀr��d1X2�,��!�����B�y�:��x}H�7�X3�(���Q�U��g���F�s̀����b���fWS�
uӑ�Ԝ�%�r�x��	b��t~D�!*��秦��۰:�9�f<|��"C[�	_�
S�^p���F�?�s�)���9��vW�u9�A���9Z�����(��J��o�:���
�T��F#�t2u��������A�M�<���3�\:K��X��YG�1�t� y���\�:A�\'�aQS�F$K	�.p���z�����)��l����(:;�.=�HN|t1ċ	w��N�@q�2tn��kd��C���(�`C7���h*6t�b����턐W������D���F#1]'��vk�q��s��%Q�����<��ۼ%c.|.I>r��3�s��#�ST�5)�T{Z���T��&��'�D#]pH�o�@;nA�%WlOΩ��������d3����尿��JV�ߞ/��&R�g/��w���+u-8
�l�;Ҩ`ѻ����~�u�\@����(�"������M�Q�׃E��x�p5W0��3�}x���y��΃[ۜ�5��>I�f�:�l&3"=d�1@|k�j�9l�-u�&�s�R��*^�&I�����D��}�l½���m�k�츃0:�F)��V��ë����u��߇]w��v����7�{%�zn���o����QS�(Iը��/^0x�ޖ�qn��c�]�ϻ­fc:���8miWi�#�b�'@�O�#u+��F�4P�%w����-E[I�v(e�E��C󴨎5�qZ�,O��Z�\���O\8���9�>��.��!&c�m��DN������W���F@�E��	��n�kwfQa�Io�}���T�nb|�J�w�·%�D�2��x�@�Y���	��Ii:�ߺ%y�)����>������G�"�E1i��`��V��(���'I�(�]�E�Q�.���e	E^��:r=�����j��#}�`�1N��>������/t�?�� �&��牜ˇ�5���������y�?�y�����uS�6��G4�Є�`�S��ҵ��C��m�sBua Q�31�(���w/���yq��k��i0�[Ke�#×�ˬV��#�l~偰��冷�>������{�;���"�8��dow�|�8�Q�OrEL�Ū�(mՍ�x��0ى��8-��3�q8lFň	������r�TˣQCʚ��n���N���q�i�Ѓ�JEؽ7�:��ƣv&�B�>@4r�	4y#��A��<��Kj��	n��`�a�2A�f"/�;
�(sh��^?��Dl�3��G�yT�aj���z9K�$���#T�n9B%�v���M��DiIqf����݋7d�Di�_̉�￻�����&��&g'��Jt?�hr9^�h��Q�M,�1�.@��Ћ��b�?���ws�"O@���:�l@��[]��k�ýEN+�y7�v��A�{4Oѩ ŖF=�K����co�e}�V^��2���1�0�5���6.܅i.��t穫�vaJ	+'��;7LKl���Î#�4��Y����FW::6���4�Ԙ��~i$<v�y�g��� �y��;�އ�y��d�3��3X0W�P��%U:U�q����0MM�k􆉘�M��\��s�y�C|.i�q�b�T𸺂��Y�8�eyM/0P��I��/�*�NJ�ɫ:�}����h�&mN%4;ƀ5<IK�y[snq��\�Ok<|��Ͱ�8M7��8�����o�r�6#Z�N'��İ���7�����u.b��y��l�%�V"�����,)�r�S6I�2=`txwi(�\�OЈ	�Q��Q#(n�A��W�E����h�N��"Vz��j�3�`n��#�CQ"dAWG�^;�t[�7�����yd��d�G�,"d	�:3x���F�aCکi���151Osy�]1b{�.��w�n�ҫ'�E����U5HI'��޶0 ���$�p6]pHΪ�l�a��'靽~��L��ք��ۼ�n�_�ɻ5;�]�]]��o\��    ������������|��R��/7��'��Lp�9M{�X�`�FVXF���h��.	;tI⋁/��W�����7߾z��5�K��J��J��˯��O���k۹y�`ĕ��{0{9�}���89h�9$�VR嫤�ù�u�P��&Z�I�z�2�[K)�����m����f����ֶD*{+"U��A�!�N��B��D&}U"���?������	*���;f�ħn$=�XY#��&j��KOqT_��[v�31�6��&wAHB�^��Ca5��Y�9A&p�d\�c��ř���}������n�`W=�x���N)5b�#��ҫ�@A-�7�E�0�H���:Đ���)S�,��f���9ی��\�e��$i���Ʒ�Y������e��&MY˨i-.ڼO�VRQ������g�Lg}� ��nzE��.��3C��a���:�x�Õ����i,��y��O�+�BpYP�y�{<���Of@��>�{�7g�{4��rv4���S�_8W���7?|��˯���3���-�����,���-\��eR&f�g)�� ��3��M�PSoO�!���!ȹ�kd���:���P߁94��t���W�[�@',O��S��.�5 �½��d���nA ]µ���*MD�Ea/��%�̰%i�s���1�!\3=o���U�EF���6�����,gg��N�h�������tH��wi�����T��dJ��4�!�'%�"��iÍP8̠�i�čP$E��W߸���=���.���t������QIx�A�y�f��fȴ7�����#���l"et҈e��AM�������l_$b����>�Q\�sc���b�w�w���A�)�Tw0xAf���Y�=���}�������τ/�#i�?u(�f8No
�2�eTްX.jx��+���-%͔��6�k;��eD�/W{ܦ�ϣa�n���n��5��Ա��F�K�R�e��y�@�(v��K�K|���R�e�3>{ۆS�ڃ�e�A+��fӘ8!�LY����8� TS�C_��mZ#�rxS���3�%)�(�|�K����y���G����W#9^��W�</v�o�4�NA�O��7��_�{q��x�&�|�K��@���hإi�Y�4��V"1qkAib����(�85�z�j��̵`a,�H�t�XQM�}��&��r���v�&o�]|T�l��d@�y�a�0�=�71��O#�I9��]6s�5b�=N����1G�\f��kW��nR@8� lkj�D�L;u�$ɸs�8JN������nDE$�
=#
E�> ���u�"��!.+�rꢨK�1�#3�����Ɩ��������:�5O���h&�MZr���Ǒ��e�9�$�k�D-,XH9���GrhÃ��ʩ��弰⚘sP�LA�Xj��H��~�MJ�ڎ�_fF���|I���$?7��ni��[�̐��i� {3.���G �f  ǘ@��@��M����@��w �f \��x��橆p`L1Z���q�"���.3����ܓL&+A2��l;v��c�6�IG�-�"�P1 ��,�x�(�A����l���vk~t3��P��p���K��U��Wՠ����; �q�S�K�)��	�so2��,l���9��!����P΍i� V:�7%� ����r5���Q��Ħ(@�
߭=eS!�g��Q�7�^� �^^m��o�f*�AL�4��������=n����7>�y>��nJj�v��|�j�>l����"p*�a>X������Pw4N��8���eN��F{�%�X��.��f���Q���o�~���G)�6��$mҲ1��5�6��I��,��Π���R���|)\$�Q�lC�~9��~�|!39��T�p����N��2I�#�ն�K�V=��|2S��)��������D'ʦZ�"�j�O�]zE�Z�{7�HN? �f���o�k�7�ǀƋ�3o���T)֓e�K�^wim���GT�`:0�
3���LP�����M�s��P>����<�����#�9��9�h�Ȼ<��E��x�E�ދ�4�`�/��T�_μ�T�2��j�sX˵S����ҩ�XR��s�2'�6��T���z{˻���QPt�9�Ձ#o�p��_ݑpn������*n�]���eC�<s-���SZ�8�mp�Yx�|�R~wi/�jb�et���)3oF^Z�7�	��)�K�5���W������Hf���o���I�|������)4�n��i�u6�Q{.<E����}�EB��f1؜�b��&�sQx,�F�-��]���y$
��2����
w�tO-�z�������`�N� e�B@ܖ_~�Ek4\(�ڊ�T/5�q8�p�fF�dY��[}u���DL�i��Cݾ?�V�LL��n62��?�$���̒��W��,\�����Wj���|t�4�!�F��^����>"��(9��n�Z��}tx���#�;�fMxl}�*�}�jD�-�e�܁� ����<kIC��`�a�*j�.�h�9��V�t��}�G�`��[.l�gF�<{!QM 5�k�u��)b���H��x��@�&&�Y�s�0�0!��n�8g��ޒ���4�O�L����n���=#��UWR�&���k�=�÷@4���e�qR� 3�Ae@w��\A����a�Jz��\�4��"\��wZ��#���7��|�����U��N,�vb����6SŬBŬ����wb�\�Ai�pq�V<��	��e�c����ޙ	Њ;�� ~��Ԥ�5J���z�̩m�uP�ƭ 18�Q�i�Ұ�&�ie���-՝��jXe��*k�B�ҌEtN�r}���k��9�U�y��/����k��/h�[�r�B�mN���A(��Q�3���������&s��x�4�=rfC��F�� ���6r�~�	��pU�-�5�9���785:��4�o>����Zs���+���m���]σ�,fI̛�N��RE6�qoi$y�{��l;	��M�g�.�M�k:�jέ�;�PH~xg���]��F�+u�S�w�S�c�=�Afq%n���T)��B�����9��1@�8��5���k�6-�[�JǓm%j����B��Q������Fb
�+���^s<�<w3W�Vu��8�����p����E�o�_�;�|�2h{GJ�ˀŹX��|���Q<�{�7�eBiK��(�	��3J��L�3'HWCr��z=U=�|2r���>��k&�|!�� &0�9t�Q���ܿ�Ï>?��h��
�{� ��{��5����ܧ$�V�A��x����m;Hp3�G�^�u�(����)Ν`U8a��1i��]2����=?��r�v�NI�0��+$���b懮#՘�Ɣ����buD{
��a|�29�v�G'x�2֣����#M�|���n���(���<&��/��Gf���}
��Þ��6�6C�+�)�9v(tgO1��q�N�L5���X��a.��s�O�C�W{a5�LG���x�S�<,>��`M���7�5�f�6�	��S*X2B��s�7��W�vN�b�����x����ǻ~������ۧ�~�������<p�袅�ne!1M4�6�qM�ȋ'F�, o�Z[�湁Co@�_=@�5b�]�(��l�Og/ⶳP�B����e��'���f��y��h�-`rFy8�U��$ӄTUl�����A�6��	�%AŸ��@��nq~�^�W������Ee\Z%iO���3���55��G��^#MҩMW�3�gkju1�,D�O}V�0ս�~b�60��ڴe)����\cMu��雧.`��5�e�󻶼��躼h�3�n�wS/ZX����3��N��@\���k�7tZ�ר��ȫ[ u.�ܿ�W��YkF�lV����a+ҵ� �[�s�ҝΓ?��G����b�%���З��D]��a��-+�W���]�GϘ ~ �  X�j�s�e���̥:+t�l����-��;���6ՙ�J�T�FX�t��Gb�9�����"j��7�2u	s���2��MX�C�H���r ��;��}�3i�@F@���pni���W.��D�,%n̟�e<ϱ�P��Nm_�����m�K�L�d��W��D�>��D�	�{�����L�������d���%��]OT�~�cN2*\�۲�Ͳ{���
�c�K�7/������?;�.6�a���f�.�37y�S3�W/���W߾|��;���l_�a�i�s�k�R�iD[G���w2��@�
��P���� �����˄Z��$Ж'i�Q1�I�����lNK��B7>}In�ѽ*dA��5>8J�w��f'���Ĝ#�|�ZF!n�ʏ�� �jf��^�ɫ> \�0F�VB-�A�>�p��A&ͬ�Ͳ�cP����w�����F�6�nf��
��������P��D��L#>�#��.ZO�v���P��l���uU�Qs�_3�x�Ț 9t
+	eFu9���Rz3�w���t:s��AWF��db����2��a�Ѓ�2�v�`^�c:���,��3<���� ��ϒlj��Fm�0 l��`6�H{!���i��E�Ѝ���ܴ�ϣȿ��p:eX�9�CT${��G|�u�N�&�xL�#l$��U����:c�\��5M48�yn�h�"b�����І{�����	���H'���$N��dn��<���sP�]"��7;���|���i������j|0t����}���q,x3�e�򃤩g-˘�|c�()��� �&�̄�Ǡ�8�J�"�8\��'�2I�[脗ڑ�<�|#
%�4�1�d����BCJ�̺�_ҳ��|/��� I�������?C(W��xy'15sh��B�R�ԧo����ڦ^)I����I�3\YJ��9��\���T�y���h5�
��ׁW���+��uzO�w�+���jg.��^�Ec��\\��O��Fn2�{.�!{Ǔ�G�b#�gX���	l�]r����f��2r�e�W�v��2��kl#�$MŬ91���^���<4�����i#����x�(�VLͭ��DG��0k��񈗙
k�<W���c��jH <��>Cj;b�	 5�j� ���d�9�_�}g������n�ҋ�5��]��C��wrw��뵒L�$�8IA����Y'V�8F�X�òj���`��j#�]��F���c�z�L�t"�Nʕ�M�O��f���d�c�w,��R�;f+��p	<���;'��Mn��9���ӝ�����q!�6�Z�Mˬ������T����vY�;g�?��1�G��obz�V�h1���/�K)-``.��7r[�sq�]\�yC�st��d�q���m���N�&��:�=m&sk�C�ZA��滵�n1 �:�<d�w�{���>�~�y�'�A��~~*�sG�`�.�HuG7��GcV-zϦ_@khrB����x��A�YDe�E�}�O\Ѭ%�X��v�m�ĥD�S�����?��O���=�V\�e��VHUn�N����T�t��Q���p��(�ʮ	��tc�l�dn�]a��jfoԺ\^�<z�*^h��eDρ�� ���̴Ϡ?�
�K��5�OM4���k�:c�������\Nm�?	��M׸%l.N��M���V�p���/�v��ZM���̦]���$��#Gy�{�&#���cQTI��t�^��
�>�yiA�R{����e�/C���� 3-���H�i3�1c���ݼ=D���i�m5��f�,��#�!�x����15c"����[��X�Ԉ�1�;���d<a�	G�@�fD͈�f�9/����Ը�2�0_r3���ڞ=�ǔ:�ie��o����Lk9!����x�@����V��&+iˢ��u����̖�+m�엠.�_*Et4n�\�1��tN]FY\(�g�0�SJ���M�4�%�0]nV"d����*7L1Ț��|�+�.�&��+D�J���Ee>�eD�'D �E˅����;�8���B�t��S�|�ھ�xA=���&Ռ�?������N�i'=�;c��>Bc�95��`��1̣�J�д�� 4f��P������.�|u��b�:Z5��HK1�U�b6L18���PW��g�\p��]A�q��T��7�|	�D�%�DTw�YufąY&�y��/���R@      	      x�3��M-)���O�LDfp��qqq ��	U      	   "   x�3��M-)���O�LDf�qp��qqq ه
*      �   �  x�MSKr�@]�N����'-1F��lǕ��c#p���ls���K�cUeׯ��u�kC���Z9.�.XLχ`�Z�I��R�RuCI������U�3�I�P��Tu���R]�b[6�qYdԙ9p�.GjaB�ģ0�Dv��JE�F�F�o� &���_���1���n~QF��zTMi>����(��8Hx��r���E��.��~^�kB,G�Մ�Q��)��T��� ��� k5s����(��~β��,�ŵ1a���+��Gն�t˃g92}x~��vo!���F�1!�p��@���I:Ǉ"N9plT��td��]3�L(�Y�P�o�GoF�T���>TA���2�ZU�tR�4��7q]��6n�P�"�A/��f��/S�w1��S+���zT��_)n��YS��iRU��+	�6��8O�s<�)� <5����EO��>�����!`CF�bZ2���wL���Q��?��仢����)��F(��$���_z_HA�oo��.ۖ�B\�]�H�Ȋ�4�I7e��F�~�ñi`3̬xy�Qv$�us�_ն�莾�m���8��S'x�/r�M8��2�}���Ttg5��!�|[�H�)⎝�ШxPrRإ��e2�[Py��R�"ה1��"�O6ceɉЏo���M�
x      �   �  x�}VQr�H�n��x�� �#AY�
p�u凕f-bY��r~s�=G.�݃l�[�r�_3o^������嶨;ɠhw�?`Я� ɇI�Ic���٘^���}`.�]�w�t1	ܶm�e�ɝ��V���-5�iUhch�p�q�_�a��C��w�.k�������Eb�0dxA�DxւSڄ��	�m��A;���}/�ؤ/,��3m���}�y��[s(�����7?e^ʺ�G�&,�_Kͯ�C&�܂���>����a�1� ��x�Oq����9"@��v�l�Z�:$��
b� �?��
C"/xt�L��6�-[E��:�����%*�MS�L��)>�s��,v�@013�{Q�EU@��eU�N�*U<ʃZA�2��J��!���C�ʢ�"�Ƀ�˶CSq��aR)�@,��^+n�)�����nNm�����cK1U	-��}F1U���ٗU��V�T&�ܦ�A��&i��K�]�u9���L#��&Ȋj[��H�^�?�~D�`�����%�K����U����R�L��]�H)�:�,���S���}�:[�b�Z�Et5ULtY���3��_rYc��Yq".���KA �6�I�^@t�&���X��ħ7Kp�>B5�$_&�%� :zws�M��j爖[����6��CiB�_S���М��`$ ��n��V������V���@�;v�kC��'���D�e�F.�K�*�~{��}QjηB[������%�<�ڷ�"&ȿ������M�S�U޹)��R5�!�M��CVa|�4��l�Q�l' ������趨X�e�S�&SH(�q�*���܏6�Y���ഛl��78o�MU6�<X�D��w᝖�n���@,^iy�uj��pW�O]�T-a���ޕ��z%]l� �w}�{AتLa'6uI �dV?�F�b��|gK\E�{�i	{sF#�Rz���X\|f+�Pz�Ӛ
���H����ۥǺ&�NM]a	��~J�}Z7�T���Q�����1,���}I���1b��C���Pb �~Ź�S��NI��0�:gX< ̶�r����D̙а�Ï1G��{`������a=��ڤ~�G
���٫��7�̜}��vqJ�M����p.~� v1�ްჇ�k f>���+�u6Vo��
�s ��~q)2��Xd�S%�?�N�t���،�6]�̬s��&��Ѣi��ȗ?����AQ�       �   �  x�-�ɑ�0��`��SR.�#4�òi ����?��>/���*�E4"%�����a��T��/���"6qC�1k���l��X
`=/S�����t��P���j�9��K�A -�I` 0 ���[ҋ*U�Y�r�`��0�%A�'�Z�5��b�zo��2��E~! l�I��h8�����+��6G*m� 'ڂW�7�ڑ��"f$"��2��i�\���#����
�ih�9����t�=.�
1b�a�����PM���r�0	�� ���ŵ���C�*t.�Ʀ����=�.�Ս�v�r���jNS��
e�ȽuSΙq��ʛB��P�1t�9�3l�9v�C5��A�V�;�o�>������� 5��      �   �  x�mWKnG]7O���j��3�šEB��mxCH�L@���#�AA��et��$��g��$Z󺺫����1A�mn6��<��><��yw���D�GƔbZ���9+�F*i�a�2E�|�t����$�l�tq4�V����'�����&�e��u�(�ͩ}���X�s�l�]�ڟh�������w�U��2F��%�m��h��A�w;9���]u�,ov�.�dbca'�@�k���X�XMU�ʍ	ʙ�ޮ���6����||?�x��J�/���quž4�D�|�O�ES�t�bX���m< kŔ��TTǺ�OCN�G1L�.�;"�ݛ.ۦ��Y�l��K���K��ߒ�ۻ�}Ώ���9zQa���S,�l�<�!�~�E�x�ZD�r�s�w��l��,���Ό]x�b���p�{��z�?y�R�X]�N���jt
��Q���E����5#���k�%�ڧ��(�/2E��8[��E����Ul�u,��؁Q�	2�CqY:ʆ�YE +i����U���x��z*]�:@+���FǮJ�K=7S����-Ш���� Q):͌p`B��-lG�N�g��)}t���[S�� b-z.�7f,�=vd���эG7�qsi�ZQ�'បЋΟ�� �=������Ƶ6���~O� ��|Kb�	7Æ��s���SE��%+�ٝڤ�)�����ŬV�1��D �J�����F��s`c^T0#o���LV�@(+d��Em�#�c(1��B8���Q=�!C�B�����)�j�����,)<H,����D�h�;n�� �k��P�aH�M�#Xv q��#�?���g@��vZ��q��t���'Pr��
��l����PM��[���(�Y� ��l���Z�n#�1o&�JjV��Hɼ�� �x5]�-N�.��2�(��
Q��Q'!�-���/ȼ������((R�^�%	t���-�q<4���8�����I�+�G:���LVh��杚�;����o`�&��q��9<���F�G�\-����|q.������7D����&I:_�;�N��P��X�$�t�E##H���k��PZ
4XA��3��R�Im��VF���&u=�"6#��#�ӧng�e�����b�nW�9��K8�CM��K��^�i3��ҵ�Mq�S���c*�JS��GE-"��DF�I�	��p�/]���;���^&2(0��O��r�p�u����s!����K`6BNs>Hg�- Ņ+bm�f*�r"���
��H�D��v�i�����y���x������JR�1��,dum(��fHbȤBi�,����Xd������=���k�B����$�Z�5
]x��_-d�bR��_���� Ƌ�]�7�*F��!d������-�Џ%q�ߗ�8�?!1��ȃ)D��YoK���k'���~{���K���0Bc+��zV��ݍl>n��{�|<|��o��ʏ_�7���#ՠ>��b��sn��i��́B/P!9�p���3���w��kP�O{��O���K�ˉhGi��;y�"��B��0W� �8�U�4�c)E	� w��x�5��hG��U-O.ت�~�ڗ
/�C*�s:xnH�:,O ��[�#��`�͌�R�4q�1��У��      �   T  x�uW�n[7]S_�ucZ��՝,��`_E���	�l�1`Y���.��B������^K�] p�s��י�FY��n��EQH��WV�u�%�YѠ��YK�h4�����R��$�̺FQ�`�e��٣�xc�S����J=Ev�x0��<
TÛ\O��=��=�;J^�j~�dW����(�R���32�h#�
vۀ.=S
.�Ć�N�.Jl.����F �_����a�F8OHl>8�TK0� �rG��7�١�8��T��c���(�i�&��p�T)~`�K���Rg�h�?���D�8dE�g����������f�p9^���6��t6��j���-P�`�K�������f�߯�D���\V�OђA�)�ّx���A�*��V��YkG,ރW ���[�jW���t���ZN�Bb!ܻhufW"	�y�m��=D&��188��J$��V� ���zPK�p��fO䎨VZ�����X��q?܌Jt��a�,=J�\��=~����3���Q�$@|���J�$5k��p�jp+�YMW�㮐��2�����0����6ۯ����d��F����_��~֓�f[QrAl�#k�elo`n(�Y�7�)�l\��c{��Fј��0����$���Yx�ޘ�.H~�06�O�:����]E9~������=��~@��V���<�"�C��V���P��!͕c;�e%�C�Q�Q_ï���:��Ǚc6�8�5��mǼ���rĵu��9`D�q��`�g�Q�m��Q�MZ�k���(������W �G�����D��T�hE��`�,��En5��ws�#T^�1F,�	�'ߗD�DI$�F�p�,�=�K���ǥR�Hud�f `R�ķ�����PT������o�p�JV�?�pȰy�~�99:V,���X�&���"���k��a<G0�abE5�́yɊ�O/�| �R��c��r �k�P�x��@*a'3A��n��z|�<J1��ؕ@_)��cz�gf���wZ�DV g7�z�es�]��������F.̩^�d��as����s����+,@g+Vx�<5��*R�(��� R�������1��J�z�^����[�?GJE���~M��qs�w�hn���V7V{4������6�%�(�3�roP���}[���@'��0.��M��k��y�f9��kF�*_;��;��Q12((�7i���N៘��m���tJm�Ni��)�-�p0��5���=���V6]1�U0wN�C�����9p���l3jU�>Jҗ n�ʳ:+���\��EJ"�0_���OOaϐ]��h� r���"#)f�����Ȭ���E�x'KS����W<�a����M2�B����R��n?��Q��˯[�ǣrTIR��8��q��l#��O:,H��U7w7:�
S���x��+�Z�x���5��B�*��3�����Rʇ�*��D����O��/Gg���x�+�	�z�3���ա�r�kS}�³ї�J|EI��^�Q�'����[%;_HW����.�<����q�ǯ�j`*<�1P����_��7x���/h���UDdWS2�I�x���ߎ����g+      �   =  x�eUKr�@]�N�*;���a���Y���Je3�pB��T�9I*� ��.��n�g���g��׃�p[��C�7�_��|܀�P�U��L� �I�X�>ܭb&��8/��/���|[^��/�´{'kǪ?ԣ9{yt��9TN_}1}gϕ~:�P���Ev�
��
��GX�-���F:pQLYB�0�	"�*�`�"��PL���e��	%�p���W��3���zOUF�!�Q���ٜ~�m�4l�n_wĳ�p�y8iS���O;%���}5����l�Rg���e�\�].�ɲ�j��L�͔�h���}�X�c�}v_�;I������i�����i�\������m�'{e�d�_��3�rE�N)^;�7�-E��\��}��Ws�>�ev?�x0!}N;5#���� L��¬>�؀c��r����	E��	��L(&�hrSL""lAU[7{�m<}X�Y���4�(&>W,`��$^���?C.<�����G��s NE��3�_�-�
�G��@�	���ts�2
�.����:�ϐ�����0�r�p�Z�%g3��^��y��7Se0S��gJ��b��`�ք%�z�q��B�\��H���S�~6=�XD��!4a��bD��4_�F�s	i����<���W�<v��I��9�)�ih^9�iȩ�>�~�zѣ�w�O����^5%�=ŕJ����P��8V���$��k�O\��Ptu��76��WN��g��4���m�����?1(y��7n��R>��B#_<b,�I��/2��`f�Ԋ�9nE�~��Ǐ��f*؞}\.���Ĉ�      �   ,  x�}Vˎ�F<�����V˙!)�ȕH/=���/\��	h�E��K������UC��:�"�j�=�=5=�Z�����x��ܗ�G��H�Q`�C�n����n��ծ����$�+Bx����ת;�R,K��m�s��D�H=w�Vj�#��҂�Oh@M�c�PP����c�iC��py!8
�*Ä��L�"$M =?�+���t�������u��f+��P���)w��5��1Ć�E|����Cod�Mc�F����s�C���ׁ:
���	%����މ���xķ�K�n՟�u�+8O��&V�~L��,�e^9Oכb��"�%�zO��5���e@��qr�,(��HT��������s�S���i�m��B����|�ʳ�zZ�@�,�j]�i��=��	Y*։:#zinB�;�q�8T��T�AXG�l���Q��h�:B�wՏj�bR�Αr��(��I e F,]#c�1�#i��`dB���:�%���7F�����xW'���tf�	:ZG`��	bGp#}Gp'*����#��"ķBP<��-e���YK����0>�ˇ$���D ��Զ�7��嬌\.-� 	��� Вc�.������;J}eW`9�5��U���|3rJًw��A�l�![>�Os*�����cN�3L�ʎ��k%N�6���N9ow���|��1ch 	(��~�,(^��B��P��(�f�K�O�φV�����ih%������[�=�gf�Y��n�Wu����e�吇G3��D}���p�����+�l�+��f�_9�N�/��e������
|5� 5 P���<��b5� �i��z������K�t���O���X��#�nE�M�����4y���(Q�H-�������A/{�+�?|C����Z��;�8�Z��Xy�2_�8�M-G}��_f (JFdYuՍA-����� 
��R^_�ƺ��=���R�~��@�+t�+J�{���o�D2��?L�T �����`np�QR���EB$5^�O���|�e<��dݧ      �   H   x�3���W�400��4�2��/*I,�LT���M-)�W��;�K��� $mV���`hn`d��i����� eM      �   R   x�3���W�400��0�4�2��/*I,�LT���M-)�W��;�K��� �������)H����Ȕ���ȒӀ+F��� #�      �     x��V�n�6]�_�p��쥛x&�72E1F��!�i�U��_Pt1@���U���~I/�%�5��<��ymI%\Q��h=����Q0˖h�5���b��x������9�x*�RY�E�����;+$��b%����޿u�0@����j�T�-�a;�I�P8K�#Ä{��2�!{ş�� q�0E|l�w��2?֜�#s��1�^ ������}GxI*b�;�����$���F��h��vd��\t�7�U�G�P����,�����%��,��?��`I��a�Z�L���<��4+-fC�^(h݊S[�0�+H�g^L�x˷��Rz�Sր["	�nXK "W� ]�vO���@_�!O _-$s�O�-��U�5�D��Jw�{����A�W�Q��w��%-�	��a�<��������u.V�!z׽��vG�SOW���8Ew�'���C� h)�^˚�� �;R�C��,Ɇ3,�b ��v<a1$h�[���S\3IBTH�	4���3�-m����,��wN��Lm2��
��k�;�_��\F4S6�i��O^��Η�y���>�^^���a�`}�S��פ��a�I�-�͔�zDP?u�T��l����rAKX��	�*ܡ��J�#τ�B�A?��1s��ւ1������W���{jc2�4{C�@t���O�����/ ;[�p7��<[�dT������_��x\�Y\��!��hz���ř�
k��:G%Oa
�>���^ã�6�,G?D֫r)A��dm�	����Z�B��lcyxe0�f����3{�pa.����o�4zI��Uگ�m�(ʠ���.	6�L��B~�N5����Y��t�������{���(pQ��G�F�Ǎ�Q량���8����|�ҏ`����:�l��s�P���{K�uL��DI�)a�M��=��V	�m���lw����'�;Ija�ŋ����is��	����|�f�}ܔï��6%�}���f�R�z�       	   �  x��V�n�6>�O�p ���ɺ� ��M���m�	Y�R�v�SߡOP��@��6�^�&�$�I��Xi� 9�Ï��0d^W^7�S�DoFҌ�r�k~�+c�(�_8��f�y�Bt�f"$4j~"&"���Q;�d\���GH�����Y)��t���l�w��1�ƫ��p�8>��+���)SF���J	F	�R�R�u<����*.�cVj�h�xq����\���	b#�T�*�_��� ,I�.U�R��U7e�����?-��%�	Ą��-`B<V��B��[�!�0�A�z ��`��#,�5["�Ʉ]����1���f]USP)�>�v�#[����ʛZ��xҵ�*ߺ���?R.�.��V+ŧ�F.�eP��Bm������ ���9��oы���h*t�d�}���W��(8�����j)�)�v�ZZ��uo=&�S6W�k��"?�����2��!�0׳7��"���nn����M�P�[��	�o���3��N�!��*W(qA��������O���r{� JV9z�Q<��#]��m���k(b@5\��� ��vm�sL���G���� m)QJ �Ps����B�M�/��T��`^cAR~G����� �)�N}��x���u��҃��.w(�n�#
)��3VUl�F�6����V j�!Fr��k��xARӌ�æ�r�� ���05L$A]/�rx,�).�vkVn�\/�}�*-_��AA�}|�}��/�1��w�+*m��!wW�|�Q�,a���od� �!1��R熣�4#ޜ}2�,N�Ʉ�������o���^X���R�&D�ЖPC/I1mQpҷ�]��`�@֠C�&�ߌ��`�>�{������.gI/��B �Т{�/�I��kg������rk�t�H��?$��,[8��eelY�E>��=P��4���E9�v@I|&���J�4�����D�:e��]�|���*��k+^瘆���;�����"�W{͝ʍ*�5"�YXK�_�*�l�@��y!�$;)ScڜI���3��hK�)q��1'� =��P#NԞ�g�!��z��U�1�!1�>�z�Z��Z�ȮOJuJ%�$�CMԌ�@�/�^�7(TxUX�ǁ��XFo�,f�jm!顒>��}����-��,9�:q�@]�q�g�!=�?^�F���ڵ      	      x���]�,9n���Fq&��%q�������-\��}/���E*2S�P�
�y*��!���E�o�����/���H��v�ƿ���o��?����������d��`]r�I�S�ъ��9���?�����,��%��^�d%i��G����z/�t��o%)m��Q��~�(�*��{I��̿(�b?9��OJ9��G�h�y�r)Z~��Pj����OIG�z/Z�E���۔ڥ����C� �{�~/�� η����Ye�fMɞU~����?�����������_��?�����������\��h�Y_A���PI�{P��O@��}�����+��'z�L{�[�+�{0�W�7ᜠ"�&�	�3��q'�v�3@���'�Q��{���^9D��^9D�Q�^9��+��}��{�����F9�?�b��ϱcn�c`n�c��_�(g�%Ap���Q���~Q6�10#n#�:7�1��_��@%�#	�{�Hj�G)N��܌{��Ժ׍�f�{݈�J�{�HE�Z�V��@��M��~�+g��ړ|7�o`���iQ崍 �Hqm# Q崍 �;Ƕ���j�m` ���mz!�Nɨo{���p�����S*zǾNi���{ᔎ���D8p8�{�T\�G�p�����F9�c)�:��Q���>Ǿ(G}�I�З1��_�c]#X�)u j
 �����X+�A�`�Ic�<�U �,f� .��<  K��et� � �iP��`��;+e�( ll?9Z�;F�n�Cm�Q�p����Z��U7��u���IپH�ҵ��O��1��o~�8(�H��tt��7s
���MV#��_ǿ��5�u57�2����'��S�NҖԱD�'��o�jq�� Y6d9eHʞ��uO6\CmKR����d���P9���y��Af؞��� �;��Vᝆ��=�ʄs�������"��k(C	�^B��
ʱ9gW5�ǳ�ʱ��2\�2���2��2�J�2��2�
�2kי��g�������K��Pg�.K�X��o}ʔ}w/}����皖�*�z�����s������k^��ۢ��S��S��<���}iӲ�ê���۴��#�z�4���ȭ�}~9�P_?�U~�)mJ�{�[�L���.����7�w��|���7o�����prmj��l[��.]��m��?et_f�jڗѵm�0-��/S�g�y_�-�x-�2��m�LK7P�L^�g_Ϲ�g_Ϲ/�m��sY����\�쫹��#�}5����Ws��ھ�%�g_Ͳ��m_Ͳ�Fm_�"��j��'��wL�܆�~,-V~�?��'_�:=�I�c�ttF+V걥��k.����8��x�KAK��#9f�hva�2��7�M85�'+����'����=���	G����!��.˛|��_m����QB'Ip��(�	%�{	yu/!ʸn�z�{�a�;��a��^Bp���O��DA��ӟ(H����	}c��
b��MA�����vN7MN��u���F�)h��M�n
:IXAt��$|Z�Ih�J�n:IX�t��$u%L7	9��[�K��Ζi���C���:u����<d�k�CC�y���Qo˼א�V὆w&�א2&7*��u�7՟\|K�7U�,p��|�Ps;=l�+����!1?�Mm�b�0G��I�D_m|�qs��#}���'TY��s�p|������p��5'�@�%~'�UV�7q�߳�o�$�WK�&NR�{�o��v���o�$ᄒ�NC���7w����]{���vˮU����Ȯn-���N���
��N��#��t;j�$d���x������Zr{O��8�����?L���0m}�U��cdC��c�ȫdd|H�$m�
���}H�d��mv���ȯ�ru�>�bR"�$�VH�s�#��$e=�l�yw�m�؜�S��˴�����Rf:K�2=�R��e��)��2yy���ˌ>�S������Ⱦ����j���Sf_�:&�2�jֵ����u�3��e���辖m;�Sf_�vZ�SfW�<�Ⱦ����(���]%�"e-���QF�G���(��Pw��-���JE���䴫e�P�O�]-�2U�2�Ze�~?e��̋�r��2�f_�6E����2�����̋0r��r����9�R=����Rf_͹�g_�Y�2�j��Ӿ�-��Sf_�Y�Rf_υ���}=[���̭�-�萼>s��l���K�~�'�~�	���&c��m��3ǖ��Đ�헛�H��Us���vDƶ�>M�}`DFd�!9ȿ��d�E��<�LF��X��9�'���l;I:
��y�P��DUh_�k͝3�2,?���l���ڔUY1�#YƜ전�$s.��)c�A�`�s�?뢕��̴0����Ӝ[ e��EK��̜��,�9��*�i5�Ce�R��c.�B6[��-������������c.|!�)h��%�]��ht���Ն�r!�¥�ː�NJ ����"���f�MOPC����ܠ�K��h�!��I�4�9CRҝ�:��]E�ہ�"���;)�D�r�P1��z��w}'K$]}\Pt�d���T��H��#�A� e��=��;�m�{����d�=_�i1\�5+�I�ը��,�i��NR�jT�I���s̗�Rw�����#���%<^�}Ւ��l�C�$�6i�Cy�P��Z?��6��u����6�?mR���o�{.
D�1��=�^ζ�;��M�6����+�d������A;�bd��⫝��A�ᨼ�moR���~��N��{v��r!�ǜw?��$�a7����"�E���`q����vXK�Z,��t;���UC���X�)�j֗�9q��a�i��鰘[D�@�5Θ�%�+L-��:�U"�k�Vq�^%d�c��@��9Zԃ�q��H� x"0�F��<NJJd[z7��5K�Jȣ[�j3rTJ��a�3b�;�9>1B�QI%���	NK���-cQ����ȱ0ϯ]�i��tip˭�fנ��$�s�7V��J�r�Q�g,�#��c�n�w����K��$�C��X�T9�e_^"#�Pda_vA]SlL�YA���)jR�2�kcN�!=���@�Tm��-��I?
\a�_8��9U��j~/\��i�6HK Ǽ.��0tR�Hf�2s
�H�!�C�J����-�1�,�-������geqo����
�Ol�t�}�[�n�<�,��I�up�\Kw�R�89A�-��	��a�V@e�t��>ZY�\�5��k�w����=Iw˫��d�t'Y�-Wƿ���y&M|�����m�(��-�n��# ��Q�Q�⩔�b`���RJ$�i;tK�@ʌ8��H)5�Տ��S)-�V�G�)���Z%�ؠ�C���}	��\<]#����`ܞ��{�>6���x�F��	��y����(�"��o��O�$��~�EC��s�Զ��Fҿ�{��ԝd�����Т�v��R=��'5��Շ�|��;I��a��')�����ӝ�M��Q�x���|�oA�x�,��I�ƃ�~�t')n�d���,��+w�����Y� ;�u���o�vyאF��B
����Y��m�<i�ų|����gQ�yM~Zzj��R��@���4�v�V"�=��K�@�O:z��	����gXs��IrG}�ś=I_��׺��~�~�4�i���N�6�-��oqu�'Cٷ5���\\ݓ���s�"2�In���equ'i��Q��GXL�,6Jp���IK`u0��.�E��:<v�I�e��?,]#�,+Xы���i3i&lM�Hv ���@�v�hM�͑�/�T��@���.%��k9�:��<��NH[$��?m���<�"e[�s�!БyYL�If_J!HR �L6��qY\ݓC�xϊ_���^HE�Sdqu')>�$xTD[�$�͉<�.��{��O��U�&��������'�}�
]ۓ=�C=2��a7!��~��{&�#�v`���
��%\�ɞ�y��D�PۃȾ%�� �{�=	�����N�&
�$4I��)����⽄n�
�%���KH�Ux�!���^CxHx�!�K    s�;Ƀ&�;2cr��A�0�7%T���+_����}Wn�.���Q	��ށ[���J(��ԃa��~'�MM��j$��������䘊+��B��Ao�J�;Y�l1���#�}Ȇ�Rʝ������2'񈬑T�5�6�r��o
_��Pg=s�|'	[��������d�xn��k�8�F��L���>eq|ORm���eq|'Ii�a�.��IZ���e�H�d�����8���x=�x���0�>�zL�Hֹ-��S#�,t�a��,�o���ٗ������wL����M��teq|'��䓪]�7����!��kd?Ox0�R���;+\��ӂ����=I?����Z�I��5�6"_����wp�§};�i��%L҅��g}Y&o��M�pTp��v|ߤz�a��Iv7�ڃS%�v|獂~����`/��|�d6S	�@���}���+�<����&�~f��4���o�ӣ���)�ٿOܞ�"Y�=3�ݕ΁4���2D�H��q�I�0U��J�HfwxpG���m��Po�l��R�#����d ؿ�QC��Ǳ�QC�on|�5�}?p&��NF�3�xB�QCv>u,�`��hԐ�}��Zѫ����l��Z#i��G�Km��y����!u��F|�j Ǘ=&5؆�)EҎ1CUk�H�{Δ��Id�x�}_MỴ�������T����jj�hh�����@��hHN�XR;��-��}v8���_H��=ۍDh�S�^H�:l�J*�7)E���T�D�̾l�i���.uK�Bv?�Px\�R���>nO�H�&�y|�EC�����v�\4�LCs%�����XV�d۶⫰��"�<��!߭2ERg���6ϖ�O!�#Y��{[.�l��)�3�,�d_|
�(�\�d�>�f�d��k4V�w�t 'C��NJyp�������,S �'eVUs�������*>�C��7K$������I��阬�vJgF��d��4$���*!��K,'	~ث��E�5��'K
��n��^�"�vKH�N�\��;�����Q�������ZJ ���£4������k�{����ZږTx�Q-=��9��y-�>�	$Ғ��Q<ԘB�9;�c�p$���B�a/�t��X�R"�yv�_U$���Qax@��y�	ONe��L�[汈��&U�Rƍ�1����"Yl��@��V
d�#Q@�`�QyΙ�~�5GR�%M�˯�N��<�_a�Hv����ZY<y(vvkm�ԙ�7g��Q��]��&[�{�-G��Cu�g�2g ���Ig|;�D�uqW�a:��5�~��8��U}yXA�Gu1�/�;��p^əN�;Y���l{�V���|!ᵕ��etxМiO�ҵ��W�H��J�V�^���}�!�(Cr�{�@����Q�^C�!���!�<��^����ac��M	�TXCzS��
3^W�)a��+ӛ&	3�W�)a�x��7%L��ޔP��2��u���ࢿ��7�y�Xm���5�IOl�5���fj)J�/�~����(!�q�p��R�{Ҝ
�n)J�\yp�������y��~k�*�n��61PC=�b�oC|�7�R?7e�$�?�;9���hD�����9aɁd��3\���5&=�3�� ����_�������$���Ӄ������P������*��Ν��s���i[ ��� �)��y�����'���JB궸Ɠ4_*{Z	D�Jz�"�ǂ�b��Xx>"���$�$��oJ$-���a~����o�[m��'iac�
���=��%p��X#)�
�S �>�C�E�I6;���3G	���<嫄l�?����K$���řS[�J�xN+~"�\#�m.0
��v'm�F�|���x����K�z'��wL~%K����l�"87�7V��\Xf�wx���H�;6=HF��UB��|c��Z�;i[�p+���2/&��"!��<�u��j�q|�<ӡCP#8������'�'��o�s|�2o�O�8�'����q�����၌e�X�F���d|��-��	��M�g����$��0��I�+� �@���><��D#��̄߳�H��EſY/"w��h,B�@ڥ�|z-���n�N�'j��I�#�@�Y!?�~�z'm��c����OV{�k��=������COp�V�)��N�$M��`q�Or�5��$=�;�8m��=�yZA�8���'�wV(<e���$}���h�U����_������Y��4���&>h=E�n?�i/[� fO��adr뗚��yh��b��I� �zd�����k ev{0�r�K٨ �xı�M9��ؚ�8Sz	nN�wһ,���A�w�Z�oz,μE�{�p�z]�z��q���~�J�l�(�x���F|� r�i�B���(�25G�/�jxz�%��<G�4�z�����(��8���LO��,cU9��}��9�si;E	e�o�H7��G�s@�ś�d��	��}�����o��exf�/��I��1��}�'ɾ4R��o���w�.Aપ/��I�WZvD��;kzB%,��I��6G��}_��7)��>��M��'�����bM��!�O��Ś���Q�}žX�F�[�3�"k$݈�Fz�H�)����#�~����՛n>6L���}��_�x��]��v���A$k_��9�F
Wr}��_�X�-�[����5�"K=��|'�$�OS�MD��&$���jM;i�;��\��	Y�������lF���e�t zU=_�"~�_$����� �H~��Y~�W���T��q'K$��l���s���7�p�s$�Q�y��Aw�Gx����Ճv�~S�}��z�~ӝg�;
��	�_���ze�Մ�ge~p.��&�$=���	�d�=��T@J$���`����"۝�.nt��쁴���#"5�~��)QB���aI�м���C��mBg;b�<=v�ѕc2��};�?�	�&-�����v�ߤ��W<�}��o2�Axi�v�'9oL������v�_d�"N/��.��a��=�.��6�ەF��o�EZ�A}�%Td�Խ��d�9����z��2K�2�u��{I<f�P��FЧ���x��N�}l��z$���C3z���K��]����'>,^����1u���EB�kL���4>O���|!��
�fDd�$�^��)����i��Y#�m���9�l����6@D�Hv_���^H�����b�{O��w�0J�t!g���3�W��&ϕx�����W��Sm��;iI��/jig'g��<��z!��!�+:[�{��ߞLx���NZ�<!��lӴ��t'�����`9��
�F��R�Z�zt��Lӵ���8X�	~�$�fR2��W�������Q�<�%���@j��jS-�� �2�ɆƕK gb��Q~'uC�y��+��{�$��L[rNﾓ��N��	ͻzr�E��I��5ݿN�O�#Yl'@�G���J�K͍˝�/��_����wҒ�c�P۝�c}�H�{>?��X�����(8_S������̾���|�m�NXT����{��K��ڜ�u�+aYyfу�Z$=hD��ơ����aoK)����U�;9z[�ME��e���Wʑ�9[�c`��c�3�(����Wz�;�*�H��84����D�yOb!��G������/)Ǫ=I8r���ш`V}ͱ��q,��z��z��;�{M3�ϒ��6�Q7U*$K���XTྴ�([K�=�xp@K�����C�-���م��BWth),�V^��E(~S^����HҼd�{�E�g� �{�~'G_��qj�H���W8�"8���q**I��c�U��3O8�SQ��c28O��6t� )�wJbRV�q�}�"���k	�X}�i�2�Y�*��et�-ݔyݍy�Ym�L_�g5��2�̯2�/��W��/�$��Z�e��<�Z����i��������ۄ|���(���,,����zm?���7[
�ɰ�hFmIO�+�q �ð��0�9c]��k�����B�zY�D��mNL�H�T~.��N[����& �	  p7D[��}��a�iM
C�_{�d�MPA�9�?�	�IϷ���������Y�;�d��e|}	�m�Hz�&���H���
�Z��@�ל����z$�i�qy���	��/j������'�9�[��V�#)�jwd~���ը �1�� Oa�FYl}{p�jT��R�E��x�-�1�
��j��Z�QB�,����R���b��S�)a)%�h��FC�#:���ΏR���+��w�%��W|��VJI���v�@�fU�=w[��S��*��rSPZͩj�&R� )�<�P�Kiu�^��=�)���-O�&�D4�m�pGi�z��A�tJ�ΐ�>}p�@���T�Y9�~G-�-���D71U?�7C)�UL�K��`�"I�����|Gǫ�\��9�~���@�e>2(��Q��#&��������S[���yG~н,V ���[h)�䦴����QM�RZ��&�~i8è����]-����l��6!7��Fy�9��A���?�eӐ�I�����e��Pq�C͔�u��m���][@ms����逸���]�����Z~40/���̓&,������!Z��\��q=�_:�s��b��Q��c�%��D���R��Z��������o�e�/n��y}�6��.֫%��~��?H>Fi�^-���Y��x���&�v8秴��'z����]�WK��m���]h�h�3���Ł}�C��A'�X�3�O��:�<Z#��^�PZ,����nPZ��7Z���b�[���'���g��yp��(]��3v:n�����n�j��>Vᮿ�;j��)YJ��|&�[h��_�f�@��q��Cmt�X+GU�Aiq�'j������n>3[�_��`���'j9��B(E�oMxȅ�b9������ҁ.j�I�%�0� ˝�s�x2���v5���=�(����:����*("���:�C�~��Q���F}��i-�ڴ��U����yѳ�_`�,�3�є=k*܂$�I��x�$[�������(-��-����������b?:�Cz��9��y���.��<��^ƃ�g�@�6���Zҫ��bN[�D�Z�3΃�U��6��<�4����B4З�ʠ_#=�U���Ǒ�莁ֈz������Z�h���Uv��4P�h�팂�����'�ڶQx�h�tA�G�� ju�Q�d=��_����M��?K�D�z�>�%	��b�����=%ܟ��ZD�(�������@��fO�ca��W��~�'"��m���rDg)�OJD%�~�:�a�Hj�O}�
Ո�Z�hx�@���a�(���u	oO�XMn�(z|`?��Q����W�#���Q�Z�x�+�|�+?���|A�m;:���!z2�y3}zЫq����l�KD�
�$�F��,FS�EԼ�b��Q;��@����
�B�5(�H����#�tA˙���!�w��a�� ��勃���UL��6����N�}�C�5�Ն��!)���Yt�Z�W-y�^�C�1aՈ�V�����5>b�tn��g�&���듓�¼D�:]�ˆh����� �y2�yG�Y��]�$�F����S$6���F6���X�˓
�wt��]�	Q��z�g���.��[I��NH莎E�I�PSf�&��n|��<�®C�h	�x���@��4�nj�^P��2��$mԯ>������?I�精�,�Q�����O��EM�J�d�cLR ��.Y )D9��G����h��2�C�#&.�1G�]�'��S�]�9�S��Z#)n�&�����R�z�->u}�1��n9�g$?�������g��S�Yzcܻ���7���Zz[�r{��՞:�9��}~�%��&?�t*D%���a����:�c!ۃI���~��gV�4N_�3�z�	D���}�c���Т3g:�վ�I���'e��:�Q��a5����e�IXVX���>=���?�5�x���a	>��fV��B����[d�@[De&*���-��-���!m�AT���1J[��(
Pޢ��]5����W��6z��D�����9�NLc%�=	@wb"?�ѽ�"���:����>/����X#D�bR� ��*���ط: z��o�|�>�/N-���>��:1��#�1:���v|�4��l�@/E�ڙo��LQ�p�n���� �#�<r\a-Q����c��t�Q�dp�����B�~UL^ZuF��n6�0S@�������\�F(ƫ}���m��p�� �����ǔ�+�T3/Z�������ft���Eq�D�%��E4�Dԏĳ>��Q�г��h���~@�9� mG���F�3����9��-�<	5c;��G�3�g��Ĕ9�~9ly ��精w��]�s	��11%,�,w�0`���GL�>�=f����ٿ�����ur�      	   �   x�]�K
�@�=����g2���#�܌I-с�o�CPx�W�����Ps
�}aBkjjъbbI�(@`'p�W+X&{�6����$a��@[vz�m*x��a5}-�bM�Y��U�N!��k�|��8�?ڪL�P�j�V"xv�1_z����
�&^bM�<�j���so�,L�      	   p   x�%˱0C������%�ϑ�K�F��m#Q8h.b)����%M�(4O��ВKq)S'
����f)���\�ԉ�Acp�Y
-��0u�p�\<m�B��%��q��� �     